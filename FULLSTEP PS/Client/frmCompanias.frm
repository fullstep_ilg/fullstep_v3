VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCompanias 
   Caption         =   "Compa��as"
   ClientHeight    =   7605
   ClientLeft      =   9330
   ClientTop       =   2310
   ClientWidth     =   9915
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCompanias.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7605
   ScaleWidth      =   9915
   Begin VB.Frame fraCias 
      Height          =   615
      Left            =   0
      TabIndex        =   43
      Top             =   0
      Width           =   9855
      Begin FSPSClient.CiaSelector CiaSelector1 
         Height          =   285
         Left            =   3165
         TabIndex        =   60
         Top             =   210
         Width           =   300
         _ExtentX        =   529
         _ExtentY        =   503
      End
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   8550
         Picture         =   "frmCompanias.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   210
         Width           =   300
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   1350
         TabIndex        =   45
         Top             =   210
         Width           =   1740
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   7064
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   3069
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   3525
         TabIndex        =   46
         Top             =   210
         Width           =   4875
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5980
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   8599
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin VB.Label Label7 
         Caption         =   "Compa��a:"
         Height          =   255
         Left            =   240
         TabIndex        =   47
         Top             =   240
         Width           =   840
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6795
      Left            =   15
      TabIndex        =   0
      Top             =   720
      Width           =   9885
      _ExtentX        =   17436
      _ExtentY        =   11986
      _Version        =   393216
      Style           =   1
      Tabs            =   7
      TabsPerRow      =   7
      TabHeight       =   520
      TabCaption(0)   =   "Datos generales"
      TabPicture(0)   =   "frmCompanias.frx":0FF4
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "picEdit"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "picNavigate(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "picDatos"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdEnlazarProveedor"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "picProveGS"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Actividades"
      TabPicture(1)   =   "frmCompanias.frx":1010
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picNavigate(1)"
      Tab(1).Control(1)=   "tvwEstrMatConsul"
      Tab(1).Control(2)=   "ImageList1"
      Tab(1).Control(3)=   "tvwEstrMat"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Observaciones"
      TabPicture(2)   =   "frmCompanias.frx":102C
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "picNavigate(2)"
      Tab(2).Control(1)=   "txtObs"
      Tab(2).Control(2)=   "Label10"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Usuarios"
      TabPicture(3)   =   "frmCompanias.frx":1048
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "picUsu"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Compa��as compradoras"
      TabPicture(4)   =   "frmCompanias.frx":1064
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "sdbgCiasComp"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Archivos adjuntos"
      TabPicture(5)   =   "frmCompanias.frx":1080
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "picSizeAdjuntos"
      Tab(5).Control(1)=   "picEsp"
      Tab(5).Control(2)=   "lstvwEsp"
      Tab(5).ControlCount=   3
      TabCaption(6)   =   "dCategorias laborales"
      TabPicture(6)   =   "frmCompanias.frx":109C
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "sdbgCategorias"
      Tab(6).Control(1)=   "sdbdCategorias"
      Tab(6).Control(2)=   "picEditCat"
      Tab(6).ControlCount=   3
      Begin VB.PictureBox picProveGS 
         BorderStyle     =   0  'None
         Height          =   700
         Left            =   7350
         ScaleHeight     =   705
         ScaleWidth      =   2175
         TabIndex        =   115
         Top             =   5430
         Width           =   2175
         Begin VB.TextBox txtCodProveGS 
            BackColor       =   &H00C0FFFF&
            Enabled         =   0   'False
            Height          =   285
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   117
            Top             =   360
            Width           =   1575
         End
         Begin VB.CommandButton cmdEnlazar 
            Height          =   285
            Left            =   1700
            Picture         =   "frmCompanias.frx":10B8
            Style           =   1  'Graphical
            TabIndex        =   116
            ToolTipText     =   "Enlazar proveedor de GS"
            Top             =   360
            Width           =   300
         End
         Begin VB.Label Label19 
            Caption         =   "C�digo en GS"
            Height          =   255
            Left            =   150
            TabIndex        =   118
            Top             =   90
            Width           =   2175
         End
      End
      Begin VB.CommandButton cmdEnlazarProveedor 
         Caption         =   "Enlazar"
         Enabled         =   0   'False
         Height          =   405
         Left            =   7710
         TabIndex        =   26
         Top             =   5520
         Width           =   1245
      End
      Begin VB.PictureBox picEditCat 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   -74880
         ScaleHeight     =   435
         ScaleWidth      =   6405
         TabIndex        =   108
         Top             =   6345
         Width           =   6405
         Begin VB.CommandButton cmdCancelarCat 
            Caption         =   "dCancelar"
            Height          =   345
            Left            =   4800
            TabIndex        =   112
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptarCat 
            Caption         =   "dAceptar"
            Height          =   345
            Left            =   3720
            TabIndex        =   111
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdModificarCat 
            Caption         =   "dModificar"
            Height          =   345
            Left            =   1080
            TabIndex        =   110
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurarCat 
            Caption         =   "dRestaurar"
            Height          =   345
            Left            =   0
            TabIndex        =   109
            Top             =   0
            Width           =   1005
         End
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbdCategorias 
         Height          =   1260
         Left            =   -74040
         TabIndex        =   106
         Top             =   1200
         Width           =   3990
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCompanias.frx":110C
         stylesets(1).Name=   "Selection"
         stylesets(1).BackColor=   11513775
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCompanias.frx":1128
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1693
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3281
         Columns(2).Caption=   "COSTEHORA"
         Columns(2).Name =   "COSTEHORA"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "BAJALOG"
         Columns(3).Name =   "BAJALOG"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         _ExtentX        =   7038
         _ExtentY        =   2222
         _StockProps     =   77
      End
      Begin VB.PictureBox picSizeAdjuntos 
         BorderStyle     =   0  'None
         Height          =   875
         Left            =   -74760
         ScaleHeight     =   870
         ScaleWidth      =   9375
         TabIndex        =   101
         Top             =   500
         Width           =   9375
         Begin VB.CommandButton cmdGuardarSize 
            Caption         =   "DGuardar cambios"
            Enabled         =   0   'False
            Height          =   345
            Left            =   3600
            TabIndex        =   87
            Top             =   300
            Width           =   1800
         End
         Begin VB.TextBox txtTamanyoMax 
            Height          =   345
            Left            =   0
            TabIndex        =   86
            Top             =   300
            Width           =   2500
         End
         Begin VB.Label lblEspacioLibre 
            AutoSize        =   -1  'True
            Caption         =   "DEspacio libre:"
            Height          =   285
            Left            =   5860
            TabIndex        =   104
            Top             =   340
            Width           =   3400
         End
         Begin VB.Label lblBytes 
            Caption         =   "KBytes"
            Height          =   285
            Left            =   2600
            TabIndex        =   103
            Top             =   340
            Width           =   1000
         End
         Begin VB.Label lblTamanyoMax 
            Caption         =   "DTama�o m�ximo permitido"
            Height          =   285
            Left            =   0
            TabIndex        =   102
            Top             =   0
            Width           =   2415
         End
      End
      Begin VB.PictureBox picEsp 
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         Height          =   400
         Left            =   -68160
         ScaleHeight     =   405
         ScaleWidth      =   2850
         TabIndex        =   91
         Top             =   6200
         Width           =   2850
         Begin VB.CommandButton cmdAbrirEsp 
            Height          =   300
            Left            =   2350
            Picture         =   "frmCompanias.frx":1144
            Style           =   1  'Graphical
            TabIndex        =   96
            TabStop         =   0   'False
            Top             =   60
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarEsp 
            Height          =   300
            Left            =   1870
            Picture         =   "frmCompanias.frx":1486
            Style           =   1  'Graphical
            TabIndex        =   95
            TabStop         =   0   'False
            Top             =   60
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdModifEsp 
            Height          =   300
            Left            =   1390
            Picture         =   "frmCompanias.frx":17C8
            Style           =   1  'Graphical
            TabIndex        =   94
            TabStop         =   0   'False
            Top             =   60
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliEsp 
            Height          =   300
            Left            =   910
            Picture         =   "frmCompanias.frx":1912
            Style           =   1  'Graphical
            TabIndex        =   93
            TabStop         =   0   'False
            Top             =   60
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyaEsp 
            Height          =   300
            Left            =   430
            Picture         =   "frmCompanias.frx":1C54
            Style           =   1  'Graphical
            TabIndex        =   92
            TabStop         =   0   'False
            Top             =   60
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         Height          =   375
         Index           =   2
         Left            =   -74850
         ScaleHeight     =   375
         ScaleWidth      =   9600
         TabIndex        =   76
         Top             =   6255
         Width           =   9600
         Begin VB.CommandButton cmdRestaurar2 
            Caption         =   "&Restaurar"
            Height          =   345
            Left            =   75
            TabIndex        =   90
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdConsulta1 
            Caption         =   "&Consulta"
            Height          =   345
            Left            =   6900
            TabIndex        =   89
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdEditar 
            Caption         =   "&Edici�n"
            Height          =   345
            Index           =   1
            Left            =   8475
            TabIndex        =   77
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         Height          =   450
         Index           =   1
         Left            =   -74835
         ScaleHeight     =   450
         ScaleWidth      =   9525
         TabIndex        =   74
         Top             =   6300
         Width           =   9525
         Begin VB.CommandButton cmdConsulta0 
            Caption         =   "&Consulta"
            Height          =   345
            Left            =   7200
            TabIndex        =   85
            Top             =   45
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar1 
            Caption         =   "&Restaurar"
            Height          =   345
            Left            =   0
            TabIndex        =   84
            Top             =   45
            Width           =   1005
         End
         Begin VB.CommandButton cmdEditar 
            Caption         =   "&Edici�n"
            Height          =   345
            Index           =   0
            Left            =   8505
            TabIndex        =   75
            Top             =   45
            Width           =   1005
         End
      End
      Begin VB.TextBox txtObs 
         Height          =   5505
         Left            =   -74805
         MultiLine       =   -1  'True
         TabIndex        =   59
         Top             =   690
         Width           =   9450
      End
      Begin VB.PictureBox picDatos 
         BorderStyle     =   0  'None
         Height          =   5910
         Left            =   60
         ScaleHeight     =   5910
         ScaleWidth      =   9765
         TabIndex        =   28
         Top             =   360
         Width           =   9765
         Begin VB.Frame fraDir 
            Caption         =   "Datos"
            Height          =   4620
            Left            =   120
            TabIndex        =   29
            Top             =   0
            Width           =   9525
            Begin VB.TextBox txtPMvinculada 
               Height          =   285
               Left            =   1560
               MaxLength       =   100
               TabIndex        =   119
               Top             =   4200
               Width           =   2895
            End
            Begin VB.Frame Frame3 
               Caption         =   "Homologaciones"
               Height          =   1455
               Left            =   4680
               TabIndex        =   54
               Top             =   2350
               Width           =   4695
               Begin VB.TextBox txtHom1 
                  Height          =   285
                  Left            =   240
                  MaxLength       =   50
                  TabIndex        =   17
                  Top             =   285
                  Width           =   4170
               End
               Begin VB.TextBox txtHom2 
                  Height          =   285
                  Left            =   225
                  MaxLength       =   50
                  TabIndex        =   18
                  Top             =   645
                  Width           =   4170
               End
               Begin VB.TextBox txtHom3 
                  Height          =   285
                  Left            =   225
                  MaxLength       =   50
                  TabIndex        =   19
                  Top             =   1005
                  Width           =   4170
               End
            End
            Begin VB.Frame Frame2 
               Caption         =   "Clientes de referencia"
               Height          =   1755
               Left            =   4680
               TabIndex        =   53
               Top             =   550
               Width           =   4695
               Begin VB.TextBox txtCliRef1 
                  Height          =   285
                  Left            =   225
                  MaxLength       =   50
                  TabIndex        =   13
                  Top             =   285
                  Width           =   4170
               End
               Begin VB.TextBox txtCliRef2 
                  Height          =   285
                  Left            =   225
                  MaxLength       =   50
                  TabIndex        =   14
                  Top             =   650
                  Width           =   4170
               End
               Begin VB.TextBox txtCliRef3 
                  Height          =   285
                  Left            =   225
                  MaxLength       =   50
                  TabIndex        =   15
                  Top             =   1015
                  Width           =   4170
               End
               Begin VB.TextBox txtCliRef4 
                  Height          =   285
                  Left            =   225
                  MaxLength       =   50
                  TabIndex        =   16
                  Top             =   1380
                  Width           =   4170
               End
            End
            Begin VB.TextBox txtDen 
               Height          =   285
               Left            =   1560
               MaxLength       =   100
               TabIndex        =   2
               Top             =   615
               Width           =   2895
            End
            Begin VB.TextBox txtPob 
               Height          =   285
               Left            =   1560
               MaxLength       =   100
               TabIndex        =   6
               Top             =   2070
               Width           =   2895
            End
            Begin VB.TextBox txtVol 
               Height          =   285
               Left            =   6465
               TabIndex        =   12
               Top             =   248
               Width           =   1335
            End
            Begin VB.TextBox txtCP 
               Height          =   285
               Left            =   1560
               MaxLength       =   20
               TabIndex        =   5
               Top             =   1710
               Width           =   2895
            End
            Begin VB.TextBox txtNif 
               Height          =   285
               Left            =   1560
               MaxLength       =   20
               TabIndex        =   3
               Top             =   975
               Width           =   2895
            End
            Begin VB.TextBox txtDir 
               Height          =   285
               Left            =   1560
               MaxLength       =   255
               TabIndex        =   4
               Top             =   1335
               Width           =   2895
            End
            Begin VB.TextBox txtURL 
               Height          =   285
               Left            =   1560
               MaxLength       =   255
               TabIndex        =   11
               Top             =   3870
               Width           =   7455
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
               Height          =   285
               Left            =   1560
               TabIndex        =   7
               Top             =   2430
               Width           =   2895
               DataFieldList   =   "Column 0"
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   4
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3228
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Caption=   "C�digo"
               Columns(2).Name =   "COD"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "VALIDNIF"
               Columns(3).Name =   "VALIDNIF"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcIdiCod 
               Height          =   285
               Left            =   1560
               TabIndex        =   10
               Top             =   3510
               Width           =   2895
               DataFieldList   =   "Column 0"
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3545
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Caption=   "C�digo"
               Columns(2).Name =   "COD"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
               Height          =   285
               Left            =   1560
               TabIndex        =   9
               Top             =   3150
               Width           =   2895
               DataFieldList   =   "Column 0"
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3545
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Caption=   "C�digo"
               Columns(2).Name =   "Cod"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
               Height          =   285
               Left            =   1560
               TabIndex        =   8
               Top             =   2790
               Width           =   2895
               DataFieldList   =   "Column 0"
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "ID"
               Columns(0).Name =   "ID"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3545
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Caption=   "C�digo"
               Columns(2).Name =   "COD"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.TextBox txtCod 
               BackColor       =   &H00FFFFFF&
               Height          =   285
               Left            =   1560
               Locked          =   -1  'True
               MaxLength       =   20
               TabIndex        =   1
               Top             =   255
               Width           =   2895
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPortal 
               Height          =   285
               Left            =   6120
               TabIndex        =   113
               Top             =   4200
               Width           =   2895
               DataFieldList   =   "Column 0"
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               HeadLines       =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   3200
               Columns(0).Caption=   "CODIGO"
               Columns(0).Name =   "CODIGO"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   5106
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblPMvinculada 
               AutoSize        =   -1  'True
               Caption         =   "Solicitud vinculada:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   120
               Top             =   4200
               Width           =   1365
            End
            Begin VB.Label Label20 
               Caption         =   "DPortal"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   4680
               TabIndex        =   114
               Top             =   4200
               Width           =   1005
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Denominaci�n:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   42
               Top             =   630
               Width           =   1050
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "C�digo:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   41
               Top             =   270
               Width           =   555
            End
            Begin VB.Label Label16 
               AutoSize        =   -1  'True
               Caption         =   "Provincia:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   40
               Top             =   2790
               Width           =   705
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               Caption         =   "Moneda:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   39
               Top             =   3150
               Width           =   630
            End
            Begin VB.Label Label4 
               AutoSize        =   -1  'True
               Caption         =   "NIF:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   38
               Top             =   990
               Width           =   315
            End
            Begin VB.Label Label3 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   37
               Top             =   2430
               Width           =   345
            End
            Begin VB.Label Label2 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   36
               Top             =   1365
               Width           =   705
            End
            Begin VB.Label Label9 
               AutoSize        =   -1  'True
               Caption         =   "Poblaci�n:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   35
               Top             =   2085
               Width           =   735
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               Caption         =   "CP:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   160
               TabIndex        =   34
               Top             =   1725
               Width           =   255
            End
            Begin VB.Label Label17 
               Caption         =   "Idioma:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   160
               TabIndex        =   33
               Top             =   3510
               Width           =   1000
            End
            Begin VB.Label Label5 
               Caption         =   "Volumen de facturaci�n:"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   4710
               TabIndex        =   32
               Top             =   255
               Width           =   1800
            End
            Begin VB.Label Label14 
               Caption         =   "(miles de Euros)"
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   7875
               TabIndex        =   31
               Top             =   255
               Width           =   1575
            End
            Begin VB.Label Label18 
               AutoSize        =   -1  'True
               Caption         =   "URL:"
               Height          =   195
               Left            =   160
               TabIndex        =   30
               Top             =   3870
               Width           =   345
            End
         End
         Begin VB.Frame fraFuncionProve 
            Caption         =   "Funci�n proveedora"
            Height          =   1215
            Index           =   0
            Left            =   120
            TabIndex        =   55
            Top             =   4650
            Width           =   9525
            Begin VB.PictureBox picPremiumProv 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   790
               Left            =   4400
               ScaleHeight     =   795
               ScaleWidth      =   4395
               TabIndex        =   68
               Top             =   300
               Width           =   4395
               Begin VB.Frame fraPremium 
                  BorderStyle     =   0  'None
                  Height          =   210
                  Left            =   0
                  TabIndex        =   69
                  Top             =   480
                  Width           =   2610
                  Begin VB.OptionButton optPRAut 
                     Caption         =   "Autorizado"
                     Enabled         =   0   'False
                     Height          =   255
                     Left            =   75
                     TabIndex        =   24
                     Top             =   0
                     Width           =   1155
                  End
                  Begin VB.OptionButton optPRDes 
                     Caption         =   "Desautorizado"
                     Enabled         =   0   'False
                     Height          =   255
                     Left            =   1275
                     TabIndex        =   25
                     Top             =   0
                     Width           =   1395
                  End
               End
               Begin VB.CheckBox chkPremium 
                  Caption         =   "Acceso Premium Solicitado"
                  Height          =   285
                  Left            =   0
                  TabIndex        =   23
                  Top             =   45
                  Width           =   2760
               End
            End
            Begin VB.OptionButton opFPDes 
               Caption         =   "Desautorizada"
               Height          =   255
               Left            =   1440
               TabIndex        =   21
               Top             =   360
               Width           =   1395
            End
            Begin VB.OptionButton opFPSol 
               Caption         =   "Solicitada"
               Height          =   255
               Left            =   3000
               TabIndex        =   22
               Top             =   360
               Width           =   1530
            End
            Begin VB.OptionButton opFPAut 
               Caption         =   "Autorizada"
               Height          =   255
               Left            =   120
               TabIndex        =   20
               Top             =   360
               Width           =   1155
            End
            Begin VB.Label lblFecSolicit 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   3060
               TabIndex        =   99
               Top             =   800
               Width           =   1110
            End
            Begin VB.Label lblFecDesAut 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1640
               TabIndex        =   98
               Top             =   800
               Width           =   1110
            End
            Begin VB.Label lblFecAut 
               BackColor       =   &H00C0FFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   220
               TabIndex        =   97
               Top             =   800
               Width           =   1110
            End
         End
      End
      Begin VB.PictureBox picUsu 
         BorderStyle     =   0  'None
         Height          =   6330
         Left            =   -74925
         ScaleHeight     =   6330
         ScaleWidth      =   9690
         TabIndex        =   48
         Top             =   390
         Width           =   9690
         Begin SSDataWidgets_B.SSDBDropDown sdbdCatUsu 
            Height          =   1260
            Left            =   1320
            TabIndex        =   107
            Top             =   960
            Width           =   3990
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            stylesets.count =   2
            stylesets(0).Name=   "Selection"
            stylesets(0).BackColor=   11513775
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCompanias.frx":1F96
            stylesets(1).Name=   "Normal"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCompanias.frx":1FB2
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   4
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1746
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   4180
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   2355
            Columns(3).Caption=   "COSTEHORA"
            Columns(3).Name =   "COSTEHORA"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            _ExtentX        =   7038
            _ExtentY        =   2222
            _StockProps     =   77
         End
         Begin VB.PictureBox picLeyenda 
            AutoSize        =   -1  'True
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   510
            Left            =   5000
            ScaleHeight     =   510
            ScaleWidth      =   3795
            TabIndex        =   61
            Top             =   5835
            Width           =   3800
            Begin VB.Label Label13 
               BackColor       =   &H00C0C0FF&
               Height          =   135
               Left            =   2400
               TabIndex        =   67
               Top             =   225
               Width           =   195
            End
            Begin VB.Label Label12 
               BackColor       =   &H00C0FFFF&
               Height          =   135
               Left            =   105
               TabIndex        =   66
               Top             =   225
               Width           =   195
            End
            Begin VB.Label Label11 
               BackColor       =   &H00C0FFC0&
               Caption         =   "        "
               Height          =   135
               Left            =   1200
               TabIndex        =   65
               Top             =   225
               Width           =   195
            End
            Begin VB.Label lblSolic 
               AutoSize        =   -1  'True
               Caption         =   "Solicitado"
               Height          =   195
               Left            =   345
               TabIndex        =   64
               Top             =   180
               Width           =   675
            End
            Begin VB.Label lblDesaut 
               AutoSize        =   -1  'True
               Caption         =   "Desautorizado"
               Height          =   195
               Left            =   2655
               TabIndex        =   63
               Top             =   180
               Width           =   1035
            End
            Begin VB.Label lblAutoriz 
               AutoSize        =   -1  'True
               Caption         =   "Autorizado"
               Height          =   195
               Left            =   1455
               TabIndex        =   62
               Top             =   180
               Width           =   780
            End
         End
         Begin VB.PictureBox picEditUsu 
            BorderStyle     =   0  'None
            Height          =   435
            Left            =   90
            ScaleHeight     =   435
            ScaleWidth      =   4605
            TabIndex        =   49
            Top             =   5985
            Width           =   4600
            Begin VB.CommandButton cmdA�adirUsu 
               Caption         =   "A&�adir"
               Height          =   345
               Left            =   0
               TabIndex        =   52
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdModificarUsu 
               Caption         =   "&Modificar"
               Height          =   345
               Left            =   1110
               TabIndex        =   51
               Top             =   0
               Width           =   1005
            End
            Begin VB.CommandButton cmdEliminarUsu 
               Caption         =   "Elimi&nar"
               Height          =   345
               Left            =   2220
               TabIndex        =   50
               Top             =   0
               Width           =   1005
            End
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgUsu 
            Height          =   5760
            Left            =   60
            TabIndex        =   27
            Top             =   60
            Width           =   9585
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   13
            stylesets.count =   5
            stylesets(0).Name=   "Rojo"
            stylesets(0).BackColor=   11448063
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmCompanias.frx":1FCE
            stylesets(1).Name=   "Selection"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   11513775
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmCompanias.frx":1FEA
            stylesets(1).AlignmentPicture=   1
            stylesets(2).Name=   "Normal"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmCompanias.frx":2006
            stylesets(3).Name=   "Amarillo"
            stylesets(3).BackColor=   11796479
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmCompanias.frx":2022
            stylesets(4).Name=   "Verde"
            stylesets(4).BackColor=   11796403
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmCompanias.frx":203E
            AllowDelete     =   -1  'True
            ActiveCellStyleSet=   "Normal"
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ActiveRowStyleSet=   "Normal"
            Columns.Count   =   13
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1905
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16776960
            Columns(2).Width=   3228
            Columns(2).Caption=   "Nombre"
            Columns(2).Name =   "NOM"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).HasBackColor=   -1  'True
            Columns(2).BackColor=   16776960
            Columns(3).Width=   4471
            Columns(3).Caption=   "Apellidos"
            Columns(3).Name =   "APE"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).HasBackColor=   -1  'True
            Columns(3).BackColor=   16776960
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "Idioma"
            Columns(4).Name =   "IDI"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).Locked=   -1  'True
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "F.C. aut."
            Columns(5).Name =   "FCAUT"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   11
            Columns(5).FieldLen=   256
            Columns(5).Style=   2
            Columns(6).Width=   1349
            Columns(6).Caption=   "F.P. aut"
            Columns(6).Name =   "FPAUT"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   11
            Columns(6).FieldLen=   256
            Columns(6).Style=   2
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "COMP"
            Columns(7).Name =   "COMP"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "PROV"
            Columns(8).Name =   "PROV"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   1376
            Columns(9).Caption=   "Ppal"
            Columns(9).Name =   "PPAL"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   11
            Columns(9).FieldLen=   256
            Columns(9).Style=   2
            Columns(10).Width=   2831
            Columns(10).Caption=   "CATEGORIA"
            Columns(10).Name=   "CATEGORIA"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   1852
            Columns(11).Caption=   "ACCESOFSGA"
            Columns(11).Name=   "ACCESOFSGA"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Style=   2
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "IDCATEGORIA"
            Columns(12).Name=   "IDCATEGORIA"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            _ExtentX        =   16907
            _ExtentY        =   10160
            _StockProps     =   79
            Caption         =   "Usuarios"
            ForeColor       =   0
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin MSComctlLib.TreeView tvwEstrMatConsul 
         Height          =   5835
         Left            =   -74835
         TabIndex        =   57
         Top             =   420
         Width           =   9525
         _ExtentX        =   16801
         _ExtentY        =   10292
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   -74000
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   13
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":205A
               Key             =   "Raiz"
               Object.Tag             =   "Raiz"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":24AE
               Key             =   "ACT"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":25C0
               Key             =   "ACTASIG"
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":26D2
               Key             =   "Raiz2"
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":2B24
               Key             =   "GMN4"
               Object.Tag             =   "GMN4"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":2E78
               Key             =   "GMN4A"
               Object.Tag             =   "GMN4A"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":31CC
               Key             =   "GMN1"
               Object.Tag             =   "GMN1"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":3520
               Key             =   "GMN1A"
               Object.Tag             =   "GMN1A"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":3874
               Key             =   "GMN2"
               Object.Tag             =   "GMN2"
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":3BC8
               Key             =   "GMN2A"
               Object.Tag             =   "GMN2A"
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":3F1C
               Key             =   "GMN3A"
               Object.Tag             =   "GMN3A"
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":4270
               Key             =   "GMN3"
               Object.Tag             =   "GMN3"
            EndProperty
            BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCompanias.frx":45C4
               Key             =   "ESP"
               Object.Tag             =   "ESP"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwEstrMat 
         Height          =   5835
         Left            =   -74835
         TabIndex        =   56
         Top             =   420
         Width           =   8685
         _ExtentX        =   15319
         _ExtentY        =   10292
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCiasComp 
         Height          =   5760
         Left            =   -74865
         TabIndex        =   82
         Top             =   450
         Width           =   9585
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   5
         stylesets.count =   5
         stylesets(0).Name=   "Rojo"
         stylesets(0).BackColor=   11448063
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCompanias.frx":471E
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCompanias.frx":473A
         stylesets(2).Name=   "Selection"
         stylesets(2).ForeColor=   0
         stylesets(2).BackColor=   11513775
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmCompanias.frx":4756
         stylesets(2).AlignmentPicture=   1
         stylesets(3).Name=   "Amarillo"
         stylesets(3).BackColor=   11796479
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmCompanias.frx":4772
         stylesets(4).Name=   "Verde"
         stylesets(4).BackColor=   11796403
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmCompanias.frx":478E
         AllowDelete     =   -1  'True
         ActiveCellStyleSet=   "Normal"
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ActiveRowStyleSet=   "Normal"
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   3122
         Columns(1).Caption=   "C�digo de compa��a"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   7117
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16776960
         Columns(3).Width=   2884
         Columns(3).Caption=   "Solicitud de registro"
         Columns(3).Name =   "SOLICITADA"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   11
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   2
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16776960
         Columns(4).Width=   2805
         Columns(4).Caption=   "Registrada"
         Columns(4).Name =   "REGISTRADA"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   11
         Columns(4).FieldLen=   256
         Columns(4).Style=   2
         _ExtentX        =   16907
         _ExtentY        =   10160
         _StockProps     =   79
         Caption         =   "Compa��as compradoras"
         ForeColor       =   0
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ListView lstvwEsp 
         Height          =   4755
         Left            =   -74760
         TabIndex        =   88
         Top             =   1380
         Width           =   9375
         _ExtentX        =   16536
         _ExtentY        =   8387
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCategorias 
         Height          =   5760
         Left            =   -74880
         TabIndex        =   105
         Top             =   420
         Width           =   9585
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   4
         stylesets.count =   5
         stylesets(0).Name=   "Rojo"
         stylesets(0).BackColor=   11448063
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCompanias.frx":47AA
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCompanias.frx":47C6
         stylesets(2).Name=   "Selection"
         stylesets(2).ForeColor=   0
         stylesets(2).BackColor=   11513775
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmCompanias.frx":47E2
         stylesets(2).AlignmentPicture=   1
         stylesets(3).Name=   "Amarillo"
         stylesets(3).BackColor=   11796479
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmCompanias.frx":47FE
         stylesets(4).Name=   "Verde"
         stylesets(4).BackColor=   11796403
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmCompanias.frx":481A
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         ActiveCellStyleSet=   "Normal"
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ActiveRowStyleSet=   "Normal"
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   1667
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   2805
         Columns(2).Caption=   "COSTEHORA"
         Columns(2).Name =   "COSTEHORA"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16777215
         Columns(3).Width=   1746
         Columns(3).Caption=   "BAJALOG"
         Columns(3).Name =   "BAJALOG"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   2
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16777215
         _ExtentX        =   16907
         _ExtentY        =   10160
         _StockProps     =   79
         Caption         =   "dCategor�as laborales"
         ForeColor       =   0
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         Height          =   375
         Index           =   0
         Left            =   165
         ScaleHeight     =   375
         ScaleWidth      =   8685
         TabIndex        =   70
         Top             =   6345
         Width           =   8685
         Begin VB.CommandButton cmdCodigo 
            Caption         =   "&Cambiar c�digo"
            Enabled         =   0   'False
            Height          =   345
            Left            =   2220
            TabIndex        =   100
            Top             =   0
            Width           =   1515
         End
         Begin VB.CommandButton cmdRestaurar0 
            Caption         =   "&Restaurar"
            Height          =   345
            Left            =   4890
            TabIndex        =   83
            Top             =   0
            Width           =   1065
         End
         Begin VB.CommandButton CmdListado 
            Caption         =   "&Listado"
            Height          =   345
            Left            =   6015
            TabIndex        =   81
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdModificar 
            Caption         =   "&Modificar"
            Height          =   345
            Left            =   1110
            TabIndex        =   73
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdA�adir 
            Caption         =   "A&�adir"
            Height          =   345
            Left            =   0
            TabIndex        =   72
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliminar 
            Caption         =   "Elimi&nar"
            Height          =   345
            Left            =   3825
            TabIndex        =   71
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEdit 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   375
         Left            =   165
         ScaleHeight     =   375
         ScaleWidth      =   8685
         TabIndex        =   78
         Top             =   6345
         Visible         =   0   'False
         Width           =   8685
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Height          =   345
            Left            =   3210
            TabIndex        =   80
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "&Cancelar"
            Height          =   345
            Left            =   4425
            TabIndex        =   79
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.Label Label10 
         Caption         =   "Observaciones acerca de la compa��a:"
         Height          =   285
         Left            =   -74595
         TabIndex        =   58
         Top             =   435
         Width           =   5820
      End
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "DSeleccione plantilla"
      Filter          =   "Plantillas de Word (*.dot)|*.dot"
   End
End
Attribute VB_Name = "frmCompanias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Const LONGCOD = 20

Private m_oCias As CCias
Public g_oCiaSeleccionada As CCia
Private m_oUsuarioSeleccionado As CUsuario

Private m_bCargarComboDesde As Boolean
Public m_bRespetarCombo As Boolean
Private m_CargarComboDesde As Boolean

Private m_nodxSelNode As MSComctlLib.Node
Private m_oActsN1Seleccionados As FSPSServer.CActividadesNivel1
Private m_oActsN2Seleccionados As FSPSServer.CActividadesNivel2
Private m_oActsN3Seleccionados As FSPSServer.CActividadesNivel3
Private m_oActsN4Seleccionados As FSPSServer.CActividadesNivel4
Private m_oActsN5Seleccionados As FSPSServer.CActividadesNivel5

Private m_oActsN1DesSeleccionados As CActividadesNivel1
Private m_oActsN2DesSeleccionados As FSPSServer.CActividadesNivel2
Private m_oActsN3DesSeleccionados As FSPSServer.CActividadesNivel3
Private m_oActsN4DesSeleccionados As FSPSServer.CActividadesNivel4
Private m_oActsN5DesSeleccionados As FSPSServer.CActividadesNivel5

Private m_oActsN1 As FSPSServer.CActividadesNivel1
Private m_oActsN2 As FSPSServer.CActividadesNivel2
Private m_oActsN3 As FSPSServer.CActividadesNivel3
Private m_oActsN4 As FSPSServer.CActividadesNivel4
Private m_oActsN5 As FSPSServer.CActividadesNivel5

Private m_bMouseDown As Boolean
Public g_sOrigen As String
Public g_sComentario As String
Public g_bCancelarEsp As Boolean
Public g_frmEnProves As Form
Public g_frmEnProve As Form
Public g_sCodigoNuevo As String

Private m_bAnyadirCia As Boolean
Private m_bModifCia As Boolean
Private m_bActividadModif As Boolean
Private m_bLoad As Boolean
Private m_iDesactivarPremium As Integer

'Constantes para los colores
Private Const m_ROJO = "Rojo"
Private Const m_VERDE = "Verde"
Private Const m_AMARILLO = "Amarillo"
Private Const m_GRIS = &H80000004
Private Const m_lblAmarillo = &HC0FFFF
Private Const m_lblVerde = &HC0FFC0

'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private m_sArFileNames() As String

Private m_bSesionIniciada As Boolean

Private m_bDesautorizaCancel As Boolean

Private m_bProvinciaObligatoria As Boolean ' Indica si la provincia es obligatoria
Private m_bVariosPortales As Boolean
Private m_sDenMonDef As String
Private m_sCodMonDef As String  'Variable que contiene el c�digo de la moneda central.
Private m_sDenPaiDef As String
Private m_lIdPaiDef As Long
Private m_lIdMonPai As Variant
Private m_sDenIdiDef As String

Private sIdiSelFichero As String
Private sIdiTodosArch As String
Private sIdiElArch As String
Private sIdiElUsu As String
Private sIdiUsuario As String
Private sIdiGuardarEsp As String
Private sIdiTipoOriginal As String
Private sIdiArch As String
Private sIdiFichero As String
Private sIdiComent As String
Private sIdiFecha As String
Private sIdiPai As String
Private sIdiCodigo As String
Private sIdiCodCompania As String
Private sIdiDen As String
Private sIdiAccesPortal As String
Private sIdiNif As String
Private sIdiDir As String
Private sIdiCP As String
Private sIdiPob As String
Private sIdiMon As String
Private sIdiVolFact As String
Private sIdiIdioma As String
Private sIdiProvi As String
Private sIdiAct As String
Private sIdiNotifAut As String
Private sIdiNotifDesaut As String
Private sIdiNoDisponible As String
Private sIdiSi As String
Private sIdiNo As String
Private sIdiAbrevPend As String
Private sIdiAbrevTodas As String
Private sIdiCiaCod As String
Private msIdiProve As String
Private m_sIdiLibre As String

Private m_bBloquearModif As Boolean 'Si hay que bloquear la modificaci�n de nif etc

Private m_oIdiomas As CIdiomas  'Aqu� mantenemos todos los idiomas de la aplicaci�n, teniendo cargado en primer lugar el idioma del usuario.
Private m_sLitDenom As String

Private m_oCategoriasCod As CCategorias  'Colecci�n con todas las categor�as laborales ordenadas por bajalog-c�digo
Private m_oCategoriasDen As CCategorias  'Colecci�n con todas las categor�as laborales ordenadas por bajalog-denominaci�n del idioma del usuario.
Public m_oCategoriasProve As CCategorias   'Colecci�n con las categor�as del proveedor.

Private m_bRespetar_sdbgCategorias_BeforeRowColChange As Boolean
Private m_bRespetar_sdbgCategorias_BeforeDelete As Boolean
Private m_bAceptarCategorias As Boolean
Private m_sCarpeta_Plantilla As String

Private m_IdCia_Ant As Long

'Variables para registrar la acci�n de modificar datos generales
Private m_sDen_Inicial As String
Private m_sNIF_Inicial As String
Private m_sDir_Inicial As String
Private m_sCP_Inicial  As String
Private m_sPob_Inicial  As String
Private m_sPais_Inicial As String
Private m_sProvi_Inicial As String
Private m_sMon_Inicial As String
Private m_sIdioma_Inicial As String
Private m_sURL_Inicial As String
Private m_lPMVinculada_Inicial As Long
Private m_sPortal_Inicial As String
Private m_dblVolumen_Inicial As Double
Private m_sPCliRef1_Inicial As String
Private m_sPCliRef2_Inicial As String
Private m_sPCliRef3_Inicial As String
Private m_sPCliRef4_Inicial As String
Private m_sHom1_Inicial As String
Private m_sHom2_Inicial As String
Private m_sHom3_Inicial As String

Private m_iAutorizacion_Inicial As Integer
Private m_bPremium_Inicial As Boolean
Private m_iPremium_Inicial As Integer
Private Function Acceso_Cambiado(ByRef sLitRegAccion() As String) As String
Dim s As String
Select Case m_iAutorizacion_Inicial
    Case 1
        If Not opFPSol.Value Then
            s = " " & sLitRegAccion(0) & ": " & sLitRegAccion(44)
        End If
    Case 2
        If Not opFPDes.Value Then
            s = " " & sLitRegAccion(0) & ": " & sLitRegAccion(16)
        End If
    Case 3
        If Not opFPAut.Value Then
            s = " " & sLitRegAccion(0) & ": " & sLitRegAccion(3)
        End If
    Case Else
        s = " " & sLitRegAccion(0) & ": " & sLitRegAccion(26)
End Select
If s <> "" Then
    If opFPSol.Value Then
        s = s & ", " & sLitRegAccion(28) & ": " & sLitRegAccion(44)
    ElseIf opFPDes.Value Then
        s = s & ", " & sLitRegAccion(28) & ": " & sLitRegAccion(16)
    ElseIf opFPAut.Value Then
        s = s & ", " & sLitRegAccion(28) & ": " & sLitRegAccion(3)
    Else    'Lo vaciamos porque coincide con lo inicial
        s = ""
    End If
End If
If s <> "" Then
    s = s & ";"
End If
If m_bPremium_Inicial And Not chkPremium.Value = Checked Then
    s = s & " " & sLitRegAccion(34) & ";"
ElseIf Not m_bPremium_Inicial And chkPremium.Value = Checked Then
    s = s & " " & sLitRegAccion(32) & ";"
ElseIf m_bPremium_Inicial Then
    Select Case m_iPremium_Inicial
        Case 0
            If optPRAut.Value Then
                s = s & " " & sLitRegAccion(33) & ": " & sLitRegAccion(43) & ", " & sLitRegAccion(28) & ": " & sLitRegAccion(4)
            ElseIf optPRDes.Value Then
                s = s & " " & sLitRegAccion(33) & ": " & sLitRegAccion(43) & ", " & sLitRegAccion(28) & ": " & sLitRegAccion(17)
            End If
        Case 1
            If optPRDes.Value Then
                s = s & " " & sLitRegAccion(33) & ": " & sLitRegAccion(4) & ", " & sLitRegAccion(28) & ": " & sLitRegAccion(17)
            End If
        Case 2
            If optPRAut.Value Then
                s = s & " " & sLitRegAccion(33) & ": " & sLitRegAccion(17) & ", " & sLitRegAccion(28) & ": " & sLitRegAccion(4)
            End If
    End Select
End If
Acceso_Cambiado = s
End Function

Private Sub chkPremium_Click()
If chkPremium.Value = vbChecked Then
    optPRAut.Enabled = True
    optPRDes.Enabled = True
Else
    optPRAut.Value = False
    optPRDes.Value = False
    optPRAut.Enabled = False
    optPRDes.Enabled = False
End If

End Sub

Private Sub CiaSelector1_Click(ByVal opcion As PSSeleccion)
    sdbcCiaCod = ""
End Sub



Private Sub cmdAbrirEsp_Click()
    Dim Item As MSComctlLib.ListItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim oError As CTESError
    Dim FOSFile As Scripting.FileSystemObject
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim rutaWS As String
    Dim ador As ador.Recordset
    Dim oCias As CCias
        
    On Error GoTo Cancelar:
    
    If g_oCiaSeleccionada Is Nothing Then Exit Sub
    
    Set Item = lstvwEsp.SelectedItem
    
    If Item Is Nothing Then
        basMensajes.SeleccioneFichero
    Else
                
        sFileName = basUtilidades.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
        
        ' Cargamos el contenido en la esp.
        Screen.MousePointer = vbHourglass
        Set oEsp = g_oCiaSeleccionada.Especificaciones.Item(CStr(lstvwEsp.SelectedItem.Tag))
        
        Set oError = oEsp.ComenzarLecturaData
        If oError.NumError <> 0 Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError oError
            Set oEsp = Nothing
            Exit Sub
        End If
                
        Set oCias = g_oRaiz.Generar_CCias
        Set ador = oCias.DevolverCiasCompradoras()
        
        oEsp.LeerAdjunto ador.Fields("ID").Value, oEsp.Cia.Id, oEsp.DataSize, sFileName
        
        ador.Close
        Set ador = Nothing
        
        m_sArFileNames(UBound(m_sArFileNames)) = sFileName
        ReDim Preserve m_sArFileNames(UBound(m_sArFileNames) + 1)
        
        Screen.MousePointer = vbNormal
        
        'Lanzamos la aplicacion
        ShellExecute frmMDI.hWnd, "Open", sFileName, 0&, "C:\", 1
                
        g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Archivo_Abrir, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_SPA(2) & ": " & oEsp.Nombre, _
                                          g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_ENG(2) & ": " & oEsp.Nombre, _
                                          g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_FRA(2) & ": " & oEsp.Nombre, _
                                          g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_GER(2) & ": " & oEsp.Nombre, _
                                          g_oCiaSeleccionada.Id
    End If
    
Cancelar:
    
    Screen.MousePointer = vbNormal
    Unload frmProgreso
    
    Set oEsp = Nothing
    Set oCias = Nothing

End Sub

'**********************************************************************************************
'*** Descripci�n: Confirma la acci�n en curso.                                            *****
'*** Par�metros:                                                                          *****
'*** Valor que devuelve:                                                                  *****
'**********************************************************************************************

Private Sub cmdAceptar_Click()
    Dim oTESError As CTESError
    Dim mostrarEspera As Boolean
    Dim actPremium As Integer
    Dim sAcceso_Cambiado As String
    mostrarEspera = False
    If g_udtParametrosGenerales.g_bPremium Then
        If chkPremium And optPRAut Then
            actPremium = 2
        ElseIf chkPremium And optPRDes Then
            actPremium = 3
        ElseIf chkPremium Then
            actPremium = 1
        Else
            actPremium = 0
        End If
        If NullToDbl0(g_oCiaSeleccionada.Premium) <> actPremium Then
            mostrarEspera = True
        End If

    End If
    
    If Not Validacion Then
        Exit Sub
    Else
        If m_iDesactivarPremium = vbYes Then
            mostrarEspera = True
        End If

        Screen.MousePointer = vbHourglass
        If Not m_bAnyadirCia Then  'Modifica la cia
            
            If txtCodProveGS.Text <> "" Then
                Dim oProvesVinculados As CProveedores
                Dim sProvesVinc As String
                Dim oProve As CProveedor
                Dim iRet As Integer
                Dim ador As ador.Recordset
                Dim oCias As CCias
                Set oCias = g_oRaiz.Generar_CCias
                
                Set ador = oCias.DevolverCiasCompradoras()
                sProvesVinc = ""
                While Not ador.EOF
                    If g_oRaiz.CodigosErpActivado(ador.Fields("ID").Value) Then
                        Set oProvesVinculados = g_oRaiz.ProveedoresVinculados(ador.Fields("ID").Value, g_oCiaSeleccionada.NIF, g_oCiaSeleccionada.Cod)
                        If oProvesVinculados.Count > 0 Then
                            If g_udtParametrosGenerales.g_bUnaCompradora Then
                                sProvesVinc = sProvesVinc & msIdiProve & ": " & vbCrLf
                            Else
                                sProvesVinc = sProvesVinc & msIdiProve & "(" & ador.Fields("DEN").Value & "): " & vbCrLf
                            End If
                            For Each oProve In oProvesVinculados
                                sProvesVinc = sProvesVinc & oProve.Cod & " - " & oProve.Den & vbCrLf
                            Next
                        End If
                        Set oProvesVinculados = Nothing
                    End If
                    ador.MoveNext
                Wend
                
                ador.Close
                Set ador = Nothing
                If sProvesVinc <> "" Then
                    iRet = basMensajes.ExistenProveedoresVinculados(sProvesVinc)
                    If iRet = vbNo Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
            End If
            
            
            If mostrarEspera Then
                frmEsperaPortal.Show
                DoEvents
            End If
            
            
            Set oTESError = g_oCiaSeleccionada.ModificarDatosCia(, g_oRaiz.SPTS)
            If mostrarEspera Then
                Unload frmEsperaPortal
                DoEvents
            End If
            If oTESError.NumError <> TESnoerror Then
                basErrores.TratarError oTESError
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Modif_DatosGen, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(13) & ":" & DatosGen_Cambiados(g_sLitRegAccion_SPA()), _
                                                g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_ENG(13) & ":" & DatosGen_Cambiados(g_sLitRegAccion_ENG()), _
                                                g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(13) & ":" & DatosGen_Cambiados(g_sLitRegAccion_FRA()), _
                                                g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(13) & ":" & DatosGen_Cambiados(g_sLitRegAccion_GER()), _
                                                g_oCiaSeleccionada.Id
                sAcceso_Cambiado = Acceso_Cambiado(g_sLitRegAccion_SPA())
                If sAcceso_Cambiado <> "" Then
                    g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Modif_Acceso, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(13) & ":" & sAcceso_Cambiado, _
                                                g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_ENG(13) & ":" & Acceso_Cambiado(g_sLitRegAccion_ENG()), _
                                                g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(13) & ":" & Acceso_Cambiado(g_sLitRegAccion_FRA()), _
                                                g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(13) & ":" & Acceso_Cambiado(g_sLitRegAccion_GER()), _
                                                g_oCiaSeleccionada.Id
                End If
                basMensajes.ActualizacionCorrecta
                BloquearDatosProve False
                m_bModifCia = False
                ModoConsultaGen
            End If
            If g_oCiaSeleccionada.EstadoProveedora > 0 And Not g_udtParametrosGenerales.g_bUnaCompradora Then
                SSTab1.TabVisible(4) = True
            End If
            
        Else  'Inserta la cia
            If g_oCiaSeleccionada.EstadoCompradora > 0 And g_oCiaSeleccionada.EstadoCompradora <> 2 Then
                basMensajes.AvisoLinkedServer
            End If
            Set oTESError = g_oCiaSeleccionada.AnyadirCompania
            If oTESError.NumError <> TESnoerror Then
                basErrores.TratarError oTESError
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                'Registramos la acci�n
                g_oGestorAcciones.RegistrarAccion g_sCodADM, Cia_Anyadir, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_oCiaSeleccionada.Id
                                                
                sdbcCiaCod.Text = g_oCiaSeleccionada.Cod
                CiaSelector1.Seleccion = PSSeleccion.PSTodos
                sdbcCiaCod_Validate False
                basMensajes.ActualizacionCorrecta

                'Pasamos a modo consulta
                ModoConsultaGen
                
                txtCod.Locked = True

                'Hace visibles los tabs
                SSTab1.TabVisible(1) = True
                SSTab1.TabVisible(2) = True
                SSTab1.TabVisible(3) = True
                If g_oCiaSeleccionada.EstadoProveedora > 0 And Not g_udtParametrosGenerales.g_bUnaCompradora Then
                    SSTab1.TabVisible(4) = True
                End If
                SSTab1.TabVisible(5) = True
                
                m_bAnyadirCia = False
                m_bActividadModif = False
                m_bModifCia = False
            End If
        End If
        
        Screen.MousePointer = vbNormal
    
    End If
    RestaurarAut
  
End Sub

'''Aqu� se guardar�n en las variables los datos iniciales de la compa��a para compararlas cuando se quieran modificar y poder registrar la acci�n con los datos cambiados
Private Sub DatosGen_Iniciales()

m_sDen_Inicial = txtDen.Text
m_sNIF_Inicial = txtNif.Text
m_sDir_Inicial = txtDir.Text
m_sCP_Inicial = txtCP.Text
m_sPob_Inicial = txtPob.Text
m_sPais_Inicial = sdbcPaiCod.Text
m_sProvi_Inicial = sdbcProviCod.Text
m_sMon_Inicial = sdbcMonCod.Text
m_sIdioma_Inicial = sdbcIdiCod.Text
m_sURL_Inicial = txtURL.Text
m_lPMVinculada_Inicial = IIf(txtPMvinculada.Text = "", 0, txtPMvinculada.Text)
m_sPortal_Inicial = sdbcPortal.Text
m_dblVolumen_Inicial = CDbl(IIf(txtVol.Text = "", 0, txtVol.Text))
m_sPCliRef1_Inicial = txtCliRef1.Text
m_sPCliRef2_Inicial = txtCliRef2.Text
m_sPCliRef3_Inicial = txtCliRef3.Text
m_sPCliRef4_Inicial = txtCliRef4.Text
m_sHom1_Inicial = txtHom1.Text
m_sHom2_Inicial = txtHom2.Text
m_sHom3_Inicial = txtHom3.Text

m_iAutorizacion_Inicial = g_oCiaSeleccionada.EstadoProveedora
m_bPremium_Inicial = g_oCiaSeleccionada.Premium
If optPRAut.Value Then
    m_iPremium_Inicial = 1
ElseIf optPRAut.Value Then
    m_iPremium_Inicial = 2
Else
    m_iPremium_Inicial = 0
End If
End Sub

Private Function DatosGen_Cambiados(ByRef sLitRegAccion() As String) As String
Dim s As String

If m_sDen_Inicial <> txtDen.Text Then s = " " & sLitRegAccion(14) & ";"
If m_sNIF_Inicial <> txtNif.Text Then s = s & " " & sLitRegAccion(25) & ";"
If m_sDir_Inicial <> txtDir.Text Then s = s & " " & sLitRegAccion(18) & ";"
If m_sCP_Inicial <> txtCP.Text Then s = s & " " & sLitRegAccion(12) & ";"
If m_sPob_Inicial <> txtPob.Text Then s = s & " " & sLitRegAccion(30) & ";"
If m_sPais_Inicial <> sdbcPaiCod.Text Then s = s & " " & sLitRegAccion(29) & ";"
If m_sProvi_Inicial <> sdbcProviCod.Text Then s = s & " " & sLitRegAccion(41) & ";"
If m_sMon_Inicial <> sdbcMonCod.Text Then s = s & " " & sLitRegAccion(24) & ";"
If m_sIdioma_Inicial <> sdbcIdiCod.Text Then s = s & " " & sLitRegAccion(22) & ";"
If m_sURL_Inicial <> txtURL.Text Then s = s & " " & sLitRegAccion(47) & ";"
If m_lPMVinculada_Inicial <> CDbl(IIf(txtPMvinculada.Text = "", 0, txtPMvinculada.Text)) Then s = s & " " & sLitRegAccion(55) & ";"

If m_sPortal_Inicial <> sdbcPortal.Text Then s = s & " " & sLitRegAccion(31) & ";"
If m_dblVolumen_Inicial <> CDbl(IIf(txtVol.Text = "", 0, txtVol.Text)) Then s = s & " " & sLitRegAccion(49) & ";"
If m_sPCliRef1_Inicial <> txtCliRef1.Text Then s = s & " " & sLitRegAccion(9) & " 1;"
If m_sPCliRef2_Inicial <> txtCliRef2.Text Then s = s & " " & sLitRegAccion(9) & " 2;"
If m_sPCliRef3_Inicial <> txtCliRef3.Text Then s = s & " " & sLitRegAccion(9) & " 3;"
If m_sPCliRef4_Inicial <> txtCliRef4.Text Then s = s & " " & sLitRegAccion(9) & " 4;"
If m_sHom1_Inicial <> txtHom1.Text Then s = s & " " & sLitRegAccion(21) & " 1;"
If m_sHom2_Inicial <> txtHom2.Text Then s = s & " " & sLitRegAccion(21) & " 2;"
If m_sHom3_Inicial <> txtHom3.Text Then s = s & " " & sLitRegAccion(21) & " 3;"

If s = "" Then
    s = sLitRegAccion(26)
End If
DatosGen_Cambiados = s

End Function


Private Function AceptarActiv()
Dim nodx1 As MSComctlLib.Node
Dim nodx2 As MSComctlLib.Node
Dim nodx3 As MSComctlLib.Node
Dim nodx4 As MSComctlLib.Node
Dim nodx5 As MSComctlLib.Node
Dim nodx As MSComctlLib.Node
Dim teserror As CTESError
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim iTipo As Integer
Dim sProviId As String


    Set m_oActsN1Seleccionados = g_oRaiz.generar_CActividadesNivel1
    Set m_oActsN2Seleccionados = g_oRaiz.Generar_CActividadesNivel2
    Set m_oActsN3Seleccionados = g_oRaiz.Generar_CActividadesNivel3
    Set m_oActsN4Seleccionados = g_oRaiz.Generar_CActividadesNivel4
    Set m_oActsN5Seleccionados = g_oRaiz.Generar_CActividadesNivel5
    Set m_oActsN1DesSeleccionados = g_oRaiz.generar_CActividadesNivel1
    Set m_oActsN2DesSeleccionados = g_oRaiz.Generar_CActividadesNivel2
    Set m_oActsN3DesSeleccionados = g_oRaiz.Generar_CActividadesNivel3
    Set m_oActsN4DesSeleccionados = g_oRaiz.Generar_CActividadesNivel4
    Set m_oActsN5DesSeleccionados = g_oRaiz.Generar_CActividadesNivel5
    
    
    If tvwEstrMat.Nodes.Count > 0 Then
    
        Set nodx = tvwEstrMat.Nodes(1)
           If nodx.Children = 0 Then Exit Function
            
           Set nodx1 = nodx.Child
           
                While Not nodx1 Is Nothing
                    scod1 = DevolverId(nodx1) & Mid$("                         ", 1, 10 - Len(DevolverId(nodx1)))
                    ' No asignado
                    If Not nodx1.Checked Then
                        If Not m_oActsN1 Is Nothing Then
                            If Not m_oActsN1.Item(scod1) Is Nothing Then
                                ' Antes si
                                m_oActsN1DesSeleccionados.Add DevolverId(nodx1), "", ""
                            End If
                        End If
                        
                        Set nodx2 = nodx1.Child
                        
                        While Not nodx2 Is Nothing
                            scod2 = DevolverId(nodx2) & Mid$("                         ", 1, 10 - Len(DevolverId(nodx2)))
                            ' No asignado
                            If Not nodx2.Checked Then
                    
                                If Not m_oActsN2 Is Nothing Then
                                 If Not m_oActsN2.Item(scod1 & scod2) Is Nothing Then
                                    ' Antes si
                                    m_oActsN2DesSeleccionados.Add DevolverId(nodx2.Parent), DevolverId(nodx2), "", ""
                                 End If
                                End If
                                Set nodx3 = nodx2.Child
                                While Not nodx3 Is Nothing
                                    scod3 = DevolverId(nodx3) & Mid$("                         ", 1, 10 - Len(DevolverId(nodx3)))
                                    If Not nodx3.Checked Then
                                        
                                        If Not m_oActsN3 Is Nothing Then
                                         If Not m_oActsN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                            ' Antes si
                                             m_oActsN3DesSeleccionados.Add DevolverId(nodx3.Parent.Parent), DevolverId(nodx3.Parent), DevolverId(nodx3), "", ""
                                         End If
                                        End If
                                        Set nodx4 = nodx3.Child
                                        
                                        While Not nodx4 Is Nothing
                                            
                                            scod4 = DevolverId(nodx4) & Mid$("                         ", 1, 10 - Len(DevolverId(nodx4)))
                                                        
                                            If Not nodx4.Checked Then
                                                If Not m_oActsN4 Is Nothing Then
                                                 If Not m_oActsN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                                    ' Antes si
                                                     m_oActsN4DesSeleccionados.Add DevolverId(nodx4.Parent.Parent.Parent), DevolverId(nodx4.Parent.Parent), DevolverId(nodx4.Parent), DevolverId(nodx4), "", ""
                                                 End If
                                                End If
                                                Set nodx5 = nodx4.Child
                                                
                                                While Not nodx5 Is Nothing
                                                    scod5 = DevolverId(nodx5) & Mid$("                         ", 1, 10 - Len(DevolverId(nodx5)))
                                                        
                                                    If Not nodx5.Checked Then
                                                        If Not m_oActsN5 Is Nothing Then
                                                         If Not m_oActsN5.Item(scod1 & scod2 & scod3 & scod4 & scod5) Is Nothing Then
                                                             ' Antes si
                                                             m_oActsN5DesSeleccionados.Add DevolverId(nodx5.Parent.Parent.Parent.Parent), DevolverId(nodx5.Parent.Parent.Parent), DevolverId(nodx5.Parent.Parent), DevolverId(nodx5.Parent), DevolverId(nodx5), "", ""
                                                         End If
                                                        End If
                                                    Else
                                                        If m_oActsN5 Is Nothing Then
                                                             m_oActsN5Seleccionados.Add DevolverId(nodx5.Parent.Parent.Parent.Parent), DevolverId(nodx5.Parent.Parent.Parent), DevolverId(nodx5.Parent.Parent), DevolverId(nodx5.Parent), DevolverId(nodx5), "", ""
                                                        ElseIf m_oActsN5.Item(scod1 & scod2 & scod3 & scod4 & scod5) Is Nothing Then
                                                             m_oActsN5Seleccionados.Add DevolverId(nodx5.Parent.Parent.Parent.Parent), DevolverId(nodx5.Parent.Parent.Parent), DevolverId(nodx5.Parent.Parent), DevolverId(nodx5.Parent), DevolverId(nodx5), "", ""
                                                        End If
                                                    End If
                                                
                                                    Set nodx5 = nodx5.Next
                                                Wend
                                                
                                                
                                            Else
                                                If m_oActsN4 Is Nothing Then
                                                     m_oActsN4Seleccionados.Add DevolverId(nodx4.Parent.Parent.Parent), DevolverId(nodx4.Parent.Parent), DevolverId(nodx4.Parent), DevolverId(nodx4), "", ""
                                                ElseIf m_oActsN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                                     m_oActsN4Seleccionados.Add DevolverId(nodx4.Parent.Parent.Parent), DevolverId(nodx4.Parent.Parent), DevolverId(nodx4.Parent), DevolverId(nodx4), "", ""
                                                End If
                                            End If
                                            
                                            Set nodx4 = nodx4.Next
                                        Wend
                                    Else
                                         ' Si esta checked GMN3
                                         If m_oActsN3 Is Nothing Then
                                            'No estaba asignado antes.
                                             m_oActsN3Seleccionados.Add DevolverId(nodx3.Parent.Parent), DevolverId(nodx3.Parent), DevolverId(nodx3), "", ""
                                         ElseIf m_oActsN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                             'No estaba asignado antes.
                                             m_oActsN3Seleccionados.Add DevolverId(nodx3.Parent.Parent), DevolverId(nodx3.Parent), DevolverId(nodx3), "", ""
                                         End If
                                    End If
                                    
                                    Set nodx3 = nodx3.Next
                                Wend
                            Else
                                If m_oActsN2 Is Nothing Then
                                    'No estaba asignado antes.
                                    m_oActsN2Seleccionados.Add DevolverId(nodx2.Parent), DevolverId(nodx2), "", ""
                                ' Si esta checked GMN2
                                ElseIf m_oActsN2.Item(scod1 & scod2) Is Nothing Then
                                    'No estaba asignado antes.
                                    m_oActsN2Seleccionados.Add DevolverId(nodx2.Parent), DevolverId(nodx2), "", ""
                                End If
                            End If
                            
                            Set nodx2 = nodx2.Next
                        Wend
                        
                    Else
                    ' Si esta checked GMN1
                       If m_oActsN1.Item(scod1) Is Nothing Then
                            'No estaba asignado antes.
                            m_oActsN1Seleccionados.Add DevolverId(nodx1), "", ""
                        End If
                    End If
                    Set nodx1 = nodx1.Next
                Wend
                Set g_oCiaSeleccionada.oActividadesNivel1 = Nothing
                Set g_oCiaSeleccionada.oActividadesNivel2 = Nothing
                Set g_oCiaSeleccionada.oActividadesNivel3 = Nothing
                Set g_oCiaSeleccionada.oActividadesNivel4 = Nothing
                Set g_oCiaSeleccionada.oActividadesNivel5 = Nothing
                Set g_oCiaSeleccionada.oActividadesNivel1 = m_oActsN1Seleccionados
                Set g_oCiaSeleccionada.oActividadesNivel2 = m_oActsN2Seleccionados
                Set g_oCiaSeleccionada.oActividadesNivel3 = m_oActsN3Seleccionados
                Set g_oCiaSeleccionada.oActividadesNivel4 = m_oActsN4Seleccionados
                Set g_oCiaSeleccionada.oActividadesNivel5 = m_oActsN5Seleccionados
                Set teserror = g_oCiaSeleccionada.ModificarActividadesCia(m_oActsN1DesSeleccionados, m_oActsN2DesSeleccionados, m_oActsN3DesSeleccionados, m_oActsN4DesSeleccionados, m_oActsN5DesSeleccionados)
        
                If teserror.NumError <> TESnoerror Then
                  basErrores.TratarError teserror
                  Screen.MousePointer = vbNormal
                  AceptarActiv = False
                  Exit Function
                End If
                g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Modif_Activ, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_oCiaSeleccionada.Id
      End If
      
      
    Set m_oActsN1Seleccionados = Nothing
    Set m_oActsN2Seleccionados = Nothing
    Set m_oActsN3Seleccionados = Nothing
    Set m_oActsN4Seleccionados = Nothing
    Set m_oActsN5Seleccionados = Nothing
    Set m_oActsN1DesSeleccionados = Nothing
    Set m_oActsN2DesSeleccionados = Nothing
    Set m_oActsN3DesSeleccionados = Nothing
    Set m_oActsN4DesSeleccionados = Nothing
    Set m_oActsN5DesSeleccionados = Nothing
    
    AceptarActiv = True
    
End Function

Private Sub cmdAceptarCat_Click()
    Dim oTESError As CTESError

    Screen.MousePointer = vbHourglass
    
    m_bAceptarCategorias = True
    If sdbgCategorias.DataChanged Then sdbgCategorias.Update
    If m_bAceptarCategorias Then
        If Not ObtenerCategoriasDelGrid Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        'Guardar cambios en bd : tablas temporales y stored
        Set oTESError = g_oCiaSeleccionada.ModificarDatosCategoriasProve
        If oTESError.NumError <> TESnoerror Then
            basErrores.TratarError oTESError
            Set g_oCiaSeleccionada.Categoriaselim = Nothing
            Set g_oCiaSeleccionada.Categoriaselim = g_oRaiz.Generar_CCategorias
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        Set g_oCiaSeleccionada.Categoriaselim = Nothing
        Set g_oCiaSeleccionada.Categoriaselim = g_oRaiz.Generar_CCategorias
        Set m_oCategoriasCod = Nothing
        Set m_oCategoriasDen = Nothing
        
        sdbgCategorias.Update
                
        cmdRestaurarCat_Click
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdAnyaEsp_Click()
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim oError As CTESError
    Dim ador As ador.Recordset
    Dim oCias As CCias
    
    On Error GoTo Cancelar:

    If g_oCiaSeleccionada Is Nothing Then Exit Sub

    cmmdEsp.DialogTitle = sIdiSelFichero '"Seleccione el fichero para adjuntarlo a la especificaci�n"
    cmmdEsp.Filter = sIdiTodosArch & "|*.*" '"Todos los archivos|*.*"
    cmmdEsp.FileName = ""
    cmmdEsp.ShowOpen

    sFileName = cmmdEsp.FileName
    sFileTitle = cmmdEsp.FileTitle
    If sFileName = "" Then
        Exit Sub
    End If

    ' Ahora obtenemos el comentario para la especificacion
    frmCiaComFich.lblFich = sFileTitle
    frmCiaComFich.g_sOrigen = "frmCompanias"
    g_bCancelarEsp = False
    frmCiaComFich.Show 1

    If Not g_bCancelarEsp Then
        Set oEsp = g_oRaiz.generar_CEspecificacion
        Set oEsp.Cia = g_oCiaSeleccionada
        oEsp.Nombre = sFileTitle
        oEsp.Comentario = g_sComentario

        Set oCias = g_oRaiz.Generar_CCias
        Set ador = oCias.DevolverCiasCompradoras()

        Set oError = oEsp.guardarEspecificacionCia(ador.Fields("ID").Value, oEsp.Cia.Id, sFileName)
                
        ador.Close
        Set ador = Nothing
        If oError.NumError = TESnoerror Then
            g_oCiaSeleccionada.Especificaciones.Add oEsp.Nombre, oEsp.Fecha, oEsp.Id, g_oCiaSeleccionada, oEsp.Comentario, , oEsp.DataSize
            lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
    
            lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    
            ActualizarEspacioLibre
            g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Archivo_Anyadir, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_SPA(2) & ": " & oEsp.Nombre, _
                                            g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_ENG(2) & ": " & oEsp.Nombre, _
                                            g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_FRA(2) & ": " & oEsp.Nombre, _
                                            g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_GER(2) & ": " & oEsp.Nombre, _
                                            g_oCiaSeleccionada.Id
        End If
    End If

fin:
    Unload frmProgreso
    Set oEsp = Nothing
    Exit Sub

Cancelar:
    On Error Resume Next
    If Err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oCias = Nothing
        Set oEsp = Nothing
    End If
    Resume fin
    Resume 0
End Sub


Private Sub cmdA�adir_Click()

    m_bAnyadirCia = True
    sdbcCiaCod.Text = ""
    LimpiarDatosCia
    sdbcIdiCod.Text = m_sDenIdiDef
    sdbcIdiCod_Validate False
    
    sdbcMonCod.Text = m_sDenMonDef
    sdbcMonCod_Validate False
    
    sdbcPaiCod.Text = m_sDenPaiDef
    sdbcPaiCod_Validate False
    
    txtCod.Locked = False
    
    ModoEdicionGen
    
    Set g_oCiaSeleccionada = Nothing
    Set g_oCiaSeleccionada = g_oRaiz.Generar_CCia
    
    SSTab1.TabVisible(1) = False
    SSTab1.TabVisible(2) = False
    SSTab1.TabVisible(3) = False
    SSTab1.TabVisible(5) = False
    txtCod.SetFocus
 
End Sub

Private Sub cmdA�adirUsu_Click()
    Set m_oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
    m_oUsuarioSeleccionado.Cia = g_oCiaSeleccionada.Id
    frmDetalleUsuario.sPais = sdbcPaiCod.Text
    Set frmDetalleUsuario.oUsuario = m_oUsuarioSeleccionado
    
    frmDetalleUsuario.bModificar = True
    frmDetalleUsuario.m_bBloquearComboCat = False
    frmDetalleUsuario.Show 1
    
    CargarUsuariosCia
    Set m_oUsuarioSeleccionado = Nothing
End Sub

Private Sub cmdBuscar_Click()
    frmCiaBuscar.g_sOrigen = "frmCompanias"
    frmCiaBuscar.WindowState = vbNormal
    frmCiaBuscar.Show 1
    
    'Actualizo el bot�n del ciaselector
    CiaSelector1.Seleccion = PSSeleccion.PSTodos
End Sub


Private Sub cmdCancelarCat_Click()
    cmdRestaurarCat_Click
    Set g_oCiaSeleccionada.Categoriaselim = Nothing
    Set g_oCiaSeleccionada.Categoriaselim = g_oRaiz.Generar_CCategorias
    
    Set m_oCategoriasCod = Nothing
    Set m_oCategoriasDen = Nothing
    
    Arrange
End Sub

Private Sub cmdCodigo_Click()
    Dim oTESError As New CTESError
    
    If g_oCiaSeleccionada Is Nothing Then
        Exit Sub
        
    End If
    oTESError.NumError = 0
    
    frmMODCOD.Left = frmCompanias.Left + 500
    frmMODCOD.Top = frmCompanias.Top + 1000
    frmMODCOD.Caption = sIdiCiaCod
    
    frmMODCOD.txtCodNue.MaxLength = LONGCOD
    frmMODCOD.txtCodAct.Text = g_oCiaSeleccionada.Cod
    frmMODCOD.g_sfrmOrigen = "frmCompanias"
    frmMODCOD.Show 1
    
    If g_sCodigoNuevo = g_oCiaSeleccionada.Cod Then
        Exit Sub
    End If
    
    Set oTESError = g_oCiaSeleccionada.CambiarCodigo(g_sCodigoNuevo)
    If oTESError.NumError <> 0 Then
    
        TratarError oTESError
    Else
        m_bRespetarCombo = True
        Me.sdbcCiaCod.Text = g_sCodigoNuevo
        sdbcCiaCod_Validate False
        m_bRespetarCombo = False
        g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Modif_Cod, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & ", " & g_sLitRegAccion_SPA(10) & ": " & g_sCodigoNuevo, _
                                        g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & ", " & g_sLitRegAccion_ENG(10) & ": " & g_sCodigoNuevo, _
                                        g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & ", " & g_sLitRegAccion_FRA(10) & ": " & g_sCodigoNuevo, _
                                        g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & ", " & g_sLitRegAccion_GER(10) & ": " & g_sCodigoNuevo, _
                                        g_oCiaSeleccionada.Id
    End If
    
    Set oTESError = Nothing
    

End Sub

Private Sub cmdConsulta0_Click()

        Dim bCorrecto As Boolean
        bCorrecto = AceptarActiv
        If Not bCorrecto Then
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            basMensajes.ActualizacionCorrecta
        End If
        
        ModoConsultaAct
End Sub
Private Sub cmdConsulta1_Click()
    
        Dim oTESError As CTESError
        
        If Me.txtObs <> IIf(IsNull(g_oCiaSeleccionada.Coment), "", g_oCiaSeleccionada.Coment) Then
            
            If Not Validacion Then
                Exit Sub
            Else
                Set oTESError = g_oCiaSeleccionada.ModificarDatosCia(, g_oRaiz.SPTS)
                If oTESError.NumError <> TESnoerror Then
                    basErrores.TratarError oTESError
                    Screen.MousePointer = vbNormal
                    Exit Sub
                Else
                    g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Modif_Obs, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                    g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                    g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                    g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                    g_oCiaSeleccionada.Id
                    basMensajes.ActualizacionCorrecta
                End If
            End If
        End If
        

        ModoConsultaObs

End Sub

Private Sub cmdEditar_Click(Index As Integer)
Select Case Index
    Case 0
        ModoEdicionAct
    Case 1
        ModoEdicionObs
End Select
End Sub

Private Sub cmdEliEsp_Click()
Dim Item As MSComctlLib.ListItem
Dim oEsp As CEspecificacion
Dim oError As CTESError
Dim iRespuesta As Integer
Dim sNombre As String
On Error GoTo Cancelar:

    If g_oCiaSeleccionada Is Nothing Then Exit Sub
    
    Set Item = lstvwEsp.SelectedItem
    
    If Item Is Nothing Then
        basMensajes.SeleccioneFichero
    
    Else
        
        iRespuesta = basMensajes.PreguntaEliminar(" " & sIdiElArch & " " & lstvwEsp.SelectedItem.Text)

        If iRespuesta = vbNo Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        Set oEsp = g_oCiaSeleccionada.Especificaciones.Item(CStr(lstvwEsp.SelectedItem.Tag))
        sNombre = oEsp.Nombre
        Set oError = oEsp.EliminarEspecificacion
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
        Else
            g_oCiaSeleccionada.Especificaciones.Remove (CStr(lstvwEsp.SelectedItem.Tag))
            lstvwEsp.ListItems.Remove (CStr(lstvwEsp.SelectedItem.Key))
            ActualizarEspacioLibre
            g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Archivo_Eliminar, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_SPA(2) & ": " & sNombre, _
                                                    g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_ENG(2) & ": " & sNombre, _
                                                    g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_FRA(2) & ": " & sNombre, _
                                                    g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_GER(2) & ": " & sNombre, _
                                                    g_oCiaSeleccionada.Id
        End If
        
        Set oEsp = Nothing
    End If
    
    Screen.MousePointer = vbNormal
    Exit Sub
    
Cancelar:
    
    Set oEsp = Nothing

End Sub

Private Sub cmdEliminar_Click()
    Dim teserror As CTESError
    Dim iRespuesta As Integer
    Dim scod As String
    Dim lId As Long
    iRespuesta = basMensajes.PreguntaEliminarCia
    If iRespuesta = vbYes Then
        If g_oCiaSeleccionada.EstadoCompradora = 3 Then
            iRespuesta = basMensajes.PreguntaEliminarCiaComp
            If iRespuesta = vbNo Then Exit Sub
        End If
        If g_oCiaSeleccionada.Premium = 2 Then
            iRespuesta = basMensajes.PreguntaEliminarPremium
            If iRespuesta = vbNo Then Exit Sub
        End If
        scod = g_oCiaSeleccionada.Cod
        lId = g_oCiaSeleccionada.Id
        Screen.MousePointer = vbHourglass
        
        Set teserror = g_oCiaSeleccionada.EliminarCia(g_oRaiz.SPTS)
        If teserror.NumError <> 0 Then
            basErrores.TratarError teserror
        Else
            sdbcCiaCod = ""
            basMensajes.CiaEliminada
            'Registramos la acci�n
            g_oGestorAcciones.RegistrarAccion g_sCodADM, Cia_Eliminar, g_sLitRegAccion_SPA(8) & ": " & scod, _
                                                    g_sLitRegAccion_ENG(8) & ": " & scod, _
                                                    g_sLitRegAccion_FRA(8) & ": " & scod, _
                                                    g_sLitRegAccion_GER(8) & ": " & scod, _
                                                    lId
        End If
        Screen.MousePointer = vbNormal
    End If

End Sub

Private Sub cmdEliminarUsu_Click()
Dim iRespuesta As Integer

    If sdbgUsu.Rows > 0 Then
        If sdbgUsu.Columns(1).Text = "" Then Exit Sub
        sdbgUsu.SelBookmarks.Add sdbgUsu.Bookmark
        iRespuesta = basMensajes.PreguntaEliminar(sIdiElUsu & " " & sdbgUsu.Columns(1).Value)
        If iRespuesta = vbYes Then
            'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
            If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                If g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns(0).Value).EstaEnProyectosDeFSGA(g_oCiaSeleccionada.CodGS) Then
                    basMensajes.UusarioEnProyectosFSGA
                    Exit Sub
                End If
            End If
            Screen.MousePointer = vbHourglass
            EliminarUsuarioSeleccionado
            Screen.MousePointer = vbNormal
            basMensajes.UsuarioEliminado
            If sdbgUsu.Rows > 0 Then
                sdbgUsu.SetFocus
                sdbgUsu.Bookmark = sdbgUsu.RowBookmark(sdbgUsu.Row)
            Else
                cmdA�adirUsu.SetFocus
            End If

            If g_oCiaSeleccionada.Usuarios.Count = 0 Then
                cmdModificarUsu.Enabled = False
                cmdEliminarUsu.Enabled = False
            End If
        End If
    End If

End Sub

Private Sub cmdEnlazar_Click()
        
    DoEvents
    
    If g_oCiaSeleccionada Is Nothing Then Exit Sub
    
    If g_oCiaSeleccionada.EstadoProveedora <> 3 Then
        Exit Sub
    End If
    
    frmPROVEBuscar.sOrigen = "frmCompanias"
    frmPROVEBuscar.IdCia = basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora
    
    If txtCodProveGS.Text <> "" Then
        frmPROVEBuscar.sdbcProveCod.Value = txtCodProveGS.Text
        frmPROVEBuscar.sdbcProveCod_Validate False
        DoEvents
        frmPROVEBuscar.stabGeneral.Tab = 1
    Else
        frmPROVEBuscar.sdbcProveCod.Value = sdbcCiaCod.Text
        frmPROVEBuscar.sdbcProveCod_Validate False
        DoEvents
        frmPROVEBuscar.stabGeneral.Tab = 1
    End If
    
    frmPROVEBuscar.Hide
    frmPROVEBuscar.Show 1
    
End Sub

Private Sub cmdEnlazarProveedor_Click()

On Error Resume Next
If g_frmEnProve Is Nothing Then
    Set g_frmEnProve = New frmEnlazarProveedor
End If
g_frmEnProve.sOrigen = "frmCompanias"
Set g_frmEnProve.oCiaSeleccionada = g_oCiaSeleccionada
g_frmEnProve.CiaSeleccionada

g_frmEnProve.Show
g_frmEnProve.SetFocus

End Sub


Private Sub cmdGuardarSize_Click()
Dim oTESError As CTESError
Dim vSizeActual As Variant

    If Not IsNumeric(txtTamanyoMax.Text) Then
        basMensajes.ImposibleModificarTamanyo
        txtTamanyoMax.Text = Format(g_oCiaSeleccionada.MaxSizeAdjun / 1024, "#,##0")
        txtTamanyoMax.SetFocus
        Exit Sub
    ElseIf txtTamanyoMax.Text < 0 Then
        basMensajes.ImposibleModificarTamanyo
        txtTamanyoMax.Text = Format(g_oCiaSeleccionada.MaxSizeAdjun / 1024, "#,##0")
        txtTamanyoMax.SetFocus
        Exit Sub
    End If
    
    If CDbl(Trim(txtTamanyoMax.Text)) * 1024 < TamanyoOcupado Then
        basMensajes.ImposibleModificarTamanyo (Format(TamanyoOcupado / 1024, "#,##0") & " KB")
        txtTamanyoMax.Text = Format(g_oCiaSeleccionada.MaxSizeAdjun / 1024, "#,##0")
        txtTamanyoMax.SetFocus
        Exit Sub
    End If
    
    
    Screen.MousePointer = vbHourglass
    
    vSizeActual = g_oCiaSeleccionada.MaxSizeAdjun
    g_oCiaSeleccionada.MaxSizeAdjun = CDbl(Trim(txtTamanyoMax.Text)) * 1024
    
    Set oTESError = g_oCiaSeleccionada.ModificarTamanyoMaxAdjun
    
    If oTESError.NumError <> TESnoerror Then
        g_oCiaSeleccionada.MaxSizeAdjun = vSizeActual
        basErrores.TratarError oTESError
    Else
        cmdGuardarSize.Enabled = False
        ActualizarEspacioLibre
    End If
            
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdListado_Click()
    frmLstCIAS.g_sOrigen = "LstCIAS"
    If Not g_oCiaSeleccionada Is Nothing Then
        frmLstCIAS.txtCod.Text = g_oCiaSeleccionada.Cod
    End If
    frmLstCIAS.WindowState = vbNormal
    frmLstCIAS.SetFocus
    
End Sub

Private Sub cmdModifEsp_Click()
    Dim oEsp As CEspecificacion
    
    If g_oCiaSeleccionada Is Nothing Then Exit Sub
    
    If lstvwEsp.SelectedItem Is Nothing Then
        basMensajes.SeleccioneFichero
    Else

        Set oEsp = g_oCiaSeleccionada.Especificaciones.Item(CStr(lstvwEsp.SelectedItem.Tag))
        frmCiaEspMod.g_sOrigen = "frmCompanias"
        Set frmCiaEspMod.g_oEsp = oEsp
        frmCiaEspMod.Show 1
        
        Set oEsp = Nothing
                
    End If

End Sub

Private Sub cmdModificar_Click()
    cmdAceptar.Enabled = True
    m_bAnyadirCia = False
    m_bModifCia = True
    ModoEdicionGen
    BloquearDatosProve HayQueBloquearModif
End Sub

Private Sub cmdModificarCat_Click()

    cmdModificarCat.Visible = False
    cmdRestaurarCat.Visible = False
    cmdAceptarCat.Visible = True
    cmdCancelarCat.Visible = True

    'Desbloqueamos el grid de categorias.
    With sdbgCategorias
        .AllowAddNew = True
        .AllowDelete = True
        .AllowUpdate = True
    End With
    
    'Mostramos la columna de baja l�gica.
    sdbgCategorias.Columns("BAJALOG").Visible = True

    'Obtenemos las categor�as ordenadas por bajalog-c�digo.
    Set m_oCategoriasCod = g_oRaiz.Generar_CCategorias
    m_oCategoriasCod.CargarTodasLasCategorias 1
    
    'Obtenemos las categor�as ordenadas por bajalog-denominaci�n del idioma del usuario.
    Set m_oCategoriasDen = g_oRaiz.Generar_CCategorias
    m_oCategoriasDen.CargarTodasLasCategorias 2
    
    Arrange

End Sub

Private Sub cmdModificarUsu_Click()
Dim ador As ador.Recordset
Dim ador2 As ador.Recordset

    If Not sdbgUsu.Columns("ID").Value = "" Then
        Set ador = g_oCiaSeleccionada.Usuarios.DevolverDatosUsuario(g_oCiaSeleccionada.Id, sdbgUsu.Columns("ID").Value)
    
        If Not ador Is Nothing Then
                
            Set m_oUsuarioSeleccionado = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value)
            With m_oUsuarioSeleccionado
            
                .Cargo = NullToStr(ador("CAR").Value)
                .Departamento = NullToStr(ador("DEP").Value)
                .Email = NullToStr(ador("EMAIL").Value)
                .Fax = NullToStr(ador("FAX").Value)
                .Tfno1 = NullToStr(ador("TFNO").Value)
                .Tfno2 = NullToStr(ador("TFNO2").Value)
                .TfnoMovil = NullToStr(ador("TFNO_MOVIL").Value)
                .UsuarioBloqueado = ador("BLOQ").Value
                .PWD_HASH = NullToStr(ador("PWD_HASH").Value)
                .NIF = NullToStr(ador("NIF").Value)
                If Not IsNull(ador("IDICOD").Value) Then
                    .idioma = ador("IDIID").Value
                    .Idiomaden = ador("IDIDEN").Value
                End If
                If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                    Set ador2 = .DevolverCategoria(g_oCiaSeleccionada.CodGS)
                    If Not ador2.EOF Then
                        If IsNull(ador2.Fields("ID_CAT_LAB").Value) Then
                            .IDCategoria = 0
                        Else
                            .IDCategoria = ador2.Fields("ID_CAT_LAB").Value
                        End If
                        If ador2.Fields("DACT").Value = 0 Then
                            .AccesoFSGA = True
                        Else
                            .AccesoFSGA = False
                        End If
                    Else
                        .AccesoFSGA = False
                    End If
                    ador2.Close
                    Set ador2 = Nothing
                Else
                    .AccesoFSGA = False
                    .IDCategoria = 0
                End If
            End With
        
            ador.Close
        Else
            basMensajes.DatoEliminado sIdiUsuario
        End If
    
        Set ador = Nothing
'        If Not m_bModifCia Then
'            frmDetalleUsuario.bModificar = False
'        Else
            frmDetalleUsuario.bModificar = True
'        End If
        Set frmDetalleUsuario.oUsuario = m_oUsuarioSeleccionado
        frmDetalleUsuario.sPais = sdbcPaiCod.Text
        If m_oUsuarioSeleccionado.EstadoProv = 3 Then
            frmDetalleUsuario.m_bBloquearComboCat = False
        Else
            frmDetalleUsuario.m_bBloquearComboCat = True
        End If
        
        frmDetalleUsuario.Show 1
        Set m_oUsuarioSeleccionado = Nothing
        If m_bModifCia Then
            CargarUsuariosCia
        End If
    End If

End Sub


Private Sub ModifActiv()

    If m_bActividadModif = True Then Exit Sub
    
    DoEvents
    Screen.MousePointer = vbHourglass
    
    LockWindowUpdate Me.hWnd
    
    tvwEstrMatConsul.Visible = False
    tvwEstrMat.Visible = True
    
    GenerarEstructuraActividades False
    GenerarEstructuraActividadesCia
    
    LockWindowUpdate 0&
    Screen.MousePointer = vbDefault
    
End Sub
Private Sub RestaurarActiv()
    
    DoEvents
    Screen.MousePointer = vbHourglass

    LockWindowUpdate Me.hWnd
    
    tvwEstrMat.Visible = False
    tvwEstrMatConsul.Visible = False
    
    GenerarEstructuraActividadesConsul False
    GenerarEstructuraActividadesCiaConsul
    
    Screen.MousePointer = vbDefault
    
    LockWindowUpdate 0&
    
    tvwEstrMatConsul.Visible = True

End Sub


Private Sub RestaurarAut()
    
    Select Case g_oCiaSeleccionada.EstadoProveedora
        Case 1
            opFPSol.Value = True
            opFPSol.Enabled = True
        Case 3
            opFPAut.Value = True
            opFPSol.Enabled = False
        Case 2
            opFPDes.Value = True
            opFPSol.Enabled = False
        Case Else
            opFPSol.Value = False
            opFPAut.Value = False
            opFPDes.Value = False
            opFPSol.Enabled = True
    End Select
        
    If Not IsNull(g_oCiaSeleccionada.FecAutorizado) Then
        lblFecAut.Caption = DateValue(g_oCiaSeleccionada.FecAutorizado)
    End If
    If Not IsNull(g_oCiaSeleccionada.FecDesautorizado) Then
        lblFecDesAut.Caption = DateValue(g_oCiaSeleccionada.FecDesautorizado)
    End If
    If Not IsNull(g_oCiaSeleccionada.FecSolicitado) Then
        lblFecSolicit.Caption = DateValue(g_oCiaSeleccionada.FecSolicitado)
    End If
    
    If g_udtParametrosGenerales.g_bPremium Then
        If g_oCiaSeleccionada.Premium = 0 Or IsNull(g_oCiaSeleccionada.Premium) Then
            chkPremium.Value = vbUnchecked
            optPRAut.Value = False
            optPRAut.Enabled = False
            optPRDes.Enabled = False
            optPRAut.Value = False
        Else
            chkPremium.Value = vbChecked
            optPRAut.Enabled = True
            optPRDes.Enabled = True
            If g_oCiaSeleccionada.Premium = 2 Then
                chkPremium.Enabled = False
                
                optPRAut.Value = True
            ElseIf g_oCiaSeleccionada.Premium = 3 Then
                chkPremium.Enabled = False
                optPRDes.Value = True
            Else
                optPRAut.Value = False
                optPRDes.Value = False
                
            End If
        
        End If
        
        If g_oCiaSeleccionada.EstadoProveedora = 3 And g_oCiaSeleccionada.Premium = 2 Then
            cmdEnlazarProveedor.Enabled = True
        Else
            cmdEnlazarProveedor.Enabled = False
        End If
    End If
    

End Sub



Private Sub cmdRestaurarCat_Click()

    cmdAceptarCat.Visible = False
    cmdCancelarCat.Visible = False
    cmdModificarCat.Visible = True
    cmdRestaurarCat.Visible = True
    
    CargarCategoriasProve
    
    'Bloqueamos el grid de categorias de proveedor
    With sdbgCategorias
        .AllowAddNew = False
        .AllowDelete = False
        .AllowUpdate = False
    End With
    
    'Ocultamos la columna de baja l�gica.
    sdbgCategorias.Columns("BAJALOG").Visible = False
End Sub

Private Sub cmdSalvarEsp_Click()
    Dim Item As MSComctlLib.ListItem
    Dim oEsp As CEspecificacion
    Dim sFileName As String
    Dim sFileTitle As String
    Dim oError As CTESError
    Dim ador As ador.Recordset
    Dim oCias As CCias

    On Error GoTo Cancelar:

    If g_oCiaSeleccionada Is Nothing Then Exit Sub

    Set Item = lstvwEsp.SelectedItem

    If Item Is Nothing Then
        basMensajes.SeleccioneFichero

    Else

        cmmdEsp.DialogTitle = sIdiGuardarEsp

        cmmdEsp.Filter = sIdiTipoOriginal & "|*.*"
        cmmdEsp.FileName = Item.Text
        cmmdEsp.CancelError = True
        cmmdEsp.ShowSave
        sFileName = cmmdEsp.FileName
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            basMensajes.NoValido sIdiArch
            Exit Sub
        End If

        Set oEsp = g_oCiaSeleccionada.Especificaciones.Item(CStr(lstvwEsp.SelectedItem.Tag))

        Set oError = oEsp.ComenzarLecturaData
        If oError.NumError <> 0 Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError oError
            Set oEsp = Nothing
            Exit Sub
        End If

        Set oCias = g_oRaiz.Generar_CCias
        Set ador = oCias.DevolverCiasCompradoras()
        
        oEsp.LeerAdjunto ador.Fields("ID").Value, oEsp.Cia.Id, oEsp.DataSize, sFileName
        
        ador.Close
        Set ador = Nothing
        g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Archivo_GuardarEnDisco, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & " g_sLitRegAccion_SPA (2): " & oEsp.Nombre, _
                                                g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_ENG(2) & ": " & oEsp.Nombre, _
                                                g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_FRA(2) & ": " & oEsp.Nombre, _
                                                g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_GER(2) & ": " & oEsp.Nombre, _
                                                g_oCiaSeleccionada.Id
    End If

Cancelar:

    If Err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
    End If

End Sub


Private Sub Form_Load()
    
    m_bLoad = True
    'Me.Width = 9225
    Me.Width = 10035
    Me.Height = 8010
    m_bLoad = False
    CargarRecursos
    ConfigurarSeguridad
    
    m_bModifCia = False
    cmdAceptar.Enabled = True
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdRestaurar0.Enabled = True
    cmdListado.Enabled = False
    
    cmdRestaurar1.Enabled = False
    cmdConsulta0.Enabled = False
    cmdEditar(0).Enabled = False
    
    cmdA�adirUsu.Enabled = False
    cmdModificarUsu.Enabled = False
    cmdEliminarUsu.Enabled = False
    cmdModificar.Enabled = False
    cmdCodigo.Enabled = False
    cmdEliminar.Enabled = False
    cmdRestaurar0.Enabled = False
    cmdRestaurar1.Enabled = False
    cmdRestaurar2.Enabled = False
    
    SSTab1.TabVisible(1) = False
    SSTab1.TabVisible(2) = False
    SSTab1.TabVisible(3) = False
    SSTab1.TabVisible(4) = False
    SSTab1.TabVisible(5) = False
    SSTab1.TabVisible(6) = False
    
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
        cmdEnlazarProveedor.Visible = False
        picProveGS.Visible = True
        SSTab1.TabVisible(4) = False
    Else
        cmdEnlazarProveedor.Visible = True
        picProveGS.Visible = False
        SSTab1.TabVisible(4) = False
    End If
    
    ModoConsulta
    
    Set m_oCias = g_oRaiz.Generar_CCias
    
    CiaSelector1.AbrevParaPendientes = sIdiAbrevPend
    CiaSelector1.AbrevParaTodos = sIdiAbrevTodas
    
    lstvwEsp.ColumnHeaders(1).Text = sIdiFichero
    lstvwEsp.ColumnHeaders(2).Text = sIdiComent
    lstvwEsp.ColumnHeaders(3).Text = sIdiFecha
    
    Dim ador As ador.Recordset


    Set ador = g_oRaiz.DevolverPais(g_udtParametrosGenerales.g_lPais)
    If Not ador.EOF Then
        m_sDenPaiDef = ador.Fields("DEN").Value
        m_lIdPaiDef = ador.Fields("ID").Value
        m_lIdMonPai = ador.Fields("MON").Value
    End If
    ador.Close
    Set ador = Nothing
    
    'Portales
    Set ador = g_oRaiz.DevolverPortales
    If Not ador Is Nothing Then
        m_bVariosPortales = (ador.RecordCount > 1)
        While Not ador.EOF
            sdbcPortal.AddItem NullToStr(ador("CODIGO").Value)
            ador.MoveNext
        Wend
    End If
    ador.Close
    Set ador = Nothing
    
    If Not m_bVariosPortales Then
        Label20.Visible = False
        sdbcPortal.Visible = False
    End If
    
    If IsNull(m_lIdMonPai) Then
        m_lIdMonPai = g_udtParametrosGenerales.g_iMonedaCentral
    End If
    
    Set ador = g_oRaiz.DevolverMoneda(m_lIdMonPai)
    If Not ador.EOF Then
        m_sDenMonDef = ador.Fields("DEN").Value
        m_sCodMonDef = ador.Fields("COD").Value
    End If
    ador.Close
        
    Set ador = g_oRaiz.DevolverIdiomasDelPortalDesde(1, g_udtParametrosGenerales.g_sIdioma, , True, False)
    If Not ador.EOF Then
        m_sDenIdiDef = ador.Fields("DEN").Value
    End If
    ador.Close
    Set ador = Nothing
    
    Set m_oIdiomas = g_oGestorParametros.DevolverIdiomasPrimeroIdiUsu
    If g_udtParametrosGenerales.g_bAccesoFSGA Then
        ConfigurarGridCategorias
        ConfigurarComboCategorias
        ConfigurarComboCategoriasUsu
    End If
    'Inicializar el array de ficheros
    ReDim m_sArFileNames(0)
    LimpiarDatosCia
End Sub

Private Sub ModoConsulta()
    
    fraCias.Enabled = True
    picDatos.Enabled = False
    picNavigate(0).Visible = True
    cmdEditar(0).Visible = True
    cmdConsulta0.Visible = False
    cmdEditar(1).Visible = True
    cmdConsulta1.Visible = False
  
    SSTab1.TabEnabled(0) = True
    SSTab1.TabEnabled(1) = True
    SSTab1.TabEnabled(2) = True
    SSTab1.TabEnabled(3) = True
    SSTab1.TabEnabled(4) = True
    SSTab1.TabEnabled(5) = True
    
    picEdit.Enabled = False
    picEdit.Visible = False
    txtObs.Enabled = False

End Sub

Private Sub ModoConsultaGen()
    
    SSTab1.TabEnabled(1) = True
    SSTab1.TabEnabled(2) = True
    SSTab1.TabEnabled(3) = True
    SSTab1.TabEnabled(4) = True
    SSTab1.TabEnabled(5) = True
    
    picDatos.Enabled = False
    fraCias.Enabled = True
    picNavigate(0).Visible = True
    picEdit.Enabled = False
    picEdit.Visible = False
    
End Sub

Private Sub ModoEdicionAct()
    
    SSTab1.TabEnabled(0) = False
    SSTab1.TabEnabled(2) = False
    SSTab1.TabEnabled(3) = False
    SSTab1.TabEnabled(4) = False
    SSTab1.TabEnabled(5) = False
    
    cmdEditar(0).Visible = False
    cmdConsulta0.Visible = True
    
    ModifActiv

End Sub

Private Sub ModoConsultaAct()
        
    m_bActividadModif = False
    SSTab1.TabEnabled(0) = True
    SSTab1.TabEnabled(2) = True
    SSTab1.TabEnabled(3) = True
    SSTab1.TabEnabled(4) = True
    SSTab1.TabEnabled(5) = True
    
    cmdEditar(0).Visible = True
    cmdConsulta0.Visible = False
    RestaurarActiv
        
End Sub

Private Sub ModoEdicionObs()
        
    m_bAnyadirCia = False
    m_bModifCia = True
    SSTab1.TabEnabled(0) = False
    SSTab1.TabEnabled(1) = False
    SSTab1.TabEnabled(3) = False
    SSTab1.TabEnabled(4) = False
    SSTab1.TabEnabled(5) = False
    
    cmdEditar(1).Visible = False
    cmdConsulta1.Visible = True
    
    txtObs.Enabled = True

End Sub

Private Sub ModoConsultaObs()
        
    SSTab1.TabEnabled(0) = True
    SSTab1.TabEnabled(1) = True
    SSTab1.TabEnabled(3) = True
    SSTab1.TabEnabled(4) = True
    SSTab1.TabEnabled(5) = True
    
    cmdEditar(1).Visible = True
    cmdConsulta1.Visible = False
    txtObs.Enabled = False
    m_bModifCia = False
        
End Sub
Private Sub ModoEdicionGen()
Dim bm As Variant
    SSTab1.TabEnabled(1) = False
    SSTab1.TabEnabled(2) = False
    SSTab1.TabEnabled(3) = False
    SSTab1.TabEnabled(4) = False
    SSTab1.TabEnabled(5) = False
    
    picDatos.Enabled = True
    cmdEnlazarProveedor.Enabled = False
    If sdbcPortal.Text = "" Then
        sdbcPortal.MoveFirst
        bm = sdbcPortal.GetBookmark(0)
        sdbcPortal.Bookmark = bm
        sdbcPortal.Text = sdbcPortal.Columns(0).CellText(bm)
    End If
    fraCias.Enabled = False
    picNavigate(0).Visible = False
    picEdit.Enabled = True
    picEdit.Visible = True
End Sub

''' <summary>Bloquea la edici�n de la raz�n Social, NIF y Moneda de la Cia.</summary>
''' <param name="bBloquear">Indica si hay que bloquear/desbloquear los datos indicados</param>
''' <remarks>Llamada desde=cmdCancelar_Click, cmdAceptar_Click y cmdModificar_Click; Tiempo m�ximo=0seg.</remarks>

Private Sub BloquearDatosProve(ByVal bBloquear As Boolean)
    
    txtDen.Locked = bBloquear
    txtNif.Locked = bBloquear
    sdbcMonCod.Enabled = Not bBloquear
    
    If bBloquear Then
        txtDen.BackColor = &HE1FFFF
        txtNif.BackColor = &HE1FFFF
        sdbcMonCod.BackColor = &HE1FFFF
    Else
        txtDen.BackColor = &HFFFFFF
        txtNif.BackColor = &HFFFFFF
        sdbcMonCod.BackColor = &HFFFFFF
    End If
End Sub

Private Sub Arrange()
Static iAWidth As Integer
Static iAHeight As Integer
Dim iH As Integer
Dim iV As Integer
Dim iWidth As Integer
Dim iHeight As Integer

iWidth = 10035
iHeight = 8010

iH = Me.Height - iHeight
iV = Me.Width - iWidth

Dim bConCategorias As Boolean
Dim oIdi As CIdioma

Dim vSSTab1W As Integer
Dim vSSTab1H As Integer
Dim vfraCiasW As Integer
Dim vpicDatosH As Integer
Dim vpicDatosW As Integer
Dim vpicUsuH As Integer
Dim vpicUsuW As Integer
Dim vsdbgUsuH As Integer
Dim vsdbgUsuW As Integer
Dim vsdbgCatH As Integer
Dim vsdbgCatW As Integer
Dim vsdbgCiasCompH As Integer
Dim vsdbgCiasCompW As Integer
Dim vtxtObsH As Integer
Dim vtxtObsW As Integer
Dim vtvwEstrMatH As Integer
Dim vtvwEstrMatW As Integer
Dim vtvwEstrMatConsulH As Integer
Dim vtvwEstrMatConsulW As Integer
Dim vpicNavigate0T As Integer
Dim vpicNavigate0W As Integer
Dim vpicNavigate1T As Integer
Dim vpicNavigate1W As Integer
Dim vpicNavigate2T As Integer
Dim vpicNavigate2W As Integer
Dim vpicEditT As Integer
Dim vpicEditW As Integer
Dim vpicEditUsuT As Integer
Dim vpicEditCatT As Integer
Dim vpicEditCatW As Integer
Dim vcmdEditar0L As Integer
Dim vcmdEditar1L As Integer
Dim vcmdConsulta0L As Integer
Dim vcmdConsulta1L As Integer
Dim vpicLeyendaT As Integer
Dim vpicLeyendaL As Integer

    vSSTab1W = 9855
    vSSTab1H = 6795
    vfraCiasW = 9855
    vpicDatosH = 5910
    vpicDatosW = 9765
    vpicUsuH = 6330
    vpicUsuW = 9690
    vsdbgUsuH = 5760
    vsdbgUsuW = 9585
    vsdbgCatH = 5760
    vsdbgCatW = 9585
    vsdbgCiasCompH = 5760
    vsdbgCiasCompW = 9585
    vtxtObsH = 5505
    vtxtObsW = 9450
    vtvwEstrMatH = 5835
    vtvwEstrMatW = 9525
    vtvwEstrMatConsulH = 5835
    vtvwEstrMatConsulW = 9525
    vpicNavigate0T = 6345
    vpicNavigate0W = 8585
    vpicNavigate1T = 6300
    vpicNavigate1W = 9525
    vpicNavigate2T = 6315
    vpicNavigate2W = 9600
    vpicEditT = 6345
    vpicEditW = 8685
    vpicEditUsuT = 5985
    vpicEditCatT = 6345
    vpicEditCatW = 6405
    vcmdEditar0L = 8505
    vcmdEditar1L = 8550
    vcmdConsulta0L = 8505
    vcmdConsulta1L = 8550
    vpicLeyendaT = 5835
    vpicLeyendaL = 5000
    
    On Error Resume Next
    'Tab
    SSTab1.Width = vSSTab1W + iV
    SSTab1.Height = vSSTab1H + iH
    
    fraCias.Width = vfraCiasW + iV
    
    'Picture para los datos generales
    picDatos.Height = vpicDatosH + iH
    picDatos.Width = vpicDatosW + iV
    
    'Picture para los usuarios
    picUsu.Height = vpicUsuH + iH
    picUsu.Width = vpicUsuW + iV
    
    'Grid de usuarios
    sdbgUsu.Height = vsdbgUsuH + iH
    sdbgUsu.Width = vsdbgUsuW + iV
    sdbgCiasComp.Height = vsdbgCiasCompH + iH
    sdbgCiasComp.Width = vsdbgCiasCompW + iV
    
    'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
    If Not g_oCiaSeleccionada Is Nothing Then
        If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
            bConCategorias = True
        Else
            bConCategorias = False
        End If
    Else
        bConCategorias = False
    End If
    If bConCategorias Then
        sdbgUsu.Columns("COD").Width = sdbgUsu.Width * 0.11
        sdbgUsu.Columns("NOM").Width = sdbgUsu.Width * 0.14
        sdbgUsu.Columns("APE").Width = sdbgUsu.Width * 0.17
        sdbgUsu.Columns("FCAUT").Width = sdbgUsu.Width * 0.0945
        sdbgUsu.Columns("FPAUT").Width = sdbgUsu.Width * 0.12
        sdbgUsu.Columns("PPAL").Width = sdbgUsu.Width * 0.07
        sdbgUsu.Columns("CATEGORIA").Width = sdbgUsu.Width * 0.23
        sdbgUsu.Columns("ACCESOFSGA").Width = sdbgUsu.Width * 0.12
        sdbgUsu.Columns("FMEST").Visible = False
    Else
        sdbgUsu.Columns("COD").Width = sdbgUsu.Width * 0.1497
        sdbgUsu.Columns("NOM").Width = sdbgUsu.Width * 0.25
        sdbgUsu.Columns("APE").Width = sdbgUsu.Width * 0.32
        sdbgUsu.Columns("FCAUT").Width = sdbgUsu.Width * 0.0945
        sdbgUsu.Columns("FPAUT").Width = sdbgUsu.Width * 0.12
        sdbgUsu.Columns("PPAL").Width = sdbgUsu.Width * 0.12
        sdbgUsu.Columns("FMEST").Visible = False
    End If

    
    'Grid de categorias
    sdbgCategorias.Height = vsdbgCatH + iH
    sdbgCategorias.Width = vsdbgCatW + iV
    
    If sdbgCategorias.Columns("BAJALOG").Visible Then
        sdbgCategorias.Columns("COD").Width = sdbgCategorias.Width * 0.13
        sdbgCategorias.Columns("COSTEHORA").Width = sdbgCategorias.Width * 0.11
        For Each oIdi In m_oIdiomas
            sdbgCategorias.Columns("DEN_" & oIdi.Cod).Width = sdbgCategorias.Width * 0.2
        Next
        sdbgCategorias.Columns("BAJALOG").Width = sdbgCategorias.Width * 0.11
    Else
        sdbgCategorias.Columns("COD").Width = sdbgCategorias.Width * 0.172
        sdbgCategorias.Columns("COSTEHORA").Width = sdbgCategorias.Width * 0.13
        For Each oIdi In m_oIdiomas
            sdbgCategorias.Columns("DEN_" & oIdi.Cod).Width = sdbgCategorias.Width * 0.22
        Next
    End If
        
    sdbgCiasComp.Columns("COD").Width = (sdbgCiasComp.Width - 570) * 0.196
    sdbgCiasComp.Columns("DEN").Width = (sdbgCiasComp.Width - 570) * 0.4468
    sdbgCiasComp.Columns("SOLICITADA").Width = (sdbgCiasComp.Width - 570) * 0.181
    sdbgCiasComp.Columns("REGISTRADA").Width = (sdbgCiasComp.Width - 570) * 0.1762
    
    'Observaciones
    txtObs.Height = vtxtObsH + iH
    txtObs.Width = vtxtObsW + iV
    
    '�rboles de actividades
    tvwEstrMat.Height = vtvwEstrMatH + iH
    tvwEstrMat.Width = vtvwEstrMatW + iV
    tvwEstrMatConsul.Height = vtvwEstrMatConsulH + iH
    tvwEstrMatConsul.Width = vtvwEstrMatConsulW + iV
    
    'Botones comunes del formulario
    picNavigate(0).Top = vpicNavigate0T + iH
    picNavigate(0).Width = vpicNavigate0W + iV
    picNavigate(1).Top = vpicNavigate1T + iH
    picNavigate(1).Width = vpicNavigate1W + iV
    picNavigate(2).Top = vpicNavigate2T + iH
    picNavigate(2).Width = vpicNavigate2W + iV
    picEdit.Top = vpicEditT + iH
    picEdit.Width = vpicEditW + iV
    
    'Botones para el mantenimiento de los usuarios
    picEditUsu.Top = vpicEditUsuT + iH
    picEditCat.Top = vpicEditCatT + iH
    picEditCat.Width = vpicEditCatW + iV
    
    cmdEditar(0).Left = vcmdEditar0L + iV
    cmdEditar(1).Left = vcmdEditar1L + iV
    cmdConsulta0.Left = vcmdConsulta0L + iV
    cmdConsulta1.Left = vcmdConsulta1L + iV
    cmdAceptarCat.Left = 3720 + (iV * 0.5)
    cmdCancelarCat.Left = 4800 + (iV * 0.5)
    picLeyenda.Top = vpicLeyendaT + iH
    picLeyenda.Left = vpicLeyendaL + iV
    
    'Adjuntos:
    picSizeAdjuntos.Width = SSTab1.Width - 400
    lstvwEsp.Width = SSTab1.Width - 400
    lstvwEsp.Height = SSTab1.Height - picSizeAdjuntos.Height - 1100
    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.2
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.19
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.6
    
    picEsp.Top = lstvwEsp.Top + lstvwEsp.Height + 20
    If SSTab1.Tab = 5 Then
        picEsp.Left = lstvwEsp.Width - (cmdAbrirEsp.Width * 6) - 50
    End If
    
    iAWidth = Me.Width
    iAHeight = Me.Height
    
End Sub

Private Sub Form_Resize()
If Not m_bLoad Then Arrange
End Sub

''' <summary>
''' Descarga los objetos globales
''' </summary>
''' <param name="Cancel">Cancelar el cierre</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Set m_oActsN1Seleccionados = Nothing
    Set m_oActsN2Seleccionados = Nothing
    Set m_oActsN3Seleccionados = Nothing
    Set m_oActsN4Seleccionados = Nothing
    Set m_oActsN5Seleccionados = Nothing
    Set m_oActsN1DesSeleccionados = Nothing
    Set m_oActsN2DesSeleccionados = Nothing
    Set m_oActsN3DesSeleccionados = Nothing
    Set m_oActsN4DesSeleccionados = Nothing
    Set m_oActsN5DesSeleccionados = Nothing
    Set m_oActsN1 = Nothing
    Set m_oActsN2 = Nothing
    Set m_oActsN3 = Nothing
    Set m_oActsN4 = Nothing
    Set m_oActsN5 = Nothing
    Set g_oCiaSeleccionada = Nothing
    
    If (m_bSesionIniciada) Then
        FinalizarMailSender
    End If
End Sub

Private Sub opFPAut_Click()
    If opFPAut.Value = True Then
        lblFecAut.BackColor = m_lblVerde
        lblFecDesAut.BackColor = m_lblAmarillo
        lblFecSolicit.BackColor = m_lblAmarillo
    End If
End Sub

Private Sub opFPSol_Click()
    If opFPSol.Value = True Then
        lblFecAut.BackColor = m_lblAmarillo
        lblFecDesAut.BackColor = m_lblAmarillo
        lblFecSolicit.BackColor = m_lblVerde
    End If
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Integer
    Dim ador As ador.Recordset
    
    sdbcCiaCod.RemoveAll
    Set g_oCiaSeleccionada = Nothing
    cmdModificar.Enabled = False
    cmdCodigo.Enabled = False
    
    cmdEliminar.Enabled = False
    cmdRestaurar0.Enabled = False
    cmdRestaurar1.Enabled = False
    cmdRestaurar2.Enabled = False


    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, , , , , , , , , , , , , , 4)
            Else
                Set ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, , , , , , , , , , , , , , 4)
            End If
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasPendientesDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False)
            Else
                Set ador = m_oCias.DevolverCiasPendientesDesde(g_iCargaMaximaCombos, , , False, False)
            End If
    End Select
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("FCEST").Value & Chr(9) & ador("FPEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    
End Sub
Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaCod_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcCiaDen.Text = ""
        LimpiarDatosCia
        m_bRespetarCombo = False
        
        m_bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    m_bRespetarCombo = True
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    CiaSeleccionada
    
    
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset

    If Trim(sdbcCiaCod.Text) = "" Then Exit Sub
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            Set ador = m_oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , , , , , , , , , , , , , , 4)
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            Set ador = m_oCias.DevolverCiasPendientesDesde(1, sdbcCiaCod.Text, , True)
    End Select
    
    If ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                m_bRespetarCombo = True
                sdbcCiaDen.Text = ador("DEN").Value
                sdbcCiaCod.Text = ador("COD").Value
                sdbcCiaCod.Columns(0).Value = ador("ID").Value
                CiaSeleccionada
                m_bRespetarCombo = False
                m_bCargarComboDesde = False
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If
    
End Sub

Private Sub sdbcCiaDen_DropDown()

    Dim i As Integer
    Dim ador As ador.Recordset
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, , , , , , , , , , , , , , 4)
            Else
                Set ador = m_oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, , , , , , , , , , , , , , 4)
            End If
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasPendientesDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True)
            Else
                Set ador = m_oCias.DevolverCiasPendientesDesde(basParametros.g_iCargaMaximaCombos, , , False, True)
            End If
    End Select
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("FCEST").Value & Chr(9) & ador("FPEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcCiaCod.Text = ""
        LimpiarDatosCia
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    CiaSeleccionada
    
End Sub

Private Sub LimpiarDatosCia()
    
    sdbcIdiCod.Text = ""
    sdbcMonCod.Text = ""
    sdbcPaiCod.Text = ""
    sdbcProviCod.Text = ""
    
    txtCodProveGS.Text = ""
    
    txtCod = ""
    txtDen = ""
    txtNif = ""
    txtDir = ""
    txtPob = ""
    txtCP = ""
    txtURL = ""
        txtPMvinculada = ""
        
    txtVol = ""
    txtCliRef1 = ""
    txtCliRef2 = ""
    txtCliRef3 = ""
    txtCliRef4 = ""
    txtHom1 = ""
    txtHom2 = ""
    txtHom3 = ""
    txtObs = ""
    txtTamanyoMax.Text = ""
    
    opFPSol.Value = False
    opFPAut.Value = False
    opFPDes.Value = False
    opFPSol.Enabled = True
    lblFecAut.Caption = ""
    lblFecDesAut.Caption = ""
    lblFecSolicit.Caption = ""
    lblFecAut.BackColor = m_lblAmarillo
    lblFecDesAut.BackColor = m_lblAmarillo
    lblFecSolicit.BackColor = m_lblAmarillo
        
    chkPremium.Enabled = True
    chkPremium.Value = vbUnchecked
    optPRAut.Value = False
    optPRDes.Value = False
    
    'Limpia los treviews
    Me.tvwEstrMat.Nodes.Clear
    Me.tvwEstrMatConsul.Nodes.Clear
    
    SSTab1.TabVisible(0) = True
    SSTab1.TabVisible(1) = False
    SSTab1.TabVisible(2) = False
    SSTab1.TabVisible(3) = False
    SSTab1.TabVisible(4) = False
    SSTab1.TabVisible(5) = False
    SSTab1.TabVisible(6) = False
    
    Set g_oCiaSeleccionada = Nothing
    
    cmdA�adir.Enabled = True
    cmdModificar.Enabled = False
    cmdCodigo.Enabled = False
    cmdRestaurar0.Enabled = False
    cmdRestaurar1.Enabled = False
    cmdRestaurar2.Enabled = False
    cmdEliminar.Enabled = False
    cmdListado.Enabled = False
    cmdGuardarSize.Enabled = False
    
End Sub

Public Sub CiaSeleccionada()
Dim adores As ador.Recordset

    LimpiarDatosCia
    If Not sdbcCiaCod.Text = "" Then
        m_oCias.CargarTodasLasCiasDesde 1, sdbcCiaCod.Columns(0).Value, , , True
    
        Set g_oCiaSeleccionada = Nothing
        Set g_oCiaSeleccionada = m_oCias.Item(CStr(sdbcCiaCod.Columns(0).Value))
        cmdA�adir.Enabled = True
        cmdModificar.Enabled = True
        cmdCodigo.Enabled = True
        cmdEliminar.Enabled = True
        cmdRestaurar0.Enabled = True
        cmdRestaurar1.Enabled = True
        cmdRestaurar2.Enabled = True
        cmdListado.Enabled = True
        cmdCodigo.Enabled = True
        cmdRestaurar0_click
    
        Select Case SSTab1.Tab
            Case 1
                RestaurarActiv
                    
            Case 3
                CargarUsuariosCia

                'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
                If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                    sdbgUsu.Columns("CATEGORIA").Visible = True
                    sdbgUsu.Columns("ACCESOFSGA").Visible = True
                    sdbgUsu.Columns("COD").Width = sdbgUsu.Width * 0.11
                    sdbgUsu.Columns("NOM").Width = sdbgUsu.Width * 0.14
                    sdbgUsu.Columns("APE").Width = sdbgUsu.Width * 0.17
                    sdbgUsu.Columns("FCAUT").Width = sdbgUsu.Width * 0.0945
                    sdbgUsu.Columns("FPAUT").Width = sdbgUsu.Width * 0.12
                    sdbgUsu.Columns("PPAL").Width = sdbgUsu.Width * 0.07
                    sdbgUsu.Columns("CATEGORIA").Width = sdbgUsu.Width * 0.23
                    sdbgUsu.Columns("ACCESOFSGA").Width = sdbgUsu.Width * 0.12
                Else
                    sdbgUsu.Columns("CATEGORIA").Visible = False
                    sdbgUsu.Columns("ACCESOFSGA").Visible = False
                    sdbgUsu.Columns("COD").Width = sdbgUsu.Width * 0.1497
                    sdbgUsu.Columns("NOM").Width = sdbgUsu.Width * 0.25
                    sdbgUsu.Columns("APE").Width = sdbgUsu.Width * 0.32
                    sdbgUsu.Columns("FCAUT").Width = sdbgUsu.Width * 0.0945
                    sdbgUsu.Columns("FPAUT").Width = sdbgUsu.Width * 0.12
                    sdbgUsu.Columns("PPAL").Width = sdbgUsu.Width * 0.12
                End If
                
        End Select
        
        'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
        If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
            CargarCategoriasProve
        Else
            Set m_oCategoriasProve = Nothing
        End If
        
        Set g_oCiaSeleccionada.Categorias = Nothing
        Set g_oCiaSeleccionada.Categorias = g_oRaiz.Generar_CCategorias
        Set g_oCiaSeleccionada.Categoriaselim = Nothing
        Set g_oCiaSeleccionada.Categoriaselim = g_oRaiz.Generar_CCategorias  'En esta colecci�n guardaremos todas las categorias del proveedor a eliminar.
        'Registrar accion de seleccion de compa�ia
        If m_IdCia_Ant <> g_oCiaSeleccionada.Id Then
            g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Sel, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod, _
                                                g_oCiaSeleccionada.Id
            m_IdCia_Ant = g_oCiaSeleccionada.Id
        End If
    End If
    m_bRespetarCombo = False
End Sub

''' <summary>Carga los datos de la cia</summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>
''' <revisado>JVS 16/06/2011 </revisado>

Private Sub cmdRestaurar0_click()
    
    
    Dim ador As ador.Recordset
    Dim ador2 As ador.Recordset

    If g_oCiaSeleccionada Is Nothing Then Exit Sub
    
    Set ador = m_oCias.DevolverDatosCia(sdbcCiaCod.Columns(0).Value)
    If Not ador Is Nothing And Not ador.EOF Then
        sdbcMonCod.Text = ador("MONDEN").Value
        g_oCiaSeleccionada.codmon = ador("MONID").Value
        
        sdbcPaiCod.Text = ador("PAIDEN").Value
        g_oCiaSeleccionada.codpai = ador("PAIID").Value
        Set ador2 = g_oRaiz.DevolverPaisesDelPortalDesde(1, , ador("PAIDEN").Value, True)
        sdbcPaiCod.Columns(0).Value = ador("PAIID").Value
        sdbcPaiCod.Columns("VALIDNIF").Value = ador2("VALIDNIF").Value
        ador2.Close
        Set ador2 = Nothing
        
        If Not IsNull(ador("PROVIID").Value) Then
            sdbcProviCod.Text = ador("PROVIDEN").Value
            g_oCiaSeleccionada.codprovi = ador("PROVIID").Value
        Else
            sdbcProviCod.Text = ""
            g_oCiaSeleccionada.codprovi = ador("PROVIID").Value
        End If
        m_bProvinciaObligatoria = ProvinciaEsObligatoria()
        sdbcIdiCod.Text = ador("IDIDEN")
        g_oCiaSeleccionada.idioma = ador("IDIID").Value

        txtCod.Text = ador("COD").Value
        g_oCiaSeleccionada.Cod = ador("COD").Value
        txtDen = ador("DEN").Value
        g_oCiaSeleccionada.Den = ador("DEN").Value
        txtCP = ador("CP").Value
        g_oCiaSeleccionada.CodPos = ador("CP").Value
        If Not IsNull(ador("COMENT").Value) Then
            txtObs = ador("COMENT").Value
            g_oCiaSeleccionada.Coment = ador("COMENT").Value
        Else
            txtObs = ""
            g_oCiaSeleccionada.Coment = ador("COMENT").Value
        End If
        txtDir = ador("DIR").Value
        g_oCiaSeleccionada.Dir = ador("DIR").Value
        txtNif = ador("NIF").Value
        g_oCiaSeleccionada.NIF = ador("NIF").Value
        txtPob = ador("POB").Value
        g_oCiaSeleccionada.Poblacion = ador("POB").Value
        If Not IsNull(ador("URLCIA").Value) Then
            txtURL = ador("URLCIA").Value
            g_oCiaSeleccionada.URLCia = ador("URLCIA").Value
        Else
            txtURL = ""
            g_oCiaSeleccionada.URLCia = ador("URLCIA").Value
        End If
        
        If Not IsNull(ador("INSTANCIA_VINCULADA").Value) Then
            txtPMvinculada = ador("INSTANCIA_VINCULADA").Value
            g_oCiaSeleccionada.PMVinculada = ador("INSTANCIA_VINCULADA").Value
        Else
            txtPMvinculada = ""
            g_oCiaSeleccionada.PMVinculada = 0
        End If
        
        
        If Not IsNull(ador("VOLFACT").Value) Then
            txtVol.Text = FormateoNumerico(ador("VOLFACT").Value, "#,##0.##")
        Else
            txtVol.Text = ""
        End If
        If Not IsNull(ador("CLIREF1").Value) Then
            txtCliRef1 = ador("CLIREF1").Value
            g_oCiaSeleccionada.CliRef1 = ador("CLIREF1").Value
        Else
            txtCliRef1 = ""
            g_oCiaSeleccionada.CliRef1 = ador("CLIREF1").Value
        End If
        If Not IsNull(ador("CLIREF2").Value) Then
            txtCliRef2 = ador("CLIREF2").Value
            g_oCiaSeleccionada.CliRef2 = ador("CLIREF2").Value
        Else
            txtCliRef2 = ""
            g_oCiaSeleccionada.CliRef2 = ador("CLIREF2").Value
        End If
        If Not IsNull(ador("CLIREF3").Value) Then
            txtCliRef3 = ador("CLIREF3").Value
            g_oCiaSeleccionada.CliRef3 = ador("CLIREF3").Value
         Else
            txtCliRef3 = ""
            g_oCiaSeleccionada.CliRef2 = ador("CLIREF3").Value
        End If
        If Not IsNull(ador("CLIREF4").Value) Then
            txtCliRef4 = ador("CLIREF4").Value
            g_oCiaSeleccionada.CliRef4 = ador("CLIREF4").Value
         Else
            txtCliRef4 = ""
            g_oCiaSeleccionada.CliRef2 = ador("CLIREF4").Value
        End If
        If Not IsNull(ador("HOM1").Value) Then
            txtHom1 = ador("HOM1").Value
            g_oCiaSeleccionada.Hom1 = ador("HOM1").Value
        Else
            txtHom1 = ""
            g_oCiaSeleccionada.Hom1 = ador("HOM1").Value
        End If
        If Not IsNull(ador("HOM2").Value) Then
            txtHom2 = ador("HOM2").Value
            g_oCiaSeleccionada.Hom2 = ador("HOM2").Value
        Else
            txtHom2 = ""
            g_oCiaSeleccionada.Hom1 = ador("HOM2").Value
        End If
        If Not IsNull(ador("HOM3").Value) Then
            txtHom3 = ador("HOM3").Value
            g_oCiaSeleccionada.Hom3 = ador("HOM3").Value
        Else
            txtHom3 = ""
            g_oCiaSeleccionada.Hom1 = ador("HOM3").Value
        End If
        g_oCiaSeleccionada.EstadoCompradora = ador("FCEST").Value
        If Not IsNull(ador("FPEST").Value) Then
            g_oCiaSeleccionada.EstadoProveedora = ador("FPEST").Value
        Else
            g_oCiaSeleccionada.EstadoProveedora = 0
        End If
        
        If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
            txtCodProveGS.Text = NullToStr(ador("FSGS_COD_PROVE").Value)
            g_oCiaSeleccionada.CodGS = NullToStr(ador("FSGS_COD_PROVE").Value)
        End If
        
        g_oCiaSeleccionada.FecAutorizado = ador("FECACTFP").Value
        g_oCiaSeleccionada.FecDesautorizado = ador("FECDESACTFP").Value
        g_oCiaSeleccionada.FecSolicitado = ador("FECINS").Value
        
        g_oCiaSeleccionada.MaxSizeAdjun = ador("MAX_ADJUN").Value
        
        'PORTAL
        sdbcPortal.Text = NullToStr(ador("PORTAL").Value)
        g_oCiaSeleccionada.Portal = NullToStr(ador("PORTAL").Value)
        sdbcPortal.Columns(0).Value = NullToStr(ador("PORTAL").Value)
        
        ador.Close
        Set ador = Nothing
        

        RestaurarAut
                
        SSTab1.TabVisible(1) = True
        SSTab1.TabVisible(2) = True
        SSTab1.TabVisible(3) = True
        If g_oCiaSeleccionada.EstadoProveedora > 0 And Not basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
            SSTab1.TabVisible(4) = True
        End If
        SSTab1.TabVisible(5) = True
        
        'Si est� activo el FSGA y la CIA est� enlazada se mostrar� la pesta�a de Categor�as laborales.
        If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
            SSTab1.TabVisible(6) = True
        End If
        
        DatosGen_Iniciales
            
    Else
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    'Cargamos las especificaciones
    txtTamanyoMax.Text = Format(g_oCiaSeleccionada.MaxSizeAdjun / 1024, "#,##0")
    cmdGuardarSize.Enabled = False
    g_oCiaSeleccionada.CargarTodasLasEspecificaciones True, False
    AnyadirEspsALista
    ActualizarEspacioLibre
    
End Sub
''' <summary>Devuelve si hay que Bloquear la edici�n de la raz�n Social, NIF y Moneda de la Cia. Si no est� enlazado ni integrado se puede</summary>
''' <remarks>Llamada desde=cmdCancelar_Click, cmdAceptar_Click y cmdModificar_Click; Tiempo m�ximo=0seg.</remarks>
Private Function HayQueBloquearModif() As Boolean
    HayQueBloquearModif = False
    If g_udtParametrosGenerales.gbBloqModifProve Then
        If g_oCiaSeleccionada.CodGS <> "" Then
            HayQueBloquearModif = g_oCiaSeleccionada.IntegradoEnGS
        End If
    End If
End Function
Private Sub AnyadirEspsALista()
Dim oEsp As CEspecificacion

    lstvwEsp.ListItems.Clear
    
    For Each oEsp In g_oCiaSeleccionada.Especificaciones
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.Nombre, , "ESP"
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
    
End Sub

Private Sub cmdRestaurar1_click()
    Dim ador As ador.Recordset

    If g_oCiaSeleccionada Is Nothing Then Exit Sub
    
    Set ador = m_oCias.DevolverDatosCia(sdbcCiaCod.Columns(0).Value)
    If Not ador Is Nothing And Not ador.EOF Then
        sdbcMonCod.Text = ador("MONDEN")
        g_oCiaSeleccionada.codmon = ador("MONID")
        sdbcPaiCod.Text = ador("PAIDEN"): g_oCiaSeleccionada.codpai = ador("PAIID")
        sdbcPaiCod.Columns(0).Value = ador("PAIID")
        If Not IsNull(ador("PROVIID")) Then
            sdbcProviCod.Text = ador("PROVIDEN"): g_oCiaSeleccionada.codprovi = ador("PROVIID")
        Else
            sdbcProviCod.Text = "": g_oCiaSeleccionada.codprovi = ador("PROVIID")
        End If
        sdbcIdiCod.Text = ador("IDIDEN"): g_oCiaSeleccionada.idioma = ador("IDIID")
        txtCod.Text = ador("COD"): g_oCiaSeleccionada.Cod = ador("COD")
        txtDen = ador("DEN"): g_oCiaSeleccionada.Den = ador("DEN")
        txtCP = ador("CP"): g_oCiaSeleccionada.CodPos = ador("CP")
        If Not IsNull(ador("COMENT")) Then
            txtObs = ador("COMENT"): g_oCiaSeleccionada.Coment = ador("COMENT")
        Else
            txtObs = "": g_oCiaSeleccionada.Coment = ador("COMENT")
        End If
        txtDir = ador("DIR"): g_oCiaSeleccionada.Dir = ador("DIR")
        txtNif = ador("NIF"): g_oCiaSeleccionada.NIF = ador("NIF")
        txtPob = ador("POB"): g_oCiaSeleccionada.Poblacion = ador("POB")
        If Not IsNull(ador("URLCIA")) Then
            txtURL = ador("URLCIA"): g_oCiaSeleccionada.URLCia = ador("URLCIA")
        Else
            txtURL = "": g_oCiaSeleccionada.URLCia = ador("URLCIA")
        End If
        If Not IsNull(ador("PMVINCULADA")) Then
            txtPMvinculada.Text = ador("PMVINCULADA"): g_oCiaSeleccionada.URLCia = ador("PMVINCULADA")
        Else
            txtPMvinculada.Text = "": g_oCiaSeleccionada.PMVinculada = ador("PMVINCULADA")
        End If
        
        If Not IsNull(ador("VOLFACT").Value) Then
            txtVol.Text = FormateoNumerico(ador("VOLFACT").Value, "#,##0.##"): g_oCiaSeleccionada.VolFac = ador("VOLFACT")
        Else
            txtVol.Text = "": g_oCiaSeleccionada.VolFac = Null
        End If
        If Not IsNull(ador("CLIREF1")) Then
            txtCliRef1 = ador("CLIREF1"): g_oCiaSeleccionada.CliRef1 = ador("CLIREF1")
        Else
            txtCliRef1 = "": g_oCiaSeleccionada.CliRef1 = ador("CLIREF1")
        End If
        If Not IsNull(ador("CLIREF2")) Then
            txtCliRef2 = ador("CLIREF2"): g_oCiaSeleccionada.CliRef2 = ador("CLIREF2")
        Else
            txtCliRef2 = "": g_oCiaSeleccionada.CliRef2 = ador("CLIREF2")
        End If
        If Not IsNull(ador("CLIREF3")) Then
            txtCliRef3 = ador("CLIREF3"): g_oCiaSeleccionada.CliRef3 = ador("CLIREF3")
         Else
            txtCliRef3 = "": g_oCiaSeleccionada.CliRef2 = ador("CLIREF3")
        End If
        If Not IsNull(ador("CLIREF4")) Then
            txtCliRef4 = ador("CLIREF4"): g_oCiaSeleccionada.CliRef4 = ador("CLIREF4")
         Else
            txtCliRef4 = "": g_oCiaSeleccionada.CliRef2 = ador("CLIREF4")
        End If
        If Not IsNull(ador("HOM1")) Then
            txtHom1 = ador("HOM1"): g_oCiaSeleccionada.Hom1 = ador("HOM1")
        Else
            txtHom1 = "": g_oCiaSeleccionada.Hom1 = ador("HOM1")
        End If
        If Not IsNull(ador("HOM2")) Then
            txtHom2 = ador("HOM2"): g_oCiaSeleccionada.Hom2 = ador("HOM2")
        Else
            txtHom2 = "": g_oCiaSeleccionada.Hom1 = ador("HOM2")
        End If
        If Not IsNull(ador("HOM3")) Then
            txtHom3 = ador("HOM3"): g_oCiaSeleccionada.Hom3 = ador("HOM3")
        Else
            txtHom3 = "": g_oCiaSeleccionada.Hom1 = ador("HOM3")
        End If
        g_oCiaSeleccionada.EstadoCompradora = ador("FCEST")
        If Not IsNull(ador("FPEST")) Then
            g_oCiaSeleccionada.EstadoProveedora = ador("FPEST")
        Else
            g_oCiaSeleccionada.EstadoProveedora = 0
        End If
        'PORTAL
        sdbcPortal.Text = NullToStr(ador("PORTAL").Value)
        g_oCiaSeleccionada.Portal = NullToStr(ador("PORTAL").Value)
        sdbcPortal.Columns(0).Value = NullToStr(ador("PORTAL").Value)
        
        g_oCiaSeleccionada.FecAutorizado = ador("FECACTFP").Value
        g_oCiaSeleccionada.FecDesautorizado = ador("FECDESACTFP").Value
        g_oCiaSeleccionada.FecSolicitado = ador("FECINS").Value
        
        RestaurarAut
       
        ModoConsultaAct
        
        SSTab1.TabVisible(1) = True
        SSTab1.TabVisible(2) = True
        SSTab1.TabVisible(3) = True
        If g_oCiaSeleccionada.EstadoProveedora > 0 And Not g_udtParametrosGenerales.g_bUnaCompradora Then
            SSTab1.TabVisible(4) = True
        End If
        SSTab1.TabVisible(5) = True
    Else
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    
End Sub
Private Sub cmdRestaurar2_click()
    cmdRestaurar1_click
End Sub

Private Sub sdbcIdiCod_Change()
 
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        m_CargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcIdiCod_CloseUp()
    
    If sdbcIdiCod.Value = "..." Then
        sdbcIdiCod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcIdiCod.Text = sdbcIdiCod.Columns(1).Text
    m_bRespetarCombo = False
    
    m_CargarComboDesde = False

End Sub

Private Sub sdbcIdiCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcIdiCod.RemoveAll
    
    If m_CargarComboDesde Then
        Set ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbcIdiCod, , False)
    Else
        Set ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    End If
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcIdiCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcIdiCod_InitColumnProps()
    
    sdbcIdiCod.DataFieldList = "Column 1"
    sdbcIdiCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcIdiCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcIdiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcIdiCod.Rows - 1
            bm = sdbcIdiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcIdiCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcIdiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub


Private Sub sdbcIdiCod_Validate(Cancel As Boolean)
    If sdbcIdiCod.Text = "" Then
        Exit Sub
    Else
        Dim ador As ador.Recordset
        Set ador = g_oRaiz.DevolverIdiomasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcIdiCod.Text, True)
        If ador.RecordCount = 0 Then
            NoValido sIdiIdioma
            sdbcIdiCod.Text = ""
            Cancel = True
        End If
        ador.Close
        Set ador = Nothing
    End If

End Sub

Private Sub sdbcMonCod_Change()
 
    If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        m_CargarComboDesde = True
    End If
    
        
End Sub

Private Sub sdbcMonCod_CloseUp()
    
     If sdbcMonCod.Value = "..." Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcMonCod.Text = sdbcMonCod.Columns(1).Text
    m_bRespetarCombo = False
   
    
    m_CargarComboDesde = False

End Sub

Private Sub sdbcMonCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcMonCod.RemoveAll
    
    If m_CargarComboDesde Then
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, sdbcMonCod, , False, , True)
    Else
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False, , True)
    End If
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcMonCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcMonCod_InitColumnProps()
    
    sdbcMonCod.DataFieldList = "Column 1"
    sdbcMonCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcMonCod_PositionList(ByVal Text As String)
    Dim i As Long
   Dim bm As Variant

    On Error Resume Next
    
    sdbcMonCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMonCod.Rows - 1
            bm = sdbcMonCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMonCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcMonCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcMonCod_Validate(Cancel As Boolean)
    If sdbcMonCod.Text = "" Then
        Exit Sub
    Else
        Dim ador As ador.Recordset
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcMonCod.Text, True)
        If ador.RecordCount = 0 Then
            NoValida ("Moneda")
            sdbcMonCod.Text = ""
            Cancel = True
        End If
        ador.Close
        Set ador = Nothing
    End If
End Sub

Private Sub sdbcPaiCod_Change()
 
    If Not m_bRespetarCombo Then
        sdbcProviCod.Text = ""
        m_bRespetarCombo = True
        m_CargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcPaiCod_CloseUp()
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiCod.Columns(1).Text
    sdbcProviCod.Text = ""
    m_bRespetarCombo = False
    
    m_CargarComboDesde = False
    
End Sub

Private Sub sdbcPaiCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcPaiCod.RemoveAll
    
    If m_CargarComboDesde Then
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcPaiCod.Columns(1).Text, , False)
    Else
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    End If
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcPaiCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("VALIDNIF").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcPaiCod_InitColumnProps()
    
    sdbcPaiCod.DataFieldList = "Column 1"
    sdbcPaiCod.DataFieldToDisplay = "Column 1"
    
End Sub




Private Sub sdbcPaiCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

''' <summary>Valida cambios en el combo de pa�ses.</summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>
''' <revisado>JVS 16/06/2011 </revisado>

Private Sub sdbcPaiCod_Validate(Cancel As Boolean)
    m_bRespetarCombo = False
    If sdbcPaiCod.Text = "" Then
        Exit Sub
    Else
        Dim ador As ador.Recordset
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcPaiCod.Text, True)
        If ador.RecordCount = 0 Then
            NoValido (sIdiPai)
            sdbcPaiCod.Text = ""
            Cancel = True
        Else
            sdbcPaiCod.RemoveAll
            sdbcPaiCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("VALIDNIF").Value
            
            m_lIdMonPai = ador.Fields("MON").Value
            
            If IsNull(m_lIdMonPai) Then
                m_lIdMonPai = g_udtParametrosGenerales.g_iMonedaCentral
            End If
            
            ador.Close
            
            Set ador = g_oRaiz.DevolverMoneda(m_lIdMonPai)
            If Not ador.EOF Then
                m_sDenMonDef = ador.Fields("DEN").Value
                m_bRespetarCombo = True
                Me.sdbcMonCod.Text = m_sDenMonDef
            End If
            
        
        End If
        ador.Close
        Set ador = Nothing
    End If
    m_bProvinciaObligatoria = ProvinciaEsObligatoria()
End Sub

Private Sub sdbcPortal_CloseUp()
    sdbcPortal.Text = sdbcPortal.Columns(0).Text
End Sub

Private Sub sdbcPortal_InitColumnProps()
    sdbcPortal.DataFieldList = "Column 0"
    sdbcPortal.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcPortal_PositionList(ByVal Text As String)
Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPortal.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPortal.Rows - 1
            bm = sdbcPortal.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPortal.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPortal.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbcProviCod_Change()

    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        
        m_CargarComboDesde = True
        
        
    End If

End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcPaiCod.Text = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcProviCod.Text = sdbcProviCod.Columns(1).Text
    m_bRespetarCombo = False
    
    m_CargarComboDesde = False
    
End Sub

Private Sub sdbcProviCod_DropDown()
Dim ador As ador.Recordset
    
    sdbcProviCod.RemoveAll
    
    If sdbcPaiCod.Columns("ID").Value <> "" Then
    
        Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(sdbcPaiCod.Columns("ID").Value, , , False)
   
    End If
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcProviCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If

        
End Sub

Private Sub sdbcProviCod_InitColumnProps()
    
    sdbcProviCod.DataFieldList = "Column 1"
    sdbcProviCod.DataFieldToDisplay = "Column 1"
    
End Sub

''' <summary>Valida los datos de la cia</summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde frmCompanias; Tiempo m�ximo=0seg.</remarks>
''' <revisado>JVS 16/06/2011 </revisado>

Private Function Validacion() As Boolean
Dim adores As ador.Recordset
Dim sCiaComp As String
    
    m_iDesactivarPremium = vbNo

    If Trim(txtCod) = "" Then
        basMensajes.NoValido sIdiCodigo
        txtCod.SetFocus
        Validacion = False
        Exit Function
    Else
        Set adores = m_oCias.DevolverCiasDesde(1, Trim(txtCod), , True, , , , , , , , , , , , , , , 4)
        If adores.RecordCount > 0 Then
            If Not m_bAnyadirCia And adores("ID") = g_oCiaSeleccionada.Id Then
                GoTo seguir
            End If
            basMensajes.DatoDuplicado sIdiCodCompania
            Validacion = False
            adores.Close
            Set adores = Nothing
            Exit Function
        End If

seguir:
        adores.Close
        Set adores = Nothing

        g_oCiaSeleccionada.Cod = txtCod
    
    End If

    If Trim(txtDen) = "" Then
        basMensajes.NoValida sIdiDen
        txtDen.SetFocus
        Validacion = False
        Exit Function
    Else
        g_oCiaSeleccionada.Den = txtDen
    End If
    

    If (opFPSol Or opFPAut Or opFPDes) Or (g_oCiaSeleccionada.EstadoProveedora > 0) Then
        If opFPSol Or opFPAut Or opFPDes Then
            If g_udtParametrosGenerales.g_bPremium And g_oCiaSeleccionada.EstadoProveedora = 3 And opFPDes And chkPremium = vbChecked And optPRAut Then
                m_iDesactivarPremium = basMensajes.PreguntaDesautorizarPremium
            End If
            If opFPSol Then
                If g_oCiaSeleccionada.EstadoProveedora <> 1 Then g_oCiaSeleccionada.FecSolicitado = Date
                g_oCiaSeleccionada.EstadoProveedora = 1
            End If
            
            If opFPDes Then
                If g_oCiaSeleccionada.EstadoProveedora <> 2 Then g_oCiaSeleccionada.FecDesautorizado = Date
                g_oCiaSeleccionada.EstadoProveedora = 2
            End If
            
            If opFPAut Then
                If g_oCiaSeleccionada.EstadoProveedora <> 3 Then g_oCiaSeleccionada.FecAutorizado = Date
                g_oCiaSeleccionada.EstadoProveedora = 3
            End If
        Else
            g_oCiaSeleccionada.EstadoProveedora = 0
        End If
    Else
        basMensajes.NoValido sIdiAccesPortal
        SSTab1_Click 1
        Validacion = False
        Exit Function
    End If
    
    If Trim(txtNif) = "" Then
        basMensajes.NifIncorrecto
        txtNif.SetFocus
        Validacion = False
        Exit Function
    Else
        
        If Not ValidarNIF Then
            Validacion = False
            Exit Function
        End If
        g_oCiaSeleccionada.NIF = txtNif
    End If
    
    If Trim(txtDir) = "" Then
        basMensajes.NoValida sIdiDir
        txtDir.SetFocus
        Validacion = False
        Exit Function
    Else
        g_oCiaSeleccionada.Dir = txtDir
    End If
    
    If Trim(txtCP) = "" Then
        basMensajes.NoValido sIdiCP
        txtCP.SetFocus
        Validacion = False
        Exit Function
    Else
        g_oCiaSeleccionada.CodPos = txtCP
    End If
    
    If Trim(txtPob) = "" Then
        basMensajes.NoValida sIdiPob
        txtPob.SetFocus
        Validacion = False
        Exit Function
    Else
        g_oCiaSeleccionada.Poblacion = txtPob
    End If
    
    If sdbcPaiCod = "" Then
        basMensajes.NoValido sIdiPai
        sdbcPaiCod.SetFocus
        Validacion = False
        Exit Function
    Else
        Set adores = g_oRaiz.DevolverPaisesDelPortalDesde(1, , sdbcPaiCod.Text, True)
        If adores.RecordCount > 0 Then
            g_oCiaSeleccionada.codpai = adores("ID")
        End If
        Set adores = Nothing
    End If
    
    
    If sdbcMonCod = "" Then
        basMensajes.NoValida sIdiMon
        sdbcMonCod.SetFocus
        Validacion = False
        Exit Function
    Else
        Set adores = g_oRaiz.DevolverMonedasDelPortalDesde(1, , sdbcMonCod.Text, True)
        If adores.RecordCount > 0 Then
            g_oCiaSeleccionada.codmon = adores("ID")
        End If
        Set adores = Nothing
     End If
    
    If Trim(txtVol) = "" Then
        g_oCiaSeleccionada.VolFac = Null
    Else
        If Not IsNumeric(txtVol) Then
            basMensajes.NoValido sIdiVolFact
            txtVol.SetFocus
            Validacion = False
            Exit Function
        Else
            g_oCiaSeleccionada.VolFac = CDbl(txtVol)
        End If
    End If
    
    If sdbcIdiCod = "" Then
        basMensajes.NoValido sIdiIdioma
        sdbcIdiCod.SetFocus
        Validacion = False
        Exit Function
    Else
        Set adores = g_oRaiz.DevolverIdiomasDelPortalDesde(g_oCiaSeleccionada.codpai, , sdbcIdiCod.Text, True)
        If adores.RecordCount > 0 Then
            g_oCiaSeleccionada.idioma = adores("ID")
        End If
        Set adores = Nothing
    
    End If
    
    ' Si la provincia es obligatoria y no se ha metido el valor da error
    If Trim(sdbcProviCod.Text) = "" And m_bProvinciaObligatoria Then
        basMensajes.NoValido sIdiProvi
        Validacion = False
        Exit Function
    End If
    
    ' Portal
    If m_bVariosPortales And Trim(sdbcPortal.Text) = "" Then
        basMensajes.NoValido Label20.Caption
        Validacion = False
        Exit Function
    End If
    
    If Trim(sdbcProviCod) = "" Then
        g_oCiaSeleccionada.codprovi = Null
    Else
        Set adores = g_oRaiz.DevolverTodasLasProvinciasDesde(g_oCiaSeleccionada.codpai, , sdbcProviCod.Text, True)
        If adores.RecordCount > 0 Then
            g_oCiaSeleccionada.codprovi = adores("ID")
        End If
        Set adores = Nothing
     
    End If
    
    If chkPremium And optPRAut Then
        g_oCiaSeleccionada.Premium = 2
    ElseIf chkPremium And optPRDes Then
        g_oCiaSeleccionada.Premium = 3
    ElseIf chkPremium Then
        g_oCiaSeleccionada.Premium = 1
    Else
        g_oCiaSeleccionada.Premium = 0
    End If
    If m_iDesactivarPremium = vbYes Then g_oCiaSeleccionada.Premium = 3
    
    
    If Trim(txtCliRef1) = "" Then
        g_oCiaSeleccionada.CliRef1 = Null
    Else
        g_oCiaSeleccionada.CliRef1 = txtCliRef1
    End If
    If Trim(txtCliRef2) = "" Then
        g_oCiaSeleccionada.CliRef2 = Null
    Else
        g_oCiaSeleccionada.CliRef2 = txtCliRef2
    End If
    If Trim(txtCliRef3) = "" Then
        g_oCiaSeleccionada.CliRef3 = Null
    Else
        g_oCiaSeleccionada.CliRef3 = txtCliRef3
    End If
    If Trim(txtCliRef4) = "" Then
        g_oCiaSeleccionada.CliRef4 = Null
    Else
        g_oCiaSeleccionada.CliRef4 = txtCliRef4
    End If
    
    If Trim(txtHom1) = "" Then
        g_oCiaSeleccionada.Hom1 = Null
    Else
        g_oCiaSeleccionada.Hom1 = txtHom1
    End If
    If Trim(txtHom2) = "" Then
        g_oCiaSeleccionada.Hom2 = Null
    Else
        g_oCiaSeleccionada.Hom2 = txtHom2
    End If
    If Trim(txtHom3) = "" Then
        g_oCiaSeleccionada.Hom3 = Null
    Else
        g_oCiaSeleccionada.Hom3 = txtHom3
    End If
    
    If Trim(txtURL) = "" Then
        g_oCiaSeleccionada.URLCia = Null
    Else
        g_oCiaSeleccionada.URLCia = txtURL
    End If
    
    If Trim(txtPMvinculada.Text) = "" Then
        g_oCiaSeleccionada.PMVinculada = 0
    Else
        g_oCiaSeleccionada.PMVinculada = txtPMvinculada.Text
    End If
    
    If Trim(txtObs) = "" Then
        g_oCiaSeleccionada.Coment = Null
    Else
        g_oCiaSeleccionada.Coment = txtObs
    End If
    
    Validacion = True
        
End Function

Private Sub sdbcProviCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProviCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProviCod.Rows - 1
            bm = sdbcProviCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcProviCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcProviCod_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset
    m_bRespetarCombo = False
    If sdbcProviCod.Text = "" Then
        Exit Sub
    Else
        If sdbcPaiCod.Text = "" Then
            NoValida (sIdiProvi)
            sdbcProviCod.Text = ""
            Cancel = True
        Else
            Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(sdbcPaiCod.Columns(0).Value, , sdbcProviCod.Text, True)
            If ador.RecordCount = 0 Then
                NoValida (sIdiProvi)
                sdbcProviCod.Text = ""
                Cancel = True
            End If
            ador.Close
            Set ador = Nothing
        End If
    End If
End Sub


Private Sub sdbdCategorias_Click()
Dim bSeDaDeAlta As Boolean
Dim i As Long
Dim bm As Variant
Dim oIdi As CIdioma

    If sdbdCategorias.Columns(0).Value = vbNullString Then Exit Sub

    bSeDaDeAlta = False
    
    m_bRespetar_sdbgCategorias_BeforeRowColChange = True
    'Se comprueba que la categor�a no est� ya en el grid.
    With sdbgCategorias
        sdbgCategorias.MoveFirst
        For i = 0 To sdbgCategorias.Rows - 1
            bm = sdbgCategorias.GetBookmark(i)
            If sdbdCategorias.Columns("ID").Value = sdbgCategorias.Columns("ID").CellText(bm) Then
'                basMensajes.Categoria_Asignada (sdbdCategorias.Columns("DEN_" & g_udtParametrosGenerales.g_sIdioma).Value)
                m_bRespetar_sdbgCategorias_BeforeDelete = True
                sdbgCategorias.SelBookmarks.Add sdbgCategorias.GetBookmark(sdbgCategorias.Rows - 1)
                sdbgCategorias.DeleteSelected
                sdbgCategorias.SelBookmarks.RemoveAll
                m_bRespetar_sdbgCategorias_BeforeDelete = False
                m_bRespetar_sdbgCategorias_BeforeRowColChange = False
                Exit Sub
            End If
        Next i
    End With
    m_bRespetar_sdbgCategorias_BeforeRowColChange = False
    
    sdbgCategorias.MoveLast
    'Se comprueba si la categor�a seleccionada est� dada de baja.
    If CInt(sdbdCategorias.Columns("BAJALOG").Value) = CInt(True) Then
        If basMensajes.DarDeAltaLaCategoria = vbYes Then
            bSeDaDeAlta = True
        Else
            sdbgCategorias.Update
            Exit Sub
        End If
    End If

    With sdbgCategorias
        .Columns("ID").Text = sdbdCategorias.Columns("ID").Text
        .Columns("COD").Text = sdbdCategorias.Columns("COD").Text
        .Columns("COSTEHORA").Text = sdbdCategorias.Columns("COSTEHORA").Text
        If bSeDaDeAlta Then
            .Columns("BAJALOG").Text = CInt(False)
            sdbdCategorias.Columns("BAJALOG").Text = CInt(False)
        Else
            .Columns("BAJALOG").Text = sdbdCategorias.Columns("BAJALOG").Text
        End If
        For Each oIdi In m_oIdiomas
            .Columns("DEN_" & oIdi.Cod).Text = sdbdCategorias.Columns("DEN_" & oIdi.Cod).Text
        Next
    End With
    
    sdbgCategorias.Update

End Sub


Private Sub sdbdCategorias_DropDown()
Dim i As Integer
Dim sLinea As String
Dim oIdi As CIdioma

    'Cargamos la combo de categor�as ordenado por codigo o por den, seg�n la columna en la que nos encontremos
    With sdbdCategorias
        .RemoveAll

        Select Case Left(sdbgCategorias.Columns(sdbgCategorias.Col).Name, 3)
        Case "COD" 'Cargamos ordenado por bajalog-c�digo.
            If m_oCategoriasCod.Count > 0 Then
                For i = 1 To m_oCategoriasCod.Count
                    sLinea = m_oCategoriasCod.Item(i).Id & Chr(9) & _
                        m_oCategoriasCod.Item(i).Codigo & Chr(9) & _
                        m_oCategoriasCod.Item(i).CosteH & Chr(9) & _
                        CInt(m_oCategoriasCod.Item(i).Dact)
                    For Each oIdi In m_oIdiomas
                        sLinea = sLinea & Chr(9) & m_oCategoriasCod.Item(i).Denominaciones.Item(CStr(oIdi.Id)).Den
                    Next
                    .AddItem sLinea
                Next
            End If
        Case "DEN" 'Cargamos ordenado por bajalog-denominaci�n en el idioma del usuario.
            If m_oCategoriasDen.Count > 0 Then
                For i = 1 To m_oCategoriasDen.Count
                    sLinea = m_oCategoriasDen.Item(i).Id & Chr(9) & _
                        m_oCategoriasDen.Item(i).Codigo & Chr(9) & _
                        m_oCategoriasDen.Item(i).CosteH & Chr(9) & _
                        CInt(m_oCategoriasDen.Item(i).Dact)
                    For Each oIdi In m_oIdiomas
                        sLinea = sLinea & Chr(9) & m_oCategoriasDen.Item(i).Denominaciones.Item(CStr(oIdi.Id)).Den
                    Next
                    .AddItem sLinea
                Next
            End If
        End Select
        
        If .Rows = 0 Then .AddItem ""
        
    End With

End Sub

Private Sub sdbdCategorias_InitColumnProps()
    sdbdCategorias.DataFieldList = "Column 1"
    sdbdCategorias.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbdCategorias_PositionList(ByVal Text As String)
Dim bm As Variant
    If sdbgCategorias.Columns(sdbgCategorias.Col).Name = "COD" Then
        bm = BuscarCatCod(EliminarCaracteresEspeciales(Text))
    Else
        bm = BuscarCatDen(EliminarCaracteresEspeciales(Text))
    End If
    If Not IsEmpty(bm) Then sdbdCategorias.Bookmark = bm
    
End Sub


Private Sub sdbdCategorias_RowLoaded(ByVal Bookmark As Variant)
Dim oIdioma As CIdioma
    
    If sdbdCategorias.Columns("BAJALOG").Value <> 0 Then
        sdbdCategorias.Columns("COD").CellStyleSet "Selection"
        For Each oIdioma In m_oIdiomas
            sdbdCategorias.Columns("DEN_" & oIdioma.Cod).CellStyleSet "Selection"
        Next
        sdbdCategorias.Columns("COSTEHORA").CellStyleSet "Selection"
    End If
End Sub

Private Sub sdbdCatUsu_CloseUp()
Dim oError As CTESError
Dim lIdCategoria As Long
Dim ador As adodb.Recordset
    
    If sdbgUsu.Columns("IDCATEGORIA").Text <> sdbdCatUsu.Columns("ID").Text Then
        sdbgUsu.Columns("CATEGORIA").Text = sdbdCatUsu.Columns("DEN").Text
        sdbgUsu.Columns("IDCATEGORIA").Text = sdbdCatUsu.Columns("ID").Text
        If sdbgUsu.Columns("IDCATEGORIA").Text <> "" Then
            Set oError = g_oCiaSeleccionada.Usuarios.Item(CStr(sdbgUsu.Columns("ID").Value)).AsignarCategoria(g_oCiaSeleccionada.CodGS, sdbdCatUsu.Columns("ID").Value)
        End If
    End If
    
    sdbgUsu.Update
End Sub


Private Sub sdbdCatUsu_DropDown()
Dim i As Integer
Dim sLinea As String
Dim oIdi As CIdioma

    If sdbgUsu.Columns("CATEGORIA").Locked = True Then
        sdbdCatUsu.DroppedDown = False
        Exit Sub
    End If

    With sdbdCatUsu
        .RemoveAll
        If Not m_oCategoriasProve Is Nothing Then
            If m_oCategoriasProve.Count > 0 Then
                For i = 1 To m_oCategoriasProve.Count
                    sLinea = m_oCategoriasProve.Item(i).Id & Chr(9) & _
                        m_oCategoriasProve.Item(i).Codigo & Chr(9) & _
                        m_oCategoriasProve.Item(i).Denominaciones.Item(CStr(g_udtParametrosGenerales.g_iIdIdioma)).Den & Chr(9) & _
                        m_oCategoriasProve.Item(i).CosteH & Chr(9)
                    For Each oIdi In m_oIdiomas
                        sLinea = sLinea & Chr(9) & m_oCategoriasProve.Item(i).Denominaciones.Item(CStr(oIdi.Id)).Den
                    Next
                    If CInt(m_oCategoriasProve.Item(i).Dact) = 0 Then  'Solo se cargan en el combo los que no est�n dados de baja en el proveedor.
                        .AddItem sLinea
                    End If
                Next
            End If
        End If
        
        If .Rows = 0 Then .AddItem ""
    End With

End Sub

Private Sub sdbdCatUsu_InitColumnProps()
    sdbdCatUsu.DataFieldList = "Column 2"
    sdbdCatUsu.DataFieldToDisplay = "Column 2"
End Sub

Private Sub sdbdCatUsu_PositionList(ByVal Text As String)
Dim bm As Variant
    If Text = "" Then Exit Sub
    
    bm = BuscarCatUsu(EliminarCaracteresEspeciales(Text))
    If Not IsEmpty(bm) Then sdbdCatUsu.Bookmark = bm
End Sub



Private Sub sdbgCategorias_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
Dim i As Integer
Dim oCategoria As CCategoria
Dim iRespuesta As Integer
Dim iElim As Integer
Dim iElimSis As Integer
Dim bEliminar As Boolean

    DispPromptMsg = 0
    
    If m_bRespetar_sdbgCategorias_BeforeDelete Then Exit Sub
    
    i = 0
    
    While i < sdbgCategorias.SelBookmarks.Count
        sdbgCategorias.Bookmark = sdbgCategorias.SelBookmarks(i)
        
        bEliminar = True
        iElim = 0
        iElimSis = 0
        
        If sdbgCategorias.Columns("ID").Value <> "" Then
        
            Set oCategoria = g_oRaiz.Generar_CCategoria
            oCategoria.Id = sdbgCategorias.Columns("ID").Value
            If oCategoria.EstaAsociadaAUsuariosDelProve(g_oCiaSeleccionada.CodGS) Then 'Est� asociada a alg�n usuario del proveedor.
                iRespuesta = basMensajes.CategoriaAsociadaAUsusProve_Eliminar(m_oCategoriasProve.Item(CStr(oCategoria.Id)).Denominaciones.Item(CStr(g_udtParametrosGenerales.g_iIdIdioma)).Den)
                If iRespuesta = vbNo Then
                    sdbgCategorias.SelBookmarks.Remove (i)
                    bEliminar = False
                End If
            End If
            
            If bEliminar Then
                i = i + 1
                
                'Comprobar que existe la categor�a en la col. de categor�as del proveedor. Si no es as� es el caso de una
                'categor�a que no se ha dado de alta todav�a (sin dar a Aceptar) y no es necesario eliminarla
                If Not m_oCategoriasProve.Item(CStr(oCategoria.Id)) Is Nothing Then
                    iElim = 2 'ELIM=2 (eliminar) Flag que indica si hay que eliminar la asignaci�n de la categor�a al proveedor
                    If Not oCategoria.EstaAsociadaOUsando(g_oCiaSeleccionada.CodGS) Then
                        iRespuesta = basMensajes.Categoria_Eliminar(m_oCategoriasProve.Item(CStr(oCategoria.Id)).Denominaciones.Item(CStr(g_udtParametrosGenerales.g_iIdIdioma)).Den)
                        If iRespuesta = vbNo Then  'Se quiere conservar en el repositorio.
                            iElimSis = 0 'ELIMSIS=0 (nada) Flag que indica si hay que eliminar la categor�a de la tabla categor�as
                        Else    'Se quiere eliminar del repositorio de categorias.
                            iElimSis = 2 'ELIMSIS=2 (eliminar) Flag que indica si hay que eliminar la categor�a de la tabla categor�as
                        End If
                    Else
                        iElimSis = 0
                    End If
                    
                    'Se guarda en una colecci�n las categorias eliminadas.
                    g_oCiaSeleccionada.Categoriaselim.Add m_oCategoriasCod.Item(CStr(oCategoria.Id)).Id, m_oCategoriasCod.Item(CStr(oCategoria.Id)).Codigo, m_oCategoriasCod.Item(CStr(oCategoria.Id)).Denominaciones, m_oCategoriasCod.Item(CStr(oCategoria.Id)).CosteH, m_oCategoriasCod.Item(CStr(oCategoria.Id)).Dact, iElim, iElimSis
                End If
            End If
            
            Set oCategoria = Nothing
            
        Else
            i = i + 1
        End If
        
    Wend
    
    m_bRespetar_sdbgCategorias_BeforeDelete = True
    sdbgCategorias.DeleteSelected
    m_bRespetar_sdbgCategorias_BeforeDelete = False
    
    sdbgCategorias.SelBookmarks.RemoveAll
    

End Sub


Private Sub sdbgCategorias_BeforeRowColChange(Cancel As Integer)
Dim sDen As String
Dim oIdi As CIdioma

    If m_bRespetar_sdbgCategorias_BeforeRowColChange Then Exit Sub

    With sdbgCategorias
        If .Col >= 0 Then
            Select Case Left(.Columns(.Col).Name, 3)
                Case "COD"
                    If .Columns("COD").Value <> "" Then
                        If .Columns("COD").Value = sdbdCategorias.Columns("COD").Value Then
                            For Each oIdi In m_oIdiomas
                                .Columns("DEN_" & oIdi.Cod).Value = sdbdCategorias.Columns("DEN_" & oIdi.Cod).Value
                            Next
                            .Columns("COSTEHORA").Value = sdbdCategorias.Columns("COSTEHORA").Value
                        End If
                    End If
                Case "DEN"
                   sDen = .Columns(.Col).Value
                    If Trim(sDen) = "" Then
                        Exit Sub
                    End If
                    For Each oIdi In m_oIdiomas
                        If .Columns("DEN_" & oIdi.Cod).Value = "" Then
                            .Columns("DEN_" & oIdi.Cod).Value = sDen
                        End If
                    Next
            End Select
        End If
    End With
End Sub

Private Sub sdbgCategorias_BeforeUpdate(Cancel As Integer)
Dim oIdi As CIdioma

    If m_bRespetar_sdbgCategorias_BeforeRowColChange Then Exit Sub

    With sdbgCategorias
        If .Col >= 0 Then
            If Trim(.Columns("COD").Value) = "" Then
                m_bAceptarCategorias = False
                basMensajes.AvisoCodCategoria
                Cancel = True
                .SetFocus
                Exit Sub
            End If
            For Each oIdi In m_oIdiomas
                If Trim(.Columns("DEN_" & oIdi.Cod).Value) = "" Then
                    m_bAceptarCategorias = False
                    basMensajes.AvisoDenCategoria
                    Cancel = True
                    .SetFocus
                    Exit Sub
                End If
            Next
            If Trim(.Columns("COSTEHORA").Value) = "" Or Not IsNumeric(.Columns("COSTEHORA").Value) Then
                m_bAceptarCategorias = False
                basMensajes.AvisoCosteHoraCategoria
                Cancel = True
                .SetFocus
                Exit Sub
            End If
        End If
    End With

End Sub


Private Sub sdbgCategorias_Change()
Dim sCat As String
Dim bm As Variant
Dim oCategoria As CCategoria
Dim iRespuesta As Integer
Dim iElim As Integer
Dim iElimSis As Integer
Dim lId As Long
    
    With sdbgCategorias
        Select Case Left(.Columns(.Col).Name, 3)
            Case "COD"
                If Not sdbdCategorias.DroppedDown Then SendKeys "{F4}"
                sCat = .Columns("COD").Text
                sCat = EliminarCaracteresEspeciales(sCat)
                bm = BuscarCatCod(sCat)
                sdbdCategorias.Bookmark = bm
            Case "DEN"
                If Not sdbdCategorias.DroppedDown Then SendKeys "{F4}"
                sCat = .Columns(.Columns(.Col).Name).Text
                sCat = EliminarCaracteresEspeciales(sCat)
                bm = BuscarCatDen(sCat)
                sdbdCategorias.Bookmark = bm
            Case "BAJ"
                If sdbgCategorias.Columns("ID").Value <> vbNullString Then
                    If sdbgCategorias.Columns(.Col).Value = 0 Then
                        'Se deshace la baja l�gica
                        
                        'Se mira si anteriormente se hab�a dado de baja (es decir si est� en CategoriasElim)
                        lId = CLng(sdbgCategorias.Columns("ID").Value)
                        If Not g_oCiaSeleccionada.Categoriaselim Is Nothing Then
                            If Not g_oCiaSeleccionada.Categoriaselim.Item(CStr(lId)) Is Nothing Then 'Se elimina de la colecci�n y listo.
                                g_oCiaSeleccionada.Categoriaselim.Remove (CStr(lId))
                                sdbgCategorias.Update
                                Exit Sub
                            End If
                        End If
                        
                        iElim = 3   'ELIM=3 (se deshace la baja log)
                        iElimSis = 3    'ELIMSIS=3 (se deshace la baja log en el repositorio)
                        
                        'Se guarda en una colecci�n las categorias eliminadas.
                        g_oCiaSeleccionada.Categoriaselim.Add m_oCategoriasCod.Item(CStr(lId)).Id, m_oCategoriasCod.Item(CStr(lId)).Codigo, m_oCategoriasCod.Item(CStr(lId)).Denominaciones, m_oCategoriasCod.Item(CStr(lId)).CosteH, m_oCategoriasCod.Item(CStr(lId)).Dact, iElim, iElimSis
                        
                        
                    Else
                        'Se da de baja l�gica
                        iElim = 0
                        iElimSis = 0
                        
                        lId = CLng(sdbgCategorias.Columns("ID").Value)
                        
                        'Se mira si anteriormente se hab�a dado de alta (es decir si est� en CategoriasElim con Elim=3)
                        If Not g_oCiaSeleccionada.Categoriaselim Is Nothing Then
                            If Not g_oCiaSeleccionada.Categoriaselim.Item(CStr(lId)) Is Nothing Then 'Se elimina de la colecci�n.
                                g_oCiaSeleccionada.Categoriaselim.Remove (CStr(lId))
                                sdbgCategorias.Update
                                Exit Sub
                            End If
                        End If
                        
                        Set oCategoria = g_oRaiz.Generar_CCategoria
                        oCategoria.Id = lId
                        If oCategoria.EstaAsociadaAUsuariosDelProve(g_oCiaSeleccionada.CodGS) Then 'Est� asociada a alg�n usuario del proveedor.
                            iRespuesta = basMensajes.CategoriaAsociadaAUsusProve_BajaLog(sdbgCategorias.Columns("DEN_" & g_udtParametrosGenerales.g_sIdioma).Value)
                            If iRespuesta = vbNo Then
                                sdbgCategorias.Columns(.Col).Value = 0
                                sdbgCategorias.Update
                                Exit Sub
                            End If
                        End If
                        
                        iElim = 1 'ELIM=1 (baja log) Flag que indica si hay que eliminar la asignaci�n de la categor�a al proveedor
                        If Not oCategoria.EstaAsociadaOUsando(g_oCiaSeleccionada.CodGS) Then
                            iRespuesta = basMensajes.Categoria_BajaLog(sdbgCategorias.Columns("DEN_" & g_udtParametrosGenerales.g_sIdioma).Value)
                            If iRespuesta = vbNo Then  'Se quiere conservar en el repositorio.
                                iElimSis = 0 'ELIMSIS=0 (nada) Flag que indica si hay que eliminar la categor�a de la tabla categor�as
                            Else    'Se quiere dar de baja l�gica en el repositorio de categorias.
                                iElimSis = 1 'ELIMSIS=1 (baja log) Flag que indica si hay que eliminar la categor�a de la tabla categor�as
                            End If
                        Else
                            iElimSis = 0
                        End If
                        
                        'Se guarda en una colecci�n las categorias eliminadas.
                        g_oCiaSeleccionada.Categoriaselim.Add m_oCategoriasCod.Item(CStr(oCategoria.Id)).Id, m_oCategoriasCod.Item(CStr(oCategoria.Id)).Codigo, m_oCategoriasCod.Item(CStr(oCategoria.Id)).Denominaciones, m_oCategoriasCod.Item(CStr(oCategoria.Id)).CosteH, m_oCategoriasCod.Item(CStr(oCategoria.Id)).Dact, iElim, iElimSis
                        
                        Set oCategoria = Nothing
                    End If
                
                    sdbgCategorias.Update
                
                End If
        End Select
    End With
    
End Sub


Private Sub sdbgCategorias_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    With sdbgCategorias
        If .Col >= 0 Then
            Select Case Left(.Columns(.Col).Name, 3)
                Case "COD"
                    sdbdCategorias.DataFieldList = "Column 1"
                    sdbdCategorias.DataFieldToDisplay = "Column 1"
                    sdbdCategorias_DropDown
                Case "DEN"
                    
                    sdbdCategorias.DataFieldList = "Column " & .Col
                    sdbdCategorias.DataFieldToDisplay = "Column " & .Col
                    sdbdCategorias_DropDown
            End Select
        End If
    End With
End Sub

Private Sub sdbgCategorias_RowLoaded(ByVal Bookmark As Variant)
Dim oIdioma As CIdioma
    
    If sdbgCategorias.Columns("BAJALOG").Value <> 0 And sdbgCategorias.Columns("BAJALOG").Value <> "" Then
        sdbgCategorias.Columns("COD").CellStyleSet "Selection"
        For Each oIdioma In m_oIdiomas
            sdbgCategorias.Columns("DEN_" & oIdioma.Cod).CellStyleSet "Selection"
        Next
        sdbgCategorias.Columns("COSTEHORA").CellStyleSet "Selection"
        sdbgCategorias.Columns("BAJALOG").CellStyleSet "Selection"
    End If
End Sub


Private Sub sdbgCiasComp_Change()
    Dim teserror As CTESError
    Dim i As Integer
    Dim IdComp As Long
    Dim vEnlace As CEnlace
    Dim iRespuesta As Integer
    
    
    IdComp = sdbgCiasComp.Columns("ID").Value
    
    If sdbgCiasComp.ActiveCell.Value Then
        Set teserror = g_oCiaSeleccionada.AnyadirRegistro(IdComp)
    Else
        Screen.MousePointer = vbHourglass
        If basMensajes.PreguntaDesregistrarEnCompradora() = vbYes Then
            Set vEnlace = g_oCiaSeleccionada.DevolverEnlaceConCiaComp(IdComp)
            If Not vEnlace Is Nothing Then
                iRespuesta = basMensajes.PreguntaEliminarEnlaceComp(sdbgCiasComp.Columns("DEN").Value, g_oCiaSeleccionada.Den)
                If iRespuesta = vbYes Then
                   frmEsperaPortal.Show
                   DoEvents
                   Set teserror = vEnlace.EliminarEnlace
                   Unload frmEsperaPortal
                   
                   Screen.MousePointer = vbNormal
                   
                   If teserror.NumError <> TESnoerror Then
                       basErrores.TratarError teserror
                        sdbgCiasComp.ActiveCell.Value = Not sdbgCiasComp.ActiveCell.Value
                       Exit Sub
                   End If
                    Set teserror = g_oCiaSeleccionada.EliminarRegistro(IdComp)
                Else
                    sdbgCiasComp.ActiveCell.Value = Not sdbgCiasComp.ActiveCell.Value
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            Else
                Set teserror = g_oCiaSeleccionada.EliminarRegistro(IdComp)
            End If
        Else
            sdbgCiasComp.ActiveCell.Value = Not sdbgCiasComp.ActiveCell.Value
        End If
    End If
    sdbgCiasComp.Columns("SOLICITADA").Value = sdbgCiasComp.Columns("REGISTRADA").Value
    sdbgCiasComp.Update
    
    Screen.MousePointer = vbNormal

End Sub



Private Sub sdbgUsu_Click()
    If sdbgUsu.Columns("FPAUT").Value = False Then
        sdbgUsu.Columns("CATEGORIA").Locked = True
    Else
        sdbgUsu.Columns("CATEGORIA").Locked = False
    End If
End Sub

Private Sub sdbgUsu_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    With sdbgUsu
        If .Col >= 0 Then
            Select Case .Columns(.Col).Name
                Case "CATEGORIA"
                    sdbdCatUsu_DropDown
            End Select
        End If
    End With
End Sub

Private Sub sdbgUsu_RowLoaded(ByVal Bookmark As Variant)
    
    'Funci�n compradora
    If g_oCiaSeleccionada.EstadoCompradora = 3 Then
        If sdbgUsu.Columns("COMP").Value = 1 Then
            sdbgUsu.Columns("FCAUT").CellStyleSet m_AMARILLO
        ElseIf sdbgUsu.Columns("COMP").Value = 2 Then
            sdbgUsu.Columns("FCAUT").CellStyleSet m_ROJO
        ElseIf sdbgUsu.Columns("COMP").Value = 3 Then
            sdbgUsu.Columns("FCAUT").CellStyleSet m_VERDE
        End If
    End If
    
    'funci�n proveedora
    If g_oCiaSeleccionada.EstadoProveedora = 3 Then
        If sdbgUsu.Columns("PROV").Value = 1 Then
            sdbgUsu.Columns("FPAUT").CellStyleSet m_AMARILLO
        ElseIf sdbgUsu.Columns("PROV").Value = 2 Then
            sdbgUsu.Columns("FPAUT").CellStyleSet m_ROJO
        ElseIf sdbgUsu.Columns("PROV").Value = 3 Then
            sdbgUsu.Columns("FPAUT").CellStyleSet m_VERDE
        End If
    End If
    
    'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
    If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
        If sdbgUsu.Columns("ACCESOFSGA").Value <> 0 Then
            sdbgUsu.Columns("ACCESOFSGA").CellStyleSet m_VERDE
        End If
    End If
        
    
'    If g_oCiaSeleccionada.UsuarioPrincipal = sdbgUsu.Columns("ID").Value Then
'        sdbgUsu.Columns("PPAL").Value = 1
'    End If
            
End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    
    If g_oCiaSeleccionada Is Nothing Then Exit Sub
        
    Select Case SSTab1.Tab
        Case 0
                cmdAceptar.Enabled = True
                cmdA�adir.Enabled = True
                cmdModificar.Enabled = True
                cmdCodigo.Enabled = True
                cmdEliminar.Enabled = True
                cmdRestaurar0.Enabled = True
                cmdListado.Enabled = True
                
                cmdRestaurar2.Enabled = False
                cmdConsulta1.Enabled = False
                cmdEditar(1).Enabled = False
                
                cmdRestaurar1.Enabled = False
                cmdConsulta0.Enabled = False
                cmdEditar(0).Enabled = False
                
                cmdA�adirUsu.Enabled = False
                cmdModificarUsu.Enabled = False
                cmdEliminarUsu.Enabled = False
        Case 1
            If m_bModifCia Then
                'modificaci�n
                cmdAceptar.Enabled = False
                cmdA�adir.Enabled = False
                cmdModificar.Enabled = False
                cmdCodigo.Enabled = False
                cmdEliminar.Enabled = False
                cmdRestaurar0.Enabled = False
                cmdListado.Enabled = False
                
                cmdRestaurar2.Enabled = False
                cmdConsulta1.Enabled = False
                cmdEditar(1).Enabled = False
                
                cmdRestaurar1.Enabled = True
                cmdConsulta0.Enabled = True
                cmdEditar(0).Enabled = True
                
                cmdA�adirUsu.Enabled = False
                cmdModificarUsu.Enabled = False
                cmdEliminarUsu.Enabled = False
                
                
                ModifActiv
            Else
                'consulta
                cmdAceptar.Enabled = False
                cmdA�adir.Enabled = False
                cmdModificar.Enabled = False
                cmdCodigo.Enabled = False
                cmdEliminar.Enabled = False
                cmdRestaurar0.Enabled = False
                cmdListado.Enabled = False
                
                cmdRestaurar1.Enabled = True
                cmdConsulta0.Enabled = True
                cmdEditar(0).Enabled = True
                
                cmdRestaurar2.Enabled = False
                cmdConsulta1.Enabled = False
                cmdEditar(1).Enabled = False
                
                cmdA�adirUsu.Enabled = False
                cmdModificarUsu.Enabled = False
                cmdEliminarUsu.Enabled = False
                RestaurarActiv
            End If
        Case 2
                cmdAceptar.Enabled = False
                cmdA�adir.Enabled = False
                cmdModificar.Enabled = False
                cmdCodigo.Enabled = False
                cmdEliminar.Enabled = False
                cmdRestaurar0.Enabled = False
                cmdListado.Enabled = False
                
                cmdRestaurar1.Enabled = False
                cmdConsulta0.Enabled = False
                cmdEditar(0).Enabled = False
                
                cmdRestaurar2.Enabled = True
                cmdConsulta1.Enabled = True
                cmdEditar(1).Enabled = True
                
                cmdA�adirUsu.Enabled = False
                cmdModificarUsu.Enabled = False
                cmdEliminarUsu.Enabled = False
        Case 3
                cmdAceptar.Enabled = False
                cmdA�adir.Enabled = False
                cmdModificar.Enabled = False
                cmdCodigo.Enabled = False
                cmdEliminar.Enabled = False
                cmdRestaurar0.Enabled = False
                cmdListado.Enabled = False
                
                cmdRestaurar1.Enabled = False
                cmdConsulta0.Enabled = False
                cmdEditar(0).Enabled = False
                
                cmdRestaurar2.Enabled = False
                cmdConsulta1.Enabled = False
                cmdEditar(1).Enabled = False
                
                cmdA�adirUsu.Enabled = True
                cmdModificarUsu.Enabled = True
                cmdEliminarUsu.Enabled = True
                CargarUsuariosCia

                'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
                If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                    sdbgUsu.Columns("CATEGORIA").Visible = True
                    sdbgUsu.Columns("ACCESOFSGA").Visible = True
                    sdbgUsu.Columns("COD").Width = sdbgUsu.Width * 0.11
                    sdbgUsu.Columns("NOM").Width = sdbgUsu.Width * 0.14
                    sdbgUsu.Columns("APE").Width = sdbgUsu.Width * 0.17
                    sdbgUsu.Columns("FCAUT").Width = sdbgUsu.Width * 0.0945
                    sdbgUsu.Columns("FPAUT").Width = sdbgUsu.Width * 0.12
                    sdbgUsu.Columns("PPAL").Width = sdbgUsu.Width * 0.07
                    sdbgUsu.Columns("CATEGORIA").Width = sdbgUsu.Width * 0.23
                    sdbgUsu.Columns("ACCESOFSGA").Width = sdbgUsu.Width * 0.12
                Else
                    sdbgUsu.Columns("CATEGORIA").Visible = False
                    sdbgUsu.Columns("ACCESOFSGA").Visible = False
                    sdbgUsu.Columns("COD").Width = sdbgUsu.Width * 0.1497
                    sdbgUsu.Columns("NOM").Width = sdbgUsu.Width * 0.25
                    sdbgUsu.Columns("APE").Width = sdbgUsu.Width * 0.32
                    sdbgUsu.Columns("FCAUT").Width = sdbgUsu.Width * 0.0945
                    sdbgUsu.Columns("FPAUT").Width = sdbgUsu.Width * 0.12
                    sdbgUsu.Columns("PPAL").Width = sdbgUsu.Width * 0.12
                End If
        
        Case 4
                CargarRegistroCompradoras
    
        Case 5
                picEsp.Left = lstvwEsp.Width - (cmdAbrirEsp.Width * 6) - 50
            
        Case 6
                cmdRestaurarCat_Click
                Arrange
    End Select
    
End Sub

'**********************************************************************************************
'*** Descripci�n: Cancela la acci�n en curso.                                             *****
'*** Par�metros:                                                                          *****
'*** Valor que devuelve:                                                                  *****
'**********************************************************************************************

Private Sub cmdCancelar_Click()
    m_bAnyadirCia = False
    
    If m_bModifCia Then
        BloquearDatosProve False
        cmdRestaurar0_click
        m_bModifCia = False
    Else
        LimpiarDatosCia
    End If
    
    ModoConsultaGen
'    m_bActividadModif = False
'    fraCias.Enabled = True
'    picDatos.Enabled = False
'    picNavigate.Visible = True
'    picEdit.Enabled = False
'    picEdit.Visible = False
'    txtObs.Enabled = False
    
    'Usuarios
    'sdbgUsu.ActiveRowStyleSet = "Normal"
'    sdbgUsu.AllowDelete = False
'    sdbgUsu.AllowUpdate = False
'    sdbgUsu.AllowAddNew = False
'    picEditUsu.Visible = False
    
End Sub

Public Sub GenerarEstructuraActividadesConsul(ByVal bOrdenadoPorDen As Boolean)
    Dim oActividadesNivel1 As CActividadesNivel1
    Dim oActividadesNivel2 As FSPSServer.CActividadesNivel2
    Dim oActividadesNivel3 As FSPSServer.CActividadesNivel3
    Dim oActividadesNivel4 As FSPSServer.CActividadesNivel4
    
    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Dim nodx As MSComctlLib.Node
    
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    oActividadesNivel1.GenerarEstructuraActividades False, g_udtParametrosGenerales.g_sIdioma

    tvwEstrMatConsul.Nodes.Clear

    Set nodx = tvwEstrMatConsul.Nodes.Add(, , "Raiz", sIdiAct, "Raiz2")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    If oActividadesNivel1 Is Nothing Then Exit Sub
    For Each oACN1 In oActividadesNivel1
     
        scod1 = oACN1.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
            
        Set nodx = tvwEstrMatConsul.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACT")
        nodx.Tag = "ACN1" & oACN1.Id
        
        For Each oACN2 In oACN1.ActividadesNivel2
            scod2 = oACN2.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
            Set nodx = tvwEstrMatConsul.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACT")
            nodx.Tag = "ACN2" & oACN2.Id
            For Each oACN3 In oACN2.ActividadesNivel3
                scod3 = oACN3.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
                Set nodx = tvwEstrMatConsul.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACT")
                nodx.Tag = "ACN3" & oACN3.Id
                For Each oACN4 In oACN3.ActividadesNivel4
                    scod4 = oACN4.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
                    Set nodx = tvwEstrMatConsul.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACT")
                    nodx.Tag = "ACN4" & oACN4.Id ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    For Each oACN5 In oACN4.ActividadesNivel5
                        scod5 = oACN5.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
                        Set nodx = tvwEstrMatConsul.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & scod5, oACN5.Cod & " - " & oACN5.Den, "ACT")
                        nodx.Tag = "ACN5" & oACN5.Id ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    Next
                Next
            Next
    
        Next
    Next
        
Exit Sub

error:
    Set nodx = Nothing
    Resume Next


End Sub

Public Sub GenerarEstructuraActividades(ByVal bOrdenadoPorDen As Boolean)

    Dim oActividadesNivel1 As CActividadesNivel1
    Dim oActividadesNivel2 As FSPSServer.CActividadesNivel2
    Dim oActividadesNivel3 As FSPSServer.CActividadesNivel3
    Dim oActividadesNivel4 As FSPSServer.CActividadesNivel4
    
    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Dim nodx As MSComctlLib.Node
    
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    oActividadesNivel1.GenerarEstructuraActividades False, g_udtParametrosGenerales.g_sIdioma
    
    tvwEstrMat.Nodes.Clear
    
    'Genera la ra�z del �rbol
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdiAct, "Raiz2")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
   
     If oActividadesNivel1 Is Nothing Then Exit Sub
     For Each oACN1 In oActividadesNivel1
        
        scod1 = oACN1.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
        
        Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACT")
        nodx.Tag = "ACN1" & oACN1.Id
        
        For Each oACN2 In oACN1.ActividadesNivel2
            scod2 = oACN2.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
            Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACT")
            nodx.Tag = "ACN2" & oACN2.Id
            For Each oACN3 In oACN2.ActividadesNivel3
                scod3 = oACN3.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
                Set nodx = tvwEstrMat.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACT")
                nodx.Tag = "ACN3" & oACN3.Id
                For Each oACN4 In oACN3.ActividadesNivel4
                    scod4 = oACN4.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
                    Set nodx = tvwEstrMat.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACT")
                    nodx.Tag = "ACN4" & oACN4.Id ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    For Each oACN5 In oACN4.ActividadesNivel5
                        scod5 = oACN5.Id & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
                        Set nodx = tvwEstrMat.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & scod5, oACN5.Cod & " - " & oACN5.Den, "ACT")
                        nodx.Tag = "ACN5" & oACN5.Id ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    Next
                Next
            Next
    
        Next
    Next
       
        
Exit Sub

error:
    Set nodx = Nothing
    Resume Next
End Sub

Public Sub GenerarEstructuraActividadesCia()

    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Dim nodx As MSComctlLib.Node

    If g_oCiaSeleccionada Is Nothing Then Exit Sub
    'De la estructura cargada se marcan las actividades que est�n seleccionadas
                
    Set m_oActsN1 = g_oCiaSeleccionada.DevolverEstructuraActividades("SPA")
    
    If Not m_oActsN1 Is Nothing Then
    
        For Each oACN1 In m_oActsN1
            scod1 = CStr(oACN1.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
            tvwEstrMat.Nodes.Item("ACN1" & scod1).Checked = True
        Next
    End If
    
     Set m_oActsN2 = g_oCiaSeleccionada.oActividadesNivel2
    
    If Not m_oActsN2 Is Nothing Then
    
        For Each oACN2 In m_oActsN2
            scod1 = CStr(oACN2.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.ACN1)))
            scod2 = CStr(oACN2.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
            
            tvwEstrMat.Nodes.Item("ACN2" & scod1 & scod2).Checked = True
            If (tvwEstrMat.Nodes.Item("ACN1" & scod1).Checked = False) Then
                tvwEstrMat.Nodes.Item("ACN1" & scod1).Expanded = True
            End If
            
            
        Next
    End If
    
     Set m_oActsN3 = g_oCiaSeleccionada.oActividadesNivel3
    
    
    If Not m_oActsN3 Is Nothing Then
        For Each oACN3 In m_oActsN3
            scod1 = CStr(oACN3.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.ACN1)))
            scod2 = CStr(oACN3.ACN2) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.ACN2)))
            scod3 = CStr(oACN3.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
            
            tvwEstrMat.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Checked = True
            If tvwEstrMat.Nodes.Item("ACN2" & scod1 & scod2).Checked = False Then
                tvwEstrMat.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                tvwEstrMat.Nodes.Item("ACN1" & scod1).Expanded = True
                
            End If
        Next
    End If
    
     Set m_oActsN4 = g_oCiaSeleccionada.oActividadesNivel4
    
    
    If Not m_oActsN4 Is Nothing Then
        For Each oACN4 In m_oActsN4
            scod1 = CStr(oACN4.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ACN1)))
            scod2 = CStr(oACN4.ACN2) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ACN2)))
            scod3 = CStr(oACN4.ACN3) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ACN3)))
            scod4 = CStr(oACN4.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
            tvwEstrMat.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Checked = True
            If tvwEstrMat.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Checked = False Then
                tvwEstrMat.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = True
                tvwEstrMat.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                tvwEstrMat.Nodes.Item("ACN1" & scod1).Expanded = True
            End If
        Next
    End If
    
     Set m_oActsN5 = g_oCiaSeleccionada.oActividadesNivel5
    
    
    If Not m_oActsN5 Is Nothing Then
    
        For Each oACN5 In m_oActsN5
            scod1 = CStr(oACN5.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN1)))
            scod2 = CStr(oACN5.ACN2) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN2)))
            scod3 = CStr(oACN5.ACN3) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN3)))
            scod4 = CStr(oACN5.ACN4) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN4)))
            scod5 = CStr(oACN5.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
            
            tvwEstrMat.Nodes.Item("ACN5" & scod1 & scod2 & scod3 & scod4 & scod5).Checked = True
            If tvwEstrMat.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Checked = False Then
                tvwEstrMat.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Expanded = True
                tvwEstrMat.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = True
                tvwEstrMat.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                tvwEstrMat.Nodes.Item("ACN1" & scod1).Expanded = True
            End If
            
        Next
    End If
    
    
Exit Sub

error:
    Set nodx = Nothing
    Resume Next
    
End Sub


Private Sub GenerarEstructuraActividadesCiaConsul()

    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
    Dim nodx As MSComctlLib.Node

    If g_oCiaSeleccionada Is Nothing Then Exit Sub
    
    'Se marcan las actividades que est�n seleccionadas para la compa��a
    
    Set m_oActsN1 = g_oCiaSeleccionada.DevolverEstructuraActividades("SPA")
    If Not m_oActsN1 Is Nothing Then
        For Each oACN1 In m_oActsN1
            scod1 = CStr(oACN1.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
            tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).image = "ACTASIG"
        Next
    End If
    
    
  Set m_oActsN2 = g_oCiaSeleccionada.oActividadesNivel2
    If Not m_oActsN2 Is Nothing Then
        For Each oACN2 In m_oActsN2
            scod1 = CStr(oACN2.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.ACN1)))
            scod2 = CStr(oACN2.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
            
            tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).image = "ACTASIG"
            If tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = False
            Else
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
            End If

         Next
    End If
    
    Set m_oActsN3 = g_oCiaSeleccionada.oActividadesNivel3
    If Not m_oActsN3 Is Nothing Then
        For Each oACN3 In m_oActsN3
            scod1 = CStr(oACN3.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.ACN1)))
            scod2 = CStr(oACN3.ACN2) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.ACN2)))
            scod3 = CStr(oACN3.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
            tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).image = "ACTASIG"
            
            If tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = False
            ElseIf tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = False
            Else
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
            End If

        Next
    End If
    
    Set m_oActsN4 = g_oCiaSeleccionada.oActividadesNivel4
    If Not m_oActsN4 Is Nothing Then
        For Each oACN4 In m_oActsN4
            scod1 = CStr(oACN4.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ACN1)))
            scod2 = CStr(oACN4.ACN2) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ACN2)))
            scod3 = CStr(oACN4.ACN3) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ACN3)))
            scod4 = CStr(oACN4.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
            tvwEstrMatConsul.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).image = "ACTASIG"
            
            If tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = False
            ElseIf tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = False
            ElseIf tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = False
            Else
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = True
            End If
        Next
    End If
  Set m_oActsN5 = g_oCiaSeleccionada.oActividadesNivel5
    
    
    If Not m_oActsN5 Is Nothing Then
    
        For Each oACN5 In m_oActsN5
            scod1 = CStr(oACN5.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN1)))
            scod2 = CStr(oACN5.ACN2) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN2)))
            scod3 = CStr(oACN5.ACN3) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN3)))
            scod4 = CStr(oACN5.ACN4) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN4)))
            scod5 = CStr(oACN5.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
            
            tvwEstrMatConsul.Nodes.Item("ACN5" & scod1 & scod2 & scod3 & scod4 & scod5).image = "ACTASIG"
            
            If tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Expanded = False
            ElseIf tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Expanded = False
            ElseIf tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = False
                tvwEstrMatConsul.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Expanded = False
            ElseIf tvwEstrMatConsul.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).image = "ACTASIG" Then
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Expanded = False
            Else
                tvwEstrMatConsul.Nodes.Item("ACN1" & scod1).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN2" & scod1 & scod2).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN3" & scod1 & scod2 & scod3).Expanded = True
                tvwEstrMatConsul.Nodes.Item("ACN4" & scod1 & scod2 & scod3 & scod4).Expanded = True
            End If
        Next
    End If

Exit Sub

error:
    Set nodx = Nothing
    Resume Next
End Sub

Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim nod5 As MSComctlLib.Node

    Select Case Left(nodx.Tag, 4)
    
        Case "ACN1"
                               
            Set nod1 = nodx.Child
       
            While Not nod1 Is Nothing
                
                nod1.Checked = True
                Set nod2 = nod1.Child
                
                While Not nod2 Is Nothing
                    nod2.Checked = True
                    Set nod3 = nod2.Child
                    
                    While Not nod3 Is Nothing
                        nod3.Checked = True
                        Set nod4 = nod3.Child
                        
                        While Not nod4 Is Nothing
                            nod4.Checked = True
                            Set nod5 = nod4.Child
                            
                            While Not nod5 Is Nothing
                                nod5.Checked = True
                                Set nod5 = nod5.Next
                            Wend
                            
                            Set nod4 = nod4.Next
                            
                        Wend
                        Set nod3 = nod3.Next
                    Wend
                    Set nod2 = nod2.Next
                Wend
                Set nod1 = nod1.Next
             Wend
                
        DoEvents
        Case "ACN2"
            
            Set nod2 = nodx.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = True
                Set nod3 = nod2.Child
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod5 = nod4.Child
                        While Not nod5 Is Nothing
                            nod5.Checked = True
                            Set nod5 = nod5.Next
                        Wend
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
        
        Case "ACN3"
                
                Set nod3 = nodx.Child
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod5 = nod4.Child
                        While Not nod5 Is Nothing
                            nod5.Checked = True
                            Set nod5 = nod5.Next
                        Wend
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                
                
        Case "ACN4"
                
                    Set nod4 = nodx.Child
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod5 = nod4.Child
                        While Not nod5 Is Nothing
                            nod5.Checked = True
                            Set nod5 = nod5.Next
                        Wend
                        Set nod4 = nod4.Next
                    Wend
                    
        Case "ACN5"
                
                    Set nod5 = nodx.Child
                    While Not nod5 Is Nothing
                        nod5.Checked = True
                        Set nod5 = nod5.Next
                    Wend
                
                    
    End Select

End Sub

Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nodx1 As MSComctlLib.Node
Dim nodx2 As MSComctlLib.Node
Dim nodx3 As MSComctlLib.Node
Dim nodx4 As MSComctlLib.Node
Dim nodx5 As MSComctlLib.Node

    Select Case Left(nodx.Tag, 4)
    
        Case "ACN1"
                               
            Set nodx1 = nodx.Child
       
            While Not nodx1 Is Nothing
                
                nodx1.Checked = False
                Set nodx2 = nodx1.Child
                
                While Not nodx2 Is Nothing
                    nodx2.Checked = False
                    Set nodx3 = nodx2.Child
                    
                    While Not nodx3 Is Nothing
                        nodx3.Checked = False
                        Set nodx4 = nodx3.Child
                        
                        While Not nodx4 Is Nothing
                            nodx4.Checked = False
                            Set nodx5 = nodx4.Child
                            
                            While Not nodx5 Is Nothing
                                nodx5.Checked = False
                                Set nodx5 = nodx5.Next
                            Wend
                            
                            Set nodx4 = nodx4.Next
                            
                        Wend
                        Set nodx3 = nodx3.Next
                    Wend
                    Set nodx2 = nodx2.Next
                Wend
                Set nodx1 = nodx1.Next
             Wend
                
        DoEvents
        Case "ACN2"
            
            Set nodx2 = nodx.Child
            
            While Not nodx2 Is Nothing
                nodx2.Checked = False
                Set nodx3 = nodx2.Child
                While Not nodx3 Is Nothing
                    nodx3.Checked = False
                    Set nodx4 = nodx3.Child
                    While Not nodx4 Is Nothing
                        nodx4.Checked = False
                        Set nodx5 = nodx4.Child
                        While Not nodx5 Is Nothing
                            nodx5.Checked = False
                            Set nodx5 = nodx5.Next
                        Wend
                        Set nodx4 = nodx4.Next
                    Wend
                    Set nodx3 = nodx3.Next
                Wend
                Set nodx2 = nodx2.Next
            Wend
        
        Case "ACN3"
                
                Set nodx3 = nodx.Child
                While Not nodx3 Is Nothing
                    nodx3.Checked = False
                    Set nodx4 = nodx3.Child
                    While Not nodx4 Is Nothing
                        nodx4.Checked = False
                        Set nodx5 = nodx4.Child
                        While Not nodx5 Is Nothing
                            nodx5.Checked = False
                            Set nodx5 = nodx5.Next
                        Wend
                        Set nodx4 = nodx4.Next
                    Wend
                    Set nodx3 = nodx3.Next
                Wend
                
                
        Case "ACN4"
                
                    Set nodx4 = nodx.Child
                    While Not nodx4 Is Nothing
                        nodx4.Checked = False
                        Set nodx5 = nodx4.Child
                        While Not nodx5 Is Nothing
                            nodx5.Checked = False
                            Set nodx5 = nodx5.Next
                        Wend
                        Set nodx4 = nodx4.Next
                    Wend
                    
        Case "ACN5"
                
                    Set nodx5 = nodx.Child
                    While Not nodx5 Is Nothing
                        nodx5.Checked = False
                        Set nodx5 = nodx5.Next
                    Wend
                
                    
    End Select

End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.Node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
    DoEvents

End Sub

Private Sub tvwEstrMat_KeyUp(KeyCode As Integer, Shift As Integer)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
      
    If m_bMouseDown Then
        
        m_bMouseDown = False
        
        If m_nodxSelNode.Tag = "Raiz" Then
            m_nodxSelNode.Checked = False
            DoEvents
            Exit Sub
        End If
        
    If Mid$(m_nodxSelNode.Tag, 4, 1) < g_udtParametrosGenerales.g_iNivelMinAct And m_nodxSelNode.Checked = True Then
        m_nodxSelNode.Checked = False
        basMensajes.NivelActividades
        Exit Sub
    End If
            
        If m_nodxSelNode.Checked Then
            MarcarTodosLosHijos m_nodxSelNode
                           
        Else
            QuitarMarcaTodosSusPadres m_nodxSelNode
            QuitarMarcaTodosLosHijos m_nodxSelNode
                        
        End If
    
    End If

End Sub

Private Sub tvwEstrMat_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.Node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMat.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMat.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.Flags And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                m_bMouseDown = True
                Set m_nodxSelNode = nod
                             
                End If
            End If
        End If
    End If
    
End Sub

Private Sub tvwEstrMat_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
      
If m_bMouseDown Then
    
    m_bMouseDown = False
    
    If m_nodxSelNode.Tag = "Raiz" Then
        m_nodxSelNode.Checked = False
        DoEvents
        Exit Sub
    End If
    
    If Mid$(m_nodxSelNode.Tag, 4, 1) < g_udtParametrosGenerales.g_iNivelMinAct And m_nodxSelNode.Checked = True Then
        m_nodxSelNode.Checked = False
        basMensajes.NivelActividades
        Exit Sub
    End If

                    
    If m_nodxSelNode.Checked Then
        MarcarTodosLosHijos m_nodxSelNode
                       
    Else
        QuitarMarcaTodosSusPadres m_nodxSelNode
        QuitarMarcaTodosLosHijos m_nodxSelNode
                    
    End If
End If

End Sub

Private Sub tvwEstrMat_NodeCheck(ByVal Node As MSComctlLib.Node)
    
    m_bActividadModif = True
    If m_bMouseDown Then
        Exit Sub
    End If
    
    m_bMouseDown = True
    Set m_nodxSelNode = Node
End Sub

Private Function DevolverId(ByVal Node As MSComctlLib.Node) As String
    
    DevolverId = Right(Node.Tag, Len(Node.Tag) - 4)

End Function

Public Sub CargarUsuariosCia()
Dim adores As ador.Recordset
Dim oUsuarios As CUsuarios
Dim oUsuario As CUsuario
Dim bFCAut As Boolean
Dim bFPAut As Boolean
Dim bFMAut As Boolean
        
    sdbgUsu.RemoveAll
    
    'Le quita los colores a las celdas de autorizados
    sdbgUsu.Columns("FCAUT").HasBackColor = False
    sdbgUsu.Columns("FPAUT").HasBackColor = False
   
    'Si est� activo el FSGA y la CIA est� enlazada.
    If g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
        Set oUsuarios = g_oCiaSeleccionada.CargarTodosLosUsuariosDesde(, , , , True)
    Else
        Set oUsuarios = g_oCiaSeleccionada.CargarTodosLosUsuariosDesde
    End If
    
    If Not oUsuarios Is Nothing Then
    
        For Each oUsuario In oUsuarios
            If oUsuario.EstadoComp = 3 Then
                bFCAut = True
            Else
                bFCAut = False
            End If
            If oUsuario.EstadoProv = 3 Then
                bFPAut = True
            Else
                bFPAut = False
            End If
            
           'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
            If frmCompanias.g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                If oUsuario.IDCategoria = 0 Then 'No tiene categoria asignada
                    sdbgUsu.AddItem oUsuario.Id & Chr(9) & oUsuario.Cod & Chr(9) & oUsuario.Nombre & Chr(9) & oUsuario.Apellidos & Chr(9) & oUsuario.Idiomaden & Chr(9) & CInt(bFCAut) & Chr(9) & CInt(bFPAut) & Chr(9) & oUsuario.EstadoComp & Chr(9) & oUsuario.EstadoProv & Chr(9) & CInt((IIf(IsNull(g_oCiaSeleccionada.UsuarioPrincipal), -1, g_oCiaSeleccionada.UsuarioPrincipal) = oUsuario.Id)) & Chr(9) & "" & Chr(9) & CInt(oUsuario.AccesoFSGA) & Chr(9) & 0
                Else
                    sdbgUsu.AddItem oUsuario.Id & Chr(9) & oUsuario.Cod & Chr(9) & oUsuario.Nombre & Chr(9) & oUsuario.Apellidos & Chr(9) & oUsuario.Idiomaden & Chr(9) & CInt(bFCAut) & Chr(9) & CInt(bFPAut) & Chr(9) & oUsuario.EstadoComp & Chr(9) & oUsuario.EstadoProv & Chr(9) & CInt((IIf(IsNull(g_oCiaSeleccionada.UsuarioPrincipal), -1, g_oCiaSeleccionada.UsuarioPrincipal) = oUsuario.Id)) & Chr(9) & m_oCategoriasProve.Item(CStr(oUsuario.IDCategoria)).Denominaciones.Item(CStr(g_udtParametrosGenerales.g_iIdIdioma)).Den & Chr(9) & CInt(oUsuario.AccesoFSGA) & Chr(9) & oUsuario.IDCategoria
                End If
            Else
                sdbgUsu.AddItem oUsuario.Id & Chr(9) & oUsuario.Cod & Chr(9) & oUsuario.Nombre & Chr(9) & oUsuario.Apellidos & Chr(9) & oUsuario.Idiomaden & Chr(9) & CInt(bFCAut) & Chr(9) & CInt(bFPAut) & Chr(9) & oUsuario.EstadoComp & Chr(9) & oUsuario.EstadoProv & Chr(9) & CInt((IIf(IsNull(g_oCiaSeleccionada.UsuarioPrincipal), -1, g_oCiaSeleccionada.UsuarioPrincipal) = oUsuario.Id)) & Chr(9) & "" & Chr(9) & "" & Chr(9) & 0
            End If
            
            
            'Trata los colores
            If g_oCiaSeleccionada.EstadoCompradora = 2 Then
                'Si la cia est� desautorizada pone en gris el check
               'sdbgUsu.Columns("FCAUT").Value = False
                sdbgUsu.Columns("FCAUT").BackColor = m_GRIS

            End If
            
            If g_oCiaSeleccionada.EstadoProveedora = 2 Then
                'Si la cia est� desautorizada pone en gris el check
                'sdbgUsu.Columns("FPAUT").Value = False
                sdbgUsu.Columns("FPAUT").BackColor = m_GRIS

            End If
            
        Next
        'Si hay usuarios
        cmdA�adirUsu.Enabled = True
        cmdModificarUsu.Enabled = True
        cmdEliminarUsu.Enabled = True

    Else
            'Si no hay usuarios
        cmdA�adirUsu.Enabled = True
        cmdModificarUsu.Enabled = False
        cmdEliminarUsu.Enabled = False
    End If
    
    If picEditUsu.Visible = True Then  'Gesti�n
        'Habilita el grid para la modificaci�n
        picUsu.Enabled = True
        sdbgUsu.AllowDelete = True
        sdbgUsu.AllowUpdate = True
        'sdbgUsu.ActiveRowStyleSet = "Selection"
        
    Else  'Consulta
        'Deshabilita el grid para la modificaci�n
        sdbgUsu.AllowDelete = False
        sdbgUsu.AllowUpdate = False
        'sdbgUsu.ActiveRowStyleSet = "Normal"
    End If
    
    sdbgUsu.Update
    
    Set oUsuario = Nothing
    Set oUsuarios = Nothing
   
End Sub

Public Sub EliminarUsuarioSeleccionado()
Dim oError As CTESError
Dim sUsuario As String
    Screen.MousePointer = vbHourglass
    sUsuario = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns(0).Value).Cod
    Set oError = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns(0).Value).EliminarUsuario(g_oRaiz.SPTS, g_oCiaSeleccionada.CodGS)
    If oError.NumError <> 0 Then
        basErrores.TratarError oError
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    g_oCiaSeleccionada.Usuarios.Remove (sdbgUsu.Columns(0).Value)
    
    If sdbgUsu.AddItemRowIndex(sdbgUsu.Bookmark) > -1 Then
        sdbgUsu.RemoveItem (sdbgUsu.AddItemRowIndex(sdbgUsu.Bookmark))
    Else
        sdbgUsu.RemoveItem (0)
    End If
    g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Usu_Eliminar, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(48) & ": " & sUsuario, _
                                        g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_ENG(48) & ": " & sUsuario, _
                                        g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(48) & ": " & sUsuario, _
                                        g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(48) & ": " & sUsuario, _
                                        g_oCiaSeleccionada.Id
    sdbgUsu.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbgUsu_AfterDelete(RtnDispErrMsg As Integer)

    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    sdbgUsu.SetFocus
    If sdbgUsu.Rows > 0 Then
        sdbgUsu.Bookmark = sdbgUsu.RowBookmark(sdbgUsu.Row)
    End If

End Sub

Private Sub sdbgUsu_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    ''' * Objetivo: Confirmacion antes de eliminar y eliminar de base datos
    Dim iRespuesta As Integer
    Dim IndFor As Long, IndUsu As Long
    Dim oUsuarioEnEdicion As CUsuario
    Dim oError As CTESError

    
    DispPromptMsg = 0
    
    iRespuesta = basMensajes.PreguntaEliminar(sIdiElUsu & " " & sdbgUsu.Columns(1).Value & " - " & sdbgUsu.Columns(2).Value & " " & sdbgUsu.Columns(3).Value)
    
    If iRespuesta = vbNo Then
        Cancel = True
        Exit Sub
    End If
    
    If sdbgUsu.Rows > 0 Then
        sdbgUsu.SelBookmarks.Add sdbgUsu.Bookmark
        Screen.MousePointer = vbHourglass
        '''Eliminar de Base de datos
        Set oError = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns(0).Value).EliminarUsuario(g_oRaiz.SPTS)
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
            Screen.MousePointer = vbNormal
            Cancel = True
            Exit Sub
        End If
        
        ''' Eliminar de la coleccion
        IndUsu = Val(sdbgUsu.Bookmark)
        
        'For IndFor = IndUsu To g_oCiaSeleccionada.Usuarios.Count - 2
            
            g_oCiaSeleccionada.Usuarios.Remove (sdbgUsu.Columns(0).CellText(IndUsu))
         '   Set oUsuarioEnEdicion = g_oCiaSeleccionada.Usuarios.Item(CStr(IndFor + 1))
         '   g_oCiaSeleccionada.Usuarios.Add oUsuarioEnEdicion.Cia, oUsuarioEnEdicion.ID, oUsuarioEnEdicion.Nombre, oUsuarioEnEdicion.Apellidos, oUsuarioEnEdicion.FECACT, IndFor
         '   Set oUsuarioEnEdicion = Nothing
            
        'Next IndFor
        
        'g_oCiaSeleccionada.Usuarios.Remove (CStr(IndFor))

        
    End If

    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgUsu_Change()
Dim oError As CTESError
Dim intEstado As Integer
Dim PPalAnterior As Integer
Dim i As Integer
Dim sCat As String
Dim bm As Variant
Dim sCodPPal_Anterior As String
    If sdbgUsu.Columns(sdbgUsu.Col).Name = "FPAUT" Then
        
        If g_oCiaSeleccionada.EstadoProveedora <> 3 Or sdbgUsu.Columns("FCAUT").Value = True Then
            sdbgUsu.Columns(sdbgUsu.Col).Value = False
            sdbgUsu.DataChanged = False
        Else
            If Not sdbgUsu.ActiveCell.Value Then
                If sdbgUsu.Columns("PPAL").Value And Not sdbgUsu.Columns("FCAUT").Value Then
                    If basMensajes.PreguntaDesautorizadPrincipal() = vbYes Then
                        m_bDesautorizaCancel = False
                        intEstado = 2
                        Set oError = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).DesAutorizarusuario(True, g_oRaiz.SPTS)
                        If frmCompanias.g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                            If oError.NumError <> 0 Then
                                basErrores.TratarError oError
                                sdbgUsu.DataChanged = False
                                Exit Sub
                            End If
                            'Si se desmarca el acceso a portal a un usuario autom�ticamente se desmarca el acceso a FSGA.
                            sdbgUsu.Columns("ACCESOFSGA").Value = 0
                            Set oError = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Activar_Desactivar_AccesoFSGA(g_oCiaSeleccionada.CodGS, False)
                        End If
                    Else
                        m_bDesautorizaCancel = True
                        intEstado = 3
                        sdbgUsu.ActiveCell.Value = True
                        Set oError = New CTESError
                        oError.NumError = 0
                    End If
                Else
                    intEstado = 2
                    Set oError = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).DesAutorizarusuario(True, g_oRaiz.SPTS)
                    If frmCompanias.g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                        If oError.NumError <> 0 Then
                            basErrores.TratarError oError
                            sdbgUsu.DataChanged = False
                            Exit Sub
                        End If

                        'Si se desmarca el acceso a portal a un usuario autom�ticamente se desmarca el acceso a FSGA.
                        sdbgUsu.Columns("ACCESOFSGA").Value = 0
                        Set oError = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Activar_Desactivar_AccesoFSGA(g_oCiaSeleccionada.CodGS, False)
                    End If
                End If
            Else
                intEstado = 3
                Set oError = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Autorizarusuario(True, g_oRaiz.SPTS)
            End If
            
            If oError.NumError <> 0 Then
                basErrores.TratarError oError
                sdbgUsu.DataChanged = False
                Exit Sub
            Else
                g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Usu_Modif_Acceso, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(48) & ": " & g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Cod & "; " & g_sLitRegAccion_SPA(0) & ": " & IIf(sdbgUsu.ActiveCell.Value, g_sLitRegAccion_SPA(42), g_sLitRegAccion_SPA(11)) & ", " & g_sLitRegAccion_SPA(28) & ": " & IIf(sdbgUsu.ActiveCell.Value, g_sLitRegAccion_SPA(11), g_sLitRegAccion_SPA(42)), _
                                    g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_ENG(48) & ": " & g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Cod & "; " & g_sLitRegAccion_ENG(0) & ": " & IIf(sdbgUsu.ActiveCell.Value, g_sLitRegAccion_ENG(42), g_sLitRegAccion_ENG(11)) & ", " & g_sLitRegAccion_ENG(28) & ": " & IIf(sdbgUsu.ActiveCell.Value, g_sLitRegAccion_ENG(11), g_sLitRegAccion_ENG(42)), _
                                    g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(48) & ": " & g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Cod & "; " & g_sLitRegAccion_FRA(0) & ": " & IIf(sdbgUsu.ActiveCell.Value, g_sLitRegAccion_FRA(42), g_sLitRegAccion_FRA(11)) & ", " & g_sLitRegAccion_FRA(28) & ": " & IIf(sdbgUsu.ActiveCell.Value, g_sLitRegAccion_FRA(11), g_sLitRegAccion_FRA(42)), _
                                    g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(48) & ": " & g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Cod & "; " & g_sLitRegAccion_GER(0) & ": " & IIf(sdbgUsu.ActiveCell.Value, g_sLitRegAccion_GER(42), g_sLitRegAccion_GER(11)) & ", " & g_sLitRegAccion_GER(28) & ": " & IIf(sdbgUsu.ActiveCell.Value, g_sLitRegAccion_GER(11), g_sLitRegAccion_GER(42)), _
                                    g_oCiaSeleccionada.Id
                sdbgUsu.Columns("PROV").Value = intEstado
                sdbgUsu.Update
                sdbgUsu_RowLoaded (sdbgUsu.Bookmark)
            End If
        End If
    
        ElseIf sdbgUsu.Columns(sdbgUsu.Col).Name = "PPAL" Then
            If g_oCiaSeleccionada.EstadoProveedora <> 3 And Not opFPAut Then
                sdbgUsu.Columns(sdbgUsu.Col).Value = (sdbgUsu.Columns("ID").Value = g_oCiaSeleccionada.UsuarioPrincipal)
            Else
                If sdbgUsu.Columns("PPAL").Value = True Then
                    If Not sdbgUsu.Columns("FPAUT").Value Then
                        sdbgUsu.Columns("PPAL").Value = False
                        basMensajes.ImposibleEstablecerUsuPpal
                    Else
                            
                        Dim ppalBookmark As Variant
                        ppalBookmark = sdbgUsu.Bookmark
                        If Not IsNull(g_oCiaSeleccionada.UsuarioPrincipal) Then
                            PPalAnterior = g_oCiaSeleccionada.UsuarioPrincipal
                            sCodPPal_Anterior = g_oCiaSeleccionada.Usuarios.Item(g_oCiaSeleccionada.UsuarioPrincipal).Cod
                            'marcamos el nuevo
                            g_oCiaSeleccionada.EstablecerUsuarioPrincipal (sdbgUsu.Columns("ID").Value)
                            g_oCiaSeleccionada.UsuarioPrincipal = sdbgUsu.Columns("ID").Value
                            'desasignamos el anterior
                            sdbgUsu.MoveFirst
                            For i = 0 To sdbgUsu.Rows - 1
                                If CInt(sdbgUsu.Columns("ID").Value) = PPalAnterior Then
                                    sdbgUsu.Columns("PPAL").Value = False
                                    sdbgUsu.Bookmark = ppalBookmark
                                    Exit For
                                End If
                                sdbgUsu.MoveNext
                            Next i
                        Else
                            sCodPPal_Anterior = "Ninguno"
                            g_oCiaSeleccionada.EstablecerUsuarioPrincipal (sdbgUsu.Columns("ID").Value)
                            g_oCiaSeleccionada.UsuarioPrincipal = sdbgUsu.Columns("ID").Value
                        
                        End If
                        g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Usu_Modif_Principal, g_sLitRegAccion_SPA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(0) & ": " & sCodPPal_Anterior & ", " & g_sLitRegAccion_SPA(28) & ": " & g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Cod, _
                                                            g_sLitRegAccion_ENG(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_ENG(0) & ": " & sCodPPal_Anterior & ", " & g_sLitRegAccion_ENG(28) & ": " & g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Cod, _
                                                            g_sLitRegAccion_FRA(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(0) & ": " & sCodPPal_Anterior & ", " & g_sLitRegAccion_FRA(28) & ": " & g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Cod, _
                                                            g_sLitRegAccion_GER(8) & ": " & g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(0) & ": " & sCodPPal_Anterior & ", " & g_sLitRegAccion_GER(28) & ": " & g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Cod, _
                                                            g_oCiaSeleccionada.Id
                        sdbgUsu.Update
                    End If
                Else
            
                    sdbgUsu.Columns("PPAL").Value = True
                End If
            End If
            
        ElseIf sdbgUsu.Columns(sdbgUsu.Col).Name = "CATEGORIA" Then
            If sdbgUsu.Columns("FPAUT").Value = False Then
                sdbgUsu.Columns("CATEGORIA").Value = ""
                Exit Sub
            End If
            With sdbgUsu
               If Not sdbdCatUsu.DroppedDown Then SendKeys "{F4}"
                sCat = .Columns("CATEGORIA").Text
                sCat = EliminarCaracteresEspeciales(sCat)
                bm = BuscarCatUsu(sCat)
                sdbdCatUsu.Bookmark = bm
            End With
            
            'Si est� activo el FSGA y la CIA est� enlazada se mostrar�n las columnas de categor�a laboral y Acceso FSGA.
            If frmCompanias.g_oCiaSeleccionada.CodGS <> "" And g_udtParametrosGenerales.g_bAccesoFSGA Then
                If sCat = "" And sdbgUsu.Columns("IDCATEGORIA").Value <> 0 Then  'Es que se ha eliminado la categor�a.
                    Set oError = g_oCiaSeleccionada.Usuarios.Item(CStr(sdbgUsu.Columns("ID").Value)).DesasignarCategoria(g_oCiaSeleccionada.CodGS)
                    If oError.NumError <> 0 Then
                        basErrores.TratarError oError
                        sdbgUsu.DataChanged = False
                        Exit Sub
                    Else
                        sdbgUsu.Columns("IDCATEGORIA").Value = 0
                        sdbgUsu.Update
                        sdbgUsu_RowLoaded (sdbgUsu.Bookmark)
                    End If
                End If
            End If
        
            
        ElseIf sdbgUsu.Columns(sdbgUsu.Col).Name = "ACCESOFSGA" Then
            If sdbgUsu.Columns("FPAUT").Value = False Then
                sdbgUsu.Columns("ACCESOFSGA").Value = 0
                Exit Sub
            End If
            Set oError = g_oCiaSeleccionada.Usuarios.Item(sdbgUsu.Columns("ID").Value).Activar_Desactivar_AccesoFSGA(g_oCiaSeleccionada.CodGS, IIf(sdbgUsu.Columns(sdbgUsu.Col).Value = 0, False, True))
            
            If oError.NumError <> 0 Then
                basErrores.TratarError oError
                sdbgUsu.DataChanged = False
                Exit Sub
            Else
                sdbgUsu.Update
                sdbgUsu_RowLoaded (sdbgUsu.Bookmark)
            End If
        
        
        End If
       
End Sub

Private Sub sdbgUsu_DblClick()
Dim ador As ador.Recordset
     
    If sdbgUsu.Row < 0 Then Exit Sub
    
    If sdbdCatUsu.DroppedDown Then Exit Sub
    
    If sdbgUsu.Rows > 0 Then
        cmdModificarUsu_Click
   
    End If

End Sub



Private Sub ConfigurarSeguridad()

    If Not g_udtParametrosGenerales.g_bPremium Then
        picPremiumProv.Visible = False
        cmdEnlazarProveedor.Visible = False
    End If

    If g_udtParametrosGenerales.g_bUnaCompradora Then
        Me.SSTab1.TabVisible(4) = False
    End If
End Sub

Private Sub CargarRegistroCompradoras()
Dim adores As ador.Recordset
    
    sdbgCiasComp.RemoveAll
    
    Set adores = g_oCiaSeleccionada.devolverRegistroEnCompradoras()
    
    While Not adores.EOF
        sdbgCiasComp.AddItem adores("IDCIA").Value & Chr(9) & adores("COD").Value & Chr(9) & adores("DEN").Value & Chr(9) & adores("SOLICITADA").Value & Chr(9) & adores("REGISTRADA").Value
        adores.MoveNext
    Wend
    
    adores.Close
    Set adores = Nothing

End Sub

Private Function Disponible(ByVal param As Variant) As Boolean
'**********************************************************************
'*** Descripci�n: Determina cu�ndo un valor est� disponible.        ***
'***                                                                ***
'*** Par�metros:  param ::>> argumento de entrada sobre el cu�l     ***
'***                         se har�n las comprobaciones sobre su   ***
'***                         disponibilidad.                        ***
'***                                                                ***
'*** Valor que devuelve: True  ::>> Si el valor pasado como         ***
'***                                par�metro no es nulo y ha sido  ***
'***                                inicializado, luego est�        ***
'***                                disponible.                     ***
'***                     False ::>> En caso contrario, el par�metro ***
'***                                no est� disponible.             ***
'***                                                                ***
'**********************************************************************
    If ((Not IsEmpty(param)) And (Not IsNull(param)) And (param <> "")) Then
        Disponible = True
    Else
        Disponible = False
    End If
End Function


Private Sub EnviarMensajeAutorizacion(poUsuario As CUsuario)
'**************************************************************************
'*** Descripci�n: Envia un e-mail de autorizaci�n en el portal o bien   ***
'***              delega el envio de dicho mail en otro formulario      ***
'***              dependi�ndo de las opci�nes "Sin confirmaci�n" en los ***
'***              par�metros del PS.                                    ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos el tipo de e-mail (texto plano o html)    ***
'***              y el idioma del usuario para enviarle el e-mail seg�n ***
'***              estas opciones y para pasarselo como par�metro a las  ***
'***              funciones que generan el cuerpo del mensaje.          ***
'***                                                                    ***
'*** Valor que devuelve: -------                                        ***
'**************************************************************************
    Dim sAsunto As String
    Dim sCuerpo As String
    Dim sTo As String
    Dim errormail As TipoErrorSummit
    Dim teserror As TipoErrorSummit
    
    Select Case poUsuario.idioma
        Case 0
            sAsunto = g_udtParametrosGenerales.g_sNotifAutENG
        Case 1
            sAsunto = g_udtParametrosGenerales.g_sNotifAutSPA
        Case 2
            sAsunto = g_udtParametrosGenerales.g_sNotifAutGER
        Case 3
            sAsunto = g_udtParametrosGenerales.g_sNotifAutFRA
    End Select
    
    If ((Not m_bSesionIniciada) Or (g_oMailSender Is Nothing)) Then
        DoEvents
        IniciarSesionMail
        m_bSesionIniciada = True
    End If
    
    If (g_udtParametrosGenerales.g_iConfirmAut = 0) Then
        Set frmConfirNotif.g_oCiaSeleccionada = g_oCiaSeleccionada
        Set frmConfirNotif.g_oUsuarioSeleccionado = poUsuario
        frmConfirNotif.Caption = sIdiNotifAut
        frmConfirNotif.Show vbModal
        If frmConfirNotif.bCancelar = True Then
            Unload frmConfirNotif
            Exit Sub
        End If
        sTo = frmConfirNotif.txtMail
        Unload frmConfirNotif
        
    Else
        sTo = poUsuario.Email
    End If
    
    If g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS <> "" Then
        g_oCiaSeleccionada.SacarRemitente
        m_sCarpeta_Plantilla = g_oCiaSeleccionada.CarpetaPlantilla
    End If
    Select Case poUsuario.TipoMail
        Case 1
            'HTML
            sCuerpo = GenerarMensajeAutHTML(poUsuario)
            
        Case Else
            'TEXTO
            sCuerpo = GenerarMensajeAutTXT(poUsuario)
    End Select
    
    If sCuerpo <> "" Then
        errormail = ComponerMensaje(sTo, sAsunto, sCuerpo, , "frmCompanias", g_oCiaSeleccionada.Id, SQLBinaryToBoolean(poUsuario.TipoMail))
        If errormail.NumError <> TESnoerror Then
            If errormail.NumError <> TESnoerror Then
                basMensajes.ErrorOriginal errormail
            End If
        End If
    End If
            
    Clipboard.Clear
    
End Sub

Private Sub EnviarMensajeDesautorizacion(poUsuario As CUsuario)
'**************************************************************************
'*** Descripci�n: Envia un e-mail de desautorizaci�n en el portal o     ***
'***              bien delega el envio de dicho mail en otro formulario ***
'***              dependi�ndo de las opci�nes "Sin confirmaci�n" en los ***
'***              par�metros del PS.                                    ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos el tipo de e-mail (texto plano o html)    ***
'***              y el idioma del usuario para enviarle el e-mail seg�n ***
'***              estas opciones y para pasarselo como par�metro a las  ***
'***              funciones que generan el cuerpo del mensaje.          ***
'***                                                                    ***
'*** Valor que devuelve: -------                                        ***
'**************************************************************************
    Dim sAsunto As String
    Dim sCuerpo As String
    Dim sTo As String
    Dim errormail As TipoErrorSummit
    
    Select Case poUsuario.idioma
        Case 0
            sAsunto = g_udtParametrosGenerales.g_sNotifDesautENG
        Case 1
            sAsunto = g_udtParametrosGenerales.g_sNotifDesautSPA
        Case 2
            sAsunto = g_udtParametrosGenerales.g_sNotifDesautGER
        Case 3
            sAsunto = g_udtParametrosGenerales.g_sNotifDesautFRA
    End Select
    
    If ((Not m_bSesionIniciada) Or (g_oMailSender Is Nothing)) Then
        DoEvents
        IniciarSesionMail
        m_bSesionIniciada = True
    End If
    
    If (g_udtParametrosGenerales.g_iConfirmAut = 0) Then
        Set frmConfirNotif.g_oCiaSeleccionada = g_oCiaSeleccionada
        Set frmConfirNotif.g_oUsuarioSeleccionado = poUsuario
        frmConfirNotif.Caption = sIdiNotifDesaut
        frmConfirNotif.Show vbModal
        If frmConfirNotif.bCancelar = True Then
            Unload frmConfirNotif
            Exit Sub
        End If
        sTo = frmConfirNotif.txtMail
        Unload frmConfirNotif
        
    Else
        sTo = poUsuario.Email
    End If

    If g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS <> "" Then
        g_oCiaSeleccionada.SacarRemitente
        m_sCarpeta_Plantilla = g_oCiaSeleccionada.CarpetaPlantilla
    End If
    Select Case poUsuario.TipoMail
        Case 1   'HTML
            sCuerpo = GenerarMensajeDesautHTML(poUsuario)
            
        Case Else 'TEXTO:
            sCuerpo = GenerarMensajeDesautTXT(poUsuario)
    End Select
    
    If sCuerpo <> "" Then
        errormail = ComponerMensaje(sTo, sAsunto, sCuerpo, , "frmCompanias", g_oCiaSeleccionada.Id, SQLBinaryToBoolean(poUsuario.TipoMail))
        If errormail.NumError <> TESnoerror Then
            basMensajes.ErrorOriginal errormail
        End If
    End If
            
End Sub

Private Function GenerarMensajeAutTXT(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifAutorizacion.txt o                       ***
'***              ENG_FSPSNotifAutorizacion.txt dependi�ndo del idioma. ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim m_oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sActividades As String
    Dim sCiasRegistradas As String
    Dim sPWDDesencriptada As String
    Dim ador As ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
    Dim oIdiomas As CIdiomas
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Set oIdiomas = g_oRaiz.Generar_CIdiomas
    sIdioma = oIdiomas.DevolverCodigoDeIdioma(poUsuario.idioma)
    sFileName = sIdioma & "_"
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COMPANIAS, sIdioma, 74)
    If Not ador Is Nothing Then
        sIdiNoDisponible = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
    
    sFileName = sFileName & "FSPSNotifAutorizacion.txt"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeAutTXT = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(g_oCiaSeleccionada.Cod, Len(g_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", g_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    sPWDDesencriptada = ""
    sPWDDesencriptada = DesEncriptar(poUsuario.PWDEncriptada, poUsuario.fecpwd)
    If (Disponible(sPWDDesencriptada)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@PWD", sPWDDesencriptada)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@PWD", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", g_oCiaSeleccionada.Den)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", g_oCiaSeleccionada.NIF)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", g_oCiaSeleccionada.CodPos)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", g_oCiaSeleccionada.Poblacion)
    If (Disponible(g_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", g_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set m_oCias = g_oRaiz.Generar_CCias
    Set adores = m_oCias.DevolverDatosCia(g_oCiaSeleccionada.Id)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If

    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODMON", adores.Fields.Item("MONCOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENMON", adores.Fields.Item("MONDEN"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IDICOD", adores.Fields.Item("IDICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IDIDEN", adores.Fields.Item("IDIDEN"))
    If (Disponible(g_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", g_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case g_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case g_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case g_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)
    Set adores = Nothing
    
    Set oUsuarios = g_oRaiz.Generar_CUsuarios()
    Set adores = oUsuarios.DevolverDatosUsuario(g_oCiaSeleccionada.Id, poUsuario.Id)
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", adores.Fields.Item("COD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", adores.Fields.Item("NOM"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", adores.Fields.Item("APE"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", adores.Fields.Item("TFNO"))
    If (Not IsNull(adores.Fields.Item("TFNO2"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", adores.Fields.Item("TFNO2"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("TFNO_MOVIL"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", adores.Fields.Item("TFNO_MOVIL"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("FAX"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", adores.Fields.Item("FAX"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("EMAIL"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", adores.Fields.Item("EMAIL"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("CAR"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", adores.Fields.Item("CAR"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("DEP"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", adores.Fields.Item("DEP"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODIDIUSU", adores.Fields.Item("IDICOD").Value)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENIDIUSU", poUsuario.Idiomaden)
    sActividades = ""
    Set adores = g_oCiaSeleccionada.DevolverActividadesSuscritas(sIdioma)
    While Not adores.EOF
        sActividades = sActividades & "  # (" & adores.Fields.Item("COD1") & ") - " & adores.Fields.Item("DENACT1") & vbCrLf
        If Not IsNull(adores.Fields.Item("COD2")) Then
            sActividades = sActividades & "    # (" & adores.Fields.Item("COD2") & ") - " & adores.Fields.Item("DENACT2") & vbCrLf
        End If
        If Not IsNull(adores.Fields.Item("COD3")) Then
            sActividades = sActividades & "      # (" & adores.Fields.Item("COD3") & ") - " & adores.Fields.Item("DENACT3") & vbCrLf
        End If
        If Not IsNull(adores.Fields.Item("COD4")) Then
            sActividades = sActividades & "        # (" & adores.Fields.Item("COD4") & ") - " & adores.Fields.Item("DENACT4") & vbCrLf
        End If
        If Not IsNull(adores.Fields.Item("COD5")) Then
            sActividades = sActividades & "          # (" & adores.Fields.Item("COD5") & ") - " & adores.Fields.Item("DENACT5") & vbCrLf & vbCrLf
        End If
        adores.MoveNext
    Wend
    If (sActividades <> "") Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES", sActividades)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES", "  " & sIdiNoDisponible)
    End If
    Set adores = g_oCiaSeleccionada.devolverRegistroEnCompradoras
    sCiasRegistradas = ""
    While Not adores.EOF
        If (adores.Fields.Item("SOLICITADA") = 1) Then
            sCiasRegistradas = sCiasRegistradas & "    # (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN") & vbCrLf
        End If
        adores.MoveNext
    Wend
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@SUSCRIPCIONCIAS", sCiasRegistradas)
    adores.Close
       
    Dim adorPortal As adodb.Recordset
    Set adorPortal = g_oCiaSeleccionada.ObtenerURLPortalProveedores
    If poUsuario.PWD_HASH = "" Then
        Dim inicio, fin As Integer
        inicio = InStr(1, sCuerpoMensaje, "@INICIOALTAUSUARIO")
        fin = InStr(1, sCuerpoMensaje, "@FINALTAUSUARIO")
        sCuerpoMensaje = Replace(sCuerpoMensaje, Mid(sCuerpoMensaje, inicio, fin - inicio + 15), "")
    Else
        Dim URL_HASH As String
        URL_HASH = adorPortal.Fields.Item("URL").Value & "/script/login/restablecerpassword.asp?Idioma=" & sIdioma & "&crc=" & poUsuario.PWD_HASH
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@LINKALTA", URL_HASH)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@PORTAL", adorPortal.Fields.Item("NOM_PORTAL").Value)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@INICIOALTAUSUARIO", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FINALTAUSUARIO", "")
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PORTAL", adorPortal.Fields.Item("NOM_PORTAL").Value)
    
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeAutTXT = sCuerpoMensaje
End Function

Private Function GenerarMensajeDesautTXT(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifDesautorizacion.txt o                    ***
'***              ENG_FSPSNotifDesautorizacion.txt dependi�ndo del      ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim m_oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sActividades As String
    Dim sCiasRegistradas As String
    Dim ador As ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
    Dim oIdiomas As CIdiomas
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Set oIdiomas = g_oRaiz.Generar_CIdiomas
    sIdioma = oIdiomas.DevolverCodigoDeIdioma(poUsuario.idioma)
    sFileName = sIdioma & "_"
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COMPANIAS, sIdioma, 74)
    If Not ador Is Nothing Then
        sIdiNoDisponible = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
    
    sFileName = sFileName & "FSPSNotifDesautorizacion.txt"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeDesautTXT = ""
        
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(g_oCiaSeleccionada.Cod, Len(g_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", g_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)

    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", g_oCiaSeleccionada.Den)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", g_oCiaSeleccionada.NIF)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", g_oCiaSeleccionada.CodPos)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", g_oCiaSeleccionada.Poblacion)
    If (Disponible(g_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", g_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set m_oCias = g_oRaiz.Generar_CCias
    Set adores = m_oCias.DevolverDatosCia(g_oCiaSeleccionada.Id)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If

    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODMON", adores.Fields.Item("MONCOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENMON", adores.Fields.Item("MONDEN"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IDICOD", adores.Fields.Item("IDICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IDIDEN", adores.Fields.Item("IDIDEN"))
    If (Disponible(g_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", g_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case g_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case g_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case g_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)
    Set adores = Nothing
    
    Set oUsuarios = g_oRaiz.Generar_CUsuarios()
    Set adores = oUsuarios.DevolverDatosUsuario(g_oCiaSeleccionada.Id, poUsuario.Id)
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", adores.Fields.Item("COD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", adores.Fields.Item("NOM"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", adores.Fields.Item("APE"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", adores.Fields.Item("TFNO"))
    If (Not IsNull(adores.Fields.Item("TFNO2"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", adores.Fields.Item("TFNO2"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("TFNO_MOVIL"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", adores.Fields.Item("TFNO_MOVIL"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("FAX"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", adores.Fields.Item("FAX"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("EMAIL"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", adores.Fields.Item("EMAIL"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("CAR"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", adores.Fields.Item("CAR"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("DEP"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", adores.Fields.Item("DEP"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODIDIUSU", adores.Fields.Item("IDICOD").Value)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENIDIUSU", poUsuario.Idiomaden)
    sActividades = ""
    Set adores = g_oCiaSeleccionada.DevolverActividadesSuscritas(sIdioma)
    While Not adores.EOF
        sActividades = sActividades & "  # (" & adores.Fields.Item("COD1") & ") - " & adores.Fields.Item("DENACT1") & vbCrLf
        If Not IsNull(adores.Fields.Item("COD2")) Then
            sActividades = sActividades & "    # (" & adores.Fields.Item("COD2") & ") - " & adores.Fields.Item("DENACT2") & vbCrLf
        End If
        If Not IsNull(adores.Fields.Item("COD3")) Then
            sActividades = sActividades & "      # (" & adores.Fields.Item("COD3") & ") - " & adores.Fields.Item("DENACT3") & vbCrLf
        End If
        If Not IsNull(adores.Fields.Item("COD4")) Then
            sActividades = sActividades & "        # (" & adores.Fields.Item("COD4") & ") - " & adores.Fields.Item("DENACT4") & vbCrLf
        End If
        If Not IsNull(adores.Fields.Item("COD5")) Then
            sActividades = sActividades & "          # (" & adores.Fields.Item("COD5") & ") - " & adores.Fields.Item("DENACT5") & vbCrLf & vbCrLf
        End If
        adores.MoveNext
    Wend
    If (sActividades <> "") Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES", sActividades)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES", "  " & sIdiNoDisponible)
    End If
    Set adores = g_oCiaSeleccionada.devolverRegistroEnCompradoras
    sCiasRegistradas = ""
    While Not adores.EOF
        If (adores.Fields.Item("SOLICITADA") = 1) Then
            sCiasRegistradas = sCiasRegistradas & "    # (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN") & vbCrLf
        End If
        adores.MoveNext
    Wend
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@SUSCRIPCIONCIAS", sCiasRegistradas)
    adores.Close
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeDesautTXT = sCuerpoMensaje
End Function

Private Function GenerarMensajeAutHTML(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en HTML a partir          ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifAutorizacion.htm o                       ***
'***              ENG_FSPSNotifAutorizacion.htm dependi�ndo del idioma. ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim m_oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sActividades As String
    Dim sCiasRegistradas As String
    Dim ador As ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
    Dim oIdiomas As CIdiomas
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Set oIdiomas = g_oRaiz.Generar_CIdiomas
    sIdioma = oIdiomas.DevolverCodigoDeIdioma(poUsuario.idioma)
    sFileName = sIdioma & "_"
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COMPANIAS, sIdioma, 74)
    If Not ador Is Nothing Then
        sIdiNoDisponible = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
    
    sFileName = sFileName & "FSPSNotifAutorizacion.htm"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeAutHTML = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(g_oCiaSeleccionada.Cod, Len(g_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", g_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", g_oCiaSeleccionada.Den)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", g_oCiaSeleccionada.NIF)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", g_oCiaSeleccionada.CodPos)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", g_oCiaSeleccionada.Poblacion)
    If (Disponible(g_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", g_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    Set m_oCias = g_oRaiz.Generar_CCias
    Set adores = m_oCias.DevolverDatosCia(g_oCiaSeleccionada.Id)
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If
    
    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODMON", adores.Fields.Item("MONCOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENMON", adores.Fields.Item("MONDEN"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IDICOD", adores.Fields.Item("IDICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IDIDEN", adores.Fields.Item("IDIDEN"))
    If (Disponible(g_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", g_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case g_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case g_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case g_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)
    Set adores = Nothing
    
    Set oUsuarios = g_oRaiz.Generar_CUsuarios()
    Set adores = oUsuarios.DevolverDatosUsuario(g_oCiaSeleccionada.Id, poUsuario.Id)
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", adores.Fields.Item("COD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", adores.Fields.Item("NOM"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", adores.Fields.Item("APE"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", adores.Fields.Item("TFNO"))
    If (Not IsNull(adores.Fields.Item("TFNO2"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", adores.Fields.Item("TFNO2"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("TFNO_MOVIL"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", adores.Fields.Item("TFNO_MOVIL"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("FAX"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", adores.Fields.Item("FAX"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("EMAIL"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", adores.Fields.Item("EMAIL"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("CAR"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", adores.Fields.Item("CAR"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("DEP"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", adores.Fields.Item("DEP"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODIDIUSU", adores.Fields.Item("IDICOD").Value)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENIDIUSU", poUsuario.Idiomaden)
    sActividades = ""
    Set adores = g_oCiaSeleccionada.DevolverActividadesSuscritas(sIdioma)
    While Not adores.EOF
        sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DENACT1")
        If Not IsNull(adores.Fields.Item("COD2")) Then
            sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DENACT2")
        End If
        If Not IsNull(adores.Fields.Item("COD3")) Then
            sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DENACT3")
        End If
        If Not IsNull(adores.Fields.Item("COD4")) Then
            sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DENACT4")
        End If
        If Not IsNull(adores.Fields.Item("COD5")) Then
            sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DENACT5") & "</UL>"
        End If
        If Not IsNull(adores.Fields.Item("COD4")) Then
            sActividades = sActividades & "</UL>"
        End If
        If Not IsNull(adores.Fields.Item("COD3")) Then
            sActividades = sActividades & "</UL>"
        End If
        If Not IsNull(adores.Fields.Item("COD2")) Then
            sActividades = sActividades & "</UL>"
        End If
        sActividades = sActividades & "</UL>"
        adores.MoveNext
    Wend
    If (sActividades <> "") Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES", sActividades)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES", sIdiNoDisponible)
    End If
    adores.Close
    Set adores = g_oCiaSeleccionada.devolverRegistroEnCompradoras
    sCiasRegistradas = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        If (adores.Fields.Item("SOLICITADA") = 1) Then
            sCiasRegistradas = sCiasRegistradas & "<LI>" & adores.Fields.Item("COD") & " - " & adores.Fields.Item("DEN")
        End If
        adores.MoveNext
    Wend
    sCiasRegistradas = sCiasRegistradas & "</UL>"
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@SUSCRIPCIONCIAS", sCiasRegistradas)
    adores.Close
        
    Dim adorPortal As adodb.Recordset
    Set adorPortal = g_oCiaSeleccionada.ObtenerURLPortalProveedores
    If poUsuario.PWD_HASH = "" Then
        Dim inicio, fin As Integer
        inicio = InStr(1, sCuerpoMensaje, "@INICIOALTAUSUARIO")
        fin = InStr(1, sCuerpoMensaje, "@FINALTAUSUARIO")
        sCuerpoMensaje = Replace(sCuerpoMensaje, Mid(sCuerpoMensaje, inicio, fin - inicio + 15), "")
    Else
        Dim URL_HASH As String
        URL_HASH = adorPortal.Fields.Item("URL").Value & "/script/login/restablecerpassword.asp?Idioma=" & sIdioma & "&crc=" & poUsuario.PWD_HASH
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@LINKALTA", URL_HASH)
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@INICIOALTAUSUARIO", "")
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FINALTAUSUARIO", "")
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PORTAL", adorPortal.Fields.Item("NOM_PORTAL").Value)
    
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeAutHTML = sCuerpoMensaje
End Function

Private Function GenerarMensajeDesautHTML(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en HTML a partir          ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifDesautorizacion.htm o                    ***
'***              ENG_FSPSNotifDesautorizacion.htm dependi�ndo del      ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim m_oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sActividades As String
    Dim sCiasRegistradas As String
    Dim sPWDDesencriptada As String
    Dim ador As ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
    Dim oIdiomas As CIdiomas
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Set oIdiomas = g_oRaiz.Generar_CIdiomas
    sIdioma = oIdiomas.DevolverCodigoDeIdioma(poUsuario.idioma)
    sFileName = sIdioma & "_"
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COMPANIAS, sIdioma, 74)
    If Not ador Is Nothing Then
        sIdiNoDisponible = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
    
    sFileName = sFileName & "FSPSNotifDesautorizacion.htm"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeDesautHTML = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(g_oCiaSeleccionada.Cod, Len(g_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", g_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    sPWDDesencriptada = ""
    sPWDDesencriptada = DesEncriptar(poUsuario.PWDEncriptada, poUsuario.fecpwd)
    If (Disponible(sPWDDesencriptada)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@PWD", sPWDDesencriptada)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@PWD", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", g_oCiaSeleccionada.Den)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", g_oCiaSeleccionada.NIF)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", g_oCiaSeleccionada.CodPos)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", g_oCiaSeleccionada.Poblacion)
    If (Disponible(g_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", g_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set m_oCias = g_oRaiz.Generar_CCias
    Set adores = m_oCias.DevolverDatosCia(g_oCiaSeleccionada.Id)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If
    
    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODMON", adores.Fields.Item("MONCOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENMON", adores.Fields.Item("MONDEN"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IDICOD", adores.Fields.Item("IDICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@IDIDEN", adores.Fields.Item("IDIDEN"))
    If (Disponible(g_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", g_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case g_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case g_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case g_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)
    Set adores = Nothing
    
    Set oUsuarios = g_oRaiz.Generar_CUsuarios()
    Set adores = oUsuarios.DevolverDatosUsuario(g_oCiaSeleccionada.Id, poUsuario.Id)
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", adores.Fields.Item("COD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", adores.Fields.Item("NOM"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", adores.Fields.Item("APE"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", adores.Fields.Item("TFNO"))
    If (Not IsNull(adores.Fields.Item("TFNO2"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", adores.Fields.Item("TFNO2"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("TFNO_MOVIL"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", adores.Fields.Item("TFNO_MOVIL"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("FAX"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", adores.Fields.Item("FAX"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("EMAIL"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", adores.Fields.Item("EMAIL"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("CAR"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", adores.Fields.Item("CAR"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Not IsNull(adores.Fields.Item("DEP"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", adores.Fields.Item("DEP"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODIDIUSU", adores.Fields.Item("IDICOD").Value)
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENIDIUSU", poUsuario.Idiomaden)
    sActividades = ""
    Set adores = g_oCiaSeleccionada.DevolverActividadesSuscritas(sIdioma)
    While Not adores.EOF
        sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD1") & " - " & adores.Fields.Item("DENACT1")
        If Not IsNull(adores.Fields.Item("COD2")) Then
            sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD2") & " - " & adores.Fields.Item("DENACT2")
        End If
        If Not IsNull(adores.Fields.Item("COD3")) Then
            sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD3") & " - " & adores.Fields.Item("DENACT3")
        End If
        If Not IsNull(adores.Fields.Item("COD4")) Then
            sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD4") & " - " & adores.Fields.Item("DENACT4")
        End If
        If Not IsNull(adores.Fields.Item("COD5")) Then
            sActividades = sActividades & "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & "><LI>" & adores.Fields.Item("COD5") & " - " & adores.Fields.Item("DENACT5") & "</UL>"
        End If
        If Not IsNull(adores.Fields.Item("COD4")) Then
            sActividades = sActividades & "</UL>"
        End If
        If Not IsNull(adores.Fields.Item("COD3")) Then
            sActividades = sActividades & "</UL>"
        End If
        If Not IsNull(adores.Fields.Item("COD2")) Then
            sActividades = sActividades & "</UL>"
        End If
        sActividades = sActividades & "</UL>"
        adores.MoveNext
    Wend
    If (sActividades <> "") Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES", sActividades)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@ACTIVIDADES", sIdiNoDisponible)
    End If
    Set adores = g_oCiaSeleccionada.devolverRegistroEnCompradoras
    sCiasRegistradas = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
    While Not adores.EOF
        If (adores.Fields.Item("SOLICITADA") = 1) Then
            sCiasRegistradas = sCiasRegistradas & "<LI>" & adores.Fields.Item("COD") & " - " & adores.Fields.Item("DEN")
        End If
        adores.MoveNext
    Wend
    sCiasRegistradas = sCiasRegistradas & "</UL>"
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@SUSCRIPCIONCIAS", sCiasRegistradas)
    adores.Close
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeDesautHTML = sCuerpoMensaje
End Function

Private Sub sdbgUsu_AfterColUpdate(ByVal ColIndex As Integer)
Dim oError As CTESError

    If (sdbgUsu.Columns(sdbgUsu.Col).Name = "FCAUT") Then
        If (sdbgUsu.Columns("FCAUT").Value = True) Then
            Set m_oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
            Set m_oUsuarioSeleccionado = g_oCiaSeleccionada.Usuarios.Item(CStr(sdbgUsu.Columns("ID").Value))
            If (g_udtParametrosGenerales.g_iNotifAut = 1) Then
                EnviarMensajeAutorizacion m_oUsuarioSeleccionado
            End If
        Else
            Set m_oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
            Set m_oUsuarioSeleccionado = g_oCiaSeleccionada.Usuarios.Item(CStr(sdbgUsu.Columns("ID").Value))
            If (g_udtParametrosGenerales.g_iNotifDesaut = 1) Then
                EnviarMensajeDesautorizacion m_oUsuarioSeleccionado
            End If
        End If
    End If
    If (sdbgUsu.Columns(sdbgUsu.Col).Name = "FPAUT") And Not m_bDesautorizaCancel Then
        If (sdbgUsu.Columns("FPAUT").Value) Then
            Set m_oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
            Set m_oUsuarioSeleccionado = g_oCiaSeleccionada.Usuarios.Item(CStr(sdbgUsu.Columns("ID").Value))
            If (g_udtParametrosGenerales.g_iNotifAut = 1) Then
                EnviarMensajeAutorizacion m_oUsuarioSeleccionado
            End If
        Else
            Set m_oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
            Set m_oUsuarioSeleccionado = g_oCiaSeleccionada.Usuarios.Item(CStr(sdbgUsu.Columns("ID").Value))
            If (g_udtParametrosGenerales.g_iNotifDesaut = 1) Then
                EnviarMensajeDesautorizacion m_oUsuarioSeleccionado
            End If
        End If
    End If
   
    m_bDesautorizaCancel = False
End Sub



Private Function ValidarNIF() As Boolean
Dim bCorrecto As Boolean
    ValidarNIF = True
    If txtNif.Text = "" Or g_udtParametrosGenerales.gbCompruebaNIF = False Then Exit Function
    Select Case Me.sdbcPaiCod.Columns("VALIDNIF").Value
    
        Case 0:
            Exit Function
        Case 1
                
            If basNIFCIF.VALIDAR_CIF(Me.txtNif) Then Exit Function
            If basNIFCIF.VALIDAR_NIF(txtNif.Text) Then Exit Function
                
            basMensajes.NifIncorrecto
        Case Else
            Exit Function
    End Select
    
    ValidarNIF = False
    


End Function

Private Sub txtCod_Validate(Cancel As Boolean)
    txtCod.Text = basUtilidades.EliminarEspacioYTab(txtCod.Text)
End Sub

Private Sub txtTamanyoMax_Change()
    If Me.ActiveControl.Name = "txtTamanyoMax" Then
        cmdGuardarSize.Enabled = True
    End If
End Sub

Private Sub txtVol_Validate(Cancel As Boolean)

If txtVol.Text = "" Then Exit Sub
If Not IsNumeric(txtVol.Text) Then
    basMensajes.NoValido sIdiVolFact
    Exit Sub
End If


txtVol.Text = FormateoNumerico(txtVol.Text, "#,##0.##")

End Sub
Private Sub opFPDes_Click()
'    chkPremium.Enabled = False
'    cmdEnlazarProveedor.Enabled = False
'    optPRAut.Enabled = False
'    optPRDes.Enabled = False
'    chkPremium.Value = vbUnchecked
'    optPRAut.Value = False
'    optPRDes.Value = False
'    cmdEnlazarProveedor.Enabled = False
    If opFPDes.Value = True Then
        lblFecAut.BackColor = m_lblAmarillo
        lblFecDesAut.BackColor = m_lblVerde
        lblFecSolicit.BackColor = m_lblAmarillo
    End If
End Sub
Private Sub CargarRecursos()
Dim ador As ador.Recordset

Dim sMoncentral As String
Dim sMon As String
    On Error Resume Next
        
    Dim oMon As CMoneda
    Set oMon = g_oRaiz.Generar_CMoneda
    oMon.Id = g_udtParametrosGenerales.g_iMonedaCentral
    oMon.cargarMoneda
    sMoncentral = oMon.Cod

    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COMPANIAS, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.chkPremium.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdAceptar.Caption = ador(0).Value
        Me.cmdAceptarCat.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdA�adir.Caption = ador(0).Value
        Me.cmdA�adirUsu.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdCancelar.Caption = ador(0).Value
        Me.cmdCancelarCat.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdConsulta0.Caption = ador(0).Value
        Me.cmdConsulta1.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdEditar(0).Caption = ador(0).Value
        Me.cmdEditar(1).Caption = ador(0).Value
        ador.MoveNext
        Me.cmdEliminar.Caption = ador(0).Value
        Me.cmdEliminarUsu.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdEnlazarProveedor.Caption = ador(0).Value
        ador.MoveNext
        ador.MoveNext
        Me.cmdListado.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdModificar.Caption = ador(0).Value
        Me.cmdModificarUsu.Caption = ador(0).Value
        Me.cmdModificarCat.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdRestaurar0.Caption = ador(0).Value
        Me.cmdRestaurar1.Caption = ador(0).Value
        Me.cmdRestaurar2.Caption = ador(0).Value
        Me.cmdRestaurarCat.Caption = ador(0).Value
        ador.MoveNext
        Me.cmmdEsp.DialogTitle = ador(0).Value
        ador.MoveNext
        Me.fraDir.Caption = ador(0).Value
        ador.MoveNext
        ador.MoveNext
        fraFuncionProve(0).Caption = ador(0).Value
        ador.MoveNext
        Me.Frame2.Caption = ador(0).Value
        ador.MoveNext
        Me.Frame3.Caption = ador(0).Value
        ador.MoveNext
        Me.Label1.Caption = ador(0).Value & ":"
        sIdiDen = ador(0).Value
        m_sLitDenom = ador(0).Value  'Denominaci�n
        sdbdCatUsu.Columns("DEN").Caption = ador(0).Value 'Denominaci�n
        ador.MoveNext
        Me.Label10.Caption = ador(0).Value
        ador.MoveNext
        sMon = ador(0).Value
        sMon = Replace(sMon, "XXX", sMoncentral)
        Me.Label14.Caption = sMon
        ador.MoveNext
        Me.Label15.Caption = ador(0).Value & ":"
        sIdiMon = ador(0).Value
        ador.MoveNext
        Me.Label16.Caption = ador(0).Value & ":"
        sIdiProvi = ador(0).Value
        ador.MoveNext
        Me.Label17.Caption = ador(0).Value & ":"
        sIdiIdioma = ador(0).Value
        ador.MoveNext
        Me.Label18.Caption = ador(0).Value
        ador.MoveNext
        Me.Label19.Caption = ador(0).Value
        ador.MoveNext
        Me.Label2.Caption = ador(0).Value & ":"
        sIdiDir = ador(0).Value
        ador.MoveNext
        Me.Label3.Caption = ador(0).Value & ":"
        sIdiPai = ador(0).Value
        ador.MoveNext
        Me.Label4.Caption = ador(0).Value & ":"
        sIdiNif = ador(0).Value
        ador.MoveNext
        Me.Label5.Caption = ador(0).Value & ":"
        sIdiVolFact = ador(0).Value
        ador.MoveNext
        Me.Label6.Caption = ador(0).Value
        ador.MoveNext
        Me.Label7.Caption = ador(0).Value
        ador.MoveNext
        Me.Label8.Caption = ador(0).Value & ":"
        Me.sdbcCiaCod.Columns(1).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = ador(0).Value
        sdbcIdiCod.Columns(2).Caption = ador(0).Value
        sdbcMonCod.Columns(2).Caption = ador(0).Value
        sdbcPaiCod.Columns(2).Caption = ador(0).Value
        sdbcProviCod.Columns(2).Caption = ador(0).Value
        sdbgUsu.Columns(1).Caption = ador(0).Value
        sIdiCodigo = ador(0).Value
        sdbgCategorias.Columns("COD").Caption = ador(0).Value  'C�digo
        sdbdCategorias.Columns("COD").Caption = ador(0).Value  'C�digo
        sdbdCatUsu.Columns("COD").Caption = ador(0).Value  'C�digo

        ador.MoveNext
        Label9.Caption = ador(0).Value & ":"
        sIdiPob = ador(0).Value
        
        ador.MoveNext    '36 Autorizado
        lblAutoriz.Caption = ador(0).Value
        optPRAut.Caption = ador(0).Value
        opFPAut.Caption = ador(0).Value
        
        ador.MoveNext    '37 Desautorizado
        lblDesaut.Caption = ador(0).Value
        optPRDes.Caption = ador(0).Value
        opFPDes.Caption = ador(0).Value
        
        ador.MoveNext          '38 Solicitado
        lblSolic.Caption = ador(0).Value
        opFPSol.Caption = ador(0).Value
        
        ador.MoveNext     '39 Autorizada
        ador.MoveNext     '40 Desautorizada
        ador.MoveNext     '41 Solicitada
        
        ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = ador(0).Value
        Me.sdbcIdiCod.Columns(1).Caption = ador(0).Value
        Me.sdbcMonCod.Columns(1).Caption = ador(0).Value
        Me.sdbcPaiCod.Columns(1).Caption = ador(0).Value
        Me.sdbcProviCod.Columns(1).Caption = ador(0).Value
        Me.sdbgCiasComp.Columns(2).Caption = ador(0).Value
        
        ador.MoveNext
        Me.sdbgCiasComp.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgCiasComp.Columns(3).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgCiasComp.Columns(4).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgUsu.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgUsu.Columns(3).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgUsu.Columns(5).Caption = ador(0).Value
        ador.MoveNext
'        Me.sdbgUsu.Columns(6).Caption = Ador(0).Value
        ador.MoveNext
        Me.sdbgUsu.Columns(9).Caption = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(0) = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(1) = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(2) = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(3) = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(4) = ador(0).Value
        ador.MoveNext
        Me.SSTab1.TabCaption(5) = ador(0).Value
        ador.MoveNext
        sIdiSelFichero = ador(0).Value
        ador.MoveNext
        sIdiTodosArch = ador(0).Value
        ador.MoveNext
        sIdiElArch = ador(0).Value
        ador.MoveNext
        sIdiElUsu = ador(0).Value
        ador.MoveNext
        sIdiUsuario = ador(0).Value
        ador.MoveNext
        sIdiGuardarEsp = ador(0).Value
        ador.MoveNext
        sIdiTipoOriginal = ador(0).Value
        ador.MoveNext
        sIdiArch = ador(0).Value
        ador.MoveNext
        sIdiFichero = ador(0).Value
        ador.MoveNext
        sIdiComent = ador(0).Value
        ador.MoveNext
        sIdiFecha = ador(0).Value
        ador.MoveNext
        sIdiCodCompania = ador(0).Value
        ador.MoveNext
        sIdiAccesPortal = ador(0).Value
        ador.MoveNext
        sIdiCP = ador(0).Value
        ador.MoveNext
        sIdiAct = ador(0).Value
        ador.MoveNext
        sIdiNotifAut = ador(0).Value
        ador.MoveNext
        sIdiNotifDesaut = ador(0).Value
        ador.MoveNext
        sIdiNoDisponible = ador(0).Value
        ador.MoveNext
        sIdiSi = ador(0).Value
        ador.MoveNext
        sIdiNo = ador(0).Value
        ador.MoveNext
        Me.cmmdEsp.Filter = ador(0).Value & "(*.dot)|*.dot"
        ador.MoveNext
        Me.cmdEnlazar.ToolTipText = ador(0).Value
        ador.MoveNext
        sIdiAbrevPend = ador(0).Value 'P
        ador.MoveNext
        sIdiAbrevTodas = ador(0).Value 'T
        ador.MoveNext
        Me.cmdCodigo.Caption = ador(0).Value
        ador.MoveNext
        sIdiCiaCod = ador(0).Value
        ador.MoveNext
        lblTamanyoMax.Caption = ador(0).Value  '84 Tama�o m�ximo permitido
        msIdiProve = ador(0).Value  '88 Proveedores
        ador.MoveNext
        m_sIdiLibre = ador(0).Value '85 Espacio libre:
        ador.MoveNext
        cmdGuardarSize.Caption = ador(0).Value '86 Guardar cambios
        ador.MoveNext
        ador.MoveNext
        Me.sdbgUsu.Columns(6).Caption = ador(0).Value   '87 Acceso Portal
        ador.MoveNext
        SSTab1.TabCaption(6) = ador(0).Value  '88 Categor�as laborales
        sdbgCategorias.Caption = ador(0).Value  '88 Categor�as laborales
        ador.MoveNext
        sdbgCategorias.Columns("COSTEHORA").Caption = ador(0).Value  '89 Coste / Hora
        sdbdCategorias.Columns("COSTEHORA").Caption = ador(0).Value  '89 Coste / Hora
        sdbdCatUsu.Columns("COSTEHORA").Caption = ador(0).Value  '89 Coste / Hora
        ador.MoveNext
        sdbgCategorias.Columns("BAJALOG").Caption = ador(0).Value  '90 Baja log
        sdbdCategorias.Columns("BAJALOG").Caption = ador(0).Value  '90 Baja log
        ador.MoveNext
        sdbgUsu.Columns("CATEGORIA").Caption = ador(0).Value  '91 Categor�a laboral
        ador.MoveNext
        sdbgUsu.Columns("ACCESOFSGA").Caption = ador(0).Value  '92 Acceso FSGA
        ador.MoveNext
        Label20.Caption = ador(0).Value & ":" '93 Portal
        ador.MoveNext
        Me.lblPMvinculada.Caption = ador(0).Value & ":" '94 Solicitud vinculada
        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub


''' <summary>
''' Configura las columnas que va a tener el grid de Categor�as laborales
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>

Private Sub ConfigurarGridCategorias()
Dim oIdi As CIdioma
Dim i As Integer
Dim oColumn As SSDataWidgets_B.Column

    i = sdbgCategorias.Columns.Count
    For Each oIdi In m_oIdiomas
        DoEvents
        Set oColumn = sdbgCategorias.Columns(i)
        DoEvents
        oColumn.Name = "DEN_" & oIdi.Cod
        oColumn.Caption = m_sLitDenom & " (" & oIdi.Den & ")"
        oColumn.Visible = True
        oColumn.Locked = False
        oColumn.Width = 1900
        Set oColumn = Nothing
        i = i + 1
    Next
    
    sdbgCategorias.Columns("COSTEHORA").Position = sdbgCategorias.Columns("COSTEHORA").Position + m_oIdiomas.Count + 1
    sdbgCategorias.Columns("COSTEHORA").Caption = sdbgCategorias.Columns("COSTEHORA").Caption & " (" & m_sCodMonDef & ")"
    sdbgCategorias.Columns("BAJALOG").Position = sdbgCategorias.Columns("BAJALOG").Position + m_oIdiomas.Count + 1
    DoEvents
    sdbgCategorias.Refresh

End Sub



Private Function TamanyoOcupado() As Double
Dim oEsp As CEspecificacion
Dim dblOcupado As Double

    dblOcupado = 0
    
    For Each oEsp In g_oCiaSeleccionada.Especificaciones
        dblOcupado = dblOcupado + oEsp.DataSize
    Next
    
    TamanyoOcupado = dblOcupado
    
End Function

Private Sub ActualizarEspacioLibre()
    Dim dblOcupado As Double
    Dim dblLibre As Double
    
    dblOcupado = TamanyoOcupado
    
    dblLibre = (g_oCiaSeleccionada.MaxSizeAdjun - dblOcupado) / 1024
    
    lblEspacioLibre.Caption = m_sIdiLibre & " " & Format(dblLibre, "#,##0") & " KB"
End Sub

''' <summary>Carga el grid de categorias laborales del proveedor</summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde frmCompanias.CiaSeleccionada; Tiempo m�ximo=0,1 seg.</remarks>

Public Sub CargarCategoriasProve()
Dim adores As ador.Recordset
Dim oCategoria As CCategoria
Dim sLinea As String
Dim oIdioma As CIdioma
Dim i As Integer

    
    sdbgCategorias.RemoveAll
    DoEvents
    sdbgCategorias.Refresh
    
    Set m_oCategoriasProve = Nothing
    Set m_oCategoriasProve = g_oCiaSeleccionada.DevolverCategoriasProveedor
        
    If Not m_oCategoriasProve Is Nothing Then
    
        For Each oCategoria In m_oCategoriasProve
        
            sLinea = oCategoria.Id & Chr(9) & oCategoria.Codigo & Chr(9) & oCategoria.CosteH & Chr(9) & CInt(oCategoria.Dact)
            For Each oIdioma In m_oIdiomas
                sLinea = sLinea & Chr(9) & oCategoria.Denominaciones.Item(oIdioma.Id).Den
            Next
            
            sdbgCategorias.AddItem sLinea
            
        Next
        
    End If
    
    DoEvents
    sdbgCategorias.Refresh
    
    Set oCategoria = Nothing
    
   
End Sub

''' <summary>Configura el combo de categor�as para la pesta�a de Categorias Laborales</summary>
''' <remarks>Llamada Form_Load</remarks>

Private Sub ConfigurarComboCategorias()
Dim oIdi As CIdioma
Dim i As Integer
Dim oColumn As SSDataWidgets_B.Column

    i = sdbdCategorias.Columns.Count
    For Each oIdi In m_oIdiomas
        DoEvents
        Set oColumn = sdbdCategorias.Columns(i)
        DoEvents
        oColumn.Name = "DEN_" & oIdi.Cod
        oColumn.Caption = m_sLitDenom & " (" & oIdi.Den & ")"
        oColumn.Visible = True
        oColumn.Locked = False
        oColumn.Width = 1900
        Set oColumn = Nothing
        i = i + 1
    Next
    
    sdbdCategorias.Columns("COSTEHORA").Position = sdbdCategorias.Columns("COSTEHORA").Position + m_oIdiomas.Count + 1
    sdbdCategorias.Columns("COSTEHORA").Caption = sdbdCategorias.Columns("COSTEHORA").Caption & " (" & m_sCodMonDef & ")"
    sdbdCategorias.Columns("BAJALOG").Position = sdbdCategorias.Columns("BAJALOG").Position + m_oIdiomas.Count + 1
    DoEvents
    sdbdCategorias.Refresh

    sdbdCategorias.AddItem ""
    sdbgCategorias.Columns("COD").DropDownHwnd = sdbdCategorias.hWnd
    For Each oIdi In m_oIdiomas
        sdbgCategorias.Columns("DEN_" & oIdi.Cod).DropDownHwnd = sdbdCategorias.hWnd
    Next
End Sub

''' <summary>Configura el combo de categorias para la pesta�a de usuarios</summary>
''' <remarks>Llamada Form_Load</remarks>

Private Sub ConfigurarComboCategoriasUsu()
    sdbdCatUsu.Columns("COSTEHORA").Caption = sdbdCatUsu.Columns("COSTEHORA").Caption & " (" & m_sCodMonDef & ")"
    
    DoEvents
    sdbdCatUsu.Refresh

    sdbdCatUsu.AddItem ""
    sdbgUsu.Columns("CATEGORIA").DropDownHwnd = sdbdCatUsu.hWnd

End Sub



''' <summary>Busca una categoria en el combo de categorias, por c�digo</summary>
''' <param name="sCat">Cadena con la categor�a</param>
''' <returns>El bookmark del elemento encontrado en el combo de categorias</returns>
''' <remarks>Llamada desde sdbgCategorias_KeyDown y sdbdCategorias_PositionList</remarks>

Private Function BuscarCatCod(ByVal sCat As String) As Variant
    Dim i As Long
    Dim bm As Variant
    Dim sColValor As String
    Dim sColName As String
    

    With sdbdCategorias
        .MoveFirst
    
        If sCat <> "" Then
            For i = 0 To .Rows - 1
                bm = .GetBookmark(i)
                sColName = sdbgCategorias.Columns(sdbgCategorias.Col).Name
                sColValor = EliminarCaracteresEspeciales(.Columns("COD").CellText(bm))
                If InStr(1, UCase(sColValor), UCase(sCat)) > 0 Then
                    BuscarCatCod = bm
                    Exit For
                End If
            Next i
        End If
    End With
End Function

''' <summary>Busca una categoria en el combo de categorias, por denominaci�n</summary>
''' <param name="sCat">Cadena con la categor�a</param>
''' <returns>El bookmark del elemento encontrado en el combo de categorias</returns>
''' <remarks>Llamada desde sdbgCategorias_KeyDown y sdbdCategorias_PositionList</remarks>

Private Function BuscarCatDen(ByVal sCat As String) As Variant
    Dim i As Long
    Dim bm As Variant
    Dim sColValor As String
    Dim sColName As String
    Dim sCodIdi As String
    

    With sdbdCategorias
        .MoveFirst
    
        If sCat <> "" Then
            For i = 0 To .Rows - 1
                bm = .GetBookmark(i)
                sColName = sdbgCategorias.Columns(sdbgCategorias.Col).Name
                sCodIdi = Mid(sColName, 5, 3)
                sColValor = EliminarCaracteresEspeciales(.Columns("DEN_" & sCodIdi).CellText(bm))
                If InStr(1, UCase(sColValor), UCase(sCat)) > 0 Then
                    BuscarCatDen = bm
                    Exit For
                End If
            Next i
        End If
    End With
End Function


''' <summary>Busca una categoria en el combo de categorias</summary>
''' <param name="sCat">Cadena con la categor�a</param>
''' <returns>El bookmark del elemento encontrado en el combo de categorias</returns>
''' <remarks>Llamada desde sdbgUsu_KeyDown y sdbdCatUsu_PositionList</remarks>

Private Function BuscarCatUsu(ByVal sCat As String) As Variant
    Dim i As Long
    Dim bm As Variant
    Dim sColValor As String
    Dim sColName As String
    

    With sdbdCatUsu
        .MoveFirst
    
        If sCat <> "" Then
            For i = 0 To .Rows - 1
                bm = .GetBookmark(i)
                sColName = sdbgUsu.Columns(sdbgUsu.Col).Name
                sColValor = EliminarCaracteresEspeciales(.Columns("DEN").CellText(bm))
                If InStr(1, UCase(sColValor), UCase(sCat)) > 0 Then
                    BuscarCatUsu = bm
                    Exit For
                End If
            Next i
        End If
    End With
    
End Function



''' <summary>Crea una colecci�n de objetos de tipo CCategorias con las categorias del grid de categorias de proveedor.</summary>
''' <returns>Estructura de tipo TipoErrorSummit con el error si se ha producido</returns>
''' <remarks>Llamada desde cmdAceptar_Click</remarks>

Private Function ObtenerCategoriasDelGrid() As Boolean
    Dim i As Integer
    Dim vbm As Variant
    Dim lId As Long
    Dim lID2 As Long
    Dim oIdi As CIdioma
    Dim oDenominaciones As CMultiidiomas
    Dim bBajaLog As Boolean
    Dim oCategoria As CCategoria
    Dim bNuevo As Boolean
    
    ObtenerCategoriasDelGrid = False

    Set g_oCiaSeleccionada.Categorias = Nothing
    Set g_oCiaSeleccionada.Categorias = g_oRaiz.Generar_CCategorias
    
    With sdbgCategorias
        For i = 0 To .Rows - 1
            vbm = .AddItemBookmark(i)

            If .Columns("COD").CellValue(vbm) <> vbNullString Then
                If .Columns("ID").CellValue(vbm) <> vbNullString Then
                    lId = CLng(.Columns("ID").CellValue(vbm))
                Else
                    lId = -i - 1  'Para que no se repita la key en la col. de dir. de env�os
                End If
                If g_oCiaSeleccionada.Categorias.Item(CStr(lId)) Is Nothing Then
                    Set oDenominaciones = g_oRaiz.Generar_CMultiidiomas
                    For Each oIdi In m_oIdiomas
                        oDenominaciones.Add oIdi.Cod, .Columns("DEN_" & oIdi.Cod).CellValue(vbm), oIdi.Id
                    Next
                    If .Columns("BAJALOG").CellValue(vbm) <> 0 And .Columns("BAJALOG").CellValue(vbm) <> "" Then
                        bBajaLog = True
                    Else
                        bBajaLog = False
                    End If
                    
                    'Si no est� en la colecci�n de categor�as a eliminar, se a�ade a la colecci�n de categor�as del grid.
                    If bBajaLog = False Then
                        If lId > 0 Then
                            bNuevo = True
                            'Se puede haber dado de baja una categor�a y luego de alta habr� que comprobar que realmente no estaba de antes.
                            For Each oCategoria In g_oCiaSeleccionada.Categoriaselim
                                If oCategoria.ElimSis = 3 Then
                                    bNuevo = False
                                Else
                                    If oCategoria.Codigo = .Columns("COD").CellValue(vbm) Then
                                        lID2 = oCategoria.Id
                                        g_oCiaSeleccionada.Categoriaselim.Remove (CStr(lID2))
                                        bNuevo = False
                                        g_oCiaSeleccionada.Categorias.Add lID2, .Columns("COD").CellValue(vbm), oDenominaciones, .Columns("COSTEHORA").CellValue(vbm), bBajaLog
                                    End If
                                End If
                            Next
                            If bNuevo Then
                                   g_oCiaSeleccionada.Categorias.Add lId, .Columns("COD").CellValue(vbm), oDenominaciones, .Columns("COSTEHORA").CellValue(vbm), bBajaLog
                            End If
    
                        Else
                            g_oCiaSeleccionada.Categorias.Add lId, .Columns("COD").CellValue(vbm), oDenominaciones, .Columns("COSTEHORA").CellValue(vbm), bBajaLog
                        End If
                    End If
                    Set oDenominaciones = Nothing
                End If
            End If
        Next
    End With

    ObtenerCategoriasDelGrid = True
    
End Function


''' <summary>Comprueba si es obligatorio introducir la provincia en el formulario en funci�n del pa�s</summary>
''' <returns>Verdadero/Falso</returns>
''' <remarks>Llamada desde </remarks>
''' <revisado>JVS 16/06/2011 </revisado>

Function ProvinciaEsObligatoria() As Boolean
    Dim ador As ador.Recordset
       
    If sdbcPaiCod.Columns("ID").Value <> "" Then
    
        Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(sdbcPaiCod.Columns("ID").Value, , , False)
        
        
        If ador Is Nothing Then
           ProvinciaEsObligatoria = False
        Else
            If ador.EOF Then
                ProvinciaEsObligatoria = False
            Else
                ProvinciaEsObligatoria = True
            End If
        End If
    Else
        ProvinciaEsObligatoria = False
    End If
End Function


