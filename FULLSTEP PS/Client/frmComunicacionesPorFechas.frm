VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmComunicacionesPorFechas 
   Caption         =   "Comunicaciones con Compa��as por fechas"
   ClientHeight    =   4545
   ClientLeft      =   300
   ClientTop       =   2175
   ClientWidth     =   9675
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmComunicacionesPorFechas.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4545
   ScaleWidth      =   9675
   Begin SSDataWidgets_B.SSDBDropDown sdbddCias 
      Height          =   1575
      Left            =   660
      TabIndex        =   14
      Top             =   1260
      Width           =   3855
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      MaxDropDownItems=   10
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmComunicacionesPorFechas.frx":0CB2
      stylesets(0).AlignmentText=   0
      stylesets(0).AlignmentPicture=   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   1561
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   4868
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   50
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   6800
      _ExtentY        =   2778
      _StockProps     =   77
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   60
      ScaleHeight     =   525
      ScaleWidth      =   9570
      TabIndex        =   8
      Top             =   4035
      Width           =   9570
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Enabled         =   0   'False
         Height          =   345
         Left            =   1095
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   105
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         Enabled         =   0   'False
         Height          =   345
         Left            =   0
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   105
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2205
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   105
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEdicion 
         Caption         =   "&Edici�n"
         Default         =   -1  'True
         Height          =   345
         Left            =   8550
         TabIndex        =   10
         Top             =   105
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurarActiv 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   0
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   105
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgComunicacion 
      Height          =   3315
      Left            =   60
      TabIndex        =   0
      Top             =   690
      Width           =   9660
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   8
      stylesets.count =   3
      stylesets(0).Name=   "ConPeticiones"
      stylesets(0).BackColor=   13172735
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmComunicacionesPorFechas.frx":0CCE
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmComunicacionesPorFechas.frx":0CEA
      stylesets(2).Name=   "Selection"
      stylesets(2).BackColor=   11513775
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmComunicacionesPorFechas.frx":0D06
      BeveColorScheme =   1
      BevelColorFace  =   12632256
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "Selection"
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "IDCOM"
      Columns(0).Name =   "IDCOM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3201
      Columns(1).Caption=   "Fecha"
      Columns(1).Name =   "FECHA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3201
      Columns(2).Caption=   "Hora"
      Columns(2).Name =   "Hora"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Mask =   "##:##:##"
      Columns(2).PromptInclude=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "IDCIA"
      Columns(3).Name =   "IDCIA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "CIA"
      Columns(4).Name =   "CIA"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   4366
      Columns(5).Caption=   "Tipo Comunicaci�n"
      Columns(5).Name =   "TIPOCOM"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   3
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID_TIPOC"
      Columns(6).Name =   "ID_TIPOC"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   1879
      Columns(7).Caption=   "Comentario"
      Columns(7).Name =   "OBSCOM"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).ButtonsAlways=   -1  'True
      _ExtentX        =   17039
      _ExtentY        =   5847
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   600
      Left            =   45
      TabIndex        =   1
      Top             =   15
      Width           =   9360
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   6360
         Picture         =   "frmComunicacionesPorFechas.frx":0D22
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   195
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   4245
         TabIndex        =   5
         Top             =   195
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecApeHasta 
         Height          =   285
         Left            =   5385
         Picture         =   "frmComunicacionesPorFechas.frx":1064
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Mantenimiento"
         Top             =   195
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         Top             =   195
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecApeDesde 
         Height          =   285
         Left            =   2340
         Picture         =   "frmComunicacionesPorFechas.frx":15EE
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Mantenimiento"
         Top             =   195
         Width           =   315
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         Height          =   225
         Left            =   3555
         TabIndex        =   7
         Top             =   225
         Width           =   615
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "Desde:"
         Height          =   255
         Left            =   435
         TabIndex        =   6
         Top             =   225
         Width           =   825
      End
   End
End
Attribute VB_Name = "frmComunicacionesPorFechas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables privadas
Private oCias As CCias
Public oCiaSeleccionada As CCia
Private RespetarCombo As Boolean
Private oComunicSeleccionada As cComunicacion
Private oComunicac As cComunicaciones
Private blnBorrando As Boolean
Private sOrden As Integer

'Variables p�blicas
Public IdCiaSeleccionada As Long

'Constantes
Private sIdiEdicion As String
Private sIdiConsulta As String
Private sIdiCompania As String
Private sIdiFecha As String
Private sIdiHora As String
Private sIdiTipoCom As String


Private Sub DeshabilitarBotones()
    'actualiza el estado de los botones
    cmdEdicion.Caption = sIdiEdicion
    cmdEdicion.Enabled = True
    Me.cmdRestaurarActiv.Enabled = True
    Me.cmdRestaurarActiv.Visible = True
    
    cmdA�adir.Enabled = False
    cmdEliminar.Enabled = False
    cmdRestaurar.Enabled = False
    
    Me.cmdA�adir.Visible = False
    Me.cmdEliminar.Visible = False
    Me.cmdRestaurar.Visible = False
    
    'Inhabilita el grid para la modificaci�n
    sdbgComunicacion.AllowAddNew = False
    sdbgComunicacion.AllowDelete = False
    sdbgComunicacion.AllowUpdate = False
 
End Sub

Private Sub CiaSeleccionada()
    Set oCiaSeleccionada = Nothing
    
    oCias.CargarTodasLasCiasDesdeComunic g_iCargaMaximaCombos
    
    CargarComunicacionCia
    
End Sub

Private Sub CargarComunicacionCia()
    Dim intCia As Long
    
    On Error GoTo Ir_Error
    
    Me.MousePointer = vbHourglass

    sdbgComunicacion.RemoveAll
    For intCia = 1 To oCias.Count
        Set oCiaSeleccionada = oCias.Item(intCia)
        CargarTodasComunicaciones
    Next intCia
   
    Me.MousePointer = vbDefault
    Exit Sub
    
Ir_Error:
    Me.MousePointer = vbDefault
    Exit Sub
End Sub

Private Sub CargarTodasComunicaciones()
    'Carga las comunicaciones asociadas a la cia seleccionada
    Dim Ador As Ador.Recordset
    Dim oComunicaciones As cComunicaciones
    Dim i As Integer
    Dim strFecha As String
    Dim strFechaSolo As String
    Dim strHora As String
    Dim intEspacio As Integer
    
    Set oComunicaciones = oCiaSeleccionada.CargarTodasLasComunicacionesDesde(Me.txtFecDesde, Me.txtFecHasta)
        
    If Not oComunicaciones Is Nothing Then
        For i = 1 To oComunicaciones.Count
            strFecha = oComunicaciones.Item(i).Fecha
            intEspacio = InStr(strFecha, " ")
            If intEspacio <> 0 Then
                strFechaSolo = Mid(strFecha, 1, intEspacio - 1)
                strHora = Mid(strFecha, intEspacio + 1)
            Else
                strFechaSolo = strFecha
                strHora = "00:00:00"
            End If
            
            sdbgComunicacion.AddItem oComunicaciones.Item(i).Id & Chr(9) & strFechaSolo & Chr(9) & strHora & Chr(9) & oComunicaciones.Item(i).Cia & Chr(9) & oCiaSeleccionada.Den & Chr(9) & oComunicaciones.Item(i).DenComunicacion & Chr(9) & oComunicaciones.Item(i).TipoComunicacion
        Next i
    End If
    
    Set oComunicaciones = Nothing
    Set oCiaSeleccionada = Nothing
    
End Sub
    
Private Sub EliminarComunicSeleccionada()
    Dim oError As CTESError
    Dim iRespuesta As Integer
    
    On Error GoTo Ir_Error:
    
    iRespuesta = basMensajes.PreguntaEliminarComunicacion(sdbgComunicacion.Columns("FECHA").Value, sdbgComunicacion.Columns("CIA").Value)
    
    If iRespuesta = vbNo Then
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
        
    If sdbgComunicacion.Columns("IDCOM").Value <> "" And sdbgComunicacion.Columns("IDCIA").Value <> "" Then
        Set oError = oComunicac.Item(sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value).EliminarComunicacion
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
       
    sdbgComunicacion.RemoveItem (sdbgComunicacion.AddItemRowIndex(sdbgComunicacion.Bookmark))
        
    sdbgComunicacion.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal

    Exit Sub
    
Ir_Error:
    Exit Sub
End Sub


Private Sub cmdBuscar_Click()
    'Busca las comunicaciones que cumplan el rango de fechas especificado
    CargarLasComunicaciones
    
    DeshabilitarBotones
End Sub

Private Sub cmdCalFecApeDesde_Click()
    'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmComunicacionesPorFechas
    Set frmCalendar.ctrDestination = txtFecDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde <> "" Then
        frmCalendar.Calendar.Value = txtFecDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
    
End Sub

Private Sub cmdCalFecApeHasta_Click()
    'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmComunicacionesPorFechas
    Set frmCalendar.ctrDestination = txtFecHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecHasta <> "" Then
        frmCalendar.Calendar.Value = txtFecHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
    
End Sub

Private Sub cmdA�adir_Click()
    sdbgComunicacion.Update
    sdbgComunicacion.AllowAddNew = True
    sdbgComunicacion.AllowUpdate = True
    cmdEliminar.Enabled = False
    cmdA�adir.Enabled = False
    cmdRestaurar.Enabled = False
    
    sdbddCias.Enabled = True
    
    Set oComunicSeleccionada = Nothing
    Set oComunicSeleccionada = g_oRaiz.Generar_CComunicacion
    
    sdbgComunicacion.Scroll 0, sdbgComunicacion.Rows - sdbgComunicacion.Row
 
    If sdbgComunicacion.VisibleRows > 0 Then
        
        If sdbgComunicacion.VisibleRows >= sdbgComunicacion.Rows Then
            sdbgComunicacion.Row = sdbgComunicacion.Rows
        Else
            sdbgComunicacion.Row = sdbgComunicacion.Rows - (sdbgComunicacion.Rows - sdbgComunicacion.VisibleRows) - 1
        End If
    End If
    sdbgComunicacion.SetFocus
End Sub

Private Sub cmdEdicion_Click()
    sdbddCias.Enabled = False
    sdbgComunicacion.Columns("CIA").Locked = True
    sdbgComunicacion.DroppedDown = False
    
    If cmdEdicion.Caption = sIdiEdicion Then
        'Modo EDICI�N
        cmdEdicion.Caption = sIdiConsulta
        
        'Deshabilita el frame de b�squeda
        Me.Frame1.Enabled = False
        
        'Hace visibles los botones
        Me.cmdRestaurarActiv.Visible = False
        Me.cmdA�adir.Visible = True
        Me.cmdEliminar.Visible = True
        Me.cmdRestaurar.Visible = True
        
        'Habilita los botones
        cmdA�adir.Enabled = True
        
        'si ha cargado alguna comunicaci�n para la compa��a
        'se habilitan los botones de modificar, eliminar y restaurar
        If sdbgComunicacion.Rows > 0 Then
            cmdEliminar.Enabled = True
        End If
        cmdRestaurar.Enabled = False
        sdbgComunicacion.Columns("CIA").Locked = True
        
        'Habilita el grid
        sdbgComunicacion.AllowDelete = True
        sdbgComunicacion.AllowUpdate = True
        sdbgComunicacion.AllowAddNew = True
        
    Else
        'MODO CONSULTA
        
        'Carga las comunicaciones
        sdbgComunicacion.Update
        CargarLasComunicaciones
        
        'Modo consulta
        cmdEdicion.Caption = sIdiEdicion
        
        'Habilita el frame de b�squeda
        Me.Frame1.Enabled = True
        
        'Hace invisible los botones de edici�n
        Me.cmdRestaurarActiv.Visible = True
        Me.cmdA�adir.Visible = False
        Me.cmdEliminar.Visible = False
        Me.cmdRestaurar.Visible = False
        
        'Bloquea el grid
        
        sdbgComunicacion.AllowDelete = False
        sdbgComunicacion.AllowUpdate = False
        sdbgComunicacion.AllowAddNew = False
    End If
    
    
    If sdbgComunicacion.Columns("IDCIA").Value <> "" Then
        oCias.CargarTodasLasCiasDesde 1, CStr(sdbgComunicacion.Columns("IDCIA").Value), , , True
        Set oCiaSeleccionada = oCias.Item(sdbgComunicacion.Columns("IDCIA").Value)
        Set oComunicSeleccionada = oComunicac.Item(sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value)
    End If
End Sub

Private Sub cmdEliminar_Click()
    blnBorrando = True
    sdbgComunicacion.Update
    
    If sdbgComunicacion.Rows > 0 Then
        If sdbgComunicacion.Columns("IDCIA").Value <> "" And sdbgComunicacion.Columns("IDCOM").Value <> "" Then
            oCias.CargarTodasLasCiasDesde 1, CStr(sdbgComunicacion.Columns("IDCIA").Value), , , True
            Set oCiaSeleccionada = oCias.Item(sdbgComunicacion.Columns("IDCIA").Value)
            Set oComunicSeleccionada = oComunicac.Item(sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value)
        End If
        sdbgComunicacion.SelBookmarks.Add sdbgComunicacion.Bookmark
        EliminarComunicSeleccionada
    End If
    blnBorrando = False
    
    'Posicionar el bookmark en la fila actual despues de eliminar
    sdbgComunicacion.SetFocus
    If sdbgComunicacion.Rows > 0 Then
        sdbgComunicacion.Bookmark = sdbgComunicacion.RowBookmark(sdbgComunicacion.Row)
    End If
End Sub

Private Sub cmdRestaurar_Click()
    sdbgComunicacion.CancelUpdate
    sdbgComunicacion.DataChanged = False
    
    If Not oComunicSeleccionada Is Nothing Then
        Set oComunicSeleccionada = Nothing
    End If
    
    If Not sdbgComunicacion.IsAddRow Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdRestaurar.Enabled = False
    Else
        cmdRestaurar.Enabled = False
    End If

End Sub

Private Sub cmdRestaurarActiv_Click()
    'Restaura los datos
    CargarLasComunicaciones
End Sub

Private Sub Form_Load()
    Me.Width = 9930
    Me.Height = 5310
    CargarRecursos
    Set oCias = g_oRaiz.Generar_CCias
    Set oComunicac = g_oRaiz.Generar_CComunicaciones
    
    Me.cmdEdicion.Enabled = False
    Me.cmdRestaurarActiv.Enabled = False
    
    'Inicializamos las dropdown
    sdbddCias.AddItem ""
    sOrden = 1 'Orden de carga del grid
    'Enlazamos las dropdown
    sdbgComunicacion.Columns("CIA").DropDownHwnd = sdbddCias.hWnd
    
    'Por defecto que aparezcan las fechas de las 2 �ltimas semanas
    txtFecHasta.Text = Date
    txtFecDesde.Text = Date - 14
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Arrange()
    '''Redimensiona el formulario
    
    If Me.Height < 1900 Then Exit Sub
    If Me.Width < 700 Then Exit Sub
    
    'Grid
    sdbgComunicacion.Height = Me.Height - 1900
    sdbgComunicacion.Width = Me.Width - 250
    
    sdbgComunicacion.Columns("CIA").Width = sdbgComunicacion.Width * 0.25
    sdbgComunicacion.Columns("FECHA").Width = sdbgComunicacion.Width * 0.1
    sdbgComunicacion.Columns("Hora").Width = sdbgComunicacion.Width * 0.08
    sdbgComunicacion.Columns("TIPOCOM").Width = sdbgComunicacion.Width * 0.3
    sdbgComunicacion.Columns("OBSCOM").Width = sdbgComunicacion.Width * 0.21
    
    'Frame
    Frame1.Width = Me.Width - 250
    
    'Botones
    Me.picNavigate.Top = Me.Height - 915
    Me.picNavigate.Width = Me.Width - 250
    Me.cmdEdicion.Left = Me.picNavigate.Width - 1050
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCias = Nothing
    Set oCiaSeleccionada = Nothing
    Set oComunicSeleccionada = Nothing
    Set oComunicac = Nothing
End Sub

Private Sub sdbddCias_CloseUp()
    
    If sdbddCias.Columns(1).Text = "..." Then
        Exit Sub
    ''' Si es nulo, correcto
    ElseIf sdbddCias.Columns(1).Text = "" Then
        Exit Sub
    Else
        ''''Deja el valor seleccionado en el campo del grid
        sdbgComunicacion.Columns("IDCIA").Value = sdbddCias.Columns(2).Value
        sdbgComunicacion.Columns("CIA").Value = sdbddCias.Columns(1).Value
        
        'Carga otra vez la clase oCias por si se ha a�adido alguna (para refrescar)
        oCias.CargarTodasLasCiasDesde g_iCargaMaximaCombos, , , , False, False
        Set oCiaSeleccionada = oCias.Item(sdbddCias.Columns(2).Value)
    End If

    
End Sub

Private Sub sdbgComunicacion_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
     
    sdbgComunicacion.SetFocus
    If sdbgComunicacion.Rows > 0 Then
        sdbgComunicacion.Bookmark = sdbgComunicacion.RowBookmark(sdbgComunicacion.Row)
    End If
End Sub

Private Sub sdbgComunicacion_AfterUpdate(RtnDispErrMsg As Integer)
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdRestaurar.Enabled = False
    If cmdEdicion.Caption = sIdiConsulta Then
        oCias.CargarTodasLasCiasDesdeComunic g_iCargaMaximaCombos 'zz
    End If
End Sub

Private Sub sdbgComunicacion_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    ''' * Objetivo: Confirmacion antes de eliminar y eliminar de base datos
    Dim iRespuesta As Integer
    Dim IndFor As Long, IndCom As Long
    Dim oError As CTESError

    
    If sdbgComunicacion.IsAddRow Then
        Cancel = True
        Exit Sub
    End If

    DispPromptMsg = 0
    blnBorrando = True
    
    oCias.CargarTodasLasCiasDesde 1, sdbgComunicacion.Columns("IDCIA").Value, , , True
    Set oCiaSeleccionada = oCias.Item(sdbgComunicacion.Columns("IDCIA").Value)
    Set oComunicSeleccionada = Nothing
    Set oComunicSeleccionada = oComunicac.Item(sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value)
    iRespuesta = basMensajes.PreguntaEliminarComunicacion(sdbgComunicacion.Columns("FECHA").Value, sdbgComunicacion.Columns("CIA").Value)
    
    If iRespuesta = vbNo Then
        blnBorrando = False
        Cancel = True
        Exit Sub
    End If
    
    If sdbgComunicacion.Rows > 0 Then
        sdbgComunicacion.SelBookmarks.Add sdbgComunicacion.Bookmark
        Screen.MousePointer = vbHourglass
        '''Eliminar de Base de datos
        If sdbgComunicacion.Columns("IDCOM").Value <> "" Then
            Set oError = oComunicSeleccionada.EliminarComunicacion
            If oError.NumError <> 0 Then
                blnBorrando = False
                basErrores.TratarError oError
                Screen.MousePointer = vbNormal
                Cancel = True
                Exit Sub
            End If
        
            oComunicac.Remove (sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value)
        End If
        
        ''' Eliminar de la coleccion
        IndCom = Val(sdbgComunicacion.Bookmark)
        
    End If

    Screen.MousePointer = vbNormal
    blnBorrando = False
End Sub

Private Sub sdbgComunicacion_BeforeUpdate(Cancel As Integer)
    Dim oError As CTESError
    Dim oComunicacion As cComunicacion
    
    'Cia seleccionada
    If sdbgComunicacion.Columns("IDCIA").Value = "" Then
        Exit Sub
    End If
    
    'Set oCiaSeleccionada = oCias.Item(sdbgComunicacion.Columns("IDCIA").Value)
    'If Not oCiaSeleccionada Is Nothing Then
    '    oComunicSeleccionada.Cia = oCiaSeleccionada.ID
    'End If
    
    'Comunicaci�n seleccionada
    Set oComunicSeleccionada = Nothing
    
    If IsNull(sdbgComunicacion.Columns("IDCOM").Value) Or (sdbgComunicacion.Columns("IDCOM").Value = "") Then
        Set oComunicSeleccionada = g_oRaiz.Generar_CComunicacion
        oComunicSeleccionada.Id = 0
        oComunicSeleccionada.Cia = sdbgComunicacion.Columns("IDCIA").Value
    Else
        Set oComunicSeleccionada = oComunicac.Item(sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value)
        'oComunicSeleccionada.ID = sdbgComunicacion.Columns("IDCOM").Value
    End If
    
    
    '*************************************************
    '''Actualiza la base de datos
    If sdbgComunicacion.DataChanged = True Then
    
        'Comprueba que todos los campos se han rellenado correctamente
        If Validar = False Then
            Cancel = True
            Exit Sub
        End If
        
        
        oComunicSeleccionada.Fecha = sdbgComunicacion.Columns("FECHA").Text & " " & sdbgComunicacion.Columns("Hora").Value
        oComunicSeleccionada.DenComunicacion = sdbgComunicacion.Columns("TIPOCOM").Value
        oComunicSeleccionada.TipoComunicacion = sdbgComunicacion.Columns("ID_TIPOC").Value
        oComunicSeleccionada.Comentario = sdbgComunicacion.Columns("OBSCOM").Value
            
        If Not IsNull(oComunicSeleccionada.Id) And Not (oComunicSeleccionada.Id = 0) Then
            'modificaci�n
            Set oError = oComunicSeleccionada.ModificarDatosComunicacion
            If oError.NumError <> 0 Then
                basErrores.TratarError oError
                Screen.MousePointer = vbNormal
                Cancel = True
                Exit Sub
            End If
        
        Else
            'inserci�n
            Set oError = oComunicSeleccionada.AnyadirComunicacion
            If oError.NumError <> 0 Then
                basErrores.TratarError oError
                Screen.MousePointer = vbNormal
                Cancel = True
                Exit Sub
            End If

            sdbgComunicacion.Columns("IDCOM").Value = oComunicSeleccionada.Id
            Set oComunicacion = oComunicac.Add(oComunicSeleccionada.Cia, oComunicSeleccionada.Id, oComunicSeleccionada.Fecha, oComunicSeleccionada.TipoComunicacion, oComunicSeleccionada.DenComunicacion, oComunicSeleccionada.Comentario)
            'cuando inserte uno que vuelva a cargar las comunicaciones
            'para que se actualice la colecci�n
            
        End If
        
    End If
    '******************************************************
End Sub

Private Sub sdbgComunicacion_Change()
    sdbgComunicacion.Columns(0).Locked = True
    cmdRestaurar.Enabled = True
    cmdA�adir.Enabled = False
    cmdEliminar.Enabled = False
End Sub

Private Sub sdbgComunicacion_ComboCloseUp()
    Dim intId As Integer
    Dim strTag As String
    Dim intIndice As Integer
    Dim intPosicion As Integer
    
    'Al seleccionar un tipo de comunicaci�n obtiene del tag el �ndice
    'correspondiente a la denominaci�n seleccionada
    
    If sdbgComunicacion.Columns("TIPOCOM").ListIndex = -1 Then Exit Sub
    
    'indice del seleccionado:
    intId = sdbgComunicacion.Columns("TIPOCOM").ListIndex
    
    If intId = 0 Then
        strTag = Mid(sdbgComunicacion.Columns("TIPOCOM").TagVariant, 1, 1)
        sdbgComunicacion.Columns("ID_TIPOC").Value = Val(strTag)
    Else
        strTag = sdbgComunicacion.Columns("TIPOCOM").TagVariant
        For intIndice = 1 To intId
            intPosicion = InStr(1, strTag, "$")
            strTag = Mid(strTag, intPosicion + 1)
        Next intIndice
        sdbgComunicacion.Columns("ID_TIPOC").Value = Val(Mid(strTag, 1, 1))
    End If
    
    
End Sub

Private Sub sdbgComunicacion_DblClick()
    'Muestra la pantalla de los comentarios
    Dim blnInsercion As Boolean
    
    If sdbgComunicacion.Rows = 0 Then Exit Sub
    
    If cmdEdicion.Caption = sIdiConsulta Then
        'Si est� en modo edici�n
        If Not Validar Then
            sdbgComunicacion.SetFocus
            Exit Sub
        End If
        sdbgComunicacion.Update
    End If
    
    'cia seleccionada
    oCias.CargarTodasLasCiasDesde 1, sdbgComunicacion.Columns("IDCIA").Value, , , True
    Set oCiaSeleccionada = oCias.Item(sdbgComunicacion.Columns("IDCIA").Value)
    
    'muestra el formulario de los comentarios
    If sdbgComunicacion.Columns("IDCIA").Value <> "" Then
        If Me.cmdEdicion.Caption = sIdiConsulta Then
        '    If Validar = False Then Exit Sub
            sdbgComunicacion.Update
        End If
        
        'modificaci�n
        'oCiaSeleccionada.CargarTodasLasComunicacionesDesde
        Set oComunicSeleccionada = oComunicac.Item(sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value)
        
    Else
        'INSERCI�N
        blnInsercion = True
        
        If IsNull(sdbgComunicacion.Columns("IDCOM").Value) Or (sdbgComunicacion.Columns("IDCOM").Value = "") Then
            oComunicSeleccionada.Id = 0
        Else
            oComunicSeleccionada.Id = sdbgComunicacion.Columns("IDCOM").Value
        End If
        
        If IsDate(sdbgComunicacion.Columns("FECHA").Value & " " & sdbgComunicacion.Columns("Hora").Value) Then
            oComunicSeleccionada.Fecha = sdbgComunicacion.Columns("FECHA").Value & " " & sdbgComunicacion.Columns("Hora").Value
        Else
            oComunicSeleccionada.Fecha = Date
        End If
        
        oComunicSeleccionada.DenComunicacion = sdbgComunicacion.Columns("TIPOCOM").Value
        oComunicSeleccionada.TipoComunicacion = sdbgComunicacion.Columns("ID_TIPOC").Value
        oComunicSeleccionada.Comentario = ""
        
        'If Validar = False Then Exit Sub
        sdbgComunicacion.Update
        Set oCiaSeleccionada = oCias.Item(sdbgComunicacion.Columns("IDCIA").Value)
    End If
        
    If Me.cmdEdicion.Caption = sIdiEdicion Then
        frmComunicaComent.blnEdicion = False
    Else
        frmComunicaComent.blnEdicion = True
    End If
    
    Set frmComunicaComent.oCia = oCiaSeleccionada
    Set frmComunicaComent.oComunic = oComunicSeleccionada
    frmComunicaComent.lblFec.Caption = sdbgComunicacion.Columns("FECHA").Value & " " & sdbgComunicacion.Columns("Hora").Value
    frmComunicaComent.lblTipoC.Caption = sdbgComunicacion.Columns("TIPOCOM").Value
    
    frmComunicaComent.Show 1
    
    'If Me.cmdEdicion.Caption = CONSULTA And blnInsercion Then
    If Me.cmdEdicion.Caption = sIdiConsulta Then
        'si est� en modo consulta
        CargarLasComunicaciones
    End If
End Sub

Private Sub sdbgComunicacion_InitColumnProps()
    Dim Ador As Ador.Recordset

    ''''Inicializa el valor del combo de Tipos de comunicaci�n
    Set Ador = oComunicac.DevolverComboTiposComunicacion
    If Not Ador.EOF Then
        Ador.MoveFirst
        While Not Ador.EOF
            sdbgComunicacion.Columns("TIPOCOM").AddItem Ador("DEN"), Ador("ID")
            sdbgComunicacion.Columns("TIPOCOM").TagVariant = sdbgComunicacion.Columns("TIPOCOM").TagVariant & Ador("ID") & "$"
            Ador.MoveNext
        Wend
        'Deja en el tag el id. de todos los tipos de comunicaci�n separados
        'por el car�cter $
        sdbgComunicacion.Columns("TIPOCOM").TagVariant = Mid(sdbgComunicacion.Columns("TIPOCOM").TagVariant, 1, Len(sdbgComunicacion.Columns("TIPOCOM").TagVariant) - 1)
    End If
    
    Ador.Close
    Set Ador = Nothing
    
End Sub


Private Sub sdbgComunicacion_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
     If sdbgComunicacion.IsAddRow And cmdEdicion.Caption = sIdiConsulta Then
        'Inserci�n
        sdbgComunicacion.Columns("CIA").Locked = False
        sdbddCias.Enabled = True
        sdbgComunicacion.DroppedDown = True
        sdbgComunicacion.AllowDelete = True
        sdbgComunicacion.AllowUpdate = True
        sdbgComunicacion.AllowAddNew = True
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        
    Else
        If cmdEdicion.Caption = sIdiConsulta Then
            'Modificaci�n
            sdbgComunicacion.AllowDelete = True
            sdbgComunicacion.AllowUpdate = True
            sdbgComunicacion.AllowAddNew = True
            sdbgComunicacion.Columns("CIA").Locked = True
            sdbddCias.Enabled = False
            sdbgComunicacion.DroppedDown = False
            If sdbgComunicacion.DataChanged = False Then
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdRestaurar.Enabled = False
            End If
        Else
            'Consulta
            sdbgComunicacion.AllowDelete = False
            sdbgComunicacion.AllowUpdate = False
            sdbgComunicacion.AllowAddNew = False
            Exit Sub
        End If
    End If
    
    If (sdbgComunicacion.Columns("IDCIA").Value <> "") And (sdbgComunicacion.Columns("IDCOM").Value <> "") Then
        
        'oCias.CargarTodasLasCiasDesde 1, (sdbgComunicacion.Columns("IDCIA").Value), , , True
        'Set oCiaSeleccionada = oCias.Item(sdbgComunicacion.Columns("IDCIA").Value)
        'oCiaSeleccionada.CargarTodasLasComunicacionesDesde
        'Set oComunicSeleccionada = oCiaSeleccionada.Comunicaciones.Item(sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value)
        
        'CargarLasComunicaciones
        Set oComunicSeleccionada = oComunicac.Item(sdbgComunicacion.Columns("IDCIA").Value & "-" & sdbgComunicacion.Columns("IDCOM").Value)
    End If

End Sub

Private Function Validar() As Boolean
    Dim oTiposComunicacion As CTiposComunicacion
    Dim Ador As Ador.Recordset
    
    If blnBorrando = True Then
        Validar = False
        Exit Function
    End If
       
    If sdbgComunicacion.Columns("CIA").Value = "" Then
        Validar = False
        NoValida (sIdiCompania)
        Exit Function
    End If
    
    
    Set Ador = oCias.DevolverCiasDesde(1, , sdbgComunicacion.Columns("CIA").Value, True)
    
    If Ador.RecordCount = 0 Then
        Validar = False
        NoValida (sIdiCompania)
        sdbgComunicacion.Columns("CIA").Value = ""
        Exit Function
    End If
 
    If Not IsDate(sdbgComunicacion.Columns("FECHA").Text) Then
        Validar = False
        NoValida (sIdiFecha)
        Exit Function
    Else
        sdbgComunicacion.Columns("FECHA").Text = Format(sdbgComunicacion.Columns("FECHA").Text, "dd/mm/yyyy")
    End If
    
    If Not IsDate(sdbgComunicacion.Columns("Hora").Text) Then
        Validar = False
        NoValida (sIdiHora)
        Exit Function
    End If
    
        
    Set oTiposComunicacion = g_oRaiz.Generar_CTiposComunicacion
    If sdbgComunicacion.Columns("TIPOCOM").Value = "" Then
        Validar = False
        NoValido (sIdiTipoCom)
        Exit Function
    End If
    
    Set Ador = oTiposComunicacion.DevolverTiposDesde(, sdbgComunicacion.Columns("TIPOCOM").Value, True)
    If Ador.RecordCount = 0 Then
        Validar = False
        NoValido ("Tipo de Comunicaci�n")
        sdbgComunicacion.Columns("TIPOCOM").Value = ""
        Exit Function
    End If
    
    Set Ador = Nothing
    Set oTiposComunicacion = Nothing
    Validar = True
End Function


Private Sub sdbddcias_DropDown()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    ''' * Objetivo: Abrir el combo de cias de la forma adecuada
    sdbddCias.RemoveAll  'Borra todas las que hab�a para volverlas a cargar
    
    Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, CStr(sdbgComunicacion.ActiveCell.Value))
        
   If Not Ador Is Nothing Then
        i = 1
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbddCias.AddItem Ador("COD").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("ID").Value
            Ador.MoveNext
        Wend
        If Not Ador.EOF Then
            sdbddCias.AddItem "..." & Chr(9) & "..." & Chr(9) & "-1"
        End If
        Ador.Close
        Set Ador = Nothing
    Else
        
    End If
     
    sdbgComunicacion.ActiveCell.SelStart = 0
    sdbgComunicacion.ActiveCell.SelLength = Len(sdbgComunicacion.ActiveCell.Text)
        
End Sub

Private Sub sdbddcias_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddCias.DataFieldList = "Column 0"
    sdbddCias.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddcias_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddCias.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddCias.Rows - 1
            bm = sdbddCias.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddCias.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddCias.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddcias_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    'Dim oCia As CCia
    
    'Dim ocias As CCias
    
    'If Text = "..." Then RtnPassed = False

    ''' Si es nulo, correcto
        
    'If Text = "" Then
    '    RtnPassed = True

    'Else
    
        ''' Comprobar la existencia en la lista
        
    '    Set ocias = g_oRaiz.Generar_CCias
         
    '    ocias.CargarTodasLasCiasDesde g_iCargaMaximaCombos, , sdbgComunicacion.Columns(1).Text, , True
    
    '    If ocias.Count <> 0 Then
    '        RtnPassed = True
    '        sdbgComunicacion.Columns("IDCIA").Value = "" 'zz
    '    Else
    '        sdbgComunicacion.Columns("CIA").Value = ""
    '        sdbgComunicacion.Columns("IDCIA").Value = ""
    '    End If
    
    '    Set ocias = Nothing
        
    'End If
    
End Sub


Private Sub txtFecDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtFecDesde.Text) And Not txtFecDesde.Text = "" Then
        basMensajes.NoValida ("Fecha Inicial")
        txtFecDesde.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtFecHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtFecHasta.Text) And Not txtFecHasta.Text = "" Then
        basMensajes.NoValida ("Fecha Hasta")
        txtFecHasta.Text = ""
        Cancel = True
    End If
End Sub

Private Sub CargarLasComunicaciones(Optional Orden As Integer)
    'Carga las comunicaciones
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim strFecha As String
    Dim strFechaSolo As String
    Dim strHora As String
    Dim intEspacio As Integer
    
    'Limpia el grid
    sdbgComunicacion.RemoveAll
    
    Set Ador = oComunicac.cargarcomunicacionesdesde(Me.txtFecDesde, Me.txtFecHasta, Orden)
        
    If Not Ador Is Nothing Then
      If Not Ador.EOF Then
        
        For i = 1 To oComunicac.Count
            strFecha = oComunicac.Item(i).Fecha
            intEspacio = InStr(strFecha, " ")
            If intEspacio <> 0 Then
                strFechaSolo = Mid(strFecha, 1, intEspacio - 1)
                strHora = Mid(strFecha, intEspacio + 1)
            Else
                strFechaSolo = strFecha
                strHora = "00:00:00"
            End If
            
            sdbgComunicacion.AddItem oComunicac.Item(i).Id & Chr(9) & strFechaSolo & Chr(9) & strHora & Chr(9) & oComunicac.Item(i).Cia & Chr(9) & Ador("CIADEN") & Chr(9) & oComunicac.Item(i).DenComunicacion & Chr(9) & oComunicac.Item(i).TipoComunicacion & Chr(9) & oComunicac.Item(i).Comentario
            Ador.MoveNext
            If Ador.EOF Then Exit For
        Next i
       End If
    End If
    
    Set Ador = Nothing

    
End Sub
    

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COMUNICACIONESPORFECHAS, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdA�adir.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdEdicion.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdEliminar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdRestaurar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdRestaurarActiv.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFecDesde.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFecHasta.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbddCias.Columns(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbddCias.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgComunicacion.Columns(1).Caption = Ador(0).Value
        sIdiFecha = Ador(0).Value
        Ador.MoveNext
        Me.sdbgComunicacion.Columns(2).Caption = Ador(0).Value
        sIdiHora = Ador(0).Value
        Ador.MoveNext
        Me.sdbgComunicacion.Columns(4).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgComunicacion.Columns(5).Caption = Ador(0).Value
        sIdiTipoCom = Ador(0).Value
        Ador.MoveNext
        Me.sdbgComunicacion.Columns(7).Caption = Ador(0).Value
        Ador.MoveNext
        sIdiCompania = Ador(0).Value
        Ador.MoveNext
        sIdiEdicion = Ador(0).Value
        Ador.MoveNext
        sIdiConsulta = Ador(0).Value
        Ador.MoveNext
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


