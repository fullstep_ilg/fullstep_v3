VERSION 5.00
Begin VB.Form frmEsperaPortal 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FULLSTEP PS"
   ClientHeight    =   660
   ClientLeft      =   1995
   ClientTop       =   3570
   ClientWidth     =   4245
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   660
   ScaleWidth      =   4245
   ShowInTaskbar   =   0   'False
   Begin VB.Timer timer 
      Interval        =   1
      Left            =   4320
      Top             =   240
   End
   Begin VB.Line lineprog 
      BorderColor     =   &H000000C0&
      BorderWidth     =   2
      X1              =   300
      X2              =   840
      Y1              =   540
      Y2              =   540
   End
   Begin VB.Image Image2 
      Height          =   480
      Left            =   30
      Picture         =   "frmEsperaPortal.frx":0000
      Top             =   60
      Width           =   480
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Comunicando con FULLSTEP GS, por favor espere"
      Height          =   420
      Left            =   600
      TabIndex        =   0
      Top             =   60
      Width           =   3555
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00C00000&
      BorderWidth     =   2
      X1              =   300
      X2              =   4200
      Y1              =   540
      Y2              =   540
   End
End
Attribute VB_Name = "frmEsperaPortal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Form_Load()
CargarRecursos

End Sub


Private Sub timer_Timer()
lineprog.X1 = lineprog.X1 + 100
lineprog.X2 = lineprog.X2 + 100
Refresh
If lineprog.X1 > 3660 Then
    lineprog.X1 = 300
    lineprog.X2 = 840
    Refresh
End If

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_ESPERAPORTAL, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Label1.Caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

