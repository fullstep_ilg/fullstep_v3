VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstEnlaceProvePremium 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de enlaces de proveedores"
   ClientHeight    =   2925
   ClientLeft      =   2895
   ClientTop       =   7245
   ClientWidth     =   5640
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstEnlaceProvePremium.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2925
   ScaleWidth      =   5640
   Begin TabDlg.SSTab SSTab1 
      Height          =   2535
      Left            =   15
      TabIndex        =   0
      Top             =   0
      Width           =   5610
      _ExtentX        =   9895
      _ExtentY        =   4471
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstEnlaceProvePremium.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraCias"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      Begin VB.Frame Frame1 
         Caption         =   "Orden"
         Height          =   825
         Left            =   105
         TabIndex        =   10
         Top             =   1560
         Width           =   5400
         Begin VB.OptionButton optOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   255
            Left            =   2715
            TabIndex        =   6
            Top             =   345
            Width           =   1815
         End
         Begin VB.OptionButton optOrdCod 
            Caption         =   "C�digo"
            Height          =   255
            Left            =   855
            TabIndex        =   5
            Top             =   345
            Value           =   -1  'True
            Width           =   1860
         End
      End
      Begin VB.Frame fraCias 
         Height          =   1230
         Left            =   105
         TabIndex        =   9
         Top             =   345
         Width           =   5400
         Begin VB.OptionButton optSelec 
            Caption         =   "Proveedor"
            Height          =   255
            Left            =   225
            TabIndex        =   2
            Top             =   720
            Width           =   1095
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Listado Completo"
            Height          =   255
            Left            =   225
            TabIndex        =   1
            Top             =   315
            Value           =   -1  'True
            Width           =   1695
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
            Height          =   285
            Left            =   1365
            TabIndex        =   3
            Top             =   705
            Width           =   1140
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   7064
            Columns(2).Caption=   "Denominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   2011
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
            Height          =   285
            Left            =   2535
            TabIndex        =   4
            Top             =   705
            Width           =   2730
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3201
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3201
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   4815
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   -1560
      ScaleHeight     =   405
      ScaleWidth      =   7605
      TabIndex        =   8
      Top             =   2520
      Width           =   7605
      Begin VB.CommandButton cmdListado 
         Caption         =   "Obtener"
         Height          =   345
         Left            =   6180
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   60
         Width           =   1000
      End
   End
End
Attribute VB_Name = "frmLstEnlaceProvePremium"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public sOrigen As String
Public oCiaSeleccionada As CCia
Private oCias As CCias
Private oEnlaces As CEnlaces
Private bModoEdicion As Boolean
Public bRespetarCombo As Boolean
Public bCargarComboDesde As Boolean

Private sIdiEnlaceProve As String
Private sIdiEnlaceComp As String
Private sIdiComp As String

Private sIdiTxtTitulo As String
Private sIditxtCodigo As String
Private sIditxtDenominacion As String
Private sIdiTxtCompradora As String
Private sIdiTxtProveedora As String
Private sIdiTxtProveGS As String
Private sIdiTxtActivo As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String

Public Sub ObtenerListado()
    Dim oReport As CRAXDRT.Report
    Dim oCREnProPre As New CREnlaceProvePremium ' crear un modulo de clase
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Comprueba que la ruta sea la correcta y que exista el informe

    Select Case sOrigen
        Case "A2B2C1"
            RepPath = basParametros.g_sRptPath & "\rptEnlaceProve.rpt"
            Set oFos = New FileSystemObject
            If Not oFos.FileExists(RepPath) Then
                basMensajes.RutaDeRPTNoValida
                Set oReport = Nothing
                Set oFos = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            Set oFos = Nothing
            Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
            oCREnProPre.Listadopropre oReport, optOrdCod, optOrdDen, Val(sdbcCiaCod.Columns(0).Text), optTodos, optSelec 'pasar los parametros
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = "'" & sIdiEnlaceProve & "'"

        Case "A2B2C2"
            RepPath = basParametros.g_sRptPath & "\rptEnlaceCompa�ias.rpt"
            'aqui estaba el error
            Set oFos = New FileSystemObject
            If Not oFos.FileExists(RepPath) Then
                basMensajes.RutaDeRPTNoValida
                Set oReport = Nothing
                Set oFos = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            Set oFos = Nothing
            Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
            oCREnProPre.ListadoCompa�ias oReport, optOrdCod, optOrdDen, Val(sdbcCiaCod.Columns(0).Text), optTodos, optSelec 'pasar los parametros
            oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = "'" & sIdiEnlaceComp & "'"
    End Select

    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = "'" & sIditxtCodigo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenominacion")).Text = "'" & sIditxtDenominacion & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCompradora")).Text = "'" & sIdiTxtCompradora & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProveedora")).Text = "'" & sIdiTxtProveedora & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProveGS")).Text = "'" & sIdiTxtProveGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtActivo")).Text = "'" & sIdiTxtActivo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"

    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set pv = New Preview
    Unload Me
    pv.Hide
    
    pv.Caption = Me.Caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport ' SQLQueryString
    
    pv.crViewer.ViewReport
    pv.Show
    
    Set oReport = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdListado_Click()

If (optSelec.Value = True) And (sdbcCiaCod.Text = "") Then
    SinProvePremium
    Exit Sub
Else
    ObtenerListado
    Unload Me
End If


End Sub

Private Sub Form_Load()
Set oCias = g_oRaiz.Generar_CCias
 'frmLstEnlaceProvePremium = 5895
 'frmLstEnlaceProvePremium.Height = 3330
CargarRecursos
If optTodos.Value = True Then
    sdbcCiaDen.Text = ""
    sdbcCiaCod.Text = ""
    sdbcCiaCod.Enabled = False
    sdbcCiaDen.Enabled = False
Else
    sdbcCiaCod.Enabled = True
    sdbcCiaDen.Enabled = True

End If


Select Case sOrigen

    Case "A2B2C1"
        frmListados.ofrmLstEnlaceProvePremium1.Width = 5760
        frmListados.ofrmLstEnlaceProvePremium1.Height = 3330

        Me.Caption = sIdiEnlaceProve
        
        
    Case "A2B2C2"
        frmListados.ofrmLstEnlaceProvePremium2.Width = 5760
        frmListados.ofrmLstEnlaceProvePremium2.Height = 3330
        Me.Caption = sIdiEnlaceComp
        optSelec.Caption = sIdiComp
    End Select
    
End Sub



Private Sub Form_Unload(Cancel As Integer)
Set oCiaSeleccionada = Nothing
Set oEnlaces = Nothing
End Sub

Private Sub optSelec_Click()
    
    sdbcCiaCod.Enabled = True
    sdbcCiaDen.Enabled = True

End Sub

Private Sub optTodos_Click()

    sdbcCiaDen.Text = ""
    sdbcCiaCod.Text = ""
    sdbcCiaCod.Enabled = False
    sdbcCiaDen.Enabled = False
    
End Sub

Private Sub sdbcCiaDen_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaCod.Text = ""
        Set oCiaSeleccionada = Nothing
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    
Select Case sOrigen

    Case "A2B2C1"
        If bCargarComboDesde Then
            Set Ador = oCias.DevolverCiasPremium(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True)
        Else
            Set Ador = oCias.DevolverCiasPremium(basParametros.g_iCargaMaximaCombos, , , , True)
        End If
    Case "A2B2C2"

        If bCargarComboDesde Then
            Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, True)
        Else
            Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , , True, True)
        End If

End Select

    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    CiaSeleccionada
    
End Sub

Private Sub sdbcCiaCod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        Set oCiaSeleccionada = Nothing
        bRespetarCombo = False
        
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    bRespetarCombo = True
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    CiaSeleccionada
    
    
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
Dim Ador As Ador.Recordset



If Trim(sdbcCiaCod.Text) = "" Then Exit Sub

Select Case sOrigen

    Case "A2B2C1"

        Set Ador = oCias.DevolverCiasPremium(1, sdbcCiaCod.Text, , True)
    
    Case "A2B2C2"
    
        Set Ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , , , True)
    
    End Select
    
        If Ador Is Nothing Then
        
            sdbcCiaCod = ""
            Exit Sub
        
        Else
            If Ador.EOF Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                If UCase(sdbcCiaCod.Text) <> UCase(Ador("COD").Value) Then
                    sdbcCiaCod.Value = ""
                    Exit Sub
                Else
                    bRespetarCombo = True
                    sdbcCiaDen.Text = Ador("DEN").Value
                    sdbcCiaCod.Text = Ador("COD").Value
                    sdbcCiaCod.Columns(0).Value = Ador("ID").Value
                    CiaSeleccionada
                    bRespetarCombo = False
                    bCargarComboDesde = False
                End If
            End If
        
            Ador.Close
            Set Ador = Nothing
        
        End If
    
End Sub
Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaCod.RemoveAll
    Set oCiaSeleccionada = Nothing
    
    

Select Case sOrigen

    Case "A2B2C1"
        If bCargarComboDesde Then
            Set Ador = oCias.DevolverCiasPremium(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False)
        Else
            Set Ador = oCias.DevolverCiasPremium(g_iCargaMaximaCombos, , , False, False)
        End If
    Case "A2B2C2"
        If bCargarComboDesde Then
            Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, True)
        Else
            Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, True)
        End If

End Select
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    
End Sub
Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub
Public Sub CiaSeleccionada()
Dim Ador As Ador.Recordset
Dim oEnlace As CEnlace

    If sOrigen <> "frmCompanias" Then
        If sdbcCiaCod.Text = "" Then Exit Sub
        oCias.CargarTodasLasCiasDesde 1, sdbcCiaCod.Columns(0).Value, , , True
    
        Set oCiaSeleccionada = Nothing
        Set oCiaSeleccionada = oCias.Item(CStr(sdbcCiaCod.Columns(0).Value))
        
    End If
        
    
    Set oEnlaces = oCiaSeleccionada.DevolverTodosLosEnlaces(True)
    
    bRespetarCombo = False
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTENLACEPROVEPREMIUM, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        sIdiTxtTitulo = Ador(0).Value
        
        Ador.MoveNext
        Me.cmdListado.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Frame1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optOrdCod.Caption = Ador(0).Value
        Me.sdbcCiaCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = Ador(0).Value
        sIditxtCodigo = Ador(0).Value
        Ador.MoveNext
        Me.optOrdDen.Caption = Ador(0).Value
        Me.sdbcCiaCod.Columns(2).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = Ador(0).Value
        sIditxtDenominacion = Ador(0).Value
        Ador.MoveNext
        Me.optSelec.Caption = Ador(0).Value
        Ador.MoveNext
        Me.optTodos.Caption = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sIdiEnlaceProve = Ador(0).Value
        Ador.MoveNext
        sIdiEnlaceComp = Ador(0).Value
        Ador.MoveNext
        sIdiComp = Ador(0).Value
        Ador.MoveNext
        
        sIdiTxtCompradora = Ador(0).Value
        Ador.MoveNext
        sIdiTxtProveedora = Ador(0).Value
        Ador.MoveNext
        sIdiTxtProveGS = Ador(0).Value
        Ador.MoveNext
        sIdiTxtActivo = Ador(0).Value
        Ador.MoveNext
        sIdiTxtPag = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDe = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub



