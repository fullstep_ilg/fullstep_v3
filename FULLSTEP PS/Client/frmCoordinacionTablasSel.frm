VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCoordinacionTablasSel 
   Caption         =   "Coordinaci�n de tablas base"
   ClientHeight    =   1830
   ClientLeft      =   555
   ClientTop       =   2415
   ClientWidth     =   5910
   Icon            =   "frmCoordinacionTablasSel.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   1830
   ScaleWidth      =   5910
   Begin VB.CommandButton cmdIniciar 
      Caption         =   "Iniciar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4830
      TabIndex        =   4
      Top             =   1410
      Width           =   1005
   End
   Begin VB.Frame Frame1 
      Caption         =   "Selecci�n"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   5775
      Begin SSDataWidgets_B.SSDBCombo sdbcTablas 
         Height          =   285
         Left            =   1660
         TabIndex        =   3
         Top             =   360
         Width           =   1845
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "DEN"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3254
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   1660
         TabIndex        =   5
         Top             =   720
         Width           =   885
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   1561
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   2590
         TabIndex        =   6
         Top             =   720
         Width           =   2945
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   5195
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "Tabla a coordinar:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   180
         TabIndex        =   2
         Top             =   360
         Width           =   1450
      End
      Begin VB.Label lblCia 
         Caption         =   "Compa��a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   180
         TabIndex        =   1
         Top             =   780
         Width           =   1275
      End
   End
End
Attribute VB_Name = "frmCoordinacionTablasSel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Private IdCiaSeleccionada As Long
Private oCias As CCias
Private oCiaSeleccionada As CCia


Private sIdiCompania As String
Private sIdiProvincias As String
Private sIdiPaises As String
Private sIdiMonedas As String


Private Sub cmdIniciar_Click()
Dim Ador As Ador.Recordset
Dim i As Integer
Dim adorMon As Ador.Recordset
                
    If oCiaSeleccionada Is Nothing Then
        basMensajes.NoValida sIdiCompania
        Exit Sub
    End If
    
    If Not oCiaSeleccionada.ComprobarConectividadConCia Then
        basMensajes.ConexionIncorrecta
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Select Case sdbcTablas.Value
        
        Case sIdiProvincias
                        
            Unload frmCoordinacionTablaProvi
            
            Set frmCoordinacionTablaProvi.oCia = oCiaSeleccionada
            frmCoordinacionTablaProvi.WindowState = vbNormal
            frmCoordinacionTablaProvi.SetFocus
        
        Case sIdiPaises
            
                Unload frmCoordinacionTablaPais
                
                Set Ador = oCiaSeleccionada.DevolverPaisesDeLaCiaDesde(10000)
    
                 If Not Ador Is Nothing Then
                     
                     i = 1
                     
                     While Not Ador.EOF
                         i = i + 1
                         frmCoordinacionTablaPais.sdbgTabla.AddItem Ador("COD").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("FSP_COD").Value & Chr(9) & Ador("FSP_DEN").Value
                         Ador.MoveNext
                     Wend
                     
                     Ador.Close
                     Set Ador = Nothing
                 End If
              
            Set frmCoordinacionTablaPais.oCia = oCiaSeleccionada
            frmCoordinacionTablaPais.WindowState = vbNormal
            frmCoordinacionTablaPais.SetFocus
        
        Case sIdiMonedas
                
                Unload frmCoordinacionTablaMonedas
                
                ''''''''''''Obtiene la moneda central de GS''''''''''
                Set adorMon = g_oRaiz.DevolverMonedasCentralesDesde(oCiaSeleccionada.Cod, , True, True)
                If Not adorMon.EOF Then
                    frmCoordinacionTablaMonedas.Mon = adorMon("MON_GS")
                Else
                    Set adorMon = Nothing
                    Set adorMon = g_oRaiz.DevolverMonedaCentralGS(oCiaSeleccionada.ID)
                    If Not adorMon.EOF Then
                        frmCoordinacionTablaMonedas.Mon = adorMon("MONCEN")
                    Else
                        frmCoordinacionTablaMonedas.Mon = ""
                    End If
                End If
                Set adorMon = Nothing
                ''''''''''''''''''''''
                
                Set Ador = oCiaSeleccionada.DevolverMonedasDeLaCiaDesde(10000)
    
                 If Not Ador Is Nothing Then
                     
                     i = 1
                     
                     While Not Ador.EOF
                         i = i + 1
                         frmCoordinacionTablaMonedas.sdbgTabla.AddItem Ador("COD").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("FSP_COD").Value & Chr(9) & Ador("FSP_DEN").Value
                         Ador.MoveNext
                     Wend
                     
                     Ador.Close
                     Set Ador = Nothing
                 End If
              
                Set frmCoordinacionTablaMonedas.oCia = oCiaSeleccionada
                frmCoordinacionTablaMonedas.WindowState = vbNormal
                frmCoordinacionTablaMonedas.SetFocus
        
    End Select
    
    Screen.MousePointer = vbNormal
    
    
End Sub

Private Sub Form_Load()
    
    Width = 6030
    Height = 2340
    CargarRecursos
    
    sdbcTablas.AddItem sIdiPaises
    sdbcTablas.AddItem sIdiProvincias
    sdbcTablas.AddItem sIdiMonedas
    
    Set oCias = g_oRaiz.Generar_CCias
    
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
        
        lblCia.Visible = False
        
        sdbcCiaCod.Visible = False
        sdbcCiaDen.Visible = False
        
        CiaSeleccionada (basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora)
        
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCias = Nothing
    Set oCiaSeleccionada = Nothing
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaCod.RemoveAll

    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, True)
    Else
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, True)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("FCEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    'Set oCias = g_oRaiz.Generar_CCias
End Sub
Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaCod_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        Set oCiaSeleccionada = Nothing
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    CiaSeleccionada (sdbcCiaCod.Columns(0).Value)
    
    
    
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
Dim Ador As Ador.Recordset

    If Trim(sdbcCiaCod.Text) = "" Then Exit Sub
    
    Set Ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , True)
    
    If Ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If Ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(Ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcCiaDen.Text = Ador("DEN").Value
                bRespetarCombo = False
                bCargarComboDesde = False
                
                CiaSeleccionada (Ador("ID").Value)
            End If
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    
End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaDen.RemoveAll

    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, True)
    Else
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, True)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("FCEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        oCiaSeleccionada = Nothing
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(2).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    CiaSeleccionada (sdbcCiaDen.Columns(0).Value)
    
    
End Sub

Private Sub sdbcTablas_InitColumnProps()
    
    sdbcTablas.DataField = "Column 0"
    sdbcTablas.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub CiaSeleccionada(IdCia As Integer)
    
    If Not oCiaSeleccionada Is Nothing Then
        If oCiaSeleccionada.ID = IdCia Then Exit Sub
    End If
    Set oCiaSeleccionada = Nothing
    oCias.CargarTodasLasCiasDesde 1, IdCia, , , True
    Set oCiaSeleccionada = oCias.Item(CStr(IdCia))
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COORDINACIONTABLASSEL, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdIniciar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Frame1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblCia.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCiaCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        sIdiCompania = Ador(0).Value
        Ador.MoveNext
        sIdiProvincias = Ador(0).Value
        Ador.MoveNext
        sIdiPaises = Ador(0).Value
        Ador.MoveNext
        sIdiMonedas = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub




