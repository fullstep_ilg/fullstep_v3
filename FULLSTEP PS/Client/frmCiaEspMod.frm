VERSION 5.00
Begin VB.Form frmCiaEspMod 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Modificar especificación"
   ClientHeight    =   2505
   ClientLeft      =   765
   ClientTop       =   3960
   ClientWidth     =   4680
   Icon            =   "frmCiaEspMod.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2505
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2385
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   2100
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1275
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2100
      Width           =   1005
   End
   Begin VB.TextBox txtCom 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   780
      Width           =   4455
   End
   Begin VB.TextBox txtNombre 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1140
      TabIndex        =   0
      Top             =   180
      Width           =   3435
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Comentario:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   3
      Top             =   540
      Width           =   1695
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Nombre:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   1
      Top             =   180
      Width           =   975
   End
End
Attribute VB_Name = "frmCiaEspMod"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Private m_sNombre As String
Private m_sExtension As String
Public g_oEsp As CEspecificacion

Private sIdiNombre As String

Private Sub cmdAceptar_Click()
Dim oError As CTESError
        
    If InStr(1, txtNombre, ".", vbTextCompare) <> 0 Then
        basMensajes.NoValido sIdiNombre
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If g_sOrigen = "frmCompanias" Then
        frmCompanias.g_oCiaSeleccionada.Especificaciones.Item(CStr(frmCompanias.lstvwEsp.SelectedItem.Tag)).Nombre = Trim(txtNombre) & "." & m_sExtension
        If txtCom = "" Then
            frmCompanias.g_oCiaSeleccionada.Especificaciones.Item(CStr(frmCompanias.lstvwEsp.SelectedItem.Tag)).Comentario = Null
        Else
            frmCompanias.g_oCiaSeleccionada.Especificaciones.Item(CStr(frmCompanias.lstvwEsp.SelectedItem.Tag)).Comentario = Trim(txtCom)
        End If
        Set oError = g_oEsp.ModificarEspecificacion
        If oError.NumError <> 0 Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError oError
            Set g_oEsp = Nothing
            Exit Sub
        End If
        frmCompanias.lstvwEsp.SelectedItem.ListSubItems.Item(1).Text = Trim(txtCom)
        frmCompanias.lstvwEsp.SelectedItem.Text = Trim(txtNombre) & "." & m_sExtension
        g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Archivo_Modif, g_sLitRegAccion_SPA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_SPA(27) & ": " & g_oEsp.Nombre, _
                                            g_sLitRegAccion_ENG(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_ENG(27) & ": " & g_oEsp.Nombre, _
                                            g_sLitRegAccion_FRA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_FRA(27) & ": " & g_oEsp.Nombre, _
                                            g_sLitRegAccion_GER(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & "; " & g_sLitRegAccion_GER(27) & ": " & g_oEsp.Nombre, _
                                            frmCompanias.g_oCiaSeleccionada.ID
    End If
    
    Set g_oEsp = Nothing
    Screen.MousePointer = vbNormal
    Unload Me

End Sub

Private Sub cmdCancelar_Click()
   Unload Me
End Sub


Private Sub Form_Activate()
    txtNombre.SetFocus
End Sub

Private Sub Form_Load()
    
    CargarRecursos

    Me.Top = frmMDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = frmMDI.ScaleWidth / 2 - Me.Width / 2

    If g_sOrigen = "frmCompanias" Then
        m_sNombre = Left(frmCompanias.lstvwEsp.SelectedItem.Text, InStr(1, frmCompanias.lstvwEsp.SelectedItem.Text, ".", vbTextCompare) - 1)
        m_sExtension = DevolverExtension(frmCompanias.lstvwEsp.SelectedItem.Text)
        txtNombre = m_sNombre
        txtCom = frmCompanias.lstvwEsp.SelectedItem.ListSubItems.Item(1).Text
    End If
    
End Sub


Private Function DevolverExtension(ByVal sNomArchivo As String) As String
Dim iPosicion As Integer

    iPosicion = InStr(1, sNomArchivo, ".")
    DevolverExtension = Mid(sNomArchivo, iPosicion + 1)

End Function

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CIAESPMOD, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label2.Caption = Ador(0).Value & ":"
        sIdiNombre = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


