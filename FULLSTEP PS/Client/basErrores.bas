Attribute VB_Name = "basErrores"

Public Sub TratarError(TESErr As CTESError)
Dim iErrDecoded As Long
Dim iErrDecodedAPI As Long
Dim sErrMensaje As String

Select Case TESErr.numerror
    
    Case erroressummit.TESUsuarioNoValido
            
            basMensajes.UsuarioNoAutorizado
            
    Case erroressummit.TESDatoDuplicado
            
             basMensajes.DatoDuplicado TESErr.Arg1
            
    Case erroressummit.TESDatoEliminado
        
             Select Case TESErr.Arg1
             
             Case Else
                basMensajes.DatoEliminado TESErr.Arg1
            End Select
            

    Case erroressummit.TESDatoNoValido
            
            If TESErr.Arg2 Then
                basMensajes.NoValida TESErr.Arg1
            Else
                basMensajes.NoValido TESErr.Arg1
            End If
    Case erroressummit.TESImposibleEliminar
            
        basMensajes.ImposibleEliDatRel TESErr.Arg1
                        
    Case erroressummit.TESInfModificada
            
            basMensajes.DatosModificados
            
    Case erroressummit.TESFaltanDatos
            
            basMensajes.FaltanDatos TESErr.Arg1
    
   Case erroressummit.TESOtroerror
            
            basMensajes.OtroError TESErr.Arg1, TESErr.Arg2
            
    
    Case erroressummit.TESInfActualModificada
            basMensajes.DatosActualesModificados
            
    Case erroressummit.TESProvePortalDesautorizado
            basMensajes.ImposibleAutorizarProvePortal
    
    Case erroressummit.TESciaportaldesautorizada
            basMensajes.CiaDesautorizadaEnPortal
    
    Case erroressummit.TESConexionServidorFailed
            basMensajes.FalloEnConexionConServidor
    Case erroressummit.TESDesconexionServidorFailed
            basMensajes.FalloEnDesconexionConServidor
    Case erroressummit.TESLinkedServerCreateError
            basMensajes.FalloAlCrearLinkedServer
    Case erroressummit.TESLinkedServerAddError
            basMensajes.FalloAlAnyadirLinkedServer
    Case erroressummit.TESLinkedServerSetOptionsError
            basMensajes.FalloEnSetOptions
    Case erroressummit.TESLinkedLoginCreateError
            basMensajes.FalloAlCrearLinkLogin
    Case erroressummit.TESLinkedLoginAddError
            basMensajes.FalloAlAnyadirLinkLogin
    Case erroressummit.TESConexionRemotoFailed
            basMensajes.FalloEnConexionConSrvRemoto TESErr.Arg1
    Case erroressummit.TESDesconexionRemotoFailed
            basMensajes.FalloEnConexionConSrvRemoto TESErr.Arg1
    Case erroressummit.TESRemotoConfigError
            basMensajes.FalloAlConfigurarSrvRemoto TESErr.Arg1
    Case erroressummit.TESLinkedServerDeleteError
            basMensajes.FalloAlBorrarLinkedServer TESErr.Arg2
    Case erroressummit.TESLinkedLoginDeleteError
            basMensajes.FalloAlBorrarLinkedLogin TESErr.Arg2
    Case erroressummit.TESOtroErrorEnLink
            basMensajes.OtroErrorDeSQLDMO TESErr.Arg2
    Case erroressummit.TESImposibleConectarALaCia
            basMensajes.ConexionIncorrecta
    Case erroressummit.TESPaisYaRelacionadoEnGS
            basMensajes.PaisYaRelacionado
    Case erroressummit.TESErrorSharePoint
            basMensajes.ImposibleActualizarSPTS
    Case erroressummit.TESProveYaRelacionadoEnGS
            basMensajes.ProveYaRelacionado TESErr.Arg1
    Case erroressummit.TESNIFRepetido
            basMensajes.NIFCiaRepetido TESErr.Arg1
    Case erroressummit.TESNIFProveRepetido
            basMensajes.NIFCiaProveRepetido TESErr.Arg1
    Case erroressummit.TESTelfNoValido
            basMensajes.TelefonoNoValido TESErr.Arg1
    Case erroressummit.TESEMailNoValido
            basMensajes.EMailNoValido TESErr.Arg1
End Select

End Sub

