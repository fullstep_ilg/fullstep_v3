Attribute VB_Name = "basUtilidades"
Option Explicit

Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

'Constantes de mensajes para el TreeView
Public Const TV_FIRST = &H1100
Public Const TVM_HITTEST = (TV_FIRST + 17)
Public Type POINTAPI
    X As Long
    Y As Long
End Type

'Estructura a pasar con el mensaje TVM_HITTEST
Public Type TVHITTESTINFO
    pt As POINTAPI
    Flags As Long
    hitem As Long
    
    
    
End Type

Public Const TVHT_ONITEMSTATEICON = &H40

''' <summary>
''' Crea el Objeto SmtClient
''' </summary>
''' <returns>Objeto SmtClient</returns>
''' <remarks>Llamada desde: pantallas q notifican ; Tiempo m�ximo: 0,2</remarks>
Public Sub IniciarSesionMail()

    Dim Email As Email
    Dim errormail As TipoErrorSummit
    
    On Error GoTo error
    
    Set Email = New Email
    
    Email.SmtpServer = g_udtParametrosGenerales.gsSMTPServer
    Email.SmtpPort = g_udtParametrosGenerales.giSMTPPort
    Email.SmtpAuthentication = g_udtParametrosGenerales.giSMTPAutent
    Email.SmtpEnableSsl = g_udtParametrosGenerales.gbSMTPSSL
    Email.SmtpDeliveryMethod = g_udtParametrosGenerales.giSMTPMetodoEntrega
    Email.SmtpUsu = g_udtParametrosGenerales.gsSMTPUser
    Email.SmtpPwd = g_udtParametrosGenerales.gsSMTPPwd 'Desencriptado desde CgestorParametros/DevolverParametrosGenerales
    Email.SmtpDominio = g_udtParametrosGenerales.gsSMTPDominio
    
    Email.ClaseCom = 1
    
    Set g_oMailSender = Email
    Exit Sub

error:
    MsgBox Err.Description, vbCritical, "FULLSTEP PS"
    Set g_oMailSender = Nothing
End Sub

''' <summary>
''' Este procedimiento libera el objeto g_oMailSender asign�ndolo a Nothing
''' </summary>
''' <remarks>Llamada desde: pantallas q notifican ; Tiempo m�ximo: 0,2</remarks>
Public Sub FinalizarMailSender()
    g_oMailSender.FuerzaFinalizeSmtpClient
    Set g_oMailSender = Nothing
End Sub

Public Function DblToStr(ByVal dblThatCanBeEmpty As Variant, Optional ByVal Formato As String) As String
    
    If IsEmpty(dblThatCanBeEmpty) Then
        DblToStr = ""
    Else
        If IsNull(dblThatCanBeEmpty) Then
            DblToStr = ""
        Else
            If Formato = "" Then
                DblToStr = Format(dblThatCanBeEmpty, "standard")
            Else
                DblToStr = Format(dblThatCanBeEmpty, Formato)
            End If
        End If
    End If
    
End Function


Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function
''' <summary>
''' Encripta un texto
''' </summary>
''' <param name="Dato">texto a encriptar</param>
''' <param name="Fecha">Si viene es la Fecha del pasword</param>
''' <returns>Texto encriptado</returns>
''' <remarks>Llamada desde: frmCambContr.cmdAceptar_Click ; Tiempo m�ximo: 0</remarks>
Public Function Encriptar(ByVal Dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt
    Dim diacrypt
    Dim oCrypt2 As CCrypt2

    If IsMissing(Fecha) Then
        fechahoracrypt = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            fechahoracrypt = DateSerial(1974, 3, 5)
        Else
            fechahoracrypt = Fecha
        End If
    End If
    
    diacrypt = Day(Fecha)
    
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = ClavePar5Bytes
    Else
        oCrypt2.Codigo = ClaveImp5bytes
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Encrypt
    Encriptar = oCrypt2.OutBuffer
            
    Set oCrypt2 = Nothing
End Function

''' <summary>
''' Desencripta un texto
''' </summary>
''' <param name="Dato">texto a desencriptar</param>
''' <param name="Fecha">Si viene es la Fecha del pasword</param>
''' <returns>Texto desencriptado</returns>
''' <remarks>Llamada desde: CRConector.Conectar basSubMain.Main     frmCompanias.cmdModificarUsu_Click
''' frmCompanias.GenerarMensajeAutTXT       frmCompanias.GenerarMensajeDesautTXT
''' frmCompanias.GenerarMensajeAutHTML      frmCompanias.GenerarMensajeDesautHTML
''' ; Tiempo m�ximo: 0</remarks>
Public Function DesEncriptar(ByVal Dato As String, Optional ByVal Fecha As Variant) As String
    Dim fechahoracrypt
    Dim diacrypt
    Dim oCrypt2 As CCrypt2
                
    If IsMissing(Fecha) Then
        fechahoracrypt = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            fechahoracrypt = DateSerial(1974, 3, 5)
        Else
            fechahoracrypt = Fecha
        End If
    End If
        
    diacrypt = Day(fechahoracrypt)
        
    Set oCrypt2 = New CCrypt2
    
    oCrypt2.InBuffer = Dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = ClavePar5Bytes
    Else
        oCrypt2.Codigo = ClaveImp5bytes
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Decrypt
    DesEncriptar = oCrypt2.OutBuffer
            
    Set oCrypt2 = Nothing
End Function

Function DateToSQLDate(DateThatCanBeEmpty) As Variant

If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDate = "NULL"
        Else
            DateToSQLDate = "'" & Format(DateThatCanBeEmpty, "dd/mm/yyyy") & "'"
        End If
    End If
End If

End Function

Function DateToSQLTimeDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLTimeDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLTimeDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLTimeDate = "NULL"
        Else
            DateToSQLTimeDate = "'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy hh:mm:ss") & "'"
        End If
    End If
End If

End Function

Public Function DblQuote(ByVal StrToDblQuote As String) As String

    Dim Ind As Long
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function

Public Function FormateoNumerico(ByVal dblNum As Double, Optional ByVal sFormato As String) As String
Dim dblRes As String
'Formato por defecto a 8 decimales
On Error GoTo error:

      If sFormato = "" Then
        dblRes = Format(dblNum, "#,##0.########")
      Else
        dblRes = Format(dblNum, sFormato)
      End If
      'Se quita el simbolo decimal si no hay decimales
      If Not IsNumeric(Right(dblRes, 1)) Then
        dblRes = Left(dblRes, Len(dblRes) - 1)
      End If
      FormateoNumerico = dblRes
      Exit Function
error:
If Err.Number = 13 Then
    dblRes = Format(dblNum, "#,##0.########")
    Resume Next
End If

End Function


Public Function NullToDbl0(ByVal ValueThatCanBeNull As Variant) As Variant
  
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0 = 0
    Else
        NullToDbl0 = ValueThatCanBeNull
    End If

End Function

Public Function DevolverPathFichTemp() As String

Dim sTempPath As String * 256
Dim sPath As String
Dim i As Integer
Dim bFinish As Boolean


    GetTempPath 256, sTempPath
        
        
        i = 256
        
        While i > 0 And Not bFinish
            
            If InStr(i, sTempPath, "\", vbTextCompare) <> 0 Then
                sPath = Mid(sTempPath, 1, i)
                bFinish = True
            End If
            i = i - 1
        Wend
        
        If Not bFinish Then
            sTempPath = "C:\"
            sPath = sTempPath
        End If
        
        DevolverPathFichTemp = sPath
        
End Function

''' <summary>
''' Envia un correo
''' </summary>
''' <param name="sTo">A quien/es va el correo</param>
''' <param name="sSubject">Subject del mail</param>
''' <param name="sBody">Cuerpo del mail</param>
''' <param name="sAttachments">Adjuntos del mail</param>
''' <param name="sCC">Carbon Copy del mail</param>
''' <param name="sBCC">Blind Carbon Copy del mail</param>
''' <param name="bReadReceipt">Pedir aviso de lectura si/no</param>
''' <param name="bHTML">Formato Html o text del mail</param>
''' <param name="Origen">Q comunicacion del mail</param>
''' <param name="Prove">Prove del mail</param>
''' <param name="NombreApellidos">nombre y Apellidos del contacto del mail</param>
''' <param name="lCia">Compania</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmMensaje  basUtilidades/ComponerMensaje; Tiempo m�ximo:0</remarks>
Public Function EnviarMensaje(ByVal sTo As String, ByVal sSubject As String, ByVal sBody As String, Optional ByVal sAttachments As Variant, Optional ByVal sCC As String, Optional ByVal sBCC As String, Optional ByVal bReadReceipt As Boolean, Optional ByVal bHTML As Boolean = True, Optional ByVal sReplyTo As String, Optional ByVal Cia As Long) As TipoErrorSummit

    Dim errormail As TipoErrorSummit
    Dim sTemp As String
    Dim ResulGrabaError As TipoErrorSummit
    Dim iError As Long
    Dim strError As String
    Dim i As Integer
    Dim auxAttachment As String
    Dim lIdError As Long
    Dim sRemitente As String
    On Error GoTo error

    errormail.NumError = TESnoerror
    sTemp = DevolverPathFichTemp

    If g_oMailSender Is Nothing Then
        IniciarSesionMail
    End If
           
    If sReplyTo = "" Then sReplyTo = g_udtParametrosGenerales.gsSMTPRespuestaMail

    If Not IsMissing(sAttachments) Then
        auxAttachment = ""
        For i = 0 To UBound(sAttachments) - 1
            auxAttachment = auxAttachment & sAttachments(i) & ";"
        Next
    End If
    sRemitente = SacarRemitentePortal(Cia)
    If sRemitente = "" Then
        sRemitente = g_udtParametrosGenerales.gsSMTPCuentaMail
    End If
    g_oMailSender.EnviarMail sRemitente, sRemitente, sSubject, sTo, sBody, sReplyTo, , sCC, sBCC, auxAttachment, sTemp, bHTML, , bReadReceipt, iError, strError
    
    If sBody <> "" Then
        If iError = 2 Then
            errormail = GrabarEnviarMensaje(sSubject, sTo, sCC, sBCC, g_udtParametrosGenerales.gsSMTPCuentaMail, sBody, bReadReceipt, bHTML, False, "", sAttachments, Cia)
        Else
            errormail = GrabarEnviarMensaje(sSubject, sTo, sCC, sBCC, g_udtParametrosGenerales.gsSMTPCuentaMail, sBody, bReadReceipt, bHTML, True, strError, sAttachments, Cia, "EnviarMensaje", "FSNLibraryCOM.Email Indica error", strError)
            
            If errormail.NumError = TESnoerror Then
                errormail.NumError = erroressummit.TESmailsmtp
                errormail.Arg1 = IIf(iError = 4, -1000000, IIf(iError = 5, -errormail.Arg1, errormail.Arg1))
                errormail.Arg2 = strError
            End If
        End If
    Else
        If iError = 2 Then
            errormail = GrabarEnviarMensaje(sSubject, sTo, sCC, sBCC, g_udtParametrosGenerales.gsSMTPCuentaMail, sBody, bReadReceipt, bHTML, True, "No hay cuerpo", sAttachments, Cia)
        Else
            errormail = GrabarEnviarMensaje(sSubject, sTo, sCC, sBCC, g_udtParametrosGenerales.gsSMTPCuentaMail, sBody, bReadReceipt, bHTML, True, "No hay cuerpo", sAttachments, Cia, "EnviarMensaje", "FSNLibraryCOM.Email Indica error", strError)
            
            If errormail.NumError = TESnoerror Then
                errormail.NumError = erroressummit.TESmailsmtp
                errormail.Arg1 = IIf(iError = 4, -1000000, IIf(iError = 5, -errormail.Arg1, errormail.Arg1))
                errormail.Arg2 = strError
            End If
        End If
    End If

    EnviarMensaje = errormail
    Exit Function

error:
    errormail.NumError = erroressummit.TESmailsmtp
    errormail.Arg1 = Err.Number
    errormail.Arg2 = Err.Description
    
    ResulGrabaError = GrabarEnviarMensaje(sSubject, sTo, sCC, sBCC, g_udtParametrosGenerales.gsSMTPCuentaMail, sBody, bReadReceipt, bHTML, True, errormail.Arg2, sAttachments, Cia, "EnviarMensaje", CStr(Err.Number), Err.Description)
    
    EnviarMensaje = errormail

    Exit Function

End Function

''' <summary>
''' Envia un correo
''' </summary>
''' <param name="sTo">A quien/es va el correo</param>
''' <param name="sSubject">Subject del mail</param>
''' <param name="sBody">Cuerpo del mail</param>
''' <param name="sAttachments">Adjuntos del mail</param>
''' <param name="Origen">Q comunicacion del mail</param>
''' <param name="lCia">Compania</param>
''' <param name="bHTML">Formato Html o text del mail</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmMensajes ; Tiempo m�ximo: 0,2</remarks>
Public Function ComponerMensaje(ByVal sTo As String, ByVal sSubject As String, ByVal sBody As String, Optional ByVal sAttachments As Variant, Optional ByVal sOrigen As Variant, Optional ByVal lCia As Long, Optional ByVal bHTML As Boolean) As TipoErrorSummit
    Dim errormail As TipoErrorSummit
    Dim i As Integer
    Dim auxAttachment As String
    Dim sTemp As String

    errormail.NumError = TESnoerror
    
    If g_udtParametrosGenerales.gbMostrarMail = True Then
        DoEvents
        frmMensaje.txtAsunto = sSubject
        DoEvents
        frmMensaje.txtDestino = sTo
        DoEvents
        
        frmMensaje.g_sCuerpo = sBody
        frmMensaje.g_bHTML = bHTML
        frmMensaje.g_idCiaSeleccionada = lCia
        
        If Not IsMissing(sAttachments) Then
            For i = 0 To UBound(sAttachments) - 1
                frmMensaje.lstvwAdjuntos.ListItems.Add , "ADJ" & CStr(i), sAttachments(i), , "ADJ"
            Next
        End If
        If Not IsMissing(sOrigen) Then
            frmMensaje.g_sOrigen = sOrigen
        End If
        
        frmMensaje.CargarCuerpoMensaje
        
        frmMensaje.Show vbModal
    Else
    
        If Not IsMissing(sAttachments) Then
            auxAttachment = ""
            sTemp = DevolverPathFichTemp
            For i = 0 To UBound(sAttachments) - 1
                auxAttachment = auxAttachment & sTemp & sAttachments(i) & ";"
            Next
        End If
        
        If Not ComprobarEmail(sTo) Then
            errormail.NumError = erroressummit.TESmailsmtp
            errormail.Arg1 = -1000000
            errormail.Arg2 = sTo & ",###1"
            
            ComponerMensaje = errormail
            
            Exit Function
        End If
    
        errormail = EnviarMensaje(sTo, sSubject, sBody, sAttachments, Trim(g_udtParametrosGenerales.gsSMTPCC), Trim(g_udtParametrosGenerales.gsSMTPCCO), g_udtParametrosGenerales.gbAcuseRecibo, bHTML, Trim(g_udtParametrosGenerales.gsSMTPRespuestaMail), lCia)
    End If
    
    ComponerMensaje = errormail
    
End Function

Private Function SacarRemitentePortal(Cia As Long) As String
Dim oCia As CCia
Dim sRemitente As String

Set oCia = g_oRaiz.Generar_CCia
oCia.ID = Cia
sRemitente = oCia.SacarRemitente
SacarRemitentePortal = IIf(sRemitente <> "", sRemitente, g_udtParametrosGenerales.gsSMTPCuentaMail)
End Function

Public Function SQLBinaryToBoolean(ByVal var As Variant) As Variant
    
    If IsNull(var) Then
        SQLBinaryToBoolean = False
    Else
        If var <> 0 Then
            SQLBinaryToBoolean = True
        Else
            SQLBinaryToBoolean = False
        End If
    End If

End Function

Public Function BooleanToSQLBinary(ByVal b As Boolean) As Variant
    
    If b Then
        BooleanToSQLBinary = 1
    Else
        BooleanToSQLBinary = 0
    End If
    
End Function

Public Function StrToNull(ByVal str As Variant) As Variant
    
    If str = "" Then
        StrToNull = Null
    Else
        If IsNull(str) Then
            StrToNull = Null
        Else
            StrToNull = str
        End If
    End If
    
End Function

Public Function EliminarEspacioYTab(ByVal sTexto As String) As String
    EliminarEspacioYTab = Trim(Replace(sTexto, Chr(9), " "))
End Function

''' <summary>Comprueba que el tel�fono sea un n� de telf. correcto.</summary>
''' <param name="sTelefono">Tel�fono</param>
''' <param name="sPais">Pa�s</param>
''' <returns>Objeto de tipo CTESError con el error si se ha producido</returns>
''' <remarks>Llamada desde=CUsuario.AnyadirUsuario y CUsuario.ModificarDatosUsuario; Tiempo m�ximo=0seg.</remarks>

Public Function ComprobarTelefono(ByVal sTelefono As String, ByVal sPais As String) As Boolean
    Dim sPhone As String
    Dim iBracket As Integer
    Dim iBrChr As Integer
    Dim iBrChr2 As Integer
    Dim sBrCadena As String
    Dim iGuion As Integer
    Dim sCadena As String
    Dim iLongCadena As Integer
    Dim sDigits As String
    Dim sPhoneNumberDelimiters As String
    Dim sValidWorldPhoneChars As String
    Dim iDigitsInIPhoneNumber As Integer
    Dim bValido As Boolean
    
    On Error GoTo error
    
    bValido = True
    
    iBracket = 3
    sPhone = Trim(sTelefono)
    If UBound(Split(sPhone, "+")) > 1 Then   'Si hay m�s de un +  false
        bValido = False
    ElseIf UBound(Split(sPhone, "(")) > 1 Then  'Si hay m�s de un (  false
        bValido = False
    ElseIf UBound(Split(sPhone, ")")) > 1 Then  'Si hay m�s de un )  false
        bValido = False
    End If
    If bValido Then
        If InStr(1, sPhone, "-") > 0 Then iBracket = iBracket + 1  'En bracket + 1 si hay guiones
        
        iBrChr = InStr(1, sPhone, "(")
                
        If iBrChr > 0 Then
'            If InStr(1, sPhone, "(+") > 0 Then
'                If Mid(sPhone, iBrChr + 4, 1) <> ")" Then bValido = False
'            Else    'Si hay ( y dos caracteres despues no hay ) --> false
'                If Mid(sPhone, iBrChr + 3, 1) <> ")" Then bValido = False
'            End If

            iBrChr2 = InStr(1, sPhone, ")")
        
            If iBrChr2 > iBrChr Then    'Primero ( y despu�s )
                'Obtener el string entre los 2 par�ntesis
                sBrCadena = Mid(sPhone, iBrChr + 1, iBrChr2 - iBrChr - 1)
                If UBound(Split(sBrCadena, "-")) > 1 Then
                    bValido = False  'M�s de un gui�n -->False
                Else
                    iGuion = InStr(1, sBrCadena, "-")
                    If iGuion > 0 Then
                        If PaisEspanna(sPais) Then
                            bValido = False
                        Else
                            sBrCadena = Left(sBrCadena, iGuion - 1) & Right(sBrCadena, Len(sBrCadena) - iGuion)
                        End If
                    End If
                    If InStr(1, sBrCadena, "+") > 0 Then bValido = False
                    bValido = bValido And IsNumeric(sBrCadena)
                End If
            Else
                bValido = False
            End If
        ElseIf iBrChr - 1 And InStr(1, sPhone, ")") > 0 Then    'Si no hay ( y s� hay ) -->  false
            bValido = False
        End If
        
        If bValido Then
            sDigits = "0123456789"
            sPhoneNumberDelimiters = "()- "    'non-digit characters which are allowed in phone numbers
            sValidWorldPhoneChars = sPhoneNumberDelimiters & "+"    'characters which are allowed in international phone
            iDigitsInIPhoneNumber = 9
    
            'Deja s�lo los numeros
            sCadena = StripCharsInBag(sPhone, sValidWorldPhoneChars)
            iLongCadena = Len(sCadena)
            If PaisEspanna(sPais) Then
                If sBrCadena <> "" Then
                    If iLongCadena = 11 Then
                        If Not (Mid(sPhone, 1, 2) = "34" Or Mid(sPhone, 1, 3) = "+34" Or Mid(sPhone, 1, 4) = "(34)" Or Mid(sPhone, 1, 5) = "+(34)") Then bValido = False
                    Else
                        bValido = False
                    End If
                Else
                    If iLongCadena > 9 Then
                        If iLongCadena = 11 Then
                            If Not (Mid(sPhone, 1, 2) = "34" Or Mid(sPhone, 1, 3) = "+34" Or Mid(sPhone, 1, 4) = "(34)" Or Mid(sPhone, 1, 5) = "+(34)") Then bValido = False
                        Else
                            bValido = False
                        End If
                    Else
                        If iLongCadena <> 9 Then
                            bValido = False
                        Else
                            If (Mid(sPhone, 1, 2) = "34" Or Mid(sPhone, 1, 3) = "+34" Or Mid(sPhone, 1, 4) = "(34)" Or Mid(sPhone, 1, 5) = "+(34)") Then
                                bValido = False
                            Else
                                If InStr(1, sPhone, "+") > 0 Then
                                    If InStr(1, sPhone, "+") > 1 Then
                                        bValido = False
                                    Else
                                        If Not InStr(1, sPhone, "+34") > 0 Then bValido = False
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
            
            'Si es entero y de longitud >= minimo numero de digitos --> true
            bValido = bValido And IsNumeric(sCadena) 'And iLongCadena >= iDigitsInIPhoneNumber
        End If
    End If
    
    ComprobarTelefono = bValido
    Exit Function
    
error:
    On Error Resume Next
    basMensajes.OtroError Err.Number, Err.Description
    ComprobarTelefono = False
End Function

''' <summary>Busca en sCadena los caracteres que no est�n en sBolsa.</summary>
''' <param name="sCadena">String cuyos caracteres se van a revisar.</param>
''' <param name="sBolsa">String que contiene los caracteres que se van a buscar.</param>
''' <returns>String con los caracteres no encontrados</returns>
''' <remarks>Llamada desde=ComprobarTelefono; Tiempo m�ximo=0seg.</remarks>

Private Function StripCharsInBag(ByVal sCadena As String, ByVal sBolsa As String) As String
    Dim iIndex As Integer
    Dim sReturnString As String
    Dim sCar As String
    
    On Error GoTo error
    
    'Search through string's characters one by one.
    'If character is not in bag, append to returnString.
    For iIndex = 1 To Len(sCadena)
        'Check that current character isn't whitespace.
        sCar = Mid(sCadena, iIndex, 1)
        If InStr(1, sBolsa, sCar) = 0 Then
            sReturnString = sReturnString & sCar
        End If
    Next
    
    StripCharsInBag = sReturnString
    Exit Function
    
error:
    On Error Resume Next
    basMensajes.OtroError Err.Number, Err.Description
End Function

''' <summary>Comprueba que el eMail sea una dir. de correo electr�nico correcta.</summary>
''' <param name="sEmail">E-mail.</param>
''' <param name="bValidarDom">Indica si hay que validar el dominio.</param>
''' <returns>Si el email es correcto o no</returns>
''' <remarks>Llamada desde=; Tiempo m�ximo=0seg.</remarks>

Public Function ComprobarEmail(ByVal sEmail As String, Optional ByVal bValidarDom As Boolean = False) As Boolean
    On Error GoTo error
    
    Dim oExp As RegExp
    Dim ListaMail() As String
    Dim iMail As Integer
    
    ComprobarEmail = True
    
    sEmail = Trim(sEmail)
    If Right(sEmail, 1) = ";" Then sEmail = Left(sEmail, Len(sEmail) - 1)
    ListaMail = Split(sEmail, ";")
    
    Set oExp = New RegExp
    'Se permite una domain extension "travelersinsurance"->18 caracteres
    oExp.Pattern = "^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,18})+$"
    For iMail = 0 To UBound(ListaMail)
        If Not oExp.Test(Trim(ListaMail(iMail))) Then
            ComprobarEmail = False
        End If
    Next

    If ComprobarEmail And bValidarDom Then
        Dim sDominio As String
        Dim iPos As Integer

        iPos = InStr(1, sEmail, "@")
        sDominio = Right(sEmail, Len(sEmail) - iPos)
    End If
  
    Exit Function
error:
    On Error Resume Next
    ComprobarEmail = False
    basMensajes.OtroError Err.Number, Err.Description
End Function

''' <summary>Elimina de una cadena de caracteres los caracteres que no sean letras o n�meros</summary>
''' <param name="sCadena">Cadena de caracteres a procesar</param>
''' <returns>Cadena de caracteres procesada</returns>
''' <remarks>Llamada desde sdbgDirecciones_KeyDown</remarks>

Public Function EliminarCaracteresEspeciales(ByVal sCadena As String) As String
    Dim sCadenaAux As String
    Dim sChar As String
    Dim iASC As Integer
    Dim i As Integer
    
    If sCadena <> vbNullString Then
        'ASCII 48 a 57 -> n�meros
        'ASCII 65 a 90 -> may�sculas
        'ASCII 97 a 122 -> min�sculas
        
        For i = 1 To Len(sCadena)
            sChar = Mid(sCadena, i, 1)
            iASC = Asc(sChar)
            If (iASC > 47 And iASC < 58) Or (iASC > 64 And iASC < 91) Or (iASC > 96 And iASC < 123) Then
                sCadenaAux = sCadenaAux & sChar
            Else
                'vocales acentuadas
                If iASC = 225 Then sCadenaAux = sCadenaAux & "a"  '�
                If iASC = 233 Then sCadenaAux = sCadenaAux & "e"  '�
                If iASC = 237 Then sCadenaAux = sCadenaAux & "i"  '�
                If iASC = 243 Then sCadenaAux = sCadenaAux & "o"  '�
                If iASC = 250 Then sCadenaAux = sCadenaAux & "u"  '�
                If iASC = 193 Then sCadenaAux = sCadenaAux & "A"  '�
                If iASC = 201 Then sCadenaAux = sCadenaAux & "E"  '�
                If iASC = 205 Then sCadenaAux = sCadenaAux & "I"  '�
                If iASC = 211 Then sCadenaAux = sCadenaAux & "O"  '�
                If iASC = 218 Then sCadenaAux = sCadenaAux & "U"  '�
            End If
        Next
    End If
    
    EliminarCaracteresEspeciales = sCadenaAux
End Function

''' <summary>
''' Graba el envio de mensaje en REGISTRO_MAIL.
''' </summary>
''' <param name="sSubject">Subject del mail</param>
''' <param name="sTo">A quien/es va el correo</param>
''' <param name="sCC">Carbon Copy del mail</param>
''' <param name="sBCC">Blind Carbon Copy del mail</param>
''' <param name="sReplyTo">De quien es el correo</param>
''' <param name="sMessage">Cuerpo del mail</param>
''' <param name="bReadReceipt">Pedir aviso de lectura si/no</param>
''' <param name="bHTML">HTML</param>
''' <param name="bKO">KO</param>
''' <param name="sTextoError">TextoError</param>
''' <param name="sAttachments">Attachments</param>
''' <param name="Cia">Codigo de compania</param>
''' <param name="Pagina">Funci�n o pantalla q da el error</param>
''' <param name="NumError">Numero de Error VB. El Id de la tabla va en teserror.arg1</param>
''' <param name="Message">Mensaje del error VB</param>
''' <returns>error de haberlo</returns>
''' <remarks>Llamada desde:basUtilidades.EnviarMensaje</remarks>
Private Function GrabarEnviarMensaje(ByVal sSubject As String, ByVal sTo As String, ByVal sCC As String, ByVal sBCC As String, ByVal sReplyTo As String _
, ByVal sMessage As String, ByVal bReadReceipt As Boolean, ByVal bHTML As Boolean, ByVal bKO As Boolean, ByVal sTextoError As String _
, ByVal sAttachments As Variant, ByVal Cia As Long, Optional ByVal Pagina As String = "", Optional ByVal NumError As String = "" _
, Optional ByVal Message As String = "") As TipoErrorSummit
    Dim errormail As TipoErrorSummit
    Dim oRegistro As CRegistroEmail
    Dim sTemp As String
    Dim i As Integer
    Dim datafile As Integer
    Dim lFl As Long
    
    Set oRegistro = g_oRaiz.Generar_CRegistroEmail
    
    oRegistro.Para = sTo
    oRegistro.Subject = sSubject
    oRegistro.Cuerpo = sMessage
    oRegistro.CC = StrToNull(sCC)
    oRegistro.CCO = StrToNull(sBCC)
    oRegistro.ResponderA = StrToNull(sReplyTo)
    oRegistro.Tipo = BooleanToSQLBinary(bHTML)
    oRegistro.AcuseRecibo = BooleanToSQLBinary(bReadReceipt)
    oRegistro.Cia = Cia
    
    sTemp = DevolverPathFichTemp
    
    'adjuntos   ****** FALTAN ********************
    If Not IsMissing(sAttachments) Then
        Set oRegistro.Adjuntos = g_oRaiz.Generar_CAdjuntosEmail
        For i = 0 To UBound(sAttachments) - 1
            datafile = 1
            'Abrimos el fichero para lectura binaria
            Open sTemp & sAttachments(i) For Binary Access Read As datafile
            lFl = LOF(datafile)    ' Length of data in file
            If lFl > 0 Then
                oRegistro.Adjuntos.Add oRegistro, i + 1, sAttachments(i), Date, , lFl
            End If
            Close datafile
        Next
    End If

    oRegistro.KO = bKO
    oRegistro.TextoError = sTextoError
    oRegistro.Producto = 1
    'es adm luego no hay oRegistro.Usuario = Usuario

    If Not (oRegistro Is Nothing) Then
        errormail = oRegistro.AnyadirComunicacion(sTemp, Pagina, NumError, Message)
    End If
    Set oRegistro = Nothing

    GrabarEnviarMensaje = errormail
End Function

Public Function PaisEspanna(ByVal sPais As String) As Boolean
    If InStr(1, UCase(sPais), "ESPA�A") > 0 Or InStr(1, UCase(sPais), "SPAIN") > 0 Or InStr(1, UCase(sPais), "SPANIEN") > 0 Or InStr(1, UCase(sPais), "ESPANYA") > 0 Then
        PaisEspanna = True
    Else
        PaisEspanna = False
    End If
End Function
