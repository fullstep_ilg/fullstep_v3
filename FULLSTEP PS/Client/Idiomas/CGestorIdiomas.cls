VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGestorIdiomas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private adoCon As New ADODB.Connection

''' <summary>
''' Cargar en un Recordeset los textos (o uno en concreto) de un modulo dado
''' </summary>
''' <param name="FormId">Formulario para el q se estan cargando textos</param>
''' <param name="Idioma">Idioma</param>
''' <param name="ControlId">Id del texto en concreto q se quiere cargar</param>
''' <returns>Recordeset con los textos</returns>
''' <remarks>Llamada desde: Todos las funciones "CargarRecursos" de todos los frm de la aplicaci�n; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Public Function DevolverTextosDelModulo(ByVal FormId As ModuleName, ByVal Idioma As String, Optional ByVal ControlId As Variant) As ADODB.Recordset
Dim adores As ADODB.Recordset
Dim sDen As String

    sDen = "TEXT_" & Idioma
    
    'Calidad sin With(nolock) pq se accede a bbdd EXCEL y de ponerlo fallar�a
    sConsulta = "SELECT " & sDen & " FROM TEXTOS WHERE MODULO=" & FormId
    If Not IsMissing(ControlId) Then
        If IsNumeric(ControlId) Then
            sConsulta = sConsulta & " AND ID = " & ControlId
        Else
            Set DevolverTextosDelModulo = Nothing
            Exit Function
        End If
    End If
    sConsulta = sConsulta & " ORDER BY MODULO,ID"
    Set adores = New ADODB.Recordset
    
    adores.Open sConsulta, adoCon, adOpenForwardOnly, adLockReadOnly
    
    Set adores.ActiveConnection = Nothing
    
    Set DevolverTextosDelModulo = adores
    

End Function
Public Sub Inicializar()
Dim adores As ADODB.Recordset

    On Error GoTo error:
    
    'Create the connection.
    adoCon.Open "Provider=Microsoft.Jet.OLEDB.4.0;" & _
    "Data Source=" & App.Path & "\FSINTCAP.MDB;"
    
    adoCon.CursorLocation = adUseClient
    
    Exit Sub

error:
    
    Err.Raise 10000, "FSPSIdiomas", "Cannot connect to FSINTCAP.MDB"
    
    

End Sub


Private Sub Class_Terminate()
    
    adoCon.Close
    
End Sub
