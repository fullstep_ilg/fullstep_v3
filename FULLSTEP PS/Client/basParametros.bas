Attribute VB_Name = "basParametros"
Public g_iCargaMaximaCombos As Integer
Public g_sRptPath As String
Public g_sPlantillasPath As String

Public g_udtParametrosGenerales As ParametrosGenerales

'Indica el tamanyo de buffer de lectura de ficheros
Public Const giChunkSize As Integer = 16384


Public Sub CargarParametrosInstalacion()

Dim bGuardar As Boolean
Dim sTemp As String

On Error Resume Next
    
    'Carga m�xima de los combos
    sTemp = GetStringSetting("FULLSTEP PS", g_sInstancia, "Parametros", "CargaMaximaCombos")
    
    If sTemp = "" Then
        g_iCargaMaximaCombos = 500
        bGuardar = True
    Else
        If IsNumeric(sTemp) Then
            g_iCargaMaximaCombos = CInt(sTemp)
        Else
            g_iCargaMaximaCombos = 500
        End If
    End If
    
    'Path de los informes
    Err.Clear
    g_sRptPath = GetStringSetting("FULLSTEP PS", g_sInstancia, "Parametros", "rptPath")
    If g_sRptPath <> "" Then
        g_udtParametrosGenerales.g_sRptPath = g_sRptPath
        Set oGestorParametros = g_oRaiz.Generar_CGestorParametros
        oGestorParametros.GuardarParametrosGenerales g_udtParametrosGenerales, g_sInstancia, m_iNivel, g_sServidor, g_sBaseDeDatos
        DeleteKey "FULLSTEP PS", g_sInstancia, "Parametros", "rptPath"
    Else
        g_sRptPath = g_udtParametrosGenerales.g_sRptPath
    End If
    
    
    Err.Clear
    g_sPlantillasPath = GetStringSetting("FULLSTEP PS", g_sInstancia, "Parametros", "plantillasPath")
    If g_sPlantillasPath <> "" Then
        g_udtParametrosGenerales.g_sMailPath = g_sPlantillasPath
        If oGestorParametros Is Nothing Then
            Set oGestorParametros = g_oRaiz.Generar_CGestorParametros
        End If
        oGestorParametros.GuardarParametrosGenerales g_udtParametrosGenerales, g_sInstancia, m_iNivel, g_sServidor, g_sBaseDeDatos
        DeleteKey "FULLSTEP PS", g_sInstancia, "Parametros", "plantillasPath"
    Else
        g_sPlantillasPath = g_udtParametrosGenerales.g_sMailPath
    End If
    
    Set oGestorParametros = Nothing
    
    If bGuardar Then
        GuardarParametrosInstalacion
    End If
    
End Sub

Public Function GuardarParametrosInstalacion()

    SaveStringSetting "FULLSTEP PS", g_sInstancia, "Parametros", "CargaMaximaCombos", basParametros.g_iCargaMaximaCombos
    SaveStringSetting "FULLSTEP PS", g_sInstancia, "Parametros", "rptPath", basParametros.g_sRptPath
 
 End Function
 


