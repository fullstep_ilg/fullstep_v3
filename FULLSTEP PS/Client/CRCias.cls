VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRCIas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Sub ListadoCompa�ias(oReport As CRAXDRT.Report, Optional ByVal premiun As Boolean, Optional ByVal OrdenarPorDen As Boolean, Optional ByVal OrdenarPorCod As Boolean, _
                            Optional ByVal Cod As String, Optional ByVal Den As String, Optional ByVal ValHasta As Double, Optional ByVal Val As Double, Optional ByVal Hom As Variant, _
                            Optional ByVal CliRef As Variant, Optional ByVal opDetalle As Boolean, Optional ByVal opResum As Boolean, Optional ByVal PaiCod As Integer, Optional ByVal MonCod As Integer, _
                            Optional ByVal ProviCod As Integer, Optional ByVal FunProAut As Boolean, Optional ByVal FunProDes As Boolean, Optional ByVal FunProPen As Boolean, _
                            Optional ByVal ACN1 As String, Optional ByVal ACN2 As String, Optional ByVal ACN3 As String, Optional ByVal ACN4 As String, Optional ByVal ACN5 As String, _
                            Optional ByVal FecAutDesde As Variant, Optional ByVal FecAutHasta As Variant, Optional ByVal FecDesAutDesde As Variant, _
                            Optional ByVal FecDesAutHasta As Variant, Optional ByVal FecSolDesde As Variant, Optional ByVal FecSolHasta As Variant)
                            

    Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSortFields As String
    Dim sSQL As String
    Dim sRecordSelectionFormula As String
    Dim GroupOrder1 As String
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    sSQL = ObtenerSQLCias(premiun, OrdenarPorDen, OrdenarPorCod, _
                            Cod, Den, ValHasta, Val, Hom, _
                            CliRef, opDetalle, opResum, PaiCod, MonCod, ProviCod, _
                            FunProAut, FunProDes, FunProPen, ACN1, ACN2, ACN3, ACN4, ACN5, , , , FecAutDesde, FecAutHasta, FecDesAutDesde, FecDesAutHasta, FecSolDesde, FecSolHasta)
    oReport.SQLQueryString = sSQL
    
End Sub

''' <summary>
''' Crear la consulta para el listado de compa�ias
''' </summary>
''' <param name="premiun">si es premiun o no</param>
''' <param name="OrdenarPorDen">Ordenar por denominacion</param>
''' <param name="OrdenarPorCod">Ordenar por codigo</param>
''' <param name="Cod">codigo de compa�ia</param>
''' <param name="Den">denominacion de compa�ia</param>
''' <param name="ValHasta">Volumen de facturacion hasta</param>
''' <param name="Val">Volumen de facturacion desde</param>
''' <param name="Hom">Homologaci�n de compa�ia</param>
''' <param name="CliRef">cliente de referencia de compa�ia</param>
''' <param name="opDetalle">si es Consulta detallada</param>
''' <param name="opResum">si es resumido</param>
''' <param name="sPaiCod">Pais de compa�ia</param>
''' <param name="sMonCod">Moneda de compa�ia</param>
''' <param name="sProviCod">Provincia de compa�ia</param>
''' <param name="FunProAut">si se listan los q cumplan CIAS_PORT.FPEST=3/param>
''' <param name="FunProDes">si se listan los q cumplan CIAS_PORT.FPEST=2</param>
''' <param name="FunProPen">si se listan los q cumplan CIAS_PORT.FPEST=1</param>
''' <param name="ACN1">Actividad del compa�ia (nivel 1)</param>
''' <param name="ACN2">Actividad del compa�ia (nivel 2)</param>
''' <param name="ACN3">Actividad del compa�ia (nivel 3)</param>
''' <param name="ACN4">Actividad del compa�ia (nivel 4)</param>
''' <param name="ACN5">Actividad del compa�ia (nivel 5</param>
''' <param name="Usu">Usuario</param>
''' <param name="UsuDet">Usuario detalle</param>
''' <param name="Activ">Activo</param>
''' <param name="FecAutDesde">Fecha autorizaci�n desde</param>
''' <param name="FecAutHasta">Fecha autorizaci�n hasta</param>
''' <param name="FecDesAutDesde">Fecha desautorizaci�n desde</param>
''' <param name="FecDesAutHasta">Fecha desautorizaci�n hasta</param>
''' <param name="FecSolDesde">Fecha pendiente autorizaci�n desde</param>
''' <param name="FecSolHasta">Fecha pendiente autorizaci�n hasta</param>
''' <returns>String con la consulta</returns>
''' <remarks>Llamada desde: ListadoCompa�ias ; Tiempo m�ximo: 0</remarks>
''' Revisado por: Jbg. Fecha: 03/07/2012
Private Function ObtenerSQLCias(Optional ByVal premiun As Boolean, Optional ByVal OrdenarPorDen As Boolean, Optional ByVal OrdenarPorCod As Boolean, _
    Optional ByVal Cod As String, Optional ByVal Den As String, Optional ByVal ValHasta As Double, Optional ByVal Val As Double, Optional ByVal Hom As String, _
    Optional ByVal CliRef As String, Optional ByVal opDetalle As Boolean, Optional ByVal opResum As Boolean, Optional ByVal sPaiCod As Integer, Optional ByVal sMonCod As Integer, _
    Optional ByVal sProviCod As Integer, Optional ByVal FunProAut As Boolean, Optional ByVal FunProDes As Boolean, _
    Optional ByVal FunProPen As Boolean, Optional ByVal ACN1 As String, Optional ByVal ACN2 As String, Optional ByVal ACN3 As String, Optional ByVal ACN4 As String, Optional ByVal ACN5 As String, _
    Optional ByVal Usu As Boolean, Optional ByVal UsuDet As Boolean, Optional ByVal Activ As Boolean, _
    Optional ByVal FecAutDesde As Variant, Optional ByVal FecAutHasta As Variant, Optional ByVal FecDesAutDesde As Variant, _
    Optional ByVal FecDesAutHasta As Variant, Optional ByVal FecSolDesde As Variant, Optional ByVal FecSolHasta As Variant) As String

    Dim sConsulta As String

    If opResum Then   'si es resumido
        sConsulta = "SELECT CIAS.COD,CIAS.DEN,CIAS.DIR,CIAS.POB,CIAS.INSTANCIA_VINCULADA as PM_VINCULADA FROM CIAS WITH(NOLOCK) "
    Else
        'Consulta detallada
        sConsulta = "SELECT CIAS.COD,CIAS.DEN,CIAS.DIR,CIAS.NIF,CIAS.CP,CIAS.URLCIA,CIAS.INSTANCIA_VINCULADA as PM_VINCULADA,CIAS.VOLFACT,CIAS.COMENT,CIAS.HOM1,CIAS.HOM2,CIAS.HOM3,CIAS.CLIREF1,CIAS.CLIREF2,CIAS.CLIREF3,CIAS.CLIREF4,PROVI.DEN,PAI.DEN,CIAS_ESP.NOM,CIAS_ESP.COM,REL_CIAS.COD_PROVE_CIA"
        sConsulta = sConsulta & " FROM CIAS WITH(NOLOCK)"
        sConsulta = sConsulta & " LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI = PROVI.PAI AND CIAS.PROVI=PROVI.ID"
        sConsulta = sConsulta & " LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID"
        sConsulta = sConsulta & " LEFT JOIN CIAS_ESP WITH(NOLOCK) ON CIAS.ID=CIAS_ESP.CIA"
    End If

    sConsulta = sConsulta & " INNER JOIN CIAS_PORT WITH(NOLOCK) ON CIAS.ID=CIAS_PORT.CIA AND CIAS.PORT=CIAS_PORT.PORT"

    If ACN1 <> "" And ACN2 <> "" And ACN3 <> "" And ACN4 <> "" And _
            ACN5 <> "" Then
                sConsulta = sConsulta & " INNER JOIN CIAS_ACT5 WITH(NOLOCK) ON CIAS.ID= CIAS_ACT5.CIA "
                sConsulta = sConsulta & " AND CIAS_ACT5.ACT1= " & CInt(ACN1)
                sConsulta = sConsulta & " AND CIAS_ACT5.ACT2= " & CInt(ACN2)
                sConsulta = sConsulta & " AND CIAS_ACT5.ACT3= " & CInt(ACN3)
                sConsulta = sConsulta & " AND CIAS_ACT5.ACT4= " & CInt(ACN4)
                sConsulta = sConsulta & " AND CIAS_ACT5.ACT5= " & CInt(ACN5)
        Else
            If ACN1 <> "" And ACN2 <> "" And ACN3 <> "" And ACN4 <> "" Then
                sConsulta = sConsulta & " INNER JOIN CIAS_ACT4 WITH(NOLOCK) ON CIAS.ID= CIAS_ACT4.CIA "
                sConsulta = sConsulta & " AND CIAS_ACT4.ACT1= " & CInt(ACN1)
                sConsulta = sConsulta & " AND CIAS_ACT4.ACT2= " & CInt(ACN2)
                sConsulta = sConsulta & " AND CIAS_ACT4.ACT3= " & CInt(ACN3)
                sConsulta = sConsulta & " AND CIAS_ACT4.ACT4= " & CInt(ACN4)
            Else
                If ACN1 <> "" And ACN2 <> "" And ACN3 <> "" Then
                    sConsulta = sConsulta & " INNER JOIN CIAS_ACT3 WITH(NOLOCK) ON CIAS.ID= CIAS_ACT3.CIA "
                    sConsulta = sConsulta & " AND CIAS_ACT3.ACT1= " & CInt(ACN1)
                    sConsulta = sConsulta & " AND CIAS_ACT3.ACT2= " & CInt(ACN2)
                    sConsulta = sConsulta & " AND CIAS_ACT3.ACT3= " & CInt(ACN3)
                Else
                    If ACN1 <> "" And ACN2 <> "" Then
                        sConsulta = sConsulta & " INNER JOIN CIAS_ACT2 WITH(NOLOCK) ON CIAS.ID= CIAS_ACT2.CIA "
                        sConsulta = sConsulta & " AND CIAS_ACT2.ACT1= " & CInt(ACN1)
                        sConsulta = sConsulta & " AND CIAS_ACT2.ACT2= " & CInt(ACN2)
                    Else
                       If ACN1 <> "" Then
                            sConsulta = sConsulta & " INNER JOIN CIAS_ACT1 WITH(NOLOCK) ON CIAS.ID= CIAS_ACT1.CIA "
                            sConsulta = sConsulta & " AND CIAS_ACT1.ACT1= " & CInt(ACN1)
                        End If
                    End If
                End If
           End If
    End If
        
    sConsulta = sConsulta & " LEFT JOIN REL_CIAS WITH(NOLOCK) ON CIAS.ID=REL_CIAS.CIA_PROVE AND REL_CIAS.CIA_COMP = " & basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora
    
    sConsulta = sConsulta & " WHERE 1=1"
    
    If FunProPen Or FunProDes Or FunProAut Then
        sConsulta = sConsulta & " AND ("

        If FunProPen = True Then
            sConsulta = sConsulta & " (CIAS_PORT.FPEST=1"
            If FecSolDesde <> "" Then
                sConsulta = sConsulta & " AND CIAS_PORT.FECINS>=" & DateToSQLTimeDate(FecSolDesde)
            End If
            If FecSolHasta <> "" Then
                sConsulta = sConsulta & " AND CIAS_PORT.FECINS<=DATEADD(s,86399,Convert(datetime,'" & Format(FecSolHasta, "mm-dd-yyyy") & "',110))"
            End If
            sConsulta = sConsulta & ")"
        End If
        If FunProDes = True Then
            If FunProPen = True Then sConsulta = sConsulta & " OR"
            sConsulta = sConsulta & "  (CIAS_PORT.FPEST=2"
            If FecDesAutDesde <> "" Then
                sConsulta = sConsulta & " AND CIAS_PORT.FECDESACTFP>=" & DateToSQLTimeDate(FecDesAutDesde)
            End If
            If FecDesAutHasta <> "" Then
                sConsulta = sConsulta & " AND CIAS_PORT.FECDESACTFP<=DATEADD(s,86399,Convert(datetime,'" & Format(FecDesAutHasta, "mm-dd-yyyy") & "',110))"
            End If
            sConsulta = sConsulta & ")"
        End If
        If FunProAut = True Then
            If FunProPen = True Or FunProDes = True Then sConsulta = sConsulta & " OR"
            sConsulta = sConsulta & " (CIAS_PORT.FPEST=3"
            If FecAutDesde <> "" Then
                sConsulta = sConsulta & " AND CIAS_PORT.FECACTFP>=" & DateToSQLTimeDate(FecAutDesde)
            End If
            If FecAutHasta <> "" Then
                sConsulta = sConsulta & " AND CIAS_PORT.FECACTFP<=DATEADD(s,86399,Convert(datetime,'" & Format(FecAutHasta, "mm-dd-yyyy") & "',110))"
            End If
            sConsulta = sConsulta & ")"
        End If
        sConsulta = sConsulta & " )"
    End If
        
    If Cod <> "" Then
        sConsulta = sConsulta & " AND CIAS.COD = '" & DblQuote(Cod) & "'"
    End If
    
    If Den <> "" Then
        sConsulta = sConsulta & " AND CIAS.DEN >= '" & DblQuote(Den) & "'"
    End If
    
    If ValHasta <> 0 Then
        sConsulta = sConsulta & " AND CIAS.VOLFACT <=" & ValHasta
    End If
    
    If Val <> 0 Then
        sConsulta = sConsulta & " AND CIAS.VOLFACT >=" & Val
    End If
    
    If sPaiCod <> 0 Then
        sConsulta = sConsulta & " AND CIAS.PAI = " & sPaiCod
    End If
    
    If sProviCod <> 0 Then
        sConsulta = sConsulta & " AND CIAS.PROVI = " & sProviCod
    End If
    
    If sMonCod <> 0 Then
        sConsulta = sConsulta & " AND CIAS.MON  = " & sMonCod
    End If
    
    If CliRef <> "" Then
        sConsulta = sConsulta & " AND (CIAS.CLIREF1 >= '" & DblQuote(CliRef) & "'"
        sConsulta = sConsulta & " OR CIAS.CLIREF2 >= '" & DblQuote(CliRef) & "'"
        sConsulta = sConsulta & " OR CIAS.CLIREF3 >= '" & DblQuote(CliRef) & "'"
        sConsulta = sConsulta & " OR CIAS.CLIREF4 >= '" & DblQuote(CliRef) & "')"
    End If

    If Hom <> "" Then
        sConsulta = sConsulta & " AND (CIAS.HOM1 >= '" & DblQuote(Hom) & "'"
        sConsulta = sConsulta & " OR CIAS.HOM2 >= '" & DblQuote(Hom) & "'"
        sConsulta = sConsulta & " OR CIAS.HOM3 >= '" & DblQuote(Hom) & "')"
    End If
    
    If premiun Then
        sConsulta = sConsulta & " AND CIAS.PREMIUM=2 "
    End If
    
    sConsulta = sConsulta & " AND 1=1"
    
    If OrdenarPorCod Then
        sConsulta = sConsulta & " ORDER BY CIAS.COD"
    End If
    
    If OrdenarPorDen = True Then
        sConsulta = sConsulta & " ORDER BY CIAS.DEN"
    End If
    

    ObtenerSQLCias = sConsulta
End Function

