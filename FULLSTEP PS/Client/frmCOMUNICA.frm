VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmComunicacionCias 
   Caption         =   "Comunicaci�n con clientes"
   ClientHeight    =   4905
   ClientLeft      =   120
   ClientTop       =   2955
   ClientWidth     =   9735
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCOMUNICA.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4905
   ScaleWidth      =   9735
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Left            =   45
      TabIndex        =   0
      Top             =   -45
      Width           =   9645
      Begin FSPSClient.CiaSelector CiaSelector1 
         Height          =   285
         Left            =   2950
         TabIndex        =   13
         Top             =   210
         Width           =   300
         _ExtentX        =   529
         _ExtentY        =   503
      End
      Begin VB.CommandButton cmdBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7830
         Picture         =   "frmCOMUNICA.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   210
         Width           =   300
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   1245
         TabIndex        =   2
         Top             =   210
         Width           =   1650
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   8308
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   2910
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   3305
         TabIndex        =   3
         Top             =   210
         Width           =   4470
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8308
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7885
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin VB.Label lblEstado 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1245
         TabIndex        =   10
         Top             =   600
         Width           =   6480
      End
      Begin VB.Label Label1 
         Caption         =   "Compa��a:"
         Height          =   255
         Left            =   150
         TabIndex        =   4
         Top             =   255
         Width           =   990
      End
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Left            =   60
      ScaleHeight     =   525
      ScaleWidth      =   9645
      TabIndex        =   5
      Top             =   4395
      Width           =   9645
      Begin VB.CommandButton cmdRestaurarActiv 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   0
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   105
         Width           =   1005
      End
      Begin VB.CommandButton cmdEdicion 
         Caption         =   "&Edici�n"
         Default         =   -1  'True
         Height          =   345
         Left            =   8625
         TabIndex        =   11
         Top             =   105
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2220
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   105
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         Enabled         =   0   'False
         Height          =   345
         Left            =   0
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   105
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Enabled         =   0   'False
         Height          =   345
         Left            =   1110
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   105
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgComunicacion 
      Height          =   3315
      Left            =   45
      TabIndex        =   9
      Top             =   1080
      Width           =   9645
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "ConPeticiones"
      stylesets(0).BackColor=   13172735
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCOMUNICA.frx":0FF4
      stylesets(1).Name=   "Selection"
      stylesets(1).BackColor=   11513775
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmCOMUNICA.frx":1010
      stylesets(2).Name=   "Normal"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmCOMUNICA.frx":102C
      BeveColorScheme =   1
      BevelColorFace  =   12632256
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "Selection"
      Columns.Count   =   7
      Columns(0).Width=   2487
      Columns(0).Caption=   "Fecha"
      Columns(0).Name =   "FECHA"
      Columns(0).Alignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1693
      Columns(1).Caption=   "Hora"
      Columns(1).Name =   "Hora"
      Columns(1).Alignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   8
      Columns(1).Mask =   "##:##:##"
      Columns(1).PromptInclude=   -1  'True
      Columns(2).Width=   5530
      Columns(2).Caption=   "Tipo Comunicaci�n"
      Columns(2).Name =   "TIPOCOM"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   3
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "IDCOM"
      Columns(3).Name =   "IDCOM"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ID_TIPOC"
      Columns(4).Name =   "ID_TIPOC"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   6271
      Columns(5).Caption=   "Comentario"
      Columns(5).Name =   "OBSCOM"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).ButtonsAlways=   -1  'True
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID"
      Columns(6).Name =   "ID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   17013
      _ExtentY        =   5847
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmComunicacionCias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables privadas
Private oCias As CCias
Public oCiaSeleccionada As CCia
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private oComunicSeleccionada As cComunicacion
Private blnComprador As Boolean
Private blnProveedor As Boolean
Private oComunicac As cComunicaciones
Private blnBorrando As Boolean
Private bAnyadir As Boolean
Private bValError As Boolean
Private bModError As Boolean
Private bAnyaError As Boolean
Private bModoEdicion As Boolean

'Variables p�blicas
Public IdCiaSeleccionada As Long

'Constantes
Private sIdiEdicion As String
Private sIdiConsulta As String
Private sIdiFCAut As String
Private sIdiFCSol As String
Private sIdiFCDesaut As String
Private sIdiFPAut As String
Private sIdiFPSol As String
Private sIdiFPDesaut As String
Private sIdiFecha As String
Private sIdiHora As String
Private sIdiTipoCom As String
Private sIdiAbrevPend As String
Private sIdiAbrevTodas As String





Private Sub CiaSeleccionada()
    Set oCiaSeleccionada = Nothing
    oCias.CargarTodasLasCiasDesde 1, IdCiaSeleccionada, , , True
      
    Set oCiaSeleccionada = oCias.Item(CStr(IdCiaSeleccionada))
    
    'actualizo los cheks que indican si la Cia es proveedora o compradora
    lblEstado.Caption = ""
    'Funci�n compradora
    Select Case oCiaSeleccionada.EstadoCompradora
    Case 3
        lblEstado.Caption = sIdiFCAut
    Case 1
        lblEstado.Caption = sIdiFCSol
    Case 2
        lblEstado.Caption = sIdiFCDesaut
    End Select
    
    'funci�n proveedora
    If oCiaSeleccionada.EstadoProveedora <> 0 And lblEstado.Caption <> "" Then
        lblEstado.Caption = lblEstado.Caption & " - "
    End If
    
    Select Case oCiaSeleccionada.EstadoProveedora
    Case 3
        lblEstado.Caption = lblEstado.Caption & sIdiFPAut
    Case 1
        lblEstado.Caption = lblEstado.Caption & sIdiFPSol
    Case 2
        lblEstado.Caption = lblEstado.Caption & sIdiFPDesaut
    End Select
    
    cmdEdicion.Enabled = True
    cmdRestaurarActiv.Enabled = True
    cmdRestaurarActiv.Visible = True

    
    CargarTodasComunicaciones
    
End Sub


Private Sub CargarTodasComunicaciones()
    'Carga las comunicaciones asociadas a la cia seleccionada
    Dim ador As ador.Recordset
    Dim oComunicaciones As cComunicaciones
    Dim i As Integer
    Dim strFecha As String
    Dim strFechaSolo As String
    Dim strHora As String
    Dim intEspacio As Integer
    
    sdbgComunicacion.RemoveAll
    
    Set oComunicaciones = oCiaSeleccionada.CargarTodasLasComunicacionesDesde
    
    If Not oComunicaciones Is Nothing Then
        For i = 1 To oComunicaciones.Count
            strFecha = oComunicaciones.Item(i).Fecha
            intEspacio = InStr(strFecha, " ")
            If intEspacio <> 0 Then
                strFechaSolo = Mid(strFecha, 1, intEspacio - 1)
                strHora = Mid(strFecha, intEspacio + 1)
            Else
                strFechaSolo = strFecha
                strHora = "00:00:00"
            End If
            sdbgComunicacion.AddItem strFechaSolo & Chr(9) & strHora & Chr(9) & oComunicaciones.Item(i).DenComunicacion & Chr(9) & oComunicaciones.Item(i).ID & Chr(9) & oComunicaciones.Item(i).TipoComunicacion & Chr(9) & oComunicaciones.Item(i).Comentario
        Next i
    End If
    
    Set oComunicaciones = Nothing
End Sub
    
Public Sub ponerCiaSeleccionadaEnCombos()
    CiaSeleccionada
    bRespetarCombo = True
    sdbcCiaCod.AddItem oCiaSeleccionada.ID & Chr(9) & oCiaSeleccionada.Cod & Chr(9) & oCiaSeleccionada.Den
    sdbcCiaCod.Text = oCiaSeleccionada.Cod
    sdbcCiaDen.Text = oCiaSeleccionada.Den
    
    bRespetarCombo = False
End Sub

Private Sub EliminarComunicSeleccionada()
    Dim oError As CTESError
    Dim iRespuesta As Integer
    
    On Error GoTo Ir_Error:
    
    iRespuesta = basMensajes.PreguntaEliminarComunicacion(sdbgComunicacion.Columns(0).Value, oCiaSeleccionada.Den)
    
    If iRespuesta = vbNo Then
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
        
    If sdbgComunicacion.Columns("IDCOM").Value <> "" Then
        Set oError = oCiaSeleccionada.Comunicaciones.Item(IdCiaSeleccionada & "-" & sdbgComunicacion.Columns("IDCOM").Value).EliminarComunicacion
        If oError.NumError <> 0 Then
            basErrores.TratarError oError
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
       
    sdbgComunicacion.RemoveItem (sdbgComunicacion.AddItemRowIndex(sdbgComunicacion.Bookmark))
        
    sdbgComunicacion.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
    
    Exit Sub
    
Ir_Error:
    Exit Sub
End Sub


Private Sub CiaSelector1_Click(ByVal opcion As PSSeleccion)
    sdbcCiaCod = ""
End Sub

Private Sub cmdA�adir_Click()

    sdbgComunicacion.Scroll 0, sdbgComunicacion.Rows - sdbgComunicacion.Row
    
    If sdbgComunicacion.VisibleRows > 0 Then
        
        If sdbgComunicacion.VisibleRows >= sdbgComunicacion.Rows Then
            sdbgComunicacion.Row = sdbgComunicacion.Rows
        Else
            sdbgComunicacion.Row = sdbgComunicacion.Rows - (sdbgComunicacion.Rows - sdbgComunicacion.VisibleRows) - 1
        End If
        
    End If
    
    bAnyadir = True
    bAnyaError = False
    
    Set oComunicSeleccionada = Nothing
    Set oComunicSeleccionada = g_oRaiz.Generar_CComunicacion
    oComunicSeleccionada.Cia = oCiaSeleccionada.ID
    
    'A�ade una fila al grid para la inserci�n
    sdbgComunicacion.SetFocus

End Sub

Private Sub cmdBuscar_Click()
    frmCiaBuscar.WindowState = vbNormal
    frmCiaBuscar.g_sOrigen = "frmComunicacionCias"
    frmCiaBuscar.Show 1
    
    'Actualizo el bot�n del ciaselector
    CiaSelector1.Seleccion = PSSeleccion.PSTodos
End Sub

Private Sub cmdEdicion_Click()
    Dim v As Variant
    
    If Not bModoEdicion Then
    'Pasa a Modo EDICI�N
        cmdEdicion.Caption = sIdiConsulta
        
        'Hace visibles los botones
        cmdRestaurarActiv.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
        
        'Habilita los botones
        cmdA�adir.Enabled = True
        cmdDeshacer.Enabled = False
        If sdbgComunicacion.Rows > 0 Then
            cmdEliminar.Enabled = True
        End If
        
        'Deshabilito el frame de b�squeda
        Me.Frame1.Enabled = False
        
        'Habilita el grid
        sdbgComunicacion.AllowDelete = True
        sdbgComunicacion.AllowUpdate = True
        sdbgComunicacion.AllowAddNew = True
       
        bModoEdicion = True
    Else
        'Pasa a modo consulta, si ha cambiado algo actualiza los datos
        If sdbgComunicacion.DataChanged = True Then
        
            v = sdbgComunicacion.ActiveCell.Value
            sdbgComunicacion.SetFocus
            sdbgComunicacion.ActiveCell.Value = v
            
            bValError = False
            bAnyaError = False
            bModError = False
            
            sdbgComunicacion.Update
            CiaSeleccionada

            If bValError Or bAnyaError Or bModError Then
                Exit Sub
            End If
            
        End If
        
        sdbgComunicacion.AllowAddNew = False
        sdbgComunicacion.AllowUpdate = False
        sdbgComunicacion.AllowDelete = False

        cmdDeshacer_Click

        'Modo consulta
        cmdEdicion.Caption = sIdiEdicion
        'Hace invisible los botones de edici�n
        cmdRestaurarActiv.Visible = True
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        
        'Habilita el frame de b�squeda
        Me.Frame1.Enabled = True
        
        bModoEdicion = False
        
    End If
    sdbgComunicacion.SetFocus

End Sub

Private Sub cmdEliminar_Click()
    blnBorrando = True
    sdbgComunicacion.Update
    
    If sdbgComunicacion.Rows > 0 Then
        sdbgComunicacion.SelBookmarks.Add sdbgComunicacion.Bookmark
        sdbgComunicacion.DeleteSelected
        sdbgComunicacion.SelBookmarks.RemoveAll
    End If
    
    blnBorrando = False
    
End Sub


Private Sub cmdDeshacer_Click()
    sdbgComunicacion.CancelUpdate
    sdbgComunicacion.DataChanged = False
    
    If Not oComunicSeleccionada Is Nothing Then
        Set oComunicSeleccionada = Nothing
    End If
    
    If Not sdbgComunicacion.IsAddRow Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    Else
        cmdDeshacer.Enabled = False
    End If
    
End Sub

Private Sub cmdRestaurarActiv_Click()
    'Restaura los datos
    CargarTodasComunicaciones
End Sub

Private Sub Form_Load()
    Width = 9930
    Height = 5310
    CargarRecursos
    Set oCias = g_oRaiz.Generar_CCias
    Set oComunicac = g_oRaiz.Generar_CComunicaciones
    
    bModoEdicion = False
    
    cmdEdicion.Enabled = False
    cmdRestaurarActiv.Enabled = False
    
    'Caption del CiaSelector
    CiaSelector1.AbrevParaPendientes = sIdiAbrevPend
    CiaSelector1.AbrevParaTodos = sIdiAbrevTodas
    
    CiaSelector1.AbrevParaPendientes = "P"
    CiaSelector1.AbrevParaTodos = "T"
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Arrange()
    '''Redimensiona el formulario
    If Height <= 1995 Then Exit Sub
    If Width <= 700 Then Exit Sub
    
    'Grid
    sdbgComunicacion.Height = Me.Height - 1995
    sdbgComunicacion.Width = Me.Width - 210
    
    sdbgComunicacion.Columns(0).Width = sdbgComunicacion.Width * 0.14
    sdbgComunicacion.Columns(1).Width = sdbgComunicacion.Width * 0.096
    sdbgComunicacion.Columns(2).Width = sdbgComunicacion.Width * 0.32
    sdbgComunicacion.Columns(5).Width = sdbgComunicacion.Width * 0.385
    
    'Frame
    Frame1.Width = Me.Width - 210
    
    'Botones
    picNavigate.Top = Me.Height - 915
    picNavigate.Width = Me.Width - 210
    cmdEdicion.Left = picNavigate.Width - 1020
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCias = Nothing
    Set oCiaSeleccionada = Nothing
    Set oComunicSeleccionada = Nothing
    Set oComunicac = Nothing
End Sub

Private Sub sdbcCiaCod_Change()
    'chkFC.Enabled = True
    'chkFP.Enabled = True
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        IdCiaSeleccionada = -1
        Set oCiaSeleccionada = Nothing
        bRespetarCombo = False
        sdbgComunicacion.RemoveAll
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = False
        
        
        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcCiaCod_Click()
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_CloseUp()
    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    IdCiaSeleccionada = sdbcCiaCod.Columns(0).Value
    CiaSeleccionada
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Integer
    Dim ador As ador.Recordset
    
    sdbcCiaCod.RemoveAll

    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            If bCargarComboDesde Then
                Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False)
            Else
                Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False)
            End If
        Case PSSeleccion.PSPendientes
            If bCargarComboDesde Then
                Set ador = oCias.DevolverCiasPendientesDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False)
            Else
                Set ador = oCias.DevolverCiasPendientesDesde(g_iCargaMaximaCombos, , , False, False)
            End If
    End Select
    
    'If bCargarComboDesde Then
    '    If (blnComprador And blnProveedor) Or (Not blnComprador And Not blnProveedor) Then
    '        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False)
    '    ElseIf blnComprador Then
    '        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, True)
    '    ElseIf blnProveedor Then
    '        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, False)
    '    End If
    'Else
    '    If (blnComprador And blnProveedor) Or (Not blnComprador And Not blnProveedor) Then
    '        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False)
    '    ElseIf blnComprador Then
    '        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, True)
    '    ElseIf blnProveedor Then
    '        Set ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, False)
    '    End If
    'End If
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
End Sub

Private Sub sdbcCiaCod_InitColumnProps()
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
Dim ador As ador.Recordset

    If Trim(sdbcCiaCod.Text) = "" Then Exit Sub
    
    Select Case CiaSelector1.Seleccion
        
        Case PSSeleccion.PSTodos
            Set ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True)
        Case PSSeleccion.PSPendientes
            Set ador = oCias.DevolverCiasPendientesDesde(1, sdbcCiaCod.Text, , True)
    End Select
    
    'If (CiaSelector And blnProveedor) Or (Not blnComprador And Not blnProveedor) Then
    '    Set ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True)
    'ElseIf blnComprador Then
        'Set ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , True)
    'ElseIf blnProveedor Then
    '    Set ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , True)
    'End If
    
    If ador Is Nothing Then
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcCiaDen.Text = ador("DEN").Value
                IdCiaSeleccionada = ador("ID").Value
                CiaSeleccionada
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If

End Sub

Private Sub sdbcCiaDen_Change()
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaCod.Text = ""
        sdbcCiaDen.Text = ""
        IdCiaSeleccionada = -1
        Set oCiaSeleccionada = Nothing
        bRespetarCombo = False
        
        sdbgComunicacion.RemoveAll
        
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = False
        
        bCargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcCiaDen_Click()
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_CloseUp()
    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    If sdbcCiaDen.Columns(0).Value <> "" Then
        IdCiaSeleccionada = sdbcCiaDen.Columns(0).Value
    End If
    CiaSeleccionada
End Sub


Private Sub sdbcCiaDen_DropDown()
    Dim i As Integer
    Dim ador As ador.Recordset
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            If bCargarComboDesde Then
                Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True)
            Else
                Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True)
            End If
        Case PSSeleccion.PSPendientes
            If bCargarComboDesde Then
                Set ador = oCias.DevolverCiasPendientesDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True)
            Else
                Set ador = oCias.DevolverCiasPendientesDesde(basParametros.g_iCargaMaximaCombos, , , False, True)
            End If
    End Select
    
    'If bCargarComboDesde Then
        'carga dependiendo de si se ha seleccionado o no que sea compradora o proveedora
    '    If (blnComprador And blnProveedor) Or (Not blnComprador And Not blnProveedor) Then
    '        Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True)
    '    ElseIf blnComprador Then
    '        Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, True)
    '    ElseIf blnProveedor Then
    '        Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, False)
    '    End If
    'Else
    '    If (blnComprador And blnProveedor) Or (Not blnComprador And Not blnProveedor) Then
    '        Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True)
    '    ElseIf blnComprador Then
    '        Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, True)
    '    ElseIf blnProveedor Then
    '        Set ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, False)
    '    End If
        
    'End If
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
End Sub

Private Sub sdbcCiaDen_InitColumnProps()
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcCiaDen_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset

    If Trim(sdbcCiaDen.Text) = "" Then Exit Sub
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            Set ador = oCias.DevolverCiasDesde(1, , sdbcCiaDen.Text, True)
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            Set ador = oCias.DevolverCiasPendientesDesde(1, , sdbcCiaDen.Text, True)
    End Select
    
    If ador Is Nothing Then
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcCiaDen.Text = ador("DEN").Value
                IdCiaSeleccionada = ador("ID").Value
                CiaSeleccionada
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If

End Sub

Private Sub sdbgComunicacion_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar

    sdbgComunicacion.SetFocus
    If sdbgComunicacion.Rows > 0 Then
        sdbgComunicacion.Bookmark = sdbgComunicacion.RowBookmark(sdbgComunicacion.Row)
    End If
End Sub

Private Sub sdbgComunicacion_AfterUpdate(RtnDispErrMsg As Integer)
    
    If Not bModoEdicion Then Exit Sub
    
    If bAnyaError = False And bModError = False Then
        If sdbgComunicacion.Rows > 0 Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
        Else
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = False
        End If
        cmdDeshacer.Enabled = False
    End If
    Set oComunicac = oCiaSeleccionada.CargarTodasLasComunicacionesDesde
    bAnyadir = False
End Sub

Private Sub sdbgComunicacion_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    ''' * Objetivo: Confirmacion antes de eliminar y eliminar de base datos
    Dim iRespuesta As Integer
    Dim IndFor As Long, IndCom As Long
    Dim oError As CTESError

    DispPromptMsg = 0
    blnBorrando = True
    
    If sdbgComunicacion.IsAddRow Or bAnyadir Then
        Cancel = True
        blnBorrando = False
        Exit Sub
    End If

    iRespuesta = basMensajes.PreguntaEliminarComunicacion(sdbgComunicacion.Columns(0).Value, oCiaSeleccionada.Den)
    
    If iRespuesta = vbNo Then
        blnBorrando = False
        Cancel = True
        Exit Sub
    End If
    
    If sdbgComunicacion.Rows > 0 Then
        sdbgComunicacion.SelBookmarks.Add sdbgComunicacion.Bookmark
        Screen.MousePointer = vbHourglass
        '''Eliminar de Base de datos
        If sdbgComunicacion.Columns("IDCOM").Value <> "" Then
            Set oError = oCiaSeleccionada.Comunicaciones.Item(IdCiaSeleccionada & "-" & sdbgComunicacion.Columns("IDCOM").Value).EliminarComunicacion
            If oError.NumError <> 0 Then
                blnBorrando = False
                basErrores.TratarError oError
                Screen.MousePointer = vbNormal
                Cancel = True
                Exit Sub
            End If
            ''' Eliminar de la coleccion
            IndCom = Val(sdbgComunicacion.Bookmark)
            oCiaSeleccionada.Comunicaciones.Remove (IdCiaSeleccionada & "-" & sdbgComunicacion.Columns("IDCOM").CellText(IndCom))
        
        End If
        
        
    End If

    Screen.MousePointer = vbNormal
    blnBorrando = False
End Sub

Private Sub sdbgComunicacion_BeforeUpdate(Cancel As Integer)
    Dim oError As CTESError
    
    bValError = False
    bModError = False
    bAnyaError = False
    Cancel = False

    'Comunicaci�n seleccionada
    Set oComunicSeleccionada = Nothing
    Set oComunicSeleccionada = g_oRaiz.Generar_CComunicacion
    oComunicSeleccionada.Cia = oCiaSeleccionada.ID
    
    If IsNull(sdbgComunicacion.Columns("IDCOM").Value) Or (sdbgComunicacion.Columns("IDCOM").Value = "") Then
        'A�adiendo
        oComunicSeleccionada.ID = 0
    Else
        oComunicSeleccionada.ID = sdbgComunicacion.Columns("IDCOM").Value
    End If
    
    
    '*************************************************
    'Guarda los cambios en la base de datos
    
    If sdbgComunicacion.DataChanged = True Then
    
        'Comprueba que todos los campos se han rellenado correctamente
        If Validar = False Then
            Cancel = True
            bValError = Cancel
            bAnyadir = False
            sdbgComunicacion.SetFocus
            Exit Sub
        End If
        
        oComunicSeleccionada.Fecha = sdbgComunicacion.Columns("FECHA").Value & " " & sdbgComunicacion.Columns("Hora").Value
        oComunicSeleccionada.DenComunicacion = sdbgComunicacion.Columns("TIPOCOM").Value
        oComunicSeleccionada.TipoComunicacion = sdbgComunicacion.Columns("ID_TIPOC").Value
            
        '******
        oComunicSeleccionada.Comentario = sdbgComunicacion.Columns("OBSCOM").Value
        '**********
        
        If Not IsNull(oComunicSeleccionada.ID) And Not (oComunicSeleccionada.ID = 0) Then
            Set oError = oComunicSeleccionada.ModificarDatosComunicacion
            If oError.NumError <> 0 Then
                basErrores.TratarError oError
                bModError = True
                Cancel = True
            End If

        Else
            Set oError = oComunicSeleccionada.AnyadirComunicacion
            If oError.NumError <> 0 Then
                basErrores.TratarError oError
                bAnyaError = True
                Cancel = True
                sdbgComunicacion.SetFocus
                Exit Sub
            Else
                sdbgComunicacion.Columns("IDCOM").Value = oComunicSeleccionada.ID
            End If

        End If
            
    End If
    '******************************************************
End Sub

Private Sub sdbgComunicacion_DblClick()
     
    If sdbgComunicacion.Rows = 0 Then Exit Sub
     
    If bModoEdicion Then
        If Not Validar Then
            bValError = True
            sdbgComunicacion.SetFocus
            Exit Sub
        End If
        'sdbgComunicacion.Bookmark = sdbgComunicacion.RowBookmark(sdbgComunicacion.Row)
        sdbgComunicacion.Update
    End If
    
    'muestra el formulario de los comentarios
    If sdbgComunicacion.Columns("IDCOM").Value <> "" Then
        'modificaci�n
        Set oComunicSeleccionada = oCiaSeleccionada.Comunicaciones.Item(IdCiaSeleccionada & "-" & sdbgComunicacion.Columns("IDCOM").Value)
    Else
        'inserci�n
        If IsNull(sdbgComunicacion.Columns("IDCOM").Value) Or (sdbgComunicacion.Columns("IDCOM").Value = "") Then
            oComunicSeleccionada.ID = 0
        Else
            oComunicSeleccionada.ID = sdbgComunicacion.Columns("IDCOM").Value
        End If
        
        If IsDate(sdbgComunicacion.Columns("FECHA").Value & " " & sdbgComunicacion.Columns("Hora").Value) Then
            oComunicSeleccionada.Fecha = sdbgComunicacion.Columns("FECHA").Value & " " & sdbgComunicacion.Columns("Hora").Value
        Else
            oComunicSeleccionada.Fecha = Date
        End If
        oComunicSeleccionada.DenComunicacion = sdbgComunicacion.Columns("TIPOCOM").Value
        oComunicSeleccionada.TipoComunicacion = sdbgComunicacion.Columns("ID_TIPOC").Value
        oComunicSeleccionada.Comentario = ""
    End If
        
    If bModoEdicion Then
        frmComunicaComent.blnEdicion = True
    Else
        frmComunicaComent.blnEdicion = False
    End If

    Set frmComunicaComent.oCia = oCiaSeleccionada
    Set frmComunicaComent.oComunic = oComunicSeleccionada
    frmComunicaComent.lblFec.Caption = sdbgComunicacion.Columns("FECHA").Value & " " & sdbgComunicacion.Columns("Hora").Value
    frmComunicaComent.lblTipoC.Caption = sdbgComunicacion.Columns("TIPOCOM").Value
       
    frmComunicaComent.Show 1
    
    'If bModoEdicion And sdbgComunicacion.Columns("IDCOM").Value = "" Then
    If bModoEdicion Then
        CargarTodasComunicaciones
    End If
End Sub

Private Sub sdbgComunicacion_Change()
    
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    
    End If
    
    If cmdEdicion.Caption = sIdiConsulta And Not sdbgComunicacion.IsAddRow Then
        Set oComunicSeleccionada = Nothing
        Set oComunicSeleccionada = oCiaSeleccionada.Comunicaciones.Item(CStr(sdbgComunicacion.Bookmark))
    End If
End Sub

Private Sub sdbgComunicacion_ComboCloseUp()
    Dim intId As Integer
    Dim strTag As String
    Dim intIndice As Integer
    Dim intPosicion As Integer
    
    'Al seleccionar un tipo de comunicaci�n obtiene del tag el �ndice
    'correspondiente a la denominaci�n seleccionada
    
    If sdbgComunicacion.Columns("TIPOCOM").ListIndex = -1 Then Exit Sub
    
    'indice del seleccionado:
    intId = sdbgComunicacion.Columns("TIPOCOM").ListIndex
    
    If intId = 0 Then
        strTag = Mid(sdbgComunicacion.Columns("TIPOCOM").TagVariant, 1, 1)
        sdbgComunicacion.Columns("ID_TIPOC").Value = Val(strTag)
    Else
        strTag = sdbgComunicacion.Columns("TIPOCOM").TagVariant
        For intIndice = 1 To intId
            intPosicion = InStr(1, strTag, "$")
            strTag = Mid(strTag, intPosicion + 1)
        Next intIndice
        sdbgComunicacion.Columns("ID_TIPOC").Value = Val(Mid(strTag, 1, 1))
    End If
    
    
End Sub

Private Sub sdbgComunicacion_InitColumnProps()
    ''''Inicializa el valor del combo de Tipos de comunicaci�n
    Dim ador As ador.Recordset

    Set ador = oComunicac.DevolverComboTiposComunicacion
    If Not ador.EOF Then
        ador.MoveFirst
        While Not ador.EOF
            sdbgComunicacion.Columns("TIPOCOM").AddItem ador("DEN"), ador("ID")
            sdbgComunicacion.Columns("TIPOCOM").TagVariant = sdbgComunicacion.Columns("TIPOCOM").TagVariant & ador("ID") & "$"
            ador.MoveNext
        Wend
        'Deja en el tag el id. de todos los tipos de comunicaci�n separados
        'por el car�cter $
        sdbgComunicacion.Columns("TIPOCOM").TagVariant = Mid(sdbgComunicacion.Columns("TIPOCOM").TagVariant, 1, Len(sdbgComunicacion.Columns("TIPOCOM").TagVariant) - 1)
    End If
    
    ador.Close
    Set ador = Nothing
End Sub


Private Sub sdbgComunicacion_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgComunicacion.DataChanged = False Then
            
            sdbgComunicacion.CancelUpdate
            sdbgComunicacion.DataChanged = False
            
            If Not oComunicSeleccionada Is Nothing Then
                'cancelar edicion
                Set oComunicSeleccionada = Nothing
            End If
            
            If Not sdbgComunicacion.IsAddRow Then
            
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                bAnyadir = False
            Else
            
                cmdA�adir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = True
                bAnyadir = True
            End If
                    
       End If
       
    End If
    

End Sub

Private Sub sdbgComunicacion_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    If Not sdbgComunicacion.IsAddRow Then
        If sdbgComunicacion.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            bAnyadir = False
        End If
    Else
        If sdbgComunicacion.DataChanged = True Then cmdDeshacer.Enabled = True
        If Not IsNull(LastRow) Then
            If Val(LastRow) <> Val(sdbgComunicacion.Row) Then
                sdbgComunicacion.Col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        
    End If

End Sub

Private Function Validar() As Boolean
    Dim oTiposComunicacion As CTiposComunicacion
    Dim ador As ador.Recordset
    
    If blnBorrando = True Then
        Validar = False
        Exit Function
    End If
            
   
    'Valida la fecha
    'If (Not IsDate(sdbgComunicacion.Columns("FECHA").Value & " " & sdbgComunicacion.Columns("Hora").Value)) Or _
    '(sdbgComunicacion.Columns("FECHA").Value = "") Then
    '    basMensajes.NoValido "Fecha y hora"
    '    Validar = False
    '    Exit Function
    'End If
    
    If Not IsDate(sdbgComunicacion.Columns("FECHA").Text) Then
        Validar = False
        NoValida (sIdiFecha)
        Exit Function
    Else
        sdbgComunicacion.Columns("FECHA").Text = Format(sdbgComunicacion.Columns("FECHA").Text, "dd/mm/yyyy")
    End If
    
    If Not IsDate(sdbgComunicacion.Columns("Hora").Text) Then
        Validar = False
        NoValida (sIdiHora)
        Exit Function
    End If
    
    'Valida el tipo de comunicaci�n
    If sdbgComunicacion.Columns("TIPOCOM").Value = "" Then
        basMensajes.NoValido sIdiTipoCom
        Validar = False
        Exit Function
    End If
    
    If sdbgComunicacion.Columns("ID_TIPOC").Value = "" Then
        basMensajes.NoValido sIdiTipoCom
        Validar = False
        Exit Function
    End If
    
    Set oTiposComunicacion = g_oRaiz.Generar_CTiposComunicacion
    If sdbgComunicacion.Columns("TIPOCOM").Value = "" Then
        Validar = False
        NoValido (sIdiTipoCom)
        Exit Function
    End If
    
    Set ador = oTiposComunicacion.DevolverTiposDesde(, sdbgComunicacion.Columns("TIPOCOM").Value, True)
    If ador.RecordCount = 0 Then
        Validar = False
        NoValido ("Tipo de Comunicaci�n")
        sdbgComunicacion.Columns("TIPOCOM").Value = ""
        Exit Function
    End If
    
    Set ador = Nothing
    Set oTiposComunicacion = Nothing
    
    Validar = True
End Function


Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COMUNICACACIONCIAS, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdA�adir.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdDeshacer.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdEdicion.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdEliminar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdRestaurarActiv.Caption = ador(0).Value
        ador.MoveNext
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.Label1.Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcCiaCod.Columns(1).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgComunicacion.Columns(0).Caption = ador(0).Value
        sIdiFecha = ador(0).Value
        ador.MoveNext
        Me.sdbgComunicacion.Columns(1).Caption = ador(0).Value
        sIdiHora = ador(0).Value
        ador.MoveNext
        Me.sdbgComunicacion.Columns(2).Caption = ador(0).Value
        sIdiTipoCom = ador(0).Value
        ador.MoveNext
        Me.sdbgComunicacion.Columns(5).Caption = ador(0).Value
        ador.MoveNext
        sIdiEdicion = ador(0).Value
        ador.MoveNext
        sIdiConsulta = ador(0).Value
        ador.MoveNext
        sIdiFCAut = ador(0).Value
        ador.MoveNext
        sIdiFCSol = ador(0).Value
        ador.MoveNext
        sIdiFCDesaut = ador(0).Value
        ador.MoveNext
        sIdiFPAut = ador(0).Value
        ador.MoveNext
        sIdiFPSol = ador(0).Value
        ador.MoveNext
        sIdiFPDesaut = ador(0).Value
        ador.MoveNext
        sIdiAbrevPend = ador(0).Value 'P
        ador.MoveNext
        sIdiAbrevTodas = ador(0).Value 'T
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub

