VERSION 5.00
Object = "{3C62B3DD-12BE-4941-A787-EA25415DCD27}#10.0#0"; "crviewer.dll"
Begin VB.Form Preview 
   Caption         =   "Preview"
   ClientHeight    =   4680
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8565
   Icon            =   "Preview.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   4680
   ScaleWidth      =   8565
   Begin CrystalActiveXReportViewerLib10Ctl.CrystalActiveXReportViewer crViewer 
      Height          =   7005
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   8025
      lastProp        =   600
      _cx             =   14155
      _cy             =   12356
      DisplayGroupTree=   -1  'True
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   0   'False
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   0   'False
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   0   'False
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   0   'False
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
      LaunchHTTPHyperlinksInNewBrowser=   -1  'True
      EnableLogonPrompts=   -1  'True
   End
End
Attribute VB_Name = "Preview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Public g_sOrigen As String
Public g_oReport As Object


Private Sub crViewer_PrintButtonClicked(UseDefault As Boolean)
g_oReport.PrinterSetup Me.hWnd
End Sub

Private Sub Form_Resize()

    crViewer.Top = 0
    crViewer.Left = 0
    crViewer.Height = ScaleHeight
    crViewer.Width = ScaleWidth

End Sub

Private Sub Form_Unload(Cancel As Integer)

Set g_oReport = Nothing

Select Case g_sOrigen

    Case "frmLstACT"
        If Not frmLstACT.g_adores_Ador Is Nothing Then
            frmLstACT.g_adores_Ador.Close
            Set frmLstACT.g_adores_Ador = Nothing
        End If
    Case "frmLstCoorPai"
        If Not frmLstCoorPai.g_adores_Ador Is Nothing Then
            frmLstCoorPai.g_adores_Ador.Close
            Set frmLstCoorPai.g_adores_Ador = Nothing
        End If
    Case "frmLstCoorProv"
        If Not frmLstCoorProv.g_adores_Ador Is Nothing Then
            frmLstCoorProv.g_adores_Ador.Close
            Set frmLstCoorProv.g_adores_Ador = Nothing
        End If
    Case "frmLstProcPub"
        If Not frmLstProcPub.g_adores_adorSubRpt Is Nothing Then
            frmLstProcPub.g_adores_adorSubRpt.Close
            Set frmLstProcPub.g_adores_adorSubRpt = Nothing
        End If
    Case "frmLstCoorMon"
        If Not frmLstCoorMon.g_adores_Ador Is Nothing Then
            frmLstCoorMon.g_adores_Ador.Close
            Set frmLstCoorMon.g_adores_Ador = Nothing
        End If
    Case "frmLstMonCentr"
        If Not frmLstMonCentr.g_adores_Ador Is Nothing Then
            frmLstMonCentr.g_adores_Ador.Close
            Set frmLstMonCentr.g_adores_Ador = Nothing
        End If
    Case "frmLstRegComunic"
        If Not frmLstRegComunic.g_adores_adorSubRpt Is Nothing Then
            frmLstRegComunic.g_adores_adorSubRpt.Close
            Set frmLstRegComunic.g_adores_adorSubRpt = Nothing
        End If
        If Not frmLstRegComunic.g_adoresAdjun Is Nothing Then
            frmLstRegComunic.g_adoresAdjun.Close
            Set frmLstRegComunic.g_adoresAdjun = Nothing
        End If
End Select
End Sub
