VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmVolAdjudicadoProv 
   Caption         =   "Volumen adjudicado por proveedor"
   ClientHeight    =   5070
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9825
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVolAdjudicadoProv.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5070
   ScaleWidth      =   9825
   Begin VB.Frame frameFiltro 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   9615
      Begin VB.CommandButton cmdExcel 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9240
         Picture         =   "frmVolAdjudicadoProv.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8505
         Picture         =   "frmVolAdjudicadoProv.frx":11C4
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   3210
         TabIndex        =   4
         Top             =   240
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecApeHasta 
         Height          =   285
         Left            =   4350
         Picture         =   "frmVolAdjudicadoProv.frx":1506
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Mantenimiento"
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   800
         TabIndex        =   2
         Top             =   240
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalFecApeDesde 
         Height          =   285
         Left            =   1940
         Picture         =   "frmVolAdjudicadoProv.frx":1A90
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Mantenimiento"
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8900
         Picture         =   "frmVolAdjudicadoProv.frx":201A
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   240
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCia 
         Height          =   285
         Left            =   5760
         TabIndex        =   5
         Top             =   240
         Width           =   2535
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3307
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   8678
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   4471
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         Height          =   225
         Left            =   2520
         TabIndex        =   12
         Top             =   270
         Width           =   615
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   270
         Width           =   825
      End
      Begin VB.Label lblCia 
         Caption         =   "Proveedor:"
         Height          =   255
         Left            =   4920
         TabIndex        =   6
         Top             =   270
         Width           =   975
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgDetalle 
      Height          =   3795
      Left            =   120
      TabIndex        =   11
      Top             =   840
      Width           =   9615
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   7
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   2170
      Columns(0).Caption=   "Comprador"
      Columns(0).Name =   "COMP"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   4895
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3519
      Columns(2).Caption=   "Adjudicado "
      Columns(2).Name =   "ADJ"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777215
      Columns(3).Width=   3519
      Columns(3).Caption=   "Adjudicado (Moneda GS)"
      Columns(3).Name =   "ADJGS"
      Columns(3).Alignment=   1
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(4).Width=   1773
      Columns(4).Caption=   "Moneda GS"
      Columns(4).Name =   "MON"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "MONEDAPS"
      Columns(5).Name =   "MONEDAPS"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID"
      Columns(6).Name =   "ID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   16960
      _ExtentY        =   6694
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   120
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   4680
      Width           =   9615
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVolAdjudicadoProv.frx":211C
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmVolAdjudicadoProv.frx":2138
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmVolAdjudicadoProv.frx":2154
      DividerType     =   1
      DividerStyle    =   2
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   3
      Columns(0).Width=   7752
      Columns(0).Caption=   "Total Adjudicado"
      Columns(0).Name =   "LIT"
      Columns(0).AllowSizing=   0   'False
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   3519
      Columns(1).Caption=   "ADJ"
      Columns(1).Name =   "ADJ"
      Columns(1).Alignment=   1
      Columns(1).AllowSizing=   0   'False
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "Standard"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   7064
      Columns(2).Caption=   "MON"
      Columns(2).Name =   "MON"
      Columns(2).AllowSizing=   0   'False
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      _ExtentX        =   16960
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmVolAdjudicadoProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Private oCias As CCias

Private DenMonCentral As String

Public ofrmLstInfVol As frmLstInfVolProv
    
Private sIdiFecDesde As String
Private sIdiFecHasta As String
Private sIdiProveedor As String
Private sIdiPor As String
Private sIdiTotAdj As String

Private Sub cmdBuscar_Click()
    'Comprueba que se hayan introducido todos los datos
    If txtFecDesde = "" Then
        basMensajes.NoValida sIdiFecDesde
        Exit Sub
    End If
    
    If txtFecHasta = "" Then
        basMensajes.NoValida sIdiFecHasta
        Exit Sub
    End If
    
    If sdbcCia.Text = "" Then
        basMensajes.NoValido sIdiProveedor
        Exit Sub
    End If
    
    If CDate(txtFecHasta.Text) < CDate(txtFecDesde.Text) Then
        basMensajes.FechaHastaMenor
        txtFecHasta.SetFocus
        Exit Sub
    End If
    
    CargarVolumen
End Sub

Private Sub cmdCalFecApeDesde_Click()
        'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmVolAdjudicadoProv
    Set frmCalendar.ctrDestination = txtFecDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde <> "" Then
        frmCalendar.Calendar.Value = txtFecDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdCalFecApeHasta_Click()
        'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmVolAdjudicadoProv
    Set frmCalendar.ctrDestination = txtFecHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecHasta <> "" Then
        frmCalendar.Calendar.Value = txtFecHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdExcel_Click()
     Set ofrmLstInfVol = New frmLstInfVolProv
  
    ofrmLstInfVol.WindowState = vbNormal
    
    If txtFecDesde.Text <> "" Then
        ofrmLstInfVol.txtFecDesde = txtFecDesde.Text
    End If
    
    If txtFecHasta.Text <> "" Then
        ofrmLstInfVol.txtFecHasta = txtFecHasta.Text
    End If
    
    ofrmLstInfVol.sdbcCia = sdbcCia

    ofrmLstInfVol.DenMonCentral = DenMonCentral
    ofrmLstInfVol.bExcel = True
    
    ofrmLstInfVol.Show vbModal
End Sub

Private Sub cmdImprimir_Click()
    Set ofrmLstInfVol = New frmLstInfVolProv
  
    ofrmLstInfVol.WindowState = vbNormal
    
    If txtFecDesde.Text <> "" Then
        ofrmLstInfVol.txtFecDesde = txtFecDesde.Text
    End If
    
    If txtFecHasta.Text <> "" Then
        ofrmLstInfVol.txtFecHasta = txtFecHasta.Text
    End If
    
    ofrmLstInfVol.sdbcCia = sdbcCia

    ofrmLstInfVol.DenMonCentral = DenMonCentral
    
    ofrmLstInfVol.Show vbModal

End Sub

Private Sub Form_Load()
    Dim Ador As Ador.Recordset
    
    'Dimensiona el formulario
    Me.Height = 5475
    Me.Width = 9945
    CargarRecursos
    
    'Obtiene el c�digo de la moneda central
    Set Ador = g_oRaiz.CodMonedaCentral
    DenMonCentral = Ador(1)
    Me.sdbgDetalle.Columns(2).Caption = Me.sdbgDetalle.Columns(2).Caption & "(" & Ador(0) & ")"
    Ador.Close
    Set Ador = Nothing
    
    'Fechas del mes actual por defecto
    Me.txtFecDesde = Format("01/" & Month(Date) & "/" & Year(Date), "dd/mm/yyyy")
    Me.txtFecHasta = Date
    
    'Clase cias
    Set oCias = g_oRaiz.Generar_CCias
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCias = Nothing
End Sub

Private Sub sdbcCia_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        bRespetarCombo = False
        'Limpia el grid
        sdbgDetalle.RemoveAll
        sdbgTotales.RemoveAll
        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcCia_Click()
    If Not sdbcCia.DroppedDown Then
        sdbcCia = ""
    End If
End Sub


Private Sub sdbcCia_CloseUp()
    'Limpia los grids
    sdbgDetalle.RemoveAll
    sdbgTotales.RemoveAll
    
    If sdbcCia.Value = "-1" Then
        sdbcCia.Text = ""
        Exit Sub
    End If
    
    If sdbcCia.Text = "" Then Exit Sub
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcCia_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    Screen.MousePointer = vbHourglass
    
    sdbcCia.RemoveAll

    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCia.Text), , False, False, , , , , , , , , , , , , , 5)
    Else
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, , , , , , , , , , , , , , 5)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCia.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCia.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCia.SelStart = 0
    sdbcCia.SelLength = Len(sdbcCia.Text)
    sdbcCia.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCia_InitColumnProps()
    sdbcCia.DataField = "Column 1"
    sdbcCia.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcCia_PositionList(ByVal Text As String)
        ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCia.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCia.Rows - 1
            bm = sdbcCia.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCia.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCia.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcCia_Validate(Cancel As Boolean)
    Dim Ador As Ador.Recordset

    If Trim(sdbcCia.Text) = "" Then Exit Sub
    
    Set Ador = oCias.DevolverCiasDesde(1, sdbcCia.Text, , True)
    
    If Ador Is Nothing Then
        sdbcCia = ""
        Exit Sub
        
    Else
        If Ador.EOF Then
            sdbcCia.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCia.Text) <> UCase(Ador("COD").Value) Then
                sdbcCia.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcCia.Text = Ador("COD").Value
                sdbcCia.Columns(0).Value = Ador("ID").Value
                sdbcCia.Columns(1).Value = Ador("COD").Value
                sdbcCia.Columns(2).Value = Ador("DEN").Value
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    End If
End Sub

Private Sub sdbgDetalle_DblClick()
    If sdbgDetalle.Rows < 1 Then Exit Sub

    'Carga el detalle
    frmDetalleProcAdj.CargarVolumen txtFecDesde.Text, txtFecHasta.Text, sdbcCia.Columns(0).Value, sdbgDetalle.Columns(6).Value
    
    frmDetalleProcAdj.Caption = frmDetalleProcAdj.Caption & sdbcCia.Columns(1).Value & " - " & sdbcCia.Columns(2).Value & " " & sIdiPor & " " & sdbgDetalle.Columns(0).Value
    
    frmDetalleProcAdj.FecDesde = txtFecDesde.Text
    frmDetalleProcAdj.FecHasta = txtFecHasta.Text
    frmDetalleProcAdj.CiaComp = sdbgDetalle.Columns(6).Value
    frmDetalleProcAdj.CiaProv = sdbcCia.Columns(0).Value
    
    frmDetalleProcAdj.Show vbModal

End Sub

Private Sub Arrange()
    
    If Width >= 330 Then frameFiltro.Width = Width - 330
    
    If Height >= 1680 Then sdbgDetalle.Height = Height - 1780
    If Width >= 330 Then sdbgDetalle.Width = Width - 330
    
    If Height >= 795 Then sdbgTotales.Top = sdbgDetalle.Top + sdbgDetalle.Height + 25
    If Width >= 330 Then sdbgTotales.Width = Width - 330
    
    sdbgDetalle.Columns(0).Width = sdbgDetalle.Width * 12 / 100
    sdbgDetalle.Columns(1).Width = sdbgDetalle.Width * 35 / 100
    sdbgDetalle.Columns(2).Width = sdbgDetalle.Width * 20 / 100
    sdbgDetalle.Columns(3).Width = sdbgDetalle.Width * 20 / 100
    sdbgDetalle.Columns(4).Width = sdbgDetalle.Width * 10 / 100
    
    sdbgTotales.Columns(0).Width = sdbgTotales.Width * 49 / 100
    sdbgTotales.Columns(1).Width = sdbgTotales.Width * 22 / 100
    sdbgTotales.Columns(2).Width = sdbgTotales.Width * 41 / 100
End Sub

Private Sub CargarVolumen()
    Dim Ador As Ador.Recordset
    Dim sum As Double
    
    If g_oGestorInformes Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    'Limpia el grid
    sdbgDetalle.RemoveAll
    sdbgTotales.RemoveAll
    
    Set Ador = g_oGestorInformes.DevolverVolumenAdjudicado(txtFecDesde.Text, txtFecHasta.Text, sdbcCia.Columns(0).Value, 0)
    If Not Ador Is Nothing Then
        sum = 0
        While Not Ador.EOF
            sdbgDetalle.AddItem Ador("COD") & Chr(9) & Ador("DEN") & Chr(9) & Ador("VOLPOR") & Chr(9) & Ador("VOLGS") & Chr(9) & Ador("MGS") & Chr(9) & Ador("MONPOR") & Chr(9) & Ador("ID")
            sum = sum + NullToDbl0(Ador("VOLPOR"))
            Ador.MoveNext
        Wend
        
        'Calcula los totales
        sdbgTotales.AddItem "  " & sIdiTotAdj & Chr(9) & sum & Chr(9) & Space(3) & DenMonCentral
    End If
    
    Ador.Close
    Set Ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgDetalle_HeadClick(ByVal ColIndex As Integer)
    ''Ordena el grid
    Dim Ador As Ador.Recordset
    
    If sdbgDetalle.Rows < 1 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    'Limpia el grid
    sdbgDetalle.RemoveAll
    
    Set Ador = g_oGestorInformes.DevolverVolumenAdjudicado(txtFecDesde.Text, txtFecHasta.Text, sdbcCia.Columns(0).Value, ColIndex)
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            sdbgDetalle.AddItem Ador("COD") & Chr(9) & Ador("DEN") & Chr(9) & Ador("VOLPOR") & Chr(9) & Ador("VOLGS") & Chr(9) & Ador("MGS") & Chr(9) & Ador("MONPOR") & Chr(9) & Ador("ID")
            Ador.MoveNext
        Wend
    End If
    
    Ador.Close
    Set Ador = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgDetalle_RowLoaded(ByVal Bookmark As Variant)
    'Si un prov. tiene adjudicaciones de una compa��a cuya moneda central
    'no est� enlazada al portal el volumen en el portal saldr� en blanco
    
    'If sdbgDetalle.Columns(5).Value = "" Then
    '    sdbgTotales.Columns(2).Value = ""
    'End If

End Sub

Private Sub txtFecDesde_Change()
    'Limpia el grid
    sdbgDetalle.RemoveAll
    sdbgTotales.RemoveAll
End Sub

Private Sub txtFecDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtFecDesde.Text) And Not txtFecDesde.Text = "" Then
        basMensajes.NoValida (sIdiFecDesde)
        txtFecDesde.Text = ""
        Cancel = True
    End If
End Sub

Private Sub txtFecHasta_Change()
    'Limpia el grid
    sdbgDetalle.RemoveAll
    sdbgTotales.RemoveAll
End Sub

Private Sub txtFecHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtFecHasta.Text) And Not txtFecHasta.Text = "" Then
        basMensajes.NoValida (sIdiFecHasta)
        txtFecHasta.Text = ""
        Cancel = True
    End If
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_VOLADJUDICADOPROV, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblCia.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFecDesde.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFecHasta.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCia.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCia.Columns(2).Caption = Ador(0).Value
        Me.sdbgDetalle.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(3).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgDetalle.Columns(4).Caption = Ador(0).Value
        Ador.MoveNext
        sIdiFecDesde = Ador(0).Value
        Ador.MoveNext
        sIdiFecHasta = Ador(0).Value
        Ador.MoveNext
        sIdiProveedor = Ador(0).Value
        Ador.MoveNext
        sIdiPor = Ador(0).Value
        Ador.MoveNext
        sIdiTotAdj = Ador(0).Value
        Ador.MoveNext
        
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub



