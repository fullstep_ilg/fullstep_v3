VERSION 5.00
Begin VB.Form frmComunicaComent 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Comentarios"
   ClientHeight    =   3150
   ClientLeft      =   1995
   ClientTop       =   4530
   ClientWidth     =   6585
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCOMUNICAComent.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   6585
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   3300
      TabIndex        =   7
      Top             =   2760
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   2190
      TabIndex        =   6
      Top             =   2760
      Width           =   1005
   End
   Begin VB.TextBox txtCom 
      Height          =   1755
      Left            =   105
      MaxLength       =   500
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   855
      Width           =   6315
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Compañía:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   105
      TabIndex        =   5
      Top             =   150
      Width           =   1050
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Comunicación:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   105
      TabIndex        =   4
      Top             =   510
      Width           =   1050
   End
   Begin VB.Label lblCia 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1155
      TabIndex        =   3
      Top             =   120
      Width           =   5220
   End
   Begin VB.Label lblTipoC 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   2370
      TabIndex        =   2
      Top             =   495
      Width           =   4005
   End
   Begin VB.Label lblFec 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1155
      TabIndex        =   1
      Top             =   495
      Width           =   1155
   End
End
Attribute VB_Name = "frmComunicaComent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables públicas
Public oComunic As cComunicacion
Public oCia As CCia
Public blnEdicion As Boolean

'Variables privadas
Private blnChange As Boolean

Private Sub cmdAceptar_Click()
    If blnChange Then
        'actualiza la base de datos con los comentarios de la comunicación
        oComunic.Comentario = txtCom.Text
        oComunic.ModificarDatosComunicacion True
    End If
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    'Carga las etiquetas con los datos de la compañía y la comuncicación
    lblCia.Caption = oCia.Cod & " - " & oCia.Den
    CargarRecursos
    If oComunic Is Nothing Then
        lblFec.Caption = ""
        lblTipoC.Caption = ""
        txtCom.Text = ""
    Else
   '     Me.lblFec.Caption = oComunic.Fecha
   '     Me.lblTipoC.Caption = oComunic.DenComunicacion
        
        'Rellena los comentarios
        If (oComunic.Comentario <> "") And (Not IsNull(oComunic.Comentario)) And (Not IsMissing(oComunic.Comentario)) Then
            txtCom.Text = oComunic.Comentario
        End If
    End If
    
    If blnEdicion Then
        txtCom.Locked = False
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
    Else
        txtCom.Locked = True
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
    End If
    
    blnChange = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oComunic = Nothing
    Set oCia = Nothing
End Sub

Private Sub txtCom_Change()
    blnChange = True
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_COMUNICACOMENT, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label2.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub



