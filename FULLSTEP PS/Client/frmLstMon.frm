VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstMon 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de monedas (Opciones)"
   ClientHeight    =   2085
   ClientLeft      =   765
   ClientTop       =   1305
   ClientWidth     =   4860
   Icon            =   "frmLstMon.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2085
   ScaleWidth      =   4860
   Begin TabDlg.SSTab SSTab1 
      Height          =   1635
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   2884
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Orden"
      TabPicture(0)   =   "frmLstMon.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin VB.Frame Frame1 
         Height          =   1095
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   4335
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2280
            TabIndex        =   4
            Top             =   480
            Width           =   1515
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   600
            TabIndex        =   3
            Top             =   480
            Value           =   -1  'True
            Width           =   1515
         End
      End
   End
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      TabIndex        =   0
      Top             =   1680
      Width           =   1335
   End
End
Attribute VB_Name = "frmLstMon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private sIdiTxtCodigo As String
Private sIdiTxtEquiv As String
Private sIdiTxtTitulo As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String
Private sIdiTxtDen As String

Private m_oIdiomas As CIdiomas

Private Sub cmdObtener_Click()
ObtenerInforme
Unload Me
End Sub

Private Sub ObtenerInforme()
    Dim oReport As CRAXDRT.Report
    Dim oCRmonedas As New CRMonedas
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim oIdi As CIdioma
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Comprueba que el el path sea correcto y que exista el informe
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptmon.rpt"
   
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oFos = Nothing
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = "'" & sIdiTxtTitulo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = "'" & sIdiTxtCodigo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEquiv")).Text = "'" & sIdiTxtEquiv & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"
    For Each oIdi In m_oIdiomas
        oReport.FormulaFields(crs_FormulaIndex(oReport, "TIT" & oIdi.Cod)).Text = """" & sIdiTxtDen & " (" & oIdi.Den & ")" & """"
    Next
    oReport.FormulaFields(crs_FormulaIndex(oReport, "IDI")).Text = "'" & g_udtParametrosGenerales.g_sIdioma & "'"
    
    oCRmonedas.ListadoGeneral oReport, opOrdCod.Value, opOrdDen.Value
            
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   
    Set pv = New Preview
    Unload Me
    pv.Hide
    pv.Caption = Me.Caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    
    Set oReport = Nothing
    
    Unload Me
    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Load()
    Dim x As Variant
    Me.Width = 4915
    Me.Height = 2460
    
    Set m_oIdiomas = g_oGestorParametros.DevolverIdiomas(False, True)
    
    CargarRecursos
    
    Me.Top = 330
    Me.Left = 330
End Sub

Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTMON, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        sIdiTxtTitulo = ador(0).Value
        ador.MoveNext
        Me.cmdObtener.Caption = ador(0).Value
        ador.MoveNext
        Me.opOrdCod.Caption = ador(0).Value
        sIdiTxtCodigo = ador(0).Value
        ador.MoveNext
        Me.opOrdDen.Caption = ador(0).Value
        sIdiTxtDen = ador(0).Value
        ador.MoveNext
        Me.SSTab1.Caption = ador(0).Value
        ador.MoveNext
        sIdiTxtEquiv = ador(0).Value
        ador.MoveNext
        sIdiTxtPag = ador(0).Value
        ador.MoveNext
        sIdiTxtDe = ador(0).Value
        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub






