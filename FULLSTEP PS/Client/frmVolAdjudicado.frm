VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmVolAdjudicado 
   Caption         =   "Volumen adjudicado"
   ClientHeight    =   5745
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8595
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVolAdjudicado.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5745
   ScaleWidth      =   8595
   Begin VB.Frame frameLeyenda 
      BorderStyle     =   0  'None
      Height          =   400
      Left            =   360
      TabIndex        =   12
      Top             =   5640
      Width           =   9000
      Begin VB.Label lblLeyendaT 
         Caption         =   "Posee adjudicaciones de compa��as con la moneda central sin enlazar al portal"
         Height          =   315
         Left            =   360
         TabIndex        =   14
         Top             =   0
         Width           =   6500
      End
      Begin VB.Label lblLeyenda 
         BackColor       =   &H00C0FFFF&
         ForeColor       =   &H00400040&
         Height          =   135
         Left            =   0
         TabIndex        =   13
         Top             =   30
         Width           =   255
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgVolumen 
      Height          =   4455
      Left            =   120
      TabIndex        =   9
      Top             =   840
      Width           =   8415
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   7
      stylesets.count =   1
      stylesets(0).Name=   "SinValor"
      stylesets(0).BackColor=   12648447
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVolAdjudicado.frx":0CB2
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   3200
      Columns(0).Caption=   "Proveedor"
      Columns(0).Name =   "PROV"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   6773
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3810
      Columns(2).Caption=   "Adjudicado "
      Columns(2).Name =   "ADJ"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777215
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ADJ GS"
      Columns(3).Name =   "ADJ GS"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "MONGS"
      Columns(4).Name =   "MONGS"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "MONPS"
      Columns(5).Name =   "MONPS"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID"
      Columns(6).Name =   "ID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   14843
      _ExtentY        =   7858
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame frameFiltro 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8415
      Begin VB.CommandButton cmdExcel 
         Height          =   285
         Left            =   7900
         Picture         =   "frmVolAdjudicado.frx":0CCE
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         Height          =   285
         Left            =   7560
         Picture         =   "frmVolAdjudicado.frx":11E0
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdCalFecApeDesde 
         Height          =   285
         Left            =   2530
         Picture         =   "frmVolAdjudicado.frx":12E2
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Mantenimiento"
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   800
         TabIndex        =   2
         Top             =   240
         Width           =   1700
      End
      Begin VB.CommandButton cmdCalFecApeHasta 
         Height          =   285
         Left            =   5660
         Picture         =   "frmVolAdjudicado.frx":186C
         Style           =   1  'Graphical
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Mantenimiento"
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   3930
         TabIndex        =   4
         Top             =   240
         Width           =   1700
      End
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   7155
         Picture         =   "frmVolAdjudicado.frx":1DF6
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   315
      End
      Begin VB.Label lblFecDesde 
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   270
         Width           =   825
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Hasta:"
         Height          =   225
         Left            =   3240
         TabIndex        =   5
         Top             =   270
         Width           =   615
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   120
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   5320
      Width           =   8415
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   2
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVolAdjudicado.frx":2138
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmVolAdjudicado.frx":2154
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmVolAdjudicado.frx":2170
      DividerType     =   1
      DividerStyle    =   2
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   2
      Columns(0).Width=   10583
      Columns(0).Caption=   "TIT"
      Columns(0).Name =   "TIT"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   5292
      Columns(1).Caption=   "ADJ"
      Columns(1).Name =   "ADJ"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "Standard"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      _ExtentX        =   14843
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmVolAdjudicado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oInformes As CGestorInformes

Private DenMonCentral As String

'Formulario para los listados
Public ofrmLstInfVol As frmLstInfVol

Private sIdiFecDesde As String
Private sIdiFecHasta As String
Private sIdiTotAdj As String

Private Sub cmdBuscar_Click()
    'Comprueba que se hayan introducido todos los datos
    If txtFecDesde = "" Then
        basMensajes.NoValida sIdiFecDesde
        Exit Sub
    End If
    
    If txtFecHasta = "" Then
        basMensajes.NoValida sIdiFecHasta
        Exit Sub
    End If
    
    If CDate(txtFecHasta.Text) < CDate(txtFecDesde.Text) Then
        basMensajes.FechaHastaMenor
        txtFecHasta.SetFocus
        Exit Sub
    End If
    
    'Rellena el grid
    CargarVolumen
End Sub

Private Sub cmdCalFecApeDesde_Click()
    'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmVolAdjudicado
    Set frmCalendar.ctrDestination = txtFecDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde <> "" Then
        frmCalendar.Calendar.Value = txtFecDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdCalFecApeHasta_Click()
    'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmVolAdjudicado
    Set frmCalendar.ctrDestination = txtFecHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecHasta <> "" Then
        frmCalendar.Calendar.Value = txtFecHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdExcel_Click()
    Set ofrmLstInfVol = New frmLstInfVol
  
    ofrmLstInfVol.WindowState = vbNormal
    
    If txtFecDesde.Text <> "" Then
        ofrmLstInfVol.txtFecDesde = txtFecDesde.Text
    End If
    
    If txtFecHasta.Text <> "" Then
        ofrmLstInfVol.txtFecHasta = txtFecHasta.Text
    End If
    
    ofrmLstInfVol.DenMonCentral = DenMonCentral
    ofrmLstInfVol.bExcel = True
    
    ofrmLstInfVol.Show vbModal
End Sub

Private Sub cmdImprimir_Click()
    Set ofrmLstInfVol = New frmLstInfVol
  
    ofrmLstInfVol.WindowState = vbNormal
    
    If txtFecDesde.Text <> "" Then
        ofrmLstInfVol.txtFecDesde = txtFecDesde.Text
    End If
    
    If txtFecHasta.Text <> "" Then
        ofrmLstInfVol.txtFecHasta = txtFecHasta.Text
    End If
    
    ofrmLstInfVol.DenMonCentral = DenMonCentral
    
    ofrmLstInfVol.Show vbModal
End Sub

Private Sub Form_Load()
    Dim Ador As Ador.Recordset
    
    'Dimensiona el formulario
    Me.Height = 6150
    Me.Width = 8715
    
    CargarRecursos
    
    'Obtiene el c�digo de la moneda central
    Set Ador = g_oRaiz.CodMonedaCentral
    DenMonCentral = Ador(1)
    sdbgVolumen.Columns(2).Caption = Me.sdbgVolumen.Columns(2).Caption & "(" & Ador(0) & ")"
    
    Ador.Close
    Set Ador = Nothing
    
    'Fechas del mes actual por defecto
    Me.txtFecDesde = Format("01/" & Month(Date) & "/" & Year(Date), "dd/mm/yyyy")
    Me.txtFecHasta = Date
    
    'Carga la pantalla
    sdbgTotales.AllowUpdate = False
    CargarVolumen
    sdbgTotales.AllowUpdate = True
    
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora = True Then
        lblLeyendaT.Visible = False
        lblLeyenda.Visible = False
    End If
    
End Sub

Private Sub Form_Resize()
    'Redimensiona el formulario
    Arrange
End Sub

Private Sub sdbgVolumen_DblClick()
    If sdbgVolumen.Rows < 1 Then Exit Sub
    
    'Carga el detalle
    
    frmDetalleVolAdj.CargarVolumen txtFecDesde.Text, txtFecHasta.Text, sdbgVolumen.Columns(6).Value
    frmDetalleVolAdj.Caption = frmDetalleVolAdj.Caption & sdbgVolumen.Columns(0).Value & " - " & sdbgVolumen.Columns(1).Value & " (" & txtFecDesde.Text & " - " & txtFecHasta.Text & ")"
    
    frmDetalleVolAdj.FecDesde = txtFecDesde.Text
    frmDetalleVolAdj.FecHasta = txtFecHasta.Text
    frmDetalleVolAdj.CiaProv = sdbgVolumen.Columns(6).Value
    
    frmDetalleVolAdj.strCodProv = sdbgVolumen.Columns(0).Value
    frmDetalleVolAdj.strDenProv = sdbgVolumen.Columns(1).Value
    
    'frmDetalleVolAdj.Top = sdbgVolumen.Top + Me.Top + frmMDI.Top + sdbgVolumen.RowTop(sdbgVolumen.Row) + sdbgVolumen.RowHeight + 1000
    'frmDetalleVolAdj.Left = sdbgVolumen.Left + Left + 400 + frmMDI.Left
    
    frmDetalleVolAdj.Show vbModal
End Sub


Private Sub Arrange()
    If Width >= 300 Then frameFiltro.Width = Width - 300
    
    If Height >= 1695 Then sdbgVolumen.Height = Height - 1895
    If Width >= 300 Then sdbgVolumen.Width = Width - 300
    
    If Height >= 1130 Then sdbgTotales.Top = sdbgVolumen.Top + sdbgVolumen.Height + 25
    If Width >= 300 Then sdbgTotales.Width = Width - 300
    
    sdbgVolumen.Columns(0).Width = sdbgVolumen.Width * 21 / 100
    sdbgVolumen.Columns(1).Width = sdbgVolumen.Width * 49 / 100
    sdbgVolumen.Columns(2).Width = sdbgVolumen.Width * 26 / 100
    
    sdbgTotales.Columns(0).Width = sdbgTotales.Width * 71 / 100
    sdbgTotales.Columns(1).Width = sdbgTotales.Width * 29 / 100
    
    'Leyenda
    frameLeyenda.Top = Height - 700
    frameLeyenda.Left = sdbgTotales.Left
End Sub

Private Sub sdbgVolumen_HeadClick(ByVal ColIndex As Integer)
    'Ordena el grid seg�n la columna
    Dim Ador As Ador.Recordset
    Dim sum As Double
    Dim Vol As Long
    
    If sdbgVolumen.Rows < 1 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    'Limpia el grid
    sdbgVolumen.RemoveAll
    
    Set Ador = g_oGestorInformes.DevolverVolumenAdjudicado(txtFecDesde.Text, txtFecHasta.Text, , ColIndex)
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            'comprueba si la compa��a proveedora tiene adjudicaciones de una cia cuya
            'moneda central no est� enlazada en el portal
            'bEnlazada = False
            If Ador("VOLPOR") <> Ador("VOLPORD") Then
                'bEnlazada = True
                sdbgVolumen.AddItem Ador("COD") & Chr(9) & Ador("DEN") & Chr(9) & "" & Chr(9) & Ador("VOLGS") & Chr(9) & Ador("MONGS") & Chr(9) & False & Chr(9) & Ador("ID")
            Else
                sdbgVolumen.AddItem Ador("COD") & Chr(9) & Ador("DEN") & Chr(9) & Ador("VOLPOR") & Chr(9) & Ador("VOLGS") & Chr(9) & Ador("MONGS") & Chr(9) & True & Chr(9) & Ador("ID")
            End If
            
            
            Ador.MoveNext
        Wend
    
    End If
    
    Ador.Close
    Set Ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub CargarVolumen()
    Dim Ador As Ador.Recordset
    Dim sum As Double
    
    If txtFecDesde = "" Or txtFecHasta = "" Then Exit Sub
    
    If g_oGestorInformes Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    'Limpia los grids
    sdbgVolumen.RemoveAll
    sdbgTotales.RemoveAll
    
    Set Ador = g_oGestorInformes.DevolverVolumenAdjudicado(txtFecDesde.Text, txtFecHasta.Text, , 0)
    If Not Ador Is Nothing Then
        sum = 0
        While Not Ador.EOF
            'comprueba si la compa��a proveedora tiene adjudicaciones de una cia cuya
            'moneda central no est� enlazada en el portal
            'bEnlazada = False
            If Ador("VOLPOR") <> Ador("VOLPORD") Then
                'bEnlazada = True
                sdbgVolumen.AddItem Ador("COD") & Chr(9) & Ador("DEN") & Chr(9) & "" & Chr(9) & Ador("VOLGS") & Chr(9) & Ador("MONGS") & Chr(9) & False & Chr(9) & Ador("ID")
            Else
                sdbgVolumen.AddItem Ador("COD") & Chr(9) & Ador("DEN") & Chr(9) & Ador("VOLPOR") & Chr(9) & Ador("VOLGS") & Chr(9) & Ador("MONGS") & Chr(9) & True & Chr(9) & Ador("ID")
                sum = sum + NullToDbl0(Ador("VOLPOR"))
            End If
            
            Ador.MoveNext
        Wend
        
        'Calcula el total
        sdbgTotales.AddItem "  " & sIdiTotAdj & " " & DenMonCentral & Chr(9) & sum
    End If
    
    Ador.Close
    Set Ador = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgVolumen_RowLoaded(ByVal Bookmark As Variant)
    'Si un prov. tiene adjudicaciones de una compa��a cuya moneda central
    'no est� enlazada al portal,se muestra el acumulado de las adjudicaciones
    'que si tienen moneda enlazada,pero se muestra en amarillo para indicarlo
    
    If sdbgVolumen.Columns(5).Value = "False" Then
        sdbgVolumen.Columns(2).CellStyleSet "SinValor"
    End If
End Sub

Private Sub txtFecDesde_Change()
    sdbgVolumen.RemoveAll
    sdbgTotales.RemoveAll
End Sub

Private Sub txtFecDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtFecDesde.Text) And Not txtFecDesde.Text = "" Then
        basMensajes.NoValida (sIdiFecDesde)
        txtFecDesde.Text = ""
        Cancel = True
    End If
End Sub

Private Sub txtFecHasta_Change()
    sdbgVolumen.RemoveAll
    sdbgTotales.RemoveAll
End Sub

Private Sub txtFecHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtFecHasta.Text) And Not txtFecHasta.Text = "" Then
        basMensajes.NoValida (sIdiFecHasta)
        txtFecHasta.Text = ""
        Cancel = True
    End If
    
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_VOLADJUDICADO, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFecDesde.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFecHasta.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblLeyendaT.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgVolumen.Columns(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgVolumen.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgVolumen.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        sIdiFecDesde = Ador(0).Value
        Ador.MoveNext
        sIdiFecHasta = Ador(0).Value
        Ador.MoveNext
        sIdiTotAdj = Ador(0).Value
       
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub




