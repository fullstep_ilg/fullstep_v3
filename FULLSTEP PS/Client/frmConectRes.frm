VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmConectRes 
   Caption         =   "Resultado del control de conectividad"
   ClientHeight    =   3315
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9150
   Icon            =   "frmConectRes.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3315
   ScaleWidth      =   9150
   Begin SSDataWidgets_B.SSDBGrid sdbgCias 
      Height          =   3135
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   8985
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConectRes.frx":014A
      AllowUpdate     =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2699
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   4313
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1508
      Columns(3).Caption=   "Accesible"
      Columns(3).Name =   "RES"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   3200
      Columns(4).Caption=   "Servidor"
      Columns(4).Name =   "SRV"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "BaseDeDatos"
      Columns(5).Name =   "BD"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   15849
      _ExtentY        =   5530
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmConectRes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Width = 9270
    Height = 3825
    CargarRecursos
   
    
End Sub

Private Sub Form_Resize()
    
    If Me.Height < 700 Then Exit Sub
    If Me.Width < 400 Then Exit Sub
    
     sdbgCias.Height = Me.Height - 550
    sdbgCias.Width = Me.Width - 300
    
    sdbgCias.Columns(1).Width = sdbgCias.Width * 0.2
    sdbgCias.Columns(2).Width = sdbgCias.Width * 0.3
    sdbgCias.Columns(3).Width = sdbgCias.Width * 0.1
    sdbgCias.Columns(4).Width = sdbgCias.Width * 0.2
    sdbgCias.Columns(5).Width = sdbgCias.Width * 0.2 - 350
     
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONECTRES, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCias.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCias.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCias.Columns(3).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCias.Columns(4).Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbgCias.Columns(5).Caption = Ador(0).Value
        Ador.MoveNext
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

