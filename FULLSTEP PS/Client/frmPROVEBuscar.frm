VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPROVEBuscar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Proveedores de la base de datos de la compa��a compradora"
   ClientHeight    =   4950
   ClientLeft      =   3405
   ClientTop       =   4335
   ClientWidth     =   7275
   Icon            =   "frmPROVEBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4950
   ScaleWidth      =   7275
   StartUpPosition =   1  'CenterOwner
   Begin TabDlg.SSTab stabGeneral 
      Height          =   4860
      Left            =   15
      TabIndex        =   11
      Top             =   45
      Width           =   7200
      _ExtentX        =   12700
      _ExtentY        =   8573
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Filtro"
      TabPicture(0)   =   "frmPROVEBuscar.frx":014A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(1)=   "Frame2"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Proveedores"
      TabPicture(1)   =   "frmPROVEBuscar.frx":0166
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "stabPROVE"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "picEdit"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "fraProve"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      Begin VB.Frame Frame2 
         Caption         =   "Datos generales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1425
         Left            =   -74760
         TabIndex        =   21
         Top             =   480
         Width           =   6720
         Begin VB.TextBox txtDen 
            Height          =   285
            Left            =   3090
            MaxLength       =   100
            TabIndex        =   23
            Top             =   240
            Width           =   3195
         End
         Begin VB.TextBox txtCod 
            Height          =   285
            Left            =   1470
            MaxLength       =   20
            TabIndex        =   22
            Top             =   240
            Width           =   1050
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
            Height          =   285
            Left            =   1470
            TabIndex        =   24
            Top             =   600
            Width           =   1065
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
            Height          =   285
            Left            =   2550
            TabIndex        =   25
            Top             =   600
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
            Height          =   285
            Left            =   1470
            TabIndex        =   26
            Top             =   960
            Width           =   1065
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
            Height          =   285
            Left            =   2550
            TabIndex        =   27
            Top             =   960
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label Label25 
            Caption         =   "Den:"
            Height          =   225
            Left            =   2610
            TabIndex        =   31
            Top             =   300
            Width           =   390
         End
         Begin VB.Label Label15 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   120
            TabIndex        =   30
            Top             =   300
            Width           =   1320
         End
         Begin VB.Label Label14 
            Caption         =   "Provincia:"
            Height          =   225
            Left            =   120
            TabIndex        =   29
            Top             =   1020
            Width           =   1320
         End
         Begin VB.Label Label8 
            Caption         =   "Pa�s:"
            Height          =   225
            Left            =   120
            TabIndex        =   28
            Top             =   660
            Width           =   1320
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Selecci�n por material"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2580
         Left            =   -74760
         TabIndex        =   16
         Top             =   1965
         Width           =   6720
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   300
            Left            =   1470
            TabIndex        =   0
            Top             =   690
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1905
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7408
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
            Height          =   300
            Left            =   1470
            TabIndex        =   2
            Top             =   1140
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1905
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7408
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
            Height          =   300
            Left            =   1470
            TabIndex        =   4
            Top             =   1590
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1905
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7408
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
            Height          =   300
            Left            =   1470
            TabIndex        =   6
            Top             =   2040
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1905
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7408
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
            Height          =   300
            Left            =   2550
            TabIndex        =   1
            Top             =   690
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
            Height          =   300
            Left            =   2550
            TabIndex        =   3
            Top             =   1140
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
            Height          =   300
            Left            =   2550
            TabIndex        =   5
            Top             =   1590
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
            Height          =   300
            Left            =   2550
            TabIndex        =   7
            Top             =   2040
            Width           =   3765
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   4657
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1535
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6641
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "Subfamilia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   20
            Top             =   1635
            Width           =   1185
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "Familia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   19
            Top             =   1170
            Width           =   1185
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Comodity:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   18
            Top             =   705
            Width           =   1260
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "Grupo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   180
            TabIndex        =   17
            Top             =   2100
            Width           =   1185
         End
      End
      Begin VB.Frame fraProve 
         Height          =   750
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   6930
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   2505
            TabIndex        =   8
            Top             =   270
            Width           =   4275
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   7541
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.CommandButton cmdBuscar 
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6990
            Style           =   1  'Graphical
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   270
            Width           =   300
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   900
            TabIndex        =   42
            Top             =   270
            Width           =   1545
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2725
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   180
            TabIndex        =   12
            Top             =   330
            Width           =   735
         End
      End
      Begin VB.PictureBox picEdit 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   75
         ScaleHeight     =   435
         ScaleWidth      =   6975
         TabIndex        =   13
         Top             =   4335
         Width           =   6975
         Begin VB.CommandButton cmdAlta 
            Caption         =   "&Alta en GS"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   56
            Top             =   75
            Width           =   1200
         End
         Begin VB.CommandButton cmdEdicion 
            Caption         =   "&Edici�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5775
            TabIndex        =   54
            Top             =   75
            Visible         =   0   'False
            Width           =   1200
         End
         Begin VB.CommandButton cmdDesEnlazar 
            Caption         =   "&Desenlazar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1350
            TabIndex        =   53
            Top             =   75
            Width           =   1200
         End
         Begin VB.CommandButton cmdSelecionar 
            Caption         =   "&Seleccionar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2700
            TabIndex        =   15
            Top             =   75
            Width           =   1200
         End
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "&Cancelar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4050
            TabIndex        =   14
            Top             =   75
            Width           =   1200
         End
      End
      Begin TabDlg.SSTab stabPROVE 
         Height          =   3135
         Left            =   75
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   1185
         Width           =   6975
         _ExtentX        =   12303
         _ExtentY        =   5530
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Datos generales"
         TabPicture(0)   =   "frmPROVEBuscar.frx":0182
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraGeneral"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Material"
         TabPicture(1)   =   "frmPROVEBuscar.frx":019E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "tvwEstrMatMod"
         Tab(1).Control(1)=   "ImageList1"
         Tab(1).Control(2)=   "tvwEstrMat"
         Tab(1).ControlCount=   3
         Begin MSComctlLib.TreeView tvwEstrMat 
            Height          =   2565
            Left            =   -74790
            TabIndex        =   41
            Top             =   405
            Width           =   6540
            _ExtentX        =   11536
            _ExtentY        =   4524
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            HotTracking     =   -1  'True
            SingleSel       =   -1  'True
            ImageList       =   "ImageList1"
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Frame fraGeneral 
            Enabled         =   0   'False
            Height          =   2475
            Left            =   180
            TabIndex        =   33
            Top             =   435
            Width           =   6615
            Begin VB.Label lblProviDen 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2445
               TabIndex        =   52
               Top             =   2040
               Width           =   4050
            End
            Begin VB.Label lblProviCod 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1335
               TabIndex        =   51
               Top             =   2040
               Width           =   1020
            End
            Begin VB.Label lblMonDen 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2445
               TabIndex        =   50
               Top             =   1680
               Width           =   4050
            End
            Begin VB.Label lblMonCod 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1335
               TabIndex        =   49
               Top             =   1680
               Width           =   1020
            End
            Begin VB.Label lblPaiDen 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   2445
               TabIndex        =   48
               Top             =   1320
               Width           =   4050
            End
            Begin VB.Label lblPaiCod 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1335
               TabIndex        =   47
               Top             =   1320
               Width           =   1020
            End
            Begin VB.Label lblPob 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   3735
               TabIndex        =   46
               Top             =   960
               Width           =   2760
            End
            Begin VB.Label lblCP 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1335
               TabIndex        =   45
               Top             =   960
               Width           =   1350
            End
            Begin VB.Label lblDir 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1335
               TabIndex        =   44
               Top             =   600
               Width           =   5160
            End
            Begin VB.Label lblNIF 
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   1335
               TabIndex        =   43
               Top             =   240
               UseMnemonic     =   0   'False
               Width           =   1815
            End
            Begin VB.Label Label7 
               Caption         =   "NIF:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   180
               TabIndex        =   40
               Top             =   300
               Width           =   1320
            End
            Begin VB.Label Label21 
               Caption         =   "Moneda:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   180
               TabIndex        =   39
               Top             =   1740
               Width           =   1320
            End
            Begin VB.Label Label2 
               Caption         =   "Direcci�n:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   180
               TabIndex        =   38
               Top             =   660
               Width           =   1320
            End
            Begin VB.Label Label3 
               Caption         =   "C�digo postal:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   180
               TabIndex        =   37
               Top             =   1020
               Width           =   1320
            End
            Begin VB.Label Label4 
               Caption         =   "Poblaci�n:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   2805
               TabIndex        =   36
               Top             =   1020
               Width           =   870
            End
            Begin VB.Label Label5 
               Caption         =   "Pa�s:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   180
               TabIndex        =   35
               Top             =   1380
               Width           =   1320
            End
            Begin VB.Label Label6 
               Caption         =   "Provincia:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Left            =   180
               TabIndex        =   34
               Top             =   2100
               Width           =   1320
            End
         End
         Begin MSComctlLib.ImageList ImageList1 
            Left            =   -75000
            Top             =   0
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   16
            ImageHeight     =   16
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   12
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":01BA
                  Key             =   "Raiz"
                  Object.Tag             =   "Raiz"
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":060E
                  Key             =   "ACT"
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":0720
                  Key             =   "ACTASIG"
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":0832
                  Key             =   "Raiz2"
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":0C84
                  Key             =   "GMN4"
                  Object.Tag             =   "GMN4"
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":0FD8
                  Key             =   "GMN4A"
                  Object.Tag             =   "GMN4A"
               EndProperty
               BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":132C
                  Key             =   "GMN1"
                  Object.Tag             =   "GMN1"
               EndProperty
               BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":1680
                  Key             =   "GMN1A"
                  Object.Tag             =   "GMN1A"
               EndProperty
               BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":19D4
                  Key             =   "GMN2"
                  Object.Tag             =   "GMN2"
               EndProperty
               BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":1D28
                  Key             =   "GMN2A"
                  Object.Tag             =   "GMN2A"
               EndProperty
               BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":207C
                  Key             =   "GMN3A"
                  Object.Tag             =   "GMN3A"
               EndProperty
               BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmPROVEBuscar.frx":23D0
                  Key             =   "GMN3"
                  Object.Tag             =   "GMN3"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.TreeView tvwEstrMatMod 
            Height          =   2565
            Left            =   -74790
            TabIndex        =   55
            Top             =   405
            Visible         =   0   'False
            Width           =   6540
            _ExtentX        =   11536
            _ExtentY        =   4524
            _Version        =   393217
            LabelEdit       =   1
            Style           =   7
            Checkboxes      =   -1  'True
            HotTracking     =   -1  'True
            ImageList       =   "ImageList1"
            Appearance      =   1
         End
      End
   End
End
Attribute VB_Name = "frmPROVEBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variable para no ejecutar el node check si se ha disparado el mousedown
Private bMouseDown As Boolean
Private SelNode As MSComctlLib.Node

Public sOrigen As String
Private oCiaSeleccionada As CCia
Private PaiRespetarCombo As Boolean
Private PaiCargarComboDesde As Boolean
Private ProviRespetarCombo As Boolean
Private ProveRespetarCombo As Boolean
Private ProviCargarComboDesde As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN1RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Private GMN2RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean
Private GMN3RespetarCombo As Boolean
Private GMN4CargarComboDesde As Boolean
Private GMN4RespetarCombo As Boolean
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4
Private oGrupsMN1 As CGruposMatNivel1
Private oProveSeleccionado As CProveedor
Private oGruposMN1Seleccionados As CGruposMatNivel1
Private oGruposMN2Seleccionados As CGruposMatNivel2
Private oGruposMN3Seleccionados As CGruposMatNivel3
Private oGruposMN4Seleccionados As CGruposMatNivel4
Private oGruposMN1DesSeleccionados As CGruposMatNivel1
Private oGruposMN2DesSeleccionados As CGruposMatNivel2
Private oGruposMN3DesSeleccionados As CGruposMatNivel3
Private oGruposMN4DesSeleccionados As CGruposMatNivel4
Public IdCia As Long

Private sIdiAltaProve  As String
Private sIdiContinuar As String
Private sIdiEnlace As String
Private sIdiEdicion As String
Private sIdiConsulta As String
Private sIdiCambiarEnlace As String
Private sIdiConfirmEnlace As String
Private sIdiEnlazar As String
Private sIdiSeleccionar As String
Private sIdiNoExiste As String
Private sIdiProve As String
Private sIdiMats As String
Private sIdiMat As String
Private msIdiProve As String


Private Sub cmdAlta_Click()
Dim oEnlace As CEnlace
Dim oEnlaces As CEnlaces
Dim iRespuesta As Integer
Dim teserror As CTESError
    
    If Trim(sdbcProveCod.Text) = "" Then Exit Sub
    

    
    Set oEnlace = basPublic.g_oRaiz.Generar_CEnlace
    oEnlace.IDCiaCompradora = basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora
    oEnlace.IDProveedorPortal = frmCompanias.g_oCiaSeleccionada.ID
    oEnlace.BaseDatosGS = basParametros.g_udtParametrosGenerales.g_sFSBDCiaCompradora
    oEnlace.ServidorGS = basParametros.g_udtParametrosGenerales.g_sFSSRVCiaCompradora
    oEnlace.NIF = frmCompanias.g_oCiaSeleccionada.NIF
    
    
        '''A�adir enlace
        oEnlace.CodigoProveedorGS = sdbcProveCod.Text
        iRespuesta = MsgBox(sIdiAltaProve & " " & frmCompanias.txtDen.Text & vbCrLf & sIdiContinuar, vbYesNo)
        If iRespuesta = vbNo Then
            Set oEnlace = Nothing
            Exit Sub
        End If
        
    
        Dim oProvesVinculados As CProveedores
        Dim sProvesVinc As String
        Dim oProve As CProveedor
        Dim iRet As Integer
        
        If g_oRaiz.CodigosErpActivado(oEnlace.IDCiaCompradora) Then
            Set oProvesVinculados = g_oRaiz.ProveedoresVinculados(oEnlace.IDCiaCompradora, oEnlace.NIF, oEnlace.CodigoProveedorPortal)
            If oProvesVinculados.Count > 0 Then
                sProvesVinc = ""
                For Each oProve In oProvesVinculados
                    sProvesVinc = msIdiProve & ": " & oProve.Cod & " - " & oProve.Den & vbCrLf
                Next
                iRet = basMensajes.ExistenProveedoresVinculados(sProvesVinc)
                If iRet = vbNo Then
                    Set oEnlace = Nothing
                    Exit Sub
                End If
            End If
        End If
            
        Set teserror = oEnlace.AnyadirEnlace(Trim(frmCompanias.txtCodProveGS.Text), Trim(frmCompanias.txtNif.Text))
        If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oGruposMN1Seleccionados = Nothing
                Set oGruposMN2Seleccionados = Nothing
                Set oGruposMN3Seleccionados = Nothing
                Set oGruposMN4Seleccionados = Nothing
                Set oGruposMN1Seleccionados = g_oRaiz.Generar_CGruposMatNivel1
                Set oGruposMN2Seleccionados = g_oRaiz.Generar_CGruposMatNivel2
                Set oGruposMN3Seleccionados = g_oRaiz.Generar_CGruposMatNivel3
                Set oGruposMN4Seleccionados = g_oRaiz.Generar_CGruposMatNivel4
                Screen.MousePointer = vbNormal
                Set oEnlace = Nothing
                Exit Sub
        Else
            
            If sOrigen = "frmCompanias" Then
                g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Enlazar_GS, g_sLitRegAccion_SPA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_SPA(37) & ": " & sdbcProveCod.Text & " (" & g_sLitRegAccion_SPA(54) & ")", _
                                            g_sLitRegAccion_ENG(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_ENG(37) & ": " & sdbcProveCod.Text & " (" & g_sLitRegAccion_ENG(54) & ")", _
                                            g_sLitRegAccion_FRA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_FRA(37) & ": " & sdbcProveCod.Text & " (" & g_sLitRegAccion_FRA(54) & ")", _
                                            g_sLitRegAccion_GER(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_GER(37) & ": " & sdbcProveCod.Text & " (" & g_sLitRegAccion_GER(54) & ")", _
                                            frmCompanias.g_oCiaSeleccionada.ID
            End If
            'Cargar la estructura de materiales del GS
            sdbcProveCod_Validate False
            DoEvents
            frmCompanias.txtCodProveGS = sdbcProveCod.Text
            frmCompanias.g_oCiaSeleccionada.CodGS = sdbcProveCod.Text
            cmdDesEnlazar.Visible = True
            cmdDesEnlazar.Enabled = True
            cmdSelecionar.Enabled = False
            cmdCancelar.Enabled = False
            cmdAlta.Enabled = False
            cmdEdicion.Visible = False
            cmdEdicion.Enabled = True
            
            'Al enlazar una compa�ia, si est� activo el FSGA se mostrar� la pesta�a de Categor�as laborales.
            If g_udtParametrosGenerales.g_bAccesoFSGA Then
                frmCompanias.SSTab1.TabVisible(6) = True
            End If

            
            Exit Sub
            
            Set oEnlace = Nothing
            Set oEnlaces = Nothing
            Unload Me
        End If
              
        
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub cmdDesEnlazar_Click()
Dim oEnlace As CEnlace
Dim oEnlaces As CEnlaces
Dim iRespuesta As Integer
Dim teserror As CTESError
Dim sCodGS As String
Dim sCodCIA As String

Set oEnlaces = frmCompanias.g_oCiaSeleccionada.DevolverTodosLosEnlaces(True, OrdPorCompCod)

If oEnlaces Is Nothing Then
    MsgBox sIdiEnlace, vbInformation, "FULLSTEP PS"
    Unload Me
    Exit Sub
End If

    Set oEnlace = oEnlaces.Item(1)
        
    'Eliminar enlace
    
        iRespuesta = basMensajes.PreguntaEliminarEnlaceComp(Null, sdbcProveCod.Text)
        
        If iRespuesta = vbYes Then
            Screen.MousePointer = vbHourglass
            Set teserror = oEnlace.EliminarEnlace
            Screen.MousePointer = vbNormal
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oEnlace = Nothing
                Set oEnlaces = Nothing
                Exit Sub
            Else
                sCodGS = frmCompanias.g_oCiaSeleccionada.CodGS
                sCodCIA = frmCompanias.g_oCiaSeleccionada.Cod
                frmCompanias.txtCodProveGS.Text = ""
                frmCompanias.g_oCiaSeleccionada.CodGS = ""
                'Al desenlazar una compa�ia, si est� activo el FSGA se ocultar� la pesta�a de Categor�as laborales.
                If g_udtParametrosGenerales.g_bAccesoFSGA Then
                    frmCompanias.SSTab1.TabVisible(6) = False
                End If
                g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Desenlazar_GS, g_sLitRegAccion_SPA(8) & ": " & sCodCIA & " " & g_sLitRegAccion_SPA(37) & ": " & sCodGS, _
                                                    g_sLitRegAccion_ENG(8) & ": " & sCodCIA & " " & g_sLitRegAccion_ENG(37) & ": " & sCodGS, _
                                                    g_sLitRegAccion_FRA(8) & ": " & sCodCIA & " " & g_sLitRegAccion_FRA(37) & ": " & sCodGS, _
                                                    g_sLitRegAccion_GER(8) & ": " & sCodCIA & " " & g_sLitRegAccion_GER(37) & ": " & sCodGS, _
                                                    frmCompanias.g_oCiaSeleccionada.ID
                Unload Me
                Set oEnlace = Nothing
                Set oEnlaces = Nothing
                Exit Sub
            End If
        
        End If
        Set oEnlace = Nothing
        Set oEnlaces = Nothing
    
End Sub

Private Sub cmdEdicion_Click()
Dim Gmn1 As String
Dim Gmn2 As String
Dim Gmn3 As String
Dim Gmn4 As String
Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim nodx As MSComctlLib.Node

Dim teserror As CTESError
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod As String
Dim i As Integer
Dim vbm As Variant
Dim oEnlace As CEnlace
Dim oEnlaces As CEnlaces
Dim bAsignado As Boolean

If cmdEdicion.Caption = sIdiEdicion Then
    
    cmdDesEnlazar.Enabled = False
    cmdSelecionar.Enabled = False
    cmdEdicion.Caption = sIdiConsulta
    tvwEstrMat.Visible = False
    tvwEstrMatMod.Visible = True
    stabPROVE.TabEnabled(0) = False
    fraProve.Enabled = False
    Screen.MousePointer = vbHourglass
    
    If oGrupsMN1 Is Nothing Then
         Set oGrupsMN1 = g_oRaiz.Generar_CGruposMatNivel1
        'Genera la estructura de materiales entera
         oGrupsMN1.GenerarEstructuraMateriales basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora
    End If
    
    GenerarEstructuraMatAsignable
    
    Screen.MousePointer = vbNormal
    
    Set oGruposMN1DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel4
    
    Set oGruposMN1Seleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3Seleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4Seleccionados = g_oRaiz.Generar_CGruposMatNivel4


        
    Else
        'Tenemos que actualizar los materiales
        

    Set teserror = New CTESError
    teserror.NumError = TESnoerror

    Screen.MousePointer = vbHourglass


    Set nodx = tvwEstrMatMod.Nodes(1)
       
    
    
    Set nod1 = nodx.Child
    
    If Not nod1 Is Nothing Then
        While Not nod1 Is Nothing
            scod1 = DevolverCod(nod1) & Mid$("                         ", 1, 20 - Len(DevolverCod(nod1)))
            ' No asignado
            If Not nod1.Checked Then
                If Not oGruposMN1.Item(scod1) Is Nothing Then
                    ' Antes si
                    oGruposMN1DesSeleccionados.Add DevolverCod(nod1), ""
                End If
                
                Set nod2 = nod1.Child
                
                While Not nod2 Is Nothing
                    scod2 = DevolverCod(nod2) & Mid$("                         ", 1, 20 - Len(DevolverCod(nod2)))
                    ' No asignado
                    If Not nod2.Checked Then
                        If Not oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                            ' Antes si
                            oGruposMN2DesSeleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                        End If
                
                        Set nod3 = nod2.Child
                        While Not nod3 Is Nothing
                            scod3 = DevolverCod(nod3) & Mid$("                         ", 1, 20 - Len(DevolverCod(nod3)))
                            If Not nod3.Checked Then
                                If Not oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                    ' Antes si
                                    oGruposMN3DesSeleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                End If
                                Set nod4 = nod3.Child
                                While Not nod4 Is Nothing
                                    scod4 = DevolverCod(nod4) & Mid$("                         ", 1, 20 - Len(DevolverCod(nod4)))
                                    If Not nod4.Checked Then
                                        If Not oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                        ' Antes si
                                        oGruposMN4DesSeleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                        End If
                                    Else
                                        If oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                            oGruposMN4Seleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                        End If
                                    End If
                                    
                                    Set nod4 = nod4.Next
                                Wend
                            Else
                                ' Si esta checked GMN3
                                If oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                    'No estaba asignado antes.
                                    oGruposMN3Seleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                End If
                            End If
                            
                            Set nod3 = nod3.Next
                        Wend
                    Else
                        ' Si esta checked GMN2
                        If oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                            'No estaba asignado antes.
                            oGruposMN2Seleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                        End If
                    End If
                    
                    Set nod2 = nod2.Next
                Wend
                
            Else
            ' Si esta checked GMN1
               If oGruposMN1.Item(scod1) Is Nothing Then
                    'No estaba asignado antes.
                    oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
                End If
            End If
            Set nod1 = nod1.Next
        Wend
  
    Set oEnlaces = frmCompanias.g_oCiaSeleccionada.DevolverTodosLosEnlaces(True, OrdPorCompCod)

    If oEnlaces Is Nothing Then
        MsgBox sIdiEnlace, vbInformation, "FULLSTEP PS"
        Unload Me
        Exit Sub
    End If

    Set oEnlace = oEnlaces.Item(1)
    Set teserror = oEnlace.ModificarMaterial(oGruposMN1DesSeleccionados, oGruposMN2DesSeleccionados, oGruposMN3DesSeleccionados, oGruposMN4DesSeleccionados, oGruposMN1Seleccionados, oGruposMN2Seleccionados, oGruposMN3Seleccionados, oGruposMN4Seleccionados)
  
    
  If teserror.NumError <> TESnoerror Then

    basErrores.TratarError teserror
    Set oGruposMN1DesSeleccionados = Nothing
    Set oGruposMN2DesSeleccionados = Nothing
    Set oGruposMN3DesSeleccionados = Nothing
    Set oGruposMN4DesSeleccionados = Nothing
    Set oGruposMN1DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel4
    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing
    Set oGruposMN1Seleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3Seleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4Seleccionados = g_oRaiz.Generar_CGruposMatNivel4
    Screen.MousePointer = vbNormal
    Exit Sub
  End If
  
  
  End If
  
  Set oGruposMN1Seleccionados = Nothing
  Set oGruposMN2Seleccionados = Nothing
  Set oGruposMN3Seleccionados = Nothing
  Set oGruposMN4Seleccionados = Nothing
  Set oGruposMN1DesSeleccionados = Nothing
  Set oGruposMN2DesSeleccionados = Nothing
  Set oGruposMN3DesSeleccionados = Nothing
  Set oGruposMN4DesSeleccionados = Nothing
      
          
      tvwEstrMatMod.Visible = False
      tvwEstrMat.Visible = True
    mostrarGruposMaterial
    Screen.MousePointer = vbNormal
    stabPROVE.TabEnabled(0) = True
    fraProve.Enabled = True
        
    If frmCompanias.txtCodProveGS.Text = sdbcProveCod.Text Then
        cmdDesEnlazar.Enabled = True
        cmdSelecionar.Enabled = False
    Else
        If frmCompanias.txtCodProveGS.Text <> "" Then
            cmdSelecionar.Enabled = True
            cmdDesEnlazar.Enabled = False
        End If
    End If
    cmdEdicion.Caption = sIdiEdicion
    
    End If
    
End Sub

Private Sub cmdSelecionar_Click()
Dim oEnlace As CEnlace
Dim oEnlaces As CEnlaces
Dim iRespuesta As Integer
Dim teserror As CTESError

Select Case sOrigen
    
    Case "frmCompanias"
        
        Set oEnlaces = frmCompanias.g_oCiaSeleccionada.DevolverTodosLosEnlaces(True, OrdPorCompCod)

        If oEnlaces Is Nothing Then
            MsgBox sIdiEnlace, vbInformation, "FULLSTEP PS"
            Unload Me
            Exit Sub
        End If

    Set oEnlace = oEnlaces.Item(1)
        If frmCompanias.txtCodProveGS.Text <> "" Then
            'Eliminar enlace
            iRespuesta = MsgBox(sIdiCambiarEnlace & " " & sdbcProveDen.Text, vbQuestion + vbYesNo)
                
                If iRespuesta = vbNo Then
                       frmCompanias.txtCodProveGS.Text = ""
                       Unload Me
                       Set oEnlace = Nothing
                       Set oEnlaces = Nothing
                       Exit Sub
                End If
        End If
        
        '''A�adir enlace
        oEnlace.CodigoProveedorGS = sdbcProveCod.Text
        iRespuesta = MsgBox(sIdiConfirmEnlace & " " & sdbcProveDen.Text & vbCrLf & sIdiContinuar, vbYesNo)
        If iRespuesta = vbNo Then
            Exit Sub
        End If
        
        Set teserror = oEnlace.AnyadirEnlace(frmCompanias.txtCodProveGS.Text, Trim(frmCompanias.txtNif.Text))
        If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oGruposMN1Seleccionados = Nothing
                Set oGruposMN2Seleccionados = Nothing
                Set oGruposMN3Seleccionados = Nothing
                Set oGruposMN4Seleccionados = Nothing
                Set oGruposMN1Seleccionados = g_oRaiz.Generar_CGruposMatNivel1
                Set oGruposMN2Seleccionados = g_oRaiz.Generar_CGruposMatNivel2
                Set oGruposMN3Seleccionados = g_oRaiz.Generar_CGruposMatNivel3
                Set oGruposMN4Seleccionados = g_oRaiz.Generar_CGruposMatNivel4
                Screen.MousePointer = vbNormal
                Exit Sub
        Else
            frmCompanias.txtCodProveGS.Text = sdbcProveCod.Text
            frmCompanias.g_oCiaSeleccionada.CodGS = sdbcProveCod.Text
            g_oGestorAcciones.RegistrarAccion g_sCodADM, TipoAccion.Cia_Enlazar_GS, g_sLitRegAccion_SPA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_SPA(37) & ": " & frmCompanias.g_oCiaSeleccionada.CodGS, _
                                            g_sLitRegAccion_ENG(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_ENG(37) & ": " & frmCompanias.g_oCiaSeleccionada.CodGS, _
                                            g_sLitRegAccion_FRA(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_FRA(37) & ": " & frmCompanias.g_oCiaSeleccionada.CodGS, _
                                            g_sLitRegAccion_GER(8) & ": " & frmCompanias.g_oCiaSeleccionada.Cod & " " & g_sLitRegAccion_GER(37) & ": " & frmCompanias.g_oCiaSeleccionada.CodGS, _
            frmCompanias.g_oCiaSeleccionada.ID
            'Si la c�a no est� enlazada, despu�s de enlazarla si est� activo el FSGA se mostrar� la pesta�a de Categor�as laborales.
            If g_udtParametrosGenerales.g_bAccesoFSGA Then
                frmCompanias.SSTab1.TabVisible(6) = True
            End If
                        
        End If
          
        
    Case "frmEnlazarProveedor"
        If Not frmCompanias.g_frmEnProve Is Nothing Then
            frmCompanias.g_frmEnProve.sdbgProve.Columns("PROVECOD").Value = sdbcProveCod.Text
        Else
            frmEnlazarProveedor.sdbgProve.Columns("PROVECOD").Value = sdbcProveCod.Text
        End If
        

    Case "frmEnlazarComprador"
        If Not frmCompanias.g_frmEnProves Is Nothing Then
            frmCompanias.g_frmEnProves.sdbgProve.Columns("PROVECOD").Value = sdbcProveCod.Text
        Else
            frmEnlazarComprador.sdbgProve.Columns("PROVECOD").Value = sdbcProveCod.Text
        End If
    
End Select
Unload Me
    
End Sub

Private Sub Form_Load()
    Dim oCias As CCias
    CargarRecursos
    cmdEdicion.Enabled = False
    
    If sOrigen = "frmCompanias" Then
        cmdDesEnlazar.Enabled = True
        cmdSelecionar.Caption = sIdiEnlazar
        cmdCancelar.Default = True
        cmdAlta.Enabled = True
        cmdAlta.Enabled = False
    Else
        cmdAlta.Visible = False
        cmdDesEnlazar.Enabled = False
        cmdSelecionar.Caption = sIdiSeleccionar
    End If
    
    'si hay sincronizaci�n de materiales/actividades no se mostrar� el tab de materiales:
    If g_udtParametrosGenerales.gbSincronizacion = True Then
        stabPROVE.TabVisible(1) = False
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    
    oCias.CargarTodasLasCiasDesde 1, IdCia, , , True
    
    Set oCiaSeleccionada = oCias.Item(1)
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
'Libera los objetos
    
    Set oGruposMN1 = Nothing
    Set oGrupsMN1 = Nothing
    Set oGruposMN2 = Nothing
    Set oGruposMN3 = Nothing
    Set oGruposMN4 = Nothing
    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing
    Set oGruposMN1DesSeleccionados = Nothing
    Set oGruposMN2DesSeleccionados = Nothing
    Set oGruposMN3DesSeleccionados = Nothing
    Set oGruposMN4DesSeleccionados = Nothing
  
    
End Sub

Private Sub sdbcGMN1_4Cod_Change()

    If Not GMN1RespetarCombo Then
    
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    GMN1CargarComboDesde = False
    
End Sub
Private Sub sdbcGMN1_4Cod_DropDown()
    Dim oGMN1 As CGrupoMatNivel1

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
        
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = g_oRaiz.Generar_CGruposMatNivel1
    'frmEsperaPortal.Show
       
    DoEvents
    If GMN1CargarComboDesde Then
        oGruposMN1.CargarTodosLosGruposMatDeLaCia oCiaSeleccionada.ID, Trim(sdbcGMN1_4Cod), , False, False
    Else
        oGruposMN1.CargarTodosLosGruposMatDeLaCia oCiaSeleccionada.ID
    End If
    'Unload frmEsperaPortal
   
    For Each oGMN1 In oGruposMN1
   
        sdbcGMN1_4Cod.AddItem oGMN1.Cod & Chr(9) & oGMN1.Den
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not GMN1RespetarCombo Then

        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
        
End Sub

Private Sub sdbcGMN1_4Den_DropDown()
    Dim oGMN1 As CGrupoMatNivel1

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll
        
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = g_oRaiz.Generar_CGruposMatNivel1
    'frmEsperaPortal.Show
    DoEvents
    If GMN1CargarComboDesde Then
        oGruposMN1.CargarTodosLosGruposMatDeLaCia oCiaSeleccionada.ID, , Trim(sdbcGMN1_4Den), False, True
    Else
        oGruposMN1.CargarTodosLosGruposMatDeLaCia oCiaSeleccionada.ID, , , , True
    End If
    'Unload frmEsperaPortal
   
    For Each oGMN1 In oGruposMN1
   
        sdbcGMN1_4Den.AddItem oGMN1.Den & Chr(9) & oGMN1.Cod
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Den.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Den.Rows - 1
            bm = sdbcGMN1_4Den.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Den.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Den.Bookmark = bm
                Exit For
            End If
        Next i
    End If


End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN2_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN2_4Cod.Rows - 1
            bm = sdbcGMN2_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN2_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN2_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        
        GMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()
    Dim oGMN2 As CGrupoMatNivel2

    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sdbcGMN2_4Cod.RemoveAll
        
    'frmEsperaPortal.Show
    DoEvents
    If GMN2CargarComboDesde Then
        oGMN1Seleccionado.CargarTodosLosGruposMat2DeLaCia oCiaSeleccionada.ID, Trim(sdbcGMN2_4Cod), , False, False
    Else
        oGMN1Seleccionado.CargarTodosLosGruposMat2DeLaCia oCiaSeleccionada.ID
    End If
    'Unload frmEsperaPortal
   
    For Each oGMN2 In oGMN1Seleccionado.GruposMatNivel2
   
        sdbcGMN2_4Cod.AddItem oGMN2.Cod & Chr(9) & oGMN2.Den
   
    Next

    If GMN2CargarComboDesde And Not oGMN1Seleccionado.GruposMatNivel2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4cod_InitColumnProps()
    
    sdbcGMN2_4Cod.DataFieldList = "Column 0"
    sdbcGMN2_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not GMN2RespetarCombo Then

        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()
Dim oGMN2 As CGrupoMatNivel2

    Screen.MousePointer = vbHourglass
    
    sdbcGMN2_4Den.RemoveAll
        
    frmEsperaPortal.Show
    DoEvents
    If GMN2CargarComboDesde Then
        oGMN1Seleccionado.CargarTodosLosGruposMat2DeLaCia oCiaSeleccionada.ID, , Trim(sdbcGMN2_4Den), False, True
    Else
        oGMN1Seleccionado.CargarTodosLosGruposMat2DeLaCia oCiaSeleccionada.ID, , , , True
    End If
    Unload frmEsperaPortal
   
    For Each oGMN2 In oGMN1Seleccionado.GruposMatNivel2
   
        sdbcGMN2_4Den.AddItem oGMN2.Den & Chr(9) & oGMN2.Cod
   
    Next

    If GMN2CargarComboDesde And Not oGMN1Seleccionado.GruposMatNivel2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN2_4Den.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN2_4Den.Rows - 1
            bm = sdbcGMN2_4Den.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN2_4Den.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN2_4Den.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN3_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN3_4Cod.Rows - 1
            bm = sdbcGMN3_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN3_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN3_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcGMN3_4Cod_Change()
    
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN3_4cod_InitColumnProps()
    sdbcGMN3_4Cod.DataFieldList = "Column 0"
    sdbcGMN3_4Cod.DataFieldToDisplay = "Column 0"
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()
Dim oGMN3 As CGrupoMatNivel3
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    Screen.MousePointer = vbHourglass
    
    sdbcGMN3_4Cod.RemoveAll
        
    'frmEsperaPortal.Show
    DoEvents
    If GMN3CargarComboDesde Then
        oGMN2Seleccionado.CargarTodosLosGruposMat3DeLaCia oCiaSeleccionada.ID, Trim(sdbcGMN3_4Cod), , False, False
    Else
        oGMN2Seleccionado.CargarTodosLosGruposMat3DeLaCia oCiaSeleccionada.ID
    End If
    'Unload frmEsperaPortal
   
    For Each oGMN3 In oGMN2Seleccionado.GruposMatNivel3
   
        sdbcGMN3_4Cod.AddItem oGMN3.Cod & Chr(9) & oGMN3.Den
   
    Next

    If GMN3CargarComboDesde And Not oGMN2Seleccionado.GruposMatNivel3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not GMN3RespetarCombo Then

        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()
Dim oGMN3 As CGrupoMatNivel3

    Screen.MousePointer = vbHourglass
    
    sdbcGMN3_4Den.RemoveAll
        
    frmEsperaPortal.Show
    DoEvents
    If GMN3CargarComboDesde Then
        oGMN2Seleccionado.CargarTodosLosGruposMat3DeLaCia oCiaSeleccionada.ID, , Trim(sdbcGMN3_4Den), False, True
    Else
        oGMN2Seleccionado.CargarTodosLosGruposMat3DeLaCia oCiaSeleccionada.ID, , , , True
    End If
    Unload frmEsperaPortal
   
    For Each oGMN3 In oGMN2Seleccionado.GruposMatNivel3
   
        sdbcGMN3_4Den.AddItem oGMN3.Den & Chr(9) & oGMN3.Cod
   
    Next

    If GMN3CargarComboDesde And Not oGMN2Seleccionado.GruposMatNivel3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN3_4Den.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN3_4Den.Rows - 1
            bm = sdbcGMN3_4Den.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN3_4Den.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN3_4Den.Bookmark = bm
                Exit For
            End If
        Next i
    End If


End Sub


Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN4_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN4_4Cod.Rows - 1
            bm = sdbcGMN4_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN4_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN4_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN4_4Cod_InitColumnProps()
    
    sdbcGMN4_4Cod.DataFieldList = "Column 0"
    sdbcGMN4_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Cod_Change()
    
    If Not GMN4RespetarCombo Then
    
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()
Dim oGMN4 As CGrupoMatNivel4
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    sdbcGMN4_4Cod.RemoveAll
        
    'frmEsperaPortal.Show
    DoEvents
    If GMN4CargarComboDesde Then
        oGMN3Seleccionado.CargarTodosLosGruposMat4DeLaCia oCiaSeleccionada.ID, Trim(sdbcGMN4_4Cod), , False, False
    Else
        oGMN3Seleccionado.CargarTodosLosGruposMat4DeLaCia oCiaSeleccionada.ID
    End If
    'Unload frmEsperaPortal
   
    For Each oGMN4 In oGMN3Seleccionado.GruposMatNivel4
   
        sdbcGMN4_4Cod.AddItem oGMN4.Cod & Chr(9) & oGMN4.Den
   
    Next

    If GMN4CargarComboDesde And Not oGMN3Seleccionado.GruposMatNivel4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN4_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcGMN4_4Den_Change()
    
    If Not GMN4RespetarCombo Then

        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub


Private Sub sdbcGMN4_4Den_DropDown()
Dim oGMN4 As CGrupoMatNivel4

    Screen.MousePointer = vbHourglass
    
    sdbcGMN4_4Den.RemoveAll
        
    frmEsperaPortal.Show
    DoEvents
    If GMN4CargarComboDesde Then
        oGMN3Seleccionado.CargarTodosLosGruposMat4DeLaCia oCiaSeleccionada.ID, , Trim(sdbcGMN4_4Den), False, True
    Else
        oGMN3Seleccionado.CargarTodosLosGruposMat4DeLaCia oCiaSeleccionada.ID, , , , True
    End If
    Unload frmEsperaPortal
   
    For Each oGMN4 In oGMN3Seleccionado.GruposMatNivel4
   
        sdbcGMN4_4Den.AddItem oGMN4.Den & Chr(9) & oGMN4.Cod
   
    Next

    If GMN4CargarComboDesde And Not oGMN3Seleccionado.GruposMatNivel4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN4_4Den.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN4_4Den.Rows - 1
            bm = sdbcGMN4_4Den.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN4_4Den.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN4_4Den.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcPaiCod_Change()
    
    If Not PaiRespetarCombo Then
    
        PaiRespetarCombo = True
        sdbcPaiDen.Text = ""
        PaiRespetarCombo = False
        
        PaiCargarComboDesde = True
        
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
    
End Sub

Private Sub sdbcPaiCod_Click()
If Not sdbcPaiCod.DroppedDown Then
    sdbcPaiCod.Text = ""
    sdbcPaiDen.Text = ""
End If
End Sub

Private Sub sdbcPaiCod_CloseUp()
    
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    PaiRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    PaiRespetarCombo = False
    
    PaiCargarComboDesde = False
        
End Sub

Private Sub sdbcPaiCod_DropDown()

    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcPaiCod.RemoveAll
    
    If PaiCargarComboDesde Then
        Set Ador = oCiaSeleccionada.DevolverPaisesDeLaCiaDesde(basParametros.g_iCargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False)
    Else
        Set Ador = oCiaSeleccionada.DevolverPaisesDeLaCiaDesde(basParametros.g_iCargaMaximaCombos)
    End If
    
    i = 0
    While Not Ador.EOF And i <= basParametros.g_iCargaMaximaCombos
        sdbcPaiCod.AddItem Ador("COD").Value & Chr(9) & Ador("DEN").Value
        i = i + 1
        Ador.MoveNext
    Wend
    
    If i > basParametros.g_iCargaMaximaCombos And Not Ador.EOF Then
        sdbcPaiCod.AddItem "..."
    End If

    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiCod_InitColumnProps()

    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcPaiCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcPaiDen_Change()
    
    If Not PaiRespetarCombo Then
    
        PaiRespetarCombo = True
        sdbcPaiCod.Text = ""
        PaiRespetarCombo = False
        PaiCargarComboDesde = True
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
        
End Sub
Private Sub sdbcPaiDen_CloseUp()
    
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    PaiRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    PaiRespetarCombo = False
    PaiCargarComboDesde = False
        
End Sub

Private Sub sdbcPaiDen_DropDown()

    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcPaiDen.RemoveAll
    
    If PaiCargarComboDesde Then
        Set Ador = oCiaSeleccionada.DevolverPaisesDeLaCiaDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcPaiDen.Text), , True)
    Else
        Set Ador = oCiaSeleccionada.DevolverPaisesDeLaCiaDesde(basParametros.g_iCargaMaximaCombos, , , , True)
    End If
    
    i = 0
    While Not Ador.EOF And i <= basParametros.g_iCargaMaximaCombos
        sdbcPaiDen.AddItem Ador("DEN").Value & Chr(9) & Ador("COD").Value
        i = i + 1
        Ador.MoveNext
    Wend
    
    If i > basParametros.g_iCargaMaximaCombos And Not Ador.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiDen.Refresh
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub


Private Sub sdbcPaiDen_PositionList(ByVal Text As String)

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiDen.Rows - 1
            bm = sdbcPaiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Sub sdbcProveCod_Change()
    If Not ProveRespetarCombo Then
        ProveRespetarCombo = True
        stabPROVE.Tab = 0
        sdbcProveDen.Text = ""
        limpiarDatosProveedor
        Set oProveSeleccionado = Nothing
        ProveRespetarCombo = False
    End If
End Sub

Private Sub sdbcProveCod_Click()
If Not sdbcProveCod.DroppedDown Then
    sdbcProveCod.Text = ""
    sdbcProveDen.Text = ""
End If
End Sub

Private Sub sdbcProveCod_CloseUp()
    ProveRespetarCombo = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    limpiarDatosProveedor
    ProveRespetarCombo = False
    ProveSeleccionado
End Sub

Private Sub sdbcProveCod_DropDown()
Dim Ador As Ador.Recordset
Dim oProveedores As CProveedores
Dim i As Integer

Set oProveedores = g_oRaiz.Generar_CProveedores
limpiarDatosProveedor
stabPROVE.Tab = 0
DoEvents
sdbcProveCod.RemoveAll

Set Ador = oProveedores.devolverTodosLosProveedores(g_iCargaMaximaCombos, IdCia, txtCod, txtDen, sdbcPaiCod, sdbcProviCod, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, sdbcGMN4_4Cod)
i = 0
If Ador Is Nothing Then
    Exit Sub
End If
While Not Ador.EOF
    i = i + 1
    sdbcProveCod.AddItem Ador("cod").Value & Chr(9) & Ador("den").Value
    Ador.MoveNext
Wend

Ador.Close
Set Ador = Nothing
    sdbcProveCod.Refresh

End Sub



Private Sub sdbcProveCod_InitColumnProps()
    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"

End Sub

Public Sub sdbcProveCod_Validate(Cancel As Boolean)
Dim Ador As Ador.Recordset
Dim oProveedores As CProveedores
    
    
    Screen.MousePointer = vbHourglass
    
    Me.sdbcProveCod.Text = basUtilidades.EliminarEspacioYTab(Me.sdbcProveCod.Text)
    
    ProveSeleccionado
    Screen.MousePointer = vbNormal
    
    
End Sub

Private Sub sdbcProveDen_Change()
    If Not ProveRespetarCombo Then
        ProveRespetarCombo = True
        stabPROVE.Tab = 0
        sdbcProveCod.Text = ""
        limpiarDatosProveedor
        Set oProveSeleccionado = Nothing
        ProveRespetarCombo = False
    End If

End Sub

Private Sub sdbcProveDen_Click()
If Not sdbcProveDen.DroppedDown Then
    sdbcProveCod.Text = ""
    sdbcProveDen.Text = ""
End If

End Sub

Private Sub sdbcProveDen_CloseUp()
    ProveRespetarCombo = True
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    ProveRespetarCombo = False
    limpiarDatosProveedor
    ProveSeleccionado

End Sub

Private Sub sdbcProveDen_DropDown()
Dim Ador As Ador.Recordset
Dim oProveedores As CProveedores
Dim i As Integer

Set oProveedores = g_oRaiz.Generar_CProveedores
limpiarDatosProveedor
stabPROVE.Tab = 0
DoEvents
sdbcProveCod.RemoveAll

Set Ador = oProveedores.devolverTodosLosProveedores(g_iCargaMaximaCombos, IdCia, txtCod, txtDen, sdbcPaiCod, sdbcProviCod, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, sdbcGMN4_4Cod, True)
i = 0
If Ador Is Nothing Then
    Exit Sub
End If
While Not Ador.EOF
    i = i + 1
    sdbcProveDen.AddItem Ador("den").Value & Chr(9) & Ador("cod").Value
    Ador.MoveNext
Wend

Ador.Close
Set Ador = Nothing
    sdbcProveCod.Refresh


End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbcProviCod_Change()
    
    If Not ProviRespetarCombo Then
    
        ProviRespetarCombo = True
        sdbcProviDen.Text = ""
        ProviRespetarCombo = False
        
        ProviCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcProviCod.Value = "..." Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    
    ProviRespetarCombo = True
    sdbcProviDen.Text = sdbcProviCod.Columns(1).Text
    sdbcProviCod.Text = sdbcProviCod.Columns(0).Text
    ProviRespetarCombo = False
    
    ProviCargarComboDesde = False
        
End Sub


Private Sub sdbcProviCod_DropDown()
Dim Ador As Ador.Recordset
Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    sdbcProviCod.RemoveAll
        
    If sdbcPaiCod <> "" Then
            Set Ador = oCiaSeleccionada.DevolverProvinciasEnCia(, Trim(sdbcPaiCod.Text), False)
        
        While Not Ador.EOF And i <= basParametros.g_iCargaMaximaCombos
            sdbcProviCod.AddItem Ador("COD").Value & Chr(9) & Ador("DEN").Value
            Ador.MoveNext
            i = i + 1
        Wend
        
        If Not Ador.EOF And i > basParametros.g_iCargaMaximaCombos Then
            sdbcProviCod.AddItem "..."
        End If

    End If
    
    sdbcProviCod.SelStart = 0
    sdbcProviCod.SelLength = Len(sdbcProviCod.Text)
    sdbcProviCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProviCod_InitColumnProps()

    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProviCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProviCod.Rows - 1
            bm = sdbcProviCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProviCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcProviDen_Change()
    
    If Not ProviRespetarCombo Then
    
        ProviRespetarCombo = True
        sdbcProviCod.Text = ""
        ProviRespetarCombo = False
        ProviCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcProviDen_CloseUp()
    
    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    
    ProviRespetarCombo = True
    sdbcProviCod.Text = sdbcProviDen.Columns(0).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(1).Text
    ProviRespetarCombo = False
    
    ProviCargarComboDesde = False
        
End Sub
Private Sub sdbcProviDen_DropDown()
Dim Ador As Ador.Recordset
Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcProviDen.RemoveAll
        
    If sdbcPaiCod <> "" Then
    
        Set Ador = oCiaSeleccionada.DevolverProvinciasEnCia(, Trim(sdbcPaiCod), True)
        
        While Not Ador.EOF And i <= basParametros.g_iCargaMaximaCombos
            sdbcProviDen.AddItem Ador("DEN").Value & Chr(9) & Ador("COD").Value
            Ador.MoveNext
            i = i + 1
        Wend
        
        If Not Ador.EOF And i > basParametros.g_iCargaMaximaCombos Then
            sdbcProviDen.AddItem "..."
        End If

    End If
    
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
    sdbcProviDen.Refresh
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcProviDen_InitColumnProps()

    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcProviDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProviCod.DroppedDown = False
        sdbcProviCod.Text = ""
        sdbcProviCod.RemoveAll
        sdbcProviDen.DroppedDown = False
        sdbcProviDen.Text = ""
        sdbcProviDen.RemoveAll
    End If
End Sub


Private Sub sdbcProviDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProviDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProviDen.Rows - 1
            bm = sdbcProviDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProviDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub



Private Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = g_oRaiz.Generar_CGrupoMatNivel1
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    sdbcGMN2_4Cod = ""
    sdbcGMN2_4Den = ""
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = g_oRaiz.Generar_CGrupoMatNivel2
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod.Text
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
    sdbcGMN3_4Cod = ""
    
   
End Sub
Private Sub GMN3Seleccionado()

    Set oGMN3Seleccionado = Nothing
    
    If sdbcGMN3_4Cod.Text = "" Or sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN3Seleccionado = g_oRaiz.Generar_CGrupoMatNivel3
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
     sdbcGMN4_4Cod = ""
End Sub

Private Sub GMN4Seleccionado()
    
    Set oGMN4Seleccionado = Nothing
    
    If sdbcGMN4_4Cod.Text = "" Or sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN4Seleccionado = g_oRaiz.Generar_CGrupoMatNivel4
    
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN3_4Cod
    
    
    
End Sub




Private Sub ProveSeleccionado()
Dim oProves As CProveedores

Set oProves = g_oRaiz.Generar_CProveedores


    If Not sdbcProveCod.Text = "" Then
        oProves.cargarTodosProveedores 1, IdCia, sdbcProveCod.Text
        
        If oProves.Count = 0 Then
            limpiarDatosProveedor
            If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
                MsgBox sIdiNoExiste, vbInformation
                cmdAlta.Enabled = True
            Else
                basMensajes.NoValido sIdiProve
            End If
            Exit Sub
        End If
        
        Set oProveSeleccionado = Nothing
        Set oProveSeleccionado = oProves.Item(Trim(CStr(sdbcProveCod.Text)))
        With oProveSeleccionado
            ProveRespetarCombo = True
            sdbcProveDen.Text = oProveSeleccionado.Den
            ProveRespetarCombo = False
            
            
            lblNIF.Caption = IIf(IsNull(.NIF), "", .NIF)
            lblDir.Caption = IIf(IsNull(.Direccion), "", .Direccion)
            lblPob.Caption = IIf(IsNull(.Poblacion), "", .Poblacion)
            lblCP.Caption = IIf(IsNull(.CP), "", .CP)
            lblPaiCod.Caption = IIf(IsNull(.CodPais), "", .CodPais)
            lblPaiDen.Caption = IIf(IsNull(.DenPais), "", .DenPais)
            lblProviCod.Caption = IIf(IsNull(.codprovi), "", .codprovi)
            lblProviDen.Caption = IIf(IsNull(.denprovi), "", .denprovi)
            lblMonCod.Caption = IIf(IsNull(.codmon), "", .codmon)
            lblMonDen.Caption = IIf(IsNull(.denmon), "", .denmon)
        End With
        
        If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
            If sdbcProveCod.Text = frmCompanias.txtCodProveGS Then
                cmdDesEnlazar.Enabled = True
                cmdSelecionar.Enabled = False
                cmdEdicion.Enabled = True
            Else
                cmdDesEnlazar.Enabled = False
                cmdSelecionar.Enabled = True
                cmdEdicion.Enabled = False
            End If
            
            cmdAlta.Enabled = False
        Else
            cmdSelecionar.Enabled = True
        End If
        
    End If

    
End Sub

Private Sub stabGeneral_Click(PreviousTab As Integer)
stabPROVE.Tab = 0
If stabGeneral.Tab = 0 Then
    ProveRespetarCombo = True
    sdbcProveCod.Text = ""
    sdbcProveDen.Text = ""
    ProveRespetarCombo = False
    limpiarDatosProveedor
    Set oProveSeleccionado = Nothing
End If

End Sub

Private Sub stabPROVE_Click(PreviousTab As Integer)

If stabPROVE.Tab = 1 Then
    If oProveSeleccionado Is Nothing Then
        stabPROVE.Tab = 0
        If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
            cmdEdicion.Visible = False
        End If
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
        
    mostrarGruposMaterial
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
        If frmCompanias.txtCodProveGS = sdbcProveCod.Text Then
            cmdEdicion.Visible = True
        End If
    End If
    Screen.MousePointer = vbNormal
Else
    cmdEdicion.Visible = False
End If

End Sub

Private Sub mostrarGruposMaterial()

Dim oGMN1 As CGrupoMatNivel1
Dim nodx As MSComctlLib.Node
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String


If oProveSeleccionado Is Nothing Then Exit Sub

tvwEstrMat.Nodes.Clear
Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdiMats, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True

On Error GoTo error

With oProveSeleccionado


'************** GruposMN1 ***********
Set oGruposMN1 = .DevolverGruposMN1Asignados(False)
    
        For Each oGMN1 In oGruposMN1
    
            scod1 = oGMN1.Cod & Mid$("                         ", 1, 20 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1A")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod) ' Con la X1X indicamos que el GMN1 ha sido asignado.
            
        Next
        
      
    
    '************** GruposMN2 ***********
    Set oGruposMN2 = .DevolverGruposMN2Asignados(False)
    
        For Each oGMN2 In oGruposMN2
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, 20 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN2.GMN1Cod) & " - " & oGMN2.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN2.GMN1Cod)
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2A")
            nodx.Tag = "GMN2" & CStr(oGMN2.Cod) ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
            If nodx.Parent.image = "GMN1" Then
                nodx.Parent.Expanded = True
            End If
        Next
    
    
    '************** GruposMN3 ***********
    Set oGruposMN3 = .DevolverGruposMN3Asignados(False)
    
    If Not oGruposMN3 Is Nothing Then
        For Each oGMN3 In oGruposMN3
            scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN3.GMN1Cod))
            scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, 20 - Len(oGMN3.GMN2Cod))
            scod3 = oGMN3.Cod & Mid$("                         ", 1, 20 - Len(oGMN3.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN3.GMN1Cod) & " - " & oGMN3.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN3.GMN1Cod)  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod) & " - " & oGMN3.GMN2Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN3.GMN2Cod)   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3A")
            nodx.Tag = "GMN3" & CStr(oGMN3.Cod) ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
            If nodx.Parent.image = "GMN2" Then
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        Next
    
    End If
    
    
    '************** GruposMN4 ***********
    Set oGruposMN4 = .DevolverGruposMN4Asignados(False)
    
    If Not oGruposMN4 Is Nothing Then
        
        For Each oGMN4 In oGruposMN4

            scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN1Cod))
            scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN2Cod))
            scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN3Cod))
            scod4 = oGMN4.Cod & Mid$("                         ", 1, 20 - Len(oGMN4.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN4.GMN1Cod) & " - " & oGMN4.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN4.GMN1Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod) & " - " & oGMN4.GMN2Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN4.GMN2Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod) & " - " & oGMN4.GMN3Den, "GMN3")
            nodx.Tag = "GMN3" & CStr(oGMN4.GMN3Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4A")
            nodx.Tag = "GMN4" & CStr(oGMN4.Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            
            If (nodx.Parent.image) = "GMN3" Then
                nodx.Parent.Parent.Parent.Expanded = True
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        
        Next
    
    End If
 
 End With
 
Exit Sub

error:
    Set nodx = Nothing
    Resume Next
End Sub


Private Sub limpiarDatosProveedor()
lblNIF.Caption = ""
lblNIF.Caption = ""
lblDir.Caption = ""
lblPob.Caption = ""
lblCP.Caption = ""
lblPaiCod.Caption = ""
lblPaiDen.Caption = ""
lblProviCod.Caption = ""
lblProviDen.Caption = ""
lblMonCod.Caption = ""
lblMonDen.Caption = ""
cmdSelecionar.Enabled = False
If basParametros.g_udtParametrosGenerales.g_bUnaCompradora Then
    
    cmdDesEnlazar.Enabled = False
    cmdEdicion.Enabled = False
End If

End Sub

Private Sub EdicionMateriales()

    'Vamos a modificar la asignaci�n de material
    tvwEstrMat.Visible = False
    tvwEstrMatMod.Visible = True
    
    Screen.MousePointer = vbHourglass
    If oGrupsMN1 Is Nothing Then
         Set oGrupsMN1 = g_oRaiz.Generar_CGruposMatNivel1
        'Genera la estructura de materiales entera
         DoEvents
         oGrupsMN1.GenerarEstructuraMateriales basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora
    End If
    GenerarEstructuraMatAsignable
    Screen.MousePointer = vbNormal
    
    Set oGruposMN1DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel4
    
    Set oGruposMN1Seleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3Seleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4Seleccionados = g_oRaiz.Generar_CGruposMatNivel4


End Sub

Private Sub tvwEstrMatMod_KeyUp(KeyCode As Integer, Shift As Integer)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

If bMouseDown Then
    
    bMouseDown = False
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = Not SelNode.Checked
        DoEvents
        Exit Sub
    End If
    
    If SelNode.Checked Then
            
            MarcarTodosLosHijos SelNode
            
    Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
        
    End If

End If

End Sub

Private Sub tvwEstrMatMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.Node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMatMod.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMatMod.hWnd, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.Flags And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                bMouseDown = True
                Set SelNode = nod
                             
                End If
            End If
        End If
    End If
    

End Sub

Private Sub tvwEstrMatMod_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

If bMouseDown Then
    
    bMouseDown = False
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = Not SelNode.Checked
        DoEvents
        Exit Sub
    End If
    
    If SelNode.Checked Then
            
            MarcarTodosLosHijos SelNode
            
    Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
        
    End If

End If

End Sub

Private Sub tvwEstrMatMod_NodeCheck(ByVal Node As MSComctlLib.Node)
If bMouseDown Then
    Exit Sub
End If

bMouseDown = True
Set SelNode = Node

End Sub
Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = True
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = True
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = True
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub
Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = False
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = False
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = False
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = False
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = False
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub

Private Sub EliminarSusHijosDeDesAsignados(ByVal nodx As MSComctlLib.Node)

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4


Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                               
           If Not oGruposMN2DesSeleccionados Is Nothing Then
                For Each oGMN2 In oGruposMN2DesSeleccionados
                    If oGMN2.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN2DesSeleccionados.Remove CStr(oGMN2.GMN1Cod) & CStr(oGMN2.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN3DesSeleccionados Is Nothing Then
                For Each oGMN3 In oGruposMN3DesSeleccionados
                    If oGMN3.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN3DesSeleccionados.Remove CStr(oGMN3.GMN1Cod) & CStr(oGMN3.GMN2Cod) & CStr(oGMN3.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN4DesSeleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4DesSeleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4DesSeleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
            
    Case "GMN2"
        
        If Not oGruposMN3DesSeleccionados Is Nothing Then
                For Each oGMN3 In oGruposMN3DesSeleccionados
                    If oGMN3.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN3DesSeleccionados.Remove CStr(oGMN3.GMN1Cod) & CStr(oGMN3.GMN2Cod) & CStr(oGMN3.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN4DesSeleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4DesSeleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4DesSeleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
    
    Case "GMN3"
            
            If Not oGruposMN4DesSeleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4DesSeleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4DesSeleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
            
    Case "GMN4"
             
End Select


End Sub

Private Sub EliminarSusHijosDeAsignados(ByVal nodx As MSComctlLib.Node)

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4


Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                               
           If Not oGruposMN2Seleccionados Is Nothing Then
                For Each oGMN2 In oGruposMN2Seleccionados
                    If oGMN2.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN2Seleccionados.Remove CStr(oGMN2.GMN1Cod) & CStr(oGMN2.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN3Seleccionados Is Nothing Then
                For Each oGMN3 In oGruposMN3Seleccionados
                    If oGMN3.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN3Seleccionados.Remove CStr(oGMN3.GMN1Cod) & CStr(oGMN3.GMN2Cod) & CStr(oGMN3.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN4Seleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4Seleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4Seleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
            
    Case "GMN2"
        
        If Not oGruposMN3Seleccionados Is Nothing Then
                For Each oGMN3 In oGruposMN3Seleccionados
                    If oGMN3.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN3Seleccionados.Remove CStr(oGMN3.GMN1Cod) & CStr(oGMN3.GMN2Cod) & CStr(oGMN3.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN4Seleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4Seleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4Seleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
    
    Case "GMN3"
            
            If Not oGruposMN4Seleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4Seleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4Seleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
            
    Case "GMN4"
             
End Select


End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.Node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
    DoEvents
End Sub

Private Sub EliminarSusPadresDeAsignados(ByVal nodx As MSComctlLib.Node)

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4


Select Case Left(nodx.Tag, 4)

    Case "GMN4"
                               
            If Not oGruposMN1Seleccionados Is Nothing Then
                 If Not oGruposMN1Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent.Parent))) Is Nothing Then
                    oGruposMN1Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent.Parent))
                 End If
            End If
            
            If Not oGruposMN2Seleccionados Is Nothing Then
                    If Not (oGruposMN2Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent.Parent)) & CStr(DevolverCod(nodx.Parent.Parent))) Is Nothing) Then
                        oGruposMN2Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent.Parent)) & CStr(DevolverCod(nodx.Parent.Parent))
                    End If
            End If
            
            If Not oGruposMN3Seleccionados Is Nothing Then
                    If Not (oGruposMN3Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent.Parent)) & CStr(DevolverCod(nodx.Parent.Parent)) & CStr(DevolverCod(nodx.Parent))) Is Nothing) Then
                        oGruposMN3Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent.Parent)) & CStr(DevolverCod(nodx.Parent.Parent)) & CStr(DevolverCod(nodx.Parent))
                    End If
                
            End If
            
    Case "GMN3"
        
            If Not oGruposMN1Seleccionados Is Nothing Then
                 If Not oGruposMN1Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent))) Is Nothing Then
                    oGruposMN1Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent))
                 End If
            End If
            
            If Not oGruposMN2Seleccionados Is Nothing Then
                    If Not (oGruposMN2Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent)) & CStr(DevolverCod(nodx.Parent))) Is Nothing) Then
                        oGruposMN2Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent)) & CStr(DevolverCod(nodx.Parent))
                    End If
            End If
            
    
    Case "GMN2"
            
             If Not oGruposMN1Seleccionados Is Nothing Then
                 If Not oGruposMN1Seleccionados.Item(CStr(DevolverCod(nodx.Parent))) Is Nothing Then
                    oGruposMN1Seleccionados.Remove CStr(DevolverCod(nodx.Parent))
                 End If
            End If
            
            
    Case "GMN1"
             
End Select
    
End Sub

Public Function DevolverCod(ByVal Node As MSComctlLib.Node) As Variant

Select Case Left(Node.Tag, 4)

Case "GMN1"
        
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "GMN2"
        
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "GMN3"
        
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
   
Case "GMN4"
        
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
   
End Select

End Function

Private Sub GenerarEstructuraMatAsignable()

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim nodx As MSComctlLib.Node
Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

On Error GoTo error

tvwEstrMatMod.Nodes.Clear

Set nodx = tvwEstrMatMod.Nodes.Add(, , "Raiz", sIdiMat, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True

For Each oGMN1 In oGrupsMN1
    scod1 = oGMN1.Cod & Mid$("                         ", 1, 20 - Len(oGMN1.Cod))
    Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
    nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
    
    For Each oGMN2 In oGMN1.GruposMatNivel2
        scod2 = oGMN2.Cod & Mid$("                         ", 1, 20 - Len(oGMN2.Cod))
        Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
        nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
            
            For Each oGMN3 In oGMN2.GruposMatNivel3
                
                scod3 = oGMN3.Cod & Mid$("                         ", 1, 20 - Len(oGMN3.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
                nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                        
                        For Each oGMN4 In oGMN3.GruposMatNivel4
                            scod4 = oGMN4.Cod & Mid$("                         ", 1, 20 - Len(oGMN4.Cod))
                            Set nodx = tvwEstrMatMod.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4")
                            nodx.Tag = "GMN4" & CStr(oGMN4.Cod)
                        Next
            Next
    Next
Next

 ' Ahora de esta estructura debemos marcar los materiales que ya estan seleccionados

For Each nodx In tvwEstrMat.Nodes
    If Right(nodx.image, 1) = "A" Then
        tvwEstrMatMod.Nodes(nodx.Key).Checked = True
        If nodx.Visible Then
            tvwEstrMatMod.Nodes(nodx.Key).EnsureVisible
        End If
        DoEvents
    End If
Next
 
Exit Sub

error:
    Set nodx = Nothing
    Resume Next
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVEBUSCAR, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAlta.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdDesEnlazar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdEdicion.Caption = Ador(0).Value
        sIdiEdicion = Ador(0).Value
        Ador.MoveNext
        Me.cmdSelecionar.Caption = Ador(0).Value
        sIdiSeleccionar = Ador(0).Value
        Ador.MoveNext
        Me.Frame1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Frame2.Caption = Ador(0).Value
        Me.stabPROVE.TabCaption(0) = Ador(0).Value
         Ador.MoveNext
        
        Me.Label1.Caption = Ador(0).Value & ":"
        Me.Label15.Caption = Ador(0).Value & ":"
        Me.sdbcGMN1_4Cod.Columns(0).Caption = Ador(0).Value
        Me.sdbcGMN1_4Den.Columns(1).Caption = Ador(0).Value
        Me.sdbcGMN2_4Cod.Columns(0).Caption = Ador(0).Value
        Me.sdbcGMN2_4Den.Columns(1).Caption = Ador(0).Value
        Me.sdbcGMN3_4Cod.Columns(0).Caption = Ador(0).Value
        Me.sdbcGMN3_4Den.Columns(1).Caption = Ador(0).Value
        Me.sdbcGMN4_4Cod.Columns(0).Caption = Ador(0).Value
        Me.sdbcGMN4_4Den.Columns(1).Caption = Ador(0).Value
        Me.sdbcPaiCod.Columns(0).Caption = Ador(0).Value
        Me.sdbcPaiDen.Columns(1).Caption = Ador(0).Value
        Me.sdbcProveCod.Columns(0).Caption = Ador(0).Value
        Me.sdbcProveDen.Columns(1).Caption = Ador(0).Value
        Me.sdbcProviCod.Columns(0).Caption = Ador(0).Value
        Me.sdbcProviDen.Columns(1).Caption = Ador(0).Value
        
        Ador.MoveNext
        Me.Label14.Caption = Ador(0).Value
        Me.Label6.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label2.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label21.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label25.Caption = Ador(0).Value
        Ador.MoveNext
        
        Me.Label3.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label4.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label5.Caption = Ador(0).Value
        Me.Label8.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label7.Caption = Ador(0).Value
        Ador.MoveNext
        
        Me.sdbcGMN1_4Cod.Columns(1).Caption = Ador(0).Value
        Me.sdbcGMN1_4Den.Columns(0).Caption = Ador(0).Value
        Me.sdbcGMN2_4Cod.Columns(1).Caption = Ador(0).Value
        Me.sdbcGMN2_4Den.Columns(0).Caption = Ador(0).Value
        Me.sdbcGMN3_4Cod.Columns(1).Caption = Ador(0).Value
        Me.sdbcGMN3_4Den.Columns(0).Caption = Ador(0).Value
        Me.sdbcGMN4_4Cod.Columns(1).Caption = Ador(0).Value
        Me.sdbcGMN4_4Den.Columns(0).Caption = Ador(0).Value
        Me.sdbcPaiCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcPaiDen.Columns(0).Caption = Ador(0).Value
        Me.sdbcProveCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcProveDen.Columns(0).Caption = Ador(0).Value
        Me.sdbcProviCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcProviDen.Columns(0).Caption = Ador(0).Value
        Ador.MoveNext
        Me.stabGeneral.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        Me.stabGeneral.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        Me.stabPROVE.TabCaption(1) = Ador(0).Value
        sIdiMat = Ador(0).Value
        Ador.MoveNext
        sIdiAltaProve = Ador(0).Value
        Ador.MoveNext
        sIdiContinuar = Ador(0).Value
        Ador.MoveNext
        sIdiEnlace = Ador(0).Value
        Ador.MoveNext
        sIdiConsulta = Ador(0).Value
        Ador.MoveNext
        sIdiCambiarEnlace = Ador(0).Value
        Ador.MoveNext
        sIdiConfirmEnlace = Ador(0).Value
        Ador.MoveNext
        sIdiEnlazar = Ador(0).Value
        Ador.MoveNext
        sIdiNoExiste = Ador(0).Value
        Ador.MoveNext
        sIdiProve = Ador(0).Value
        Ador.MoveNext
        sIdiMats = Ador(0).Value
        
        Ador.MoveNext
        lblGMN2_4.Caption = Ador(0).Value
        Ador.MoveNext
        lblGMN3_4.Caption = Ador(0).Value
        Ador.MoveNext
        lblGMN4_4.Caption = Ador(0).Value
        Ador.MoveNext
        lblGMN1_4.Caption = Ador(0).Value
        Ador.MoveNext
        msIdiProve = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub



