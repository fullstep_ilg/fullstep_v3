VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCiaBuscar 
   Caption         =   "Buscar Compa�ia"
   ClientHeight    =   7260
   ClientLeft      =   0
   ClientTop       =   1365
   ClientWidth     =   9555
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCiaBuscar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   9555
   Begin TabDlg.SSTab SSTabBusqCias 
      Height          =   7125
      Left            =   15
      TabIndex        =   45
      Top             =   60
      Width           =   9540
      _ExtentX        =   16828
      _ExtentY        =   12568
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos de B�squeda"
      TabPicture(0)   =   "frmCiaBuscar.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Actividades"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdLimpiar"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame2"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Compa��as"
      TabPicture(1)   =   "frmCiaBuscar.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdSeleccionar"
      Tab(1).Control(1)=   "cmdCancelar"
      Tab(1).Control(2)=   "fraCias"
      Tab(1).ControlCount=   3
      Begin VB.Frame Frame2 
         Caption         =   "Acceso premium"
         Height          =   750
         Left            =   135
         TabIndex        =   58
         Top             =   5725
         Width           =   9275
         Begin VB.CheckBox optPRDes 
            Caption         =   "Desautorizado"
            Height          =   255
            Left            =   2790
            TabIndex        =   34
            Top             =   330
            Width           =   1500
         End
         Begin VB.CheckBox optPRAut 
            Caption         =   "Autorizado"
            Height          =   255
            Left            =   1530
            TabIndex        =   33
            Top             =   330
            Width           =   1155
         End
         Begin VB.CheckBox chkPremium 
            Caption         =   "Solicitado"
            Height          =   285
            Left            =   165
            TabIndex        =   32
            Top             =   315
            Width           =   1200
         End
      End
      Begin VB.CommandButton cmdLimpiar 
         Caption         =   "&Limpiar"
         Height          =   345
         Left            =   150
         TabIndex        =   35
         Top             =   6630
         Width           =   1005
      End
      Begin VB.Frame Frame3 
         Caption         =   "DAcceso"
         Height          =   1450
         Left            =   135
         TabIndex        =   56
         Top             =   4230
         Width           =   9275
         Begin VB.CommandButton cmdCalSolHasta 
            Enabled         =   0   'False
            Height          =   285
            Left            =   8755
            Picture         =   "frmCiaBuscar.frx":0182
            Style           =   1  'Graphical
            TabIndex        =   31
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   1020
            Width           =   315
         End
         Begin VB.CommandButton cmdCalSolDesde 
            Enabled         =   0   'False
            Height          =   285
            Left            =   8755
            Picture         =   "frmCiaBuscar.frx":070C
            Style           =   1  'Graphical
            TabIndex        =   29
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   670
            Width           =   315
         End
         Begin VB.TextBox txtSolHasta 
            Enabled         =   0   'False
            Height          =   285
            Left            =   7600
            TabIndex        =   30
            Top             =   1020
            Width           =   1110
         End
         Begin VB.TextBox txtSolDesde 
            Enabled         =   0   'False
            Height          =   285
            Left            =   7600
            TabIndex        =   28
            Top             =   670
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalDesAutHasta 
            Enabled         =   0   'False
            Height          =   285
            Left            =   5555
            Picture         =   "frmCiaBuscar.frx":0C96
            Style           =   1  'Graphical
            TabIndex        =   26
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   1020
            Width           =   315
         End
         Begin VB.CommandButton cmdCalDesAutDesde 
            Enabled         =   0   'False
            Height          =   285
            Left            =   5555
            Picture         =   "frmCiaBuscar.frx":1220
            Style           =   1  'Graphical
            TabIndex        =   24
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   670
            Width           =   315
         End
         Begin VB.TextBox txtDesAutHasta 
            Enabled         =   0   'False
            Height          =   285
            Left            =   4400
            TabIndex        =   25
            Top             =   1020
            Width           =   1110
         End
         Begin VB.TextBox txtDesAutDesde 
            Enabled         =   0   'False
            Height          =   285
            Left            =   4400
            TabIndex        =   23
            Top             =   670
            Width           =   1110
         End
         Begin VB.CommandButton cmdCalAutHasta 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2240
            Picture         =   "frmCiaBuscar.frx":17AA
            Style           =   1  'Graphical
            TabIndex        =   21
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   1020
            Width           =   315
         End
         Begin VB.CommandButton cmdCalAutDesde 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2240
            Picture         =   "frmCiaBuscar.frx":1D34
            Style           =   1  'Graphical
            TabIndex        =   19
            TabStop         =   0   'False
            ToolTipText     =   "Mantenimiento"
            Top             =   670
            Width           =   315
         End
         Begin VB.TextBox txtAutHasta 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            TabIndex        =   20
            Top             =   1020
            Width           =   1110
         End
         Begin VB.TextBox txtAutDesde 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1080
            TabIndex        =   18
            Top             =   670
            Width           =   1110
         End
         Begin VB.CheckBox opFPDes 
            Caption         =   "Desautorizada"
            Height          =   285
            Left            =   3500
            TabIndex        =   22
            Top             =   300
            Width           =   1380
         End
         Begin VB.CheckBox opFPAut 
            Caption         =   "Autorizada"
            Height          =   285
            Left            =   180
            TabIndex        =   17
            Top             =   300
            Width           =   1410
         End
         Begin VB.CheckBox opFPSol 
            Caption         =   "Solicitada"
            Height          =   285
            Left            =   6700
            TabIndex        =   27
            Top             =   300
            Width           =   1275
         End
         Begin VB.Label lblSolHasta 
            Caption         =   "DHasta:"
            Height          =   240
            Left            =   6700
            TabIndex        =   64
            Top             =   1070
            Width           =   810
         End
         Begin VB.Label lblSolDesde 
            Caption         =   "DDesde:"
            Height          =   240
            Left            =   6700
            TabIndex        =   63
            Top             =   720
            Width           =   810
         End
         Begin VB.Label lblDesAutHasta 
            Caption         =   "DHasta:"
            Height          =   240
            Left            =   3500
            TabIndex        =   62
            Top             =   1070
            Width           =   810
         End
         Begin VB.Label lblDesAutDesde 
            Caption         =   "DDesde:"
            Height          =   240
            Left            =   3500
            TabIndex        =   61
            Top             =   720
            Width           =   810
         End
         Begin VB.Label lblAutHasta 
            Caption         =   "DHasta:"
            Height          =   240
            Left            =   180
            TabIndex        =   60
            Top             =   1070
            Width           =   815
         End
         Begin VB.Label lblAutDesde 
            Caption         =   "DDesde:"
            Height          =   240
            Left            =   180
            TabIndex        =   59
            Top             =   720
            Width           =   815
         End
      End
      Begin VB.Frame fraCias 
         BorderStyle     =   0  'None
         Height          =   6090
         Left            =   -74775
         TabIndex        =   51
         Top             =   495
         Width           =   9165
         Begin SSDataWidgets_B.SSDBGrid sdbgCias 
            Height          =   6045
            Left            =   0
            TabIndex        =   57
            Top             =   0
            Width           =   9135
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   5
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   26
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2143
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   7117
            Columns(2).Caption=   "Denominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   2910
            Columns(3).Caption=   "Funci�n Proveedora"
            Columns(3).Name =   "FPAUT"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   2910
            Columns(4).Caption=   "Acceso Premium"
            Columns(4).Name =   "PREMIUM"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   16113
            _ExtentY        =   10663
            _StockProps     =   79
            Caption         =   "Compa�ias"
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   345
         Left            =   -70230
         TabIndex        =   50
         Top             =   6630
         Width           =   1005
      End
      Begin VB.CommandButton cmdSeleccionar 
         Caption         =   "&Seleccionar"
         Height          =   345
         Left            =   -71385
         TabIndex        =   49
         Top             =   6630
         Width           =   1005
      End
      Begin VB.Frame Actividades 
         Height          =   810
         Left            =   135
         TabIndex        =   48
         Top             =   3330
         Width           =   9275
         Begin VB.CommandButton cmdSELACT 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8500
            Picture         =   "frmCiaBuscar.frx":22BE
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   330
            Width           =   285
         End
         Begin VB.CommandButton cmdBorrar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8850
            Picture         =   "frmCiaBuscar.frx":2640
            Style           =   1  'Graphical
            TabIndex        =   16
            Top             =   330
            Width           =   285
         End
         Begin VB.Label Label7 
            Caption         =   "Actividad:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   52
            Top             =   360
            Width           =   1125
         End
         Begin VB.Label lblActividad 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1320
            TabIndex        =   14
            Top             =   330
            Width           =   6780
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Datos Generales"
         Height          =   2880
         Left            =   135
         TabIndex        =   0
         Top             =   390
         Width           =   9275
         Begin VB.TextBox txtCP 
            Height          =   285
            Left            =   1320
            TabIndex        =   4
            Top             =   1380
            Width           =   2895
         End
         Begin VB.TextBox txtVolHasta 
            Height          =   285
            Left            =   6240
            TabIndex        =   11
            Top             =   1380
            Width           =   1400
         End
         Begin VB.TextBox txtNif 
            Height          =   285
            Left            =   1320
            TabIndex        =   2
            Top             =   660
            Width           =   2895
         End
         Begin VB.TextBox txtCod 
            Height          =   285
            Left            =   1320
            TabIndex        =   1
            Top             =   300
            Width           =   1455
         End
         Begin VB.TextBox txtDen 
            Height          =   285
            Left            =   6240
            TabIndex        =   8
            Top             =   300
            Width           =   2895
         End
         Begin VB.TextBox txtDir 
            Height          =   285
            Left            =   1320
            TabIndex        =   3
            Top             =   1020
            Width           =   2895
         End
         Begin VB.TextBox txtPob 
            Height          =   285
            Left            =   1320
            TabIndex        =   5
            Top             =   1740
            Width           =   2895
         End
         Begin VB.TextBox txtVol 
            Height          =   285
            Left            =   6240
            TabIndex        =   10
            Top             =   1020
            Width           =   1400
         End
         Begin VB.TextBox txtCliRef1 
            Height          =   285
            Left            =   6240
            TabIndex        =   12
            Top             =   1740
            Width           =   2895
         End
         Begin VB.TextBox txtHom1 
            Height          =   285
            Left            =   6240
            TabIndex        =   13
            Top             =   2100
            Width           =   2895
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
            Height          =   285
            Left            =   1320
            TabIndex        =   6
            Top             =   2100
            Width           =   2895
            DataFieldList   =   "Column 0"
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3228
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   5106
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
            Height          =   285
            Left            =   6240
            TabIndex        =   9
            Top             =   660
            Width           =   2895
            DataFieldList   =   "Column 0"
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3545
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   5106
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
            Height          =   285
            Left            =   1320
            TabIndex        =   7
            Top             =   2460
            Width           =   2895
            DataFieldList   =   "Column 0"
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3545
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   5106
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label Label6 
            Caption         =   "CP:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   55
            Top             =   1425
            Width           =   1125
         End
         Begin VB.Label Label14 
            BackColor       =   &H80000004&
            Caption         =   "(en miles de Euros)"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   7660
            TabIndex        =   54
            Top             =   1425
            Width           =   1580
         End
         Begin VB.Label Label20 
            Alignment       =   1  'Right Justify
            Caption         =   "hasta:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   4605
            TabIndex        =   53
            Top             =   1425
            Width           =   1200
         End
         Begin VB.Label Label8 
            Caption         =   "C�digo:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   44
            Top             =   345
            Width           =   1125
         End
         Begin VB.Label Label4 
            Caption         =   "NIF:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   43
            Top             =   705
            Width           =   1140
         End
         Begin VB.Label Label2 
            Caption         =   "Direcci�n:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   42
            Top             =   1065
            Width           =   1125
         End
         Begin VB.Label Label3 
            Caption         =   "Pa�s:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   40
            Top             =   2145
            Width           =   1125
         End
         Begin VB.Label Label15 
            Caption         =   "Moneda:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   4605
            TabIndex        =   37
            Top             =   705
            Width           =   1470
         End
         Begin VB.Label Label16 
            Caption         =   "Provincia:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   39
            Top             =   2505
            Width           =   1125
         End
         Begin VB.Label Label1 
            Caption         =   "Denominaci�n:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   4605
            TabIndex        =   38
            Top             =   345
            Width           =   1485
         End
         Begin VB.Label Label5 
            Caption         =   "Vol. fact. desde:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   4605
            TabIndex        =   36
            Top             =   1065
            Width           =   1440
         End
         Begin VB.Label Label10 
            Caption         =   "Cliente de referencia:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   4605
            TabIndex        =   47
            Top             =   1785
            Width           =   1650
         End
         Begin VB.Label Label19 
            Caption         =   "Homologaci�n:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   4605
            TabIndex        =   46
            Top             =   2145
            Width           =   1110
         End
         Begin VB.Label Label9 
            Caption         =   "Poblaci�n:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   41
            Top             =   1785
            Width           =   1170
         End
      End
   End
End
Attribute VB_Name = "frmCiaBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public ACN1Seleccionada As Variant
Public ACN2Seleccionada As Variant
Public ACN3Seleccionada As Variant
Public ACN4Seleccionada As Variant
Public ACN5Seleccionada As Variant
Public g_sOrigen As String

'Variables privadas
Private oCias As CCias
Private oCia As CCia

Private CargarComboDesde As Boolean
Private IdCiaSeleccionada As Long

Private sIdiPais As String
Private sIdiProvi As String
Private sIdiVolFact As String
Private sIdiAutorizada As String
Private sIdiDesautorizada As String
Private sIdiSolicitada As String
Private sIdiNoSolicitado As String
Private sIdiSolicitado As String
Private sIdiAutorizado As String
Private sIdiDesautorizado As String
Private sIdiHasta As String
Private sIdiDesde As String

Private Sub LimpiarControles()
        
    txtCod = ""
    txtDen = ""
    txtNif = ""
    txtCP = ""
    txtDir = ""
    txtPob = ""
    sdbcMonCod = ""
    sdbcMonCod_Validate False
    sdbcPaiCod = ""
    sdbcPaiCod_Validate False
    sdbcProviCod = ""
    sdbcProviCod_Validate False
    txtVol = ""
    txtVolHasta = ""
    txtCliRef1 = ""
    txtHom1 = ""
    lblActividad.Caption = ""
    ACN1Seleccionada = Null
    ACN2Seleccionada = Null
    ACN3Seleccionada = Null
    ACN4Seleccionada = Null
    ACN5Seleccionada = Null

    opFPSol.Value = vbUnchecked
    opFPAut.Value = vbUnchecked
    opFPDes.Value = vbUnchecked
    optPRAut.Value = vbUnchecked
    optPRDes.Value = vbUnchecked
    chkPremium.Value = vbUnchecked
    
    txtAutDesde.Text = ""
    txtAutHasta.Text = ""
    txtDesAutDesde.Text = ""
    txtDesAutHasta.Text = ""
    txtSolDesde.Text = ""
    txtSolHasta.Text = ""
    
End Sub



Private Sub cmdBorrar_Click()
    
    lblActividad.Caption = ""
    ACN1Seleccionada = Null
    ACN2Seleccionada = Null
    ACN3Seleccionada = Null
    ACN4Seleccionada = Null
    ACN5Seleccionada = Null

End Sub

Private Sub cmdCalAutDesde_Click()
    'Muestra el formulario del calendario
    
    Set frmCalendar.frmDestination = frmCiaBuscar
    Set frmCalendar.ctrDestination = txtAutDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtAutDesde <> "" Then
        frmCalendar.Calendar.Value = txtAutDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdCalAutHasta_Click()
    Set frmCalendar.frmDestination = frmCiaBuscar
    Set frmCalendar.ctrDestination = txtAutHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtAutHasta <> "" Then
        frmCalendar.Calendar.Value = txtAutHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCalDesAutDesde_Click()
    Set frmCalendar.frmDestination = frmCiaBuscar
    Set frmCalendar.ctrDestination = txtDesAutDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtDesAutDesde <> "" Then
        frmCalendar.Calendar.Value = txtDesAutDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCalDesAutHasta_Click()
    Set frmCalendar.frmDestination = frmCiaBuscar
    Set frmCalendar.ctrDestination = txtDesAutHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtDesAutHasta <> "" Then
        frmCalendar.Calendar.Value = txtDesAutHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCalSolDesde_Click()
    Set frmCalendar.frmDestination = frmCiaBuscar
    Set frmCalendar.ctrDestination = txtSolDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtSolDesde <> "" Then
        frmCalendar.Calendar.Value = txtSolDesde
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCalSolHasta_Click()
    Set frmCalendar.frmDestination = frmCiaBuscar
    Set frmCalendar.ctrDestination = txtSolHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtSolHasta <> "" Then
        frmCalendar.Calendar.Value = txtSolHasta
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub


Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdLimpiar_Click()
    LimpiarControles
End Sub

Private Sub cmdSELACT_Click()
    frmSelActividad.sOrigen = "frmCiaBuscar"
    frmSelActividad.Show 1
End Sub

Private Sub cmdSeleccionar_Click()
    Dim idtemp As Long
    Dim dentemp As String
    Dim codtemp As String
    idtemp = sdbgCias.Columns(0).Value
    codtemp = sdbgCias.Columns(1).Text
    dentemp = sdbgCias.Columns(2).Text
    
    Unload Me
    
    Select Case g_sOrigen
        Case "frmComunicacionCias"
            frmComunicacionCias.Show
            frmComunicacionCias.IdCiaSeleccionada = idtemp
            frmComunicacionCias.ponerCiaSeleccionadaEnCombos
            
        Case "frmCompanias"
            frmCompanias.sdbcCiaCod.AddItem idtemp
            frmCompanias.m_bRespetarCombo = True
            frmCompanias.sdbcCiaCod.Columns(0).Value = idtemp
            frmCompanias.sdbcCiaCod.Text = codtemp
            frmCompanias.m_bRespetarCombo = True
            frmCompanias.sdbcCiaDen.Text = dentemp
            frmCompanias.m_bRespetarCombo = False
            frmCompanias.CiaSeleccionada
        
        Case "frmEnlazarProveedor"
            frmEnlazarProveedor.sdbcCiaCod.AddItem idtemp
            frmEnlazarProveedor.bRespetarCombo = True
            frmEnlazarProveedor.sdbcCiaCod.Columns(0).Value = idtemp
            frmEnlazarProveedor.sdbcCiaCod.Text = codtemp
            frmEnlazarProveedor.bRespetarCombo = True
            frmEnlazarProveedor.sdbcCiaDen.Text = dentemp
            frmEnlazarProveedor.bRespetarCombo = False
            frmEnlazarProveedor.CiaSeleccionada
            

        Case "frmEnlazarComprador"
            frmEnlazarComprador.sdbcCiaCod.AddItem idtemp
            frmEnlazarComprador.bRespetarCombo = True
            frmEnlazarComprador.sdbcCiaCod.Columns(0).Value = idtemp
            frmEnlazarComprador.sdbcCiaCod.Text = codtemp
            frmEnlazarComprador.bRespetarCombo = True
            frmEnlazarComprador.sdbcCiaDen.Text = dentemp
            frmEnlazarComprador.bRespetarCombo = False
            frmEnlazarComprador.CiaSeleccionada
        
        Case "frmRegComunicaciones"
            frmRegComunicaciones.sdbcCiaCod.AddItem idtemp
            frmRegComunicaciones.m_bRespetarCombo = True
            frmRegComunicaciones.sdbcCiaCod.Columns(0).Value = idtemp
            frmRegComunicaciones.sdbcCiaCod.Text = codtemp
            frmRegComunicaciones.m_bRespetarCombo = True
            frmRegComunicaciones.sdbcCiaDen.Text = dentemp
            frmRegComunicaciones.m_bRespetarCombo = False
            frmRegComunicaciones.CiaSeleccionada
         Case "frmRegAcciones"
            frmRegAcciones.sdbcCiaCod.AddItem idtemp
            frmRegAcciones.m_bRespetarCombo = True
            frmRegAcciones.sdbcCiaCod.Columns(0).Value = idtemp
            frmRegAcciones.sdbcCiaCod.Text = codtemp
            frmRegAcciones.m_bRespetarCombo = True
            frmRegAcciones.sdbcCiaDen.Text = dentemp
            frmRegAcciones.m_bRespetarCombo = False
            frmRegAcciones.CiaSeleccionada
    End Select
    
    
End Sub

Private Sub Form_Load()

    If g_sOrigen = "frmEnlazarProveedor" Or g_sOrigen = "frmEnlazarComprador" Then
        Me.Width = 9675
        Me.Height = 6090
        Frame3.Visible = False
        SSTabBusqCias.Height = 4725
        fraCias.Height = 3750
        sdbgCias.Height = 3660
        cmdLimpiar.Top = 4260
        cmdSeleccionar.Top = 4260
        cmdCancelar.Top = 4260
    Else
        Me.Width = 9675
        Me.Height = 7665
    End If
    CargarRecursos
    ConfigurarSeguridad
    ACN1Seleccionada = Null
    ACN2Seleccionada = Null
    ACN3Seleccionada = Null
    ACN4Seleccionada = Null
    ACN5Seleccionada = Null
    
    Set oCia = g_oRaiz.Generar_CCia
    Set oCias = g_oRaiz.Generar_CCias
 
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oCia = Nothing
    Set oCias = Nothing
End Sub

Private Sub opFPAut_Click()
    If opFPAut.Value = vbChecked Then
        txtAutDesde.Enabled = True
        txtAutHasta.Enabled = True
        cmdCalAutDesde.Enabled = True
        cmdCalAutHasta.Enabled = True
    Else
        txtAutDesde.Enabled = False
        txtAutHasta.Enabled = False
        txtAutDesde.Text = ""
        txtAutHasta.Text = ""
        cmdCalAutDesde.Enabled = False
        cmdCalAutHasta.Enabled = False
    End If
End Sub

Private Sub opFPDes_Click()
    If opFPDes.Value = vbChecked Then
        txtDesAutDesde.Enabled = True
        txtDesAutHasta.Enabled = True
        cmdCalDesAutDesde.Enabled = True
        cmdCalDesAutHasta.Enabled = True
    Else
        txtDesAutDesde.Enabled = False
        txtDesAutHasta.Enabled = False
        txtDesAutDesde.Text = ""
        txtDesAutHasta.Text = ""
        cmdCalDesAutDesde.Enabled = False
        cmdCalDesAutHasta.Enabled = False
    End If
End Sub

Private Sub opFPSol_Click()
    If opFPSol.Value = vbChecked Then
        txtSolDesde.Enabled = True
        txtSolHasta.Enabled = True
        cmdCalSolDesde.Enabled = True
        cmdCalSolHasta.Enabled = True
    Else
        txtSolDesde.Enabled = False
        txtSolHasta.Enabled = False
        txtSolDesde.Text = ""
        txtSolHasta.Text = ""
        cmdCalSolDesde.Enabled = False
        cmdCalSolHasta.Enabled = False
    End If
End Sub

Private Sub sdbcMonCod_Change()
    CargarComboDesde = True
End Sub

Private Sub sdbcMonCod_PositionList(ByVal Text As String)
   Dim i As Long
   Dim bm As Variant

    On Error Resume Next
    
    sdbcMonCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMonCod.Rows - 1
            bm = sdbcMonCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMonCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcMonCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcMonCod_Validate(Cancel As Boolean)
    If sdbcMonCod.Text = "" Then
        sdbcMonCod.Columns(0).Value = ""
        Exit Sub
    Else
        Dim ador As ador.Recordset
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcMonCod.Text, True)
        If ador.RecordCount = 0 Then
            NoValida ("Moneda")
            sdbcMonCod.Text = ""
            Cancel = True
        Else
            sdbcMonCod.Text = ador("DEN")
            sdbcMonCod.Columns(2).Text = ador("COD")
            sdbcMonCod.Columns(0).Value = ador("ID")
        End If
        ador.Close
        Set ador = Nothing
    End If

End Sub

Private Sub sdbcPaiCod_Change()

    sdbcProviCod = ""
    CargarComboDesde = True
End Sub



Private Sub sdbcPaiCod_CloseUp()
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
         
    sdbcPaiCod.Text = sdbcPaiCod.Columns(1).Text
    sdbcProviCod = ""
    CargarComboDesde = False
End Sub

Private Sub sdbcPaiCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcPaiCod.RemoveAll
    
    If CargarComboDesde Then
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcPaiCod.Columns(1).Text, , False)
    Else
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    End If
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcPaiCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
End Sub

Private Sub sdbcPaiCod_InitColumnProps()
    
    sdbcPaiCod.DataFieldList = "Column 1"
    sdbcPaiCod.DataFieldToDisplay = "Column 1"
    
End Sub



Private Sub sdbcPaiCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcPaiCod_Validate(Cancel As Boolean)
Dim ador As ador.Recordset

    If sdbcPaiCod.Text = "" Then
        sdbcPaiCod.Columns(0).Value = ""
        sdbcProviCod.Columns(0).Value = ""
        Exit Sub
    Else
        Set ador = g_oRaiz.DevolverPaisesDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcPaiCod.Text, True)
        If ador.RecordCount = 0 Then
            NoValido (sIdiPais)
            sdbcPaiCod.Text = ""
            sdbcPaiCod.Columns(2).Text = ""
            sdbcPaiCod.Columns(0).Value = ""
            sdbcProviCod.Columns(0).Value = ""
            Cancel = True
        Else
            sdbcPaiCod.Text = ador("DEN")
            sdbcPaiCod.Columns(2).Text = ador("COD")
            sdbcPaiCod.Columns(0).Value = ador("ID")
        End If
        ador.Close
        Set ador = Nothing
    End If
End Sub


Private Sub sdbcProviCod_Change()
    CargarComboDesde = True
End Sub

Private Sub sdbcProviCod_CloseUp()
    
    If sdbcProviCod.Value = "..." Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    
    sdbcProviCod.Text = sdbcProviCod.Columns(1).Text
    
    CargarComboDesde = False
End Sub

Private Sub sdbcProviCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcProviCod.RemoveAll
    
    If sdbcPaiCod.Columns("ID").Value <> "" Then
    
        Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(sdbcPaiCod.Columns("ID").Value, , , False)
   
    End If
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcProviCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
    
        
End Sub

Private Sub sdbcProviCod_InitColumnProps()
    
    sdbcProviCod.DataFieldList = "Column 1"
    sdbcProviCod.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbcMonCod_CloseUp()
    
    If sdbcMonCod.Value = "..." Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    sdbcMonCod.Text = sdbcMonCod.Columns(1).Text
    CargarComboDesde = False
    
End Sub

Private Sub sdbcMonCod_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcMonCod.RemoveAll
    
    If CargarComboDesde Then
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , sdbcMonCod.Columns(1).Text, , False)
    Else
        Set ador = g_oRaiz.DevolverMonedasDelPortalDesde(basParametros.g_iCargaMaximaCombos, , , False)
    End If
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcMonCod.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If
       
End Sub

Private Sub sdbcMonCod_InitColumnProps()
    
    sdbcMonCod.DataFieldList = "Column 1"
    sdbcMonCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcProviCod_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProviCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProviCod.Rows - 1
            bm = sdbcProviCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcProviCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcProviCod_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset
    If sdbcProviCod.Text = "" Then
        sdbcProviCod.Columns(0).Value = ""
        Exit Sub
    Else
        If sdbcPaiCod.Text = "" Then
            NoValida (sIdiProvi)
            sdbcProviCod.Text = ""
            Cancel = True
        Else
            Set ador = g_oRaiz.DevolverTodasLasProvinciasDesde(sdbcPaiCod.Columns(0).Value, , sdbcProviCod.Text, True)
            If ador.RecordCount = 0 Then
                NoValida (sIdiProvi)
                sdbcProviCod.Text = ""
                Cancel = True
            Else
                sdbcProviCod.Text = ador("DEN")
                sdbcProviCod.Columns(2).Text = ador("COD")
                sdbcProviCod.Columns(0).Value = ador("ID")
            End If
            ador.Close
            Set ador = Nothing
        End If
    End If
End Sub



Private Sub SSTabBusqCias_Click(PreviousTab As Integer)
    Dim ador As ador.Recordset
    Dim VolFac As Double
    Dim VolFacHasta As Double
    Dim iFPEstado(3) As Integer
    Dim sFPEstado As String
    Dim ctrl As Control
    Dim sPREstado As String
    Dim bSoloPremium As Boolean
    Dim bSoloProveedoras As Boolean
    Dim bSoloCompradorasActivas As Boolean
    Dim bOrdPorDen As Boolean
    
    If SSTabBusqCias.Tab = 1 Then
        sdbgCias.RemoveAll
        
        If txtVol.Text = "" Then
            VolFac = 0
        Else
            If IsNumeric(txtVol.Text) Then
                VolFac = CDbl(txtVol.Text)
            Else
       
                SSTabBusqCias.Tab = 0
                NoValido (sIdiVolFact)
                txtVol.Text = ""
                
                Exit Sub
                
            End If
        End If
        
        iFPEstado(1) = 0: iFPEstado(2) = 0: iFPEstado(3) = 0
        If opFPSol Then iFPEstado(1) = 1
        If opFPDes Then iFPEstado(2) = 2
        If opFPAut Then iFPEstado(3) = 3
        If g_sOrigen = "frmEnlazarProveedor" Then iFPEstado(1) = 0: iFPEstado(2) = 0: iFPEstado(3) = 3
        
        
        If g_sOrigen = "frmEnlazarProveedor" Then
            bSoloProveedoras = True
            bSoloCompradorasActivas = False
            optPRDes.Value = vbUnchecked

        ElseIf g_sOrigen = "frmEnlazarComprador" Then
            bSoloProveedoras = False
            bSoloCompradorasActivas = True
            bSoloPremium = False
            optPRAut.Value = vbUnchecked
            optPRDes.Value = vbUnchecked

        Else
            bSoloProveedoras = False
            bSoloCompradorasActivas = False
            If chkPremium.Value = vbChecked Then
                bSoloPremium = True
            End If
        End If
            
            
        If txtVolHasta = "" Then
            VolFacHasta = 0
        Else
            If IsNumeric(txtVolHasta.Text) Then
                VolFacHasta = CDbl(txtVolHasta.Text)
            Else
                SSTabBusqCias.Tab = 0
                NoValido (sIdiVolFact)
                txtVolHasta = ""
                Exit Sub
            End If
        End If
        
        If txtCod = "" And txtDen <> "" Then
            bOrdPorDen = True
        End If
    
        Set ador = oCias.DevolverCiasDesdeBusqueda(basParametros.g_iCargaMaximaCombos, txtCod.Text, _
            txtDen.Text, , bOrdPorDen, bSoloCompradorasActivas, txtNif.Text, txtDir.Text, txtCP.Text, txtPob.Text, _
            sdbcPaiCod.Columns(0).Value, sdbcProviCod.Columns(0).Value, sdbcMonCod.Columns(0).Value, txtVol.Text, txtVolHasta.Text, _
            txtCliRef1.Text, txtHom1.Text, iFPEstado, ACN1Seleccionada, ACN2Seleccionada, _
            ACN3Seleccionada, ACN4Seleccionada, ACN5Seleccionada, bSoloProveedoras, , bSoloPremium, _
            optPRAut, optPRDes, txtAutDesde.Text, txtAutHasta.Text, txtDesAutDesde.Text, txtDesAutHasta.Text, _
            txtSolDesde.Text, txtSolHasta.Text, True)
    
    
        If Not ador Is Nothing Then
            While Not ador.EOF
                sFPEstado = ""
                
                If g_sOrigen <> "frmEnlazarComprador" Then
                    If ador("FPEST").Value = 3 Then
                        sFPEstado = sIdiAutorizado
                    Else
                        If ador("FPEST").Value = 2 Then
                            sFPEstado = sIdiDesautorizado
                        Else
                            If ador("FPEST").Value = 1 Then
                                sFPEstado = sIdiSolicitado
                            End If
                        End If
                    End If
                End If
                
                If g_udtParametrosGenerales.g_bPremium Then
                    Select Case IIf(IsNull(ador("PREMIUM").Value), 0, ador("PREMIUM").Value)
                        Case 0
                            sPREstado = sIdiNoSolicitado
                        Case 1
                            sPREstado = sIdiSolicitado
                        Case 2
                            sPREstado = sIdiAutorizado
                        Case 3
                            sPREstado = sIdiDesautorizado
                        Case Else
                            sPREstado = ""
                    End Select
                Else
                    sPREstado = ""
                End If
                        
                    
                sdbgCias.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & sFPEstado & Chr(9) & sPREstado
                ador.MoveNext
            
            Wend
            ador.Close
            Set ador = Nothing
        End If
    End If
End Sub



Private Sub ConfigurarSeguridad()

    If Not g_udtParametrosGenerales.g_bPremium Then
        sdbgCias.Columns("PREMIUM").Visible = False
        
        sdbgCias.Columns("COD").Width = sdbgCias.Width * 0.2
        sdbgCias.Columns("DEN").Width = sdbgCias.Width * 0.53
        sdbgCias.Columns("FPAUT").Width = sdbgCias.Width * 0.2
        
        Frame2.Visible = False
    
    Else
        optPRDes.Visible = True
        optPRAut.Visible = True
    End If

End Sub

Private Sub CargarRecursos()
Dim ador As ador.Recordset

Dim sMoncentral As String
Dim sMon As String
    On Error Resume Next
        
    Dim oMon As CMoneda
    Set oMon = g_oRaiz.Generar_CMoneda
    oMon.ID = g_udtParametrosGenerales.g_iMonedaCentral
    oMon.cargarMoneda
    sMoncentral = oMon.Cod
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CIABUSCAR, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        chkPremium.Caption = ador(0).Value
        sIdiSolicitado = ador(0).Value
        opFPSol.Caption = ador(0).Value
        
        ador.MoveNext
        Me.cmdCancelar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdLimpiar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdSeleccionar.Caption = ador(0).Value
        ador.MoveNext
        ador.MoveNext
        Me.Frame1.Caption = ador(0).Value
        ador.MoveNext
        Frame2.Caption = ador(0).Value
        sdbgCias.Columns(4).Caption = ador(0).Value
        ador.MoveNext
        Frame3.Caption = ador(0).Value
        sdbgCias.Columns(3).Caption = ador(0).Value
        ador.MoveNext
        ador.MoveNext
        Me.Label1.Caption = ador(0).Value
        ador.MoveNext
        Me.Label10.Caption = ador(0).Value
        ador.MoveNext
        sMon = ador(0).Value
        sMon = Replace(sMon, "XXX", sMoncentral)
        Me.Label14.Caption = sMon
        ador.MoveNext
        Me.Label15.Caption = ador(0).Value
        ador.MoveNext
        Me.Label16.Caption = ador(0).Value
        ador.MoveNext
        Me.Label19.Caption = ador(0).Value
        ador.MoveNext
        Me.Label2.Caption = ador(0).Value
        ador.MoveNext
        Me.Label20.Caption = ador(0).Value
        ador.MoveNext
        Me.Label3.Caption = ador(0).Value
        ador.MoveNext
        Me.Label4.Caption = ador(0).Value
        ador.MoveNext
        Me.Label5.Caption = ador(0).Value
        ador.MoveNext
        Me.Label6.Caption = ador(0).Value
        ador.MoveNext
        Me.Label7.Caption = ador(0).Value
        ador.MoveNext
        Me.Label8.Caption = ador(0).Value
        ador.MoveNext
        Me.Label9.Caption = ador(0).Value
        
        ador.MoveNext
        sIdiAutorizada = ador(0).Value
        ador.MoveNext
        sIdiDesautorizada = ador(0).Value
        ador.MoveNext
        sIdiSolicitada = ador(0).Value
        
        ador.MoveNext
        optPRAut.Caption = ador(0).Value
        opFPAut.Caption = ador(0).Value
        sIdiAutorizado = ador(0).Value
        
        ador.MoveNext
        sdbcMonCod.Columns(1).Caption = ador(0).Value
        sdbcPaiCod.Columns(1).Caption = ador(0).Value
        sdbcProviCod.Columns(1).Caption = ador(0).Value
        sdbgCias.Columns(2).Caption = ador(0).Value
        
        ador.MoveNext
        sdbcMonCod.Columns(2).Caption = ador(0).Value
        sdbcPaiCod.Columns(2).Caption = ador(0).Value
        sdbcProviCod.Columns(2).Caption = ador(0).Value
        sdbgCias.Columns(1).Caption = ador(0).Value
        
        ador.MoveNext
        sIdiPais = ador(0).Value
        ador.MoveNext
        sIdiProvi = ador(0).Value
        ador.MoveNext
        sIdiVolFact = ador(0).Value
        ador.MoveNext
        sIdiNoSolicitado = ador(0).Value
        
        ador.MoveNext
        opFPDes.Caption = ador(0).Value
        optPRDes.Caption = ador(0).Value
        sIdiDesautorizado = ador(0).Value

        ador.MoveNext
        SSTabBusqCias.TabCaption(0) = ador(0).Value
        ador.MoveNext
        SSTabBusqCias.TabCaption(1) = ador(0).Value
        sdbgCias.Caption = ador(0).Value
        
        ador.MoveNext
        lblAutDesde.Caption = ador(0).Value & ":"
        lblSolDesde.Caption = ador(0).Value & ":"
        lblDesAutDesde.Caption = ador(0).Value & ":"
        sIdiDesde = ador(0).Value
        
        ador.MoveNext
        lblAutHasta.Caption = ador(0).Value & ":"
        lblSolHasta.Caption = ador(0).Value & ":"
        lblDesAutHasta.Caption = ador(0).Value & ":"
        sIdiHasta = ador(0).Value
        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub

Private Sub txtAutDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtAutDesde.Text) And Not txtAutDesde.Text = "" Then
        basMensajes.NoValida (Frame3.Caption & " " & sIdiAutorizado & " " & sIdiDesde)
        txtAutDesde.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtAutHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtAutHasta.Text) And Not txtAutHasta.Text = "" Then
        basMensajes.NoValida (Frame3.Caption & " " & sIdiAutorizado & " " & sIdiHasta)
        txtAutHasta.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtDesAutDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtDesAutDesde.Text) And Not txtDesAutDesde.Text = "" Then
        basMensajes.NoValida (Frame3.Caption & " " & sIdiDesautorizado & " " & sIdiDesde)
        txtDesAutDesde.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtDesAutHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtDesAutHasta.Text) And Not txtDesAutHasta.Text = "" Then
        basMensajes.NoValida (Frame3.Caption & " " & sIdiDesautorizado & " " & sIdiHasta)
        txtDesAutHasta.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtSolDesde_Validate(Cancel As Boolean)
    If Not IsDate(txtSolDesde.Text) And Not txtSolDesde.Text = "" Then
        basMensajes.NoValida (Frame3.Caption & " " & sIdiSolicitado & " " & sIdiDesde)
        txtSolDesde.Text = ""
        Cancel = True
    End If
End Sub


Private Sub txtSolHasta_Validate(Cancel As Boolean)
    If Not IsDate(txtSolHasta.Text) And Not txtSolHasta.Text = "" Then
        basMensajes.NoValida (Frame3.Caption & " " & sIdiSolicitado & " " & sIdiHasta)
        txtSolHasta.Text = ""
        Cancel = True
    End If
End Sub


