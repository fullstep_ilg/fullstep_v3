VERSION 5.00
Object = "{8E27C92E-1264-101C-8A2F-040224009C02}#7.0#0"; "MSCAL.OCX"
Begin VB.Form frmCalendar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DCalendario"
   ClientHeight    =   2985
   ClientLeft      =   3780
   ClientTop       =   3405
   ClientWidth     =   4200
   Icon            =   "frmCalendar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   4200
   ShowInTaskbar   =   0   'False
   Begin MSACAL.Calendar Calendar 
      Height          =   2595
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   4215
      _Version        =   524288
      _ExtentX        =   7435
      _ExtentY        =   4577
      _StockProps     =   1
      BackColor       =   -2147483633
      Year            =   2012
      Month           =   6
      Day             =   28
      DayLength       =   1
      MonthLength     =   0
      DayFontColor    =   0
      FirstDay        =   1
      GridCellEffect  =   1
      GridFontColor   =   10485760
      GridLinesColor  =   -2147483632
      ShowDateSelectors=   -1  'True
      ShowDays        =   -1  'True
      ShowHorizontalGrid=   0   'False
      ShowTitle       =   0   'False
      ShowVerticalGrid=   0   'False
      TitleFontColor  =   10485760
      ValueIsNull     =   0   'False
      BeginProperty DayFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty GridFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty TitleFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2100
      TabIndex        =   1
      Top             =   2595
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   990
      TabIndex        =   0
      Top             =   2595
      Width           =   1005
   End
End
Attribute VB_Name = "frmCalendar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public frmDestination As Form
Public ctrDestination As Control
Public addtotop As Long
Public addtoleft As Long
Public sOrigen As String
 
Private Sub Calendar_Click()
    Calendar.ValueIsNull = False
End Sub

Private Sub Calendar_DblClick()
    cmdAceptar_Click
End Sub

Private Sub cmdAceptar_Click()
    
    
    Select Case frmDestination.Name
    
        Case "frmComunicacionesPorFechas"
            
            ctrDestination = NullToStr(Calendar.Value)
            ctrDestination = Format(ctrDestination, "Short Date")
            'frmComunicacionesPorFechas.sdbgComunicacion False
            
        Case Else
            
            ctrDestination = NullToStr(Calendar.Value)
            ctrDestination = Format(ctrDestination, "Short Date")
            
    End Select
    
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    
    CargarRecursos
    
    If Calendar.ValueIsNull Then
        Calendar.Value = Date
    End If
    
    If g_udtParametrosGenerales.g_sIdioma = "SPA" Then
        Calendar.FirstDay = 1
        
    Else
        Calendar.FirstDay = 7
    End If
End Sub

Private Sub Form_Initialize()
    addtotop = 0
    addtoleft = 0
End Sub

Private Sub Form_Load()
    On Error Resume Next
    
    If Not ctrDestination Is Nothing Then
        Me.Top = (frmMDI.Height - frmMDI.ScaleHeight) + frmDestination.Top + (frmDestination.Height - frmDestination.ScaleHeight) + ctrDestination.Top - 200 + addtotop
        Me.Left = (frmMDI.Width - frmMDI.ScaleWidth) + frmDestination.Left + (frmDestination.Width - frmDestination.ScaleWidth) + ctrDestination.Left + ctrDestination.Width - 200 + addtoleft
    End If
    CargarRecursos
    On Error GoTo 0
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set frmDestination = Nothing
    sOrigen = ""
    addtotop = 0
    addtoleft = 0
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CALENDAR, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
    
        frmCalendar.Caption = Ador(0).Value      '1
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value      '2
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value     '3
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

