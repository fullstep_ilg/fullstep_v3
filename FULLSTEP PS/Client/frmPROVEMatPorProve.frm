VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmPROVEMatPorProve 
   Caption         =   "Asignaci�n de material"
   ClientHeight    =   4260
   ClientLeft      =   4410
   ClientTop       =   4980
   ClientWidth     =   5700
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROVEMatPorProve.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4260
   ScaleWidth      =   5700
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   495
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":014A
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":049E
            Key             =   "GMDes"
            Object.Tag             =   "GMDes"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0831
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0C85
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":0FD9
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":132D
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":1681
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":19D5
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":1D29
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEMatPorProve.frx":207D
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwEstrMatMod 
      Height          =   3615
      Left            =   90
      TabIndex        =   4
      Top             =   180
      Visible         =   0   'False
      Width           =   5445
      _ExtentX        =   9604
      _ExtentY        =   6376
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      Checkboxes      =   -1  'True
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   3615
      Left            =   90
      TabIndex        =   3
      Top             =   165
      Width           =   5490
      _ExtentX        =   9684
      _ExtentY        =   6376
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      SingleSel       =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   0
      ScaleHeight     =   450
      ScaleWidth      =   5700
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3810
      Width           =   5700
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         Height          =   345
         Left            =   105
         TabIndex        =   0
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Enabled         =   0   'False
         Height          =   345
         Left            =   1215
         TabIndex        =   1
         Top             =   45
         Width           =   1005
      End
   End
   Begin VB.PictureBox picEdit 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Left            =   -15
      ScaleHeight     =   450
      ScaleWidth      =   5700
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   3810
      Visible         =   0   'False
      Width           =   5700
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   345
         Left            =   1755
         TabIndex        =   7
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   345
         Left            =   2880
         TabIndex        =   6
         Top             =   75
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmPROVEMatPorProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable para no ejecutar el node check si se ha disparado el mousedown
Private bMouseDown As Boolean
Private SelNode As MSComctlLib.Node

'Variables del resize
Private iAltura As Integer
Private iAnchura As Integer

Public bModif As Boolean 'Si false solo lectura
Public bAnadirProveedorGS As Boolean
Public vOldValue As Variant

Public oEnlaceSeleccionado As CEnlace

Private oGrupsMN1 As CGruposMatNivel1 'Contendr� la estructura entera desde la primera vez que modificar_click
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

Private oGruposMN1Seleccionados As CGruposMatNivel1
Private oGruposMN2Seleccionados As CGruposMatNivel2
Private oGruposMN3Seleccionados As CGruposMatNivel3
Private oGruposMN4Seleccionados As CGruposMatNivel4
Private oGruposMN1DesSeleccionados As CGruposMatNivel1
Private oGruposMN2DesSeleccionados As CGruposMatNivel2
Private oGruposMN3DesSeleccionados As CGruposMatNivel3
Private oGruposMN4DesSeleccionados As CGruposMatNivel4

'Private oProves As CProveedores
'Private oIBaseDatos As IBaseDatos
Private nodQuitarMarca As MSComctlLib.Node


Public bAsignar As Boolean
Private bGrabado As Boolean

Private sIdiMats As String
Private sIdiMat As String


Private Sub CargarEstructuraMateriales()

If Not oEnlaceSeleccionado Is Nothing Then
    
    
    GenerarEstructuraDeMateriales
    
    'cmdRestaurar.Enabled = True
End If

If tvwEstrMat.Nodes.Count = 0 Then Exit Sub

On Error Resume Next
tvwEstrMat.SetFocus
On Error GoTo 0
    

End Sub

Private Sub cmdAceptar_Click()
Dim Gmn1 As String
Dim Gmn2 As String
Dim Gmn3 As String
Dim Gmn4 As String
Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim nodx As MSComctlLib.Node

Dim teserror As CTESError
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod As String
Dim i As Integer
Dim vbm As Variant

Dim bAsignado As Boolean

    Set teserror = New CTESError
    teserror.NumError = TESnoerror

    Screen.MousePointer = vbHourglass


    Set nodx = tvwEstrMatMod.Nodes(1)
    If nodx.Children = 0 Then
         Screen.MousePointer = vbNormal
         Exit Sub
    End If
    
    'Comprueba que se haya seleccionado al menos alg�n material
    If bModif Or bAsignar Then
      For i = 1 To Me.tvwEstrMatMod.Nodes.Count
          If tvwEstrMatMod.Nodes(i).Checked = True Then
              bAsignado = True
              Exit For
          End If
      Next i
      If bAsignado = False Then
          Screen.MousePointer = vbNormal
          basMensajes.SeleccionarMaterial
          Exit Sub
      End If
    End If
    
    
    Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            scod1 = DevolverCod(nod1) & Mid$("                         ", 1, 20 - Len(DevolverCod(nod1)))
            ' No asignado
            If Not nod1.Checked Then
                If Not oGruposMN1.Item(scod1) Is Nothing Then
                    ' Antes si
                    oGruposMN1DesSeleccionados.Add DevolverCod(nod1), ""
                End If
                
                Set nod2 = nod1.Child
                
                While Not nod2 Is Nothing
                    scod2 = DevolverCod(nod2) & Mid$("                         ", 1, 20 - Len(DevolverCod(nod2)))
                    ' No asignado
                    If Not nod2.Checked Then
                        If Not oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                            ' Antes si
                            oGruposMN2DesSeleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                        End If
                
                        Set nod3 = nod2.Child
                        While Not nod3 Is Nothing
                            scod3 = DevolverCod(nod3) & Mid$("                         ", 1, 20 - Len(DevolverCod(nod3)))
                            If Not nod3.Checked Then
                                If Not oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                    ' Antes si
                                    oGruposMN3DesSeleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                End If
                                Set nod4 = nod3.Child
                                While Not nod4 Is Nothing
                                    scod4 = DevolverCod(nod4) & Mid$("                         ", 1, 20 - Len(DevolverCod(nod4)))
                                    If Not nod4.Checked Then
                                        If Not oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                        ' Antes si
                                        oGruposMN4DesSeleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                        End If
                                    Else
                                        If oGruposMN4.Item(scod1 & scod2 & scod3 & scod4) Is Nothing Then
                                            oGruposMN4Seleccionados.Add DevolverCod(nod4.Parent.Parent.Parent), DevolverCod(nod4.Parent.Parent), DevolverCod(nod4.Parent), "", "", "", DevolverCod(nod4), ""
                                        End If
                                    End If
                                    
                                    Set nod4 = nod4.Next
                                Wend
                            Else
                                ' Si esta checked GMN3
                                If oGruposMN3.Item(scod1 & scod2 & scod3) Is Nothing Then
                                    'No estaba asignado antes.
                                    oGruposMN3Seleccionados.Add DevolverCod(nod3.Parent.Parent), DevolverCod(nod3.Parent), DevolverCod(nod3), "", "", ""
                                End If
                            End If
                            
                            Set nod3 = nod3.Next
                        Wend
                    Else
                        ' Si esta checked GMN2
                        If oGruposMN2.Item(scod1 & scod2) Is Nothing Then
                            'No estaba asignado antes.
                            oGruposMN2Seleccionados.Add DevolverCod(nod2.Parent), "", DevolverCod(nod2), ""
                        End If
                    End If
                    
                    Set nod2 = nod2.Next
                Wend
                
            Else
            ' Si esta checked GMN1
               If oGruposMN1.Item(scod1) Is Nothing Then
                    'No estaba asignado antes.
                    oGruposMN1Seleccionados.Add DevolverCod(nod1), ""
                End If
            End If
            Set nod1 = nod1.Next
        Wend
  
    Set teserror = oEnlaceSeleccionado.DesAsignarMaterial(oGruposMN1DesSeleccionados, oGruposMN2DesSeleccionados, oGruposMN3DesSeleccionados, oGruposMN4DesSeleccionados)
  
    
  If teserror.NumError <> TESnoerror Then

    basErrores.TratarError teserror
    Set oGruposMN1DesSeleccionados = Nothing
    Set oGruposMN2DesSeleccionados = Nothing
    Set oGruposMN3DesSeleccionados = Nothing
    Set oGruposMN4DesSeleccionados = Nothing
    Set oGruposMN1DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel4
    Exit Sub
  End If
  
    If bAnadirProveedorGS Then
        Set teserror = oEnlaceSeleccionado.AnyadirEnlace(vOldValue)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oGruposMN1Seleccionados = Nothing
            Set oGruposMN2Seleccionados = Nothing
            Set oGruposMN3Seleccionados = Nothing
            Set oGruposMN4Seleccionados = Nothing
            Set oGruposMN1Seleccionados = g_oRaiz.Generar_CGruposMatNivel1
            Set oGruposMN2Seleccionados = g_oRaiz.Generar_CGruposMatNivel2
            Set oGruposMN3Seleccionados = g_oRaiz.Generar_CGruposMatNivel3
            Set oGruposMN4Seleccionados = g_oRaiz.Generar_CGruposMatNivel4
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
  
  Set teserror = oEnlaceSeleccionado.AsignarMaterial(oGruposMN1Seleccionados, oGruposMN2Seleccionados, oGruposMN3Seleccionados, oGruposMN4Seleccionados)

  
  If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing
    Set oGruposMN1Seleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3Seleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4Seleccionados = g_oRaiz.Generar_CGruposMatNivel4
    Screen.MousePointer = vbNormal
    Exit Sub
  End If
  
  
  Set oGruposMN1Seleccionados = Nothing
  Set oGruposMN2Seleccionados = Nothing
  Set oGruposMN3Seleccionados = Nothing
  Set oGruposMN4Seleccionados = Nothing
  Set oGruposMN1DesSeleccionados = Nothing
  Set oGruposMN2DesSeleccionados = Nothing
  Set oGruposMN3DesSeleccionados = Nothing
  Set oGruposMN4DesSeleccionados = Nothing
      
      
    If bModif Then
      bGrabado = True
      Unload Me
    Else
      tvwEstrMatMod.Visible = False
      tvwEstrMat.Visible = True
      picNavigate.Visible = True
      picEdit.Visible = False
      cmdRestaurar_Click
    End If
      
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdCancelar_Click()
         
    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing
    Set oGruposMN1DesSeleccionados = Nothing
    Set oGruposMN2DesSeleccionados = Nothing
    Set oGruposMN3DesSeleccionados = Nothing
    Set oGruposMN4DesSeleccionados = Nothing

If bModif Then
    Unload Me
Else
    tvwEstrMatMod.Visible = False
    tvwEstrMat.Visible = True
    picNavigate.Visible = True
    picEdit.Visible = False
End If

End Sub


Private Sub cmdModificar_Click()


    picNavigate.Visible = False
    picEdit.Visible = True
   
    'Vamos a modificar la asignaci�n de material
    tvwEstrMat.Visible = False
    tvwEstrMatMod.Visible = True
    
    Screen.MousePointer = vbHourglass
    If oGrupsMN1 Is Nothing Then
         Set oGrupsMN1 = g_oRaiz.Generar_CGruposMatNivel1
        'Genera la estructura de materiales entera
         'frmEsperaPortal.Show 1
         DoEvents
         oGrupsMN1.GenerarEstructuraMateriales oEnlaceSeleccionado.IDCiaCompradora
         Unload frmEsperaPortal
    End If
    GenerarEstructuraMatAsignable
    Screen.MousePointer = vbNormal
    
    Set oGruposMN1DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4DesSeleccionados = g_oRaiz.Generar_CGruposMatNivel4
    
    Set oGruposMN1Seleccionados = g_oRaiz.Generar_CGruposMatNivel1
    Set oGruposMN2Seleccionados = g_oRaiz.Generar_CGruposMatNivel2
    Set oGruposMN3Seleccionados = g_oRaiz.Generar_CGruposMatNivel3
    Set oGruposMN4Seleccionados = g_oRaiz.Generar_CGruposMatNivel4

    bModif = True
End Sub

Private Sub cmdRestaurar_Click()

    Screen.MousePointer = vbHourglass
    
    CargarEstructuraMateriales
        
    Screen.MousePointer = vbNormal
End Sub


Private Sub Form_Load()

    iAltura = 4665
    iAnchura = 5820
    CargarRecursos
    Me.Height = 4665
    Me.Width = 5820
    If Me.Top + Me.Height > frmMDI.Top + frmMDI.ScaleHeight Or Me.Left + Me.Width > frmMDI.Left + frmMDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Set oGrupsMN1 = Nothing

    If Not oEnlaceSeleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        
        frmEsperaPortal.Show
        DoEvents
        CargarEstructuraMateriales
        Unload frmEsperaPortal
        Me.Show
        If bModif Then
            cmdModificar_Click
        Else
            picNavigate.Visible = True
            picEdit.Visible = False
        End If

    Else
        Unload Me
    End If
    
    If bAsignar Then
        cmdCancelar.Enabled = False
    End If
    
    Screen.MousePointer = vbNormal
        
End Sub

Private Sub Form_Resize()
    
    
    If Me.Height < 2500 Then Exit Sub
    If Me.Width < 1000 Then Exit Sub
    
    tvwEstrMat.Width = Me.Width - 345
    tvwEstrMat.Height = Me.Height - 1050
    tvwEstrMatMod.Width = tvwEstrMat.Width
    tvwEstrMatMod.Height = tvwEstrMat.Height
    picEdit.Width = tvwEstrMat.Width
    picEdit.Top = tvwEstrMat.Height + tvwEstrMat.Top + 15
    cmdAceptar.Left = picEdit.Width / 2 - 1025
    cmdCancelar.Left = cmdAceptar.Left + 1050
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim bAsignado As Boolean
    Dim i As Integer
    
    'Comprueba que se haya seleccionado al menos alg�n material
    
    If tvwEstrMatMod.Nodes.Count > 1 And bAsignar And bGrabado = False Then
        For i = 1 To Me.tvwEstrMatMod.Nodes.Count
            If tvwEstrMatMod.Nodes(i).Checked = True Then
                bAsignado = True
                Exit For
            End If
        Next i
        If bAsignado = False Then
            basMensajes.SeleccionarMaterial
        End If
        Cancel = True
        Exit Sub
    End If

    'Libera los objetos
    Set oGruposMN1 = Nothing
    Set oGruposMN2 = Nothing
    Set oGruposMN3 = Nothing
    Set oGruposMN4 = Nothing
    Set oEnlaceSeleccionado = Nothing
    Set oGruposMN1Seleccionados = Nothing
    Set oGruposMN2Seleccionados = Nothing
    Set oGruposMN3Seleccionados = Nothing
    Set oGruposMN4Seleccionados = Nothing
    Set oGruposMN1DesSeleccionados = Nothing
    Set oGruposMN2DesSeleccionados = Nothing
    Set oGruposMN3DesSeleccionados = Nothing
    Set oGruposMN4DesSeleccionados = Nothing
    Set oGrupsMN1 = Nothing
    bAsignar = False
    bGrabado = False
End Sub

Private Sub GenerarEstructuraDeMateriales()

Dim oGMN1 As CGrupoMatNivel1
Dim nodx As MSComctlLib.Node
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String


If oEnlaceSeleccionado Is Nothing Then Exit Sub

tvwEstrMat.Nodes.Clear
Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdiMats, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True

On Error GoTo error

With oEnlaceSeleccionado


'************** GruposMN1 ***********
Set oGruposMN1 = .DevolverGruposMN1Asignados(False)
    
        For Each oGMN1 In oGruposMN1
    
            scod1 = oGMN1.Cod & Mid$("                         ", 1, 20 - Len(oGMN1.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1A")
            nodx.Tag = "GMN1" & CStr(oGMN1.Cod) ' Con la X1X indicamos que el GMN1 ha sido asignado.
            
        Next
        
      
    
    '************** GruposMN2 ***********
    Set oGruposMN2 = .DevolverGruposMN2Asignados(False)
    
        For Each oGMN2 In oGruposMN2
            scod1 = oGMN2.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN2.GMN1Cod))
            scod2 = oGMN2.Cod & Mid$("                         ", 1, 20 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN2.GMN1Cod) & " - " & oGMN2.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN2.GMN1Cod)
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2A")
            nodx.Tag = "GMN2" & CStr(oGMN2.Cod) ' Con la X1X indicamos que la GMN2 ha sido asignada directamente.
            If nodx.Parent.image = "GMN1" Then
                nodx.Parent.Expanded = True
            End If
        Next
    
    
    '************** GruposMN3 ***********
    Set oGruposMN3 = .DevolverGruposMN3Asignados(False)
    
    If Not oGruposMN3 Is Nothing Then
        For Each oGMN3 In oGruposMN3
            scod1 = oGMN3.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN3.GMN1Cod))
            scod2 = oGMN3.GMN2Cod & Mid$("                         ", 1, 20 - Len(oGMN3.GMN2Cod))
            scod3 = oGMN3.Cod & Mid$("                         ", 1, 20 - Len(oGMN3.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN3.GMN1Cod) & " - " & oGMN3.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN3.GMN1Cod)  ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN3.GMN2Cod) & " - " & oGMN3.GMN2Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN3.GMN2Cod)   ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3A")
            nodx.Tag = "GMN3" & CStr(oGMN3.Cod) ' Con la X1X indicamos que la GMN3 ha sido asignada directamente.
            If nodx.Parent.image = "GMN2" Then
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        Next
    
    End If
    
    
    '************** GruposMN4 ***********
    Set oGruposMN4 = .DevolverGruposMN4Asignados(False)
    
    If Not oGruposMN4 Is Nothing Then
        
        For Each oGMN4 In oGruposMN4

            scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN1Cod))
            scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN2Cod))
            scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, 20 - Len(oGMN4.GMN3Cod))
            scod4 = oGMN4.Cod & Mid$("                         ", 1, 20 - Len(oGMN4.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN4.GMN1Cod) & " - " & oGMN4.GMN1Den, "GMN1")
            nodx.Tag = "GMN1" & CStr(oGMN4.GMN1Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN4.GMN2Cod) & " - " & oGMN4.GMN2Den, "GMN2")
            nodx.Tag = "GMN2" & CStr(oGMN4.GMN2Cod) ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN4.GMN3Cod) & " - " & oGMN4.GMN3Den, "GMN3")
            nodx.Tag = "GMN3" & CStr(oGMN4.GMN3Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4A")
            nodx.Tag = "GMN4" & CStr(oGMN4.Cod) ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
            
            If (nodx.Parent.image) = "GMN3" Then
                nodx.Parent.Parent.Parent.Expanded = True
                nodx.Parent.Parent.Expanded = True
                nodx.Parent.Expanded = True
            End If
        
        Next
    
    End If
 
 End With
 
Exit Sub

error:
    Set nodx = Nothing
    Resume Next
End Sub


Public Function DevolverCod(ByVal Node As MSComctlLib.Node) As Variant

Select Case Left(Node.Tag, 4)

Case "GMN1"
        
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "GMN2"
        
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "GMN3"
        
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
   
Case "GMN4"
        
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
   
End Select

End Function

Private Sub GenerarEstructuraMatAsignable()

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim nodx As MSComctlLib.Node
Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

On Error GoTo error

tvwEstrMatMod.Nodes.Clear

Set nodx = tvwEstrMatMod.Nodes.Add(, , "Raiz", sIdiMat, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True

For Each oGMN1 In oGrupsMN1
    scod1 = oGMN1.Cod & Mid$("                         ", 1, 20 - Len(oGMN1.Cod))
    Set nodx = tvwEstrMatMod.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, CStr(oGMN1.Cod) & " - " & oGMN1.Den, "GMN1")
    nodx.Tag = "GMN1" & CStr(oGMN1.Cod)
    
    For Each oGMN2 In oGMN1.GruposMatNivel2
        scod2 = oGMN2.Cod & Mid$("                         ", 1, 20 - Len(oGMN2.Cod))
        Set nodx = tvwEstrMatMod.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, CStr(oGMN2.Cod) & " - " & oGMN2.Den, "GMN2")
        nodx.Tag = "GMN2" & CStr(oGMN2.Cod)
            
            For Each oGMN3 In oGMN2.GruposMatNivel3
                
                scod3 = oGMN3.Cod & Mid$("                         ", 1, 20 - Len(oGMN3.Cod))
                Set nodx = tvwEstrMatMod.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, CStr(oGMN3.Cod) & " - " & oGMN3.Den, "GMN3")
                nodx.Tag = "GMN3" & CStr(oGMN3.Cod)
                        
                        For Each oGMN4 In oGMN3.GruposMatNivel4
                            scod4 = oGMN4.Cod & Mid$("                         ", 1, 20 - Len(oGMN4.Cod))
                            Set nodx = tvwEstrMatMod.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, CStr(oGMN4.Cod) & " - " & oGMN4.Den, "GMN4")
                            nodx.Tag = "GMN4" & CStr(oGMN4.Cod)
                        Next
            Next
    Next
Next

 ' Ahora de esta estructura debemos marcar los materiales que ya estan seleccionados

For Each nodx In tvwEstrMat.Nodes
    If Right(nodx.image, 1) = "A" Then
        tvwEstrMatMod.Nodes(nodx.Key).Checked = True
        If nodx.Visible Then
            tvwEstrMatMod.Nodes(nodx.Key).EnsureVisible
        End If
        DoEvents
    End If
Next
 
Exit Sub

error:
    Set nodx = Nothing
    Resume Next
    
End Sub

Private Sub tvwEstrMatMod_KeyUp(KeyCode As Integer, Shift As Integer)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

If bMouseDown Then
    
    bMouseDown = False
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = Not SelNode.Checked
        DoEvents
        Exit Sub
    End If
    
    If SelNode.Checked Then
            
            MarcarTodosLosHijos SelNode
            
    Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
        
    End If

End If

End Sub

Private Sub tvwEstrMatMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.Node
Dim tvhti As TVHITTESTINFO

                  
    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwEstrMatMod.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox
            
            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwEstrMatMod.HWND, TVM_HITTEST, 0, tvhti)) Then
            
                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento
                             
                bMouseDown = True
                Set SelNode = nod
                             
                End If
            End If
        End If
    End If
    

End Sub

Private Sub tvwEstrMatMod_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

If bMouseDown Then
    
    bMouseDown = False
    
    If SelNode.Tag = "Raiz" Then
        SelNode.Checked = Not SelNode.Checked
        DoEvents
        Exit Sub
    End If
    
    If SelNode.Checked Then
            
            MarcarTodosLosHijos SelNode
            
    Else
            QuitarMarcaTodosSusPadres SelNode
            QuitarMarcaTodosLosHijos SelNode
        
    End If

End If

End Sub

Private Sub tvwEstrMatMod_NodeCheck(ByVal Node As MSComctlLib.Node)
If bMouseDown Then
    Exit Sub
End If

bMouseDown = True
Set SelNode = Node

End Sub
Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = True
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = True
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = True
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = True
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = True
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = True
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = True
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub
Private Sub QuitarMarcaTodosLosHijos(ByVal nodx As MSComctlLib.Node)

Dim nod1 As MSComctlLib.Node
Dim nod2 As MSComctlLib.Node
Dim nod3 As MSComctlLib.Node
Dim nod4 As MSComctlLib.Node

Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                           
        Set nod1 = nodx.Child
   
        While Not nod1 Is Nothing
            
            nod1.Checked = False
            Set nod2 = nod1.Child
            
            While Not nod2 Is Nothing
                nod2.Checked = False
                Set nod3 = nod2.Child
                
                While Not nod3 Is Nothing
                    nod3.Checked = False
                    Set nod4 = nod3.Child
                    
                    While Not nod4 Is Nothing
                        nod4.Checked = False
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set nod1 = nod1.Next
         Wend
            
    DoEvents
    Case "GMN2"
        
        Set nod2 = nodx.Child
        
        While Not nod2 Is Nothing
            nod2.Checked = False
            Set nod3 = nod2.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            Set nod2 = nod2.Next
        Wend
    
    Case "GMN3"
            
            Set nod3 = nodx.Child
            While Not nod3 Is Nothing
                nod3.Checked = False
                Set nod4 = nod3.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                Set nod3 = nod3.Next
            Wend
            
            
    Case "GMN4"
            
                Set nod4 = nodx.Child
                While Not nod4 Is Nothing
                    nod4.Checked = False
                    Set nod4 = nod4.Next
                Wend
                
End Select

End Sub

Private Sub EliminarSusHijosDeDesAsignados(ByVal nodx As MSComctlLib.Node)

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4


Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                               
           If Not oGruposMN2DesSeleccionados Is Nothing Then
                For Each oGMN2 In oGruposMN2DesSeleccionados
                    If oGMN2.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN2DesSeleccionados.Remove CStr(oGMN2.GMN1Cod) & CStr(oGMN2.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN3DesSeleccionados Is Nothing Then
                For Each oGMN3 In oGruposMN3DesSeleccionados
                    If oGMN3.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN3DesSeleccionados.Remove CStr(oGMN3.GMN1Cod) & CStr(oGMN3.GMN2Cod) & CStr(oGMN3.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN4DesSeleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4DesSeleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4DesSeleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
            
    Case "GMN2"
        
        If Not oGruposMN3DesSeleccionados Is Nothing Then
                For Each oGMN3 In oGruposMN3DesSeleccionados
                    If oGMN3.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN3DesSeleccionados.Remove CStr(oGMN3.GMN1Cod) & CStr(oGMN3.GMN2Cod) & CStr(oGMN3.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN4DesSeleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4DesSeleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4DesSeleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
    
    Case "GMN3"
            
            If Not oGruposMN4DesSeleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4DesSeleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4DesSeleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
            
    Case "GMN4"
             
End Select


End Sub

Private Sub EliminarSusHijosDeAsignados(ByVal nodx As MSComctlLib.Node)

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4


Select Case Left(nodx.Tag, 4)

    Case "GMN1"
                               
           If Not oGruposMN2Seleccionados Is Nothing Then
                For Each oGMN2 In oGruposMN2Seleccionados
                    If oGMN2.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN2Seleccionados.Remove CStr(oGMN2.GMN1Cod) & CStr(oGMN2.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN3Seleccionados Is Nothing Then
                For Each oGMN3 In oGruposMN3Seleccionados
                    If oGMN3.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN3Seleccionados.Remove CStr(oGMN3.GMN1Cod) & CStr(oGMN3.GMN2Cod) & CStr(oGMN3.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN4Seleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4Seleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4Seleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
            
    Case "GMN2"
        
        If Not oGruposMN3Seleccionados Is Nothing Then
                For Each oGMN3 In oGruposMN3Seleccionados
                    If oGMN3.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN3Seleccionados.Remove CStr(oGMN3.GMN1Cod) & CStr(oGMN3.GMN2Cod) & CStr(oGMN3.Cod)
                    End If
                Next
            End If
            
            If Not oGruposMN4Seleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4Seleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4Seleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
    
    Case "GMN3"
            
            If Not oGruposMN4Seleccionados Is Nothing Then
                For Each oGMN4 In oGruposMN4Seleccionados
                    If oGMN4.GMN1Cod = DevolverCod(nodx) Then
                        oGruposMN4Seleccionados.Remove CStr(oGMN4.GMN1Cod) & CStr(oGMN4.GMN2Cod) & CStr(oGMN4.GMN3Cod) & CStr(oGMN4.Cod)
                    End If
                Next
            End If
            
    Case "GMN4"
             
End Select


End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.Node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        nodx.Checked = False
        Set nodx = nodx.Parent
    Wend
    DoEvents
End Sub

Private Sub EliminarSusPadresDeAsignados(ByVal nodx As MSComctlLib.Node)

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4


Select Case Left(nodx.Tag, 4)

    Case "GMN4"
                               
            If Not oGruposMN1Seleccionados Is Nothing Then
                 If Not oGruposMN1Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent.Parent))) Is Nothing Then
                    oGruposMN1Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent.Parent))
                 End If
            End If
            
            If Not oGruposMN2Seleccionados Is Nothing Then
                    If Not (oGruposMN2Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent.Parent)) & CStr(DevolverCod(nodx.Parent.Parent))) Is Nothing) Then
                        oGruposMN2Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent.Parent)) & CStr(DevolverCod(nodx.Parent.Parent))
                    End If
            End If
            
            If Not oGruposMN3Seleccionados Is Nothing Then
                    If Not (oGruposMN3Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent.Parent)) & CStr(DevolverCod(nodx.Parent.Parent)) & CStr(DevolverCod(nodx.Parent))) Is Nothing) Then
                        oGruposMN3Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent.Parent)) & CStr(DevolverCod(nodx.Parent.Parent)) & CStr(DevolverCod(nodx.Parent))
                    End If
                
            End If
            
    Case "GMN3"
        
            If Not oGruposMN1Seleccionados Is Nothing Then
                 If Not oGruposMN1Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent))) Is Nothing Then
                    oGruposMN1Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent))
                 End If
            End If
            
            If Not oGruposMN2Seleccionados Is Nothing Then
                    If Not (oGruposMN2Seleccionados.Item(CStr(DevolverCod(nodx.Parent.Parent)) & CStr(DevolverCod(nodx.Parent))) Is Nothing) Then
                        oGruposMN2Seleccionados.Remove CStr(DevolverCod(nodx.Parent.Parent)) & CStr(DevolverCod(nodx.Parent))
                    End If
            End If
            
    
    Case "GMN2"
            
             If Not oGruposMN1Seleccionados Is Nothing Then
                 If Not oGruposMN1Seleccionados.Item(CStr(DevolverCod(nodx.Parent))) Is Nothing Then
                    oGruposMN1Seleccionados.Remove CStr(DevolverCod(nodx.Parent))
                 End If
            End If
            
            
    Case "GMN1"
             
End Select
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVEMATPORPROVE, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdModificar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdRestaurar.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiMats = Ador(0).Value
        Ador.MoveNext
        sIdiMat = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub






