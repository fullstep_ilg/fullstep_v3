Attribute VB_Name = "basBrowse"
'------------------------------------------------------------------------------
'
' M�dulo con las declaraciones y funciones para BrowseForFolder
'
'------------------------------------------------------------------------------
Option Explicit
' Variable para guardar el Caption a mostrar
Private sBFFCaption As String

Private sFolderIni As String

Private Const WM_USER = &H400&
Public Const MAX_PATH = 260&

' Tipo para usar con SHBrowseForFolder
Private Type BrowseInfo
    hWndOwner               As Long             ' hWnd del formulario
    pIDLRoot                As Long             ' Especifica el pID de la carpeta inicial
    pszDisplayName          As String           ' Nombre del item seleccionado
    lpszTitle               As String           ' T�tulo a mostrar encima del �rbol
    ulFlags                 As Long             '
    lpfnCallback            As Long             ' Funci�n CallBack
    lParam                  As Long             ' Informaci�n extra a pasar a la funci�n Callback
    iImage                  As Long             '
End Type

'// Browsing for directory.
Public Const BIF_RETURNONLYFSDIRS = &H1&       '// For finding a folder to start document searching
Public Const BIF_DONTGOBELOWDOMAIN = &H2&      '// For starting the Find Computer
Public Const BIF_STATUSTEXT = &H4&
Public Const BIF_RETURNFSANCESTORS = &H8&
Public Const BIF_EDITBOX = &H10&
Public Const BIF_VALIDATE = &H20&              '// insist on valid result (or CANCEL)
'
Public Const BIF_BROWSEFORCOMPUTER = &H1000&   '// Browsing for Computers.
Public Const BIF_BROWSEFORPRINTER = &H2000&    '// Browsing for Printers
Public Const BIF_BROWSEINCLUDEFILES = &H4000&  '// Browsing for Everything
'
'// message from browser
Public Const BFFM_INITIALIZED = 1
Public Const BFFM_SELCHANGED = 2
Public Const BFFM_VALIDATEFAILED = 3          '// lParam:szPath ret:1(cont),0(EndDialog)
'Public Const BFFM_VALIDATEFAILEDW = 4&         '// lParam:wzPath ret:1(cont),0(EndDialog)
'
'// messages to browser
Public Const BFFM_SETSTATUSTEXT = (WM_USER + 100)
Public Const BFFM_ENABLEOK = (WM_USER + 101)
Public Const BFFM_SETSELECTION = (WM_USER + 102)
'Public Const BFFM_SETSELECTIONW = (WM_USER + 103&)
'Public Const BFFM_SETSTATUSTEXTW = (WM_USER + 104&)
'
Private Declare Function SHBrowseForFolder Lib "shell32.dll" _
        (lpbi As BrowseInfo) As Long
'
'Private Declare Sub CoTaskMemFree Lib "OLE32.DLL" _
'        (ByVal hMem As Long)
'
Private Declare Function SHGetPathFromIDList Lib "shell32.dll" _
        (ByVal pidList As Long, ByVal lpBuffer As String) As Long
'
'Private Declare Function SendMessage Lib "user32.dll" Alias "SendMessageA" _
'    (ByVal hWnd As Long, ByVal wMsg As Long, _
'    ByVal wParam As Long, lParam As Any) As Long

' Declaraci�n de la funci�n del API para cambiar el t�tulo de una ventana
Private Declare Function SetWindowText Lib "user32.dll" Alias "SetWindowTextA" _
    (ByVal hWnd As Long, ByVal lpString As String) As Long


' Estas son las dos funciones para "browsear"
' La primera es la funci�n callback, que se encargar� de inicializar la ventana de selecci�n

Public Function BrowseFolderCallbackProc(ByVal hWndOwner As Long, _
                                        ByVal uMSG As Long, _
                                        ByVal lParam As Long, _
                                        ByVal pData As Long) As Long
    ' Llamada CallBack para usar con la funci�n BrowseForFolder     (12/May/99)
    Dim szDir As String

    On Local Error Resume Next

    Select Case uMSG
    '--------------------------------------------------------------------------
    ' Este mensaje se enviar� cuando se inicia el di�logo,
    ' entonces es cuando hay que indicar el directorio de inicio.
    Case BFFM_INITIALIZED
        ' Si se ha asignado el path de inicio, empezar por ese path
        If Len(sFolderIni) Then
            szDir = sFolderIni & Chr$(0)
            ' WParam  ser� TRUE  si se especifica un path.
            '         ser� FALSE si se especifica un pIDL.
            Call SendMessage(hWndOwner, BFFM_SETSELECTION, 1&, ByVal szDir)
        End If

        ' Si se ha especificado el t�tulo de la ventana
        If Len(sBFFCaption) Then
            ' Cambiar el t�tulo de la ventana.
            ' Aunque parezca que se cambia el t�tulo de la ventana "propietaria",
            ' realmente se cambia el de la ventana de selecci�n.
            Call SetWindowText(hWndOwner, sBFFCaption)
        End If
    '--------------------------------------------------------------------------
    ' Este mensaje se produce cuando se cambia el directorio
    ' Si nuestro form est� subclasificado para recibir mensajes,
    ' puede interceptar el mensaje BFFM_SETSTATUSTEXT
    ' para mostrar el directorio que se est� seleccionando.
'    Case BFFM_SELCHANGED
'        szDir = String$(MAX_PATH, 0)
'        ' Notifica a la ventana del directorio actualmente seleccionado,
'        ' (al menos en teor�a, ya que no lo hace...)
'        If SHGetPathFromIDList(lParam, szDir) Then
'            'Debug.Print szDir
'            Call SendMessage(hWndOwner, BFFM_SETSTATUSTEXT, 0&, ByVal szDir)
'        End If
'        Call CoTaskMemFree(lParam)
    End Select

    Err = 0
    BrowseFolderCallbackProc = 0

End Function


Public Function BrowseForFolder(ByVal hWndOwner As Long, _
                                ByVal sPrompt As String, _
                                Optional sInitDir As String = "", _
                                Optional ByVal lFlags As Long = BIF_RETURNONLYFSDIRS) As String

    ' Muestra el di�logo de selecci�n de directorios de Windows
    ' Si todo va bien, devuelve el directorio seleccionado
    ' Si se cancela, se devuelve una cadena vac�a y se produce el error 32755
    '
    ' Los par�metros de entrada:
    '   El hWnd de la ventana
    '   El t�tulo a mostrar encima del �rbol
    '   Opcionalmente el directorio de inicio
    '   En lFlags se puede especificar lo que se podr� seleccionar:
    '       BIF_BROWSEINCLUDEFILES, etc.
    '       por defecto es: BIF_RETURNONLYFSDIRS
    '   El Caption de la ventana
    '
    Dim iNull As Integer
    Dim lpIDList As Long
    Dim lResult As Long
    Dim sPath As String
    Dim udtBI As BrowseInfo
    Dim sCaption As String
    Dim ador As ador.Recordset

    On Local Error Resume Next

    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(OTROS, g_udtParametrosGenerales.g_sIdioma, 1)
    If Not ador.EOF Then sCaption = ador(0).Value
    ador.Close

    With udtBI
        .hWndOwner = hWndOwner
        ' T�tulo a mostrar encima del �rbol de selecci�n
        .lpszTitle = sPrompt & vbNullChar
        ' Que es lo que debe devolver esta funci�n
        .ulFlags = lFlags
        '
        ' Asignar el caption de la ventana
        sBFFCaption = sCaption
        '
        ' Asignar la variable que contendr� el directorio de inicio
        sFolderIni = sInitDir
        '
        ' Indicar la funci�n Callback a usar.
        '   Nota:   Esto s�lo es necesario si se quiere cambiar el caption
        '           y especificar el directorio de inicio.
        '
        ' Como hay que asignar esa direcci�n a una variable,
        ' se usa una funci�n "intermedia" que devuelve el valor
        ' del par�metro pasado... es decir: �la direcci�n de la funci�n!
        .lpfnCallback = rtnAddressOf(AddressOf BrowseFolderCallbackProc)
    End With
    Err = 0
    On Local Error GoTo 0

    ' Mostramos el cuadro de di�logo
    lpIDList = SHBrowseForFolder(udtBI)
    '
    If lpIDList Then
        ' Si se ha seleccionado un directorio...
        '
        ' Obtener el path
        sPath = String$(MAX_PATH, 0)
        lResult = SHGetPathFromIDList(lpIDList, sPath)
        'Call CoTaskMemFree(lpIDList)
        ' Quitar los caracteres nulos del final
        iNull = InStr(sPath, vbNullChar)
        If iNull Then
            sPath = Left$(sPath, iNull - 1)
        End If
    Else
        ' Si se ha pulsado en cancelar...
        '
        ' Devolver una cadena vac�a y asignar un error
        sPath = ""
    End If

    BrowseForFolder = sPath
End Function





Public Function rtnAddressOf(lngProc As Long) As Long
    ' Devuelve la direcci�n pasada como par�metro
    ' Esto se usar� para asignar a una variable la direcci�n de una funci�n
    ' o procedimiento.
    ' Por ejemplo, si en un tipo definido se asigna a una variable la direcci�n
    ' de una funci�n o procedimiento
    rtnAddressOf = lngProc
End Function

