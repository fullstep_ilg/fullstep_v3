VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRMonedas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Public Sub ListadoGeneral(oReport As CRAXDRT.Report, OrdenarPorCod As Boolean, OrdenarPorDen As Boolean)

    Dim Table As CRAXDRT.DatabaseTable
    Dim RecordSortFields As String
    'Dim sSQL As String
    'Dim sRecordSelectionFormula As String
    Dim GroupOrder1 As String
    
    If crs_Connected = False Then
        Set oReport = Nothing
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    'sRecordSelectionFormula = ""
    'sRecordSelectionFormula = "CDate({VOLADJ.FECADJ}) >= CDate('" & DblQuote(FecDesde) & "')"
    'sRecordSelectionFormula = sRecordSelectionFormula & " AND CDate({VOLADJ.FECADJ}) <= CDate('" & DblQuote(FecHasta) & "')"
    'oReport.RecordSelectionFormula = sRecordSelectionFormula
    
    
    'Orden del informe
    If OrdenarPorCod Then   'ordenado por c�digo
        RecordSortFields = "+{MON.COD}"
        GroupOrder1 = "{MON.COD}"
    End If
    
    If OrdenarPorDen = True Then   'ordenado por denominaci�n
        RecordSortFields = "+{MON.DEN_" & g_udtParametrosGenerales.g_sIdioma & "}"
        GroupOrder1 = "{MON.DEN" & g_udtParametrosGenerales.g_sIdioma & "}"
    End If

    While oReport.RecordSortFields.Count > 0
        oReport.RecordSortFields.Delete 1
    Wend
    
    If OrdenarPorCod Or OrdenarPorDen Then
        'oReport.GroupSortFields.Delete 1
        'oReport.FormulaFields(crs_FormulaIndex(oReport, "ORDEN_GRUPO")).Text = GroupOrder1
        oReport.RecordSortFields.Add oReport.Database.Tables(crs_Tableindex(oReport, crs_SortTable(RecordSortFields))).Fields(crs_FieldIndex(oReport, crs_SortTable(RecordSortFields), crs_SortField(RecordSortFields))), crs_SortDirection(RecordSortFields)
    End If

    
End Sub
