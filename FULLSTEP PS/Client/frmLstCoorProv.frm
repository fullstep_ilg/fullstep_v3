VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstCoorProv 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Coordinaci�n de Provincias"
   ClientHeight    =   3480
   ClientLeft      =   585
   ClientTop       =   2010
   ClientWidth     =   5625
   Icon            =   "frmLstCoorProv.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Picture         =   "frmLstCoorProv.frx":0CB2
   ScaleHeight     =   3480
   ScaleMode       =   0  'User
   ScaleWidth      =   5550.792
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4620
      TabIndex        =   11
      Top             =   3105
      Width           =   1005
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   3030
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   5640
      _ExtentX        =   9948
      _ExtentY        =   5345
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Opciones"
      TabPicture(0)   =   "frmLstCoorProv.frx":11F4
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      Begin VB.Frame Frame3 
         Caption         =   "Compa��a"
         Height          =   675
         Left            =   90
         TabIndex        =   0
         Top             =   360
         Width           =   5445
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
            Height          =   285
            Left            =   315
            TabIndex        =   1
            Top             =   240
            Width           =   1695
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   7064
            Columns(2).Caption=   "Denominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   2990
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
            Height          =   285
            Left            =   2055
            TabIndex        =   2
            Top             =   240
            Width           =   3225
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5980
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   5689
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Provincias"
         Height          =   1110
         Left            =   90
         TabIndex        =   3
         Top             =   1020
         Width           =   5445
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
            Height          =   285
            Left            =   1140
            TabIndex        =   6
            Top             =   660
            Width           =   885
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1561
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.OptionButton opTodos 
            Caption         =   "De todos los pa�ses"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   165
            TabIndex        =   4
            Top             =   330
            Value           =   -1  'True
            Width           =   2715
         End
         Begin VB.OptionButton opPais 
            Caption         =   "Pa�s:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   165
            TabIndex        =   5
            Top             =   660
            Width           =   945
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
            Height          =   285
            Left            =   2055
            TabIndex        =   7
            Top             =   660
            Width           =   3225
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5689
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Listado de Coordinacion de Pa�ses"
         Height          =   975
         Left            =   -74895
         TabIndex        =   13
         Top             =   420
         Width           =   4575
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   15
            Top             =   600
            Width           =   3750
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   14
            Top             =   240
            Value           =   -1  'True
            Width           =   3840
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Orden"
         Height          =   735
         Left            =   90
         TabIndex        =   8
         Top             =   2160
         Width           =   5445
         Begin VB.OptionButton opOrdDenF 
            Caption         =   "Denom. FULLSTEP"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2850
            TabIndex        =   10
            Top             =   330
            Width           =   2085
         End
         Begin VB.OptionButton opOrdCodF 
            Caption         =   "C�digo FULLSTEP"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   315
            TabIndex        =   9
            Top             =   330
            Value           =   -1  'True
            Width           =   1635
         End
      End
   End
End
Attribute VB_Name = "frmLstCoorProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Public oCia As CCia
Private oCias As CCias
Private CodPaiSeleccionado As String
Private IdPaiPortalSeleccionado As Integer
Private CodPaiPortalSeleccionado As String
Private sTitulo As String  'preview
'Private FormulaRpt(1 To 2, 1 To 5) As String      'formulas textos RPT
Private IdCiaSeleccionada As Long
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean
Public g_adores_Ador As Ador.Recordset

Private sIdiCompania As String

Private sIdiPais As String

Private sIdiTxtTitulo As String
Private sIdiTxtCodPaisGS As String
Private sIdiTxtDenPaisGS As String
Private sIdiTxtCodPaisP As String
Private sIdiTxtDenPaisP As String
Private sIdiTxtProvi As String
Private sIdiTxtCodGS As String
Private sIdiTxtDenGS As String
Private sIdiTxtCodP As String
Private sIdiTxtDenP As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String



Private Sub cmdObtener_Click()
If oCia Is Nothing Then
        basMensajes.NoValida sIdiCompania
        Exit Sub
End If
oCia.Id = IdCiaSeleccionada
If Not oCia.ComprobarConectividadConCia Then
        basMensajes.ConexionIncorrecta
        Exit Sub
End If
''' * Objetivo: Obtener un listado de Coordinaci�n de Provincias de un pa�s o todos los Pa�ses
ObtenerInforme
End Sub


Private Sub Form_Load()

    Me.Width = 5745
    Me.Height = 3885
    
    CargarRecursos
    Set oCias = g_oRaiz.Generar_CCias
   
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCia = Nothing
    Set oCias = Nothing
End Sub



Private Sub sdbcPaiCod_DropDown()

    Dim i As Integer
    Dim Ador As Ador.Recordset
    
    sdbcPaiCod.RemoveAll
    
    If oCia Is Nothing Then Exit Sub
    
    If bCargarComboDesde Then
        Set Ador = oCia.DevolverPaisesDeLaCiaDesde(g_iCargaMaximaCombos, Trim(sdbcPaiCod.Text), , False, False)
    Else
        Set Ador = oCia.DevolverPaisesDeLaCiaDesde(g_iCargaMaximaCombos, , , False, False)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcPaiCod.AddItem Ador("COD").Value & Chr(9) & Ador("DEN").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcPaiCod.AddItem "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    opPais = True
    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
End Sub

Private Sub sdbcPaiCod_InitColumnProps()
    
    sdbcPaiCod.DataField = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcPaiCod_Change()

    opPais.Value = True
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcPaiDen.Text = ""
        bRespetarCombo = False
        
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcPaiCod_Click()
    
    If Not sdbcPaiCod.DroppedDown Then
        sdbcPaiCod = ""
        sdbcPaiDen = ""
    End If
End Sub

Private Sub sdbcPaiCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiCod.Rows - 1
            bm = sdbcPaiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcPaiCod_CloseUp()

    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
End Sub

Public Sub sdbcPaiCod_Validate(Cancel As Boolean)
Dim Ador As Ador.Recordset

    If Trim(sdbcPaiCod.Text) = "" Then Exit Sub
    
    If oCia Is Nothing Then Exit Sub
    
    'seguimos si tenemos un codigo de pa�s y
    'un codigo de empresa
    
    Set Ador = oCia.DevolverPaisesDeLaCiaDesde(1, sdbcPaiCod.Text, , True)
    
    If Ador Is Nothing Then
        
        sdbcPaiCod = ""
        Exit Sub
        
    Else
        If Ador.EOF Then
            basMensajes.NoValido sIdiPais
            sdbcPaiCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcPaiCod.Text) <> UCase(Ador("COD").Value) Then
                basMensajes.NoValido sIdiPais
                sdbcPaiCod.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcPaiDen.Text = Ador("DEN").Value
                sdbcPaiDen.Columns("COD").Value = Ador("COD").Value
                sdbcPaiDen.Columns("DEN").Value = Ador("DEN").Value
                bRespetarCombo = False
                bCargarComboDesde = False
                
            End If
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    
End Sub

Private Sub sdbcPaiDen_DropDown()

    Dim i As Integer
    Dim Ador As Ador.Recordset
    
    If oCia Is Nothing Then Exit Sub
    
    sdbcPaiDen.RemoveAll

    If bCargarComboDesde Then
        Set Ador = oCia.DevolverPaisesDeLaCiaDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcPaiDen.Text), , True)
    Else
        Set Ador = oCia.DevolverPaisesDeLaCiaDesde(basParametros.g_iCargaMaximaCombos, , , False, True)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcPaiDen.AddItem Ador("DEN").Value & Chr(9) & Ador("COD").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcPaiDen.AddItem "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiDen.Refresh
    
End Sub
Private Sub sdbcPaiDen_InitColumnProps()
    
    sdbcPaiDen.DataField = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcPaiDen_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcPaiCod.Text = ""
        CodPaiSeleccionado = -1
        IdPaiPortalSeleccionado = -1
        bRespetarCombo = False
        bCargarComboDesde = True
       ' sdbgTabla.RemoveAll
    End If
    
End Sub

Private Sub sdbcPaiDen_Click()
    
    If Not sdbcPaiDen.DroppedDown Then
        sdbcPaiDen = ""
        sdbcPaiDen = ""
    End If
End Sub


Private Sub sdbcPaiDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPaiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPaiDen.Rows - 1
            bm = sdbcPaiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiDen.Columns("DEN").CellText(bm), 1, Len(Text))) Then
                sdbcPaiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcPaiDen_CloseUp()

    If sdbcPaiDen.Text = "-1" Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcPaiDen.Text = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiDen.Columns("DEN").Text
    sdbcPaiCod.Text = sdbcPaiDen.Columns("COD").Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    CodPaiSeleccionado = sdbcPaiDen.Columns("COD").Value
        
End Sub
Private Sub sdbcCiaCod_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        IdCiaSeleccionada = -1
        bRespetarCombo = False
        bCargarComboDesde = True
        Set oCia = Nothing
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
    IdCiaSeleccionada = sdbcCiaCod.Columns(0).Value
    Set oCia = g_oRaiz.Generar_CCia
    oCia.Id = IdCiaSeleccionada
    
    
    
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaCod.RemoveAll
  

    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, True)
    Else
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, True)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("FCEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    
End Sub

Public Sub sdbcCiaCod_Validate(Cancel As Boolean)

    Dim Ador As Ador.Recordset

    If sdbcCiaCod.Text = "" Then
        Exit Sub
    End If
    
    Set Ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , True)
    
    If Ador.RecordCount = 0 Then
        Set oCia = Nothing
        sdbcCiaCod.Text = ""
        sdbcCiaDen.Text = ""
        Exit Sub
        
    Else
        bRespetarCombo = True
        sdbcCiaDen.Text = Ador("DEN").Value
        bRespetarCombo = False
        bCargarComboDesde = False
        IdCiaSeleccionada = Ador("ID").Value
        Set oCia = g_oRaiz.Generar_CCia
        oCia.Id = IdCiaSeleccionada
    End If
    Ador.Close
    Set Ador = Nothing

        
End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaDen.RemoveAll

    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, True)
    Else
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, True)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("FCEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaCod.Text = ""
        IdCiaSeleccionada = -1
        bRespetarCombo = False
        bCargarComboDesde = True
        Set oCia = Nothing
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(2).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    bRespetarCombo = False
        
    bCargarComboDesde = False
    
    IdCiaSeleccionada = sdbcCiaDen.Columns(0).Value
    Set oCia = g_oRaiz.Generar_CCia
    oCia.Id = IdCiaSeleccionada

    
End Sub




Private Sub sdbcCiaDen_Validate(Cancel As Boolean)
    
    Dim Ador As Ador.Recordset

    If sdbcCiaDen.Text = "" Then
        Exit Sub
    End If
    
    Set Ador = oCias.DevolverCiasDesde(1, , sdbcCiaDen.Text, True, , True)
    
    If Ador.RecordCount = 0 Then
        Set oCia = Nothing
        sdbcCiaCod.Text = ""
        sdbcCiaDen.Text = ""
        Exit Sub
        
    Else
        bRespetarCombo = True
        sdbcCiaCod.Text = Ador("COD").Value
        bRespetarCombo = False
        bCargarComboDesde = False
        IdCiaSeleccionada = Ador("ID").Value
        Set oCia = g_oRaiz.Generar_CCia
        oCia.Id = IdCiaSeleccionada
    End If
    Ador.Close
    Set Ador = Nothing

End Sub

Private Sub ObtenerInforme()
    Dim RecordSortFields(1 To 1) As String
    Dim oReport As CRAXDRT.Report
    Dim pv As Preview
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim intOrden As Integer
    Dim i As Integer
    Dim oCia As CCia
    Dim Cod As String
    Dim Den As String
       
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If sdbcCiaDen.Text = "" And opTodos.Value = False Then
        basMensajes.NoValida sIdiCompania
        Exit Sub
    End If
    
   ' If Not oCiaSeleccionada.ComprobarConectividadConCia Then
    '    basMensajes.ConexionIncorrecta
    '    Exit Sub
    'End If
    
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptcoorprovi.rpt"
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
   
    If sdbcCiaCod.Text = "" Then
       basMensajes.NoValida sIdiCompania
        Exit Sub
    End If
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = "'" & sIdiTxtTitulo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodPaiGS")).Text = "'" & sIdiTxtCodPaisGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenPaisGS")).Text = "'" & sIdiTxtDenPaisGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodPaisP")).Text = "'" & sIdiTxtCodPaisP & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenPaisP")).Text = "'" & sIdiTxtDenPaisP & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProvi")).Text = "'" & sIdiTxtProvi & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodGS")).Text = "'" & sIdiTxtCodGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenGS")).Text = "'" & sIdiTxtDenGS & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodP")).Text = "'" & sIdiTxtCodP & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenP")).Text = "'" & sIdiTxtDenP & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"
    
    Screen.MousePointer = vbHourglass
    
'Orden del informe
    
    Set oCia = g_oRaiz.Generar_CCia
    oCia.Id = IdCiaSeleccionada
    Cod = sdbcPaiCod.Text
    Den = sdbcPaiDen.Text
   ' oCia.CodPai = sdbcPaiCod.Columns(1).Value
   ' Set Ador = oCia.DevolverPaisesDeLaCiaDesde(32000, , , , opOrdDenF)
    Set g_adores_Ador = oCia.DevolverPaisesConProvinciasEnCia(32000, Cod, Den, , opOrdDenF)
    oReport.Database.SetDataSource g_adores_Ador
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set pv = New Preview
    pv.g_sOrigen = "frmLstCoorProv"
    pv.Hide
    pv.Caption = sTitulo 'Listado de coordinaci�n de Provincias por Pa�s/es
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    Unload Me
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTCOORPROV, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        sIdiTxtTitulo = Ador(0).Value
        Frame1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Frame2.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Frame3.Caption = Ador(0).Value
        sIdiCompania = Ador(0).Value
        Ador.MoveNext
        Me.Frame4.Caption = Ador(0).Value
        sIdiTxtProvi = Ador(0).Value
        Ador.MoveNext
        Me.opOrdCod(0).Caption = Ador(0).Value
        Me.sdbcCiaCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = Ador(0).Value
        Me.sdbcPaiCod.Columns(0).Caption = Ador(0).Value
        Me.sdbcPaiDen.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.opOrdCodF.Caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = Ador(0).Value
        Me.sdbcPaiCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcPaiDen.Columns(0).Caption = Ador(0).Value
        Me.opOrdDen.Caption = Ador(0).Value
        Ador.MoveNext
        Me.opOrdDenF.Caption = Ador(0).Value
        Ador.MoveNext
        Me.opPais.Caption = Ador(0).Value & ":"
        sIdiPais = Ador(0).Value
        Ador.MoveNext
        Me.opTodos.Caption = Ador(0).Value
        Ador.MoveNext
        
        Me.SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        
        sIdiTxtCodPaisGS = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDenPaisGS = Ador(0).Value
        Ador.MoveNext
        sIdiTxtCodPaisP = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDenPaisP = Ador(0).Value
        Ador.MoveNext
        
        sIdiTxtCodGS = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDenGS = Ador(0).Value
        Ador.MoveNext
        sIdiTxtCodP = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDenP = Ador(0).Value
        Ador.MoveNext
        sIdiTxtPag = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDe = Ador(0).Value
        

        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


