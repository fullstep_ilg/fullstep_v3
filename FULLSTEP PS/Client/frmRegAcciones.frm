VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmRegAcciones 
   Caption         =   "DHist�rico de emails enviados"
   ClientHeight    =   7095
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12135
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegAcciones.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   12135
   Begin MSComDlg.CommonDialog cmmExcel 
      Left            =   11520
      Top             =   960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   16
      Top             =   6720
      Width           =   12135
      _ExtentX        =   21405
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin FSPSClient.PaginacionGrid pgRegistro 
      Height          =   375
      Left            =   120
      TabIndex        =   14
      Top             =   1080
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   661
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRegistro 
      Height          =   5580
      Left            =   60
      TabIndex        =   3
      Top             =   1470
      Width           =   12015
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      GroupHeaders    =   0   'False
      Col.Count       =   10
      stylesets.count =   3
      stylesets(0).Name=   "Sobre"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmRegAcciones.frx":0CB2
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmRegAcciones.frx":124C
      stylesets(2).Name=   "Adjunto"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmRegAcciones.frx":1268
      DividerType     =   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   10
      Columns(0).Width=   635
      Columns(0).Name =   "MSG"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "USU"
      Columns(2).Name =   "USU"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "FECHA"
      Columns(3).Name =   "FECHA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ACCION_DEN"
      Columns(4).Name =   "ACCION_DEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "CIA_COD"
      Columns(5).Name =   "CIA_COD"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "CIA_DEN"
      Columns(6).Name =   "CIA_DEN"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "ACCION_DAT"
      Columns(7).Name =   "ACCION_DAT"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "REGMAIL"
      Columns(8).Name =   "REGMAIL"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "CIA_ID"
      Columns(9).Name =   "CIA_ID"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   21193
      _ExtentY        =   9842
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraCias 
      Height          =   975
      Left            =   60
      TabIndex        =   0
      Top             =   0
      Width           =   11985
      Begin VB.CommandButton cmdCargar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   11520
         Picture         =   "frmRegAcciones.frx":15BA
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdListado 
         Height          =   285
         Left            =   11520
         Picture         =   "frmRegAcciones.frx":1645
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   600
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.TextBox txtFecDesde 
         Height          =   285
         Left            =   4800
         TabIndex        =   7
         Top             =   240
         Width           =   1035
      End
      Begin VB.CommandButton cmdFecDesde 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5880
         Picture         =   "frmRegAcciones.frx":1B57
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtFecHasta 
         Height          =   285
         Left            =   7910
         TabIndex        =   5
         Top             =   240
         Width           =   1035
      End
      Begin VB.CommandButton cmdFecHasta 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9050
         Picture         =   "frmRegAcciones.frx":1E69
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   9050
         Picture         =   "frmRegAcciones.frx":217B
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   600
         Width           =   300
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   3725
         TabIndex        =   2
         Top             =   600
         Width           =   5220
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5980
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   9208
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   1350
         TabIndex        =   10
         Top             =   600
         Width           =   1940
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   7064
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   3422
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcUsu 
         Height          =   285
         Left            =   1350
         TabIndex        =   11
         Top             =   240
         Width           =   1935
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   6773
         Columns(0).Caption=   "USU"
         Columns(0).Name =   "USU"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3422
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin FSPSClient.CiaSelector CiaSelector1 
         Height          =   285
         Left            =   3360
         TabIndex        =   15
         Top             =   600
         Width           =   300
         _ExtentX        =   529
         _ExtentY        =   503
      End
      Begin VB.Label lblUsu 
         Caption         =   "Usuario:"
         Height          =   195
         Left            =   200
         TabIndex        =   13
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblCia 
         Caption         =   "Compa��a:"
         Height          =   195
         Left            =   200
         TabIndex        =   12
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label lblFechaDesde 
         Caption         =   "Fecha desde:"
         Height          =   195
         Left            =   3720
         TabIndex        =   9
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblFecHasta 
         Caption         =   "Fecha hasta:"
         Height          =   195
         Left            =   6840
         TabIndex        =   8
         Top             =   240
         Width           =   1020
      End
   End
End
Attribute VB_Name = "frmRegAcciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnMargen As Long = 60

Public m_bRespetarCombo As Boolean
Public g_oCiaSeleccionada As CCia

Private m_oRegistros As CGestorAcciones
Private m_bCargarComboDesde As Boolean
Private m_oCias As CCias

Private sIdiAbrevPend As String
Private sIdiAbrevTodas As String
Private sIdiOrdenando As String

Private sIdiCol(6) As String




Private m_iOrden As Integer

Private Sub CiaSelector1_Click(ByVal opcion As PSSeleccion)
    sdbcCiaCod = ""
End Sub

Private Sub cmdBuscar_Click()
    frmCiaBuscar.g_sOrigen = "frmRegAcciones"
    frmCiaBuscar.WindowState = vbNormal
    frmCiaBuscar.Show 1
    
    'Actualizo el bot�n del ciaselector
    CiaSelector1.Seleccion = PSSeleccion.PSTodos
End Sub

Private Sub cmdCargar_Click()
    CargarHistorico 1, True, m_iOrden
End Sub

Private Sub cmdFecDesde_Click()
    Set frmCalendar.frmDestination = frmRegAcciones
    Set frmCalendar.ctrDestination = txtFecDesde
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde.Text <> "" Then
        frmCalendar.Calendar.Value = txtFecDesde.Text
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Sub cmdFecHasta_Click()
    Set frmCalendar.frmDestination = frmRegAcciones
    Set frmCalendar.ctrDestination = txtFecHasta
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecHasta.Text <> "" Then
        frmCalendar.Calendar.Value = txtFecHasta.Text
    Else
        frmCalendar.Calendar.ValueIsNull = True
    End If
    
    frmCalendar.Show 1
End Sub

Private Function GenerarHojaExcel(Docexcel As String) As Boolean
    Dim sConnect As String
    Dim oExcelAdoConn As adodb.Connection
    Dim oExcelAdoRS As adodb.Recordset
    Dim SQL As String
    Dim i As Integer
    Dim dtFecha As Date
    
        If g_oCiaSeleccionada Is Nothing Then
            Set oExcelAdoRS = m_oRegistros.CargarAccionesDesdeConPaginacion(0, False, 0, , txtFecDesde.Text, txtFecHasta.Text, 1, sdbcUsu.Text)
        Else
            Set oExcelAdoRS = m_oRegistros.CargarAccionesDesdeConPaginacion(0, False, 0, g_oCiaSeleccionada.Id, txtFecDesde.Text, txtFecHasta.Text, 1, sdbcUsu.Text)
        End If
        
    Set oExcelAdoConn = New adodb.Connection
    sConnect = "Provider=MSDASQL.1;Extended Properties=""DBQ=" & Docexcel & ";Driver={Microsoft Excel Driver (*.xls)};" _
             & "FIL=excel 8.0;ReadOnly=0;UID=admin;"""
            
    oExcelAdoConn.Open sConnect
    
    'Columnas fijas!!
    SQL = "CREATE TABLE [Log$]  ([" & sIdiCol(0) & "] memo,[" & sIdiCol(1) & "] memo, [" & sIdiCol(2) & "] memo, [" & sIdiCol(3) & "] memo,[" & sIdiCol(4) & "] memo,[" & sIdiCol(5) & "] memo)"
    oExcelAdoConn.Execute SQL
    
    While Not oExcelAdoRS.EOF
        dtFecha = CDate(Left(oExcelAdoRS("FECHA").Value, Len(oExcelAdoRS("FECHA").Value) - 4))
    
        SQL = "INSERT INTO [Log$] values ('" & oExcelAdoRS("USU").Value & "','" & dtFecha & "','" & oExcelAdoRS("ACCION_DEN").Value & "'"
        SQL = SQL & ",'" & oExcelAdoRS("CIA_COD").Value & "','" & oExcelAdoRS("CIA_DEN").Value & "'"
        SQL = SQL & ",'" & oExcelAdoRS("ACCION_DAT").Value & "')"
       
        oExcelAdoConn.Execute SQL
        oExcelAdoRS.MoveNext
    Wend
       
    GenerarHojaExcel = True
    
    Exit Function
       
ERROR_Frm:
    Select Case Err.Number
        Case 9:
            Resume 0
        
        Case 1004:
            Resume Next
        
        Case Else:
            MsgBox Err.Description
            If Err.Number = 7 Then
                GenerarHojaExcel = False
                Exit Function
            End If
    End Select
 Exit Function
End Function

Private Sub cmdListado_Click()
    
   Dim sTemp As String
    Dim fso As Scripting.FileSystemObject
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim oFile As Scripting.File
        Dim sFileName As String
    Dim sFileTitle As String
    
        On Error GoTo Cancelar:

    'Comprueba que el el path sea correcto y que exista el informe
    
    RepPath = Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, "") & "\infRegLog.xls"
    sTemp = DevolverPathFichTemp & "infRegLog.xls"

    Set fso = New Scripting.FileSystemObject
    fso.CopyFile RepPath, sTemp
    Set oFile = fso.GetFile(sTemp)
    If oFile.Attributes And 1 Then
        oFile.Attributes = oFile.Attributes Xor 1
    End If
    Set oFile = Nothing
    
    If GenerarHojaExcel(sTemp) Then
        cmmExcel.CancelError = True
               
        cmmExcel.DialogTitle = Me.Caption
        cmmExcel.Filter = "Excel |*.xls"
        cmmExcel.FileName = "RegLog.xls"
        
        cmmExcel.ShowSave
                
      
        sFileName = cmmExcel.FileName
        sFileTitle = cmmExcel.FileTitle
        If sFileTitle = "" Then
            'oMensajes.NoValido "File" 'm_sIdiArchivo
            Exit Sub
        End If
            
        fso.CopyFile sTemp, sFileName
        Set fso = Nothing
       
    End If
    
Cancelar:

    If Err.Number <> 32755 Then '32755 = han pulsado cancel

    End If

End Sub
    
    
Private Sub Form_Load()
   
    Me.Width = 12255
    Me.Height = 7605

    CargarRecursos

    cmdListado.Enabled = False
    
    Set m_oCias = g_oRaiz.Generar_CCias
    
    CiaSelector1.AbrevParaPendientes = sIdiAbrevPend
    CiaSelector1.AbrevParaTodos = sIdiAbrevTodas
    
    m_iOrden = 0
    
    CargarFiltrosDefecto
    CargarHistorico 1, True
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>Dimensiona y coloca los controles en pantalla</summary>
''' <remarks>Llamada desde Form_Resize</remarks>

Private Sub Arrange()
    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 400 Then Exit Sub
    
    fraCias.Left = cnMargen
    fraCias.Width = Me.ScaleWidth - (2 * cnMargen)
    
    cmdCargar.Left = fraCias.Width - cmdCargar.Width - (2 * cnMargen)
    
    cmdListado.Left = cmdCargar.Left
    
    pgRegistro.Left = cnMargen
    
    ArrangeGrid
    
    sdbgRegistro.Columns("FECHA").Width = sdbgRegistro.Width * 0.1
    sdbgRegistro.Columns("CIA_COD").Width = sdbgRegistro.Width * 0.1
    sdbgRegistro.Columns("CIA_DEN").Width = sdbgRegistro.Width * 0.2
    sdbgRegistro.Columns("USU").Width = sdbgRegistro.Width * 0.1
    sdbgRegistro.Columns("ACCION_DEN").Width = sdbgRegistro.Width * 0.15
    sdbgRegistro.Columns("ACCION_DAT").Width = sdbgRegistro.Width * 0.5
    
End Sub

''' <summary>Dimensiona y coloca en pantalla el grid</summary>
''' <remarks>Llamada desde Arrange y CargarHistorico</remarks>

Private Sub ArrangeGrid()
    sdbgRegistro.Top = IIf(pgRegistro.Visible, pgRegistro.Top + pgRegistro.Height + cnMargen, pgRegistro.Top)
    sdbgRegistro.Height = Me.ScaleHeight - IIf(pgRegistro.Visible, (pgRegistro.Top + pgRegistro.Height), (fraCias.Top + fraCias.Height)) - cnMargen
    sdbgRegistro.Left = cnMargen
    sdbgRegistro.Width = Me.ScaleWidth - (2 * cnMargen)
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_REGACCION, g_udtParametrosGenerales.g_sIdioma)
    
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        lblCia.Caption = ador(0).Value
        ador.MoveNext
        lblUsu.Caption = ador(0).Value
        sdbcUsu.Columns("USU").Caption = ador(0).Value
        sdbgRegistro.Columns("USU").Caption = ador(0).Value
        sIdiCol(0) = ador(0).Value
        
        ador.MoveNext
        lblFechaDesde.Caption = ador(0).Value & ":"
        ador.MoveNext
        lblFecHasta.Caption = ador(0).Value & ":"
        ador.MoveNext
        sIdiAbrevPend = ador(0).Value 'P
        ador.MoveNext
        sIdiAbrevTodas = ador(0).Value 'T
        ador.MoveNext
        sIdiOrdenando = ador(0).Value
        ador.MoveNext
        pgRegistro.PageCaption = ador(0).Value
        ador.MoveNext
        sdbcCiaCod.Columns("COD").Caption = ador(0).Value
        sdbcCiaDen.Columns("COD").Caption = ador(0).Value
        
        ador.MoveNext
        sdbcCiaCod.Columns("DEN").Caption = ador(0).Value
        sdbcCiaDen.Columns("DEN").Caption = ador(0).Value
        ador.MoveNext
        sdbgRegistro.Columns("FECHA").Caption = ador(0).Value
        sIdiCol(1) = ador(0).Value
                
        ador.MoveNext
        sdbgRegistro.Columns("CIA_COD").Caption = ador(0).Value
        sIdiCol(2) = ador(0).Value
                
        ador.MoveNext
        sdbgRegistro.Columns("CIA_DEN").Caption = ador(0).Value
        sIdiCol(3) = ador(0).Value
        ador.MoveNext
        
        sdbgRegistro.Columns("ACCION_DEN").Caption = ador(0).Value
        sIdiCol(4) = ador(0).Value
        ador.MoveNext
        sdbgRegistro.Columns("ACCION_DAT").Caption = ador(0).Value
        sIdiCol(5) = ador(0).Value
        ador.Close
    End If

   Set ador = Nothing

End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set m_oRegistros = Nothing
    Set g_oCiaSeleccionada = Nothing
    Set m_oCias = Nothing
End Sub

Private Sub pgRegistro_OnFirstClick()
    CargarHistorico 1, True, m_iOrden
End Sub

Private Sub pgRegistro_OnLastClick()
    CargarHistorico pgRegistro.NumPaginas, True, m_iOrden
End Sub

Private Sub pgRegistro_OnNextClick(ByVal iPaginaActual As Integer)
    CargarHistorico iPaginaActual, True, m_iOrden
End Sub

Private Sub pgRegistro_OnPreviousClick(ByVal iPaginaActual As Integer)
    CargarHistorico iPaginaActual, True, m_iOrden
End Sub



Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim ador As ador.Recordset
    
    sdbcCiaCod.RemoveAll
    Set g_oCiaSeleccionada = Nothing
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, , , , , , , , , , , , , , 4)
            Else
                Set ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, , , , , , , , , , , , , , 4)
            End If
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasPendientesDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False)
            Else
                Set ador = m_oCias.DevolverCiasPendientesDesde(g_iCargaMaximaCombos, , , False, False)
            End If
    End Select
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("FCEST").Value & Chr(9) & ador("FPEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
    
End Sub

Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcCiaCod_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcCiaDen.Text = ""
                
        m_bRespetarCombo = False
        
        m_bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    m_bRespetarCombo = True
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    CiaSeleccionada
    
    
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset

    If Trim(sdbcCiaCod.Text) = "" Then
        Set g_oCiaSeleccionada = Nothing
        Exit Sub
    End If
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            Set ador = m_oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , , , , , , , , , , , , , , 4)
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            Set ador = m_oCias.DevolverCiasPendientesDesde(1, sdbcCiaCod.Text, , True)
    End Select
    
    If ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                m_bRespetarCombo = True
                sdbcCiaDen.Text = ador("DEN").Value
                sdbcCiaCod.Text = ador("COD").Value
                sdbcCiaCod.Columns(0).Value = ador("ID").Value
                CiaSeleccionada
                m_bRespetarCombo = False
                m_bCargarComboDesde = False
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If
    
End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim ador As ador.Recordset
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    Set g_oCiaSeleccionada = Nothing
    
    Select Case CiaSelector1.Seleccion
        Case PSSeleccion.PSTodos
            'Carga todos
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, , , , , , , , , , , , , , 4)
            Else
                Set ador = m_oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, , , , , , , , , , , , , , 4)
            End If
            
        Case PSSeleccion.PSPendientes
            'Carga pendientes
            If m_bCargarComboDesde Then
                Set ador = m_oCias.DevolverCiasPendientesDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True)
            Else
                Set ador = m_oCias.DevolverCiasPendientesDesde(basParametros.g_iCargaMaximaCombos, , , False, True)
            End If
    End Select
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("FCEST").Value & Chr(9) & ador("FPEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcCiaCod.Text = ""
        
        m_bRespetarCombo = False
        m_bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    CiaSeleccionada
    
End Sub

Public Sub CiaSeleccionada()
    If Not sdbcCiaCod.Text = "" Then
        m_oCias.CargarTodasLasCiasDesde 1, sdbcCiaCod.Columns(0).Value, , , True
          
        Set g_oCiaSeleccionada = Nothing
        Set g_oCiaSeleccionada = m_oCias.Item(CStr(sdbcCiaCod.Columns(0).Value))
    End If
    
    m_bRespetarCombo = False
End Sub

''' <summary>Carga los filtros por defecto de la pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub CargarFiltrosDefecto()
    txtFecHasta.Text = Date
    txtFecDesde.Text = DateAdd("d", -6, Date)
End Sub

Private Sub CargarHistorico(ByVal iNumPagCargar As Integer, ByVal bPaginacionSig As Boolean, Optional ByVal iOrden As Integer)
    'Carga las comunicaciones asociadas a la cia seleccionada
    Dim ador As ador.Recordset
    Dim i As Long
    Dim iNumPag As Long
    Dim dtFecha As Date
    
    If ComprobarFiltros Then
        sdbgRegistro.RemoveAll
        
        If m_oRegistros Is Nothing Then Set m_oRegistros = g_oRaiz.Generar_CGestorAcciones
        
        If g_oCiaSeleccionada Is Nothing Then
            Set ador = m_oRegistros.CargarAccionesDesdeConPaginacion(iNumPagCargar, bPaginacionSig, iNumPag, , txtFecDesde.Text, txtFecHasta.Text, iOrden, sdbcUsu.Text)
        Else
            Set ador = m_oRegistros.CargarAccionesDesdeConPaginacion(iNumPagCargar, bPaginacionSig, iNumPag, g_oCiaSeleccionada.Id, txtFecDesde.Text, txtFecHasta.Text, iOrden, sdbcUsu.Text)
        End If
        
        pgRegistro.PaginaActual = iNumPagCargar
        pgRegistro.NumPaginas = iNumPag
        pgRegistro.Visible = (iNumPag > 1)
        ArrangeGrid
        
        DoEvents
                
        If Not ador Is Nothing Then
            'Cargar el grid
            While Not ador.EOF
                dtFecha = CDate(Left(ador("FECHA").Value, Len(ador("FECHA").Value) - 4))
                sdbgRegistro.AddItem "" & Chr(9) & ador("ID") & Chr(9) & ador("USU") & Chr(9) & dtFecha & Chr(9) & ador("ACCION_DEN") & Chr(9) & ador("CIA_COD").Value & Chr(9) & ador("CIA_DEN").Value & Chr(9) & ador("ACCION_DAT").Value & Chr(9) & ador("REGMAIL").Value & Chr(9) & ador("CIA_ID").Value
                
                ador.MoveNext
            Wend
        End If
        Set ador = Nothing
        
        If sdbgRegistro.Rows = 0 Then
            cmdListado.Enabled = False
        Else
            cmdListado.Enabled = True
        End If
        
        If Me.sdbgRegistro.Visible = True Then
            sdbgRegistro.SetFocus
        End If
    End If
End Sub

Private Function ComprobarFiltros() As Boolean
    ComprobarFiltros = True
    
    If Trim(txtFecDesde.Text) <> "" Then
        If Not IsDate(txtFecDesde.Text) Then
            basMensajes.NoValido Left(lblFechaDesde.Caption, Len(lblFechaDesde.Caption) - 1)
            ComprobarFiltros = False
            
            txtFecDesde.SetFocus
            Exit Function
        End If
    End If
    
    If Trim(txtFecHasta.Text) <> "" Then
        If Not IsDate(txtFecHasta.Text) Then
            basMensajes.NoValido Left(lblFecHasta.Caption, Len(lblFecHasta.Caption) - 1)
            ComprobarFiltros = False
            
            txtFecDesde.SetFocus
            Exit Function
        End If
    End If
End Function

Private Sub sdbcCiaDen_Validate(Cancel As Boolean)
    If Trim(sdbcCiaDen.Text) = "" Then
        Set g_oCiaSeleccionada = Nothing
        Exit Sub
    End If
End Sub

Private Sub sdbcUsu_DropDown()
Dim ador As ador.Recordset
    
   
    sdbcUsu.RemoveAll
    
    Dim oUsuario As CUsuario
    Set oUsuario = g_oRaiz.Generar_CUsuario
    Set ador = oUsuario.leer_usuario
    
    If Not ador Is Nothing Then
        
        While Not ador.EOF
        
            sdbcUsu.AddItem ador("USU").Value
            ador.MoveNext
        
        Wend
        
        ador.Close
        Set ador = Nothing
        
    End If

End Sub

Private Sub sdbgRegistro_DblClick()
    'Muestra el detalle del proceso:
    Dim oRegistro As CRegistroEmail
        
    If sdbgRegistro.Columns("CIA_ID").Value <> "" And sdbgRegistro.Columns("REGMAIL").Value <> "" Then
       Set oRegistro = g_oRaiz.Generar_CRegistroEmail
        Set frmMensajeDetalle.g_oRegistro = oRegistro.CargarObjDetalleMensaje(sdbgRegistro.Columns("CIA_ID").Value, sdbgRegistro.Columns("REGMAIL").Value)
        If frmMensajeDetalle.g_oRegistro Is Nothing Then Exit Sub
        frmMensajeDetalle.g_bLock = True
        
        frmMensajeDetalle.Show vbModal
        
        Set oRegistro = Nothing
    End If
    
End Sub

Private Sub sdbgRegistro_HeadClick(ByVal ColIndex As Integer)
    Dim sHeadCaption As String
    Dim bCambioOrden As Boolean
    
    'Ordena el hist�rico por la columna seleccionada
    sHeadCaption = sdbgRegistro.Columns(ColIndex).Caption
    sdbgRegistro.Columns(ColIndex).Caption = sIdiOrdenando
           
    bCambioOrden = False
    If m_iOrden <> ColIndex Then bCambioOrden = True
       
    Select Case ColIndex
        Case 2
            m_iOrden = TipoOrdenAccion.Col_Usu
        Case 3
            m_iOrden = TipoOrdenAccion.col_fecha
        Case 4
            m_iOrden = TipoOrdenAccion.Col_Accion
        Case 5
            m_iOrden = TipoOrdenAccion.Col_CiaCod
        Case 6
            m_iOrden = TipoOrdenAccion.Col_ciaden
        Case 7
            m_iOrden = TipoOrdenAccion.Col_Dato
        Case Else
            m_iOrden = TipoOrdenAccion.col_fecha
    End Select
            
    If bCambioOrden Then
        'Si se cambia el criterio de ordenaci�n cargar la primera p�gina
        pgRegistro.PaginaActual = 1
        CargarHistorico pgRegistro.PaginaActual, True, m_iOrden
    End If
    
    sdbgRegistro.Columns(ColIndex).Caption = sHeadCaption
End Sub

Private Sub sdbgRegistro_RowLoaded(ByVal Bookmark As Variant)
    If sdbgRegistro.Columns("REGMAIL").Value >= "1" And sdbgRegistro.Columns("CIA_ID").Value >= "1" Then
        sdbgRegistro.Columns("MSG").CellStyleSet "Sobre"
    End If
End Sub


