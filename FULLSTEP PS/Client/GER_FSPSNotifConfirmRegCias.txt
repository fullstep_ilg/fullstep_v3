Le notificamos que ha sido confirmada la solicitud de cambio de registro.


Datos de la cia:
  C�digo: @CODCIA
  Denominaci�n: @DENCIA
  NIF de la cia: @NIF
  C.P.: @CP
  Poblaci�n: @POB

  Provincia: 
    C�digo: @CODPROVI
    Denominaci�n: @DENPROVI

  Pa�s:
    C�digo: @CODPAI
    Denominaci�n: @DENPAI

  Direcci�n web de la cia: @URL

  Funci�n compradora autorizada: @FC
  Funci�n proveedora autorizada: @FP
  Funci�n Premium autorizada: @PREMIUM

  Cias compradoras en las que estaba suscrito antes de la solicitud:

@CIAS_ACTUALES

  Cias compradoras en las que est� suscrito tras la solicitud:

@CIAS_NUEVAS

Datos del usuario que realiz� la solicitud de cambio:
  C�digo: @CODUSU
  Nombre: @NOMUSU
  Apellidos: @APEUSU
  Tel�fono 1: @TFNO1USU
  Tel�fono 2: @TFNO2USU
  Tel�fono m�vil: @MOVILUSU
  Fax: @FAXUSU
  E-mail: @EMAILUSU
  Cargo: @CARGOUSU
  Departamento: @DEPUSU


Un saludo


(Si desea recibir este email en HTML acceda al portal y modifique sus preferencias en el apartado de registro de usuario)
