VERSION 5.00
Begin VB.Form frmLogin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inicio de sesi�n"
   ClientHeight    =   2520
   ClientLeft      =   375
   ClientTop       =   2220
   ClientWidth     =   4545
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   2520
   ScaleWidth      =   4545
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      Height          =   1695
      Left            =   210
      ScaleHeight     =   1635
      ScaleWidth      =   4095
      TabIndex        =   4
      Top             =   570
      Width           =   4155
      Begin VB.TextBox txtCOD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1260
         TabIndex        =   0
         Top             =   240
         Width           =   2535
      End
      Begin VB.TextBox txtPWD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1260
         MaxLength       =   100
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   660
         Width           =   2535
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2040
         TabIndex        =   3
         Top             =   1155
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   945
         TabIndex        =   2
         Top             =   1155
         Width           =   1005
      End
      Begin VB.Label Label1 
         BackColor       =   &H00808000&
         Caption         =   "Administrador:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   6
         Top             =   300
         Width           =   1035
      End
      Begin VB.Label Label2 
         BackColor       =   &H00808000&
         Caption         =   "Contrase�a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   180
         TabIndex        =   5
         Top             =   720
         Width           =   975
      End
   End
   Begin VB.Label Label3 
      Caption         =   "Inicio de sesi�n FULLSTEP PS"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00400040&
      Height          =   285
      Left            =   180
      TabIndex        =   7
      Top             =   150
      Width           =   4545
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oUsuario As CUsuario
Private bLogin As Boolean

Private Sub CargarRecursosAcciones()
Dim Ador As Ador.Recordset
Dim i As Integer
'1   antes
'2   Apellidos
'3   Archivo
'4   Autorizada
'5   Autorizado
'6   Cambio de Contrase�a
'7   Cargo
'8   Categor�a laboral
'9   C�a
'10  Cliente de referencia
'11  C�digo Nuevo
'12  Con acceso
'13  CP
'14  Datos
'15  Denominaci�n
'16  Departamento
'17  Desautorizada
'18  Desautorizado
'19  Direcci�n
'20  Email
'21  Fax
'22  Homologaci�n
'23  Idioma
'24  Modificada selecci�n de actividades por usuario
'25  Moneda
'26  NIF
'27  Ninguno
'28  Nombre
'29  Nuevo
'30  Pa�s
'31  Poblaci�n
'32  Portal
'33  Premium activado
'34  Premium antes
'35  Premium desactivado
'36  Acceso a par�metros
'37  Asunto
'38  CodGS
'39  Desde
'40  Filtro
'41  Hasta
'42  Provincia
'43  Sin acceso
'44  Sin marcar
'45  Solicitada
'46  Tel�fono
'47  Tel�fono m�vil
'48  URL
'49  Usuario
'50  Volumen facturaci�n
'51  Modificaci�n de par�metros
On Error Resume Next
''SPA
Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(GESTOR_ACCIONES, "SPA")
If Not Ador Is Nothing Then
   For i = 0 To c_iNumLitRegAccion
       g_sLitRegAccion_SPA(i) = Ador(0).Value
       If Not Ador.EOF Then Ador.MoveNext
   Next i
   Ador.Close
End If
''ENG
Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(GESTOR_ACCIONES, "ENG")
If Not Ador Is Nothing Then
   For i = 0 To c_iNumLitRegAccion
       g_sLitRegAccion_ENG(i) = Ador(0).Value
       If Not Ador.EOF Then Ador.MoveNext
   Next i
   Ador.Close
End If
''FRA
Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(GESTOR_ACCIONES, "FRA")
If Not Ador Is Nothing Then
   For i = 0 To c_iNumLitRegAccion
       g_sLitRegAccion_FRA(i) = Ador(0).Value
       If Not Ador.EOF Then Ador.MoveNext
   Next i
   Ador.Close
End If
''GER
Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(GESTOR_ACCIONES, "GER")
If Not Ador Is Nothing Then
   For i = 0 To c_iNumLitRegAccion
       g_sLitRegAccion_GER(i) = Ador(0).Value
       If Not Ador.EOF Then Ador.MoveNext
   Next i
   Ador.Close
End If
Set Ador = Nothing
End Sub

Private Sub cmdAceptar_Click()
On Error GoTo error:
Set oUsuario = g_oRaiz.Generar_CUsuario
g_sCodADM = txtCod.Text
Select Case oUsuario.ValidarUsuario(txtCod.Text, txtPWD.Text)
    Case 0
        bLogin = True
        CargarRecursosAcciones
        'Registramos la acci�n del login
        g_oGestorAcciones.RegistrarAccion txtCod.Text, TipoAccion.Login_Usu, g_sLitRegAccion_SPA(48) & ": " & txtCod.Text, _
                                                        g_sLitRegAccion_ENG(48) & ": " & txtCod.Text, _
                                                        g_sLitRegAccion_FRA(48) & ": " & txtCod.Text, _
                                                        g_sLitRegAccion_GER(48) & ": " & txtCod.Text
        Unload Me
    Case 1
        basMensajes.UsuarioNoAutorizado
        g_oRaiz.Desconectar
        Set g_oRaiz = Nothing
        Unload Me
        End
    Case 2
        Me.Hide
        frmCambContr.b_PasswordCaducada = True
        frmCambContr.Show vbModal
        CargarRecursosAcciones
        'Registramos la acci�n del login
        g_oGestorAcciones.RegistrarAccion txtCod.Text, TipoAccion.Login_Usu, g_sLitRegAccion_SPA(48) & ": " & txtCod.Text, _
                                                        g_sLitRegAccion_ENG(48) & ": " & txtCod.Text, _
                                                        g_sLitRegAccion_FRA(48) & ": " & txtCod.Text, _
                                                        g_sLitRegAccion_GER(48) & ": " & txtCod.Text
End Select

Exit Sub
error:
    End
End Sub

Private Sub cmdCancelar_Click()
 On Error GoTo error:
 
    g_oRaiz.Desconectar
    Set g_oRaiz = Nothing
    Unload Me
    End
error:
    End
End Sub

Private Sub Form_Activate()
    txtCod.SetFocus
End Sub

Private Sub Form_Load()
    CargarRecursos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'En caso de que no se haya conectado finaliza la sesi�n
    If bLogin = False Then
        End
    End If
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LOGIN, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label1.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label2.Caption = Ador(0).Value
        Ador.MoveNext
        Me.Label3.Caption = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing
End Sub


