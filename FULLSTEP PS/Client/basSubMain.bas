Attribute VB_Name = "basSubMain"

''' <summary>
''' Procedimiento de inicio de Fullstep PS, cargando los parametros generales y
''' en el caso de autenticaci�n windows, el usuario de windows.
''' </summary>
''' <remarks>Llamada desde: Autom�tica, tiempo m�ximo: 1 seg</remarks>
Sub Main()
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim sLicfec As String, sLicLoginEnc As String, sLicContraEnc As String
    Dim sLicLogin As String, sLicContra As String
    Dim iRes As Integer
    Dim oDialogo As CommonDialog

    CargarInstancia
    
    g_udtParametrosGenerales.g_sIdioma = GetStringSetting("FULLSTEP PS", g_sInstancia, "Opciones", "Idioma")
    If g_udtParametrosGenerales.g_sIdioma = "" Then
       g_udtParametrosGenerales.g_sIdioma = "ENG"
    End If
    
    Set g_oGestorIdiomas = New FSPSIdiomas.CGestorIdiomas
    g_oGestorIdiomas.Inicializar
    
   
    
    frmSplash.Show
    DoEvents

    ''' Recogemos los valores de autorizaci�n del servidor de objetos
    Set fil = fso.GetFile(App.Path & "\FSPSOS.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    sLicfec = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    sLicLoginEnc = ts.ReadLine
    sLicContraEnc = ts.ReadLine
    ts.Close
          
    ''' Desencriptamos el usuario y la contrase�a del servidor de objetos
    sLicLogin = basUtilidades.DesEncriptar(sLicLoginEnc, sLicfec)
    sLicContra = basUtilidades.DesEncriptar(sLicContraEnc, sLicfec)
    
    ' Creamos el objeto raiz
    Set g_oRaiz = New CRaiz
    iRes = g_oRaiz.Autentificar(sLicLogin, sLicContra, g_sInstancia, g_sServidor, g_sBaseDeDatos)
    
    Select Case iRes
        Case 1
            basMensajes.ImposibleConectarBD
            End
        Case 2
            basMensajes.ImposibleAutentificar
            End
    End Select
    
    'Nos conectamos a la BD
    iRes = g_oRaiz.Conectar(g_sInstancia, g_sServidor, g_sBaseDeDatos)
    Select Case iRes
        Case 1
            basMensajes.ImposibleConectarBD
            End
    End Select
    ''' Conexi�n con Acciones
    Set g_oGestorAcciones = g_oRaiz.Generar_CGestorAcciones
    If iRes = 0 Then
        frmInitial.Show
        DoEvents
   
        Unload frmSplash
        If Command <> "" Then
            LoginInvisible Command
        Else
            frmLogin.Show 1
        End If
        
        Unload frmInitial
        
        frmEsperaAccion.Show
        DoEvents
      
        ''' Carga de par�metros generales
        Set g_oGestorParametros = g_oRaiz.Generar_CGestorParametros
        g_udtParametrosGenerales = g_oGestorParametros.DevolverParametrosGenerales
        
        If g_udtParametrosGenerales.g_bCollab Then
            If g_oRaiz.InstanciarSPTS Then
                basMensajes.ImposibleConectarAlServidorDeColaboracion
            End If
        End If
        
        CargarParametrosInstalacion
                
        ''' Conexi�n con el servidor de informes
        Set g_oGestorInformes = g_oRaiz.Generar_CGestorInformes
        
        Dim oCRConector As New CRConector
        If Not oCRConector.Conectar(g_sInstancia, g_sServidor, g_sBaseDeDatos) Then
            basMensajes.ImposibleConectarAlServidorDeInformes
        End If
        
        Unload frmEsperaAccion
        
        SaveStringSetting "FULLSTEP PS", g_sInstancia, "Opciones", "Idioma", g_udtParametrosGenerales.g_sIdioma

        frmMDI.Show
    End If
End Sub


''' <summary>
''' Procedimiento que se ejecuta cuando se lanza el PS desde el Launcher. Comprueba que el usuario exista en BD.
''' </summary>
''' <remarks>Llamada desde: Main(), tiempo m�ximo: 1 seg</remarks>
Private Sub LoginInvisible(sCommand As String)
    Dim i As Integer
    Dim PassEncript As String
    Dim oUsuario As CUsuario
    Dim sUsuCod As String
    Dim sUsuPWD As String
    
   On Error GoTo error:
   
    i = InStr(1, sCommand, " ", vbTextCompare)
    
    If i = 0 Then
        sUsuPWD = ""
        sUsuCod = sCommand
    Else
        sUsuCod = Left(sCommand, i - 1)
        sUsuPWD = Right(sCommand, Len(sCommand) - InStr(1, sCommand, " ", vbTextCompare))
    End If
    
    Set oUsuario = g_oRaiz.Generar_CUsuario
    Select Case oUsuario.ValidarUsuario(sUsuCod, sUsuPWD)
        Case 0
        Case 1
            basMensajes.UsuarioNoAutorizado
            g_oRaiz.Desconectar
            Set g_oRaiz = Nothing
            End
        Case 2
            frmCambContr.b_PasswordCaducada = True
            frmCambContr.Show vbModal
    End Select
    
    Set oUsuario = Nothing
Exit Sub

error:

    End
    
End Sub

Private Function CargarInstancia()
Dim oFos As FileSystemObject
Dim ostream As TextStream

    Set oFos = New FileSystemObject
    
    Set ostream = oFos.OpenTextFile(App.Path & "\FSPSClient.ini", ForReading, True)
    
    If Not ostream Is Nothing Then
        If Not ostream.AtEndOfStream Then
            g_sInstancia = ostream.ReadLine
            If InStr(1, g_sInstancia, "INSTANCIA=") Then
                g_sInstancia = Right(g_sInstancia, Len(g_sInstancia) - 10)
                If Not ostream.AtEndOfStream Then
                    g_sServidor = ostream.ReadLine
                    If InStr(1, g_sServidor, "SERVIDOR=") Then
                        g_sServidor = Right(g_sServidor, Len(g_sServidor) - 9)
                        If Not ostream.AtEndOfStream Then
                            g_sBaseDeDatos = ostream.ReadLine
                            If InStr(1, g_sBaseDeDatos, "BASEDATOS=") Then
                                g_sBaseDeDatos = Right(g_sBaseDeDatos, Len(g_sBaseDeDatos) - 10)
                            End If
                        End If
                    End If
                End If
                '10 es el n�mero de letras de 'INSTANCIA=' +1
            Else
                Err.Raise 10000, "FULLSTEP PS", "Instance name missing"
                End
            End If
        Else
            Err.Raise 10000, "FULLSTEP PS", "Instance name missing"
            End
        End If
    End If
    ostream.Close
    Set ostream = Nothing
    
'    If mServidor <> "" And mBaseDeDatos <> "" Then
'        SaveStringSetting "FULLSTEP PS", g_sInstancia, "Conexion", "Servidor", mServidor
'        SaveStringSetting "FULLSTEP PS", g_sInstancia, "Conexion", "BaseDeDatos", mBaseDeDatos
'    End If
    
    Set oFos = Nothing
End Function

