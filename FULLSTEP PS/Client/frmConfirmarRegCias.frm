VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmConfirmarRegCias 
   Caption         =   "Solicitudes de cambio de registro en compa��as compradoras"
   ClientHeight    =   7020
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9840
   Icon            =   "frmConfirmarRegCias.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7020
   ScaleWidth      =   9840
   Begin VB.CommandButton cmdDeshacer 
      Caption         =   "&Deshacer"
      Height          =   345
      Left            =   1275
      TabIndex        =   6
      Top             =   6585
      Width           =   1005
   End
   Begin VB.CommandButton cmdConfirmar 
      Caption         =   "&Confirmar"
      Height          =   345
      Left            =   90
      TabIndex        =   5
      Top             =   6585
      Width           =   1005
   End
   Begin VB.Frame fraCias 
      Height          =   615
      Left            =   105
      TabIndex        =   0
      Top             =   0
      Width           =   9600
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
         Height          =   285
         Left            =   1350
         TabIndex        =   1
         Top             =   210
         Width           =   1740
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   7064
         Columns(2).Caption=   "Denominaci�n"
         Columns(2).Name =   "DEN"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   3069
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
         Height          =   285
         Left            =   3525
         TabIndex        =   2
         Top             =   210
         Width           =   5910
         DataFieldList   =   "Column 0"
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5980
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "C�digo"
         Columns(2).Name =   "COD"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FC"
         Columns(3).Name =   "FC"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "FP"
         Columns(4).Name =   "FP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   10425
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   0
         BackColor       =   -2147483643
      End
      Begin VB.Label Label7 
         Caption         =   "Proveedor:"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   825
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCiasComp 
      Height          =   5760
      Left            =   135
      TabIndex        =   4
      Top             =   705
      Width           =   9600
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   5
      stylesets.count =   5
      stylesets(0).Name=   "Rojo"
      stylesets(0).BackColor=   11448063
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfirmarRegCias.frx":014A
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfirmarRegCias.frx":0166
      stylesets(2).Name=   "Selection"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   11513775
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfirmarRegCias.frx":0182
      stylesets(2).AlignmentPicture=   1
      stylesets(3).Name=   "Amarillo"
      stylesets(3).BackColor=   11796479
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfirmarRegCias.frx":019E
      stylesets(4).Name=   "Verde"
      stylesets(4).BackColor=   11796403
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmConfirmarRegCias.frx":01BA
      AllowDelete     =   -1  'True
      ActiveCellStyleSet=   "Normal"
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "Normal"
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3122
      Columns(1).Caption=   "C�digo de compa��a"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   7117
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   2884
      Columns(3).Caption=   "Solicitud de registro"
      Columns(3).Name =   "SOLICITADA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   11
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).Style=   2
      Columns(4).Width=   2805
      Columns(4).Caption=   "Registrada"
      Columns(4).Name =   "REGISTRADA"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).Style=   2
      _ExtentX        =   16933
      _ExtentY        =   10160
      _StockProps     =   79
      Caption         =   "Compa��as compradoras"
      ForeColor       =   0
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmConfirmarRegCias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oCiaSeleccionada As CCia
Private m_bRespetarCombo As Boolean
Private m_bValidando As Boolean
Dim m_oCias As CCias
Private bLoad As Boolean
Private m_bSesionIniciada As Boolean


Private sIdiNoDisponible As String
Private sIdiSi As String
Private sIdiNo As String

Private m_sCarpeta_Plantilla As String
''' <summary>
''' Confirmar "Solicitud de cambio de registro en compa��as compradoras"
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdConfirmar_Click()
    Dim i As Integer
    Dim teserror As CTESError
    Dim vEnlace As CEnlace
    Dim iRespuesta As Integer
    Dim bRegistroNoCompleto As Boolean
    Dim oUsuarioSeleccionado As CUsuario
    Dim oUsuarios As CUsuarios
    Dim errormail As TipoErrorSummit
    Dim bSinUsuarioPrincipal As Boolean
    bRegistroNoCompleto = False
    
    If m_oCiaSeleccionada Is Nothing Then Exit Sub
    
    RellenarDatosCia
    
    Screen.MousePointer = vbHourglass
    
    iRespuesta = basMensajes.PreguntaConfirmarRegistroCiasComp
    
    If iRespuesta = vbYes Then
        Set oUsuarios = g_oRaiz.Generar_CUsuarios
        Set oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
        Set oUsuarios = m_oCiaSeleccionada.CargarTodosLosUsuariosDesde
        If IsNull(m_oCiaSeleccionada.UsuarioPrincipal) Then
            basMensajes.sinUsuarioPrincipal
            bSinUsuarioPrincipal = True
            For Each oUsuarioSeleccionado In oUsuarios
                Exit For
            Next
        Else
            Set oUsuarioSeleccionado = oUsuarios.Item((CStr(m_oCiaSeleccionada.UsuarioPrincipal)))
        End If
        RellenarDatosUsuario oUsuarioSeleccionado
        
        
'        PrepararMensajeConfirmacion oUsuarioSeleccionado
        
        Dim sAsunto As String
        
        If ((Not m_bSesionIniciada) Or (g_oMailSender Is Nothing)) Then
            DoEvents
            IniciarSesionMail
            m_bSesionIniciada = True
        End If
            
        Select Case oUsuarioSeleccionado.idioma
            Case 0
                sAsunto = g_udtParametrosGenerales.g_sNotifConfirmRegCiasENG
            Case 1
                sAsunto = g_udtParametrosGenerales.g_sNotifConfirmRegCiasSPA
            Case 2
                sAsunto = g_udtParametrosGenerales.g_sNotifConfirmRegCiasGER
        End Select
        
        
        sdbgCiasComp.MoveFirst
        i = 1
        While sdbgCiasComp.Rows >= i
            If sdbgCiasComp.Columns("SOLICITADA").Value Then
                Set teserror = m_oCiaSeleccionada.AnyadirRegistro(sdbgCiasComp.Columns("ID").Value)
            Else
                Set vEnlace = m_oCiaSeleccionada.DevolverEnlaceConCiaComp(sdbgCiasComp.Columns("ID").Value)
                If Not vEnlace Is Nothing Then
                    iRespuesta = basMensajes.PreguntaEliminarEnlaceComp(sdbgCiasComp.Columns("DEN").Value, m_oCiaSeleccionada.Den)
                    If iRespuesta = vbYes Then
                        frmEsperaPortal.Show
                        DoEvents
                        Set teserror = vEnlace.EliminarEnlace
                        Unload frmEsperaPortal
                        
                        Screen.MousePointer = vbNormal
                        
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Exit Sub
                        End If
                        Set teserror = m_oCiaSeleccionada.EliminarRegistro(sdbgCiasComp.Columns("ID").Value)
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                    Else
                        bRegistroNoCompleto = True
                        Screen.MousePointer = vbNormal
                        If basMensajes.PreguntaContinuarConfirmarRegistroCiasComp = vbNo Then
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                    End If
                Else
                    Set teserror = m_oCiaSeleccionada.EliminarRegistro(sdbgCiasComp.Columns("id").Value)
                End If
            End If
            
            i = i + 1
            sdbgCiasComp.MoveNext
        Wend
        
        If bRegistroNoCompleto = False Then
            Set teserror = m_oCiaSeleccionada.registroConfirmado()
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
        
        Dim sTo As String
        
        If (g_udtParametrosGenerales.g_iNotifRegCias = 1) Then
            Screen.MousePointer = vbNormal
            
            If (g_udtParametrosGenerales.g_iConfirmRegCias = 0) Or bSinUsuarioPrincipal Then
                Set frmConfirNotif.g_oCiaSeleccionada = m_oCiaSeleccionada
                frmConfirNotif.Show vbModal
                If frmConfirNotif.bCancelar = True Then
                    Unload frmConfirNotif
                    Exit Sub
                End If
                sTo = frmConfirNotif.txtMail
                Unload frmConfirNotif
            
            Else
                sTo = oUsuarioSeleccionado.Email
            End If
            
        
            Dim sCuerpo As String
            If g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS <> "" Then
                m_oCiaSeleccionada.SacarRemitente
                m_sCarpeta_Plantilla = m_oCiaSeleccionada.CarpetaPlantilla
            End If
            Select Case oUsuarioSeleccionado.TipoMail
                Case 1
                    sCuerpo = GenerarMensajeConfirmRegCiasHTML(oUsuarioSeleccionado)
                Case Else
                    sCuerpo = GenerarMensajeConfirmRegCiasTXT(oUsuarioSeleccionado)
            End Select
            
            If sCuerpo <> "" Then
                errormail = ComponerMensaje(sTo, sAsunto, sCuerpo, , "frmConfirmarRegCias", m_oCiaSeleccionada.Id, SQLBinaryToBoolean(oUsuarioSeleccionado.TipoMail))
                If errormail.NumError <> TESnoerror Then
                    If errormail.NumError <> TESnoerror Then
                        basMensajes.ErrorOriginal errormail
                    End If
                End If
            End If
    
            If (m_bSesionIniciada) Then
                FinalizarMailSender
            End If
            
        End If
                
        Screen.MousePointer = vbNormal
        
        sdbgCiasComp.RemoveAll
        sdbcCiaCod.RemoveAll
        sdbcCiaDen.RemoveAll
        sdbcCiaCod = ""
        sdbcCiaDen = ""
        
    End If
End Sub

''' <summary>
''' Deshacer  "Solicitud de cambio de registro en compa��as compradoras"
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdDeshacer_Click()
Dim teserror As CTESError
Dim iRespuesta As Integer
Dim sAsunto As String
Dim sTo As String
Dim errormail As TipoErrorSummit

If m_oCiaSeleccionada Is Nothing Then Exit Sub


RellenarDatosCia

Screen.MousePointer = vbHourglass

iRespuesta = basMensajes.PreguntaDeshacerRegistroCiasComp()
If iRespuesta = vbYes Then
    Set teserror = m_oCiaSeleccionada.deshacerSolicitudRegCompradoras()
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
Else
    Screen.MousePointer = vbNormal
    Exit Sub
End If
    
    Dim oUsuarios As CUsuarios
    Dim oUsuarioSeleccionado As CUsuario
    Set oUsuarios = g_oRaiz.Generar_CUsuarios
    Set oUsuarioSeleccionado = g_oRaiz.Generar_CUsuario
    Set oUsuarios = m_oCiaSeleccionada.CargarTodosLosUsuariosDesde
    Set oUsuarioSeleccionado = oUsuarios.Item((CStr(m_oCiaSeleccionada.UsuarioPrincipal)))
    RellenarDatosUsuario oUsuarioSeleccionado

        If (g_udtParametrosGenerales.g_iNotifRegCias = 1) Then
            Screen.MousePointer = vbNormal
            
            Select Case oUsuarioSeleccionado.idioma
                Case 0
                    sAsunto = g_udtParametrosGenerales.g_sNotifRechazoRegCiasENG
                Case 1
                    sAsunto = g_udtParametrosGenerales.g_sNotifRechazoRegCiasSPA
                Case 2
                    sAsunto = g_udtParametrosGenerales.g_sNotifRechazoRegCiasGER
            End Select
            
            If (g_udtParametrosGenerales.g_iConfirmRegCias = 0) Then
                Set frmConfirNotif.g_oCiaSeleccionada = m_oCiaSeleccionada
                frmConfirNotif.Show vbModal
                If frmConfirNotif.bCancelar = True Then
                    Unload frmConfirNotif
                    Exit Sub
                End If
                sTo = frmConfirNotif.txtMail
                Unload frmConfirNotif
            
            Else
                sTo = oUsuarioSeleccionado.Email
            End If
            
            Dim sCuerpo As String
            
            If g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS <> "" Then
                m_oCiaSeleccionada.SacarRemitente
                m_sCarpeta_Plantilla = m_oCiaSeleccionada.CarpetaPlantilla
            End If
            Select Case oUsuarioSeleccionado.TipoMail
                Case 1
                    sCuerpo = GenerarMensajeRechazoRegCiasHTML(oUsuarioSeleccionado)
                Case Else
                    sCuerpo = GenerarMensajeRechazoRegCiasTXT(oUsuarioSeleccionado)
            End Select
            
            If sCuerpo <> "" Then
                errormail = ComponerMensaje(sTo, sAsunto, sCuerpo, , "frmConfirmarRegCias", m_oCiaSeleccionada.Id, SQLBinaryToBoolean(oUsuarioSeleccionado.TipoMail))
                If errormail.NumError <> TESnoerror Then
                    If errormail.NumError <> TESnoerror Then
                        basMensajes.ErrorOriginal errormail
                    End If
                End If
            End If
    
            If (m_bSesionIniciada) Then
                FinalizarMailSender
            End If
   
        End If

Screen.MousePointer = vbNormal
    
sdbgCiasComp.RemoveAll
sdbcCiaCod.RemoveAll
sdbcCiaDen.RemoveAll
sdbcCiaCod = ""
sdbcCiaDen = ""
        
    
End Sub

Private Sub Form_Load()
    bLoad = True
    Me.Width = 9960
    Me.Height = 7425
    CargarRecursos
    bLoad = False
End Sub

Private Sub Form_Resize()
If Not bLoad Then Arrange

End Sub


Private Sub sdbcCiaCod_Change()

If Not m_bRespetarCombo Then
        m_bValidando = False
        m_bRespetarCombo = True
        sdbcCiaDen.Text = ""
        sdbgCiasComp.RemoveAll
        m_bRespetarCombo = False
End If
  
End Sub

Private Sub sdbcCiaCod_Click()
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If

End Sub

Private Sub sdbcCiaCod_CloseUp()
    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    m_bRespetarCombo = True
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    m_bRespetarCombo = False
    
    
    CiaSeleccionada
    m_bValidando = True
    
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim ador As ador.Recordset
    Set m_oCias = Nothing
    Set m_oCias = g_oRaiz.Generar_CCias
    sdbcCiaCod.RemoveAll


    Set ador = m_oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, , , , , , , , , , , , , , , , , , , , , , , , , True)

    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem ador("ID").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("FCEST").Value & Chr(9) & ador("FPEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh

End Sub


Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
    Dim ador As ador.Recordset

    If m_bValidando Then Exit Sub
    m_bValidando = True

    Set m_oCias = Nothing
    Set m_oCias = g_oRaiz.Generar_CCias

    If Trim(sdbcCiaCod.Text) = "" Then Exit Sub
    
    Set ador = m_oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , , , , , , , , , , , , , , , , , , , , , , , , , True)
    
    If ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                m_bRespetarCombo = True
                sdbcCiaDen.Text = ador("DEN").Value
                sdbcCiaCod.Text = ador("COD").Value
                sdbcCiaCod.Columns(0).Value = ador("ID").Value
                CiaSeleccionada
                m_bRespetarCombo = False
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If



End Sub

Private Sub sdbcCiaDen_Change()
   
If Not m_bRespetarCombo Then
        m_bValidando = False
        
        m_bRespetarCombo = True
        sdbcCiaCod.Text = ""
        sdbgCiasComp.RemoveAll
        
        m_bRespetarCombo = False
End If

End Sub

Private Sub sdbcCiaDen_Click()
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_CloseUp()
    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    m_bRespetarCombo = False
    
    
    CiaSeleccionada
    m_bValidando = True

End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim ador As ador.Recordset
    Dim oCiasInner As CCias
    Set oCiasInner = g_oRaiz.Generar_CCias
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    
    Set ador = oCiasInner.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, , , , , , , , , , , , , , , , , , , , , , , , , True)
    
    If Not ador Is Nothing Then
        
        i = 1
        
        While Not ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem ador("ID").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("FCEST").Value & Chr(9) & ador("FPEST").Value
            ador.MoveNext
        Wend
        
        If Not ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        ador.Close
        Set ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    

End Sub

Private Sub sdbcCiaDen_Validate(Cancel As Boolean)
Dim ador As ador.Recordset
If m_bValidando Then Exit Sub
m_bValidando = True

    If Trim(sdbcCiaDen.Text) = "" Then Exit Sub
    Set m_oCias = Nothing
    Set m_oCias = g_oRaiz.Generar_CCias
    
    Set ador = m_oCias.DevolverCiasDesde(1, , sdbcCiaDen.Text, True, , , , , , , , , , , , , , , , , , , , , , , , , , True)
    
    If ador Is Nothing Then
            
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaDen.Text) <> UCase(ador("DEN").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                m_bRespetarCombo = True
                sdbcCiaDen.Text = ador("DEN").Value
                sdbcCiaCod.Text = ador("COD").Value
                sdbcCiaCod.Columns(0).Value = ador("ID").Value
                CiaSeleccionada
                m_bRespetarCombo = False
            End If
        End If
        
        ador.Close
        Set ador = Nothing
        
    End If
    


End Sub

Private Sub CiaSeleccionada()

Dim ador As ador.Recordset

sdbgCiasComp.RemoveAll

Set m_oCias = g_oRaiz.Generar_CCias
m_oCias.CargarTodasLasCiasDesde 1, sdbcCiaCod.Columns(0).Value, , , True

Set m_oCiaSeleccionada = Nothing
Set m_oCiaSeleccionada = m_oCias.Item(CStr(sdbcCiaCod.Columns(0).Value))

Set ador = m_oCiaSeleccionada.devolverRegistroEnCompradoras()

While Not ador.EOF
    If ador("SOLICITADA").Value <> ador("REGISTRADA").Value Then
        sdbgCiasComp.AddItem ador("IDCIA").Value & Chr(9) & ador("COD").Value & Chr(9) & ador("DEN").Value & Chr(9) & ador("SOLICITADA").Value & Chr(9) & ador("REGISTRADA").Value
    End If

ador.MoveNext
Wend
ador.Close
Set ador = Nothing



End Sub


Private Sub Arrange()
Dim vfraCiasW As Integer
Dim vcmdConfirmarT As Integer
Dim vcmdDeshacerT As Integer
Dim vsdbgCiasCompW As Integer
Dim vsdbgCiasCompH As Integer
Dim vsdgbCiasCompCW  As Integer

Dim vWidth As Integer
Dim vHeight As Integer

Dim iH As Integer
Dim iV As Integer
On Error Resume Next

vfraCiasW = 9600

vcmdConfirmarT = 6585
vcmdDeshacerT = 6585
vsdbgCiasCompW = 9600
vsdgbCiasCompCW = 9030
vsdbgCiasCompH = 5760
vWidth = 9960
vHeight = 7425

iH = Me.Height - vHeight
iV = Me.Width - vWidth




fraCias.Width = vfraCiasW + iV

cmdConfirmar.Top = vcmdConfirmarT + iH

cmdDeshacer.Top = vcmdDeshacerT + iH

sdbgCiasComp.Width = vsdbgCiasCompW + iV
sdbgCiasComp.Height = vsdbgCiasCompH + iH

sdbgCiasComp.Columns("COD").Width = (sdbgCiasComp.Width - 570) * 0.196
sdbgCiasComp.Columns("DEN").Width = (sdbgCiasComp.Width - 570) * 0.4468
sdbgCiasComp.Columns("SOLICITADA").Width = (sdbgCiasComp.Width - 570) * 0.181
sdbgCiasComp.Columns("REGISTRADA").Width = (sdbgCiasComp.Width - 570) * 0.1762

End Sub

Private Function Disponible(ByVal param As Variant) As Boolean
'**********************************************************************
'*** Descripci�n: Determina cu�ndo un valor est� disponible.        ***
'***                                                                ***
'*** Par�metros:  param ::>> argumento de entrada sobre el cu�l     ***
'***                         se har�n las comprobaciones sobre su   ***
'***                         disponibilidad.                        ***
'***                                                                ***
'*** Valor que devuelve: True  ::>> Si el valor pasado como         ***
'***                                par�metro no es nulo y ha sido  ***
'***                                inicializado, luego est�        ***
'***                                disponible.                     ***
'***                     False ::>> En caso contrario, el par�metro ***
'***                                no est� disponible.             ***
'***                                                                ***
'**********************************************************************
    If ((Not IsEmpty(param)) And (Not IsNull(param)) And (param <> "")) Then
        Disponible = True
    Else
        Disponible = False
    End If
End Function

Private Sub RellenarDatosUsuario(ByRef poUsuarioSeleccionado As CUsuario)
'******************************************************************
'*** Descripci�n: Rellena los datos del usuario seleccionado.   ***
'***                                                            ***
'*** Par�metros:  ::>> poUsuarioSeleccionado: el usuario cuyos  ***
'***                   datos ser�n introducidos. Param. por ref.***
'***                                                            ***
'*** Valor que devuelve: -------------                          ***
'******************************************************************
    Dim adores As ador.Recordset
    
    Set adores = m_oCiaSeleccionada.Usuarios.DevolverDatosUsuario(m_oCiaSeleccionada.Id, poUsuarioSeleccionado.Id)
    If Not adores Is Nothing Then
        With poUsuarioSeleccionado
            .Cargo = NullToStr(adores("CAR").Value)
            .Departamento = NullToStr(adores("DEP").Value)
            .Email = NullToStr(adores("EMAIL").Value)
            .Fax = NullToStr(adores("FAX").Value)
            .Tfno1 = NullToStr(adores("TFNO").Value)
            .Tfno2 = NullToStr(adores("TFNO2").Value)
            .TfnoMovil = NullToStr(adores("TFNO_MOVIL").Value)
            .PWDEncriptada = adores("PWD").Value
            .fecpwd = adores("FECPWD").Value
            If Not IsNull(adores("IDICOD").Value) Then
                .idioma = adores("IDIID").Value
                .Idiomaden = adores("IDIDEN").Value
            End If
        End With
    End If
    adores.Close
    Set adores = Nothing
End Sub

Private Function GenerarMensajeConfirmRegCiasTXT(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifConfirmRegCias.txt o                     ***
'***              ENG_FSPSNotifConfirmRegCias.txt dependi�ndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sCiasRegistradas As String
    Dim ador As ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARREGCIAS, sIdioma, 11)
    If Not ador Is Nothing Then
        sIdiNoDisponible = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
    
    sFileName = sFileName & "FSPSNotifConfirmRegCias.txt"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeConfirmRegCiasTXT = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(m_oCiaSeleccionada.Cod, Len(m_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", m_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", m_oCiaSeleccionada.Den)
    If (Disponible(m_oCiaSeleccionada.NIF)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", m_oCiaSeleccionada.NIF)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.CodPos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", m_oCiaSeleccionada.CodPos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Poblacion)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", m_oCiaSeleccionada.Poblacion)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", m_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    Set adores = oCias.DevolverDatosCia(m_oCiaSeleccionada.Id)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If
    
    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    If (Disponible(m_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", m_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case m_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case m_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case m_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)
    
    If (Disponible(poUsuario.Cod)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Nombre)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", poUsuario.Nombre)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Apellidos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", poUsuario.Apellidos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno1)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", poUsuario.Tfno1)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno2)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", poUsuario.Tfno2)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.TfnoMovil)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", poUsuario.TfnoMovil)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Fax)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", poUsuario.Fax)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Email)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", poUsuario.Email)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Cargo)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", poUsuario.Cargo)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Departamento)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", poUsuario.Departamento)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If
    

    Set adores = m_oCiaSeleccionada.devolverRegistroEnCompradoras
    Select Case adores.EOF
        Case True
            sCiasRegistradas = sIdiNoDisponible
        Case False
            sCiasRegistradas = ""
            While Not adores.EOF
                If (adores.Fields.Item("REGISTRADA") = 1) Then
                    sCiasRegistradas = sCiasRegistradas & "    # (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN") & vbCrLf
                End If
                adores.MoveNext
            Wend
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIAS_ACTUALES", sCiasRegistradas)
    adores.Close
    
    
    Set adores = m_oCiaSeleccionada.devolverRegistroEnCompradoras
    Select Case adores.EOF
        Case True
            sCiasRegistradas = sIdiNoDisponible
        Case False
            sCiasRegistradas = ""
            While Not adores.EOF
                If (adores.Fields.Item("SOLICITADA") = 1) Then
                    sCiasRegistradas = sCiasRegistradas & "    # (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN") & vbCrLf
                End If
                adores.MoveNext
            Wend
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIAS_NUEVAS", sCiasRegistradas)
    adores.Close
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeConfirmRegCiasTXT = sCuerpoMensaje
End Function

Private Function GenerarMensajeRechazoRegCiasTXT(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifRechazoRegCias.txt o                     ***
'***              ENG_FSPSNotifRechazoRegCias.txt dependi�ndo del       ***
'***              idioma                                                ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sCiasRegistradas As String
    Dim ador As ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARREGCIAS, sIdioma, 11)
    If Not ador Is Nothing Then
        sIdiNoDisponible = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
    
    sFileName = sFileName & "FSPSNotifRechazoRegCias.txt"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeRechazoRegCiasTXT = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(m_oCiaSeleccionada.Cod, Len(m_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", m_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", m_oCiaSeleccionada.Den)
    If (Disponible(m_oCiaSeleccionada.NIF)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", m_oCiaSeleccionada.NIF)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.CodPos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", m_oCiaSeleccionada.CodPos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Poblacion)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", m_oCiaSeleccionada.Poblacion)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", m_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    Set adores = oCias.DevolverDatosCia(m_oCiaSeleccionada.Id)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If
    
    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    If (Disponible(m_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", m_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case m_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case m_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case m_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)
    
    If (Disponible(poUsuario.Cod)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Nombre)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", poUsuario.Nombre)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Apellidos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", poUsuario.Apellidos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno1)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", poUsuario.Tfno1)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno2)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", poUsuario.Tfno2)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.TfnoMovil)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", poUsuario.TfnoMovil)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Fax)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", poUsuario.Fax)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Email)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", poUsuario.Email)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Cargo)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", poUsuario.Cargo)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Departamento)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", poUsuario.Departamento)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If
    
    
    Set adores = m_oCiaSeleccionada.devolverRegistroEnCompradoras
    Select Case adores.EOF
        Case True
            sCiasRegistradas = sIdiNoDisponible
        Case False
            sCiasRegistradas = ""
            While Not adores.EOF
                If (adores.Fields.Item("REGISTRADA") = 1) Then
                    sCiasRegistradas = sCiasRegistradas & "    # (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN") & vbCrLf
                End If
                adores.MoveNext
            Wend
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIAS_ACTUALES", sCiasRegistradas)
    adores.Close
    
    
    Set adores = m_oCiaSeleccionada.devolverRegistroEnCompradoras
    Select Case adores.EOF
        Case True
            sCiasRegistradas = sIdiNoDisponible
        Case False
            sCiasRegistradas = ""
            While Not adores.EOF
                If (adores.Fields.Item("SOLICITADA") = 1) Then
                    sCiasRegistradas = sCiasRegistradas & "    # (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN") & vbCrLf
                End If
                adores.MoveNext
            Wend
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIAS_NUEVAS", sCiasRegistradas)
    adores.Close
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeRechazoRegCiasTXT = sCuerpoMensaje
End Function

Private Function GenerarMensajeRechazoRegCiasHTML(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en HTML a partir          ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifRechazoRegCias.htm o                     ***
'***              ENG_FSPSNotifRechazoRegCias.htm dependi�ndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sCiasRegistradas As String
    Dim ador As ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARREGCIAS, sIdioma, 11)
    If Not ador Is Nothing Then
        sIdiNoDisponible = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
    
    sFileName = sFileName & "FSPSNotifRechazoRegCias.htm"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeRechazoRegCiasHTML = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(m_oCiaSeleccionada.Cod, Len(m_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", m_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", m_oCiaSeleccionada.Den)
    If (Disponible(m_oCiaSeleccionada.NIF)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", m_oCiaSeleccionada.NIF)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.CodPos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", m_oCiaSeleccionada.CodPos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", m_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Poblacion)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", m_oCiaSeleccionada.Poblacion)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", sIdiNoDisponible)
    End If
    
    Set oCias = g_oRaiz.Generar_CCias
    Set adores = oCias.DevolverDatosCia(m_oCiaSeleccionada.Id)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If

    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    If (Disponible(m_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", m_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case m_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case m_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case m_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)
    
    If (Disponible(poUsuario.Cod)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Nombre)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", poUsuario.Nombre)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Apellidos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", poUsuario.Apellidos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno1)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", poUsuario.Tfno1)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno2)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", poUsuario.Tfno2)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.TfnoMovil)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", poUsuario.TfnoMovil)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Fax)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", poUsuario.Fax)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Email)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", poUsuario.Email)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Cargo)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", poUsuario.Cargo)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Departamento)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", poUsuario.Departamento)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If
    
    
    Set adores = m_oCiaSeleccionada.devolverRegistroEnCompradoras
    Select Case adores.EOF
        Case True
            sCiasRegistradas = sIdiNoDisponible
        Case False
            sCiasRegistradas = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
            While Not adores.EOF
                If (adores.Fields.Item("REGISTRADA") = 1) Then
                    sCiasRegistradas = sCiasRegistradas & "<LI> (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN")
                End If
                adores.MoveNext
            Wend
            sCiasRegistradas = sCiasRegistradas & "</UL>"
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIAS_ACTUALES", sCiasRegistradas)
    adores.Close
    
    Set adores = m_oCiaSeleccionada.devolverRegistroEnCompradoras
    Select Case adores.EOF
        Case True
            sCiasRegistradas = sIdiNoDisponible
        Case False
            sCiasRegistradas = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
            While Not adores.EOF
                If (adores.Fields.Item("SOLICITADA") = 1) Then
                    sCiasRegistradas = sCiasRegistradas & "<LI> (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN")
                End If
                adores.MoveNext
            Wend
            sCiasRegistradas = sCiasRegistradas & "</UL>"
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIAS_NUEVAS", sCiasRegistradas)
    adores.Close
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeRechazoRegCiasHTML = sCuerpoMensaje
End Function

Private Function GenerarMensajeConfirmRegCiasHTML(poUsuario As CUsuario) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en HTML a partir          ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSPSNotifConfirmRegCias.htm o                     ***
'***              ENG_FSPSNotifConfirmRegCias.htm dependi�ndo del       ***
'***              idioma.                                               ***
'***                                                                    ***
'*** Par�metros:  poUsuario, un objeto de tipo CUsuario del que         ***
'***              obtendremos datos relativos al usuario.               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim sConsulta As String
    Dim adores As ador.Recordset
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim oUsuarios As CUsuarios
    Dim oCias As CCias
    Dim sCuerpoMensaje As String
    Dim sEstado As String
    Dim sFileName As String
    Dim sCiasRegistradas As String
    Dim ador As ador.Recordset
    Dim sIdioma As String
    Dim oActividadesNivel1 As CActividadesNivel1
        
    Set oActividadesNivel1 = g_oRaiz.generar_CActividadesNivel1
    
    Select Case poUsuario.idioma
        Case 0
            sFileName = "ENG_"
            sIdioma = "ENG"
        Case 1
            sFileName = "SPA_"
            sIdioma = "SPA"
        Case 2
            sFileName = "GER_"
            sIdioma = "GER"
    End Select
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARREGCIAS, sIdioma, 11)
    If Not ador Is Nothing Then
        sIdiNoDisponible = ador(0).Value
    End If
    ador.Close
    Set ador = Nothing
    
    sFileName = sFileName & "FSPSNotifConfirmRegCias.htm"

    Set oFos = New FileSystemObject
    
    If Not oFos.FileExists(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName) Then
        basMensajes.RutaPlantillasNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        GenerarMensajeConfirmRegCiasHTML = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(Replace(basParametros.g_sPlantillasPath, g_udtParametrosGenerales.g_sMARCADOR_CARPETA_PLANTILLAS, m_sCarpeta_Plantilla) & "\" & sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    sCuerpoMensaje = ostream.ReadAll
    If oActividadesNivel1.EsPYME Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", Right(m_oCiaSeleccionada.Cod, Len(m_oCiaSeleccionada.Cod) - LEN_PYME))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODCIA", m_oCiaSeleccionada.Cod)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENCIA", m_oCiaSeleccionada.Den)
    If (Disponible(m_oCiaSeleccionada.NIF)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", m_oCiaSeleccionada.NIF)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NIF", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.CodPos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", m_oCiaSeleccionada.CodPos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CP", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Poblacion)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", m_oCiaSeleccionada.Poblacion)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@POB", sIdiNoDisponible)
    End If
    If (Disponible(m_oCiaSeleccionada.Dir)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", m_oCiaSeleccionada.Dir)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DIR", sIdiNoDisponible)
    End If

    Set oCias = g_oRaiz.Generar_CCias
    Set adores = oCias.DevolverDatosCia(m_oCiaSeleccionada.Id)
    
    If (Not IsNull(adores.Fields.Item("PROVICOD"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", adores.Fields.Item("PROVICOD"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPROVI", sIdiNoDisponible)
    End If

    If (Not IsNull(adores.Fields.Item("PROVIDEN"))) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", adores.Fields.Item("PROVIDEN"))
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPROVI", sIdiNoDisponible)
    End If
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODPAI", adores.Fields.Item("PAICOD"))
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@DENPAI", adores.Fields.Item("PAIDEN"))
    If (Disponible(m_oCiaSeleccionada.URLCia)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", m_oCiaSeleccionada.URLCia)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@URL", sIdiNoDisponible)
    End If
    Select Case m_oCiaSeleccionada.EstadoCompradora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FC", sEstado)
    Select Case m_oCiaSeleccionada.EstadoProveedora
        Case 3
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@FP", sEstado)
    Select Case m_oCiaSeleccionada.Premium
        Case 2
            sEstado = sIdiSi
        Case Else
            sEstado = sIdiNo
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@PREMIUM", sEstado)

    If (Disponible(poUsuario.Cod)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", poUsuario.Cod)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CODUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Nombre)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", poUsuario.Nombre)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@NOMUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Apellidos)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", poUsuario.Apellidos)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@APEUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno1)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", poUsuario.Tfno1)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO1USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Tfno2)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", poUsuario.Tfno2)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@TFNO2USU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.TfnoMovil)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", poUsuario.TfnoMovil)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@MOVILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Fax)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", poUsuario.Fax)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@FAXUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Email)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", poUsuario.Email)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@EMAILUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Cargo)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", poUsuario.Cargo)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@CARGOUSU", sIdiNoDisponible)
    End If
    If (Disponible(poUsuario.Departamento)) Then
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", poUsuario.Departamento)
    Else
        sCuerpoMensaje = Replace(sCuerpoMensaje, "@DEPUSU", sIdiNoDisponible)
    End If

    Set adores = m_oCiaSeleccionada.devolverRegistroEnCompradoras
    Select Case adores.EOF
        Case True
            sCiasRegistradas = sIdiNoDisponible
        Case False
            sCiasRegistradas = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
            While Not adores.EOF
                If (adores.Fields.Item("REGISTRADA") = 1) Then
                    sCiasRegistradas = sCiasRegistradas & "<LI> (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN")
                End If
                adores.MoveNext
            Wend
            sCiasRegistradas = sCiasRegistradas & "</UL>"
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIAS_ACTUALES", sCiasRegistradas)
    adores.Close
    
    Set adores = m_oCiaSeleccionada.devolverRegistroEnCompradoras
    Select Case adores.EOF
        Case True
            sCiasRegistradas = sIdiNoDisponible
        Case False
            sCiasRegistradas = "<UL TYPE=" & Chr(34) & "disc" & Chr(34) & ">"
            While Not adores.EOF
                If (adores.Fields.Item("SOLICITADA") = 1) Then
                    sCiasRegistradas = sCiasRegistradas & "<LI> (" & adores.Fields.Item("COD") & ") - " & adores.Fields.Item("DEN")
                End If
                adores.MoveNext
            Wend
            sCiasRegistradas = sCiasRegistradas & "</UL>"
    End Select
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@CIAS_NUEVAS", sCiasRegistradas)
    adores.Close
    Set adores = Nothing
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing
    Set oUsuarios = Nothing
    Set oActividadesNivel1 = Nothing
    GenerarMensajeConfirmRegCiasHTML = sCuerpoMensaje
End Function

Private Sub RellenarDatosCia()
'******************************************************************
'*** Descripci�n: Rellena los datos de la compa��a seleccionada.***
'***                                                            ***
'*** Par�metros:         -------------                          ***
'***                                                            ***
'*** Valor que devuelve: -------------                          ***
'******************************************************************
    Dim adores As ador.Recordset
    
    If m_oCiaSeleccionada Is Nothing Then Exit Sub
    Set adores = m_oCias.DevolverDatosCia(sdbcCiaCod.Columns(0).Value)
    If ((Not adores Is Nothing) And (Not adores.EOF)) Then
        m_oCiaSeleccionada.codpai = adores("PAIID")
        m_oCiaSeleccionada.codprovi = adores("PROVIID")
        m_oCiaSeleccionada.idioma = adores("IDIID")
        m_oCiaSeleccionada.Cod = adores("COD")
        m_oCiaSeleccionada.Den = adores("DEN")
        m_oCiaSeleccionada.CodPos = adores("CP")
        m_oCiaSeleccionada.Coment = adores("COMENT")
        m_oCiaSeleccionada.Dir = adores("DIR")
        m_oCiaSeleccionada.NIF = adores("NIF")
        m_oCiaSeleccionada.Poblacion = adores("POB")
        m_oCiaSeleccionada.URLCia = adores("URLCIA")
        m_oCiaSeleccionada.VolFac = adores("VOLFACT")
        m_oCiaSeleccionada.CliRef1 = adores("CLIREF1")
        m_oCiaSeleccionada.CliRef2 = adores("CLIREF2")
        m_oCiaSeleccionada.CliRef3 = adores("CLIREF3")
        m_oCiaSeleccionada.CliRef4 = adores("CLIREF4")
        m_oCiaSeleccionada.Hom1 = adores("HOM1")
        m_oCiaSeleccionada.Hom1 = adores("HOM2")
        m_oCiaSeleccionada.Hom1 = adores("HOM3")
        m_oCiaSeleccionada.EstadoCompradora = adores("FCEST")
        m_oCiaSeleccionada.EstadoProveedora = adores("FPEST")
    End If
    adores.Close
    Set adores = Nothing
End Sub

Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRMARREGCIAS, g_udtParametrosGenerales.g_sIdioma)
   
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdConfirmar.Caption = ador(0).Value
        ador.MoveNext
        Me.cmdDeshacer.Caption = ador(0).Value
        ador.MoveNext
        Me.Label7.Caption = ador(0).Value
        ador.MoveNext
        
        Me.sdbcCiaCod.Columns(1).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbcCiaCod.Columns(2).Caption = ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = ador(0).Value
        Me.sdbgCiasComp.Columns(2).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgCiasComp.Caption = ador(0).Value
        ador.MoveNext
        
        Me.sdbgCiasComp.Columns(1).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgCiasComp.Columns(3).Caption = ador(0).Value
        ador.MoveNext
        Me.sdbgCiasComp.Columns(4).Caption = ador(0).Value
        ador.MoveNext
        sIdiNoDisponible = ador(0).Value
        ador.MoveNext
        sIdiSi = ador(0).Value
        ador.MoveNext
        sIdiNo = ador(0).Value
        
        ador.Close
    End If
    
    Set ador = Nothing
           
End Sub



