VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRACTDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle"
   ClientHeight    =   1995
   ClientLeft      =   45
   ClientTop       =   780
   ClientWidth     =   7230
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRACTDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1995
   ScaleWidth      =   7230
   ShowInTaskbar   =   0   'False
   Begin VB.Timer timDet 
      Interval        =   60000
      Left            =   165
      Top             =   3015
   End
   Begin VB.PictureBox picCod 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   1800
      ScaleHeight     =   360
      ScaleWidth      =   2265
      TabIndex        =   7
      Top             =   165
      Width           =   2265
      Begin VB.TextBox txtCod 
         Height          =   270
         Left            =   60
         MaxLength       =   2
         TabIndex        =   0
         Top             =   60
         Width           =   1410
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   735
      ScaleHeight     =   420
      ScaleWidth      =   3825
      TabIndex        =   4
      Top             =   1485
      Width           =   3825
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   315
         Left            =   510
         TabIndex        =   2
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1650
         TabIndex        =   3
         Top             =   60
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAct 
      Height          =   240
      Left            =   75
      TabIndex        =   1
      Top             =   990
      Width           =   7020
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      stylesets.count =   4
      stylesets(0).Name=   "Normal"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   16777215
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmESTRACTDetalle.frx":0CB2
      stylesets(1).Name=   "ActiveRow"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8421376
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmESTRACTDetalle.frx":0CCE
      stylesets(2).Name=   "ActiveRowBlue"
      stylesets(2).ForeColor=   16777215
      stylesets(2).BackColor=   8388608
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmESTRACTDetalle.frx":0CEA
      stylesets(3).Name=   "Green"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   8421376
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmESTRACTDetalle.frx":0D06
      DividerType     =   2
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Caption=   "IDI"
      Columns(0).Name =   "IDI"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).StyleSet=   "Green"
      Columns(1).Width=   8996
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).StyleSet=   "Normal"
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD"
      Columns(2).Name =   "COD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   12382
      _ExtentY        =   423
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblDen 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominaci�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   645
      Width           =   1755
   End
   Begin VB.Label lblCod 
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   240
      Width           =   660
   End
End
Attribute VB_Name = "frmESTRACTDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_oACN1 As CActividadNivel1
Private m_oACN2 As CActividadNivel2
Private m_oACN3 As CActividadNivel3
Private m_oACN4 As CActividadNivel4
Private m_oACN5 As CActividadNivel5
Private m_aCod() As String
Private m_aDen() As String
Private m_iNum As Integer

Private sIdiCodigo As String
Private sIdiDenominacion As String


Private Sub cmdAceptar_Click()
Dim i As Integer
Dim oIBaseDatos As IBaseDatos
Dim teserror As CTESError

'********* Validar datos *********
If picCod.Enabled = True Then
    If Trim(txtCod.Text) = "" Then
        basMensajes.NoValido sIdiCodigo
        txtCod.SetFocus
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
End If

sdbgAct.MoveFirst

For i = 0 To m_iNum - 1
    If sdbgAct.Columns("DEN").Text = " " Or sdbgAct.Columns("DEN").Text = "" Then
        basMensajes.NoValida sIdiDenominacion
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    m_aDen(i) = sdbgAct.Columns("DEN").Text
        
    sdbgAct.MoveNext
Next

Screen.MousePointer = vbHourglass

Select Case frmActiv.g_udtAccion

    Case accionesps.ACCACT1Anya


            Set m_oACN1 = g_oRaiz.Generar_CActividadNivel1
            m_oACN1.Cod = Trim(txtCod.Text)
            'm_oACN1.Den = Trim(txtDen.Text)

            Set oIBaseDatos = m_oACN1

            Set teserror = oIBaseDatos.AnyadirABaseDatos(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                AnyadirACN1AEstructura
            Else
                TratarError teserror
                txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If

            Set m_oACN1 = Nothing
            Set oIBaseDatos = Nothing


    Case accionesps.ACCACT2Anya
            
            Set m_oACN2 = g_oRaiz.Generar_CActividadNivel2
            m_oACN2.Cod = Trim(txtCod.Text)
            m_oACN2.ACN1 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem)

            Set oIBaseDatos = m_oACN2

            Set teserror = oIBaseDatos.AnyadirABaseDatos(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                AnyadirACN2AEstructura
            Else
                TratarError teserror
                txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If

            Set m_oACN2 = Nothing
            Set oIBaseDatos = Nothing
            
    Case accionesps.ACCACT3Anya

            Set m_oACN3 = g_oRaiz.Generar_CActividadNivel3
            m_oACN3.Cod = Trim(txtCod.Text)
            m_oACN3.ACN1 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem.Parent)
            m_oACN3.ACN2 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem)

            Set oIBaseDatos = m_oACN3

            Set teserror = oIBaseDatos.AnyadirABaseDatos(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                AnyadirACN3AEstructura
            Else
                TratarError teserror
                txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If

            Set m_oACN3 = Nothing
            Set oIBaseDatos = Nothing

'
'
    Case accionesps.ACCACT4Anya

            Set m_oACN4 = g_oRaiz.Generar_CActividadNivel4
            m_oACN4.Cod = Trim(txtCod.Text)
            m_oACN4.ACN1 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem.Parent.Parent)
            m_oACN4.ACN2 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem.Parent)
            m_oACN4.ACN3 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem)

            Set oIBaseDatos = m_oACN4

            Set teserror = oIBaseDatos.AnyadirABaseDatos(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                AnyadirACN4AEstructura
            Else
                TratarError teserror
                txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If

            Set m_oACN4 = Nothing
            Set oIBaseDatos = Nothing
            
    Case accionesps.ACCACT5Anya

            Set m_oACN5 = g_oRaiz.Generar_CActividadNivel5
            m_oACN5.Cod = Trim(txtCod.Text)
            'm_oACN5.Den = Trim(txtDen.Text)
            m_oACN5.ACN1 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem.Parent.Parent.Parent)
            m_oACN5.ACN2 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem.Parent.Parent)
            m_oACN5.ACN3 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem.Parent)
            m_oACN5.ACN4 = frmActiv.DevolverId(frmActiv.tvwEstrAct.SelectedItem)
            
            Set oIBaseDatos = m_oACN5

            Set teserror = oIBaseDatos.AnyadirABaseDatos(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                AnyadirACN5AEstructura
            Else
                TratarError teserror
                txtCod.SetFocus
                Screen.MousePointer = vbNormal
                Exit Sub
            End If

            Set m_oACN5 = Nothing
            Set oIBaseDatos = Nothing

    Case accionesps.ACCACT1Mod


            frmActiv.g_oACN1Seleccionado.Cod = Trim(txtCod)
            
            For i = 0 To UBound(m_aCod())
                If frmActiv.sdbcIdiCod.Text = m_aCod(i) Then
                    frmActiv.g_oACN1Seleccionado.Den = Trim(sdbgAct.Columns("DEN").Text)
                    Exit For
                End If
            Next
            Set teserror = frmActiv.g_oIBaseDatos.FinalizarEdicionModificando(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                ModificarGMEnEstructura frmActiv.g_oACN1Seleccionado.Den
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set oIBaseDatos = Nothing

    Case accionesps.ACCACT2Mod
'

            frmActiv.g_oACN2Seleccionado.Cod = Trim(txtCod)
            For i = 0 To UBound(m_aCod())
                If frmActiv.sdbcIdiCod.Text = m_aCod(i) Then
                    frmActiv.g_oACN2Seleccionado.Den = Trim(sdbgAct.Columns("DEN").Text)
                    Exit For
                End If
            Next

            Set teserror = frmActiv.g_oIBaseDatos.FinalizarEdicionModificando(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                ModificarGMEnEstructura frmActiv.g_oACN2Seleccionado.Den
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set oIBaseDatos = Nothing
    
    Case accionesps.ACCACT3Mod

            frmActiv.g_oACN3Seleccionado.Cod = Trim(txtCod)
            For i = 0 To UBound(m_aCod())
                If frmActiv.sdbcIdiCod.Text = m_aCod(i) Then
                    frmActiv.g_oACN3Seleccionado.Den = Trim(sdbgAct.Columns("DEN").Text)
                    Exit For
                End If
            Next

            Set teserror = frmActiv.g_oIBaseDatos.FinalizarEdicionModificando(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                ModificarGMEnEstructura frmActiv.g_oACN3Seleccionado.Den
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If

            Set oIBaseDatos = Nothing

    Case accionesps.ACCACT4Mod

            frmActiv.g_oACN4Seleccionado.Cod = Trim(txtCod)
            For i = 0 To UBound(m_aCod())
                If frmActiv.sdbcIdiCod.Text = m_aCod(i) Then
                    frmActiv.g_oACN4Seleccionado.Den = Trim(sdbgAct.Columns("DEN").Text)
                    Exit For
                End If
            Next

            Set teserror = frmActiv.g_oIBaseDatos.FinalizarEdicionModificando(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                ModificarGMEnEstructura frmActiv.g_oACN4Seleccionado.Den
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set oIBaseDatos = Nothing

     Case accionesps.ACCACT5Mod
     
            frmActiv.g_oACN5Seleccionado.Cod = Trim(txtCod)
            
            sdbgAct.MoveFirst
            For i = 0 To m_iNum - 1
                If frmActiv.sdbcIdiCod.Text = sdbgAct.Columns("COD").Text Then
                    frmActiv.g_oACN5Seleccionado.Den = Trim(sdbgAct.Columns("DEN").Text)
                    Exit For
                End If
                sdbgAct.MoveNext
            Next

            Set teserror = frmActiv.g_oIBaseDatos.FinalizarEdicionModificando(m_aCod(), m_aDen(), frmActiv.sdbcIdiCod.Text)
            If teserror.numerror = TESnoerror Then
                ModificarGMEnEstructura frmActiv.g_oACN5Seleccionado.Den
            Else
                TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
     
            Set oIBaseDatos = Nothing
     
End Select

Screen.MousePointer = vbNormal

Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub
'
Private Sub Form_Activate()

Select Case frmActiv.g_udtAccion

    Case accionesps.ACCACT1Mod, accionesps.ACCACT2Mod, accionesps.ACCACT3Mod, accionesps.ACCACT4Mod, accionesps.ACCACT5Mod

        picCod.Enabled = False
        
    Case accionesps.ACCACT1Det, accionesps.ACCACT2Det, accionesps.ACCACT3Det, accionesps.ACCACT4Det, accionesps.ACCACT5Det

        
        picEdit.Visible = False
        picCod.Enabled = False
        Height = Height - 400

    Case Else
        txtCod.SetFocus

End Select
End Sub



Private Sub Form_Load()
Dim i As Integer
Dim Ador As Ador.Recordset
Dim nodx As MSComctlLib.Node
CargarRecursos

Set nodx = frmActiv.tvwEstrAct.SelectedItem

m_iNum = 0
Set Ador = g_oRaiz.DevolverIdiomas

If Not Ador Is Nothing Then

    While Not Ador.EOF
        ReDim Preserve m_aCod(m_iNum)
        ReDim Preserve m_aDen(m_iNum)
        sdbgAct.AddItem Ador(1).Value & Chr(9) & "" & Chr(9) & Ador(0).Value
        m_aCod(m_iNum) = Ador(0).Value
        sdbgAct.Height = sdbgAct.Height + sdbgAct.RowHeight
        m_iNum = m_iNum + 1
        Ador.MoveNext
    Wend
    
    picEdit.Top = sdbgAct.Height + sdbgAct.Top
    frmESTRACTDetalle.Height = Me.Height + sdbgAct.RowHeight * m_iNum
    
End If


Select Case frmActiv.g_udtAccion

    Case accionesps.ACCACT1Mod

        Set Ador = frmActiv.g_oACN1Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            sdbgAct.MoveFirst
        End If
            
    Case accionesps.ACCACT2Mod
        frmActiv.g_oACN2Seleccionado.ACN1 = frmActiv.DevolverId(nodx.Parent)
        Set Ador = frmActiv.g_oACN2Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            sdbgAct.MoveFirst
        End If
          
    Case accionesps.ACCACT3Mod

        frmActiv.g_oACN3Seleccionado.ACN1 = frmActiv.DevolverId(nodx.Parent.Parent)
        frmActiv.g_oACN3Seleccionado.ACN2 = frmActiv.DevolverId(nodx.Parent)
        Set Ador = frmActiv.g_oACN3Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            sdbgAct.MoveFirst
        End If
        
    Case accionesps.ACCACT4Mod

        frmActiv.g_oACN4Seleccionado.ACN1 = frmActiv.DevolverId(nodx.Parent.Parent.Parent)
        frmActiv.g_oACN4Seleccionado.ACN2 = frmActiv.DevolverId(nodx.Parent.Parent)
        frmActiv.g_oACN4Seleccionado.ACN3 = frmActiv.DevolverId(nodx.Parent)
        Set Ador = frmActiv.g_oACN4Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            sdbgAct.MoveFirst
        End If
        
    Case accionesps.ACCACT5Mod

        frmActiv.g_oACN5Seleccionado.ACN1 = frmActiv.DevolverId(nodx.Parent.Parent.Parent.Parent)
        frmActiv.g_oACN5Seleccionado.ACN2 = frmActiv.DevolverId(nodx.Parent.Parent.Parent)
        frmActiv.g_oACN5Seleccionado.ACN3 = frmActiv.DevolverId(nodx.Parent.Parent)
        frmActiv.g_oACN5Seleccionado.ACN4 = frmActiv.DevolverId(nodx.Parent)
        Set Ador = frmActiv.g_oACN5Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            sdbgAct.MoveFirst
        End If
        
    Case accionesps.ACCACT1Det

        Set Ador = frmActiv.g_oACN1Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            If accionesps.ACCACT1Det Then
                sdbgAct.Columns("DEN").Locked = True
            End If
            sdbgAct.MoveFirst
        End If
        
        
        
            
    Case accionesps.ACCACT2Det
        frmActiv.g_oACN2Seleccionado.ACN1 = frmActiv.DevolverId(nodx.Parent)
        Set Ador = frmActiv.g_oACN2Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            If accionesps.ACCACT2Det Then
                sdbgAct.Columns("DEN").Locked = True
            End If
            sdbgAct.MoveFirst
        End If
          
    Case accionesps.ACCACT3Det

        frmActiv.g_oACN3Seleccionado.ACN1 = frmActiv.DevolverId(nodx.Parent.Parent)
        frmActiv.g_oACN3Seleccionado.ACN2 = frmActiv.DevolverId(nodx.Parent)
        Set Ador = frmActiv.g_oACN3Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            If accionesps.ACCACT3Det Then
                sdbgAct.Columns("DEN").Locked = True
            End If
            sdbgAct.MoveFirst
        End If
        
    Case accionesps.ACCACT4Det

        frmActiv.g_oACN4Seleccionado.ACN1 = frmActiv.DevolverId(nodx.Parent.Parent.Parent)
        frmActiv.g_oACN4Seleccionado.ACN2 = frmActiv.DevolverId(nodx.Parent.Parent)
        frmActiv.g_oACN4Seleccionado.ACN3 = frmActiv.DevolverId(nodx.Parent)
        Set Ador = frmActiv.g_oACN4Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            If accionesps.ACCACT4Det Then
                sdbgAct.Columns("DEN").Locked = True
            End If
            sdbgAct.MoveFirst
        End If
        
    Case accionesps.ACCACT5Det

        frmActiv.g_oACN5Seleccionado.ACN1 = frmActiv.DevolverId(nodx.Parent.Parent.Parent.Parent)
        frmActiv.g_oACN5Seleccionado.ACN2 = frmActiv.DevolverId(nodx.Parent.Parent.Parent)
        frmActiv.g_oACN5Seleccionado.ACN3 = frmActiv.DevolverId(nodx.Parent.Parent)
        frmActiv.g_oACN5Seleccionado.ACN4 = frmActiv.DevolverId(nodx.Parent)
        Set Ador = frmActiv.g_oACN5Seleccionado.DevolverDenominaciones(m_aCod())
        sdbgAct.MoveFirst
        If Not Ador Is Nothing Then
            For i = 0 To m_iNum - 1
                sdbgAct.Columns("DEN").Text = Ador.Fields(i).Value
                sdbgAct.MoveNext
            Next
            If accionesps.ACCACT5Det Then
                sdbgAct.Columns("DEN").Locked = True
            End If
            sdbgAct.MoveFirst
        End If
        
        
End Select


End Sub

Public Function AnyadirACN1AEstructura()

Dim nodeAct As MSComctlLib.Node
Dim scod1 As String

scod1 = m_oACN1.ID & Mid$("                         ", 1, 10 - Len(CStr(m_oACN1.ID)))

Set nodeAct = frmActiv.tvwEstrAct.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, m_oACN1.Cod & " - " & m_oACN1.Den, "ACT")

nodeAct.Tag = "ACN1" & m_oACN1.ID

nodeAct.Selected = True
frmActiv.tvwEstrAct_NodeClick nodeAct
nodeAct.Selected = True

Set nodeAct = Nothing

End Function
Public Function AnyadirACN2AEstructura()

Dim nodeAct As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String

scod1 = m_oACN2.ACN1 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN2.ACN1)))
scod2 = m_oACN2.ID & Mid$("                         ", 1, 10 - Len(CStr(m_oACN2.ID)))

Set nodeAct = frmActiv.tvwEstrAct.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, m_oACN2.Cod & " - " & m_oACN2.Den, "ACT")
            
nodeAct.Tag = "ACN2" & m_oACN2.ID

nodeAct.Selected = True
frmActiv.tvwEstrAct_NodeClick nodeAct
nodeAct.Selected = True

Set nodeAct = Nothing

End Function

Public Function AnyadirACN3AEstructura()
Dim nodeAct As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String

scod1 = m_oACN3.ACN1 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN3.ACN1)))
scod2 = m_oACN3.ACN2 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN3.ACN2)))
scod3 = m_oACN3.ID & Mid$("                         ", 1, 10 - Len(CStr(m_oACN3.ID)))

Set nodeAct = frmActiv.tvwEstrAct.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, m_oACN3.Cod & " - " & m_oACN3.Den, "ACT")

nodeAct.Tag = "ACN3" & m_oACN3.ID

nodeAct.Selected = True
frmActiv.tvwEstrAct_NodeClick nodeAct
nodeAct.Selected = True

Set nodeAct = Nothing

End Function
Public Function AnyadirACN4AEstructura()
Dim nodeAct As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

scod1 = m_oACN4.ACN1 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN4.ACN1)))
scod2 = m_oACN4.ACN2 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN4.ACN2)))
scod3 = m_oACN4.ACN3 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN4.ACN3)))
scod4 = m_oACN4.ID & Mid$("                         ", 1, 10 - Len(CStr(m_oACN4.ID)))

Set nodeAct = frmActiv.tvwEstrAct.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, m_oACN4.Cod & " - " & m_oACN4.Den, "ACT")

nodeAct.Tag = "ACN4" & m_oACN4.ID

nodeAct.Selected = True
frmActiv.tvwEstrAct_NodeClick nodeAct
nodeAct.Selected = True

Set nodeAct = Nothing

End Function
Public Function AnyadirACN5AEstructura()
Dim nodeAct As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

scod1 = m_oACN5.ACN1 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN5.ACN1)))
scod2 = m_oACN5.ACN2 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN5.ACN2)))
scod3 = m_oACN5.ACN3 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN5.ACN3)))
scod4 = m_oACN5.ACN4 & Mid$("                         ", 1, 10 - Len(CStr(m_oACN5.ACN4)))
scod5 = m_oACN5.ID & Mid$("                         ", 1, 10 - Len(CStr(m_oACN5.ID)))

Set nodeAct = frmActiv.tvwEstrAct.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & scod5, m_oACN5.Cod & " - " & m_oACN5.Den, "ACT")

nodeAct.Tag = "ACN5" & m_oACN5.ID

nodeAct.Selected = True
frmActiv.tvwEstrAct_NodeClick nodeAct
nodeAct.Selected = True

Set nodeAct = Nothing

End Function
Public Function ModificarGMEnEstructura(ByVal sDen As String)

Dim nodeAct As MSComctlLib.Node

Set nodeAct = frmActiv.tvwEstrAct.SelectedItem

nodeAct.Text = Trim(txtCod.Text) & " - " & sDen

nodeAct.Selected = True
frmActiv.tvwEstrAct_NodeClick nodeAct
nodeAct.Selected = True

Set nodeAct = Nothing

End Function



Private Sub Form_Unload(Cancel As Integer)
Set m_oACN1 = Nothing
Set m_oACN2 = Nothing
Set m_oACN3 = Nothing
Set m_oACN4 = Nothing
Set m_oACN5 = Nothing
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRACTDETALLE, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblCod.Caption = Ador(0).Value & ":"
        sIdiCodigo = Ador(0).Value
        Ador.MoveNext
        Me.lblDen.Caption = Ador(0).Value & ":"
        sIdiDenominacion = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub



