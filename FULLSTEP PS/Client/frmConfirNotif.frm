VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmConfirNotif 
   BackColor       =   &H00808000&
   Caption         =   "Confirmación de notificación"
   ClientHeight    =   2220
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8310
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConfirNotif.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   8310
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4680
      TabIndex        =   5
      Top             =   1680
      Width           =   1455
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   2880
      TabIndex        =   4
      Top             =   1680
      Width           =   1455
   End
   Begin SSDataWidgets_B.SSDBCombo ssdbApellidos 
      Height          =   285
      Left            =   3960
      TabIndex        =   3
      Top             =   480
      Width           =   3255
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Caption=   "Apellidos"
      Columns(0).Name =   "Apellidos"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Email"
      Columns(2).Name =   "Email"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBCombo ssdbNombre 
      Height          =   285
      Left            =   1920
      TabIndex        =   2
      Top             =   480
      Width           =   1815
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Caption=   "Nombre"
      Columns(0).Name =   "Nombre"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "Apellidos"
      Columns(1).Name =   "Apellidos"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Email"
      Columns(2).Name =   "Email"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   3201
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.TextBox txtMail 
      Height          =   285
      Left            =   1920
      TabIndex        =   0
      Top             =   1080
      Width           =   5295
   End
   Begin VB.Label lblUsuNotif 
      BackColor       =   &H00808000&
      Caption         =   "Usuario a notificar:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   540
      Width           =   1575
   End
End
Attribute VB_Name = "frmConfirNotif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oCiaSeleccionada As CCia
Public g_oUsuarioSeleccionado As CUsuario
Dim m_sNombrePorDefecto As String
Dim m_sApellidosPorDefecto As String
Dim m_sEmailPorDefecto As String

Public bCancelar As Boolean

Private Sub cmdAceptar_Click()
 On Error GoTo error:
        
    
    Me.Hide
    
fin:
    Exit Sub
error:
    Resume fin
End Sub

Private Sub cmdCancelar_Click()
    bCancelar = True
    Me.Hide
End Sub


Private Sub Form_Load()
    Dim oUsuarios As CUsuarios
    bCancelar = False
    
    Me.Top = (frmMDI.Height - Me.Height) / 2
    Me.Left = (frmMDI.Width - Me.Width) / 2
    CargarRecursos
    If g_oUsuarioSeleccionado Is Nothing Then
        Set oUsuarios = g_oCiaSeleccionada.CargarTodosLosUsuariosDesde
        
        If IsNull(g_oCiaSeleccionada.UsuarioPrincipal) Then
            For Each g_oUsuarioSeleccionado In oUsuarios
                Exit For
            Next
        Else
            Set g_oUsuarioSeleccionado = oUsuarios.Item(CStr(g_oCiaSeleccionada.UsuarioPrincipal))
        End If
    End If
    
    m_sNombrePorDefecto = g_oUsuarioSeleccionado.nombre
    m_sApellidosPorDefecto = g_oUsuarioSeleccionado.Apellidos
    m_sEmailPorDefecto = g_oUsuarioSeleccionado.Email
    Set oUsuarios = Nothing
    ssdbNombre.Text = m_sNombrePorDefecto
    ssdbApellidos.Text = m_sApellidosPorDefecto
    txtMail.Text = m_sEmailPorDefecto
End Sub

Private Sub Form_Unload(Cancel As Integer)
    bCancelar = True
    Set Me.g_oUsuarioSeleccionado = Nothing
    Set Me.g_oCiaSeleccionada = Nothing
End Sub

Private Sub ssdbApellidos_Click()
    If Not ssdbApellidos.DroppedDown Then
        ssdbApellidos.Text = ""
        ssdbNombre.Text = ""
    End If
End Sub

Private Sub ssdbApellidos_CloseUp()
    ssdbNombre.Text = ssdbApellidos.Columns.Item(1).Text
    ssdbApellidos.Text = ssdbApellidos.Columns.Item(0).Text
    txtMail.Text = ssdbApellidos.Columns.Item(2).Text
End Sub

Private Sub ssdbApellidos_DropDown()
    Dim oUsuarios As CUsuarios
    Dim oUsuarioActual As CUsuario
    
    ssdbApellidos.RemoveAll
    Set oUsuarios = g_oCiaSeleccionada.CargarTodosLosUsuariosDesde
    For Each oUsuarioActual In oUsuarios
        ssdbApellidos.AddItem oUsuarioActual.Apellidos & Chr(9) & oUsuarioActual.nombre & Chr(9) & oUsuarioActual.Email
    Next
    Set oUsuarios = Nothing
    Set oUsuarioActual = Nothing
End Sub

Private Sub ssdbApellidos_InitColumnProps()
    ssdbApellidos.DataFieldList = "Column 0"
    ssdbApellidos.DataFieldToDisplay = "Column 0"
End Sub

Private Sub ssdbNombre_Change()
    If (ssdbNombre.Text = "") Then
        ssdbApellidos.Text = ""
        txtMail.Text = ""
    End If
End Sub

Private Sub ssdbApellidos_Change()
    If (ssdbApellidos.Text = "") Then
        ssdbNombre.Text = ""
        txtMail.Text = ""
    End If
End Sub
Private Sub ssdbNombre_Click()
    If Not ssdbNombre.DroppedDown Then
        ssdbNombre.Text = ""
        ssdbApellidos.Text = ""
    End If
End Sub

Private Sub ssdbNombre_CloseUp()
    ssdbApellidos.Text = ssdbNombre.Columns.Item(1).Text
    ssdbNombre.Text = ssdbNombre.Columns.Item(0).Text
    txtMail.Text = ssdbNombre.Columns.Item(2).Text
End Sub

Private Sub ssdbNombre_DropDown()
    Dim oUsuarios As CUsuarios
    Dim oUsuarioActual As CUsuario
    
    ssdbNombre.RemoveAll
    Set oUsuarios = g_oCiaSeleccionada.CargarTodosLosUsuariosDesde
    For Each oUsuarioActual In oUsuarios
        ssdbNombre.AddItem oUsuarioActual.nombre & Chr(9) & oUsuarioActual.Apellidos & Chr(9) & oUsuarioActual.Email
    Next
    Set oUsuarios = Nothing
    Set oUsuarioActual = Nothing
End Sub

Private Sub ssdbNombre_InitColumnProps()
    ssdbNombre.DataFieldList = "Column 0"
    ssdbNombre.DataFieldToDisplay = "Column 0"
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIRNOTIF, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblUsuNotif.Caption = Ador(0).Value
        Ador.MoveNext
        Me.ssdbApellidos.Columns(0).Caption = Ador(0).Value
        Me.ssdbNombre.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.ssdbApellidos.Columns(1).Caption = Ador(0).Value
        Me.ssdbNombre.Columns(0).Caption = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub




