VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstRegComunic 
   Caption         =   "DListado del hist�rico de comunicaciones"
   ClientHeight    =   2700
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4995
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstRegComunic.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2700
   ScaleWidth      =   4995
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      Height          =   375
      Left            =   3600
      TabIndex        =   0
      Top             =   2260
      Width           =   1335
   End
   Begin TabDlg.SSTab ssTabListado 
      Height          =   2215
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   3916
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstRegComunic.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraCias"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstRegComunic.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "frameOrden"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraCias 
         Height          =   1675
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   4725
         Begin VB.OptionButton optTodos 
            Caption         =   "Listado Completo"
            Height          =   255
            Left            =   225
            TabIndex        =   12
            Top             =   315
            Value           =   -1  'True
            Width           =   1695
         End
         Begin VB.OptionButton optSelec 
            Caption         =   "Proveedor"
            Height          =   255
            Left            =   225
            TabIndex        =   11
            Top             =   720
            Width           =   1095
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
            Height          =   285
            Left            =   465
            TabIndex        =   13
            Top             =   980
            Width           =   1140
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   7064
            Columns(2).Caption=   "Denominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   2011
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
            Height          =   285
            Left            =   1635
            TabIndex        =   14
            Top             =   980
            Width           =   2930
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3201
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3201
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   5168
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame frameOrden 
         Height          =   1675
         Left            =   -74880
         TabIndex        =   2
         Top             =   360
         Width           =   4725
         Begin VB.OptionButton optAsunto 
            Caption         =   "Asunto"
            Height          =   255
            Left            =   3240
            TabIndex        =   9
            Top             =   1320
            Width           =   1300
         End
         Begin VB.OptionButton optCCO 
            Caption         =   "CCo"
            Height          =   255
            Left            =   3240
            TabIndex        =   8
            Top             =   960
            Width           =   1300
         End
         Begin VB.OptionButton optCC 
            Caption         =   "CC"
            Height          =   255
            Left            =   3240
            TabIndex        =   7
            Top             =   600
            Width           =   1215
         End
         Begin VB.OptionButton optPara 
            Caption         =   "Para"
            Height          =   255
            Left            =   3240
            TabIndex        =   6
            Top             =   240
            Width           =   1215
         End
         Begin VB.OptionButton optFecha 
            Caption         =   "Fecha"
            Height          =   255
            Left            =   240
            TabIndex        =   5
            Top             =   240
            Value           =   -1  'True
            Width           =   1095
         End
         Begin VB.OptionButton optCod 
            Caption         =   "C�digo de compa��a"
            Height          =   255
            Left            =   240
            TabIndex        =   4
            Top             =   600
            Width           =   3135
         End
         Begin VB.OptionButton optDen 
            Caption         =   "Denominaci�n de compa��a"
            Height          =   255
            Left            =   240
            TabIndex        =   3
            Top             =   960
            Width           =   3015
         End
      End
   End
End
Attribute VB_Name = "frmLstRegComunic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_adores_adorSubRpt As Ador.Recordset
Public g_adoresAdjun As Ador.Recordset

Public bRespetarCombo As Boolean

Private bCargarComboDesde As Boolean
Private CargarComboDesde As Boolean
Private oCias As CCias

'Variables de idiomas
Private sIdiTxtTitulo As String
Private sIditxtCompania As String
Private sIditxtCodigo As String
Private sIdiTxtDen As String
Private sIdiTxtFecPub As String
Private sIdiTxtProve As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String
Private sIditxtFecha As String
Private sIditxtSubject As String
Private sIditxtPara As String
Private sIditxtCC As String
Private sIditxtCCo As String
Private sIditxtRespuestaA As String
Private sIditxtTipo As String
Private sIditxtAcuse As String
Private sIditxtSi As String
Private sIditxtNo As String
Private sIditxtTexto As String
Private sIditxtHTML As String
Private m_stxtAdjuntos As String

Private Sub Form_Load()
    Me.Height = 3210
    Me.Width = 5115
    
    CargarRecursos
    
    Set oCias = g_oRaiz.Generar_CCias
    
    If optTodos.Value = True Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        sdbcCiaCod.Enabled = False
        sdbcCiaDen.Enabled = False
    Else
        sdbcCiaCod.Enabled = True
        sdbcCiaDen.Enabled = True
    
    End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oCias = Nothing
End Sub

Private Sub cmdObtener_Click()
    If (optSelec.Value = True) And (sdbcCiaCod.Text = "") Then
        basMensajes.SeleccioneCia
        Exit Sub
    End If
    
    ''Obtiene el report de Procesos publicados
    ObtenerListado
End Sub

Private Sub ObtenerListado()
    Dim oReport As CRAXDRT.Report
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim iOrden As Integer
    Dim Table As CRAXDRT.DatabaseTable
    Dim sublistado As CRAXDRT.Report
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Comprueba que el el path sea correcto y que exista el informe
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptRegcomunic.rpt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oFos = Nothing
    
    'Abre el report
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = "'" & sIdiTxtTitulo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecha")).Text = "'" & sIditxtFecha & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodCia")).Text = "'" & sIditxtCodigo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenCia")).Text = "'" & sIdiTxtDen & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSubject")).Text = "'" & sIditxtSubject & ":" & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPara")).Text = "'" & sIditxtPara & ":" & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCC")).Text = "'" & sIditxtCC & ":" & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCCo")).Text = "'" & sIditxtCCo & ":" & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRespuestaA")).Text = "'" & sIditxtRespuestaA & ":" & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipo")).Text = "'" & sIditxtTipo & ":" & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAcuseRecibo")).Text = "'" & sIditxtAcuse & ":" & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSi")).Text = "'" & sIditxtSi & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNo")).Text = "'" & sIditxtNo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTexto")).Text = "'" & sIditxtTexto & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtHTML")).Text = "'" & sIditxtHTML & "'"
    
    iOrden = 0
    If optFecha.Value = True Then
        iOrden = 1
    ElseIf optCod.Value = True Then
        iOrden = 2
    ElseIf optDen.Value = True Then
        iOrden = 3
    ElseIf optAsunto.Value = True Then
        iOrden = 5
    ElseIf optCC.Value = True Then
        iOrden = 6
    ElseIf optCCO.Value = True Then
        iOrden = 7
    ElseIf optPara.Value = True Then
        iOrden = 4
    End If
    
    If optTodos.Value = True Then
        Set g_adores_adorSubRpt = g_oGestorInformes.DevolverHistoricoEmails(, iOrden)
    Else
        Set g_adores_adorSubRpt = g_oGestorInformes.DevolverHistoricoEmails(sdbcCiaCod.Columns(0).Value, iOrden)
    End If
    
    If g_adores_adorSubRpt Is Nothing Then
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    oReport.Database.SetDataSource g_adores_adorSubRpt
    
    
    'SubListado
    Set sublistado = oReport.OpenSubreport("rptAdjunEmail")
    sublistado.FormulaFields(crs_FormulaIndex(sublistado, "txtAdjuntos")).Text = """" & m_stxtAdjuntos & """"
    
    If optTodos.Value = True Then
        Set g_adoresAdjun = g_oGestorInformes.DevolverAdjuntosEmails
    Else
        Set g_adoresAdjun = g_oGestorInformes.DevolverAdjuntosEmails(sdbcCiaCod.Columns(0).Value)
    End If
    
    If Not g_adoresAdjun Is Nothing Then
        sublistado.Database.SetDataSource g_adoresAdjun
    End If
    Set sublistado = Nothing
    
    
    'Imprime el informe
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   
    Set pv = New Preview
    pv.g_sOrigen = "frmLstRegComunic"
    pv.Hide
    pv.Caption = Me.Caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    
    Unload Me
    
    Set oReport = Nothing
    
    Unload Me
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTREGCOMUNIC, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        sIdiTxtTitulo = Ador(0).Value
        Ador.MoveNext
        optSelec.Caption = Ador(0).Value & ":"
        sIditxtCompania = Ador(0).Value
        
        Ador.MoveNext
        ssTabListado.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        ssTabListado.TabCaption(1) = Ador(0).Value
        
        Ador.MoveNext
        optFecha.Caption = Ador(0).Value
        sIditxtFecha = Ador(0).Value
        Ador.MoveNext
        optCod.Caption = Ador(0).Value
        sIditxtCodigo = Ador(0).Value
        sdbcCiaCod.Columns(1).Caption = Ador(0).Value
        sdbcCiaDen.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        optDen.Caption = Ador(0).Value
        sIdiTxtDen = Ador(0).Value
        sdbcCiaCod.Columns(2).Caption = Ador(0).Value
        sdbcCiaDen.Columns(1).Caption = Ador(0).Value
        
        Ador.MoveNext
        optAsunto.Caption = Ador(0).Value
        sIditxtSubject = Ador(0).Value
        Ador.MoveNext
        optPara.Caption = Ador(0).Value
        sIditxtPara = Ador(0).Value
        Ador.MoveNext
        optCC.Caption = Ador(0).Value
        sIditxtCC = Ador(0).Value
        Ador.MoveNext
        optCCO.Caption = Ador(0).Value
        sIditxtCCo = Ador(0).Value
        
        Ador.MoveNext
        cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        sIdiTxtPag = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDe = Ador(0).Value
        Ador.MoveNext
        optTodos.Caption = Ador(0).Value
        
        Ador.MoveNext
        sIditxtRespuestaA = Ador(0).Value
        Ador.MoveNext
        sIditxtTipo = Ador(0).Value
        Ador.MoveNext
        sIditxtAcuse = Ador(0).Value
        Ador.MoveNext
        sIditxtSi = Ador(0).Value
        Ador.MoveNext
        sIditxtNo = Ador(0).Value
        Ador.MoveNext
        sIditxtTexto = Ador(0).Value
        Ador.MoveNext
        sIditxtHTML = Ador(0).Value
        Ador.MoveNext
        m_stxtAdjuntos = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

Private Sub optSelec_Click()
    sdbcCiaCod.Enabled = True
    sdbcCiaDen.Enabled = True
End Sub

Private Sub optTodos_Click()
    sdbcCiaDen.Text = ""
    sdbcCiaCod.Text = ""
    sdbcCiaCod.Enabled = False
    sdbcCiaDen.Enabled = False
End Sub

Private Sub sdbcCiaCod_Change()
    If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        bRespetarCombo = False
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcCiaCod_Click()
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_CloseUp()
    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Text = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    bRespetarCombo = True
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaCod.RemoveAll

    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, , , , , , , , , , , , , , 4)
    Else
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, , , , , , , , , , , , , , 4)
    End If
    
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("FCEST").Value & Chr(9) & Ador("FPEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh
End Sub

Private Sub sdbcCiaCod_InitColumnProps()
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Public Sub sdbcCiaCod_Validate(Cancel As Boolean)
    Dim Ador As Ador.Recordset

    If Trim(sdbcCiaCod.Text) = "" Then Exit Sub
    
    Set Ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , , , , , , , , , , , , , , 4)
    
    If Ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If Ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(Ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcCiaDen.Text = Ador("DEN").Value
                sdbcCiaCod.Text = Ador("COD").Value
                sdbcCiaCod.Columns(0).Value = Ador("ID").Value
                bRespetarCombo = False
                bCargarComboDesde = False
            End If
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    End If
End Sub

Private Sub sdbcCiaDen_Change()
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaCod.Text = ""
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcCiaDen_Click()
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_CloseUp()
     If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    sdbcCiaCod.Columns("ID").Value = sdbcCiaDen.Columns("ID").Value
    bRespetarCombo = False
    
    bCargarComboDesde = False

End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaDen.RemoveAll
    sdbcCiaDen.ReBind
    
    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, , , , , , , , , , , , , , 4)
    Else
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, , , , , , , , , , , , , , 4)
    End If

    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("FCEST").Value & Chr(9) & Ador("FPEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub

Private Sub sdbcCiaDen_InitColumnProps()
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbcCiaDen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
