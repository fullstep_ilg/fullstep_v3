VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstProcPub 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de procesos publicados"
   ClientHeight    =   2565
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4965
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstProcPub.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2565
   ScaleWidth      =   4965
   Begin VB.CommandButton cmdObtener 
      Caption         =   "Obtener"
      Height          =   375
      Left            =   3600
      TabIndex        =   0
      Top             =   2160
      Width           =   1335
   End
   Begin TabDlg.SSTab ssTabListado 
      Height          =   2115
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   3731
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstProcPub.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstProcPub.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "frameOrden"
      Tab(1).ControlCount=   1
      Begin VB.Frame frameOrden 
         Height          =   1575
         Left            =   -74880
         TabIndex        =   4
         Top             =   360
         Width           =   4725
         Begin VB.OptionButton optDen 
            Caption         =   "Denominaci�n "
            Height          =   255
            Left            =   360
            TabIndex        =   6
            Top             =   960
            Width           =   3015
         End
         Begin VB.OptionButton optCod 
            Caption         =   "C�digo "
            Height          =   255
            Left            =   360
            TabIndex        =   5
            Top             =   400
            Value           =   -1  'True
            Width           =   3135
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1575
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   4725
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaCod 
            Height          =   285
            Left            =   960
            TabIndex        =   7
            Top             =   720
            Width           =   885
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "Denominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "FC"
            Columns(3).Name =   "FC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FP"
            Columns(4).Name =   "FP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   1561
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCiaDen 
            Height          =   285
            Left            =   1890
            TabIndex        =   8
            Top             =   720
            Width           =   2745
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Caption=   "C�digo"
            Columns(2).Name =   "COD"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   4842
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
         End
         Begin VB.Label lblCia 
            Caption         =   "Compa��a :"
            Height          =   255
            Left            =   120
            TabIndex        =   3
            Top             =   760
            Width           =   1065
         End
      End
   End
End
Attribute VB_Name = "frmLstProcPub"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private bCargarComboDesde As Boolean
Public bRespetarCombo As Boolean
Private CargarComboDesde As Boolean
Private oCias As CCias
Public g_adores_adorSubRpt As Ador.Recordset

Private sIdiTxtTitulo As String
Private sIditxtCompania As String
Private sIditxtCodigo As String
Private sIdiTxtDen As String
Private sIdiTxtFecPub As String
Private sIdiTxtProve As String
Private sIdiTxtPag As String
Private sIdiTxtDe As String


Private Sub cmdObtener_Click()
    ''Obtiene el report de Procesos publicados
    ObtenerListado
End Sub

Private Sub Form_Load()
    Me.Height = 2970
    Me.Width = 5055
    CargarRecursos
    Set oCias = g_oRaiz.Generar_CCias
    
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora = True Then
        ssTabListado.TabVisible(0) = False
    End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set oCias = Nothing
End Sub

Private Sub sdbcCiaCod_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaCod.RemoveAll

    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, Trim(sdbcCiaCod.Text), , False, False, True)
    Else
        Set Ador = oCias.DevolverCiasDesde(g_iCargaMaximaCombos, , , False, False, True)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaCod.AddItem Ador("ID").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("FCEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaCod.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaCod.SelStart = 0
    sdbcCiaCod.SelLength = Len(sdbcCiaCod.Text)
    sdbcCiaCod.Refresh

End Sub
Private Sub sdbcCiaCod_InitColumnProps()
    
    sdbcCiaCod.DataField = "Column 1"
    sdbcCiaCod.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaCod_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaCod_Click()
    
    If Not sdbcCiaCod.DroppedDown Then
        sdbcCiaCod = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaCod_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaCod.Rows - 1
            bm = sdbcCiaCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaCod.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCiaCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaCod_CloseUp()

    If sdbcCiaCod.Value = "-1" Then
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaCod.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaCod.Columns(2).Text
    sdbcCiaCod.Text = sdbcCiaCod.Columns(1).Text
    
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
End Sub

Private Sub sdbcCiaCod_Validate(Cancel As Boolean)
Dim Ador As Ador.Recordset

    If Trim(sdbcCiaCod.Text) = "" Then Exit Sub
    
    Set Ador = oCias.DevolverCiasDesde(1, sdbcCiaCod.Text, , True, , True)
    
    If Ador Is Nothing Then
        
        sdbcCiaCod = ""
        Exit Sub
        
    Else
        If Ador.EOF Then
            sdbcCiaCod.Value = ""
            Exit Sub
        Else
            If UCase(sdbcCiaCod.Text) <> UCase(Ador("COD").Value) Then
                sdbcCiaCod.Value = ""
                Exit Sub
            Else
                bRespetarCombo = True
                sdbcCiaDen.Text = Ador("DEN").Value
                bRespetarCombo = False
                bCargarComboDesde = False

            End If
        End If
        
        Ador.Close
        Set Ador = Nothing
        
    End If
    
End Sub

Private Sub sdbcCiaDen_DropDown()
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    sdbcCiaDen.RemoveAll

    If bCargarComboDesde Then
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , Trim(sdbcCiaDen.Text), , True, True)
    Else
        Set Ador = oCias.DevolverCiasDesde(basParametros.g_iCargaMaximaCombos, , , False, True, True)
    End If
    
    If Not Ador Is Nothing Then
        
        i = 1
        
        While Not Ador.EOF And i <= g_iCargaMaximaCombos
            i = i + 1
            sdbcCiaDen.AddItem Ador("ID").Value & Chr(9) & Ador("DEN").Value & Chr(9) & Ador("COD").Value & Chr(9) & Ador("FCEST").Value
            Ador.MoveNext
        Wend
        
        If Not Ador.EOF Then
            sdbcCiaDen.AddItem "-1" & Chr(9) & "..." & Chr(9) & "..."
        End If
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    sdbcCiaDen.SelStart = 0
    sdbcCiaDen.SelLength = Len(sdbcCiaDen.Text)
    sdbcCiaDen.Refresh
    
End Sub
Private Sub sdbcCiaDen_InitColumnProps()
    
    sdbcCiaDen.DataField = "Column 1"
    sdbcCiaDen.DataFieldToDisplay = "Column 1"
    
End Sub
Private Sub sdbcCiaDen_Change()

    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcCiaDen.Text = ""
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcCiaDen_Click()
    
    If Not sdbcCiaDen.DroppedDown Then
        sdbcCiaDen = ""
        sdbcCiaDen = ""
    End If
End Sub

Private Sub sdbcCiaDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCiaDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCiaDen.Rows - 1
            bm = sdbcCiaDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCiaDen.Columns(2).CellText(bm), 1, Len(Text))) Then
                sdbcCiaDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub
Private Sub sdbcCiaDen_CloseUp()

    If sdbcCiaDen.Columns(0).Value = "-1" Then
        sdbcCiaDen.Text = ""
        sdbcCiaCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCiaDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcCiaDen.Text = sdbcCiaDen.Columns(1).Text
    sdbcCiaCod.Text = sdbcCiaDen.Columns(2).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
    
End Sub


Private Sub ObtenerListado()
    Dim oReport As CRAXDRT.Report
    Dim pv As Preview
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim Ador As Ador.Recordset
    Dim Table As CRAXDRT.DatabaseTable
    Dim GroupOrder1 As String
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Comprueba que el el path sea correcto y que exista el informe
    If basParametros.g_sRptPath = "" Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    RepPath = basParametros.g_sRptPath & "\rptProcPub.rpt"

    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        basMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set oFos = Nothing
    
    'Obtiene el servidor y BD de GS de la Cia compradora
    If basParametros.g_udtParametrosGenerales.g_bUnaCompradora = True Then
        Set Ador = oCias.DevolverCiasCompradoras(, , , basParametros.g_udtParametrosGenerales.g_lIdCiaCompradora)
    Else
        
        If sdbcCiaCod.Text <> "" Then
            Set Ador = oCias.DevolverCiasCompradoras(sdbcCiaCod.Text, , True)
        Else
            Screen.MousePointer = vbNormal
            basMensajes.SeleccioneCia
            Exit Sub
        End If
    End If
    
    'Comprueba si se puede conectar con la compa��a
    If Ador Is Nothing Then
        Screen.MousePointer = vbNormal
        basMensajes.ConexionIncorrecta
        Exit Sub
    End If
    If IsNull(Ador("SRV")) Or Ador("SRV") = "" Then
        Screen.MousePointer = vbNormal
        basMensajes.ConexionIncorrecta
        Exit Sub
    End If
    If IsNull(Ador("BD")) Or Ador("BD") = "" Then
        Screen.MousePointer = vbNormal
        basMensajes.ConexionIncorrecta
        Exit Sub
    End If
    
    
    'Obtiene los procesos publicados para la cia seleccionada
    Set g_adores_adorSubRpt = g_oGestorInformes.DevolverProcesosPublicados(Ador("CIA"), Ador("ID"), Ador("DEN"), Ador("SRV"), Ador("BD"))
    
    If g_adores_adorSubRpt Is Nothing Then
        Screen.MousePointer = vbNormal
        basMensajes.ConexionIncorrecta
        Exit Sub
    End If
    
    'Abre el report
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    'Le pasa los datos al informe
    oReport.Database.SetDataSource g_adores_adorSubRpt
    
    'Orden del informe
    If optDen.Value = True Then
        GroupOrder1 = "{ProcPub_ttx.PROCEDEN}"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "GRUPO")).Text = GroupOrder1
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "Titulo")).Text = "'" & sIdiTxtTitulo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCompania")).Text = "'" & sIditxtCompania & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = "'" & sIditxtCodigo & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDen")).Text = "'" & sIdiTxtDen & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecPub")).Text = "'" & sIdiTxtFecPub & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = "'" & sIdiTxtProve & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = "'" & sIdiTxtPag & "'"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = "'" & sIdiTxtDe & "'"
    
    'Imprime el informe
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   
    Set pv = New Preview
    
    pv.Hide
    pv.g_sOrigen = "frmLstProcPub"
    pv.Caption = Me.Caption
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    
    Set oReport = Nothing
    
    Unload Me
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPROCPUB, g_udtParametrosGenerales.g_sIdioma)
   
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        sIdiTxtTitulo = Ador(0).Value
        Ador.MoveNext
        Me.cmdObtener.Caption = Ador(0).Value
        Ador.MoveNext
        Me.lblCia.Caption = Ador(0).Value & ":"
        sIditxtCompania = Ador(0).Value
        Ador.MoveNext
        Me.optCod.Caption = Ador(0).Value
        sIditxtCodigo = Ador(0).Value
        Me.sdbcCiaCod.Columns(1).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(2).Caption = Ador(0).Value
        Ador.MoveNext
        Me.optDen.Caption = Ador(0).Value
        sIdiTxtDen = Ador(0).Value
        Me.sdbcCiaCod.Columns(2).Caption = Ador(0).Value
        Me.sdbcCiaDen.Columns(1).Caption = Ador(0).Value
        Ador.MoveNext
        Me.ssTabListado.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        Me.ssTabListado.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sIdiTxtFecPub = Ador(0).Value
        Ador.MoveNext
        sIdiTxtProve = Ador(0).Value
        Ador.MoveNext
        sIdiTxtPag = Ador(0).Value
        Ador.MoveNext
        sIdiTxtDe = Ador(0).Value
        
        
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub

