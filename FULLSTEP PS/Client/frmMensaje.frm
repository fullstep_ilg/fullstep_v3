VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMensaje 
   Caption         =   "Mensaje de e-mail desde FullStep PS"
   ClientHeight    =   7950
   ClientLeft      =   2115
   ClientTop       =   1890
   ClientWidth     =   9585
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMensaje.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7950
   ScaleWidth      =   9585
   Begin SHDocVwCtl.WebBrowser wbMyBrowser 
      Height          =   3985
      Left            =   0
      TabIndex        =   19
      Top             =   3940
      Visible         =   0   'False
      Width           =   9555
      ExtentX         =   16854
      ExtentY         =   7029
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   885
      Left            =   0
      Picture         =   "frmMensaje.frx":09CA
      ScaleHeight     =   885
      ScaleWidth      =   9585
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   0
      Width           =   9585
      Begin VB.CommandButton cmdEnviar 
         Appearance      =   0  'Flat
         Caption         =   "Enviar"
         Height          =   705
         Left            =   90
         Picture         =   "frmMensaje.frx":393E8
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   90
         Width           =   915
      End
      Begin VB.CommandButton cmdCancelar 
         Appearance      =   0  'Flat
         Caption         =   "Cancelar"
         Height          =   705
         Left            =   1230
         Picture         =   "frmMensaje.frx":396F2
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   90
         Width           =   915
      End
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3085
      Left            =   0
      TabIndex        =   11
      Top             =   840
      Width           =   9555
      Begin VB.TextBox txtRespuestaA 
         Height          =   285
         Left            =   2120
         TabIndex        =   5
         Top             =   1290
         Width           =   7245
      End
      Begin VB.CheckBox chkAcuseRecibo 
         Caption         =   "DPedir acuse de recibo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2120
         TabIndex        =   9
         Top             =   2760
         Width           =   6255
      End
      Begin VB.TextBox txtBcc 
         Height          =   285
         Left            =   2120
         TabIndex        =   4
         Top             =   930
         Width           =   7245
      End
      Begin VB.TextBox txtCc 
         Height          =   285
         Left            =   2120
         TabIndex        =   3
         Top             =   570
         Width           =   7245
      End
      Begin VB.CommandButton cmdAdjuntar 
         Caption         =   "Adjuntar"
         Height          =   705
         Left            =   8420
         Picture         =   "frmMensaje.frx":3983C
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   1995
         UseMaskColor    =   -1  'True
         Width           =   945
      End
      Begin VB.TextBox txtDestino 
         Height          =   285
         Left            =   2120
         TabIndex        =   2
         Top             =   210
         Width           =   7245
      End
      Begin VB.TextBox txtAsunto 
         Height          =   285
         Left            =   2120
         TabIndex        =   6
         Top             =   1650
         Width           =   7245
      End
      Begin MSComctlLib.ListView lstvwAdjuntos 
         Height          =   675
         Left            =   2120
         TabIndex        =   7
         Top             =   2010
         Width           =   6225
         _ExtentX        =   10980
         _ExtentY        =   1191
         View            =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "FicheroAdjunto"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label lblRespuestaA 
         Caption         =   "Enviar la respuesta a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   1350
         Width           =   1960
      End
      Begin VB.Label lblBcc 
         Caption         =   "Cco:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   990
         Width           =   1700
      End
      Begin VB.Label lblCc 
         Caption         =   "Cc:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   630
         Width           =   1700
      End
      Begin VB.Label lblDestino 
         Caption         =   "Para:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   270
         Width           =   1700
      End
      Begin VB.Label lblAsunto 
         Caption         =   "Asunto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   1710
         Width           =   1700
      End
      Begin VB.Label lblAdjuntos 
         Caption         =   "Archivos adjuntos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   120
         TabIndex        =   12
         Top             =   2070
         Width           =   1960
      End
   End
   Begin VB.TextBox txtMensaje 
      Height          =   3985
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   10
      Top             =   3940
      Width           =   9555
   End
   Begin MSComDlg.CommonDialog cmmdAdjuntar 
      Left            =   7260
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7920
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMensaje.frx":398AE
            Key             =   "ADJ"
            Object.Tag             =   "ADJ"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmMensaje"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_idCiaSeleccionada As Long
Public g_bHTML As Boolean
Public g_sCuerpo As String

Private sTemp As String
Private iNumElmtos As Integer
Private oFos As Scripting.FileSystemObject
Private sCuerpoName As String

'Variables para Multilenguaje
Private sIdiSelAdjuntos As String
Private sIdiTodos As String
Private sIdiDirNoValida As String
Private bCargando As Boolean

Private Sub cmdAdjuntar_Click()
Dim sFileName As String
Dim sFileTitle As String
Dim oFile As File
On Error GoTo ErrAdjuntar
    
    cmmdAdjuntar.CancelError = True
    cmmdAdjuntar.DialogTitle = sIdiSelAdjuntos
    cmmdAdjuntar.Flags = cdlOFNHideReadOnly
    cmmdAdjuntar.Filter = sIdiTodos & "|*.*"
    cmmdAdjuntar.ShowOpen
    
    sFileName = cmmdAdjuntar.FileName
    sFileTitle = cmmdAdjuntar.FileTitle
    If sFileName = "" Then Exit Sub
    iNumElmtos = iNumElmtos + 1
    lstvwAdjuntos.ListItems.Add , "NEW" & CStr(iNumElmtos), sFileTitle, , "ADJ"
    
    Set oFile = oFos.GetFile(sFileName)
    oFile.Copy sTemp & sFileTitle, True
    
Exit Sub
    
ErrAdjuntar:
    
    Exit Sub
End Sub


Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdEnviar_Click()
Dim Item As MSComctlLib.ListItem
Dim errormail As TipoErrorSummit
Dim arAdjuntos() As String
Dim i As Integer
Dim sEmail As String
Dim ListaMail() As String

    If txtDestino = "" Then
        basMensajes.errormail sIdiDirNoValida
        Exit Sub
    ElseIf Not ComprobarEmail(txtDestino.Text) Then
        
        sEmail = Trim(txtDestino.Text)
        If Right(sEmail, 1) = ";" Then sEmail = Left(sEmail, Len(sEmail) - 1)
        ListaMail = Split(sEmail, ";")
        
        errormail.NumError = erroressummit.TESmailsmtp
        errormail.Arg1 = -1000000
        errormail.Arg2 = Replace(txtDestino.Text, ";", ",") & ",###" & CStr(UBound(ListaMail) + 1)
    
        basMensajes.ErrorOriginal errormail
        Exit Sub
    End If
    cmdEnviar.Enabled = False
    Screen.MousePointer = vbHourglass
    
    ReDim arAdjuntos(0)
    i = 0
    For Each Item In lstvwAdjuntos.ListItems
        arAdjuntos(i) = Item.Text
        ReDim Preserve arAdjuntos(i + 1)
        i = i + 1
    Next
    
    If i > 0 Then
        errormail = EnviarMensaje(txtDestino.Text, txtAsunto.Text, IIf(g_bHTML = True, g_sCuerpo, txtMensaje.Text), arAdjuntos, Trim(txtCC.Text), Trim(txtBcc.Text), SQLBinaryToBoolean(chkAcuseRecibo.Value), g_bHTML, Trim(txtRespuestaA.Text), g_idCiaSeleccionada)
    Else
        errormail = EnviarMensaje(txtDestino.Text, txtAsunto.Text, IIf(g_bHTML = True, g_sCuerpo, txtMensaje.Text), , Trim(txtCC.Text), Trim(txtBcc.Text), SQLBinaryToBoolean(chkAcuseRecibo.Value), g_bHTML, Trim(txtRespuestaA.Text), g_idCiaSeleccionada)
    End If
    
    If errormail.NumError <> TESnoerror Then
        basMensajes.ErrorOriginal errormail
    End If
    
    cmdEnviar.Enabled = True
    Screen.MousePointer = vbNormal
    
    Unload Me
    
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbNormal
    CargarRecursos
    
    txtCC.Text = NullToStr(g_udtParametrosGenerales.gsSMTPCC)
    txtBcc.Text = NullToStr(g_udtParametrosGenerales.gsSMTPCCO)
    txtRespuestaA.Text = NullToStr(g_udtParametrosGenerales.gsSMTPRespuestaMail)
     
    If g_udtParametrosGenerales.gbAcuseRecibo Then
        chkAcuseRecibo.Value = vbChecked
    Else
        chkAcuseRecibo.Value = vbUnchecked
    End If
    
    sTemp = basUtilidades.DevolverPathFichTemp
    Set oFos = New Scripting.FileSystemObject

End Sub

Private Sub Form_Resize()
    'Redimensiona el formulario:
    If Me.Width < 4000 Then Exit Sub
    If Me.Height < 4000 Then Exit Sub
    
    txtMensaje.Height = Me.Height - txtMensaje.Top - 530
    txtMensaje.Width = Me.Width - 150
    
    wbMyBrowser.Height = Me.Height - wbMyBrowser.Top - 530
    wbMyBrowser.Width = Me.Width - 150
    
    Frame1.Width = Me.Width - 150
    txtAsunto.Width = Me.Width - 2460
    txtBcc.Width = Me.Width - 2460
    txtCC.Width = Me.Width - 2460
    txtDestino.Width = Me.Width - 2460
    txtRespuestaA.Width = Me.Width - 2460
    lstvwAdjuntos.Width = Me.Width - 3480

    cmdAdjuntar.Left = Me.Width - 1285
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim Item As MSComctlLib.ListItem
Dim bErroresEnElBorrado As Boolean

On Error GoTo error:

    Screen.MousePointer = vbHourglass
    
    g_sOrigen = ""
    g_idCiaSeleccionada = 0
    g_sCuerpo = ""
    
    'Elimina los adjuntos generados en el fic. temporal
    For Each Item In lstvwAdjuntos.ListItems
        If Left(Item.Key, 3) = "NEW" Then
          If oFos.FileExists(sTemp & Item.Text) Then
              oFos.DeleteFile sTemp & Item.Text, True
          End If
        End If
    Next
    
    If g_bHTML = True Then   'Si es html elimina el fichero .htm generado
        oFos.DeleteFile sTemp & sCuerpoName, True
        g_bHTML = False
    End If
    
    
    Set oFos = Nothing
    Screen.MousePointer = vbDefault
    Exit Sub

error:
    
    Select Case Err.Number
        
        Case 1
            
        Case 2
            bErroresEnElBorrado = True
    End Select
    
    Resume Next
    
End Sub


Private Sub lstvwAdjuntos_DblClick()
Dim Item As MSComctlLib.ListItem
Dim sFileName As String

    Set Item = lstvwAdjuntos.SelectedItem
    
    If Item Is Nothing Then
        basMensajes.SeleccioneFichero
    Else
        sFileName = sTemp & Item.Text
        'Lanzamos la aplicacion
        ShellExecute frmMDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
    
End Sub

Private Sub lstvwAdjuntos_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 Then
    ' suprimir
        If lstvwAdjuntos.SelectedItem Is Nothing Then
            basMensajes.SeleccioneFichero
        Else
            lstvwAdjuntos.ListItems.Remove (CStr(lstvwAdjuntos.SelectedItem.Key))
            lstvwAdjuntos.Refresh
        End If
    End If
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = g_oGestorIdiomas.DevolverTextosDelModulo(FRM_MENSAJE, g_udtParametrosGenerales.g_sIdioma)
    
    If Not Ador Is Nothing Then
        Caption = Ador(0).Value
        Ador.MoveNext
        cmdEnviar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        lblDestino.Caption = Ador(0).Value
        Ador.MoveNext
        lblAsunto.Caption = Ador(0).Value
        Ador.MoveNext
        lblAdjuntos.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAdjuntar.Caption = Ador(0).Value
        Ador.MoveNext
        
        sIdiSelAdjuntos = Ador(0).Value
        Ador.MoveNext
        sIdiTodos = Ador(0).Value
        Ador.MoveNext
        sIdiDirNoValida = Ador(0).Value
        Ador.MoveNext
        lblBcc.Caption = Ador(0).Value
        Ador.MoveNext
        chkAcuseRecibo.Caption = Ador(0).Value

        Ador.MoveNext
        lblCc.Caption = Ador(0).Value
        Ador.MoveNext
        lblRespuestaA.Caption = Ador(0).Value
        
        Ador.Close
    
    End If
    
    Set Ador = Nothing

End Sub

Private Sub wbMyBrowser_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, Flags As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)
    If bCargando = True Then Exit Sub
    If URL <> sTemp & "Cuerpo.htm" Then Cancel = True
End Sub

Public Sub CargarCuerpoMensaje()
    Dim datafile As Integer
    
    If g_bHTML = True Then
        bCargando = True
        wbMyBrowser.Visible = True
        txtMensaje.Visible = False
        
        sCuerpoName = "Cuerpo" & CStr(Year(Now())) & CStr(Month(Now())) & CStr(Day(Now())) & CStr(Hour(Now())) & CStr(Minute(Now())) & CStr(Second(Now())) & ".htm"
        
        datafile = 1
        Open sTemp & sCuerpoName For Binary Access Write As datafile
        Put datafile, , g_sCuerpo
        
        DoEvents
        wbMyBrowser.Navigate2 sTemp & sCuerpoName

        Close datafile
        datafile = 0
    
        bCargando = False
        
    Else
        'Es de tipo texto
        wbMyBrowser.Visible = False
        txtMensaje.Visible = True
        
        txtMensaje.Text = g_sCuerpo
    End If
End Sub

