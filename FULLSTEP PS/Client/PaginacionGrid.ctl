VERSION 5.00
Begin VB.UserControl PaginacionGrid 
   ClientHeight    =   285
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3120
   ScaleHeight     =   285
   ScaleWidth      =   3120
   Begin VB.CommandButton cmdLast 
      Height          =   285
      Left            =   2790
      Picture         =   "PaginacionGrid.ctx":0000
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   315
   End
   Begin VB.CommandButton cmdNext 
      Height          =   285
      Left            =   2445
      Picture         =   "PaginacionGrid.ctx":00AD
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   0
      Width           =   315
   End
   Begin VB.CommandButton cmdPrevious 
      Height          =   285
      Left            =   345
      Picture         =   "PaginacionGrid.ctx":014F
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   0
      Width           =   315
   End
   Begin VB.CommandButton cmdFirst 
      Height          =   285
      Left            =   0
      Picture         =   "PaginacionGrid.ctx":01F1
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   0
      Width           =   315
   End
   Begin VB.Label lblPageNumber 
      Alignment       =   2  'Center
      Caption         =   "DPagina {n} de {m}"
      Height          =   195
      Left            =   705
      TabIndex        =   4
      Top             =   45
      Width           =   1680
   End
End
Attribute VB_Name = "PaginacionGrid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Event OnFirstClick()
Public Event OnLastClick()
Public Event OnNextClick(ByVal iPaginaActual As Integer)
Public Event OnPreviousClick(ByVal iPaginaActual As Integer)

Private m_iNumPaginas As Integer
Private m_iPaginaActual As Integer
Private m_sPageCaption As String

Public Property Get NumPaginas() As Integer
    NumPaginas = m_iNumPaginas
End Property

Public Property Let NumPaginas(ByVal iNewValue As Integer)
    m_iNumPaginas = iNewValue
    PrintPageCaption
    ControlBotones
End Property

Public Property Get PaginaActual() As Integer
    PaginaActual = m_iPaginaActual
End Property

Public Property Let PaginaActual(ByVal iNewValue As Integer)
    m_iPaginaActual = iNewValue
    PrintPageCaption
    ControlBotones
End Property

Public Property Get PageCaption() As String
    PageCaption = m_sPageCaption
End Property

Public Property Let PageCaption(ByVal sNewValue As String)
    m_sPageCaption = sNewValue
End Property

Private Sub cmdFirst_Click()
    m_iPaginaActual = 1
    ControlBotones
    PrintPageCaption
    
    RaiseEvent OnFirstClick
End Sub

Private Sub cmdLast_Click()
    m_iPaginaActual = m_iNumPaginas
    ControlBotones
    PrintPageCaption
    
    RaiseEvent OnLastClick
End Sub

Private Sub cmdNext_Click()
    If Not m_iNumPaginas = m_iPaginaActual Then m_iPaginaActual = m_iPaginaActual + 1
    ControlBotones
    PrintPageCaption
    
    RaiseEvent OnNextClick(m_iPaginaActual)
End Sub

Private Sub cmdPrevious_Click()
    If Not m_iPaginaActual = 1 Then m_iPaginaActual = m_iPaginaActual - 1
    ControlBotones
    PrintPageCaption
    
    RaiseEvent OnPreviousClick(m_iPaginaActual)
End Sub

Private Sub ControlBotones()
    cmdFirst.Enabled = True
    cmdPrevious.Enabled = True
    cmdNext.Enabled = True
    cmdLast.Enabled = True
        
    If m_iPaginaActual = 1 Then
        cmdFirst.Enabled = False
        cmdPrevious.Enabled = False
    ElseIf m_iPaginaActual = m_iNumPaginas Then
        cmdNext.Enabled = False
        cmdLast.Enabled = False
    End If
End Sub

Private Sub UserControl_Initialize()
    PrintPageCaption
End Sub

Private Sub PrintPageCaption()
    lblPageNumber.Caption = Replace(Replace(m_sPageCaption, "{n}", m_iPaginaActual), "{m}", m_iNumPaginas)
End Sub
