Attribute VB_Name = "bas_V_31900_4"
Option Explicit

Public Function CodigoDeActualizacion31900_03_13_09A31900_04_14_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_4_Tablas_000
        
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_4_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.14.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_09A31900_04_14_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_09A31900_04_14_00 = False
End Function

Private Sub V_31900_4_Tablas_000()
Dim sConsulta As String

sConsulta = "if not exists (select 1 from syscolumns where id = object_id('PARGEN_INTERNO') and name = 'COMPRUEBATFNO')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_INTERNO" & vbCrLf
sConsulta = sConsulta & "ADD COMPRUEBATFNO TINYINT NOT NULL DEFAULT (1)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CONF_CALIFICACIONES]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CONF_CALIFICACIONES](" & vbCrLf
sConsulta = sConsulta & "      [CIA] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "      [ID] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "      [UNQA_VISIBLE_TAMANO] [nvarchar](max) NULL," & vbCrLf
sConsulta = sConsulta & "      [VARCAL_VISIBLE] [nvarchar](max) NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CONF_CALIFICACIONES] PRIMARY KEY CLUSTERED" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "      [CIA] ASC," & vbCrLf
sConsulta = sConsulta & "      [ID] Asc" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_CALIFICACIONES]  WITH CHECK ADD  CONSTRAINT [FK_CONF_CALIFICACIONES_USU] FOREIGN KEY([CIA], [ID])" & vbCrLf
sConsulta = sConsulta & "References [dbo].[USU]([CIA], [ID])" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_CALIFICACIONES] CHECK CONSTRAINT [FK_CONF_CALIFICACIONES_USU]" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_4_Storeds_000()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_LOGGED_USER]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_LOGGED_USER]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_LOGGED_USER] " & vbCrLf
sConsulta = sConsulta & "   @CIAPORTAL INT," & vbCrLf
sConsulta = sConsulta & "   @CODUSU NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @PORTAL INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @UNA_COMPRADORA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIACOMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS_SRV NVARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS_BD NVARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @UNA_COMPRADORA=UNA_COMPRADORA FROM PARGEN_INTERNO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @UNA_COMPRADORA<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @CIACOMP=ID,@FSGS_SRV=FSGS_SRV,@FSGS_BD=FSGS_BD" & vbCrLf
sConsulta = sConsulta & "   FROM CIAS WITH(NOLOCK) WHERE FCEST=3" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT CIAS.COD AS PROVECOD_PORTAL,CIAS.ID AS PROVEID_PORTAL,REL_CIAS.COD_PROVE_CIA PROVECOD_GS," & vbCrLf
sConsulta = sConsulta & "       USU.COD USUCOD,USU.PWD USUPWD,USU.FECPWD USUFECPWD,USU.DECIMALFMT DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "       USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT,USU.DATEFMT DATEFMT,IDI.COD AS IDIOMA," & vbCrLf
sConsulta = sConsulta & "       @FSGS_SRV FSGS_SRV,@FSGS_BD FSGS_BD" & vbCrLf
sConsulta = sConsulta & "   FROM USU WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=@PORTAL" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CIAS WITH(NOLOCK) ON USU.CIA=CIAS.ID " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN REL_CIAS WITH(NOLOCK) ON CIAS.ID=REL_CIAS.CIA_PROVE AND REL_CIAS.CIA_COMP=@CIACOMP" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IDI WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE USU.COD=@CODUSU AND USU.CIA=@CIAPORTAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_CONFIGURACION_FICHA_CALIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_CONFIGURACION_FICHA_CALIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_CONFIGURACION_FICHA_CALIDAD] " & vbCrLf
sConsulta = sConsulta & "   @IDCIACOMP INT," & vbCrLf
sConsulta = sConsulta & "   @IDUSU INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT UNQA_VISIBLE_TAMANO,VARCAL_VISIBLE  " & vbCrLf
sConsulta = sConsulta & "   FROM CONF_CALIFICACIONES WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE ID=@IDUSU AND CIA=@IDCIACOMP " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GUARDAR_CONFIGURACION_FICHACALIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GUARDAR_CONFIGURACION_FICHACALIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GUARDAR_CONFIGURACION_FICHACALIDAD] " & vbCrLf
sConsulta = sConsulta & "   @IDCIACOMP INT," & vbCrLf
sConsulta = sConsulta & "   @IDUSU INT," & vbCrLf
sConsulta = sConsulta & "   @CONFIGUNQA NVARCHAR(MAX)," & vbCrLf
sConsulta = sConsulta & "   @CONFIGVARCAL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF EXISTS(SELECT ID FROM CONF_CALIFICACIONES WITH(NOLOCK) WHERE CIA=@IDCIACOMP AND ID=@IDUSU)" & vbCrLf
sConsulta = sConsulta & "       UPDATE CONF_CALIFICACIONES SET UNQA_VISIBLE_TAMANO=@CONFIGUNQA,VARCAL_VISIBLE=@CONFIGVARCAL" & vbCrLf
sConsulta = sConsulta & "       WHERE CIA=@IDCIACOMP AND ID=@IDUSU" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CONF_CALIFICACIONES (CIA,ID,UNQA_VISIBLE_TAMANO,VARCAL_VISIBLE)" & vbCrLf
sConsulta = sConsulta & "       VALUES(@IDCIACOMP,@IDUSU,@CONFIGUNQA,@CONFIGVARCAL)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_14_00A31900_04_14_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_4_Tablas_001
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.14.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_14_00A31900_04_14_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_14_00A31900_04_14_01 = False
End Function

Private Sub V_31900_4_Tablas_001()
    Dim sConsulta As String
    
    sConsulta = "ALTER TABLE PARGEN_DEF " & vbCrLf
    sConsulta = sConsulta & "ADD CARGAMAX int NOT NULL DEFAULT 500" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_03_14_01A31900_04_14_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_4_Storeds_002
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.14.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_14_01A31900_04_14_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_14_01A31900_04_14_02 = False
End Function


Private Sub V_31900_4_Storeds_002()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS] @CIA INT, @PROVE VARCHAR(50), @IDI VARCHAR(20) =NULL, @ID INT=0, @TIPO INT=0, @FECHA VARCHAR(10)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos el servidor y la base de datos del GS." & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos el c�digo y el identificador del PROVEEDOR en el GS." & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos las NOCONFORMIDADES cerradas." & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_NOCONFORMIDADES_CERRADAS @PROVE =@PROVE, @IDI=@IDI, @ID = @ID , @TIPO = @TIPO, @FECHA = @FECHA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @IDI VARCHAR(10), @ID INT, @TIPO INT, @FECHA VARCHAR(10)' , @PROVE = @PROVEGS, @IDI=@IDI, @ID=@ID, @TIPO=@TIPO, @FECHA=@FECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =32 AND W.ID>=17 AND W.ID<=19 ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @IDI AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CERTIFICADOS_PROVE @PROVE= @PROVE, @IDI = @IDI'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@IDI VARCHAR(3)',@PROVE= @PROVEGS, @IDI = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =118 AND ((W.ID>=9 AND W.ID<=13) OR W.ID=21) ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_03_14_02A31900_04_14_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_4_Storeds_003
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.14.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_14_02A31900_04_14_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_14_02A31900_04_14_03 = False
End Function


Private Sub V_31900_4_Storeds_003()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_COMPROBAR_PRECIOS_ITEMS_GRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_COMPROBAR_PRECIOS_ITEMS_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_COMPROBAR_PRECIOS_ITEMS_GRUPO] @ID INT, @GRUPO VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT COUNT(ID) as NUMITEMS" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND (PRECIO IS NOT NULL OR PRECIO2 IS NOT NULL OR PRECIO3 IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_03_14_03A31900_04_14_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_4_Storeds_004
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.14.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_14_03A31900_04_14_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_14_03A31900_04_14_04 = False
End Function


Private Sub V_31900_4_Storeds_004()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS] @CIA INT, @ADJUNTOSACT NVARCHAR(50), @ADJUNTOSNEW NVARCHAR(50),@LAST_ADJUNTO INT OUTPUT, @DEFECTO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_CREAR_ADJUNTOS_COPIADOS @ADJUNTOSACT =@ADJUNTOSACT ,@ADJUNTOSNEW =@ADJUNTOSNEW,@LAST_ADJUNTO=@LAST_ADJUNTO OUTPUT,@DEFECTO=@DEFECTO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ADJUNTOSACT NVARCHAR(50), @ADJUNTOSNEW NVARCHAR(50),@LAST_ADJUNTO INT OUTPUT, @DEFECTO INT', @ADJUNTOSACT =@ADJUNTOSACT ,@ADJUNTOSNEW =@ADJUNTOSNEW,@LAST_ADJUNTO=@LAST_ADJUNTO OUTPUT,@DEFECTO=@DEFECTO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub
