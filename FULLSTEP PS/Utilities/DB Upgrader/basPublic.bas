Attribute VB_Name = "basPublic"
Option Explicit

Public Declare Sub ExitProcess Lib "kernel32" (ByVal uExitCode As Long)

Private bTransaccionEnCurso As Boolean
Private sConsulta As String
Public gRDOCon As rdo.rdoConnection
Public gRDOErs As rdo.rdoErrors
Public Const gcUltimaVersion = "3.00.37.03" 'Versi�n ACTUAL
Public psVersionBD As String
Public gdatFecha As Date


Public gServer As String
Public gBD As String
Public gUID As String
Public gPWD As String

'Cod de los idiomas que van en las columnas TEXT_SPA,TEXT_ENG,TEXT_GER,TEXT_FRA
Public gIdiomaSPA As String
Public gIdiomaENG As String
Public gIdiomaGER As String
Public gIdiomaFRA As String

Public Function InsertWEBTEXT() As Boolean
Dim adoCon As New ADODB.Connection
Dim adores As ADODB.Recordset
Dim sSQL As String
Dim oRS As ADODB.Recordset
Dim adoFld As ADODB.Field
Dim oADOConnection As ADODB.Connection

    On Error GoTo Error:
    

Set oADOConnection = New ADODB.Connection
oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
oADOConnection.CursorLocation = adUseClient
oADOConnection.CommandTimeout = 120
    
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    
    
    
    adoCon.Open "Provider=Microsoft.Jet.OLEDB.4.0;" & "Data Source=" & App.Path & "\FSPSUpgrader.mdb;"
    adoCon.CursorLocation = adUseClient

'WEBTEXT
    sConsulta = "CREATE TABLE #ACT_CONSERVAR (MODULO INT, ID INT)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    
    sSQL = "SELECT * FROM ACT_CONSERVAR ORDER BY MODULO,ID"
    
    Set adores = New ADODB.Recordset
    
    adores.Open sSQL, adoCon, adOpenForwardOnly, adLockReadOnly
    
    
    While Not adores.EOF
        sConsulta = "INSERT INTO [dbo].[#ACT_CONSERVAR] VALUES (" & adores("MODULO").Value & ", " & adores("ID").Value & ")"
        ExecuteADOSQL oADOConnection, sConsulta
        adores.MoveNext
    Wend
    
    adores.Close
    Set adores = Nothing
    
    sConsulta = "DELETE WEBTEXT FROM WEBTEXT W WHERE NOT EXISTS (SELECT * FROM #ACT_CONSERVAR A WHERE A.MODULO = W.MODULO AND A.ID = W.ID)"
    ExecuteADOSQL oADOConnection, sConsulta

    
    sSQL = "SELECT * FROM ACT_WEBTEXT W LEFT JOIN ACT_CONSERVAR A ON W.MODULO = A.MODULO AND W.ID = A.ID WHERE A.MODULO IS NULL ORDER BY W.MODULO, W.ID"
    
    Set adores = New ADODB.Recordset
    
    adores.Open sSQL, adoCon, adOpenForwardOnly, adLockReadOnly
    
    While Not adores.EOF
        sConsulta = "INSERT INTO [dbo].[WEBTEXT] VALUES (" & adores("w.MODULO").Value & ", " & adores("w.ID").Value & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaFRA).Value)) & ")"
        ExecuteADOSQL oADOConnection, sConsulta
        adores.MoveNext
    Wend
    adores.Close
    Set adores = Nothing
        
        
    sSQL = "SELECT * FROM ACT_WEBTEXT W INNER JOIN ACT_CONSERVAR A ON W.MODULO = A.MODULO AND W.ID = A.ID ORDER BY W.MODULO, W.ID"
    
    Set adores = New ADODB.Recordset
    
    adores.Open sSQL, adoCon, adOpenForwardOnly, adLockReadOnly
    
    Dim SQL As String
    While Not adores.EOF
        sConsulta = "IF NOT EXISTS(SELECT * FROM WEBTEXT WHERE MODULO =" & adores("w.MODULO").Value & " AND ID=" & adores("w.ID").Value & ")" & vbCrLf _
                  & "INSERT INTO [dbo].[WEBTEXT] VALUES (" & adores("w.MODULO").Value & ", " & adores("w.ID").Value & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaFRA).Value)) & ")"
        ExecuteADOSQL oADOConnection, sConsulta
                
                
        sConsulta = "SELECT * FROM WEBTEXT WHERE MODULO =" & adores("w.MODULO").Value & " AND ID=" & adores("w.ID").Value
        
        SQL = ""
        Set oRS = New ADODB.Recordset
        oRS.Open sConsulta, oADOConnection, adOpenForwardOnly
        
        For Each adoFld In oRS.Fields
            Select Case adoFld.Name
                Case "MODULO", "ID"
                
                Case Else
                    If IsNull(adoFld.Value) Then
                        SQL = SQL & "UPDATE WEBTEXT SET " & adoFld.Name & "=" & StrToSQLNULL(adoFld.Value) & " WHERE MODULO =" & adores("w.MODULO").Value & " AND ID=" & adores("w.ID").Value & vbCrLf
                    End If
            End Select
        Next
        oRS.Close
        Set oRS = Nothing
        If SQL <> "" Then
            ExecuteADOSQL oADOConnection, SQL
        End If
        
        
        
        
        adores.MoveNext
    Wend
    adores.Close
    Set adores = Nothing
    
'WEBTEXTHELP
    sConsulta = "DELETE FROM WEBTEXTHELP"
    ExecuteADOSQL oADOConnection, sConsulta

    
    sSQL = "SELECT * FROM ACT_WEBTEXTHELP"
    
    Set adores = New ADODB.Recordset
    
    adores.Open sSQL, adoCon, adOpenForwardOnly, adLockReadOnly
    
    While Not adores.EOF
        sConsulta = "INSERT INTO [dbo].[WEBTEXTHELP] VALUES (" & adores("MODULO").Value & ", " & adores("ID").Value & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(adores(gIdiomaFRA).Value)) & ")"
        ExecuteADOSQL oADOConnection, sConsulta
        adores.MoveNext
    Wend
    adores.Close
    Set adores = Nothing
    
'FSPMWEBTEXT

    sConsulta = "CREATE TABLE #ACT_PM_CONSERVAR (MODULO INT, ID INT)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    
    sSQL = "SELECT * FROM ACT_WEBFSPMTEXT_CONS ORDER BY MODULO,ID"
    
    Set adores = New ADODB.Recordset
    
    adores.Open sSQL, adoCon, adOpenForwardOnly, adLockReadOnly
    
    
    While Not adores.EOF
        sConsulta = "INSERT INTO [dbo].[#ACT_PM_CONSERVAR] VALUES (" & adores("MODULO").Value & ", " & adores("ID").Value & ")"
        ExecuteADOSQL oADOConnection, sConsulta
        adores.MoveNext
    Wend
    
    adores.Close
    Set adores = Nothing
    
    sConsulta = "DELETE WEBFSPMTEXT FROM WEBFSPMTEXT W WHERE NOT EXISTS (SELECT * FROM #ACT_PM_CONSERVAR A WHERE A.MODULO = W.MODULO AND A.ID = W.ID)"
    ExecuteADOSQL oADOConnection, sConsulta

    'Create the connection.
    
    sSQL = "SELECT * FROM ACT_WEBFSPMTEXT W LEFT JOIN ACT_WEBFSPMTEXT_CONS A ON W.MODULO = A.MODULO AND W.ID = A.ID WHERE A.MODULO IS NULL ORDER BY W.MODULO, W.ID"
    
    Set adores = New ADODB.Recordset
    
    adores.Open sSQL, adoCon, adOpenForwardOnly, adLockReadOnly
    
    While Not adores.EOF
        sConsulta = "INSERT INTO [dbo].[WEBFSPMTEXT] VALUES (" & adores("w.MODULO").Value & ", " & adores("w.ID").Value & ", " & StrToSQLNULL(NullToStr(adores("TEXT_" & gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(adores("TEXT_" & gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(adores("TEXT_" & gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(adores("TEXT_" & gIdiomaFRA).Value)) & ")"
        ExecuteADOSQL oADOConnection, sConsulta
        adores.MoveNext
    Wend
    adores.Close
    Set adores = Nothing
        
        
    sSQL = "SELECT * FROM ACT_WEBFSPMTEXT W INNER JOIN ACT_WEBFSPMTEXT_CONS A ON W.MODULO = A.MODULO AND W.ID = A.ID ORDER BY W.MODULO, W.ID"
    
    Set adores = New ADODB.Recordset
    
    adores.Open sSQL, adoCon, adOpenForwardOnly, adLockReadOnly
    
    While Not adores.EOF
        sConsulta = "IF NOT EXISTS(SELECT * FROM WEBFSPMTEXT WHERE MODULO =" & adores("w.MODULO").Value & " AND ID=" & adores("w.ID").Value & ")" & vbCrLf _
                  & "INSERT INTO [dbo].[WEBFSPMTEXT] VALUES (" & adores("w.MODULO").Value & ", " & adores("w.ID").Value & ", " & StrToSQLNULL(NullToStr(adores("TEXT_" & gIdiomaSPA).Value)) & ", " & StrToSQLNULL(NullToStr(adores("TEXT_" & gIdiomaENG).Value)) & ", " & StrToSQLNULL(NullToStr(adores("TEXT_" & gIdiomaGER).Value)) & ", " & StrToSQLNULL(NullToStr(adores("TEXT_" & gIdiomaFRA).Value)) & ")"
        ExecuteADOSQL oADOConnection, sConsulta
                
                
        sConsulta = "SELECT * FROM WEBFSPMTEXT WHERE MODULO =" & adores("w.MODULO").Value & " AND ID=" & adores("w.ID").Value
        
        SQL = ""
        Set oRS = New ADODB.Recordset
        oRS.Open sConsulta, oADOConnection, adOpenForwardOnly
        
        For Each adoFld In oRS.Fields
            Select Case adoFld.Name
                Case "MODULO", "ID"
                
                Case Else
                    If IsNull(adoFld.Value) Then
                        SQL = SQL & "UPDATE WEBFSPMTEXT SET " & adoFld.Name & "=" & StrToSQLNULL(adoFld.Value) & " WHERE MODULO =" & adores("w.MODULO").Value & " AND ID=" & adores("w.ID").Value & vbCrLf
                    End If
            End Select
        Next
        oRS.Close
        Set oRS = Nothing
        If SQL <> "" Then
            ExecuteADOSQL oADOConnection, SQL
        End If
        
        
        
        
        adores.MoveNext
    Wend
    adores.Close
    Set adores = Nothing
        
    adoCon.Close
    
    ExecuteADOSQL oADOConnection, "COMMIT"
    bTransaccionEnCurso = False
    
    
    InsertWEBTEXT = True
fin:
    Exit Function

Error:
    
    If NumTransaccionesAbiertasADO(oADOConnection) > 0 Then
        ExecuteADOSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox Err.Description, vbCritical + vbOKOnly
    
    InsertWEBTEXT = False
    Resume fin
    Resume 0


End Function

Public Function Insert_SysMessages() As Boolean
On Error GoTo Error

ExecuteSQL gRDOCon, "EXEC sp_addmessage @msgnum =50009, @severity=16, @msgtext = '%s',@replace='replace', @LANG='us_english'"
ExecuteSQL gRDOCon, "EXEC sp_addmessage @msgnum =50009, @severity=16, @msgtext = '%s',@replace='replace'"

ExecuteSQL gRDOCon, "EXEC sp_addmessage @msgnum =50005, @severity=16, @msgtext = 'Only one buyer is allowed with actual settings ! Set PARGEN_INTERNO.UNA_COMPRADORA to 0 to allow more buyers.', @replace = 'replace', @lang='us_english'"
ExecuteSQL gRDOCon, "EXEC sp_addmessage @msgnum =50005, @severity=16, @msgtext = 'Only one buyer is allowed with actual settings ! Set PARGEN_INTERNO.UNA_COMPRADORA to 0 to allow more buyers.', @replace = 'replace'"

Insert_SysMessages = True

fin:
Exit Function

Error:

    Insert_SysMessages = False
    Resume fin
End Function


'Private Function Crear_SQL_Eliminar_Storeds() As String
'
'Dim sConsulta As String
'
'sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ANYA_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_ANYA_ADJUNTO] "
'
'sConsulta = sConsulta & vbCrLf & "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ANYA_OFERTA_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_ANYA_OFERTA_WEB] "
'
'sConsulta = sConsulta & vbCrLf & "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_MODIF_ITEM_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_MODIF_ITEM_OFE] "
'
'sConsulta = sConsulta & vbCrLf & "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_UPLOAD_ADJUN_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_UPLOAD_ADJUN_WEB] "
'
'Crear_SQL_Eliminar_Storeds = sConsulta
'End Function
'
'
'Public Function Crear_SP_ANYA_ADJUNTO() As String
'Dim sConsulta As String
'
'sConsulta = "CREATE PROCEDURE SP_ANYA_ADJUNTO @CIA_COMP int, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @PROVE varchar(50), @OFE smallint,@OFEWEB smallint, @NOM varchar(300), @COM varchar(500) , @ID int = null OUTPUT   AS "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "--SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_ANYA_ADJUNTO @ANYO= ' + CONVERT(VARCHAR,@ANYO) + ', @GMN1 = ''' + @GMN1 + ''', @PROCE = ' + convert(varchar,@PROCE)  + ', @PROVE=''' +  @PROVE + ''',@OFE=' +  CONVERT(VARCHAR,@OFE) +  ',@OFEWEB=' +  CONVERT(VARCHAR,@OFEWEB) +  ',@ID=' +  CONVERT(VARCHAR,@ID) +  ', @NOM=''' +  @NOM + ''', @COM=''' +  @COM + ''',@WEB=1' "
'sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_ANYA_ADJUNTO @ANYO= @ANYO, @GMN1 =@GMN1, @PROCE =@PROCE, @PROVE=@PROVE,@OFE=@OFE,@OFEWEB=@OFEWEB, @NOM= @NOM, @COM=@COM,@WEB=1,@ID=@ID OUTPUT' "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @OFE SMALLINT, @OFEWEB SMALLINT, @NOM VARCHAR(300), @COM VARCHAR(500), @ID INT OUTPUT' "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "/* Execute the string with the first parameter value. */ "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition, "
'sConsulta = sConsulta & vbCrLf & "                      @ANYO = @ANYO,  "
'sConsulta = sConsulta & vbCrLf & "         @GMN1 =@GMN1,  "
'sConsulta = sConsulta & vbCrLf & "                     @PROCE =@PROCE,  "
'sConsulta = sConsulta & vbCrLf & "                     @PROVE=@PROVE, "
'sConsulta = sConsulta & vbCrLf & "        @OFE=@OFE, "
'sConsulta = sConsulta & vbCrLf & "@OFEWEB=@OFEWEB, @NOM= @NOM, @COM=@COM,@ID=@ID OUTPUT "
'Crear_SP_ANYA_ADJUNTO = sConsulta
'End Function
'Function Crear_SP_ANYA_OFERTA_WEB() As String
'Dim sConsulta As String
'
'sConsulta = "CREATE PROCEDURE SP_ANYA_OFERTA_WEB @CIA_COMP int,  @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @CODMON VARCHAR(50),@FECVAL VARCHAR(10),@OBS text ,@NUMOFE INT OUTPUT , @NUMOFEWEB  INT OUTPUT  "
'sConsulta = sConsulta & vbCrLf & "AS "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "--SELECT @SQLSTRING = @LNKSERVER + 'SP_ANYA_OFERTA_WEB @ANYO=' + convert(varchar,@ANYO) + ', @GMN1 = ''' + @GMN1 + ''', @PROCE = ' + convert(varchar,@PROCE)  + ', @PROVE=''' +  @PROVE + ''',@CODMON=''' + @CODMON + ''', @FECVAL=''' + @FECVAL + ''',@OBS=''' + convert(nvarchar,@OBS) + ''',@NUMOFE=@NUMOFE,@NUMOFEWEB=@NUMOFEWEB ' "
'sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_ANYA_OFERTA_WEB @ANYO=@ANYO, @GMN1 =@GMN1, @PROCE =@PROCE, @PROVE=@PROVE,@CODMON=@CODMON, @FECVAL=@FECVAL,@OBS=@OBS,@NUMOFE=@NUMOFE OUTPUT ,@NUMOFEWEB=@NUMOFEWEB OUTPUT ' "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "/* Build the SQL string once.*/ "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @CODMON VARCHAR(50),@FECVAL VARCHAR(10),@OBS text ,@NUMOFE INT OUTPUT, @NUMOFEWEB  INT OUTPUT' "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "/* Execute the string with the first parameter value. */ "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition, "
'sConsulta = sConsulta & vbCrLf & "                      @ANYO = @ANYO,  "
'sConsulta = sConsulta & vbCrLf & "         @GMN1 =@GMN1,  "
'sConsulta = sConsulta & vbCrLf & "                     @PROCE =@PROCE,  "
'sConsulta = sConsulta & vbCrLf & "                     @PROVE=@PROVE, "
'sConsulta = sConsulta & vbCrLf & "                     @CODMON=@CODMON, "
'sConsulta = sConsulta & vbCrLf & "                     @FECVAL=@FECVAL, "
'sConsulta = sConsulta & vbCrLf & "                     @OBS=@OBS, "
'sConsulta = sConsulta & vbCrLf & "                     @NUMOFE=@NUMOFE OUTPUT, "
'sConsulta = sConsulta & vbCrLf & "         @NUMOFEWEB=@NUMOFEWEB OUTPUT  "
'sConsulta = sConsulta & vbCrLf & " "
'Crear_SP_ANYA_OFERTA_WEB = sConsulta
'End Function
'
'Function Crear_SP_MODIF_ITEM_OFE() As String
'Dim sConsulta As String
'
'sConsulta = "CREATE PROCEDURE SP_MODIF_ITEM_OFE @CIA_COMP int,  @PRECIO  AS VARCHAR(50),@CANTMAX AS VARCHAR(50),@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@ITEM INT,@PROVE VARCHAR(50) ,@OFE INT  AS "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200) "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = @LNKSERVER + 'SP_MODIF_ITEM_OFE  @PRECIO='+ CONVERT(VARCHAR,@PRECIO) + ' , @CANTMAX=' + CONVERT(VARCHAR,@CANTMAX) + ', @ANYO=' + convert(varchar,@ANYO) + ', @GMN1 = ''' + @GMN1 + ''', @PROCE = ' + convert(varchar,@PROCE)  + ', @ITEM = ' + convert(varchar,@ITEM)  + ', @PROVE=''' +  @PROVE + ''',@OFE=' +  CONVERT(VARCHAR,@OFE) +  ',@WEB=1' "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "EXEC SP_EXECUTESQL @SQLSTRING  "
'
'Crear_SP_MODIF_ITEM_OFE = sConsulta
'End Function
'
'Public Function Crear_SP_UPLOAD_ADJUN_WEB() As String
'Dim sConsulta As String
'
'
'sConsulta = "CREATE PROCEDURE SP_UPLOAD_ADJUN_WEB @CIA_COMP INT, @INIT INT,@ID INT,@DATA IMAGE  AS "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_UPLOAD_ADJUN_WEB @INIT=@INIT,@ID=@ID,@DATA=@DATA ' "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "/* Build the SQL string once.*/ "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@INIT INT, @ID INT, @DATA IMAGE' "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "/* Execute the string with the first parameter value. */ "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & " "
'sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition, @INIT = @INIT, @ID = @ID, @DATA = @DATA "
'sConsulta = sConsulta & vbCrLf & " "
'
'Crear_SP_UPLOAD_ADJUN_WEB = sConsulta
'
'End Function
'
'Private Function Crear_SP_DEVOLVER_PROCESOS_PROVE() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_PROVE (@CIA_COMP INTEGER,@PROVE VARCHAR(50),@NORMAL SMALLINT OUTPUT,@SUBASTA SMALLINT OUTPUT) AS"
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'    sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCESOS_PROVE_WEB @PROVE=@PROVE,@NORMAL=@NORMAL OUTPUT,@SUBASTA=@SUBASTA OUTPUT'"
'    sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@PROVE VARCHAR(50), @NORMAL SMALLINT OUTPUT, @SUBASTA SMALLINT OUTPUT'"
'    sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition, @PROVE = @PROVE, @NORMAL = @NORMAL OUTPUT , @SUBASTA = @SUBASTA OUTPUT"
'    sConsulta = sConsulta & vbCrLf & "RETURN"
'    sConsulta = sConsulta & vbCrLf & "END"
'
'    Crear_SP_DEVOLVER_PROCESOS_PROVE = sConsulta
'End Function
'
'Private Function Crear_SP_DEVOLVER_PROCESO() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESO (@CIA_COMP INTEGER,@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ACTIVA SMALLINT OUTPUT) AS"
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'    sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCESO_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@ACTIVA=@ACTIVA OUTPUT'"
'    sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ACTIVA SMALLINT OUTPUT'"
'    sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @ACTIVA = @ACTIVA OUTPUT"
'    sConsulta = sConsulta & vbCrLf & "RETURN"
'    sConsulta = sConsulta & vbCrLf & "END"
'
'    Crear_SP_DEVOLVER_PROCESO = sConsulta
'End Function
'
'Private Function Crear_SP_DEVOLVER_OFERTA_PROVE() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE (@CIA_COMP INTEGER,@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,"
'    sConsulta = sConsulta & vbCrLf & "@PROVE VARCHAR(50),@ADJUDICABLE AS SMALLINT,@COMPARABLE AS SMALLINT) AS"
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'    sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_OFERTA_PROVE_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@PROVE=@PROVE,@ADJUDICABLE=@ADJUDICABLE,@COMPARABLE=@COMPARABLE'"
'    sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50),@ADJUDICABLE AS SMALLINT,@COMPARABLE AS SMALLINT'"
'    sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @PROVE = @PROVE,@ADJUDICABLE=@ADJUDICABLE,@COMPARABLE=@COMPARABLE"
'    sConsulta = sConsulta & vbCrLf & "RETURN"
'    sConsulta = sConsulta & vbCrLf & "END"
'
'    Crear_SP_DEVOLVER_OFERTA_PROVE = sConsulta
'End Function
'
'
'Private Function Crear_SP_DEVOLVER_PUJAS_ITEM() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PUJAS_ITEM (@CIA_COMP INTEGER,@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER) AS"
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'    sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PUJAS_ITEM_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@ITEM=@ITEM'"
'    sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER'"
'    sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @ITEM = @ITEM"
'    sConsulta = sConsulta & vbCrLf & "RETURN"
'    sConsulta = sConsulta & vbCrLf & "END"
'
'    Crear_SP_DEVOLVER_PUJAS_ITEM = sConsulta
'End Function
'
'
'Private Function Crear_SQL_Eliminar_Storeds_2_7_25() As String
'    Dim sConsulta As String
'
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'    sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE] "
'
'    sConsulta = sConsulta & vbCrLf & "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'    sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_DEVOLVER_PROCESO] "
'
'    sConsulta = sConsulta & vbCrLf & "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'    sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE] "
'
'    sConsulta = sConsulta & vbCrLf & "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PUJAS_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'    sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_DEVOLVER_PUJAS_ITEM] "
'
'    sConsulta = sConsulta & vbCrLf & "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PEND_NOTIF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'    sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_ACTUALIZAR_PEND_NOTIF] "
'
'    sConsulta = sConsulta & vbCrLf & "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_MODIF_ITEM_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) "
'    sConsulta = sConsulta & vbCrLf & "drop procedure [dbo].[SP_MODIF_ITEM_OFE] "
'
'
'    Crear_SQL_Eliminar_Storeds_2_7_25 = sConsulta
'End Function
'
'
'Private Function CrearTablaPROVENOTIF() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[PROVENOTIF] ("
'    sConsulta = sConsulta & vbCrLf & "[CIACOMP] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[ANYO] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (3) NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[PROCE] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[PROCEDEN] [varchar] (500) NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[ITEMDEN] [varchar] (500) NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[PROVE] [varchar] (50) NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[PROVEDEN] [varchar] (500) NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[EMAIL] [varchar] (100) NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[CONTACTO] [varchar] (500) NULL"
'    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
'    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PROVENOTIF] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PROVENOTIF] PRIMARY KEY  NONCLUSTERED"
'    sConsulta = sConsulta & vbCrLf & "("
'    sConsulta = sConsulta & vbCrLf & "[CIACOMP],"
'    sConsulta = sConsulta & vbCrLf & "[ANYO],"
'    sConsulta = sConsulta & vbCrLf & "[GMN1],"
'    sConsulta = sConsulta & vbCrLf & "[PROCE],"
'    sConsulta = sConsulta & vbCrLf & "[ITEM],"
'    sConsulta = sConsulta & vbCrLf & "[PROVE]"
'    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
'
'    CrearTablaPROVENOTIF = sConsulta
'End Function
'
'Private Function Crear_SP_ACTUALIZAR_PEND_NOTIF() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_ACTUALIZAR_PEND_NOTIF (@CIACOD VARCHAR(50),@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER,"
'    sConsulta = sConsulta & vbCrLf & "@ITEM INTEGER,@PROVE VARCHAR(50),@PROCEDEN VARCHAR(500),@ITEMDEN VARCHAR(500),@EMAIL VARCHAR(100),"
'    sConsulta = sConsulta & vbCrLf & "@PROVEDEN VARCHAR(500),@CONTACTO VARCHAR(500)) AS"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @EMAIL2 AS VARCHAR(100)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @CONTACTO2 AS VARCHAR(500)"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @CIACOMP int"
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & "exec @CIACOMP = SP_SELECT_ID @CIACOD"
'    sConsulta = sConsulta & vbCrLf & "/*Si el email o el contacto vienen como nulos se obtienen de la tabla USU del portal*/"
'    sConsulta = sConsulta & vbCrLf & "IF @EMAIL IS NULL OR @CONTACTO IS NULL"
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & "SELECT @EMAIL2=EMAIL, @CONTACTO2 = NOM + ' ' + APE  FROM USU INNER JOIN CIAS ON USU.CIA=CIAS.ID AND USU.ID=CIAS.USUPPAL"
'    sConsulta = sConsulta & vbCrLf & "WHERE CIA=@CIACOMP"
'    sConsulta = sConsulta & vbCrLf & "INSERT INTO PROVENOTIF (CIACOMP,ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES"
'    sConsulta = sConsulta & vbCrLf & "(@CIACOMP,@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL2,@CONTACTO2)"
'    sConsulta = sConsulta & vbCrLf & "END"
'    sConsulta = sConsulta & vbCrLf & "ELSE"
'    sConsulta = sConsulta & vbCrLf & "INSERT INTO PROVENOTIF (CIACOMP,ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES"
'    sConsulta = sConsulta & vbCrLf & "(@CIACOMP,@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL,@CONTACTO)"
'    sConsulta = sConsulta & vbCrLf & "RETURN"
'    sConsulta = sConsulta & vbCrLf & "END"
'
'    Crear_SP_ACTUALIZAR_PEND_NOTIF = sConsulta
'End Function
'
'Function Crear_SP_MODIF_ITEM_OFE_2_7_25() As String
'Dim sConsulta As String
'
'sConsulta = "CREATE PROCEDURE SP_MODIF_ITEM_OFE @CIA_COMP int,  @PRECIO  AS FLOAT,@CANTMAX AS FLOAT,@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@ITEM INT,@PROVE VARCHAR(50) ,@OFE INT  AS "
'sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200) "
'sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500) "
'sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP "
'sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_MODIF_ITEM_OFE  @PRECIO=@PRECIO, @CANTMAX=@CANTMAX, @ANYO=@ANYO , @GMN1 = @GMN1 , @PROCE = @PROCE, @ITEM = @ITEM, @PROVE= @PROVE ,@OFE=@OFE,@WEB=1'"
'sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@PRECIO FLOAT, @CANTMAX FLOAT, @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT, @ITEM INT,@PROVE VARCHAR(50),@OFE INT'"
'sConsulta = sConsulta & vbCrLf & "EXEC SP_EXECUTESQL @SQLSTRING , @ParmDefinition, "
'sConsulta = sConsulta & vbCrLf & "@PRECIO=@PRECIO,  "
'sConsulta = sConsulta & vbCrLf & "@CANTMAX=@CANTMAX,"
'sConsulta = sConsulta & vbCrLf & "@ANYO = @ANYO, "
'sConsulta = sConsulta & vbCrLf & "@GMN1 =@GMN1,  "
'sConsulta = sConsulta & vbCrLf & "@PROCE =@PROCE, "
'sConsulta = sConsulta & vbCrLf & "@ITEM=@ITEM , "
'sConsulta = sConsulta & vbCrLf & "@PROVE=@PROVE,"
'sConsulta = sConsulta & vbCrLf & "@OFE=@OFE"
'
'Crear_SP_MODIF_ITEM_OFE_2_7_25 = sConsulta
'End Function
'
'Public Function CodigoDeActualizacion2_0_7_25A2_0_7_50() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    'Nuevo campo TFNO_MOVIL en la tabla USU
'    sConsulta = "ALTER TABLE [dbo].[USU] ADD "
'    sConsulta = sConsulta & vbCrLf & "TFNO_MOVIL [varchar] (20) NULL"
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Se pasar�n todos los tfnos de TFNO y TFNO2 que empiecen por 6 al campo TFNO_MOVIL
'    sConsulta = "UPDATE USU SET TFNO_MOVIL=TFNO WHERE TFNO LIKE '6%'"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = "UPDATE USU SET TFNO2=NULL,TFNO_MOVIL=TFNO2 WHERE TFNO2 LIKE '6%'"
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nueva store procedure SP_MODIF_OBJ_PROVE
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_MODIF_OBJ_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_MODIF_OBJ_PROVE]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = Crear_SP_MODIF_OBJ_PROVE()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Modificar la store procedure SP_ANYA_OFERTA_WEB
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ANYA_OFERTA_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ANYA_OFERTA_WEB]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = Modificar_SP_ANYA_OFERTA_WEB_2_7_50()
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Registro en compa��as compradoras:
'    'Crear tabla REL_COMP
'    sConsulta = CrearTabla_REL_COMP()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Crear tabla MREL_COMP
'    sConsulta = CrearTabla_MREL_COMP()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'A�adir el campo PENDCONFREG a la tabla CIAS
'    sConsulta = "ALTER TABLE [dbo].[CIAS] ADD "
'    sConsulta = sConsulta & vbCrLf & "PENDCONFREG [tinyint] NOT NULL CONSTRAINT DF_CIAS_PENDCONFREG DEFAULT 0"
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo store procedure SP_DEVOLVER_REGISTRO_COMPRADORAS
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_DEVOLVER_REGISTRO_COMPRADORAS()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Modificar el store procedure SP_ELIMINAR_COMPANIA
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ELIMINAR_COMPANIA]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ModificarSP_ELIMINAR_COMPANIA()
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.7.50'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_7_25A2_0_7_50 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_7_25A2_0_7_50 = False
'End Function
'
'Public Function CodigoDeActualizacion2_0_7_50A2_0_7_51() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    'Nuevos campos en USU
'    frmProgreso.lblDetalle = "A�adimos campos a USU"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    ExecuteSQL gRDOCon, A�adirCamposUSU_Formato
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.7.51'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_7_50A2_0_7_51 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_7_50A2_0_7_51 = False
'End Function
'
'Private Function Crear_SP_MODIF_OBJ_PROVE() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_MODIF_OBJ_PROVE (@CIA_COMP INTEGER,@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INTEGER ,"
'    sConsulta = sConsulta & vbCrLf & "@PROVE VARCHAR(50),@NUMOBJ INTEGER) AS"
'    sConsulta = sConsulta & vbCrLf & ""
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & ""
'    sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500) "
'    sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200) "
'    sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500) "
'    sConsulta = sConsulta & vbCrLf & "DECLARE @NUM INTEGER, @NUMNUE INTEGER,@NUMAREV INTEGER"
'    sConsulta = sConsulta & vbCrLf & " "
'    sConsulta = sConsulta & vbCrLf & "/*Obtengo el linked server */"
'    sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP "
'    sConsulta = sConsulta & vbCrLf & " "
'    sConsulta = sConsulta & vbCrLf & "/*Ejecuto el store procedure de GS*/"
'    sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_MODIF_OBJ_PROVE_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@PROVE=@PROVE,@NUMOBJ=@NUMOBJ,@NUM=@NUM OUTPUT,@NUMNUE=@NUMNUE OUTPUT,@NUMAREV=@NUMAREV OUTPUT'"
'    sConsulta = sConsulta & vbCrLf & "  "
'    sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50),@NUMOBJ INTEGER,@NUM INTEGER OUTPUT,@NUMNUE INTEGER OUTPUT,@NUMAREV INTEGER OUTPUT'  "
'    sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @PROVE = @PROVE,@NUMOBJ=@NUMOBJ,@NUM=@NUM OUTPUT,@NUMNUE=@NUMNUE OUTPUT,@NUMAREV=@NUMAREV OUTPUT"
'    sConsulta = sConsulta & vbCrLf & "  "
'    sConsulta = sConsulta & vbCrLf & "/*Actualiza REL_CIAS con los valores obtenidos del SP_MODIF_OBJ_PROVE_WEB*/"
'    sConsulta = sConsulta & vbCrLf & "UPDATE REL_CIAS SET PROCE_ACT=@NUM,PROCE_NUE=@NUMNUE,PROCE_REV=@NUMAREV WHERE CIA_COMP=@CIA_COMP AND COD_PROVE_CIA=@PROVE"
'    sConsulta = sConsulta & vbCrLf & ""
'    sConsulta = sConsulta & vbCrLf & "RETURN"
'    sConsulta = sConsulta & vbCrLf & "END"
'
'    Crear_SP_MODIF_OBJ_PROVE = sConsulta
'End Function
'
'Private Function Modificar_SP_ANYA_OFERTA_WEB_2_7_50() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_ANYA_OFERTA_WEB @CIA_COMP int,  @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @CODMON VARCHAR(50),@FECVAL VARCHAR(10),@OBS text ,@NUMOFE INT OUTPUT , @NUMOFEWEB  INT OUTPUT  "
'    sConsulta = sConsulta & vbCrLf & "AS "
'    sConsulta = sConsulta & vbCrLf & " "
'    sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500) "
'    sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200) "
'    sConsulta = sConsulta & vbCrLf & "DECLARE @PARMDEFINITION NVARCHAR(500) "
'    sConsulta = sConsulta & vbCrLf & "DECLARE @TODOS int"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @NUEVOS int"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @AREVISAR int "
'    sConsulta = sConsulta & vbCrLf & " "
'    sConsulta = sConsulta & vbCrLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP "
'    sConsulta = sConsulta & vbCrLf & "  "
'    sConsulta = sConsulta & vbCrLf & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_ANYA_OFERTA_WEB @ANYO=@ANYO, @GMN1 =@GMN1, @PROCE =@PROCE, @PROVE=@PROVE,@CODMON=@CODMON, @FECVAL=@FECVAL,@OBS='''',@NUMOFE=@NUMOFE OUTPUT ,@NUMOFEWEB=@NUMOFEWEB OUTPUT' "
'    sConsulta = sConsulta & vbCrLf & "  "
'    sConsulta = sConsulta & vbCrLf & "/* Build the SQL string once.*/ "
'    sConsulta = sConsulta & vbCrLf & " "
'    sConsulta = sConsulta & vbCrLf & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @CODMON VARCHAR(50),@FECVAL VARCHAR(10),@NUMOFE INT OUTPUT, @NUMOFEWEB  INT OUTPUT' "
'    sConsulta = sConsulta & vbCrLf & " "
'    sConsulta = sConsulta & vbCrLf & "/* Execute the string with the first parameter value. */ "
'    sConsulta = sConsulta & vbCrLf & "  "
'    sConsulta = sConsulta & vbCrLf & "EXECUTE sp_executesql @SQLString, @ParmDefinition, "
'    sConsulta = sConsulta & vbCrLf & "                      @ANYO = @ANYO,  "
'    sConsulta = sConsulta & vbCrLf & "         @GMN1 =@GMN1,  "
'    sConsulta = sConsulta & vbCrLf & "                     @PROCE =@PROCE,  "
'    sConsulta = sConsulta & vbCrLf & "                     @PROVE=@PROVE, "
'    sConsulta = sConsulta & vbCrLf & "                     @CODMON=@CODMON, "
'    sConsulta = sConsulta & vbCrLf & "                     @FECVAL=@FECVAL, "
'    sConsulta = sConsulta & vbCrLf & "                     @NUMOFE=@NUMOFE OUTPUT, "
'    sConsulta = sConsulta & vbCrLf & "         @NUMOFEWEB=@NUMOFEWEB OUTPUT "
'
'    Modificar_SP_ANYA_OFERTA_WEB_2_7_50 = sConsulta
'End Function
'
'Private Function CrearTabla_REL_COMP() As String
'Dim sConsulta As String
'
'sConsulta = "CREATE TABLE [dbo].[REL_COMP] ("
'sConsulta = sConsulta & vbCrLf & "[CIA_PROVE] [int] NOT NULL ,"
'sConsulta = sConsulta & vbCrLf & "[CIA_COMP] [int] NOT NULL"
'sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
'
'sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[REL_COMP] WITH NOCHECK ADD"
'sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_REL_COMP] PRIMARY KEY  CLUSTERED"
'sConsulta = sConsulta & vbCrLf & "("
'sConsulta = sConsulta & vbCrLf & "[CIA_PROVE],"
'sConsulta = sConsulta & vbCrLf & "[CIA_COMP]"
'sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
'
'CrearTabla_REL_COMP = sConsulta
'End Function
'
'Private Function CrearTabla_MREL_COMP() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[MREL_COMP] ("
'    sConsulta = sConsulta & vbCrLf & "[CIA_PROVE] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "[CIA_COMP] [int] NOT NULL"
'    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
'
'    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[MREL_COMP] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_MREL_COMP] PRIMARY KEY  CLUSTERED"
'    sConsulta = sConsulta & vbCrLf & "("
'    sConsulta = sConsulta & vbCrLf & "[CIA_PROVE],"
'    sConsulta = sConsulta & vbCrLf & "[CIA_COMP]"
'    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
'
'    CrearTabla_MREL_COMP = sConsulta
'End Function
'
'Private Function CrearSP_DEVOLVER_REGISTRO_COMPRADORAS() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_REGISTRO_COMPRADORAS @PROVE int  AS"
'    sConsulta = sConsulta & vbCrLf & "SELECT A.ID IDCIA, A.COD COD, A.DEN DEN,"
'    sConsulta = sConsulta & vbCrLf & "CASE WHEN B.CIA_COMP IS NULL THEN 0 ELSE 1 END REGISTRADA,"
'    sConsulta = sConsulta & vbCrLf & "CASE WHEN C.CIA_COMP IS NULL THEN 0 ELSE 1 END SOLICITADA"
'    sConsulta = sConsulta & vbCrLf & "FROM CIAS A LEFT JOIN REL_COMP B"
'    sConsulta = sConsulta & vbCrLf & "ON A.ID = B.CIA_COMP"
'    sConsulta = sConsulta & vbCrLf & "AND @PROVE = B.CIA_PROVE"
'    sConsulta = sConsulta & vbCrLf & "LEFT JOIN MREL_COMP C"
'    sConsulta = sConsulta & vbCrLf & "ON A.ID = C.CIA_COMP"
'    sConsulta = sConsulta & vbCrLf & "AND @PROVE = C.CIA_PROVE"
'    sConsulta = sConsulta & vbCrLf & "Where A.FCEST = 3"
'
'    CrearSP_DEVOLVER_REGISTRO_COMPRADORAS = sConsulta
'End Function
'
'Private Function ModificarSP_ELIMINAR_COMPANIA() As String
'Dim sConsulta As String
'
'sConsulta = "CREATE PROCEDURE SP_ELIMINAR_COMPANIA @ID INT AS"
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)"
'sConsulta = sConsulta & vbCrLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'sConsulta = sConsulta & vbCrLf & "DECLARE @LNKSERVER VARCHAR(200)"
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID"
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & "--SET NOCOUNT ON"
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & "OPEN curRELCIAS"
'sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
'sConsulta = sConsulta & vbCrLf & "BEGIN"
'sConsulta = sConsulta & vbCrLf & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'sConsulta = sConsulta & vbCrLf & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''"
'sConsulta = sConsulta & vbCrLf & " EXEC SP_EXECUTESQL @SQLSTRING "
'sConsulta = sConsulta & vbCrLf & "        "
'sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'sConsulta = sConsulta & vbCrLf & "End"
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & "CLOSE CURRELCIAS"
'sConsulta = sConsulta & vbCrLf & "DEALLOCATE CURRELCIAS"
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS_MON WHERE CIA = @ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS_COM WHERE CIA=@ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS_PORT WHERE CIA=@ID "
'sConsulta = sConsulta & vbCrLf & "DELETE FROM USU_PORT WHERE CIA=@ID"
'sConsulta = sConsulta & vbCrLf & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID"
'sConsulta = sConsulta & vbCrLf & "DELETE FROM USU WHERE CIA=@ID"
'sConsulta = sConsulta & vbCrLf & ""
'sConsulta = sConsulta & vbCrLf & "DELETE FROM CIAS WHERE ID=@ID"
'
'ModificarSP_ELIMINAR_COMPANIA = sConsulta
'End Function
'
'
'Private Function A�adirCamposUSU_Formato() As String
'Dim sConsulta As String
'
'sConsulta = "ALTER TABLE dbo.USU ADD "
'sConsulta = sConsulta & vbCrLf & "    THOUSANFMT char(1) NULL,"
'sConsulta = sConsulta & vbCrLf & "    DECIMALFMT char(1) NULL,"
'sConsulta = sConsulta & vbCrLf & "    PRECISIONFMT smallint NOT NULL CONSTRAINT DF_USU_PRECISIONFMT DEFAULT 2,"
'sConsulta = sConsulta & vbCrLf & "    DATEFMT varchar(50) NULL,"
'sConsulta = sConsulta & vbCrLf & "    MOSTRARFMT tinyint NOT NULL CONSTRAINT DF_USU_MOSTRARFMT DEFAULT 1"
'
'A�adirCamposUSU_Formato = sConsulta
'
'End Function
'
'Private Function CodigoDeActualizacion2_0_7_51A2_0_7_51_0() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    'Registramos las compa��as proveedoras en las cias
'    frmProgreso.lblDetalle = "Registramos las compa��as proveedoras en las cias"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'
'    'Inserta en MREL_COMP para cada cia compradora los proveedores dados de alta
'    sConsulta = "INSERT INTO MREL_COMP (CIA_PROVE,CIA_COMP) SELECT P.CIA, C.ID "
'    sConsulta = sConsulta & " FROM CIAS C ,CIAS_PORT P WHERE C.FCEST=3 AND P.FPEST>=1 "
'    sConsulta = sConsulta & " AND NOT EXISTS (SELECT * FROM MREL_COMP M WHERE M.CIA_PROVE=P.CIA AND M.CIA_COMP=C.ID) "
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Inserta en REL_COMP para cada cia compradora los proveedores autorizados
'    sConsulta = "INSERT INTO REL_COMP (CIA_PROVE,CIA_COMP) SELECT P.CIA, C.ID "
'    sConsulta = sConsulta & " FROM CIAS C ,CIAS_PORT P WHERE C.FCEST=3 AND P.FPEST=3 "
'    sConsulta = sConsulta & " AND NOT EXISTS (SELECT * FROM REL_COMP M WHERE M.CIA_PROVE=P.CIA AND M.CIA_COMP=C.ID) "
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.7.51.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_7_51A2_0_7_51_0 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_7_51A2_0_7_51_0 = False
'End Function
'
'
'
'Private Function CodigoDeActualizacion2_0_7_51_0A2_0_8_00_0() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    'Creamos los triggers de inserci�n autom�tica de actividades de niveles superiores
'    frmProgreso.lblDetalle = "Creamos los triggers de inserci�n autom�tica de actividades de niveles superiores"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'
'
'
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MCIAS_ACT4_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[MCIAS_ACT4_TG_INS]"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    sConsulta = "CREATE TRIGGER MCIAS_ACT4_TG_INS ON dbo.MCIAS_ACT4"
'    sConsulta = sConsulta & vbCrLf & " FOR INSERT"
'    sConsulta = sConsulta & vbCrLf & " AS"
'    sConsulta = sConsulta & vbCrLf & " DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER,@ACT3 INTEGER,@ACT4 INTEGER"
'    sConsulta = sConsulta & vbCrLf & " DECLARE curTG_MCia_Act4_Ins CURSOR FOR SELECT CIA,ACT1,ACT2,ACT3,ACT4 FROM INSERTED"
'    sConsulta = sConsulta & vbCrLf & " OPEN curTG_MCia_Act4_Ins"
'    sConsulta = sConsulta & vbCrLf & " FETCH NEXT FROM curTG_MCia_Act4_Ins INTO @CIA,@ACT1,@ACT2,@ACT3,@ACT4"
'    sConsulta = sConsulta & vbCrLf & " WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbCrLf & " BEGIN"
'    sConsulta = sConsulta & vbCrLf & "         "
'    sConsulta = sConsulta & vbCrLf & " DELETE FROM MCIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3= @ACT3 AND ACT4=@ACT4"
'    sConsulta = sConsulta & vbCrLf & " INSERT INTO MCIAS_ACT5 SELECT @CIA,ACT1,ACT2,ACT3,ACT4,ID FROM ACT5 WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3 AND ACT4=@ACT4"
'    sConsulta = sConsulta & vbCrLf & " FETCH NEXT FROM curTG_MCia_Act4_Ins INTO @CIA,@ACT1,@ACT2,@ACT3,@ACT4"
'    sConsulta = sConsulta & vbCrLf & " End"
'    sConsulta = sConsulta & vbCrLf & " "
'    sConsulta = sConsulta & vbCrLf & " Close curTG_MCia_Act4_Ins"
'    sConsulta = sConsulta & vbCrLf & " DEALLOCATE curTG_MCia_Act4_Ins"
'
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Creamos el trigger para sincronizar el flag de registro de compa��as pendiente de autorizar
'    frmProgreso.lblDetalle = "Creamos el trigger para sincronizar el flag de registro de compa��as pendiente de autorizar"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REL_COMP_TG_INS_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[REL_COMP_TG_INS_DEL]"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    sConsulta = "CREATE TRIGGER REL_COMP_TG_INS_DEL ON [dbo].[REL_COMP]"
'    sConsulta = sConsulta & vbCrLf & " FOR INSERT, DELETE"
'    sConsulta = sConsulta & vbCrLf & " AS"
'
'    sConsulta = sConsulta & vbCrLf & " DECLARE @PROVE varchar(40)"
'    sConsulta = sConsulta & vbCrLf & " "
'    sConsulta = sConsulta & vbCrLf & " DECLARE @MREL int"
'    sConsulta = sConsulta & vbCrLf & " DECLARE @REL int"
'    sConsulta = sConsulta & vbCrLf & " DECLARE @COINCIDENTES INT"
'
'    sConsulta = sConsulta & vbCrLf & " declare c1 cursor for Select cia_prove from inserted union Select cia_prove from deleted"
'
'    sConsulta = sConsulta & vbCrLf & " open c1"
'
'    sConsulta = sConsulta & vbCrLf & " FETCH NEXT FROM c1 INTO @PROVE"
'    sConsulta = sConsulta & vbCrLf & " WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbCrLf & "     BEGIN"
'    sConsulta = sConsulta & vbCrLf & "              SELECT @MREL = COUNT(*) FROM MREL_COMP WHERE CIA_PROVE=@PROVE"
'    sConsulta = sConsulta & vbCrLf & "              SELECT @REL = COUNT(*) FROM REL_COMP WHERE CIA_PROVE=@PROVE"
'    sConsulta = sConsulta & vbCrLf & "          SELECT @COINCIDENTES = COUNT(*)"
'    sConsulta = sConsulta & vbCrLf & "                FROM MREL_COMP M"
'    sConsulta = sConsulta & vbCrLf & "                         INNER JOIN REL_COMP R"
'    sConsulta = sConsulta & vbCrLf & "                                 ON M.CIA_PROVE = R.CIA_PROVE"
'    sConsulta = sConsulta & vbCrLf & "                                AND M.CIA_COMP = R.CIA_COMP"
'    sConsulta = sConsulta & vbCrLf & "               WHERE M.CIA_PROVE=@PROVE"
'    sConsulta = sConsulta & vbCrLf & "              IF @COINCIDENTES!=@MREL OR @COINCIDENTES != @REL"
'    sConsulta = sConsulta & vbCrLf & "                    UPDATE CIAS SET PENDCONFREG = 1"
'    sConsulta = sConsulta & vbCrLf & "                     WHERE ID=@PROVE"
'    sConsulta = sConsulta & vbCrLf & "              Else"
'    sConsulta = sConsulta & vbCrLf & "                    UPDATE CIAS SET PENDCONFREG = 0"
'    sConsulta = sConsulta & vbCrLf & "                    WHERE ID=@PROVE"
'    sConsulta = sConsulta & vbCrLf & "         FETCH NEXT FROM c1 INTO @PROVE"
'    sConsulta = sConsulta & vbCrLf & "     End"
'    sConsulta = sConsulta & vbCrLf & " Close c1"
'    sConsulta = sConsulta & vbCrLf & " DEALLOCATE c1"
'
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.8.00.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_7_51_0A2_0_8_00_0 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_7_51_0A2_0_8_00_0 = False
'End Function
'
'
'
'Private Function CodigoDeActualizacion2_0_8_0_0A2_0_8_00_1() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    'Creamos los triggers de inserci�n autom�tica de actividades de niveles superiores
'    frmProgreso.lblDetalle = "Creamos los triggers de inserci�n autom�tica de actividades de niveles superiores"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'
'
'
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVENOTIF]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) drop table [dbo].[PROVENOTIF]"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    sConsulta = "CREATE TABLE [dbo].[PROVENOTIF] ("
'    sConsulta = sConsulta & vbCrLf & "    [ID] [int] IDENTITY (1, 1) NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [CIACOMP] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [ANYO] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [GMN1] [varchar] (3) NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [PROCE] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [PROCEDEN] [varchar] (500) NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [ITEM] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [ITEMDEN] [varchar] (500) NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [PROVE] [varchar] (50) NOT NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [PROVEDEN] [varchar] (500) NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [EMAIL] [varchar] (100) NULL ,"
'    sConsulta = sConsulta & vbCrLf & "    [CONTACTO] [varchar] (500) NULL"
'    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = "ALTER TABLE [dbo].[PROVENOTIF] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbCrLf & "    CONSTRAINT [PK_PROVENOTIF] PRIMARY KEY  NONCLUSTERED"
'    sConsulta = sConsulta & vbCrLf & "    ("
'    sConsulta = sConsulta & vbCrLf & "        [ID]"
'    sConsulta = sConsulta & vbCrLf & "    )  ON [PRIMARY]"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.8.00.1'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_8_0_0A2_0_8_00_1 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_8_0_0A2_0_8_00_1 = False
'End Function
'
'
'
'
'Private Function CodigoDeActualizacion2_0_8_0_1A2_0_8_00_2() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'
'
'    'Creamos un trigger en REL_CIAS que evita enlazar dos proveedores del portal con uno solo del GS
'    frmProgreso.lblDetalle = "Creamos un trigger en REL_CIAS que evita enlazar dos proveedores del portal con uno solo del GS"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'
'    sConsulta = "if (select count(*) from master.dbo.sysmessages where error=50009)>0 exec sp_dropmessage 50009"
'    ExecuteSQL gRDOCon, sConsulta
'    ExecuteSQL gRDOCon, "EXEC sp_addmessage @msgnum =50009,@severity=16,@msgtext = '%s'"
'
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REL_CIAS_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[REL_CIAS_TG_UPD]"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'
'    sConsulta = "CREATE TRIGGER REL_CIAS_TG_UPD ON [dbo].[REL_CIAS]"
'    sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
'    sConsulta = sConsulta & vbCrLf & "AS"
'
'
'    sConsulta = sConsulta & vbCrLf & "DECLARE @CIACOMP int"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @CIAPROVE int"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @PROVECOD varchar(40)"
'
'    sConsulta = sConsulta & vbCrLf & "DECLARE @CODOLD varchar(40)"
'
'
'    sConsulta = sConsulta & vbCrLf & "DECLARE C1 CURSOR FOR SELECT cia_comp, cia_prove, cod_prove_cia from inserted"
'
'    sConsulta = sConsulta & vbCrLf & "open c1"
'
'    sConsulta = sConsulta & vbCrLf & "declare @PROVESYAENLAZADOS varchar(2000)"
'
'    sConsulta = sConsulta & vbCrLf & "SET @PROVESYAENLAZADOS =''"
'
'    sConsulta = sConsulta & vbCrLf & "fetch next FROM c1 into @CIACOMP, @CIAPROVE, @PROVECOD"
'    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & "       SELECT @CODOLD = c.cod"
'    sConsulta = sConsulta & vbCrLf & "         from rel_cias r"
'    sConsulta = sConsulta & vbCrLf & "               inner join cias c"
'    sConsulta = sConsulta & vbCrLf & "                       on r.cia_prove = c.id"
'    sConsulta = sConsulta & vbCrLf & "        where r.cia_comp = @CIACOMP"
'    sConsulta = sConsulta & vbCrLf & "          and r.cod_prove_cia = @PROVECOD"
'    sConsulta = sConsulta & vbCrLf & "          and r.cia_prove != @CIAPROVE"
'    sConsulta = sConsulta & vbCrLf & "       IF @CODOLD is not null"
'    sConsulta = sConsulta & vbCrLf & "          BEGIN"
'    sConsulta = sConsulta & vbCrLf & "            IF @PROVESYAENLAZADOS = ''"
'    sConsulta = sConsulta & vbCrLf & "                set @PROVESYAENLAZADOS = @PROVESYAENLAZADOS  + @CODOLD"
'     sConsulta = sConsulta & vbCrLf & "           Else"
'    sConsulta = sConsulta & vbCrLf & "                set @PROVESYAENLAZADOS = @PROVESYAENLAZADOS +  ', ' + @CODOLD"
'    sConsulta = sConsulta & vbCrLf & "          End"
'
'    sConsulta = sConsulta & vbCrLf & "       fetch next FROM c1 into @CIACOMP, @CIAPROVE, @PROVECOD"
'    sConsulta = sConsulta & vbCrLf & "   End"
'    sConsulta = sConsulta & vbCrLf & "Close C1"
'    sConsulta = sConsulta & vbCrLf & "DEALLOCATE C1"
'
'    sConsulta = sConsulta & vbCrLf & "if @PROVESYAENLAZADOS !=''"
'    sConsulta = sConsulta & vbCrLf & "    BEGIN"
'    sConsulta = sConsulta & vbCrLf & "    RAISERROR (50009,16,1,@PROVESYAENLAZADOS)"
'    sConsulta = sConsulta & vbCrLf & "    ROLLBACK TRANSACTION"
'    sConsulta = sConsulta & vbCrLf & "    End"
'
'    ExecuteSQL gRDOCon, sConsulta
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.8.00.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_8_0_1A2_0_8_00_2 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_8_0_1A2_0_8_00_2 = False
'End Function
'
'Private Function CodigoDeActualizacion2_0_8_0_2A2_0_9_00_0() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    'Actualizar campo tabla USU
'    frmProgreso.lblDetalle = "Actualizamos el campo PRECISIONFMT en la tabla USU"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU"
'    sConsulta = sConsulta & vbLf & "   DROP CONSTRAINT USU_FK_IDI"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU"
'    sConsulta = sConsulta & vbLf & "   DROP CONSTRAINT DF_USU_PRECISIONFMT"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU"
'    sConsulta = sConsulta & vbLf & "   DROP CONSTRAINT DF_USU_MOSTRARFMT"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "CREATE TABLE dbo.Tmp_USU"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   CIA int NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   ID int NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   COD varchar(20) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   PWD varchar(128) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   FECPWD datetime NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   APE varchar(100) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   NOM varchar(20) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   DEP varchar(100) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   CAR varchar(100) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   TFNO varchar(20) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   TFNO2 varchar(20) NULL,"
'    sConsulta = sConsulta & vbLf & "   FAX varchar(20) NULL,"
'    sConsulta = sConsulta & vbLf & "   EMAIL varchar(100) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   IDI int NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   FCEST int NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   FECINS datetime NULL,"
'    sConsulta = sConsulta & vbLf & "   FECLASTUPD datetime NULL,"
'    sConsulta = sConsulta & vbLf & "   ORIGINS varchar(50) NULL,"
'    sConsulta = sConsulta & vbLf & "   ORIGLASTUPD varchar(50) NULL,"
'    sConsulta = sConsulta & vbLf & "   FECACTFC datetime NULL,"
'    sConsulta = sConsulta & vbLf & "   FECDESACTFC datetime NULL,"
'    sConsulta = sConsulta & vbLf & "   TFNO_MOVIL varchar(20) NULL,"
'    sConsulta = sConsulta & vbLf & "   THOUSANFMT char(1) NULL,"
'    sConsulta = sConsulta & vbLf & "   DECIMALFMT char(1) NULL,"
'    sConsulta = sConsulta & vbLf & "   PRECISIONFMT tinyint NOT NULL,"
'    sConsulta = sConsulta & vbLf & "   DATEFMT varchar(50) NULL,"
'    sConsulta = sConsulta & vbLf & "   MOSTRARFMT tinyint NOT NULL"
'    sConsulta = sConsulta & vbLf & "   )  ON [PRIMARY]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.Tmp_USU ADD CONSTRAINT"
'    sConsulta = sConsulta & vbLf & "   DF_USU_PRECISIONFMT DEFAULT (2) FOR PRECISIONFMT"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.Tmp_USU ADD CONSTRAINT"
'    sConsulta = sConsulta & vbLf & "   DF_USU_MOSTRARFMT DEFAULT (1) FOR MOSTRARFMT"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "IF EXISTS(SELECT * FROM dbo.USU)"
'    sConsulta = sConsulta & vbLf & "    EXEC('INSERT INTO dbo.Tmp_USU (CIA, ID, COD, PWD, FECPWD, APE, NOM, DEP, CAR, TFNO, TFNO2, FAX, EMAIL, IDI, FCEST, FECINS, FECLASTUPD, ORIGINS, ORIGLASTUPD, FECACTFC, FECDESACTFC, TFNO_MOVIL, THOUSANFMT, DECIMALFMT, PRECISIONFMT, DATEFMT, MOSTRARFMT)"
'    sConsulta = sConsulta & vbLf & "       SELECT CIA, ID, COD, PWD, FECPWD, APE, NOM, DEP, CAR, TFNO, TFNO2, FAX, EMAIL, IDI, FCEST, FECINS, FECLASTUPD, ORIGINS, ORIGLASTUPD, FECACTFC, FECDESACTFC, TFNO_MOVIL, THOUSANFMT, DECIMALFMT, CONVERT(tinyint, PRECISIONFMT), DATEFMT, MOSTRARFMT FROM dbo.USU TABLOCKX')"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.CIAS"
'    sConsulta = sConsulta & vbLf & "   DROP CONSTRAINT FK_CIAS_USU"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU"
'    sConsulta = sConsulta & vbLf & "   DROP CONSTRAINT USU_FK_CIAS"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU_PORT"
'    sConsulta = sConsulta & vbLf & "   DROP CONSTRAINT USU_PORT_FK_USU"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "DROP TABLE dbo.USU"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "EXECUTE sp_rename N'dbo.Tmp_USU', N'USU', 'OBJECT'"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU ADD CONSTRAINT"
'    sConsulta = sConsulta & vbLf & "   PK_USU PRIMARY KEY NONCLUSTERED "
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   CIA,"
'    sConsulta = sConsulta & vbLf & "   ID"
'    sConsulta = sConsulta & vbLf & "   ) ON [PRIMARY]"
'    sConsulta = sConsulta & vbLf & ""
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "CREATE UNIQUE NONCLUSTERED INDEX IX_USU_01 ON dbo.USU"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   CIA,"
'    sConsulta = sConsulta & vbLf & "   COD"
'    sConsulta = sConsulta & vbLf & "   ) ON [PRIMARY]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "CREATE NONCLUSTERED INDEX IX_USU_02 ON dbo.USU"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   CIA,"
'    sConsulta = sConsulta & vbLf & "   APE"
'    sConsulta = sConsulta & vbLf & "   ) ON [PRIMARY]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "CREATE NONCLUSTERED INDEX IX_USU_03 ON dbo.USU"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   IDI"
'    sConsulta = sConsulta & vbLf & "   ) ON [PRIMARY]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU WITH NOCHECK ADD CONSTRAINT"
'    sConsulta = sConsulta & vbLf & "   USU_FK_IDI FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   IDI"
'    sConsulta = sConsulta & vbLf & "   ) REFERENCES dbo.IDI"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   ID"
'    sConsulta = sConsulta & vbLf & "   )"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "CREATE TRIGGER [USU_TG_UPD] ON dbo.USU "
'    sConsulta = sConsulta & vbLf & "FOR UPDATE"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@CIA INT"
'    sConsulta = sConsulta & vbLf & "DECLARE curTG_USU_Upd CURSOR FOR SELECT ID,FCEST,CIA FROM INSERTED"
'    sConsulta = sConsulta & vbLf & "OPEN curTG_USU_Upd"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "   BEGIN"
'    sConsulta = sConsulta & vbLf & "       "
'    sConsulta = sConsulta & vbLf & "           UPDATE USU SET FECLASTUPD=GETDATE() WHERE ID=@ID AND CIA=@CIA"
'    sConsulta = sConsulta & vbLf & "           SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID AND CIA=@CIA)"
'    sConsulta = sConsulta & vbLf & "           IF @ESTOLD<> 3 AND @EST=3"
'    sConsulta = sConsulta & vbLf & "               BEGIN   "
'    sConsulta = sConsulta & vbLf & "               UPDATE USU SET FECACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA"
'    sConsulta = sConsulta & vbLf & "               END"
'    sConsulta = sConsulta & vbLf & "           ELSE"
'    sConsulta = sConsulta & vbLf & "               IF @ESTOLD=3 AND @EST<>3 "
'    sConsulta = sConsulta & vbLf & "               BEGIN   "
'    sConsulta = sConsulta & vbLf & "               UPDATE USU SET FECDESACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA"
'    sConsulta = sConsulta & vbLf & "               END"
'    sConsulta = sConsulta & vbLf & "           "
'    sConsulta = sConsulta & vbLf & "   FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA"
'    sConsulta = sConsulta & vbLf & "   END"
'    sConsulta = sConsulta & vbLf & "CLOSE curTG_USU_Upd"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_USU_Upd"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "CREATE TRIGGER [USU_TG_INS] ON dbo.USU "
'    sConsulta = sConsulta & vbLf & "FOR INSERT"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @ID INT,@CIA INT"
'    sConsulta = sConsulta & vbLf & "DECLARE curTG_USU_Ins CURSOR FOR SELECT ID,CIA FROM INSERTED"
'    sConsulta = sConsulta & vbLf & "OPEN curTG_USU_Ins"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_USU_Ins INTO @ID,@CIA"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "   BEGIN"
'    sConsulta = sConsulta & vbLf & "       UPDATE USU SET FECINS=GETDATE() WHERE ID=@ID AND CIA=@CIA"
'    sConsulta = sConsulta & vbLf & "       FETCH NEXT FROM curTG_USU_Ins INTO @ID,@CIA"
'    sConsulta = sConsulta & vbLf & "   END"
'    sConsulta = sConsulta & vbLf & "CLOSE curTG_USU_Ins"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_USU_Ins"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU_PORT WITH NOCHECK ADD CONSTRAINT"
'    sConsulta = sConsulta & vbLf & "   USU_PORT_FK_USU FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   CIA,"
'    sConsulta = sConsulta & vbLf & "   USU"
'    sConsulta = sConsulta & vbLf & "   ) REFERENCES dbo.USU"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   CIA,"
'    sConsulta = sConsulta & vbLf & "   ID"
'    sConsulta = sConsulta & vbLf & "   )"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.CIAS WITH NOCHECK ADD CONSTRAINT"
'    sConsulta = sConsulta & vbLf & "   FK_CIAS_USU FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   ID,"
'    sConsulta = sConsulta & vbLf & "   USUPPAL"
'    sConsulta = sConsulta & vbLf & "   ) REFERENCES dbo.USU"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   CIA,"
'    sConsulta = sConsulta & vbLf & "   ID"
'    sConsulta = sConsulta & vbLf & "   )"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & vbLf & "ALTER TABLE dbo.USU WITH NOCHECK ADD CONSTRAINT"
'    sConsulta = sConsulta & vbLf & "   USU_FK_CIAS FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   CIA"
'    sConsulta = sConsulta & vbLf & "   ) REFERENCES dbo.CIAS"
'    sConsulta = sConsulta & vbLf & "   ("
'    sConsulta = sConsulta & vbLf & "   ID"
'    sConsulta = sConsulta & vbLf & "   )"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    'Nuevo campo TIPOEMAIL en tabla USU
'    frmProgreso.lblDetalle = "Creamos un nuevo campo TIPOEMAIL en la tabla USU"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    sConsulta = "ALTER TABLE [dbo].[USU] ADD TIPOEMAIL [tinyint] NULL "
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo campo MON en la tabla USU
'    frmProgreso.lblDetalle = "Creamos un nuevo campo MON en la tabla USU"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    sConsulta = "ALTER TABLE [dbo].[USU] ADD MON [int] NULL "
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo campo SPTS_ID en la tabla USU
'    frmProgreso.lblDetalle = "Creamos un nuevo campo SPTS_ID en la tabla USU"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    sConsulta = "ALTER TABLE [dbo].[USU] ADD SPTS_ID [int] NULL "
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevas claves for�neas en la tabla _USU
'    frmProgreso.lblDetalle = "Nuevas FK en la tabla USU"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    sConsulta = "ALTER TABLE [dbo].[USU] ADD "
'    sConsulta = sConsulta & vbLf & "    CONSTRAINT [FK_USU_MON] FOREIGN KEY "
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "        [MON]"
'    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[MON] ("
'    sConsulta = sConsulta & vbLf & "    [ID]"
'    sConsulta = sConsulta & vbLf & ")"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    'Nuevo campo COLAB en la tabla PARGEN_INTERNO
'    frmProgreso.lblDetalle = "Creamos un nuevo campo COLAB en la tabla PARGEN_INTERNO"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    sConsulta = "ALTER TABLE [dbo].[PARGEN_INTERNO] ADD COLAB [bit] NOT NULL DEFAULT(0)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevos campos en la tabla REL_CIAS
'    frmProgreso.lblDetalle = "Creamos nuevos campos en la tabla REL_CIAS"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    sConsulta = "ALTER TABLE [dbo].[REL_CIAS] ADD ORDEN_ACT [int] NOT NULL DEFAULT (0)"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = "ALTER TABLE [dbo].[REL_CIAS] ADD ORDEN_NUE [int] NOT NULL DEFAULT (0)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo store prodecure FSEP_ACT_REL_CIAS
'    frmProgreso.lblDetalle = "Nuevo store prodecure FSEP_ACT_REL_CIAS"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    sConsulta = Crear_FSEP_ACT_REL_CIAS()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'nueva tabla SPTS_COLAB
'    frmProgreso.lblDetalle = "Creamos una nueva tabla SPTS_COLAB"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    sConsulta = CrearTablaSPTS_COLAB
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.00.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_8_0_2A2_0_9_00_0 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_8_0_2A2_0_9_00_0 = False
'End Function
'
'
'Private Function Crear_FSEP_ACT_REL_CIAS() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE FSEP_ACT_REL_CIAS(@COMP VARCHAR(50),@PROVE VARCHAR(50),@ACTIVAS INTEGER,@NUEVAS INTEGER) AS"
'    sConsulta = sConsulta & vbCrLf & "DECLARE @ID_COMP AS INT"
'    sConsulta = sConsulta & vbCrLf & "BEGIN"
'    sConsulta = sConsulta & vbCrLf & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)"
'    sConsulta = sConsulta & vbCrLf & "UPDATE REL_CIAS SET ORDEN_ACT=@ACTIVAS,ORDEN_NUE=@NUEVAS WHERE CIA_COMP=@ID_COMP AND COD_PROVE_CIA=@PROVE"
'    sConsulta = sConsulta & vbCrLf & "END"
'
'    Crear_FSEP_ACT_REL_CIAS = sConsulta
'End Function
'
'
'Private Function CrearTablaSPTS_COLAB() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[SPTS_COLAB] ("
'    sConsulta = sConsulta & vbLf & "[SPTS_ID] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[LIST_ID] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[MAIL] [varchar] (50) NULL"
'    sConsulta = sConsulta & vbLf & ") ON [PRIMARY]"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[SPTS_COLAB] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [DF_SPTS_COLAB_SPTS_ID] DEFAULT (0) FOR [SPTS_ID],"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [DF_SPTS_COLAB_LIST_ID] DEFAULT (0) FOR [LIST_ID]"
'
'    CrearTablaSPTS_COLAB = sConsulta
'End Function
'
'
'Private Sub CrearTablaPARGEN_COLAB()
'Dim sConsulta As String
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PARGEN_COLAB]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop table [dbo].[PARGEN_COLAB]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TABLE [dbo].[PARGEN_COLAB] (" & vbCrLf
'sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [SRVNAME] [varchar] (100) NULL ," & vbCrLf
'sConsulta = sConsulta & "   [DBNAME] [varchar] (100) NULL ," & vbCrLf
'sConsulta = sConsulta & "   [IP_SRV] [varchar] (50) NULL ," & vbCrLf
'sConsulta = sConsulta & "   [SRVGROUP] [varchar] (50) NULL ," & vbCrLf
'sConsulta = sConsulta & "   [GRUPO] [varchar] (100) NULL ," & vbCrLf
'sConsulta = sConsulta & "   [WEBSPTS] [varchar] (100) NULL " & vbCrLf
'sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_COLAB] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [PK_PARGEN_COLAB] PRIMARY KEY  CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [ID]" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY] "
'ExecuteSQL gRDOCon, sConsulta
'
'
'End Sub
'
'
'Private Function CodigoDeActualizacion2_9_00_0A2_9_00_1() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'
'    'nueva tabla PARGEN_COLAB
'
'
'    frmProgreso.lblDetalle = "Creamos una nueva tabla PARGEN_COLAB"
'    frmProgreso.lblDetalle.Refresh
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    CrearTablaPARGEN_COLAB
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.00.1'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_00_0A2_9_00_1 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_00_0A2_9_00_1 = False
'End Function
'
'
'
'Private Function CodigoDeActualizacion2_9_00_1A2_9_00_2() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificamos el stored procedure SP_ADD_VOLADJ"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'    V_2_9_002_Stored_SP_ADD_VOLADJ
'
'
'
'    frmProgreso.lblDetalle = "Modificamos la tabla WEBTEXT"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'    V_2_9_002_Tabla_WEBTEXT
'
'
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.00.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_00_1A2_9_00_2 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_00_1A2_9_00_2 = False
'End Function
'
'
'Private Sub V_2_9_002_Stored_SP_ADD_VOLADJ()
'Dim sConsulta As String
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ADD_VOLADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_ADD_VOLADJ]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ADD_VOLADJ(@COMP INT,@PROVE VARCHAR(50),@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT,@VOL FLOAT,@MONGS VARCHAR(50),@MONPOR VARCHAR(50),@FECADJ DATETIME,@DENPROCE VARCHAR(100) ) AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Inserta el registro correspondiente en VOLADJ" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @MONPORTAL AS VARCHAR(3)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @VOLPORTAL AS FLOAT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @EQUIV AS FLOAT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @EQUIVGS AS FLOAT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Obtiene el ID de la compaNYia proveedora" & vbCrLf
'sConsulta = sConsulta & "SET @ID = (SELECT ID FROM CIAS WHERE COD=@PROVE)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--Comprueba si existe la moneda en el portal" & vbCrLf
'sConsulta = sConsulta & "SET @MONPORTAL =(SELECT COD FROM MON WHERE COD=@MONPOR)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--Si existe el registro primero lo borra" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND CIA_PROVE=@ID AND CIA_COMP=@COMP" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "IF NOT @MONPORTAL IS NULL " & vbCrLf
'sConsulta = sConsulta & "    BEGIN" & vbCrLf
'sConsulta = sConsulta & " --Si existe la moneda en el portal se calcula el total y lo inserta," & vbCrLf
'sConsulta = sConsulta & " --Calcula la equivalencia de la moneda en el portal" & vbCrLf
'sConsulta = sConsulta & " SET @EQUIV = (SELECT EQUIV FROM MON WHERE COD=@MONPOR)" & vbCrLf
'sConsulta = sConsulta & " IF NOT @EQUIV IS NULL" & vbCrLf
'sConsulta = sConsulta & "       BEGIN" & vbCrLf
'sConsulta = sConsulta & "     --Calcula el total del volumen en la moneda del portal" & vbCrLf
'sConsulta = sConsulta & "     SET @VOLPORTAL=(@VOL) / @EQUIV  " & vbCrLf
'sConsulta = sConsulta & "              INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,@MONPORTAL,@VOLPORTAL,@FECADJ,@DENPROCE)" & vbCrLf
'sConsulta = sConsulta & "      END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            ELSE" & vbCrLf
'sConsulta = sConsulta & "     INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,@MONPORTAL,NULL,@FECADJ,@DENPROCE)    " & vbCrLf
'sConsulta = sConsulta & "    END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ELSE" & vbCrLf
'sConsulta = sConsulta & " --si no existe la moneda en el portal lo inserta a null" & vbCrLf
'sConsulta = sConsulta & " INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,NULL,NULL,@FECADJ,@DENPROCE)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'
'End Sub
'
'
'
'Private Sub V_2_9_002_Tabla_WEBTEXT()
'Dim sConsulta As String
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TABLE dbo.Tmp_WEBTEXT" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   MODULO int NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   ID int NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   SPA text NULL," & vbCrLf
'sConsulta = sConsulta & "   ENG text NULL" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY]" & vbCrLf
'sConsulta = sConsulta & "    TEXTIMAGE_ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.Tmp_WEBTEXT ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   DF_WEBTEXT_MODULO DEFAULT 0 FOR MODULO" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "IF EXISTS(SELECT * FROM dbo.WEBTEXT)" & vbCrLf
'sConsulta = sConsulta & "    EXEC('INSERT INTO dbo.Tmp_WEBTEXT (ID, SPA, ENG)" & vbCrLf
'sConsulta = sConsulta & "       SELECT ID, SPA, ENG FROM dbo.WEBTEXT TABLOCKX')" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "DROP TABLE dbo.WEBTEXT" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "EXECUTE sp_rename N'dbo.Tmp_WEBTEXT', N'WEBTEXT', 'OBJECT'" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.WEBTEXT ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   PK_WEBTEXT PRIMARY KEY CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   MODULO," & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'
'End Sub
'
'Private Function CodigoDeActualizacion2_9_00_3A2_9_00_4() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Creamos tabla OBJSADM e insertamos valores"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = ""
'    sConsulta = sConsulta & "CREATE TABLE [dbo].[OBJSADM] (" & vbCrLf
'    sConsulta = sConsulta & "    [USU] [varchar] (20) NOT NULL ," & vbCrLf
'    sConsulta = sConsulta & "    [PWD] [varchar] (100) NOT NULL ," & vbCrLf
'    sConsulta = sConsulta & "    [FECPWD] [datetime] NOT NULL " & vbCrLf
'    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
'
'    ExecuteSQL gRDOCon, sConsulta
'
'    frmProgreso.lblDetalle = "insertamos valores en tabla OBJSADM"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = ""
'    sfecha = "20/12/2001 15:20:53"
'    sConsulta = "INSERT INTO [dbo].[OBJSADM] VALUES ('4�vTa^?','ЎfrqT\0�e�jp[�&9','" & Format(sfecha, "mm-dd-yyyy hh:mm:ss") & "')"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.00.4'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_00_3A2_9_00_4 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_00_3A2_9_00_4 = False
'End Function
'Private Function CodigoDeActualizacion2_9_00_2A2_9_00_3() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificamos el campo COMENT de CIAS"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'    V_2_9_Cambiar_Coment_Cias
'
'
'
'    frmProgreso.lblDetalle = "Regla que evita que se inserten usuarios con mismo c�digo."
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = ""
'    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_INSUPD]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "CREATE TRIGGER [USU_TG_INSUPD] ON dbo.USU " & vbCrLf
'    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
'    sConsulta = sConsulta & "AS" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "IF (SELECT COUNT(*) " & vbCrLf
'    sConsulta = sConsulta & "      FROM INSERTED I " & vbCrLf
'    sConsulta = sConsulta & "              INNER JOIN CIAS  C" & vbCrLf
'    sConsulta = sConsulta & "                      ON I.CIA = C.ID " & vbCrLf
'    sConsulta = sConsulta & "     WHERE EXISTS (SELECT * " & vbCrLf
'    sConsulta = sConsulta & "                     FROM USU " & vbCrLf
'    sConsulta = sConsulta & "                           INNER JOIN CIAS " & vbCrLf
'    sConsulta = sConsulta & "                                   ON USU.CIA = CIAS.ID " & vbCrLf
'    sConsulta = sConsulta & "                    WHERE USU.COD =I.COD " & vbCrLf
'    sConsulta = sConsulta & "                      AND I.CIA!=USU.CIA" & vbCrLf
'    sConsulta = sConsulta & "                      AND CIAS.FCEST=3)" & vbCrLf
'    sConsulta = sConsulta & "       AND C.FCEST=3)>0" & vbCrLf
'    sConsulta = sConsulta & "  BEGIN" & vbCrLf
'    sConsulta = sConsulta & "    RAISERROR ('El c�digo del usuario comprador ya existe en el portal',16,1)" & vbCrLf
'    sConsulta = sConsulta & "    ROLLBACK TRANSACTION" & vbCrLf
'    sConsulta = sConsulta & "  end" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.00.3'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_00_2A2_9_00_3 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_00_2A2_9_00_3 = False
'End Function
'
'
'
'Private Function V_2_9_Cambiar_Coment_Cias()
'
'Dim sConsulta As String
'
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_FK_PROVI" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_FK_PORT" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_FK_PAI" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_FK_MON" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_FK_IDI" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT DF__CIAS__PREMIUM__131DCD43" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT DF__CIAS__PENDCONF__2DD1C37F" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT DF_CIAS_PENDCONFREG" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TABLE dbo.Tmp_CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID int NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   COD varchar(20) NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   DEN varchar(100) NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   DIR varchar(255) NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   CP varchar(20) NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   POB varchar(100) NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   PAI int NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   PROVI int NULL," & vbCrLf
'sConsulta = sConsulta & "   MON int NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   IDI int NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   PORT int NULL," & vbCrLf
'sConsulta = sConsulta & "   FCEST int NULL," & vbCrLf
'sConsulta = sConsulta & "   FSGS_SRV varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   FSGS_BD varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   NIF varchar(20) NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   VOLFACT float(53) NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   CLIREF1 varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   CLIREF2 varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   CLIREF3 varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   CLIREF4 varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   HOM1 varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   HOM2 varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   HOM3 varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   URLCIA varchar(255) NULL," & vbCrLf
'sConsulta = sConsulta & "   COMENT text NULL," & vbCrLf
'sConsulta = sConsulta & "   FECINS datetime NULL," & vbCrLf
'sConsulta = sConsulta & "   FECLASTUPD datetime NULL," & vbCrLf
'sConsulta = sConsulta & "   ORIGINS varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   ORIGLASTUPD varchar(50) NULL," & vbCrLf
'sConsulta = sConsulta & "   FECACTFC datetime NULL," & vbCrLf
'sConsulta = sConsulta & "   FECDESACTFC datetime NULL," & vbCrLf
'sConsulta = sConsulta & "   PREMIUM int NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   USUPPAL int NULL," & vbCrLf
'sConsulta = sConsulta & "   PENDCONF bit NOT NULL," & vbCrLf
'sConsulta = sConsulta & "   PENDCONFREG tinyint NOT NULL" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY]" & vbCrLf
'sConsulta = sConsulta & "    TEXTIMAGE_ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.Tmp_CIAS ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   DF__CIAS__PREMIUM__131DCD43 DEFAULT (0) FOR PREMIUM" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.Tmp_CIAS ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   DF__CIAS__PENDCONF__2DD1C37F DEFAULT (0) FOR PENDCONF" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.Tmp_CIAS ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   DF_CIAS_PENDCONFREG DEFAULT (0) FOR PENDCONFREG" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "IF EXISTS(SELECT * FROM dbo.CIAS)" & vbCrLf
'sConsulta = sConsulta & "    EXEC('INSERT INTO dbo.Tmp_CIAS (ID, COD, DEN, DIR, CP, POB, PAI, PROVI, MON, IDI, PORT, FCEST, FSGS_SRV, FSGS_BD, NIF, VOLFACT, CLIREF1, CLIREF2, CLIREF3, CLIREF4, HOM1, HOM2, HOM3, URLCIA, COMENT, FECINS, FECLASTUPD, ORIGINS, ORIGLASTUPD, FECACTFC, FECDESACTFC, PREMIUM, USUPPAL, PENDCONF, PENDCONFREG)" & vbCrLf
'sConsulta = sConsulta & "       SELECT ID, COD, DEN, DIR, CP, POB, PAI, PROVI, MON, IDI, PORT, FCEST, FSGS_SRV, FSGS_BD, NIF, VOLFACT, CLIREF1, CLIREF2, CLIREF3, CLIREF4, HOM1, HOM2, HOM3, URLCIA, CONVERT(text, COMENT), FECINS, FECLASTUPD, ORIGINS, ORIGLASTUPD, FECACTFC, FECDESACTFC, PREMIUM, USUPPAL, PENDCONF, PENDCONFREG FROM dbo.CIAS TABLOCKX')" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT FK_CIAS_USU" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.USU" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT USU_FK_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT1" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_ACT1_FK_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT2" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_ACT2_FK_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT3" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_ACT3_FK_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT4" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_ACT4_FK_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT5" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_ACT5_FK_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_COM" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_COM_FK_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_PORT" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT CIAS_PORT_FK_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.REL_CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT REL_CIAS_FK_CIAS_01" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.REL_CIAS" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT REL_CIAS_FK_CIAS_02" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.VOLADJ" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT FK_VOLADJ_CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.VOLADJ" & vbCrLf
'sConsulta = sConsulta & "   DROP CONSTRAINT FK_VOLADJ_CIAS1" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "DROP TABLE dbo.CIAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "EXECUTE sp_rename N'dbo.Tmp_CIAS', N'CIAS', 'OBJECT'" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   PK_CIAS PRIMARY KEY NONCLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE UNIQUE NONCLUSTERED INDEX IX_CIAS_01 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   COD" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_CIAS_02 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   DEN" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_CIAS_03 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PAI," & vbCrLf
'sConsulta = sConsulta & "   COD" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_CIAS_04 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PAI," & vbCrLf
'sConsulta = sConsulta & "   DEN" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_CIAS_05 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PORT," & vbCrLf
'sConsulta = sConsulta & "   COD" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_CIAS_06 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PORT," & vbCrLf
'sConsulta = sConsulta & "   DEN" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_CIAS_07 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PAI," & vbCrLf
'sConsulta = sConsulta & "   PROVI" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_CIAS_08 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   MON" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_CIAS_09 ON dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   IDI" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_FK_IDI FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   IDI" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.IDI" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_FK_MON FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   MON" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.MON" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_FK_PAI FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PAI" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.PAI" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_FK_PORT FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PORT" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.PORT" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_FK_PROVI FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PAI," & vbCrLf
'sConsulta = sConsulta & "   PROVI" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.PROVI" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   PAI," & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TRIGGER [CIAS_TG_INS] ON dbo.CIAS " & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_CIAS_Ins CURSOR FOR SELECT ID FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_CIAS_Ins" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Ins INTO @ID" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "       UPDATE CIAS SET FECINS=GETDATE() WHERE ID=@ID" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_Ins INTO @ID" & vbCrLf
'sConsulta = sConsulta & "   END" & vbCrLf
'sConsulta = sConsulta & "CLOSE curTG_CIAS_Ins" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Ins" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TRIGGER [CIAS_TG_UPD] ON dbo.CIAS " & vbCrLf
'sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ID INT,@EST INT,@ESTOLD INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_CIAS_Upd CURSOR FOR SELECT ID,FCEST FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_CIAS_Upd" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "       UPDATE CIAS SET FECLASTUPD=GETDATE() WHERE ID=@ID" & vbCrLf
'sConsulta = sConsulta & "       SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID)" & vbCrLf
'sConsulta = sConsulta & "       IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
'sConsulta = sConsulta & "       BEGIN   " & vbCrLf
'sConsulta = sConsulta & "           UPDATE CIAS SET FECACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "       ELSE" & vbCrLf
'sConsulta = sConsulta & "           IF @ESTOLD=3 AND @EST<>3 " & vbCrLf
'sConsulta = sConsulta & "           BEGIN   " & vbCrLf
'sConsulta = sConsulta & "               UPDATE CIAS SET FECDESACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
'sConsulta = sConsulta & "           END" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST" & vbCrLf
'sConsulta = sConsulta & "   END" & vbCrLf
'sConsulta = sConsulta & "CLOSE curTG_CIAS_Upd" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Upd" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'
'sConsulta = sConsulta & "ALTER TABLE dbo.VOLADJ WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   FK_VOLADJ_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.VOLADJ WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   FK_VOLADJ_CIAS1 FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA_COMP" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.REL_CIAS WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   REL_CIAS_FK_CIAS_01 FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.REL_CIAS WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   REL_CIAS_FK_CIAS_02 FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA_COMP" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_PORT WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_PORT_FK_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_COM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_COM_FK_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT5 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_ACT5_FK_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT4 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_ACT4_FK_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_ACT3_FK_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT2 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_ACT2_FK_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS_ACT1 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   CIAS_ACT1_FK_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.CIAS WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   FK_CIAS_USU FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID," & vbCrLf
'sConsulta = sConsulta & "   USUPPAL" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.USU" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA," & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.USU WITH NOCHECK ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   USU_FK_CIAS FOREIGN KEY" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   CIA" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES dbo.CIAS" & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   ID" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'End Function
'
''******************************************************************
''******************************************************************
''***** ACTUALIZACI�N A LA VERSION 2.9.5 A PARTIR DE AQU�  *********
''******************************************************************
''******************************************************************
'
'
'Private Function CodigoDeActualizacion2_9_00_4A2_9_50_0() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Nuevas tablas para la versi�n 2.9.5"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'    V_2_9_5_Tablas
'
'    frmProgreso.lblDetalle = "Nuevos campos para la versi�n 2.9.5"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    V_2_9_5_Nuevos_Campos
'
'    frmProgreso.lblDetalle = "Nuevos triggers para la versi�n 2.9.5"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    V_2_9_5_Triggers
'
'    frmProgreso.lblDetalle = "Nuevos storeds para la versi�n 2.9.5"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'    V_2_9_5_Storeds
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.50.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_00_4A2_9_50_0 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_00_4A2_9_50_0 = False
'End Function
'
'Private Sub V_2_9_5_Tablas()
'Dim sConsulta As String
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TABLE [dbo].[SPTS_DOC] (" & vbCrLf
'sConsulta = sConsulta & "   [SPTS_ID] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [DOCLIB_ID] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [DOC_ID] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [MAIL] [bit] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [WRITEP] [bit] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [READP] [bit] NOT NULL " & vbCrLf
'sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TABLE [dbo].[PARGEN_GEST] (" & vbCrLf
'sConsulta = sConsulta & "   [ID] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOM_PORTAL] [varchar] (255)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [URL] [varchar] (255)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NIVELMINACT] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOTIF_AUT] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOTIF_DESAUT] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOTIF_CAMB_ACT] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOTIF_REGCIAS] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [CONFIRM_AUT] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [CONFIRM_DESAUT] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [CONFIRM_CAMB_ACT] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [CONFIRM_REGCIAS] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_REINTENTOS] [smallint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_CICLOS] [smallint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_PERIODICIDAD] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_HORA_INICIO] [datetime] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_HORA_FIN] [datetime] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_UNA_HORA] [datetime] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_CADAMIN] [smallint] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_PASADAS] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [PROV_FECULT] [datetime] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [TIPOEMAIL] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [ADM] [varchar] (255)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [EMAIL] [varchar] (255)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOTIF_SOLICREG] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOTIF_SOLICACTIV] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOTIF_SOLICCOMP] [tinyint] NOT NULL " & vbCrLf
'sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TABLE [dbo].[MVTOS_INT] (" & vbCrLf
'sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [TIPO] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [ACT1] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [ACT2] [int] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [ACT3] [int] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [ACT4] [int] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [COD] [varchar] (2)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [COD_NEW] [varchar] (2)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [DEN_1] [varchar] (1000)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [DEN_2] [varchar] (1000)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
'sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TABLE [dbo].[MVTOS_PROV] (" & vbCrLf
'sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [USU] [int] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [TIPO] [varchar] (100)  NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [ESPEC] [int] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [TABLA] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [FECACT] [datetime] NOT NULL " & vbCrLf
'sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TABLE [dbo].[MVTOS_PROV_EST] (" & vbCrLf
'sConsulta = sConsulta & "   [MVTO] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [EST] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [INTENTOS] [tinyint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [FECACT] [datetime] NOT NULL " & vbCrLf
'sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[SPTS_DOC] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [PK_SPTS_DOC] PRIMARY KEY  CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [SPTS_ID]," & vbCrLf
'sConsulta = sConsulta & "       [DOCLIB_ID]," & vbCrLf
'sConsulta = sConsulta & "       [DOC_ID]" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_GEST] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [PK_PARGEN_GEST] PRIMARY KEY  CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [ID]" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[MVTOS_INT] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [PK_MVTOS_INT] PRIMARY KEY  CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [ID]" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[MVTOS_PROV] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [PK_MVTOS_PROV] PRIMARY KEY  CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [ID]" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[MVTOS_PROV_EST] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [PK_MVTOS_PROV_EST] PRIMARY KEY  CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [MVTO]," & vbCrLf
'sConsulta = sConsulta & "       [CIA]" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[SPTS_DOC] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_SPTS_DOC_MAIL] DEFAULT (0) FOR [MAIL]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_SPTS_DOC_WRITEP] DEFAULT (0) FOR [WRITEP]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_SPTS_DOC_READP] DEFAULT (0) FOR [READP]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_GEST] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_AUT] DEFAULT (0) FOR [NOTIF_AUT]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_DESAUT] DEFAULT (0) FOR [NOTIF_DESAUT]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_CAMB_ACT] DEFAULT (0) FOR [NOTIF_CAMB_ACT]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_REGCIAS] DEFAULT (0) FOR [NOTIF_REGCIAS]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONFIRM_AUT] DEFAULT (0) FOR [CONFIRM_AUT]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONFIRM_DESAUT] DEFAULT (0) FOR [CONFIRM_DESAUT]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONFIRM_CAMB_ACT] DEFAULT (0) FOR [CONFIRM_CAMB_ACT]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONFIRM_REGCIAS] DEFAULT (0) FOR [CONFIRM_REGCIAS]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_REINTENTOS] DEFAULT (0) FOR [PROV_REINTENTOS]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_CICLOS] DEFAULT (0) FOR [PROV_CICLOS]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_PERIODICIDAD] DEFAULT (0) FOR [PROV_PERIODICIDAD]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_PASADAS] DEFAULT (0) FOR [PROV_PASADAS]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_FECULT] DEFAULT (1 / 1 / 2000) FOR [PROV_FECULT]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_TIPOEMAIL] DEFAULT (1) FOR [TIPOEMAIL]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_SOLICREG] DEFAULT (0) FOR [NOTIF_SOLICREG]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_SOLICACTIV] DEFAULT (0) FOR [NOTIF_SOLICACTIV]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_SOLICCOMP] DEFAULT (0) FOR [NOTIF_SOLICCOMP]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[MVTOS_PROV] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_MVTOS_PROV_TABLA] DEFAULT (0) FOR [TABLA]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ESP_TG_INSUPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ESP_TG_DEL]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ESP_TG_INS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ESP_TG_UPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop table [dbo].[CIAS_ESP]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TABLE [dbo].[CIAS_ESP] (" & vbCrLf
'sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [ID] [smallint] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [NOM] [varchar] (300)  NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [COM] [varchar] (500)  NULL ," & vbCrLf
'sConsulta = sConsulta & "   [DATA] [image] NULL ," & vbCrLf
'sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
'sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ESP] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [PK_CIAS_ESP] PRIMARY KEY  CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [CIA]," & vbCrLf
'sConsulta = sConsulta & "       [ID]" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ESP] ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [FK_CIAS_ESP_CIAS] FOREIGN KEY " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [CIA]" & vbCrLf
'sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
'sConsulta = sConsulta & "       [ID]" & vbCrLf
'sConsulta = sConsulta & "   )" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER CIAS_ESP_TG_INSUPD ON dbo.CIAS_ESP" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ID INT, @CIA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_CIAS_ESP_Ins CURSOR FOR SELECT ID,CIA FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_CIAS_ESP_Ins" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_ESP_Ins INTO @ID,@CIA" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "UPDATE CIAS_ESP SET FECACT=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_ESP_Ins INTO @ID,@CIA" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "Close curTG_CIAS_ESP_Ins" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_ESP_Ins" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER CIAS_ESP_TG_DEL ON dbo.CIAS_ESP " & vbCrLf
'sConsulta = sConsulta & "FOR DELETE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Inserta un movimiento tipo Delete 'D' en la tabla MVTOS_PROV" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare c1 cursor local for select cia, id from deleted" & vbCrLf
'sConsulta = sConsulta & "open c1" & vbCrLf
'sConsulta = sConsulta & "fetch next from c1 into @cia, @id" & vbCrLf
'sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
'sConsulta = sConsulta & "  begin" & vbCrLf
'sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
'sConsulta = sConsulta & "                      FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                           INNER JOIN CIAS " & vbCrLf
'sConsulta = sConsulta & "                                   ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
'sConsulta = sConsulta & "                                  AND CIAS.FCEST=3 " & vbCrLf
'sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
'sConsulta = sConsulta & "    IF @ENLAZADA > 0 " & vbCrLf
'sConsulta = sConsulta & "      begin" & vbCrLf
'sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
'sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
'sConsulta = sConsulta & "        ELSE" & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE= @INDICE + 1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) " & vbCrLf
'sConsulta = sConsulta & "                        VALUES  (@INDICE,@CIA,NULL,'D',@ID,2,GETDATE())" & vbCrLf
'sConsulta = sConsulta & "      end" & vbCrLf
'sConsulta = sConsulta & "    fetch next from c1 into @cia, @id" & vbCrLf
'sConsulta = sConsulta & "  end" & vbCrLf
'sConsulta = sConsulta & "close c1" & vbCrLf
'sConsulta = sConsulta & "deallocate c1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER CIAS_ESP_TG_INS ON dbo.CIAS_ESP " & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Inserta un movimiento tipo Insert 'I' en la tabla MVTOS_PROV" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare c1 cursor local for select cia, id from inserted" & vbCrLf
'sConsulta = sConsulta & "open c1" & vbCrLf
'sConsulta = sConsulta & "fetch next from c1 into @cia, @id" & vbCrLf
'sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
'sConsulta = sConsulta & "  begin" & vbCrLf
'sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
'sConsulta = sConsulta & "                      FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                           INNER JOIN CIAS " & vbCrLf
'sConsulta = sConsulta & "                                   ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
'sConsulta = sConsulta & "                                  AND CIAS.FCEST=3 " & vbCrLf
'sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
'sConsulta = sConsulta & "    IF @ENLAZADA > 0 " & vbCrLf
'sConsulta = sConsulta & "      begin" & vbCrLf
'sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
'sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
'sConsulta = sConsulta & "        ELSE" & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE= @INDICE + 1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) " & vbCrLf
'sConsulta = sConsulta & "                        VALUES  (@INDICE,@CIA,NULL,'I',@ID,2,GETDATE())" & vbCrLf
'sConsulta = sConsulta & "      end" & vbCrLf
'sConsulta = sConsulta & "    fetch next from c1 into @cia, @id" & vbCrLf
'sConsulta = sConsulta & "  end" & vbCrLf
'sConsulta = sConsulta & "close c1" & vbCrLf
'sConsulta = sConsulta & "deallocate c1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER CIAS_ESP_TG_UPD ON dbo.CIAS_ESP " & vbCrLf
'sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Inserta un movimiento tipo Update 'U' en la tabla MVTOS_PROV" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare c1 cursor local for select cia, id from inserted" & vbCrLf
'sConsulta = sConsulta & "open c1" & vbCrLf
'sConsulta = sConsulta & "fetch next from c1 into @cia, @id" & vbCrLf
'sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
'sConsulta = sConsulta & "  begin" & vbCrLf
'sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
'sConsulta = sConsulta & "                      FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                           INNER JOIN CIAS " & vbCrLf
'sConsulta = sConsulta & "                                   ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
'sConsulta = sConsulta & "                                  AND CIAS.FCEST=3 " & vbCrLf
'sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
'sConsulta = sConsulta & "    IF @ENLAZADA > 0 " & vbCrLf
'sConsulta = sConsulta & "      begin" & vbCrLf
'sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
'sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
'sConsulta = sConsulta & "        ELSE" & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE= @INDICE + 1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) " & vbCrLf
'sConsulta = sConsulta & "                        VALUES  (@INDICE,@CIA,NULL,'U',@ID,2,GETDATE())" & vbCrLf
'sConsulta = sConsulta & "      end" & vbCrLf
'sConsulta = sConsulta & "    fetch next from c1 into @cia, @id" & vbCrLf
'sConsulta = sConsulta & "  end" & vbCrLf
'sConsulta = sConsulta & "close c1" & vbCrLf
'sConsulta = sConsulta & "deallocate c1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TABLE [dbo].[SPTS_LIB] (" & vbCrLf
'sConsulta = sConsulta & "   [SPTS_ID] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [DOCLIB_ID] [int] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [MAIL] [bit] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [WRITEP] [bit] NOT NULL ," & vbCrLf
'sConsulta = sConsulta & "   [READP] [bit] NOT NULL " & vbCrLf
'sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[SPTS_LIB] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [PK_SPTS_LIB] PRIMARY KEY  CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "       [SPTS_ID]," & vbCrLf
'sConsulta = sConsulta & "       [DOCLIB_ID]" & vbCrLf
'sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE [dbo].[SPTS_LIB] WITH NOCHECK ADD " & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_SPTS_LIB_MAIL] DEFAULT (0) FOR [MAIL]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_SPTS_LIB_WRITEP] DEFAULT (0) FOR [WRITEP]," & vbCrLf
'sConsulta = sConsulta & "   CONSTRAINT [DF_SPTS_LIB_READP] DEFAULT (0) FOR [READP]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'
'
'End Sub
'
'Private Sub V_2_9_5_Nuevos_Campos()
'Dim sConsulta As String
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.ACT1 ADD" & vbCrLf
'sConsulta = sConsulta & "   FECACT datetime NULL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.ACT2 ADD" & vbCrLf
'sConsulta = sConsulta & "   FECACT datetime NULL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.ACT3 ADD" & vbCrLf
'sConsulta = sConsulta & "   FECACT datetime NULL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.ACT4 ADD" & vbCrLf
'sConsulta = sConsulta & "   FECACT datetime NULL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "ALTER TABLE dbo.ACT5 ADD" & vbCrLf
'sConsulta = sConsulta & "   FECACT datetime NULL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE dbo.OBJSADM ADD CONSTRAINT" & vbCrLf
'sConsulta = sConsulta & "   PK_OBJSADM PRIMARY KEY CLUSTERED " & vbCrLf
'sConsulta = sConsulta & "   (" & vbCrLf
'sConsulta = sConsulta & "   USU" & vbCrLf
'sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'End Sub
'
'Private Sub V_2_9_5_Triggers()
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[CIAS_TG_UPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TRIGGER [CIAS_TG_UPD] ON dbo.CIAS " & vbCrLf
'sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_CIAS_Upd CURSOR LOCAL FOR SELECT ID,FCEST FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_CIAS_Upd" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "  UPDATE CIAS SET FECLASTUPD=GETDATE() WHERE ID=@ID" & vbCrLf
'sConsulta = sConsulta & "  SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID)" & vbCrLf
'sConsulta = sConsulta & "  IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
'sConsulta = sConsulta & "    UPDATE CIAS SET FECACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
'sConsulta = sConsulta & "  ELSE" & vbCrLf
'sConsulta = sConsulta & "    IF @ESTOLD=3 AND @EST<>3 " & vbCrLf
'sConsulta = sConsulta & "      UPDATE CIAS SET FECDESACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
'sConsulta = sConsulta & "  -- Inserta un moviemto tipo Udate 'U' en la tabla MVTOS_PROV" & vbCrLf
'sConsulta = sConsulta & "  IF UPDATE(DEN) OR UPDATE(DIR) OR UPDATE(CP) OR UPDATE(POB) OR UPDATE(PROVI) OR UPDATE(NIF) OR" & vbCrLf
'sConsulta = sConsulta & "    UPDATE(PAI) OR UPDATE(MON) OR UPDATE(URLCIA)" & vbCrLf
'sConsulta = sConsulta & "    BEGIN" & vbCrLf
'sConsulta = sConsulta & "      SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
'sConsulta = sConsulta & "                        FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                                INNER JOIN CIAS " & vbCrLf
'sConsulta = sConsulta & "                                        ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
'sConsulta = sConsulta & "                                       AND CIAS.FCEST=3 " & vbCrLf
'sConsulta = sConsulta & "                       WHERE CIA_PROVE=@ID " & vbCrLf
'sConsulta = sConsulta & "                         AND EST=3)" & vbCrLf
'sConsulta = sConsulta & "      IF @ENLAZADA >0" & vbCrLf
'sConsulta = sConsulta & "        BEGIN" & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV) " & vbCrLf
'sConsulta = sConsulta & "          IF @INDICE IS NULL       " & vbCrLf
'sConsulta = sConsulta & "            SET @INDICE=1" & vbCrLf
'sConsulta = sConsulta & "          ELSE" & vbCrLf
'sConsulta = sConsulta & "            SET @INDICE=@INDICE+1" & vbCrLf
'sConsulta = sConsulta & "          INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@ID,NULL,'U',0,GETDATE())" & vbCrLf
'sConsulta = sConsulta & "        END" & vbCrLf
'sConsulta = sConsulta & "      END" & vbCrLf
'sConsulta = sConsulta & "  FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CLOSE curTG_CIAS_Upd" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Upd" & vbCrLf
'
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_UPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TRIGGER [USU_TG_UPD] ON dbo.USU " & vbCrLf
'sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@CIA INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_USU_Upd CURSOR FOR SELECT ID,FCEST,CIA FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_USU_Upd" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           UPDATE USU SET FECLASTUPD=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
'sConsulta = sConsulta & "           SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID AND CIA=@CIA)" & vbCrLf
'sConsulta = sConsulta & "           IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
'sConsulta = sConsulta & "               BEGIN   " & vbCrLf
'sConsulta = sConsulta & "               UPDATE USU SET FECACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
'sConsulta = sConsulta & "               END" & vbCrLf
'sConsulta = sConsulta & "           ELSE" & vbCrLf
'sConsulta = sConsulta & "               IF @ESTOLD=3 AND @EST<>3 " & vbCrLf
'sConsulta = sConsulta & "               BEGIN   " & vbCrLf
'sConsulta = sConsulta & "               UPDATE USU SET FECDESACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
'sConsulta = sConsulta & "               END" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA" & vbCrLf
'sConsulta = sConsulta & "   END" & vbCrLf
'sConsulta = sConsulta & "CLOSE curTG_USU_Upd" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_USU_Upd" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Inserta un movimiento en la tabla MVTOS_PROV (el tipo de movimiento no nos importa porque siempre hacemos borrar e insertar)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT CIA, ID FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN C1" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @CIA,@ID" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "  BEGIN" & vbCrLf
'sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
'sConsulta = sConsulta & "                      FROM INSERTED " & vbCrLf
'sConsulta = sConsulta & "                            INNER JOIN USU_PORT " & vbCrLf
'sConsulta = sConsulta & "                                    ON INSERTED.ID=USU_PORT.USU" & vbCrLf
'sConsulta = sConsulta & "                              AND INSERTED.CIA=USU_PORT.CIA " & vbCrLf
'sConsulta = sConsulta & "                                   AND (USU_PORT.FPEST=3 OR  USU_PORT.FPEST=2)" & vbCrLf
'sConsulta = sConsulta & "                                   AND USU_PORT.PORT=0" & vbCrLf
'sConsulta = sConsulta & "                            INNER JOIN REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                                    ON INSERTED.CIA=REL_CIAS.CIA_PROVE " & vbCrLf
'sConsulta = sConsulta & "                                   AND REL_CIAS.EST=3 " & vbCrLf
'sConsulta = sConsulta & "                            INNER JOIN CIAS " & vbCrLf
'sConsulta = sConsulta & "                                    ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
'sConsulta = sConsulta & "                                   AND CIAS.FCEST=3 " & vbCrLf
'sConsulta = sConsulta & "                     WHERE INSERTED.ID=@ID " & vbCrLf
'sConsulta = sConsulta & "                       AND INSERTED.CIA=@CIA)" & vbCrLf
'sConsulta = sConsulta & "    IF @ENLAZADA >0" & vbCrLf
'sConsulta = sConsulta & "      BEGIN" & vbCrLf
'sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
'sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
'sConsulta = sConsulta & "        ELSE" & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE= @INDICE + 1" & vbCrLf
'sConsulta = sConsulta & "        INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@CIA,@ID,'U',1,GETDATE())" & vbCrLf
'sConsulta = sConsulta & "      END" & vbCrLf
'sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @CIA,@ID" & vbCrLf
'sConsulta = sConsulta & "  END" & vbCrLf
'sConsulta = sConsulta & "CLOSE C1" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_PORT_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[USU_PORT_TG_UPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_PORT_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[USU_PORT_TG_DEL]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TRIGGER [USU_PORT_TG_UPD] ON [USU_PORT] " & vbCrLf
'sConsulta = sConsulta & "FOR UPDATE " & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA INT,@USU INT,@PORT INT,@EST INT,@ESTOLD INT,@ID INT,@USU_CIA INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_USU_PORT_Upd CURSOR FOR SELECT CIA,USU,PORT,FPEST FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_USU_PORT_Upd" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_USU_PORT_Upd INTO @CIA,@USU,@PORT,@EST" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "       UPDATE USU_PORT SET FECLASTUPD=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
'sConsulta = sConsulta & "       SET @ESTOLD=(SELECT FPEST FROM DELETED  WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT)" & vbCrLf
'sConsulta = sConsulta & "       IF @ESTOLD<>3 AND @EST=3" & vbCrLf
'sConsulta = sConsulta & "       BEGIN" & vbCrLf
'sConsulta = sConsulta & "           UPDATE USU_PORT SET FECACTFP=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "       ELSE" & vbCrLf
'sConsulta = sConsulta & "           IF @ESTOLD=3 AND @EST<> 3 " & vbCrLf
'sConsulta = sConsulta & "           BEGIN" & vbCrLf
'sConsulta = sConsulta & "               UPDATE USU_PORT SET FECDESACTFP=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
'sConsulta = sConsulta & "           END" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curTG_USU_PORT_Upd INTO @CIA,@USU,@PORT,@EST" & vbCrLf
'sConsulta = sConsulta & "   END" & vbCrLf
'sConsulta = sConsulta & "CLOSE curTG_USU_PORT_Upd" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_USU_PORT_Upd  " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare c1 cursor local for select usu, cia from INSERTED" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "open c1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "fetch next from c1 into @ID, @CIA" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "  BEGIN" & vbCrLf
'sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
'sConsulta = sConsulta & "                      FROM USU " & vbCrLf
'sConsulta = sConsulta & "                            INNER JOIN INSERTED " & vbCrLf
'sConsulta = sConsulta & "                                    ON USU.ID=INSERTED.USU " & vbCrLf
'sConsulta = sConsulta & "                                   AND USU.CIA=INSERTED.CIA " & vbCrLf
'sConsulta = sConsulta & "                                   AND (INSERTED.FPEST=3 OR INSERTED.FPEST=2)" & vbCrLf
'sConsulta = sConsulta & "                                   AND INSERTED.PORT=0 " & vbCrLf
'sConsulta = sConsulta & "                            INNER JOIN REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                                    ON USU.CIA=REL_CIAS.CIA_PROVE " & vbCrLf
'sConsulta = sConsulta & "                                   AND REL_CIAS.EST=3 " & vbCrLf
'sConsulta = sConsulta & "                            INNER JOIN CIAS " & vbCrLf
'sConsulta = sConsulta & "                                    ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
'sConsulta = sConsulta & "                                   AND CIAS.FCEST=3 " & vbCrLf
'sConsulta = sConsulta & "                     WHERE USU.ID=INSERTED.USU " & vbCrLf
'sConsulta = sConsulta & "                       AND USU.CIA=INSERTED.CIA)" & vbCrLf
'sConsulta = sConsulta & "    IF @ENLAZADA > 0" & vbCrLf
'sConsulta = sConsulta & "      BEGIN" & vbCrLf
'sConsulta = sConsulta & "        SET @INDICE = (SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
'sConsulta = sConsulta & "        IF @INDICE IS NULL" & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE = 1" & vbCrLf
'sConsulta = sConsulta & "        ELSE" & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE = @INDICE + 1    " & vbCrLf
'sConsulta = sConsulta & "        INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@CIA,@ID,'U',1,GETDATE())" & vbCrLf
'sConsulta = sConsulta & "      END" & vbCrLf
'sConsulta = sConsulta & "    fetch next from c1 into @ID, @CIA" & vbCrLf
'sConsulta = sConsulta & "  END  " & vbCrLf
'sConsulta = sConsulta & "CLOSE C1" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER [USU_PORT_TG_DEL] ON dbo.USU_PORT" & vbCrLf
'sConsulta = sConsulta & "FOR DELETE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ENLAZADA INT,@CIA INT,@ID INT,@INDICE INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare c1 cursor local for select usu, cia from deleted" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "open c1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "fetch next from c1 into @ID, @CIA" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "  BEGIN" & vbCrLf
'sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) FROM USU " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN DELETED " & vbCrLf
'sConsulta = sConsulta & "                                  ON USU.ID=DELETED.USU " & vbCrLf
'sConsulta = sConsulta & "                                 AND USU.CIA=DELETED.CIA " & vbCrLf
'sConsulta = sConsulta & "                                 AND (DELETED.FPEST=3 OR DELETED.FPEST=2) " & vbCrLf
'sConsulta = sConsulta & "                                 AND DELETED.PORT=0 " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                                  ON USU.CIA=REL_CIAS.CIA_PROVE " & vbCrLf
'sConsulta = sConsulta & "                                 AND REL_CIAS.EST=3 " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN CIAS " & vbCrLf
'sConsulta = sConsulta & "                                  ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
'sConsulta = sConsulta & "                                 AND CIAS.FCEST=3 " & vbCrLf
'sConsulta = sConsulta & "                    WHERE USU.ID=@ID " & vbCrLf
'sConsulta = sConsulta & "                      AND USU.CIA=@CIA)" & vbCrLf
'sConsulta = sConsulta & "    IF @ENLAZADA > 0" & vbCrLf
'sConsulta = sConsulta & "      BEGIN" & vbCrLf
'sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV) " & vbCrLf
'sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
'sConsulta = sConsulta & "        ELSE" & vbCrLf
'sConsulta = sConsulta & "          SET @INDICE=@INDICE+1" & vbCrLf
'sConsulta = sConsulta & "        INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@CIA,@ID,'D',1,GETDATE())" & vbCrLf
'sConsulta = sConsulta & "      END" & vbCrLf
'sConsulta = sConsulta & "    fetch next from c1 into @ID, @CIA" & vbCrLf
'sConsulta = sConsulta & "  END" & vbCrLf
'sConsulta = sConsulta & "CLOSE C1" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT1_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT1_TG_INSUPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT2_TG_INS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT2_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT2_TG_INSUPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT3_TG_INS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT3_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT3_TG_INSUPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT4_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT4_TG_INS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT4_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT4_TG_INSUPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT5_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT5_TG_INS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT5_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[ACT5_TG_INSUPD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER ACT1_TG_INSUPD ON dbo.ACT1" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "UPDATE ACT1 " & vbCrLf
'sConsulta = sConsulta & "   SET FECACT=GETDATE()" & vbCrLf
'sConsulta = sConsulta & "  FROM ACT1 A" & vbCrLf
'sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
'sConsulta = sConsulta & "            ON A.ID = I.ID" & vbCrLf
'sConsulta = sConsulta & " " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER ACT2_TG_INS ON dbo.ACT2" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "INSERT INTO CIAS_ACT2 (CIA, ACT1, ACT2)" & vbCrLf
'sConsulta = sConsulta & "SELECT C.CIA,I.ACT1,I.ID" & vbCrLf
'sConsulta = sConsulta & "  FROM INSERTED I" & vbCrLf
'sConsulta = sConsulta & "         INNER JOIN CIAS_ACT1 C" & vbCrLf
'sConsulta = sConsulta & "                 ON I.ACT1 = C.ACT1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER ACT2_TG_INSUPD ON dbo.ACT2 " & vbCrLf
'sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "UPDATE ACT2" & vbCrLf
'sConsulta = sConsulta & "   SET FECACT = GETDATE()" & vbCrLf
'sConsulta = sConsulta & "  FROM ACT2 A " & vbCrLf
'sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
'sConsulta = sConsulta & "            ON A.ACT1=I.ACT1" & vbCrLf
'sConsulta = sConsulta & "           AND A.ID=I.ID" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER ACT3_TG_INS ON dbo.ACT3" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "INSERT INTO CIAS_ACT3 (CIA, ACT1, ACT2,ACT3)" & vbCrLf
'sConsulta = sConsulta & "SELECT C.CIA,I.ACT1,I.ACT2,I.ID" & vbCrLf
'sConsulta = sConsulta & "  FROM INSERTED I" & vbCrLf
'sConsulta = sConsulta & "         INNER JOIN CIAS_ACT2 C" & vbCrLf
'sConsulta = sConsulta & "                 ON I.ACT1 = C.ACT1" & vbCrLf
'sConsulta = sConsulta & "                AND I.ACT2 = C.ACT2" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER ACT3_TG_INSUPD ON dbo.ACT3" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "UPDATE ACT3" & vbCrLf
'sConsulta = sConsulta & "   SET FECACT = GETDATE()" & vbCrLf
'sConsulta = sConsulta & "  FROM ACT3 A " & vbCrLf
'sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
'sConsulta = sConsulta & "            ON A.ACT1=I.ACT1" & vbCrLf
'sConsulta = sConsulta & "           AND A.ACT2=I.ACT2" & vbCrLf
'sConsulta = sConsulta & "           AND A.ID=I.ID" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER ACT4_TG_INS ON dbo.ACT4" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "INSERT INTO CIAS_ACT4 (CIA, ACT1, ACT2, ACT3, ACT4)" & vbCrLf
'sConsulta = sConsulta & "SELECT C.CIA,I.ACT1,I.ACT2,I.ACT3, I.ID" & vbCrLf
'sConsulta = sConsulta & "  FROM INSERTED I" & vbCrLf
'sConsulta = sConsulta & "         INNER JOIN CIAS_ACT3 C" & vbCrLf
'sConsulta = sConsulta & "                 ON I.ACT1 = C.ACT1" & vbCrLf
'sConsulta = sConsulta & "                AND I.ACT2 = C.ACT2" & vbCrLf
'sConsulta = sConsulta & "                AND I.ACT3 = C.ACT3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER ACT4_TG_INSUPD ON dbo.ACT4" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "UPDATE ACT4" & vbCrLf
'sConsulta = sConsulta & "   SET FECACT = GETDATE()" & vbCrLf
'sConsulta = sConsulta & "  FROM ACT4 A " & vbCrLf
'sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
'sConsulta = sConsulta & "            ON A.ACT1=I.ACT1" & vbCrLf
'sConsulta = sConsulta & "           AND A.ACT2=I.ACT2" & vbCrLf
'sConsulta = sConsulta & "           AND A.ACT3=I.ACT3" & vbCrLf
'sConsulta = sConsulta & "           AND A.ID=I.ID" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "CREATE TRIGGER ACT5_TG_INS ON dbo.ACT5" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "INSERT INTO CIAS_ACT5 (CIA, ACT1, ACT2, ACT3, ACT4, ACT5)" & vbCrLf
'sConsulta = sConsulta & "SELECT C.CIA,I.ACT1,I.ACT2,I.ACT3, I.ACT4, I.ID" & vbCrLf
'sConsulta = sConsulta & "  FROM INSERTED I" & vbCrLf
'sConsulta = sConsulta & "         INNER JOIN CIAS_ACT4 C" & vbCrLf
'sConsulta = sConsulta & "                 ON I.ACT1 = C.ACT1" & vbCrLf
'sConsulta = sConsulta & "                AND I.ACT2 = C.ACT2" & vbCrLf
'sConsulta = sConsulta & "                AND I.ACT3 = C.ACT3" & vbCrLf
'sConsulta = sConsulta & "                AND I.ACT4 = C.ACT4" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER ACT5_TG_INSUPD ON dbo.ACT5" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "UPDATE ACT5" & vbCrLf
'sConsulta = sConsulta & "   SET FECACT = GETDATE()" & vbCrLf
'sConsulta = sConsulta & "  FROM ACT5 A " & vbCrLf
'sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
'sConsulta = sConsulta & "            ON A.ACT1=I.ACT1" & vbCrLf
'sConsulta = sConsulta & "           AND A.ACT2=I.ACT2" & vbCrLf
'sConsulta = sConsulta & "           AND A.ACT3=I.ACT3" & vbCrLf
'sConsulta = sConsulta & "           AND A.ACT4=I.ACT4" & vbCrLf
'sConsulta = sConsulta & "           AND A.ID=I.ID" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MCIAS_ACT1_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[MCIAS_ACT1_TG_INS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MCIAS_ACT2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[MCIAS_ACT2_TG_INS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MCIAS_ACT3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[MCIAS_ACT3_TG_INS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER MCIAS_ACT1_TG_INS ON dbo.MCIAS_ACT1" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "    " & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_MCia_Act1_Ins CURSOR FOR SELECT CIA,ACT1 FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_MCia_Act1_Ins" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act1_Ins INTO @CIA,@ACT1" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
'sConsulta = sConsulta & "INSERT INTO MCIAS_ACT2 SELECT @CIA,ACT1,ID FROM ACT2 WHERE ACT1=@ACT1" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act1_Ins INTO @CIA,@ACT1" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "Close curTG_MCia_Act1_Ins" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_MCia_Act1_Ins" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER MCIAS_ACT2_TG_INS ON dbo.MCIAS_ACT2" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_MCia_Act2_Ins CURSOR FOR SELECT CIA,ACT1,ACT2 FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_MCia_Act2_Ins" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act2_Ins INTO @CIA,@ACT1,@ACT2" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
'sConsulta = sConsulta & "INSERT INTO MCIAS_ACT3 SELECT @CIA,ACT1,ACT2,ID FROM ACT3 WHERE ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act2_Ins INTO @CIA,@ACT1,@ACT2" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "Close curTG_MCia_Act2_Ins" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_MCia_Act2_Ins" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER MCIAS_ACT3_TG_INS ON dbo.MCIAS_ACT3" & vbCrLf
'sConsulta = sConsulta & "FOR INSERT" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER,@ACT3 INTEGER" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE curTG_MCia_Act3_Ins CURSOR FOR SELECT CIA,ACT1,ACT2,ACT3 FROM INSERTED" & vbCrLf
'sConsulta = sConsulta & "OPEN curTG_MCia_Act3_Ins" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act3_Ins INTO @CIA,@ACT1,@ACT2,@ACT3" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3= @ACT3" & vbCrLf
'sConsulta = sConsulta & "INSERT INTO MCIAS_ACT4 SELECT @CIA,ACT1,ACT2,ACT3,ID FROM ACT4 WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act3_Ins INTO @CIA,@ACT1,@ACT2,@ACT3" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "Close curTG_MCia_Act3_Ins" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE curTG_MCia_Act3_Ins" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'
'End Sub
'
'Private Sub V_2_9_5_Storeds()
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[MON_COD]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DETALLE_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_DETALLE_PROVE_PORTAL]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ACTIVIDADES_CIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ACTIVIDADES_CIA]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_MVTOS_PROV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_MVTOS_PROV]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_PROV_REINTENTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRANSFER_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_TRANSFER_PROVE_ESP]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_LOGIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[USU_LOGIN]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CAMBIO_NIVEL_ACT_CIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_CAMBIO_NIVEL_ACT_CIAS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_PENDIENTE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_PENDIENTE]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_VISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_VISIBLE]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_PRUEBA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_PRUEBA]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "CREATE PROCEDURE MON_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
'sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
'sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
'sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM MON WHERE COD=@OLD" & vbCrLf
'sConsulta = sConsulta & "OPEN C" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
'sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE CIAS_MON NOCHECK CONSTRAINT ALL" & vbCrLf
'sConsulta = sConsulta & "ALTER TABLE VOLADJ NOCHECK CONSTRAINT ALL" & vbCrLf
'sConsulta = sConsulta & "UPDATE MON SET COD=@NEW WHERE COD=@OLD" & vbCrLf
'sConsulta = sConsulta & "UPDATE CIAS_MON SET MON_PORTAL=@NEW WHERE MON_PORTAL=@OLD" & vbCrLf
'sConsulta = sConsulta & "UPDATE VOLADJ SET MONPOR=@NEW WHERE MONPOR=@OLD" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "Close C" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
'sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
'sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(3),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
'sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
'sConsulta = sConsulta & "   END" & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
'sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
'sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
'sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
'sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       BEGIN" & vbCrLf
'sConsulta = sConsulta & "    " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
'sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
'sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "              IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "                IF @FCEST = 3" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "            BEGIN" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "             SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
'sConsulta = sConsulta & "             FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
'sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
'sConsulta = sConsulta & "                    " & vbCrLf
'sConsulta = sConsulta & "                                                   " & vbCrLf
'sConsulta = sConsulta & "           SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
'sConsulta = sConsulta & "                       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "          END                                          " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "                " & vbCrLf
'sConsulta = sConsulta & "      " & vbCrLf
'sConsulta = sConsulta & "             ELSE  " & vbCrLf
'sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "                            BEGIN" & vbCrLf
'sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTOS @COD_PROVE_CIA=@COD_PROVE_CIA'" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SET @PARAM= '@COD_PROVE_CIA VARCHAR(200)'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           DECLARE Cur_CONTAC CURSOR FOR   " & vbCrLf
'sConsulta = sConsulta & "               SELECT APE,NOM,DEP,CAR,TFNO,TFNO2,FAX,EMAIL,TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "                FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                   USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND " & vbCrLf
'sConsulta = sConsulta & "                   USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            OPEN Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_DEP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_FAX VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO2 VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @COD_PROVE_CIA VARCHAR(200)'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "           BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                   @COD_PROVE_CIA=@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           END" & vbCrLf
'sConsulta = sConsulta & "           CLOSE Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "           DEALLOCATE Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "             ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
'sConsulta = sConsulta & "           BEGIN   " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
'sConsulta = sConsulta & "                  BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                                      " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                                           " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                  END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
'sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
'sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
'sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "            " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
'sConsulta = sConsulta & "             ELSE" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                       SET @INTENTO = 1             " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "     " & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
'sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_DETALLE_PROVE_PORTAL @SCOD VARCHAR(30) , @SIDI VARCHAR(8) AS" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA1 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA2 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA3 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA4 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA5 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA1= 'ACT1.' + @SIDI" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA2= 'ACT2.' + @SIDI" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA3= 'ACT3.' + @SIDI" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA4= 'ACT4.' + @SIDI" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA5= 'ACT5.' + @SIDI" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare @SQL NVARCHAR(4000)" & vbCrLf
'sConsulta = sConsulta & "set @SQL = '" & vbCrLf
'sConsulta = sConsulta & "SELECT CIAS.DEN,CIAS.COD,CIAS.NIF,CIAS.VOLFACT,CIAS.CP,CIAS.POB" & vbCrLf
'sConsulta = sConsulta & ",USU.NOM,USU.APE,USU.COD,USU.FCEST,USU_PORT.FPEST,USU.DEP,USU.CAR" & vbCrLf
'sConsulta = sConsulta & ",USU.TFNO,USU.TFNO2,USU.FAX,USU.EMAIL,USU.TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & ",null,null ,null,null,null,null,null,null " & vbCrLf
'sConsulta = sConsulta & "FROM CIAS  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PROVI ON CIAS.PROVI=PROVI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PAI ON CIAS.PAI=PAI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ESP ON CIAS.ID=CIAS_ESP.CIA   " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN USU ON CIAS.ID=USU.CIA " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN USU_PORT ON CIAS.ID=USU_PORT.CIA AND USU.ID=USU_PORT.USU " & vbCrLf
'sConsulta = sConsulta & "WHERE CIAS.COD =''' + @SCOD + '''" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "SELECT CIAS.DEN,CIAS.COD,CIAS.NIF,CIAS.VOLFACT,CIAS.CP,CIAS.POB" & vbCrLf
'sConsulta = sConsulta & ",null,null,null,null,null,null,null,null,null,null,null,null" & vbCrLf
'sConsulta = sConsulta & ",CIAS_ESP.NOM AS CIAESPNOM,CIAS_ESP.COM AS CIAESPCOM " & vbCrLf
'sConsulta = sConsulta & ",null,null,null,null,null,null" & vbCrLf
'sConsulta = sConsulta & "FROM CIAS  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PROVI ON CIAS.PROVI=PROVI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PAI ON CIAS.PAI=PAI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ESP ON CIAS.ID=CIAS_ESP.CIA  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_PORT ON CIAS.ID=CIAS_PORT.CIA " & vbCrLf
'sConsulta = sConsulta & "WHERE CIAS.COD = ''' + @SCOD + '''" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "SELECT CIAS.DEN,CIAS.COD,CIAS.NIF,CIAS.VOLFACT,CIAS.CP,CIAS.POB" & vbCrLf
'sConsulta = sConsulta & ",null,null,null,null,null,null,null,null,null,null,null,null" & vbCrLf
'sConsulta = sConsulta & ",null,null" & vbCrLf
'sConsulta = sConsulta & ",ACT1.ID,null,null,null,null, ' + @IDIOMA1 + '" & vbCrLf
'sConsulta = sConsulta & "FROM CIAS  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PROVI ON CIAS.PROVI=PROVI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PAI ON CIAS.PAI=PAI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT1  ON  CIAS.ID=CIAS_ACT1.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN ACT1 ON CIAS_ACT1.ACT1=ACT1.ID" & vbCrLf
'sConsulta = sConsulta & "WHERE CIAS.COD = ''' + @SCOD + '''" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "SELECT CIAS.DEN,CIAS.COD,CIAS.NIF,CIAS.VOLFACT,CIAS.CP,CIAS.POB" & vbCrLf
'sConsulta = sConsulta & ",null,null,null,null,null,null,null,null,null,null,null,null" & vbCrLf
'sConsulta = sConsulta & ",null,null" & vbCrLf
'sConsulta = sConsulta & ",ACT2.ACT1,ACT2.ID,null,null,null, ' + @IDIOMA2 + '" & vbCrLf
'sConsulta = sConsulta & "FROM CIAS  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PROVI ON CIAS.PROVI=PROVI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PAI ON CIAS.PAI=PAI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT2  J ON CIAS.ID =J.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN ACT2 ON J.ACT2=ACT2.ID AND J.ACT2=ACT2.ID  AND J.ACT1=ACT2.ACT1" & vbCrLf
'sConsulta = sConsulta & "inner join  ACT1 ON ACT1.ID = ACT2.ACT1  " & vbCrLf
'sConsulta = sConsulta & "WHERE CIAS.COD = ''' + @SCOD + '''" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "SELECT CIAS.DEN,CIAS.COD,CIAS.NIF,CIAS.VOLFACT,CIAS.CP,CIAS.POB" & vbCrLf
'sConsulta = sConsulta & ",null,null,null,null,null,null,null,null,null,null,null,null" & vbCrLf
'sConsulta = sConsulta & ",null,null" & vbCrLf
'sConsulta = sConsulta & ",ACT3.ACT1,ACT3.ACT2,ACT3.ID,null,null, ' + @IDIOMA3 + '" & vbCrLf
'sConsulta = sConsulta & "FROM CIAS  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PROVI ON CIAS.PROVI=PROVI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PAI ON CIAS.PAI=PAI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT3  J ON   CIAS.ID = J.CIA " & vbCrLf
'sConsulta = sConsulta & "INNER JOIN ACT3 ON J.ACT3=ACT3.ID  AND J.ACT3=ACT3.ID AND J.ACT2=ACT3.ACT2  AND J.ACT1=ACT3.ACT1 " & vbCrLf
'sConsulta = sConsulta & "inner join  ACT1 ON ACT1.ID = ACT3.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join  ACT2 ON ACT2.ID = ACT3.ACT2 AND ACT2.ACT1 = ACT3.ACT1  " & vbCrLf
'sConsulta = sConsulta & "WHERE CIAS.COD = ''' + @SCOD + '''" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "SELECT CIAS.DEN,CIAS.COD,CIAS.NIF,CIAS.VOLFACT,CIAS.CP,CIAS.POB" & vbCrLf
'sConsulta = sConsulta & ",null,null,null,null,null,null,null,null,null,null,null,null" & vbCrLf
'sConsulta = sConsulta & ",null,null" & vbCrLf
'sConsulta = sConsulta & ",ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID,null, ' + @IDIOMA4 + '" & vbCrLf
'sConsulta = sConsulta & "FROM CIAS  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PROVI ON CIAS.PROVI=PROVI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT4  J ON CIAS.ID = J.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN ACT4 ON J.ACT4=ACT4.ID AND J.ACT4=ACT4.ID AND J.ACT3=ACT4.ACT3 AND J.ACT2=ACT4.ACT2  AND J.ACT1=ACT4.ACT1" & vbCrLf
'sConsulta = sConsulta & "inner join  ACT1 ON ACT1.ID = ACT4.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join  ACT2 ON ACT2.ID = ACT4.ACT2 AND ACT2.ACT1 = ACT4.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join  ACT3 ON ACT3.ID = ACT4.ACT3 AND ACT3.ACT2 = ACT4.ACT2 AND ACT3.ACT1 = ACT4.ACT1  " & vbCrLf
'sConsulta = sConsulta & "WHERE CIAS.COD = ''' + @SCOD + '''" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "SELECT CIAS.DEN,CIAS.COD,CIAS.NIF,CIAS.VOLFACT,CIAS.CP,CIAS.POB" & vbCrLf
'sConsulta = sConsulta & ",null,null,null,null,null,null,null,null,null,null,null,null" & vbCrLf
'sConsulta = sConsulta & ",null,null" & vbCrLf
'sConsulta = sConsulta & ",ACT5.ACT1,ACT5.ACT2,ACT5.ACT3,ACT5.ACT4,ACT5.ID, ' + @IDIOMA5 + '" & vbCrLf
'sConsulta = sConsulta & "FROM CIAS  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PROVI ON CIAS.PROVI=PROVI.ID  " & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT5  J ON CIAS.ID = J.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN ACT5 ON J.ACT5=ACT5.ID AND J.ACT5=ACT5.ID AND J.ACT4=ACT5.ACT4 AND J.ACT3=ACT5.ACT3 AND J.ACT2=ACT5.ACT2  AND J.ACT1=ACT5.ACT1" & vbCrLf
'sConsulta = sConsulta & "inner join  ACT1 ON ACT1.ID = ACT5.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join ACT2 ON ACT2.ID = ACT5.ACT2 AND ACT2.ACT1 = ACT5.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join ACT3 ON ACT3.ID = ACT5.ACT3 AND ACT3.ACT2 = ACT5.ACT2 AND ACT3.ACT1 = ACT5.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join  ACT4 ON ACT4.ID = ACT5.ACT4 AND ACT4.ACT3 = ACT5.ACT3 AND ACT4.ACT2 = ACT5.ACT2 AND ACT4.ACT1 = ACT5.ACT1  " & vbCrLf
'sConsulta = sConsulta & "WHERE CIAS.COD = ''' + @SCOD + '''" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "ORDER BY 9'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "PRINT  @SQL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_ACTIVIDADES_CIA @CIA int,@IDI varchar(20),@TODO bit = 0" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
'sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int , ActAsign smallint)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if @TODO = 0" & vbCrLf
'sConsulta = sConsulta & "  begin" & vbCrLf
'sConsulta = sConsulta & "    --Insertamos los datos correspondientes a #raiz" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, act5, actAsign) " & vbCrLf
'sConsulta = sConsulta & "    select act1, act2, act3, act4, act5, 5" & vbCrLf
'sConsulta = sConsulta & "      from cias_act5 a" & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act4 b" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                          and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                          and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "                          and a.act4 = b.act4 )" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act3 c" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                          and a.act2 = c.act2" & vbCrLf
'sConsulta = sConsulta & "                          and a.act3 = c.act3 )" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act2 d" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = d.act1" & vbCrLf
'sConsulta = sConsulta & "                          and a.act2 = d.act2)" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act1 e" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = e.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, actAsign ) " & vbCrLf
'sConsulta = sConsulta & "    select act1, act2, act3, act4, 4  " & vbCrLf
'sConsulta = sConsulta & "      from cias_act4 a" & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act3 b" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                          and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                          and a.act3 = b.act3 )" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act2 c" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                          and a.act2 = c.act2 )" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act1 d" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = d.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, actAsign ) " & vbCrLf
'sConsulta = sConsulta & "    select act1, act2, act3, 3" & vbCrLf
'sConsulta = sConsulta & "      from cias_act3 a" & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act2 b" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                          and a.act2 = b.act2 )" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act1 c" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = c.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, act2, actAsign ) " & vbCrLf
'sConsulta = sConsulta & "    select act1, act2 , 2" & vbCrLf
'sConsulta = sConsulta & "      from cias_act2 a" & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                         from cias_act1 b" & vbCrLf
'sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                          and a.act1 = b.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, actAsign) " & vbCrLf
'sConsulta = sConsulta & "    select act1, 1 " & vbCrLf
'sConsulta = sConsulta & "      from cias_act1 " & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "  end" & vbCrLf
'sConsulta = sConsulta & "else" & vbCrLf
'sConsulta = sConsulta & "  begin" & vbCrLf
'sConsulta = sConsulta & "    --Insertamos los datos correspondientes a #raiz" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, act5, actAsign) " & vbCrLf
'sConsulta = sConsulta & "    select act1, act2, act3, act4, act5, 5" & vbCrLf
'sConsulta = sConsulta & "      from cias_act5 a" & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, actAsign ) " & vbCrLf
'sConsulta = sConsulta & "    select act1, act2, act3, act4, 4  " & vbCrLf
'sConsulta = sConsulta & "      from cias_act4 a" & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, actAsign ) " & vbCrLf
'sConsulta = sConsulta & "    select act1, act2, act3, 3" & vbCrLf
'sConsulta = sConsulta & "      from cias_act3 a" & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, act2, actAsign ) " & vbCrLf
'sConsulta = sConsulta & "    select act1, act2 , 2" & vbCrLf
'sConsulta = sConsulta & "      from cias_act2 a" & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "    insert into #raiz (act1, actAsign) " & vbCrLf
'sConsulta = sConsulta & "    select act1, 1 " & vbCrLf
'sConsulta = sConsulta & "      from cias_act1 " & vbCrLf
'sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "  end" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET NOCOUNT OFF" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
'sConsulta = sConsulta & "select #raiz.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
'sConsulta = sConsulta & "       #raiz.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
'sConsulta = sConsulta & "       #raiz.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
'sConsulta = sConsulta & "       #raiz.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
'sConsulta = sConsulta & "       #raiz.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
'sConsulta = sConsulta & "       actasign" & vbCrLf
'sConsulta = sConsulta & "from #raiz  " & vbCrLf
'sConsulta = sConsulta & "     left join act1 " & vbCrLf
'sConsulta = sConsulta & "            on #raiz.act1 = act1.id" & vbCrLf
'sConsulta = sConsulta & "     left join act2 " & vbCrLf
'sConsulta = sConsulta & "            on #raiz.act1=act2.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act2 = act2.id" & vbCrLf
'sConsulta = sConsulta & "     left join act3 " & vbCrLf
'sConsulta = sConsulta & "            on #raiz.act1=act3.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act2 = act3.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act3 = act3.id" & vbCrLf
'sConsulta = sConsulta & "     left join act4 " & vbCrLf
'sConsulta = sConsulta & "            on #raiz.act1=act4.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act2 = act4.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act3 = act4.act3 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act4 = act4.id" & vbCrLf
'sConsulta = sConsulta & "     left join act5 " & vbCrLf
'sConsulta = sConsulta & "            on #raiz.act1=act5.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act2 = act5.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act3 = act5.act3 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act4 = act5.act4 " & vbCrLf
'sConsulta = sConsulta & "           and #raiz.act5= act5.id" & vbCrLf
'sConsulta = sConsulta & "order by #raiz.act1, #raiz.act2,#raiz.act3,#raiz.act4,#raiz.act5" & vbCrLf
'sConsulta = sConsulta & "'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_MVTOS_PROV AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "/********************************************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "/******       Descripci�n : Borra los registros de MVTOS_PROV  y MVTOS_PROV_EST que han acabado  correctamente        ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                                                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                                                     ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                     ******/" & vbCrLf
'sConsulta = sConsulta & "/********************************************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Borramos los registros correspondientes de MVTOS_PROV" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DELETE MVTOS_PROV FROM MVTOS_PROV INNER JOIN MVTOS_PROV_EST ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Borramos tambien los registos correspondientes de MVTOS_PROV_EST" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MVTOS_PROV_EST WHERE EST=3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_PROV_REINTENTOS AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "/******       Descripci�n : Intenta pasar los movimientos fallidos si toca       ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                                         ******/" & vbCrLf
'sConsulta = sConsulta & "/******       Par�metros :  ------------------                                                           ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                        ******/" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR,@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(1000),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(3),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@ESPEC INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @CICLOS= (SELECT PROV_CICLOS FROM PARGEN_GEST) " & vbCrLf
'sConsulta = sConsulta & "SET @REINTENTOS = (SELECT PROV_REINTENTOS FROM PARGEN_GEST)" & vbCrLf
'sConsulta = sConsulta & "SET @PASADAS = (SELECT PROV_PASADAS FROM PARGEN_GEST)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "IF @CICLOS <> 0 AND @PASADAS%@CICLOS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "      BEGIN" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
'sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
'sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
'sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
'sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 1 AND MVTOS_PROV_EST.INTENTOS < @REINTENTOS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       BEGIN" & vbCrLf
'sConsulta = sConsulta & "    " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
'sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
'sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "              IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "                IF @FCEST = 3" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "            BEGIN" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "             SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
'sConsulta = sConsulta & "             FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
'sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
'sConsulta = sConsulta & "                    " & vbCrLf
'sConsulta = sConsulta & "                                                   " & vbCrLf
'sConsulta = sConsulta & "           SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
'sConsulta = sConsulta & "                       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "          END                                          " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "                " & vbCrLf
'sConsulta = sConsulta & "      " & vbCrLf
'sConsulta = sConsulta & "             ELSE  " & vbCrLf
'sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "                            BEGIN" & vbCrLf
'sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTOS @COD_PROVE_CIA=@COD_PROVE_CIA'" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SET @PARAM= '@COD_PROVE_CIA VARCHAR(200)'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           DECLARE Cur_CONTAC CURSOR FOR   " & vbCrLf
'sConsulta = sConsulta & "               SELECT APE,NOM,DEP,CAR,TFNO,TFNO2,FAX,EMAIL,TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "                FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                   USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND " & vbCrLf
'sConsulta = sConsulta & "                   USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            OPEN Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_DEP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_FAX VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO2 VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @COD_PROVE_CIA VARCHAR(200)'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "           BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                   @COD_PROVE_CIA=@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           END" & vbCrLf
'sConsulta = sConsulta & "           CLOSE Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "           DEALLOCATE Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "             ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
'sConsulta = sConsulta & "           BEGIN   " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'         " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
'sConsulta = sConsulta & "                  BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                                      " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                                           " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                  END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
'sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
'sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
'sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "            " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "            IF @@ERROR <> 0"
'sConsulta = sConsulta & "                BEGIN " & vbCrLf
'sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1" & vbCrLf
'sConsulta = sConsulta & "             IF @INTENTO = @REINTENTOS" & vbCrLf
'sConsulta = sConsulta & "               SET @EST_FILA = 2" & vbCrLf
'sConsulta = sConsulta & "             ELSE" & vbCrLf
'sConsulta = sConsulta & "               SET @EST_FILA = 1" & vbCrLf
'sConsulta = sConsulta & "                END" & vbCrLf
'sConsulta = sConsulta & "             ELSE" & vbCrLf
'sConsulta = sConsulta & "                BEGIN" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 3"
'sConsulta = sConsulta & "                 END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "     " & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @PASADAS = @PASADAS + 1" & vbCrLf
'sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS = @PASADAS" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_TRANSFER_PROVE_ESP @CIA INT, @ESP INT,@INIT INT,@SIZE INT AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
'sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
'sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET nocount on" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM CIAS_ESP WHERE CIA=@CIA AND ID=@ESP " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
'sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
'sConsulta = sConsulta & "ELSE" & vbCrLf
'sConsulta = sConsulta & " BEGIN" & vbCrLf
'sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
'sConsulta = sConsulta & " END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "READTEXT  CIAS_ESP.DATA @textpointer @INIT  @LSIZE" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE USU_LOGIN(@USUOLD VARCHAR(50),@USUNEW VARCHAR(50),@PWDNEW VARCHAR(512),@FECPWD DATETIME) AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
'sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   UPDATE ADM SET  USU=@USUNEW,PWD=@PWDNEW,FECPWD=@FECPWD WHERE USU=@USUOLD" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "   IF @@ERROR=0" & vbCrLf
'sConsulta = sConsulta & "     begin " & vbCrLf
'sConsulta = sConsulta & "       print @@error" & vbCrLf
'sConsulta = sConsulta & "       COMMIT TRANSACTION" & vbCrLf
'sConsulta = sConsulta & "     end" & vbCrLf
'sConsulta = sConsulta & "   ELSE" & vbCrLf
'sConsulta = sConsulta & "       SET XACT_ABORT OFF" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_CAMBIO_NIVEL_ACT_CIAS (@NIVEL INTEGER, @RES INTEGER OUTPUT) AS" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "DECLARE @NIVEL_SUP AS INTEGER" & vbCrLf
'sConsulta = sConsulta & "DECLARE @NIVEL_INF AS INTEGER" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ERROR AS INTEGER" & vbCrLf
'sConsulta = sConsulta & "IF @NIVEL = 2 " & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT1" & vbCrLf
'sConsulta = sConsulta & "   SELECT @NIVEL_INF =  COUNT( DISTINCT (ACT1))  FROM ACT2" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "ELSE" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "IF @NIVEL = 3" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT2" & vbCrLf
'sConsulta = sConsulta & "   SELECT  ACT2.ACT1,ACT2.ID FROM ACT2" & vbCrLf
'sConsulta = sConsulta & "   INNER JOIN ACT3 ON ACT3.ACT1 = ACT2.ACT1 AND ACT3.ACT2 = ACT2.ID " & vbCrLf
'sConsulta = sConsulta & "   GROUP BY ACT2.ACT1,ACT2.ID" & vbCrLf
'sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "ELSE" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "IF @NIVEL=4" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT3" & vbCrLf
'sConsulta = sConsulta & "   SELECT ACT3.ACT1,ACT3.ACT2,ACT3.ID FROM ACT3" & vbCrLf
'sConsulta = sConsulta & "   INNER JOIN ACT4 ON ACT4.ACT1 = ACT3.ACT1 AND ACT4.ACT2 = ACT3.ACT2 AND ACT4.ACT3=ACT3.ID " & vbCrLf
'sConsulta = sConsulta & "   GROUP BY ACT3.ACT2,ACT3.ACT1,ACT3.ID" & vbCrLf
'sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "ELSE" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "IF @NIVEL=5" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT4" & vbCrLf
'sConsulta = sConsulta & "   SELECT ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID FROM ACT4" & vbCrLf
'sConsulta = sConsulta & "   INNER JOIN ACT5 ON ACT5.ACT1 = ACT4.ACT1 AND ACT5.ACT2 = ACT4.ACT2 AND ACT5.ACT3=ACT4.ACT3 AND ACT5.ACT4=ACT4.ID " & vbCrLf
'sConsulta = sConsulta & "   GROUP BY ACT4.ACT3,ACT4.ACT2,ACT4.ACT1,ACT4.ID" & vbCrLf
'sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "IF @NIVEL_INF < @NIVEL_SUP " & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "   SET @ERROR = 1" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "ELSE" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "   SET @ERROR = 0" & vbCrLf
'sConsulta = sConsulta & "END " & vbCrLf
'sConsulta = sConsulta & "IF @ERROR = 0 " & vbCrLf
'sConsulta = sConsulta & "begin" & vbCrLf
'sConsulta = sConsulta & "   IF @NIVEL = 2 " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "   DELETE FROM CIAS_ACT1 " & vbCrLf
'sConsulta = sConsulta & "   DELETE FROM MCIAS_ACT1 " & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   IF @NIVEL = 3" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "   IF @NIVEL = 4" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "                DELETE FROM CIAS_ACT3 " & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT3" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "   IF @NIVEL = 5" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "                DELETE FROM CIAS_ACT3 " & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM CIAS_ACT4 " & vbCrLf
'sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT4 " & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "SET @RES=@ERROR" & vbCrLf
'sConsulta = sConsulta & "RETURN " & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL @SDEN VARCHAR(8),@SBUSCAR VARCHAR(50),@SCOMP INT AS" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA1 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA2 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA3 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA4 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "declare @IDIOMA5 AS VARCHAR(15)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA1= 'ACT1.' + @SDEN" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA2= 'ACT2.' + @SDEN" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA3= 'ACT3.' + @SDEN" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA4= 'ACT4.' + @SDEN" & vbCrLf
'sConsulta = sConsulta & "SET @IDIOMA5= 'ACT5.' + @SDEN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare @SQL NVARCHAR(4000)" & vbCrLf
'sConsulta = sConsulta & "set @SQL = '" & vbCrLf
'sConsulta = sConsulta & "SELECT ACT1.ID,NULL,NULL, NULL,NULL, ACT1.COD, ' + @IDIOMA1 + ' ,COUNT(J.CIA) AS PROVE" & vbCrLf
'sConsulta = sConsulta & "FROM ACT1" & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT1  J ON J.ACT1=ACT1.ID " & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS AS C ON C.ID = J.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS_PORT AS CIAS_PORT ON CIAS_PORT.CIA=C.ID" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN REL_COMP AS REL_COMP ON REL_COMP.CIA_PROVE=C.ID AND '  + CAST(@SCOMP AS VARCHAR) + '=REL_COMP.CIA_COMP " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "WHERE ' +  @IDIOMA1+ ' LIKE ''' + @SBUSCAR + '% ''" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "GROUP BY   ACT1.ID, ACT1.COD, ' + @IDIOMA1 + '" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT ACT2.ACT1, ACT2.ID,NULL, NULL,NULL, ACT2.COD, ' + @IDIOMA2 + ',COUNT(J.CIA) AS PROVE" & vbCrLf
'sConsulta = sConsulta & "FROM ACT2" & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT2  J ON J.ACT2=ACT2.ID  AND J.ACT1=ACT2.ACT1 " & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS AS C ON C.ID = J.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS_PORT AS CIAS_PORT ON CIAS_PORT.CIA=C.ID" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN REL_COMP AS REL_COMP ON REL_COMP.CIA_PROVE=C.ID AND ' + CAST(@SCOMP AS VARCHAR) + '=REL_COMP.CIA_COMP " & vbCrLf
'sConsulta = sConsulta & "inner join ACT1 AS ACT1  ON ACT1.ID = ACT2.ACT1  " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "WHERE ' +  @IDIOMA2+ ' LIKE ''' + @SBUSCAR + '%''" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "GROUP BY  ACT2.ACT1, ACT2.ID, ACT2.COD, ' + @IDIOMA2 + '" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT ACT3.ACT1,ACT3.ACT2, ACT3.ID, NULL,NULL,ACT3.COD, ' + @IDIOMA3 + ',COUNT(J.CIA) AS PROVE" & vbCrLf
'sConsulta = sConsulta & "FROM ACT3" & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT3  J ON J.ACT3=ACT3.ID  AND J.ACT2=ACT3.ACT2  AND J.ACT1=ACT3.ACT1 " & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS AS C ON C.ID = J.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS_PORT AS CIAS_PORT ON CIAS_PORT.CIA=C.ID" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN REL_COMP AS REL_COMP ON REL_COMP.CIA_PROVE=C.ID AND ' + CAST(@SCOMP AS VARCHAR) + '=REL_COMP.CIA_COMP " & vbCrLf
'sConsulta = sConsulta & "inner join ACT1 AS ACT1  ON ACT1.ID = ACT3.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join ACT2 AS ACT2  ON ACT2.ID = ACT3.ACT2 AND ACT2.ACT1 = ACT3.ACT1  " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "WHERE ' +  @IDIOMA3+ ' LIKE ''' + @SBUSCAR + '%''" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "GROUP BY ACT3.ACT1,ACT3.ACT2,ACT3.ID,ACT3.COD, ' + @IDIOMA3 + '" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT ACT4.ACT1,ACT4.ACT2, ACT4.ACT3, ACT4.ID, NULL,ACT4.COD, ' + @IDIOMA4 + ',COUNT(J.CIA) AS PROVE" & vbCrLf
'sConsulta = sConsulta & "FROM ACT4" & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT4  J ON J.ACT4=ACT4.ID AND J.ACT3=ACT4.ACT3 AND J.ACT2=ACT4.ACT2  AND J.ACT1=ACT4.ACT1 " & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS AS C ON C.ID = J.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS_PORT AS CIAS_PORT ON CIAS_PORT.CIA=C.ID" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN REL_COMP AS REL_COMP ON REL_COMP.CIA_PROVE=C.ID AND ' + CAST(@SCOMP AS VARCHAR) + '=REL_COMP.CIA_COMP " & vbCrLf
'sConsulta = sConsulta & "inner join ACT1 AS ACT1  ON ACT1.ID = ACT4.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join ACT2 AS ACT2  ON ACT2.ID = ACT4.ACT2 AND ACT2.ACT1 = ACT4.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join ACT3 AS ACT3  ON ACT3.ID = ACT4.ACT3 AND ACT3.ACT2 = ACT4.ACT2 AND ACT3.ACT1 = ACT4.ACT1  " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "WHERE ' +  @IDIOMA4+ ' LIKE ''' + @SBUSCAR + '%''" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "GROUP BY ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID,ACT4.COD, ' + @IDIOMA4 + '" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "union" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT ACT5.ACT1,ACT5.ACT2, ACT5.ACT3, ACT5.ACT4, ACT5.ID,ACT5.COD, ' + @IDIOMA5 + ',COUNT(J.CIA) AS PROVE" & vbCrLf
'sConsulta = sConsulta & "FROM ACT5" & vbCrLf
'sConsulta = sConsulta & "LEFT JOIN CIAS_ACT5  J ON J.ACT5=ACT5.ID AND J.ACT4=ACT5.ACT4 AND J.ACT3=ACT5.ACT3 AND J.ACT2=ACT5.ACT2  AND J.ACT1=ACT5.ACT1 " & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS AS C ON C.ID = J.CIA" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN CIAS_PORT AS CIAS_PORT ON CIAS_PORT.CIA=C.ID" & vbCrLf
'sConsulta = sConsulta & "INNER JOIN REL_COMP AS REL_COMP ON REL_COMP.CIA_PROVE=C.ID AND ' + CAST(@SCOMP AS VARCHAR) + '=REL_COMP.CIA_COMP " & vbCrLf
'sConsulta = sConsulta & "inner join ACT1 AS ACT1  ON ACT1.ID = ACT5.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join ACT2 AS ACT2  ON ACT2.ID = ACT5.ACT2 AND ACT2.ACT1 = ACT5.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join ACT3 AS ACT3  ON ACT3.ID = ACT5.ACT3 AND ACT3.ACT2 = ACT5.ACT2 AND ACT3.ACT1 = ACT5.ACT1  " & vbCrLf
'sConsulta = sConsulta & "inner join ACT4 AS ACT4  ON ACT4.ID = ACT5.ACT4 AND ACT4.ACT3 = ACT5.ACT3 AND ACT4.ACT2 = ACT5.ACT2 AND ACT4.ACT1 = ACT5.ACT1  " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "WHERE ' +  @IDIOMA5+ ' LIKE ''' + @SBUSCAR + '%''" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "GROUP BY ACT5.ACT1,ACT5.ACT2,ACT5.ACT3,ACT5.ACT4,ACT5.ID,ACT5.COD, ' + @IDIOMA5 + '" & vbCrLf
'sConsulta = sConsulta & " " & vbCrLf
'sConsulta = sConsulta & "ORDER BY 1,2,3,4,5,6'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ACT_PENDIENTE  @CIA int,@IDI varchar(20)" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4, act5 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
'sConsulta = sConsulta & "  from cias_act5 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act4 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act3 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act2 d" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act1 e" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
'sConsulta = sConsulta & "  from cias_act4 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act3 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act2 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act1 d" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1, act2, act3 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
'sConsulta = sConsulta & "  from cias_act3 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act2 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act1 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1, act2 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2 " & vbCrLf
'sConsulta = sConsulta & "  from cias_act2 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act1 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1) " & vbCrLf
'sConsulta = sConsulta & "select act1 " & vbCrLf
'sConsulta = sConsulta & "  from cias_act1 " & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4, act5 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act5 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act4 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act3 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act2 d" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act1 e" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act4 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act3 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act2 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act1 d" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act3 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act2 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act1 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1, act2 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2 " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act2 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act1 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1) " & vbCrLf
'sConsulta = sConsulta & "select act1 " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act1 " & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--Si no hab�a actividades pendientes de confirmar, se copia #raiz en #mraiz" & vbCrLf
'sConsulta = sConsulta & "if (select count(*) from #mraiz)=0" & vbCrLf
'sConsulta = sConsulta & "  insert into #mraiz select * from #raiz" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4,act5)" & vbCrLf
'sConsulta = sConsulta & "  select act1,act2,act3,act4,min(act5) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz where act5 is not null" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz where act5 is not null) a" & vbCrLf
'sConsulta = sConsulta & "group by act1,act2,act3,act4" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4)" & vbCrLf
'sConsulta = sConsulta & "  select act1,act2,act3,min(act4) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz where act4 is not null" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz where act4 is not null) a" & vbCrLf
'sConsulta = sConsulta & "group by act1,act2,act3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1,act2,act3)" & vbCrLf
'sConsulta = sConsulta & "  select act1,act2,min(act3) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz where act3 is not null" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz where act3 is not null) a" & vbCrLf
'sConsulta = sConsulta & "group by act1,act2" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1,act2)" & vbCrLf
'sConsulta = sConsulta & "  select act1,min(act2) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz where act2 is not null" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz where act2 is not null) a" & vbCrLf
'sConsulta = sConsulta & "group by act1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1)" & vbCrLf
'sConsulta = sConsulta & "  select min(act1) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz) a" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4,act5)" & vbCrLf
'sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.act4,a.id" & vbCrLf
'sConsulta = sConsulta & "  from act5 a " & vbCrLf
'sConsulta = sConsulta & "     inner join #base b " & vbCrLf
'sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
'sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
'sConsulta = sConsulta & "            and a.act3 = b.act3 " & vbCrLf
'sConsulta = sConsulta & "            and a.act4 = b.act4" & vbCrLf
'sConsulta = sConsulta & " Where b.act5 Is Not Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4)" & vbCrLf
'sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.id" & vbCrLf
'sConsulta = sConsulta & "  from act4 a " & vbCrLf
'sConsulta = sConsulta & "     inner join #base b " & vbCrLf
'sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
'sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
'sConsulta = sConsulta & "            and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & " Where b.act5 Is Null " & vbCrLf
'sConsulta = sConsulta & "   And b.act4 Is Not Null" & vbCrLf
'sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
'sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
'sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
'sConsulta = sConsulta & "                      and a.act3=c.act3 " & vbCrLf
'sConsulta = sConsulta & "                      and a.id=c.act4)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3)" & vbCrLf
'sConsulta = sConsulta & "select a.act1,a.act2,a.id" & vbCrLf
'sConsulta = sConsulta & "  from act3 a " & vbCrLf
'sConsulta = sConsulta & "     inner join #base b " & vbCrLf
'sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
'sConsulta = sConsulta & "            and a.act2=b.act2" & vbCrLf
'sConsulta = sConsulta & " Where b.act4 Is Null " & vbCrLf
'sConsulta = sConsulta & "   And b.act3 Is Not Null" & vbCrLf
'sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
'sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
'sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
'sConsulta = sConsulta & "                      and a.id=c.act3)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1,Act2)" & vbCrLf
'sConsulta = sConsulta & "select a.act1,a.id" & vbCrLf
'sConsulta = sConsulta & "  from act2 a " & vbCrLf
'sConsulta = sConsulta & "     inner join #base b " & vbCrLf
'sConsulta = sConsulta & "             on a.act1=b.act1" & vbCrLf
'sConsulta = sConsulta & " Where b.act3 Is Null " & vbCrLf
'sConsulta = sConsulta & "   And b.act2 Is Not Null" & vbCrLf
'sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
'sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
'sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
'sConsulta = sConsulta & "                      and a.id = c.act2)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1)" & vbCrLf
'sConsulta = sConsulta & "select a.id" & vbCrLf
'sConsulta = sConsulta & "  from act1 a" & vbCrLf
'sConsulta = sConsulta & " where not exists (SELECT * " & vbCrLf
'sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
'sConsulta = sConsulta & "                    where a.id=c.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura set actAsign=0 ,actPend=0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actasign = 1" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1" & vbCrLf
'sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actasign = 2" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actasign = 3" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actasign = 4" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
'sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actasign = 5" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actpend = 1" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1" & vbCrLf
'sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actpend = 2" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actpend = 3" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actpend = 4" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
'sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actpend = 5" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
'sConsulta = sConsulta & "select #Estructura.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
'sConsulta = sConsulta & "       #Estructura.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
'sConsulta = sConsulta & "       #Estructura.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
'sConsulta = sConsulta & "       #Estructura.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
'sConsulta = sConsulta & "       #Estructura.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
'sConsulta = sConsulta & "       actasign, actpend" & vbCrLf
'sConsulta = sConsulta & "from #Estructura  " & vbCrLf
'sConsulta = sConsulta & "     left join act1 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1 = act1.id" & vbCrLf
'sConsulta = sConsulta & "     left join act2 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1=act2.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act2 = act2.id" & vbCrLf
'sConsulta = sConsulta & "     left join act3 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1=act3.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act2 = act3.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act3 = act3.id " & vbCrLf
'sConsulta = sConsulta & "     left join act4 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1=act4.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act2 = act4.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act3 = act4.act3 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act4 = act4.id" & vbCrLf
'sConsulta = sConsulta & "     left join act5 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1=act5.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act2 = act5.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act3 = act5.act3 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act4 = act5.act4 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act5= act5.id" & vbCrLf
'sConsulta = sConsulta & "Where actasign > 0 And actpend > 0" & vbCrLf
'sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
'sConsulta = sConsulta & "'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ACT_VISIBLE @CIA int,@IDI varchar(20)" & vbCrLf
'sConsulta = sConsulta & "AS" & vbCrLf
'sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
'sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
'sConsulta = sConsulta & "--la tabla temporal #mraiz contendr� las actividades pendientes de confirmar por el gerente del portal y en caso" & vbCrLf
'sConsulta = sConsulta & "--de que no haya nada pendiente, contendr� una copia de #raiz" & vbCrLf
'sConsulta = sConsulta & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--Insertamos los datos correspondientes a #raiz" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4, act5 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
'sConsulta = sConsulta & "  from cias_act5 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act4 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act3 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act2 d" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act1 e" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
'sConsulta = sConsulta & "  from cias_act4 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act3 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act2 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act1 d" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1, act2, act3 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
'sConsulta = sConsulta & "  from cias_act3 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act2 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act1 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1, act2 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2 " & vbCrLf
'sConsulta = sConsulta & "  from cias_act2 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from cias_act1 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #raiz (act1) " & vbCrLf
'sConsulta = sConsulta & "select act1 " & vbCrLf
'sConsulta = sConsulta & "  from cias_act1 " & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--Insertamos los datos correspondientes a #mraiz" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4, act5 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act5 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act4 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA " & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act3 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act2 d" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act1 e" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act4 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act3 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act2 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act1 d" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act3 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act2 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act1 c" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1, act2 ) " & vbCrLf
'sConsulta = sConsulta & "select act1, act2 " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act2 a" & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
'sConsulta = sConsulta & "                     from mcias_act1 b" & vbCrLf
'sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #mraiz (act1) " & vbCrLf
'sConsulta = sConsulta & "select act1 " & vbCrLf
'sConsulta = sConsulta & "  from mcias_act1 " & vbCrLf
'sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--Si no hab�a actividades pendientes de confirmar, se copia #raiz en #mraiz" & vbCrLf
'sConsulta = sConsulta & "if (select count(*) from #mraiz)=0" & vbCrLf
'sConsulta = sConsulta & "  insert into #mraiz select * from #raiz" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--La tabla #base va a contener las actividades de las cuales se obtendr�n la " & vbCrLf
'sConsulta = sConsulta & "--estructura completa (con asignadas, pendientes y hermanas de estas)" & vbCrLf
'sConsulta = sConsulta & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4,act5)" & vbCrLf
'sConsulta = sConsulta & "  select act1,act2,act3,act4,min(act5) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz where act5 is not null" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz where act5 is not null) a" & vbCrLf
'sConsulta = sConsulta & "group by act1,act2,act3,act4" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4)" & vbCrLf
'sConsulta = sConsulta & "  select act1,act2,act3,min(act4) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz where act4 is not null" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz where act4 is not null) a" & vbCrLf
'sConsulta = sConsulta & "group by act1,act2,act3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1,act2,act3)" & vbCrLf
'sConsulta = sConsulta & "  select act1,act2,min(act3) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz where act3 is not null" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz where act3 is not null) a" & vbCrLf
'sConsulta = sConsulta & "group by act1,act2" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1,act2)" & vbCrLf
'sConsulta = sConsulta & "  select act1,min(act2) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz where act2 is not null" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz where act2 is not null) a" & vbCrLf
'sConsulta = sConsulta & "group by act1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  insert into #base (act1)" & vbCrLf
'sConsulta = sConsulta & "  select min(act1) " & vbCrLf
'sConsulta = sConsulta & "    from (select * from #raiz" & vbCrLf
'sConsulta = sConsulta & "           Union" & vbCrLf
'sConsulta = sConsulta & "          select * from #mraiz) a" & vbCrLf
'sConsulta = sConsulta & " " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--La tabla #Estructura contendr� toda la estructura completa de actividades asignadas, pendientes y hermanas" & vbCrLf
'sConsulta = sConsulta & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4,act5)" & vbCrLf
'sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.act4,a.id" & vbCrLf
'sConsulta = sConsulta & "  from act5 a " & vbCrLf
'sConsulta = sConsulta & "     inner join #base b " & vbCrLf
'sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
'sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
'sConsulta = sConsulta & "            and a.act3 = b.act3 " & vbCrLf
'sConsulta = sConsulta & "            and a.act4 = b.act4" & vbCrLf
'sConsulta = sConsulta & " Where b.act5 Is Not Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4)" & vbCrLf
'sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.id" & vbCrLf
'sConsulta = sConsulta & "  from act4 a " & vbCrLf
'sConsulta = sConsulta & "     inner join #base b " & vbCrLf
'sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
'sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
'sConsulta = sConsulta & "            and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & " Where b.act5 Is Null " & vbCrLf
'sConsulta = sConsulta & "   And b.act4 Is Not Null" & vbCrLf
'sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
'sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
'sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
'sConsulta = sConsulta & "                      and a.act3=c.act3 " & vbCrLf
'sConsulta = sConsulta & "                      and a.id=c.act4)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3)" & vbCrLf
'sConsulta = sConsulta & "select a.act1,a.act2,a.id" & vbCrLf
'sConsulta = sConsulta & "  from act3 a " & vbCrLf
'sConsulta = sConsulta & "     inner join #base b " & vbCrLf
'sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
'sConsulta = sConsulta & "            and a.act2=b.act2" & vbCrLf
'sConsulta = sConsulta & " Where b.act4 Is Null " & vbCrLf
'sConsulta = sConsulta & "   And b.act3 Is Not Null" & vbCrLf
'sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
'sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
'sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
'sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
'sConsulta = sConsulta & "                      and a.id=c.act3)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1,Act2)" & vbCrLf
'sConsulta = sConsulta & "select a.act1,a.id" & vbCrLf
'sConsulta = sConsulta & "  from act2 a " & vbCrLf
'sConsulta = sConsulta & "     inner join #base b " & vbCrLf
'sConsulta = sConsulta & "             on a.act1=b.act1" & vbCrLf
'sConsulta = sConsulta & " Where b.act3 Is Null " & vbCrLf
'sConsulta = sConsulta & "   And b.act2 Is Not Null" & vbCrLf
'sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
'sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
'sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
'sConsulta = sConsulta & "                      and a.id = c.act2)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into #Estructura (Act1)" & vbCrLf
'sConsulta = sConsulta & "select a.id" & vbCrLf
'sConsulta = sConsulta & "  from act1 a" & vbCrLf
'sConsulta = sConsulta & " where not exists (SELECT * " & vbCrLf
'sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
'sConsulta = sConsulta & "                    where a.id=c.act1)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--Actualizamos los campos actAsign y actPend con el valor correspondiente. Estos campos" & vbCrLf
'sConsulta = sConsulta & "--contendr�n el nivel al que la actividad est� asignada (actAsign) o en el caso de que est� pendiente (actPend), el nivel" & vbCrLf
'sConsulta = sConsulta & "-- al que lo est�" & vbCrLf
'sConsulta = sConsulta & "update #Estructura set actAsign=0 ,actPend=0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actAsign = 1" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1" & vbCrLf
'sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actAsign = 2" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actAsign = 3" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actAsign = 4" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
'sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actAsign = 5" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actPend = 1" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1" & vbCrLf
'sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actPend = 2" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
'sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actPend = 3" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
'sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actPend = 4" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
'sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "update #Estructura" & vbCrLf
'sConsulta = sConsulta & "Set actPend = 5" & vbCrLf
'sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
'sConsulta = sConsulta & "select #Estructura.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
'sConsulta = sConsulta & "       #Estructura.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
'sConsulta = sConsulta & "       #Estructura.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
'sConsulta = sConsulta & "       #Estructura.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
'sConsulta = sConsulta & "       #Estructura.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
'sConsulta = sConsulta & "       actasign, actpend" & vbCrLf
'sConsulta = sConsulta & "from #Estructura  " & vbCrLf
'sConsulta = sConsulta & "     left join act1 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1 = act1.id" & vbCrLf
'sConsulta = sConsulta & "     left join act2 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1=act2.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act2 = act2.id" & vbCrLf
'sConsulta = sConsulta & "     left join act3 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1=act3.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act2 = act3.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act3 = act3.id"
'sConsulta = sConsulta & "     left join act4 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1=act4.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act2 = act4.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act3 = act4.act3 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act4 = act4.id" & vbCrLf
'sConsulta = sConsulta & "     left join act5 " & vbCrLf
'sConsulta = sConsulta & "            on #Estructura.act1=act5.act1 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act2 = act5.act2 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act3 = act5.act3 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act4 = act5.act4 " & vbCrLf
'sConsulta = sConsulta & "           and #Estructura.act5= act5.id" & vbCrLf
'sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
'sConsulta = sConsulta & "'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_COMPANIA @ID INT AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
'sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
'sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "End" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
'sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
'sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "--- 2.9.5" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "------------" & vbCrLf
'sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'
'End Sub
'
'
'
'
'Private Function CodigoDeActualizacion2_9_50_00A2_9_50_01() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificaci�n de triggers"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    V_2_9_5_01_Triggers
'
'    sConsulta = sConsulta & "if (select count(*) from pargen_gest)=0" & vbCrLf
'    sConsulta = sConsulta & "INSERT INTO PARGEN_GEST (ID, NIVELMINACT) VALUES (1,4)" & vbCrLf
'
'    V_2_9_5_01_Storeds
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.50.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_50_00A2_9_50_01 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_50_00A2_9_50_01 = False
'End Function
'
'
'Private Sub V_2_9_5_01_Triggers()
'Dim sConsulta As String
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REL_COMP_TG_INS_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop trigger [dbo].[REL_COMP_TG_INS_DEL]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE TRIGGER REL_COMP_TG_INS_DEL ON [dbo].[REL_COMP]" & vbCrLf
'sConsulta = sConsulta & " FOR INSERT, DELETE" & vbCrLf
'sConsulta = sConsulta & " AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "delete from mrel_comp " & vbCrLf
'sConsulta = sConsulta & "  from mrel_comp m" & vbCrLf
'sConsulta = sConsulta & "         inner join deleted d" & vbCrLf
'sConsulta = sConsulta & "                 on m.cia_prove = d.cia_prove" & vbCrLf
'sConsulta = sConsulta & "                and m.cia_comp = d.cia_comp" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "insert into mrel_comp (cia_prove, cia_comp)" & vbCrLf
'sConsulta = sConsulta & "select i.cia_prove, i.cia_comp " & vbCrLf
'sConsulta = sConsulta & "  from inserted i " & vbCrLf
'sConsulta = sConsulta & " where not exists (select * " & vbCrLf
'sConsulta = sConsulta & "                     from mrel_comp mc " & vbCrLf
'sConsulta = sConsulta & "                    where mc.cia_prove = i.cia_prove " & vbCrLf
'sConsulta = sConsulta & "                      and mc.cia_comp = i.cia_comp)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & ""
'
'End Sub
'
'
'Private Sub V_2_9_5_01_Storeds()
'Dim sConsulta As String
'
'sConsulta = ""
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_JOB_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_JOB_ACTUALIZAR_PROV_GS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_MVTOS_PROV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_MVTOS_PROV]" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_JOB_ACTUALIZAR_PROV_GS AS" & vbCrLf
'sConsulta = sConsulta & "BEGIN DISTRIBUTED TRAN" & vbCrLf
'sConsulta = sConsulta & "EXEC SP_PROV_REINTENTOS" & vbCrLf
'sConsulta = sConsulta & "IF @@ERROR = 0 " & vbCrLf
'sConsulta = sConsulta & "  BEGIN" & vbCrLf
'sConsulta = sConsulta & "    EXEC SP_ACTUALIZAR_PROV_GS" & vbCrLf
'sConsulta = sConsulta & "    IF @@ERROR = 0 " & vbCrLf
'sConsulta = sConsulta & "      EXEC SP_ELIMINAR_MVTOS_PROV" & vbCrLf
'sConsulta = sConsulta & "  END" & vbCrLf
'sConsulta = sConsulta & "IF @@ERROR = 0" & vbCrLf
'sConsulta = sConsulta & "  COMMIT TRAN" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
'sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
'sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
'sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
'sConsulta = sConsulta & "   END" & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
'sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
'sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
'sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
'sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
'sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
'sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "              IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "                IF @FCEST = 3" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "            BEGIN" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "             SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
'sConsulta = sConsulta & "             FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
'sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
'sConsulta = sConsulta & "                    " & vbCrLf
'sConsulta = sConsulta & "                                                   " & vbCrLf
'sConsulta = sConsulta & "           SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
'sConsulta = sConsulta & "                       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "          END                                          " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "                " & vbCrLf
'sConsulta = sConsulta & "      " & vbCrLf
'sConsulta = sConsulta & "             ELSE  " & vbCrLf
'sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "                            BEGIN" & vbCrLf
'sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTOS @COD_PROVE_CIA=@COD_PROVE_CIA'" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SET @PARAM= '@COD_PROVE_CIA VARCHAR(200)'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           DECLARE Cur_CONTAC CURSOR FOR   " & vbCrLf
'sConsulta = sConsulta & "               SELECT APE,NOM,DEP,CAR,TFNO,TFNO2,FAX,EMAIL,TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "                FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                   USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND " & vbCrLf
'sConsulta = sConsulta & "                   USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            OPEN Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_DEP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_FAX VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO2 VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                           @COD_PROVE_CIA VARCHAR(200)'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "           BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                   @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                   @COD_PROVE_CIA=@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           END" & vbCrLf
'sConsulta = sConsulta & "           CLOSE Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "           DEALLOCATE Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "             ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
'sConsulta = sConsulta & "           BEGIN   " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
'sConsulta = sConsulta & "                  BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                                      " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                                           " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                  END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
'sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
'sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
'sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "            " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
'sConsulta = sConsulta & "             ELSE" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "     " & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
'sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_MVTOS_PROV AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "/********************************************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "/******       Descripci�n : Borra los registros de MVTOS_PROV  y MVTOS_PROV_EST que han acabado  correctamente        ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                                                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                                                     ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                     ******/" & vbCrLf
'sConsulta = sConsulta & "/********************************************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Borramos los registros correspondientes de MVTOS_PROV" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DELETE MVTOS_PROV FROM MVTOS_PROV INNER JOIN MVTOS_PROV_EST ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO AND MVTOS_PROV_EST.EST=3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "-- Borramos tambien los registos correspondientes de MVTOS_PROV_EST" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DELETE FROM MVTOS_PROV_EST WHERE EST=3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'ExecuteSQL gRDOCon, sConsulta
'sConsulta = ""
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & ""
'
'End Sub
'Private Function CodigoDeActualizacion2_9_50_01A2_9_50_02() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificaci�n de triggers"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, Codigo_SP_ACT_PROV_GS
'    sConsulta = ""
'    sConsulta = sConsulta & vbCrLf & ""
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.50.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_50_01A2_9_50_02 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_50_01A2_9_50_02 = False
'End Function
'
'Private Function Codigo_SP_ACT_PROV_GS() As String
'Dim sConsulta As String
'
'sConsulta = ""
'sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
'sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
'sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ID_P INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
'sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
'sConsulta = sConsulta & "   END" & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
'sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
'sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
'sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
'sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
'sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
'sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "              IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "                IF @FCEST = 3" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "            BEGIN" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "             SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
'sConsulta = sConsulta & "             FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
'sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
'sConsulta = sConsulta & "                    " & vbCrLf
'sConsulta = sConsulta & "                                                   " & vbCrLf
'sConsulta = sConsulta & "           SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
'sConsulta = sConsulta & "                       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "          END                                          " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "                " & vbCrLf
'sConsulta = sConsulta & "      " & vbCrLf
'sConsulta = sConsulta & "             ELSE  " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "               BEGIN" & vbCrLf
'sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
'sConsulta = sConsulta & "           BEGIN" & vbCrLf
'sConsulta = sConsulta & "           SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "           SET @PARAM='@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                   ,@ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@ID_P" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "              END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       IF @TIPO='I' " & vbCrLf
'sConsulta = sConsulta & "           BEGIN" & vbCrLf
'sConsulta = sConsulta & "                    DECLARE Cur_CONTAC CURSOR FOR   " & vbCrLf
'sConsulta = sConsulta & "                             SELECT APE,NOM,DEP,CAR,TFNO,TFNO2,FAX,EMAIL,TFNO_MOVIL,ID" & vbCrLf
'sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND " & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "              -- SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               OPEN Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL,@ID_P" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,"
'sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "           @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                   @ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "  " & vbCrLf
'sConsulta = sConsulta & "                       WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "                     BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                        @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                            @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                         @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                         @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                        @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                   @ID_P=@ID_P" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL,@ID_P" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                       END" & vbCrLf
'sConsulta = sConsulta & "               CLOSE Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "               DEALLOCATE Cur_CONTAC" & vbCrLf
'sConsulta = sConsulta & "       END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       IF @TIPO='U' " & vbCrLf
'sConsulta = sConsulta & "       BEGIN" & vbCrLf
'sConsulta = sConsulta & "             SELECT APE,NOM,DEP,CAR,TFNO,TFNO2,FAX,EMAIL,TFNO_MOVIL,ID" & vbCrLf
'sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND " & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                       SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "               @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                      SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "           @ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                   @ID_P=@ID_P" & vbCrLf
'sConsulta = sConsulta & "                                          " & vbCrLf
'sConsulta = sConsulta & "               END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       END " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "             ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
'sConsulta = sConsulta & "           BEGIN   " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
'sConsulta = sConsulta & "                  BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                                      " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                                           " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                  END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
'sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
'sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
'sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "            " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
'sConsulta = sConsulta & "             ELSE" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "     " & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
'sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END"
'
'
'Codigo_SP_ACT_PROV_GS = sConsulta
'End Function
'
'Private Function CodigoDeActualizacion2_9_50_02A2_9_50_03() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificaci�n de triggers"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, Codigo_SP_ACT_PROV_GS_2_9_5_3
'
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_PROV_REINTENTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, Codigo_SP_REINT_GS_2_9_5_3
'
'    sConsulta = "ALTER TABLE dbo.PARGEN_COLAB" & vbCrLf
'    sConsulta = sConsulta & "DROP COLUMN IP_SRV"
'
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.50.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_50_02A2_9_50_03 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_50_02A2_9_50_03 = False
'End Function
'
'
'Private Function Codigo_SP_ACT_PROV_GS_2_9_5_3() As String
'Dim sConsulta As String
'
'
'sConsulta = "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
'sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
'sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
'sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "   BEGIN" & vbCrLf
'sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
'sConsulta = sConsulta & "   END" & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
'sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
'sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
'sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
'sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
'sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
'sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "            BEGIN" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
'sConsulta = sConsulta & "                BEGIN" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
'sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
'sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
'sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
'sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
'sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
'sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
'sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
'sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
'sConsulta = sConsulta & "                   FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
'sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
'sConsulta = sConsulta & "                    BEGIN" & vbCrLf
'sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
'sConsulta = sConsulta & "                    " & vbCrLf
'sConsulta = sConsulta & "                                                   " & vbCrLf
'sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
'sConsulta = sConsulta & "                       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
'sConsulta = sConsulta & "                 END       " & vbCrLf
'sConsulta = sConsulta & "              END                                          " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "                " & vbCrLf
'sConsulta = sConsulta & "      " & vbCrLf
'sConsulta = sConsulta & "      ELSE  " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "         BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
'sConsulta = sConsulta & "             BEGIN" & vbCrLf
'sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "             END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
'sConsulta = sConsulta & "             BEGIN" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
'sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 WHERE " & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
'sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                      @ID_P=@USU" & vbCrLf
'sConsulta = sConsulta & "                 END                " & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
'sConsulta = sConsulta & "           BEGIN" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
'sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 WHERE " & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
'sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
'sConsulta = sConsulta & "                 BEGIN" & vbCrLf
'sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                  @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                              @ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                      @ID_P=@USU" & vbCrLf
'sConsulta = sConsulta & "                   END                       " & vbCrLf
'sConsulta = sConsulta & "               END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
'sConsulta = sConsulta & "     BEGIN   " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   END        " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
'sConsulta = sConsulta & "                  BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                                      " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                                           " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                  END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
'sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
'sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
'sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   END     " & vbCrLf
'sConsulta = sConsulta & "                END" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "            " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
'sConsulta = sConsulta & "             ELSE" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "     " & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
'sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END"
'
'Codigo_SP_ACT_PROV_GS_2_9_5_3 = sConsulta
'
'End Function
'
'Private Function Codigo_SP_REINT_GS_2_9_5_3() As String
'Dim sConsulta As String
'
'sConsulta = "CREATE PROCEDURE SP_PROV_REINTENTOS AS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "/******       Descripci�n : Intenta pasar los movimientos fallidos si toca       ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                                                                                         ******/" & vbCrLf
'sConsulta = sConsulta & "/******       Par�metros :  ------------------                                                           ******/" & vbCrLf
'sConsulta = sConsulta & "/******                                        ******/" & vbCrLf
'sConsulta = sConsulta & "/*******************************************************************************************/" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR,@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(1000),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(3),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
'sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@ESPEC INT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @CICLOS= (SELECT PROV_CICLOS FROM PARGEN_GEST) " & vbCrLf
'sConsulta = sConsulta & "SET @REINTENTOS = (SELECT PROV_REINTENTOS FROM PARGEN_GEST)" & vbCrLf
'sConsulta = sConsulta & "SET @PASADAS = (SELECT PROV_PASADAS FROM PARGEN_GEST)" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "IF @CICLOS <> 0 AND @PASADAS%@CICLOS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "      BEGIN" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
'sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
'sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
'sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
'sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
'sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 1 AND MVTOS_PROV_EST.INTENTOS < @REINTENTOS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       BEGIN         " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
'sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
'sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
'sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "              IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "          BEGIN" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "                IF @FCEST = 3" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "            BEGIN" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
'sConsulta = sConsulta & "              SET @CIAS_DEN=NULL" & vbCrLf
'sConsulta = sConsulta & "              SET @CIAS_NIF=NULL" & vbCrLf
'sConsulta = sConsulta & "              SET @CIAS_PAI=NULL" & vbCrLf
'sConsulta = sConsulta & "              SET @CIAS_PROVI=NULL" & vbCrLf
'sConsulta = sConsulta & "              SET @CIAS_CP=NULL" & vbCrLf
'sConsulta = sConsulta & "              SET  @CIAS_POB=NULL" & vbCrLf
'sConsulta = sConsulta & "              SET @CIAS_DIR=NULL" & vbCrLf
'sConsulta = sConsulta & "              SET @CIAS_MON=NULL" & vbCrLf
'sConsulta = sConsulta & "              SET @CIAS_URLCIA=NULL" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "             SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
'sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
'sConsulta = sConsulta & "             FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
'sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
'sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
'sConsulta = sConsulta & "             IF @CIAS_DEN IS NOT NULL " & vbCrLf
'sConsulta = sConsulta & "             BEGIN" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "             SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
'sConsulta = sConsulta & "                    " & vbCrLf
'sConsulta = sConsulta & "                                                   " & vbCrLf
'sConsulta = sConsulta & "            SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
'sConsulta = sConsulta & "                       " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "            EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
'sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
'sConsulta = sConsulta & "          END " & vbCrLf
'sConsulta = sConsulta & "          END                                          " & vbCrLf
'sConsulta = sConsulta & "       END" & vbCrLf
'sConsulta = sConsulta & "                " & vbCrLf
'sConsulta = sConsulta & "      " & vbCrLf
'sConsulta = sConsulta & "      ELSE  " & vbCrLf
'sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
'sConsulta = sConsulta & "        BEGIN" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
'sConsulta = sConsulta & "             BEGIN" & vbCrLf
'sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "             END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
'sConsulta = sConsulta & "             BEGIN" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_APE=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_NOM=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_DEP=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_CAR=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_TFNO=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_FAX=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_EMAIL=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_TFNO2=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_TFNO_MOVIL=NULL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
'sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 WHERE " & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
'sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                      SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                       SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                         EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                      @ID_P=@USU" & vbCrLf
'sConsulta = sConsulta & "                END               " & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
'sConsulta = sConsulta & "           BEGIN" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_APE=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_NOM=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_DEP=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_CAR=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_TFNO=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_FAX=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_EMAIL=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_TFNO2=NULL" & vbCrLf
'sConsulta = sConsulta & "                  SET @USU_TFNO_MOVIL=NULL" & vbCrLf
'sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
'sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL" & vbCrLf
'sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 WHERE " & vbCrLf
'sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "       IF @USU_APE IS NOT NULL" & vbCrLf
'sConsulta = sConsulta & "                 BEGIN" & vbCrLf
'sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                  @ID_P=@ID_P'    " & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                      SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
'sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                              @ID_P INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
'sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
'sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                      @ID_P=@USU" & vbCrLf
'sConsulta = sConsulta & "                 END                                          " & vbCrLf
'sConsulta = sConsulta & "               END " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "       " & vbCrLf
'sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
'sConsulta = sConsulta & "     BEGIN   " & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'         " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   END        " & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "           " & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
'sConsulta = sConsulta & "                  BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                                      " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                                           " & vbCrLf
'sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                  END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
'sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
'sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   IF  @NOM IS NOT NULL                   " & vbCrLf
'sConsulta = sConsulta & "                   BEGIN" & vbCrLf
'sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
'sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
'sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
'sConsulta = sConsulta & "                                       " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
'sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
'sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
'sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
'sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   " & vbCrLf
'sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
'sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
'sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
'sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
'sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
'sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "                   END" & vbCrLf
'sConsulta = sConsulta & "               " & vbCrLf
'sConsulta = sConsulta & "           END " & vbCrLf
'sConsulta = sConsulta & "            " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "            IF @@ERROR <> 0                BEGIN " & vbCrLf
'sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1" & vbCrLf
'sConsulta = sConsulta & "             IF @INTENTO = @REINTENTOS" & vbCrLf
'sConsulta = sConsulta & "               SET @EST_FILA = 2" & vbCrLf
'sConsulta = sConsulta & "             ELSE" & vbCrLf
'sConsulta = sConsulta & "               SET @EST_FILA = 1" & vbCrLf
'sConsulta = sConsulta & "                END" & vbCrLf
'sConsulta = sConsulta & "             ELSE" & vbCrLf
'sConsulta = sConsulta & "                BEGIN" & vbCrLf
'sConsulta = sConsulta & "             SET @EST_FILA = 3                 END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "        " & vbCrLf
'sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   " & vbCrLf
'sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "     " & vbCrLf
'sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "END" & vbCrLf
'sConsulta = sConsulta & "" & vbCrLf
'sConsulta = sConsulta & "SET @PASADAS = @PASADAS + 1" & vbCrLf
'sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS = @PASADAS"
'
'Codigo_SP_REINT_GS_2_9_5_3 = sConsulta
'
'End Function
'
'
'
'
'Private Function CodigoDeActualizacion2_9_50_03A2_9_50_04() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificaciones de las monedas"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = ""
'    sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
'    sConsulta = sConsulta & "   PAI int NOT NULL CONSTRAINT DF_PARGEN_INTERNO_PAI DEFAULT 198" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "ALTER TABLE dbo.MON ADD" & vbCrLf
'    sConsulta = sConsulta & "   EQ_ACT tinyint NOT NULL CONSTRAINT DF_MON_EQ_ACT DEFAULT 1" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.50.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_50_03A2_9_50_04 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_50_03A2_9_50_04 = False
'End Function
'
'
'Private Function CodigoDeActualizacion2_9_50_04A2_9_50_05() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificaciones para el registro de proveedores"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = ""
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_OFERTA_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_OFERTA_WEB]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "CREATE PROCEDURE SP_ANYA_OFERTA_WEB @CIA_COMP int,  @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @CODMON VARCHAR(50),@FECVAL VARCHAR(10),@OBS text ,@NUMOFE INT OUTPUT , @NUMOFEWEB  INT OUTPUT  " & vbCrLf
'    sConsulta = sConsulta & "AS " & vbCrLf
'    sConsulta = sConsulta & " " & vbCrLf
'    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
'    sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
'    sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
'    sConsulta = sConsulta & "DECLARE @TODOS int" & vbCrLf
'    sConsulta = sConsulta & "DECLARE @NUEVOS int" & vbCrLf
'    sConsulta = sConsulta & "DECLARE @AREVISAR int " & vbCrLf
'    sConsulta = sConsulta & " " & vbCrLf
'    sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
'    sConsulta = sConsulta & "  " & vbCrLf
'    sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_ANYA_OFERTA_WEB @ANYO=@ANYO, @GMN1 =@GMN1, @PROCE =@PROCE, @PROVE=@PROVE,@CODMON=@CODMON, @FECVAL=@FECVAL,@OBS=@OBS,@NUMOFE=@NUMOFE OUTPUT ,@NUMOFEWEB=@NUMOFEWEB OUTPUT' " & vbCrLf
'    sConsulta = sConsulta & "  " & vbCrLf
'    sConsulta = sConsulta & "/* Build the SQL string once.*/ " & vbCrLf
'    sConsulta = sConsulta & " " & vbCrLf
'    sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @CODMON VARCHAR(50),@FECVAL VARCHAR(10),@OBS TEXT,@NUMOFE INT OUTPUT, @NUMOFEWEB  INT OUTPUT' " & vbCrLf
'    sConsulta = sConsulta & " " & vbCrLf
'    sConsulta = sConsulta & "/* Execute the string with the first parameter value. */ " & vbCrLf
'    sConsulta = sConsulta & "  " & vbCrLf
'    sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, " & vbCrLf
'    sConsulta = sConsulta & "                      @ANYO = @ANYO,  " & vbCrLf
'    sConsulta = sConsulta & "         @GMN1 =@GMN1,  " & vbCrLf
'    sConsulta = sConsulta & "                     @PROCE =@PROCE,  " & vbCrLf
'    sConsulta = sConsulta & "                     @PROVE=@PROVE, " & vbCrLf
'    sConsulta = sConsulta & "                     @CODMON=@CODMON, " & vbCrLf
'    sConsulta = sConsulta & "                     @FECVAL=@FECVAL," & vbCrLf
'    sConsulta = sConsulta & "           @OBS=@OBS,   " & vbCrLf
'    sConsulta = sConsulta & "                     @NUMOFE=@NUMOFE OUTPUT, " & vbCrLf
'    sConsulta = sConsulta & "         @NUMOFEWEB=@NUMOFEWEB OUTPUT" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_REGISTRAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_REGISTRAR_COMPANIA]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "CREATE PROCEDURE SP_REGISTRAR_COMPANIA  @IDPORTAL int, @COD varchar(50) , @DEN varchar(100), " & vbCrLf
'    sConsulta = sConsulta & "                 @DIR varchar(255), @CP varchar(20), @POB varchar(100), @PAI INT, " & vbCrLf
'    sConsulta = sConsulta & "                 @PROVI INT, @MON INT, @FCEST tinyint, @FPEST tinyint, @NIF varchar(20)," & vbCrLf
'    sConsulta = sConsulta & "                 @VOLFACT float, @CLIREF1 varchar(50),@CLIREF2 varchar(50),@CLIREF3 varchar(50),@CLIREF4 varchar(50)," & vbCrLf
'    sConsulta = sConsulta & "                 @HOM1 varchar(50), @HOM2 varchar(50), @HOM3 varchar(50), @URLCIA varchar(255)," & vbCrLf
'    sConsulta = sConsulta & "                 @COMENT text, @ORIGINS varchar(50), @PREMIUM int, @TIPOACC tinyint ,@IDI varchar(50), @IDCIA int OUTPUT AS" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "DECLARE @IDIDI int" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "SELECT @IDCIA=ISNULL(MAX(ID),0) + 1 " & vbCrLf
'    sConsulta = sConsulta & "  FROM CIAS" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
'    sConsulta = sConsulta & "  FROM IDI" & vbCrLf
'    sConsulta = sConsulta & " WHERE COD = @IDI" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "INSERT INTO CIAS (ID,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,IDI,PORT,FCEST,NIF,PREMIUM,VOLFACT, " & vbCrLf
'    sConsulta = sConsulta & "                  CLIREF1,CLIREF2,CLIREF3,CLIREF4,HOM1,HOM2,HOM3,URLCIA,COMENT,ORIGINS) " & vbCrLf
'    sConsulta = sConsulta & "VALUES (@IDCIA,@COD,@DEN,@DIR,@CP,@POB,@PAI,@PROVI,@MON,@IDIDI,@IDPORTAL,@FCEST,@NIF,@PREMIUM,@VOLFACT ," & vbCrLf
'    sConsulta = sConsulta & "                  @CLIREF1, @CLIREF2, @CLIREF3, @CLIREF4, @HOM1, @HOM2, @HOM3, @URLCIA, @COMENT, @ORIGINS)" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "INSERT INTO CIAS_PORT (CIA,PORT,FPEST,TIPOACC,ORIGINS) " & vbCrLf
'    sConsulta = sConsulta & "VALUES (@IDCIA,@IDPORTAL,@FPEST,@TIPOACC, @ORIGINS)" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "INSERT INTO rel_comp (cia_prove, cia_comp) " & vbCrLf
'    sConsulta = sConsulta & "SELECT @IDCIA, ID" & vbCrLf
'    sConsulta = sConsulta & "  FROM CIAS" & vbCrLf
'    sConsulta = sConsulta & " WHERE FCEST=3" & vbCrLf
'    sConsulta = sConsulta & "   AND PORT = 0" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.50.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_50_04A2_9_50_05 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_50_04A2_9_50_05 = False
'End Function
'
'
'Private Function CodigoDeActualizacion2_9_50_05A2_9_50_06() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificaciones para el cambio de contrase�a del Administrador del PS"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = ""
'    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_LOGIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[USU_LOGIN]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "CREATE PROCEDURE USU_LOGIN(@USUOLD VARCHAR(50),@USUNEW VARCHAR(50),@PWDNEW VARCHAR(512),@FECPWD varchar(50)) AS" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
'    sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
'    sConsulta = sConsulta & "BEGIN" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "   UPDATE ADM SET  USU=@USUNEW,PWD=@PWDNEW,FECPWD=convert(datetime,@FECPWD,103) WHERE USU=@USUOLD" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "       " & vbCrLf
'    sConsulta = sConsulta & "   IF @@ERROR=0" & vbCrLf
'    sConsulta = sConsulta & "     begin " & vbCrLf
'    sConsulta = sConsulta & "       print @@error" & vbCrLf
'    sConsulta = sConsulta & "       COMMIT TRANSACTION" & vbCrLf
'    sConsulta = sConsulta & "     end" & vbCrLf
'    sConsulta = sConsulta & "   ELSE" & vbCrLf
'    sConsulta = sConsulta & "       SET XACT_ABORT OFF" & vbCrLf
'    sConsulta = sConsulta & "END" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.50.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_50_05A2_9_50_06 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_50_05A2_9_50_06 = False
'End Function
'
'
'
'Private Function CodigoDeActualizacion2_9_50_06A2_9_50_07() As Boolean
'Dim bTransaccionEnCurso As Boolean
'Dim sConsulta As String
'Dim sfecha As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
'    bTransaccionEnCurso = True
'
'
'
'    frmProgreso.lblDetalle = "Modificaciones para la transferencia de archivos desde el portal"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
'        frmProgreso.ProgressBar1.Value = 1
'    End If
'
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROVE_ESP]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIA_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIA_ESP]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRANSFER_CIA_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_TRANSFER_CIA_ESP]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROVE_ESP @IDPROVE int output, @IDESP int = NULL, @CODPROVE varchar(50) = null AS" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "if @IDPROVE is null" & vbCrLf
'    sConsulta = sConsulta & "   select @IDPROVE = ID FROM CIAS WHERE COD = @CODPROVE" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "IF @IDESP = NULL" & vbCrLf
'    sConsulta = sConsulta & "SELECT id, nom,com, fecact , datalength(data) TAMANYO" & vbCrLf
'    sConsulta = sConsulta & "  from cias_esp  " & vbCrLf
'    sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
'    sConsulta = sConsulta & "ELSE" & vbCrLf
'    sConsulta = sConsulta & "SELECT id, nom,com, fecact, datalength(data)  TAMANYO" & vbCrLf
'    sConsulta = sConsulta & "  from cias_esp  " & vbCrLf
'    sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
'    sConsulta = sConsulta & "   AND ID = @IDESP" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "CREATE PROCEDURE SP_ANYA_CIA_ESP @IDCIA int, @NOM varchar(300), @COM varchar(500) , @IDESP int output AS" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "SET @IDESP = 1" & vbCrLf
'    sConsulta = sConsulta & "select @IDESP = ISNULL(ID,0) + 1 " & vbCrLf
'    sConsulta = sConsulta & "  FROM CIAS_ESP " & vbCrLf
'    sConsulta = sConsulta & " WHERE CIA = @IDCIA" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA, ID, NOM, COM) " & vbCrLf
'    sConsulta = sConsulta & "VALUES (@IDCIA, @IDESP, @NOM, @COM)" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "CREATE PROCEDURE SP_TRANSFER_CIA_ESP @IDCIA INTEGER, @ESP SMALLINT, @INIT INT, @SIZE INT, @DATA IMAGE AS" & vbCrLf
'    sConsulta = sConsulta & " " & vbCrLf
'    sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
'    sConsulta = sConsulta & " " & vbCrLf
'    sConsulta = sConsulta & "SET nocount on " & vbCrLf
'    sConsulta = sConsulta & " " & vbCrLf
'    sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM CIAS_ESP WHERE CIA = @IDCIA AND ID=@ESP" & vbCrLf
'    sConsulta = sConsulta & " " & vbCrLf
'    sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
'    sConsulta = sConsulta & "    UPDATE CIAS_ESP SET DATA = @DATA WHERE CIA = @IDCIA AND ID=@ESP" & vbCrLf
'    sConsulta = sConsulta & "ELSE " & vbCrLf
'    sConsulta = sConsulta & "    UPDATETEXT CIAS_ESP.DATA @textpointer @init 0 @data" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'    sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = ""
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_COMPANIA @ID INT AS" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
'    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
'    sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
'    sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
'    sConsulta = sConsulta & "BEGIN" & vbCrLf
'    sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
'    sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
'    sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
'    sConsulta = sConsulta & "        " & vbCrLf
'    sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
'    sConsulta = sConsulta & "End" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
'    sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "--- 2.9.5" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "------------" & vbCrLf
'    sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
'    sConsulta = sConsulta & "" & vbCrLf
'    sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.9.50.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_9_50_06A2_9_50_07 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_9_50_06A2_9_50_07 = False
'End Function
'
'
'
'
'Public Function CodigoDeActualizacion2_0_0A2_0_1() As Boolean
'Dim rdores As rdoResultset
'Dim bTransaccionEnCurso As Boolean
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    'Inserta la tabla version
'    sConsulta = "CREATE TABLE [dbo].[VERSION] ("
'    sConsulta = sConsulta & vbLf & "[NUM] [varchar] (50) NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[FECHA] [datetime] NULL"
'    sConsulta = sConsulta & vbLf & ") ON [PRIMARY]"
'    ExecuteSQL gRDOCon, sConsulta
'
'      'Inserta la tabla COM
'    sConsulta = "CREATE TABLE [dbo].[COM] ("
'    sConsulta = sConsulta & vbLf & "[ID] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[COD] [varchar] (50) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "[DEN] [varchar] (200) NOT NULL,"
'    sConsulta = sConsulta & vbLf & "[FECACT] [datetime] NULL"
'    sConsulta = sConsulta & vbLf & ") ON [PRIMARY]"
'
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[COM] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_COM] PRIMARY KEY CLUSTERED"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[ID]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = CrearTriggerCOM_TG_InsUpd_2_0_1
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Inserta la tabla CIAS_COM
'    sConsulta = "CREATE TABLE [dbo].[CIAS_COM] ("
'    sConsulta = sConsulta & vbLf & "[CIA] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ID] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[FECHA] [datetime] NOT NULL,"
'    sConsulta = sConsulta & vbLf & "[TIPOCOM] [int] NOT NULL,"
'    sConsulta = sConsulta & vbLf & "[PORT] [int] NOT NULL,"
'    sConsulta = sConsulta & vbLf & "[COMENT] [varchar] (710) NULL,"
'    sConsulta = sConsulta & vbLf & "[FECACT] [datetime] NULL"
'    sConsulta = sConsulta & vbLf & ") ON [PRIMARY]"
'
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[CIAS_COM] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_CIAS_COM] PRIMARY KEY CLUSTERED"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA],"
'    sConsulta = sConsulta & vbLf & "[ID]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[CIAS_COM] ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [CIAS_COM_FK_CIAS] FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA]"
'    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[CIAS] ("
'    sConsulta = sConsulta & vbLf & "[ID]"
'    sConsulta = sConsulta & vbLf & "),"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_CIAS_COM_COM] FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[TIPOCOM]"
'    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[COM] ("
'    sConsulta = sConsulta & vbLf & "[ID]"
'    sConsulta = sConsulta & vbLf & ")"
'
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = CrearTriggerCIAS_COM_TG_InsUpd_2_0_1
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "INSERT INTO [dbo].[VERSION] VALUES ('2.0.1','" & Format(Date, "mm-dd-yyyy") & "')"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_0A2_0_1 = True
'    Exit Function
'
'error:
'
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_0A2_0_1 = False
'
'End Function
'
'Private Function CrearTablaPARGEN_INTERNO() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[PARGEN_INTERNO] ("
'    sConsulta = sConsulta & vbLf & "[ID] [tinyint] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[MAX_ADJUN] float (53) NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[PREMIUM] [Bit] NOT NULL"
'    sConsulta = sConsulta & vbLf & ") ON [PRIMARY]"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[PARGEN_INTERNO] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [DF_PARGEN_INTERNO_MAX_ADJUN] DEFAULT (0) FOR [MAX_ADJUN],"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [DF_PARGEN_INTERNO_PREMIUM] DEFAULT (1) FOR [PREMIUM],"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_PARGEN_INTERNO] PRIMARY KEY  NONCLUSTERED"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[ID]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'
'    CrearTablaPARGEN_INTERNO = sConsulta
'End Function
'
'Private Function CrearTriggerCOM_TG_InsUpd_2_0_1() As String
'
'sConsulta = "CREATE TRIGGER COM_TG_INSUPD ON dbo.COM"
'sConsulta = sConsulta & vbLf & "FOR INSERT,UPDATE"
'sConsulta = sConsulta & vbLf & "AS"
'sConsulta = sConsulta & vbLf & "DECLARE @ID INT"
'sConsulta = sConsulta & vbLf & "DECLARE curTG_COM_Ins CURSOR FOR SELECT ID FROM INSERTED"
'sConsulta = sConsulta & vbLf & "OPEN curTG_COM_Ins"
'sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_COM_Ins INTO @ID"
'sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'sConsulta = sConsulta & vbLf & "BEGIN"
'sConsulta = sConsulta & vbLf & "UPDATE COM SET FECACT=GETDATE() WHERE ID = @ID"
'sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_COM_Ins INTO @ID"
'sConsulta = sConsulta & vbLf & "END"
'sConsulta = sConsulta & vbLf & "CLOSE curTG_COM_Ins"
'sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_COM_Ins"
'
'CrearTriggerCOM_TG_InsUpd_2_0_1 = sConsulta
'
'End Function
'
'Private Function CrearTriggerCIAS_COM_TG_InsUpd_2_0_1() As String
'
'sConsulta = "CREATE TRIGGER CIAS_COM_TG_INSUPD ON dbo.CIAS_COM"
'sConsulta = sConsulta & vbLf & "FOR INSERT,UPDATE"
'sConsulta = sConsulta & vbLf & "AS"
'sConsulta = sConsulta & vbLf & "DECLARE @CIA INT,@ID INT"
'sConsulta = sConsulta & vbLf & "DECLARE curTG_CIAS_COM_Ins CURSOR FOR SELECT CIA,ID FROM INSERTED"
'sConsulta = sConsulta & vbLf & "OPEN curTG_CIAS_COM_Ins"
'sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_CIAS_COM_Ins INTO @CIA,@ID"
'sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'sConsulta = sConsulta & vbLf & "BEGIN"
'sConsulta = sConsulta & vbLf & "UPDATE CIAS_COM SET FECACT=GETDATE() WHERE CIA = @CIA AND ID = @ID"
'sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_CIAS_COM_Ins INTO @CIA,@ID"
'sConsulta = sConsulta & vbLf & "END"
'sConsulta = sConsulta & vbLf & "CLOSE curTG_CIAS_COM_Ins"
'sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_CIAS_COM_Ins"
'
'CrearTriggerCIAS_COM_TG_InsUpd_2_0_1 = sConsulta
'
'End Function
'
'
'Public Function CodigoDeActualizacion2_0_1A2_0_2() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    ''' Nuevo store procedure SP_SELECT_ID
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_SELECT_ID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_SELECT_ID]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_SELECT_ID
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.0.2'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_1A2_0_2 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_1A2_0_2 = False
'
'End Function
'
'Private Function CrearSP_SELECT_ID() As String
'    Dim sConsulta As String
'
'    'Crea el proc.almacenado SP_SELECT_ID
'
'    sConsulta = "CREATE PROCEDURE SP_SELECT_ID(@FSP_CIA VARCHAR(20)) AS"
'    sConsulta = sConsulta & vbLf & "DECLARE   @ID SMALLINT "
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SET @ID = (SELECT ID FROM CIAS WHERE COD=@FSP_CIA)"
'    sConsulta = sConsulta & vbLf & "Return @ID"
'    sConsulta = sConsulta & vbLf & "END"
'
'    CrearSP_SELECT_ID = sConsulta
'
'End Function
'
'Public Function CodigoDeActualizacion2_0_2A2_0_3() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    '''A�adir en la tabla CIAS los campos FECACTFC y FECDESACTFC
'    sConsulta = "ALTER TABLE [dbo].[CIAS] ADD FECACTFC DATETIME NULL"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = "ALTER TABLE [dbo].[CIAS] ADD FECDESACTFC DATETIME NULL"
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''A�adir en la tabla USU los campos FECACTFC y FECDESACTFC
'    sConsulta = "ALTER TABLE [dbo].[USU] ADD FECACTFC DATETIME NULL"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = "ALTER TABLE [dbo].[USU] ADD FECDESACTFC DATETIME NULL"
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''A�adir en la tabla CIAS_PORT los campos FECACTFP y FECDESACTFP
'    sConsulta = "ALTER TABLE [dbo].[CIAS_PORT] ADD FECACTFP DATETIME NULL"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = "ALTER TABLE [dbo].[CIAS_PORT] ADD FECDESACTFP DATETIME NULL"
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''A�adir en la tabla USU_PORT los campos FECACTFP y FECDESACTFP
'    sConsulta = "ALTER TABLE [dbo].[USU_PORT] ADD FECACTFP DATETIME NULL"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = "ALTER TABLE [dbo].[USU_PORT] ADD FECDESACTFP DATETIME NULL"
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Cambios en los triggers de update de CIAS,CIAS_PORT,USU y USU_PORT
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[CIAS_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[CIAS_TG_UPD]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearTriggerITEM_CIAS_TG_UPD
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[CIAS_PORT_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[CIAS_PORT_TG_UPD]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearTriggerITEM_CIAS_PORT_TG_UPD
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[USU_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[USU_TG_UPD]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearTriggerITEM_USU_TG_UPD
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[USU_PORT_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[USU_PORT_TG_UPD]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearTriggerITEM_USU_PORT_TG_UPD
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.0.3'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_2A2_0_3 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_2A2_0_3 = False
'
'End Function
'
'
'Public Function CodigoDeActualizacion2_0_3A2_0_4() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    '''Nueva tabla pargen interno
'    sConsulta = CrearTablaPARGEN_INTERNO
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nuevo campo PREMIUM en CIAS del Portal
'    sConsulta = "ALTER TABLE [dbo].[CIAS] ADD PREMIUM [int] NOT NULL DEFAULT (0)"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    '''Nueva tabla PARGEN_DEF
'    sConsulta = CrearTablaPARGEN_DEF
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nueva tabla CIAS_MON
'    sConsulta = CrearTablaCIAS_MON
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nueva tabla VOLADJ
'    sConsulta = CrearTablaVOLADJ
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nuevo trigger en la tabla VOLADJ
'    sConsulta = CrearTriggerVOLADJ_InsUpd
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nuevo store procedure SP_ADD_VOLADJ
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ADD_VOLADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ADD_VOLADJ]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_ADD_VOLADJ
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nuevo store procedure SP_DEL_VOLADJ
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEL_VOLADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEL_VOLADJ]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_DEL_VOLADJ
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nuevo campo USUPPAL en CIAS
'    sConsulta = "ALTER TABLE [dbo].[CIAS] ADD USUPPAL [int] NULL"
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''ID y USUPPAL de CIAS ser�n clave foranea de USU
'    sConsulta = "ALTER TABLE [dbo].[CIAS] ADD "
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_CIAS_USU] FOREIGN KEY "
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[ID],"
'    sConsulta = sConsulta & vbLf & "[USUPPAL]"
'    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[USU] ("
'    sConsulta = sConsulta & vbLf & "[CIA],"
'    sConsulta = sConsulta & vbLf & "[ID]"
'    sConsulta = sConsulta & vbLf & ")"
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Crear el procedimiento almacenado SP_ACT_VISIBLE
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ACT_VISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ACT_VISIBLE]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_ACT_VISIBLE
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.0.4'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_3A2_0_4 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_3A2_0_4 = False
'
'End Function
'
'
'Private Function CrearTriggerVOLADJ_InsUpd() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE TRIGGER [VOLADJ_TG_INS] ON dbo.VOLADJ"
'    sConsulta = sConsulta & vbLf & "FOR INSERT,UPDATE"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @PROVE INT"
'    sConsulta = sConsulta & vbLf & "DECLARE @COMP INT"
'    sConsulta = sConsulta & vbLf & "DECLARE @PROCE INT"
'    sConsulta = sConsulta & vbLf & "DECLARE @ANYO SMALLINT"
'    sConsulta = sConsulta & vbLf & "DECLARE @GMN1 VARCHAR(10)"
'    sConsulta = sConsulta & vbLf & "DECLARE curTG_VOLADJ_Ins CURSOR FOR SELECT CIA_PROVE,CIA_COMP,ANYO,GMN1,PROCE FROM INSERTED"
'    sConsulta = sConsulta & vbLf & "OPEN curTG_VOLADJ_Ins"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_VOLADJ_Ins INTO @PROVE,@COMP,@ANYO,@GMN1,@PROCE"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "    BEGIN"
'    sConsulta = sConsulta & vbLf & "          UPDATE VOLADJ SET FECACT=GETDATE() WHERE CIA_PROVE=@PROVE AND CIA_COMP=@COMP AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
'    sConsulta = sConsulta & vbLf & "          FETCH NEXT FROM curTG_VOLADJ_Ins INTO @PROVE,@COMP,@ANYO,@GMN1,@PROCE"
'    sConsulta = sConsulta & vbLf & "   End"
'    sConsulta = sConsulta & vbLf & "Close curTG_VOLADJ_Ins"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_VOLADJ_Ins"
'
'    CrearTriggerVOLADJ_InsUpd = sConsulta
'
'End Function
'
'Private Function CrearSP_DEL_VOLADJ() As String
'    Dim sConsulta As String
'
'    'Crea el proc.almacenado SP_DEL_VOLADJ
'
'    sConsulta = "CREATE PROCEDURE SP_DEL_VOLADJ(@COMP INT,@PROVE VARCHAR(50), @ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT) AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @ID AS INT "
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SET @ID = (SELECT ID FROM CIAS WHERE COD=@PROVE)"
'    sConsulta = sConsulta & vbLf & "DELETE FROM VOLADJ WHERE CIA_COMP=@COMP AND CIA_PROVE=@ID AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
'    sConsulta = sConsulta & vbLf & "Return"
'    sConsulta = sConsulta & vbLf & "END"
'
'    CrearSP_DEL_VOLADJ = sConsulta
'
'End Function
'
'Private Function CrearSP_ADD_VOLADJ() As String
'    Dim sConsulta As String
'
'    'Crea el proc.almacenado SP_ADD_VOLADJ
'
'    sConsulta = "CREATE PROCEDURE SP_ADD_VOLADJ(@COMP INT,@PROVE VARCHAR(50),@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT,@VOL FLOAT,@MONGS VARCHAR(50),@MONPOR VARCHAR(50),@FECADJ DATETIME,@DENPROCE VARCHAR(100) ) AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @ID AS INT "
'    sConsulta = sConsulta & vbLf & "DECLARE @MONPORTAL AS VARCHAR(3)"
'    sConsulta = sConsulta & vbLf & "DECLARE @VOLPORTAL AS FLOAT"
'    sConsulta = sConsulta & vbLf & "DECLARE @EQUIV AS FLOAT"
'    sConsulta = sConsulta & vbLf & "DECLARE @EQUIVGS AS FLOAT"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SET @ID = (SELECT ID FROM CIAS WHERE COD=@PROVE)"
'    sConsulta = sConsulta & vbLf & "SET @MONPORTAL =(SELECT COD FROM MON WHERE COD=@MONPOR)"
'    sConsulta = sConsulta & vbLf & "IF NOT @MONPORTAL IS NULL "
'    sConsulta = sConsulta & vbLf & "  BEGIN"
'    sConsulta = sConsulta & vbLf & "    SET @EQUIV = (SELECT EQUIV FROM MON WHERE COD=@MONPOR)"
'    sConsulta = sConsulta & vbLf & "    IF NOT @EQUIV IS NULL"
'    sConsulta = sConsulta & vbLf & "          BEGIN"
'    sConsulta = sConsulta & vbLf & "        SET @VOLPORTAL=(@VOL) / @EQUIV  "
'    sConsulta = sConsulta & vbLf & "                 INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,@MONPORTAL,@VOLPORTAL,@FECADJ,@DENPROCE)"
'    sConsulta = sConsulta & vbLf & "         END"
'    sConsulta = sConsulta & vbLf & "    ELSE"
'    sConsulta = sConsulta & vbLf & "        INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,@MONPORTAL,NULL,@FECADJ,@DENPROCE) "
'    sConsulta = sConsulta & vbLf & "    END"
'    sConsulta = sConsulta & vbLf & "ELSE"
'    sConsulta = sConsulta & vbLf & "    INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,NULL,NULL,@FECADJ,@DENPROCE)"
'    sConsulta = sConsulta & vbLf & "END"
'
'    CrearSP_ADD_VOLADJ = sConsulta
'
'End Function
'
'Private Function CrearSP_ACT_VISIBLE() As String
'    Dim sConsulta As String
'
'    'Crea el proc.almacenado SP_ACT_VISIBLE
'
'    sConsulta = "CREATE PROCEDURE SP_ACT_VISIBLE @CIA int,@IDI varchar(20)"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "select cias_act5.act1 act1,cias_act5.act2 act2, cias_act5.act3 act3, cias_act5.act4 act4, cias_act5.act5 act5 into #act5"
'    sConsulta = sConsulta & vbLf & "from cias_act5 left join cias_act4 on cias_act5.act1 = cias_act4.act1"
'    sConsulta = sConsulta & vbLf & "and cias_act5.act2 = cias_act4.act2"
'    sConsulta = sConsulta & vbLf & "and cias_act5.act3 = cias_act4.act3"
'    sConsulta = sConsulta & vbLf & "and cias_act5.act4 = cias_act4.act4"
'    sConsulta = sConsulta & vbLf & "and cias_act5.cia  = cias_act4.cia"
'    sConsulta = sConsulta & vbLf & "Where cias_act4.cia Is Null"
'    sConsulta = sConsulta & vbLf & "and cias_act5.cia = @CIA"
'    sConsulta = sConsulta & vbLf & "select cias_act4.act1 act1,cias_act4.act2 act2, cias_act4.act3 act3, cias_act4.act4 act4 into #act4"
'    sConsulta = sConsulta & vbLf & "from cias_act4 left join cias_act3 on cias_act4.act1 = cias_act3.act1"
'    sConsulta = sConsulta & vbLf & "and cias_act4.act2 = cias_act3.act2"
'    sConsulta = sConsulta & vbLf & "and cias_act4.act3 = cias_act3.act3"
'    sConsulta = sConsulta & vbLf & "and cias_act4.cia  = cias_act3.cia"
'    sConsulta = sConsulta & vbLf & "Where cias_act3.cia Is Null"
'    sConsulta = sConsulta & vbLf & "and cias_act4.cia = @CIA"
'    sConsulta = sConsulta & vbLf & "select cias_act3.act1 act1,cias_act3.act2 act2, cias_act3.act3 act3 into #act3"
'    sConsulta = sConsulta & vbLf & "from cias_act3 left join cias_act2 on cias_act3.act1 = cias_act2.act1"
'    sConsulta = sConsulta & vbLf & "  and cias_act3.act2 = cias_act2.act2"
'    sConsulta = sConsulta & vbLf & "and cias_act3.cia  = cias_act2.cia"
'    sConsulta = sConsulta & vbLf & "Where cias_act2.cia Is Null"
'    sConsulta = sConsulta & vbLf & "and cias_act3.cia = @CIA"
'    sConsulta = sConsulta & vbLf & "select cias_act2.act1 act1,cias_act2.act2 act2  into #act2"
'    sConsulta = sConsulta & vbLf & "from cias_act2 left join cias_act1 on cias_act2.act1 = cias_act1.act1"
'    sConsulta = sConsulta & vbLf & "  and cias_act2.cia  = cias_act1.cia"
'    sConsulta = sConsulta & vbLf & "Where cias_act1.cia Is Null"
'    sConsulta = sConsulta & vbLf & "and cias_act2.cia = @CIA"
'    sConsulta = sConsulta & vbLf & "select act1.id act1, case when cias_act1.act1 is null then 0 else 1 end actAsign into #act1"
'    sConsulta = sConsulta & vbLf & "from act1 left join cias_act1 on act1.id = cias_act1.act1"
'    sConsulta = sConsulta & vbLf & "and     @CIA = cias_act1.cia"
'    sConsulta = sConsulta & vbLf & "delete from #act1 where act1 in (select act1 from #act2 union select act1 from #act3 union select act1 from #act4 union select act1 from #act5)"
'    sConsulta = sConsulta & vbLf & "select act1,act2,act3,act4,act5, max(actasign) actasign  into #EstrAct from"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "select act1, null act2, null act3, null act4, null act5, actAsign from #act1"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "select act2.act1, act2.id,null act3, null act4, null  act5, case when #act2.act2=act2.id then 1 else 0 end"
'    sConsulta = sConsulta & vbLf & "from act2 inner join #act2 on act2.act1 = #act2.act1"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "select act3.act1, act3.act2, act3.id, null act4, null  act5, case when #act3.act3 = act3.id then 1 else 0 end"
'    sConsulta = sConsulta & vbLf & "from act3 inner join #act3 on act3.act1 = #act3.act1"
'    sConsulta = sConsulta & vbLf & "and act3.act2 = #act3.act2"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "select act4.act1, act4.act2, act4.act3, act4.id, null  act5, case when #act4.act4 = act4.id then 1 else 0 end"
'    sConsulta = sConsulta & vbLf & "from act4 inner join #act4 on act4.act1 = #act4.act1"
'    sConsulta = sConsulta & vbLf & "and act4.act2 = #act4.act2"
'    sConsulta = sConsulta & vbLf & "and act4.act3 = #act4.act3"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "select act5.act1, act5.act2, act5.act3, act5.act4, act5.id act5, case when #act5.act5 = act5.id then 1 else 0 end"
'    sConsulta = sConsulta & vbLf & "from act5 inner join #act5 on act5.act1 = #act5.act1"
'    sConsulta = sConsulta & vbLf & "and act5.act2 = #act5.act2"
'    sConsulta = sConsulta & vbLf & "and act5.act3 = #act5.act3"
'    sConsulta = sConsulta & vbLf & "and act5.act4 = #act5.act4"
'    sConsulta = sConsulta & vbLf & ") Actividades"
'    sConsulta = sConsulta & vbLf & "group by act1, act2,act3,act4,act5"
'    sConsulta = sConsulta & vbLf & "order by act1, act2,act3,act4,act5"
'    sConsulta = sConsulta & vbLf & "SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "if @IDI='SPA'"
'    sConsulta = sConsulta & vbLf & "select #estract.act1 act1, act1.cod cod1, act1.den_spa denact1 , #estract.act2 act2, act2.cod cod2,  act2.den_spa denact2, #estract.act3 act3, act3.cod cod3,  act3.den_spa denact3, #estract.act4 act4,  act4.cod cod4, act4.den_spa denact4,  #estract.act5,  act5.cod cod5, act5.den_spa denact5, actasign"
'    sConsulta = sConsulta & vbLf & "from #estract left join act1 on #estract.act1 = act1.id"
'    sConsulta = sConsulta & vbLf & "left join act2 on #estract.act1=act2.act1 and  #estract.act2 = act2.id"
'    sConsulta = sConsulta & vbLf & "left join act3 on #estract.act1=act3.act1 and  #estract.act2 = act3.act2 and #estract.act3 = act3.id"
'    sConsulta = sConsulta & vbLf & "left join act4 on #estract.act1=act4.act1 and  #estract.act2 = act4.act2 and #estract.act3 = act4.act3 and #estract.act4 = act4.id"
'    sConsulta = sConsulta & vbLf & "left join act5 on #estract.act1=act5.act1 and  #estract.act2 = act5.act2 and #estract.act3 = act5.act3 and #estract.act4 = act5.act4 and #estract.act5= act5.id"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "select #estract.act1 act1, act1.cod cod1, act1.den_eng denact1 , #estract.act2 act2, act2.cod cod2,  act2.den_eng denact2, #estract.act3 act3, act3.cod cod3,  act3.den_eng denact3, #estract.act4 act4,  act4.cod cod4, act4.den_eng denact4,  #estract.act5,  act5.cod cod5, act5.den_eng denact5, actasign"
'    sConsulta = sConsulta & vbLf & "from #estract left join act1 on #estract.act1 = act1.id"
'    sConsulta = sConsulta & vbLf & "left join act2 on #estract.act1=act2.act1 and  #estract.act2 = act2.id"
'    sConsulta = sConsulta & vbLf & "left join act3 on #estract.act1=act3.act1 and  #estract.act2 = act3.act2 and #estract.act3 = act3.id"
'    sConsulta = sConsulta & vbLf & "left join act4 on #estract.act1=act4.act1 and  #estract.act2 = act4.act2 and #estract.act3 = act4.act3 and #estract.act4 = act4.id"
'    sConsulta = sConsulta & vbLf & "left join act5 on #estract.act1=act5.act1 and  #estract.act2 = act5.act2 and #estract.act3 = act5.act3 and #estract.act4 = act5.act4 and #estract.act5= act5.id"
'
'    CrearSP_ACT_VISIBLE = sConsulta
'End Function
'
'Private Function CrearTablaPARGEN_DEF()
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[PARGEN_DEF] ("
'    sConsulta = sConsulta & vbLf & "    [ID] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "    [MONCEN] [int] NOT NULL)"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[PARGEN_DEF] WITH NOCHECK ADD "
'    sConsulta = sConsulta & vbLf & "    CONSTRAINT [PK_PARGEN_DEF] PRIMARY KEY  CLUSTERED ("
'    sConsulta = sConsulta & vbLf & "        [ID]"
'    sConsulta = sConsulta & vbLf & "    )  ON [PRIMARY] "
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[PARGEN_DEF] ADD "
'    sConsulta = sConsulta & vbLf & "    CONSTRAINT [FK_PARGEN_DEF_MON] FOREIGN KEY "
'    sConsulta = sConsulta & vbLf & "    ("
'    sConsulta = sConsulta & vbLf & "        [MONCEN]"
'    sConsulta = sConsulta & vbLf & "    ) REFERENCES [dbo].[MON] ("
'    sConsulta = sConsulta & vbLf & "        [ID] )"
'
'    CrearTablaPARGEN_DEF = sConsulta
'End Function
'
'Private Function CrearTablaVOLADJ()
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[VOLADJ] ("
'    sConsulta = sConsulta & vbLf & "[CIA_PROVE] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[CIA_COMP] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ANYO] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[GMN1] [varchar] (10) NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[PROCE] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[DENPROCE] [varchar] (100) NULL ,"
'    sConsulta = sConsulta & vbLf & "[FECADJ] [datetime] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[VOLGS] [float] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[MONGS] [varchar] (10) NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[MONPOR] [varchar] (3) NULL ,"
'    sConsulta = sConsulta & vbLf & "[VOLPOR] [float] NULL ,"
'    sConsulta = sConsulta & vbLf & "[FECACT] [datetime] NULL"
'    sConsulta = sConsulta & vbLf & ")"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[VOLADJ] WITH NOCHECK ADD "
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_VOLADJ] PRIMARY KEY  CLUSTERED "
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA_PROVE],"
'    sConsulta = sConsulta & vbLf & "[CIA_COMP],"
'    sConsulta = sConsulta & vbLf & "[ANYO],"
'    sConsulta = sConsulta & vbLf & "[GMN1],"
'    sConsulta = sConsulta & vbLf & "[PROCE]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[VOLADJ] ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_VOLADJ_CIAS] FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA_PROVE]"
'    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[CIAS] ("
'    sConsulta = sConsulta & vbLf & "[ID]"
'    sConsulta = sConsulta & vbLf & "),"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_VOLADJ_CIAS1] FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA_COMP]"
'    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[CIAS] ("
'    sConsulta = sConsulta & vbLf & "[ID]"
'    sConsulta = sConsulta & vbLf & "),"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_VOLADJ_MON] FOREIGN KEY"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[MONPOR]"
'    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[MON] ("
'    sConsulta = sConsulta & vbLf & "[COD]"
'    sConsulta = sConsulta & vbLf & ")"
'
'    CrearTablaVOLADJ = sConsulta
'End Function
'
'Private Function CrearTablaCIAS_MON()
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[CIAS_MON] ("
'    sConsulta = sConsulta & vbLf & "    [CIA] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "    [MON_PORTAL] [varchar] (3) NULL ,"
'    sConsulta = sConsulta & vbLf & "    [MON_GS] [varchar] (10) NULL ,"
'    sConsulta = sConsulta & vbLf & "    [DEN_GS] [varchar] (100) NULL )"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[CIAS_MON] WITH NOCHECK ADD "
'    sConsulta = sConsulta & vbLf & "    CONSTRAINT [PK_MON_CEN] PRIMARY KEY  CLUSTERED"
'    sConsulta = sConsulta & vbLf & "    ("
'    sConsulta = sConsulta & vbLf & "        [CIA]"
'    sConsulta = sConsulta & vbLf & "    )  ON [PRIMARY] "
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[CIAS_MON] ADD "
'    sConsulta = sConsulta & vbLf & "    CONSTRAINT [FK_CIAS_MON_MON] FOREIGN KEY "
'    sConsulta = sConsulta & vbLf & "    ("
'    sConsulta = sConsulta & vbLf & "        [MON_PORTAL]"
'    sConsulta = sConsulta & vbLf & "    ) REFERENCES [dbo].[MON] ("
'    sConsulta = sConsulta & vbLf & "        [COD]"
'    sConsulta = sConsulta & vbLf & ")"
'
'    CrearTablaCIAS_MON = sConsulta
'End Function
'
'Private Function CrearTriggerITEM_CIAS_TG_UPD() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TRIGGER [CIAS_TG_UPD] ON dbo.CIAS "
'    sConsulta = sConsulta & vbLf & "FOR UPDATE"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @ID INT,@EST INT,@ESTOLD INT"
'    sConsulta = sConsulta & vbLf & "DECLARE curTG_CIAS_Upd CURSOR FOR SELECT ID,FCEST FROM INSERTED"
'    sConsulta = sConsulta & vbLf & "OPEN curTG_CIAS_Upd"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS SET FECLASTUPD=GETDATE() WHERE ID=@ID"
'    sConsulta = sConsulta & vbLf & "SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID)"
'    sConsulta = sConsulta & vbLf & "IF @ESTOLD<> 3 AND @EST=3"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS SET FECACTFC=GETDATE() WHERE ID=@ID"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "IF @ESTOLD=3 AND @EST<>3"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS SET FECDESACTFC=GETDATE() WHERE ID=@ID"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "   FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Close curTG_CIAS_Upd"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_CIAS_Upd"
'
'    CrearTriggerITEM_CIAS_TG_UPD = sConsulta
'End Function
'
'
'Private Function CrearTriggerITEM_CIAS_PORT_TG_UPD() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TRIGGER [CIAS_PORT_TG_UPD] ON [CIAS_PORT] "
'    sConsulta = sConsulta & vbLf & "FOR UPDATE"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @CIA INT,@PORT INT,@ESTOLD INT, @EST INT"
'    sConsulta = sConsulta & vbLf & "DECLARE curTG_CIAS_PORT_Upd CURSOR FOR SELECT CIA,PORT,FPEST FROM INSERTED"
'    sConsulta = sConsulta & vbLf & "OPEN curTG_CIAS_PORT_Upd"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_CIAS_PORT_Upd INTO @CIA,@PORT,@EST"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS_PORT SET FECLASTUPD=GETDATE() WHERE CIA=@CIA AND PORT=@PORT"
'    sConsulta = sConsulta & vbLf & "SET @ESTOLD=(SELECT FPEST FROM DELETED WHERE CIA=@CIA AND PORT=@PORT)"
'    sConsulta = sConsulta & vbLf & "IF @ESTOLD<> 3 AND @EST=3"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS_PORT SET FECACTFP=GETDATE() WHERE CIA=@CIA AND PORT=@PORT"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "IF @ESTOLD=3 AND @EST<>3"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS_PORT SET FECDESACTFP=GETDATE() WHERE CIA=@CIA AND PORT=@PORT"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_CIAS_PORT_Upd INTO @CIA,@PORT,@EST"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Close curTG_CIAS_PORT_Upd"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_CIAS_PORT_Upd"
'
'    CrearTriggerITEM_CIAS_PORT_TG_UPD = sConsulta
'End Function
'
'Private Function CrearTriggerITEM_USU_TG_UPD() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TRIGGER [USU_TG_UPD] ON dbo.USU "
'    sConsulta = sConsulta & vbLf & "FOR UPDATE"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@CIA INT"
'    sConsulta = sConsulta & vbLf & "DECLARE curTG_USU_Upd CURSOR FOR SELECT ID,FCEST,CIA FROM INSERTED"
'    sConsulta = sConsulta & vbLf & "OPEN curTG_USU_Upd"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE USU SET FECLASTUPD=GETDATE() WHERE ID=@ID AND CIA=@CIA"
'    sConsulta = sConsulta & vbLf & "SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID AND CIA=@CIA)"
'    sConsulta = sConsulta & vbLf & "IF @ESTOLD<> 3 AND @EST=3"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE USU SET FECACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "IF @ESTOLD=3 AND @EST<>3"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE USU SET FECDESACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Close curTG_USU_Upd"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_USU_Upd"
'
'    CrearTriggerITEM_USU_TG_UPD = sConsulta
'End Function
'
'Private Function CrearTriggerITEM_USU_PORT_TG_UPD() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE TRIGGER [USU_PORT_TG_UPD] ON [USU_PORT] "
'    sConsulta = sConsulta & vbLf & "FOR UPDATE"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @CIA INT,@USU INT,@PORT INT,@EST INT,@ESTOLD INT"
'    sConsulta = sConsulta & vbLf & "DECLARE curTG_USU_PORT_Upd CURSOR FOR SELECT CIA,USU,PORT,FPEST FROM INSERTED"
'    sConsulta = sConsulta & vbLf & "OPEN curTG_USU_PORT_Upd"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_USU_PORT_Upd INTO @CIA,@USU,@PORT,@EST"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE USU_PORT SET FECLASTUPD=GETDATE()"
'    sConsulta = sConsulta & vbLf & "SET @ESTOLD=(SELECT FPEST FROM DELETED WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT)"
'    sConsulta = sConsulta & vbLf & "IF @ESTOLD<>3 AND @EST=3"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE USU_PORT SET FECACTFP=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "IF @ESTOLD=3 AND @EST<> 3"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "UPDATE USU_PORT SET FECDESACTFP=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_USU_PORT_Upd INTO @CIA,@USU,@PORT,@EST"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Close curTG_USU_PORT_Upd"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_USU_PORT_Upd"
'
'    CrearTriggerITEM_USU_PORT_TG_UPD = sConsulta
'
'End Function
'
'Public Function CodigoDeActualizacion2_0_4A2_0_5() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    '''Nuevo campo PREM_ACTIVO en  REL_CIAS
'    sConsulta = "ALTER TABLE [dbo].[REL_CIAS] ADD PREM_ACTIVO [tinyint] NOT NULL DEFAULT (0)"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    '''Nueva tabla MCIAS_ACT5
'    sConsulta = CrearTablaMCIAS_ACT5
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nueva tabla MCIAS_ACT4
'    sConsulta = CrearTablaMCIAS_ACT4
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nueva tabla MCIAS_ACT3
'    sConsulta = CrearTablaMCIAS_ACT3
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nueva tabla MCIAS_ACT2
'    sConsulta = CrearTablaMCIAS_ACT2
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nueva tabla MCIAS_ACT1
'    sConsulta = CrearTablaMCIAS_ACT1
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    '''Nuevo campo PENDCONF en CIAS
'    sConsulta = "ALTER TABLE [dbo].[CIAS] ADD PENDCONF [bit] NOT NULL DEFAULT (0)"
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    '''Nuevo store procedure  SP_ACT_PENDIENTE
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ACT_PENDIENTE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ACT_PENDIENTE]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_ACT_PENDIENTE
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nuevo store procedure SP_ACT_VISIBLE_2_0_4A2_0_5
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ACT_VISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ACT_VISIBLE]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_ACT_VISIBLE_2_0_4A2_0_5
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    '''Nuevo store procedure SP_AUTORIZAR_PREMIUM
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AUTORIZAR_PREMIUM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AUTORIZAR_PREMIUM]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_AUTORIZAR_PREMIUM
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nuevo store procedure SP_DESAUTORIZAR_PREMIUM
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DESAUTORIZAR_PREMIUM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DESAUTORIZAR_PREMIUM]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_DESAUTORIZAR_PREMIUM
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    '''Nuevo store procedure SP_COMPROBAR_EMAIL
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_COMPROBAR_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_COMPROBAR_EMAIL]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_COMPROBAR_EMAIL
'    ExecuteSQL gRDOCon, sConsulta
'
'    '''Nuevo store procedure SP_ELIMINAR_COMPANIA
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ELIMINAR_COMPANIA]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_ELIMINAR_COMPANIA
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    '''Nuevo store procedure SP_MODIFICAR_CODIGO_CIA
'    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_MODIFICAR_CODIGO_CIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_MODIFICAR_CODIGO_CIA]"
'    ExecuteSQL gRDOCon, sConsulta
'    sConsulta = CrearSP_MODIFICAR_CODIGO_CIA
'    ExecuteSQL gRDOCon, sConsulta
'
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.0.5'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_4A2_0_5 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_4A2_0_5 = False
'
'End Function
'
'Public Function CodigoDeActualizacion2_0_5A2_0_6() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'
'
'
'    'Eliminar (por si acaso existen los stored que se crean en esta nueva versi�n)
'    sConsulta = Crear_SQL_Eliminar_Storeds()
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = Crear_SP_ANYA_ADJUNTO()
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = Crear_SP_ANYA_OFERTA_WEB()
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = Crear_SP_MODIF_ITEM_OFE()
'    ExecuteSQL gRDOCon, sConsulta
'
'    sConsulta = Crear_SP_UPLOAD_ADJUN_WEB()
'    ExecuteSQL gRDOCon, sConsulta
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.7.00'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_5A2_0_6 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_5A2_0_6 = False
'
'End Function
'
'
'Public Function CodigoDeActualizacion2_0_7A2_0_7_25() As Boolean
'    Dim bTransaccionEnCurso As Boolean
'    Dim sConsulta As String
'
'    On Error GoTo error
'
'    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
'    bTransaccionEnCurso = True
'
'    'Eliminar (por si acaso existen los stored que se crean en esta nueva versi�n)
'    sConsulta = Crear_SQL_Eliminar_Storeds_2_7_25()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo store procedure SP_DEVOLVER_PROCESOS_PROVE
'    sConsulta = Crear_SP_DEVOLVER_PROCESOS_PROVE()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo store procedure SP_DEVOLVER_PROCESO
'    sConsulta = Crear_SP_DEVOLVER_PROCESO()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo store procedure SP_DEVOLVER_OFERTA_PROVE
'    sConsulta = Crear_SP_DEVOLVER_OFERTA_PROVE()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo store procedure SP_DEVOLVER_PUJAS_ITEM
'    sConsulta = Crear_SP_DEVOLVER_PUJAS_ITEM()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nueva tabla PROVENOTIF
'    sConsulta = CrearTablaPROVENOTIF()
'    ExecuteSQL gRDOCon, sConsulta
'
'    'Nuevo store procedure SP_ACTUALIZAR_PEND_NOTIF
'    sConsulta = Crear_SP_ACTUALIZAR_PEND_NOTIF()
'    ExecuteSQL gRDOCon, sConsulta
'
'
'    'Actualizar el store procedure SP_MODIF_ITEM_OFE
'    sConsulta = Crear_SP_MODIF_ITEM_OFE_2_7_25
'    ExecuteSQL gRDOCon, sConsulta
'
'
'
'    'Introducimos el nuevo valor de versi�n
'    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.7.25'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
'    ExecuteSQL gRDOCon, sConsulta
'
'    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
'    bTransaccionEnCurso = False
'
'    CodigoDeActualizacion2_0_7A2_0_7_25 = True
'    Exit Function
'
'error:
'    If bTransaccionEnCurso Then
'        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
'    End If
'
'    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
'
'    CodigoDeActualizacion2_0_7A2_0_7_25 = False
'
'End Function
'
'
'
'Private Function CrearSP_ELIMINAR_COMPANIA() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_ELIMINAR_COMPANIA @ID INT AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)"
'    sConsulta = sConsulta & vbLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'    sConsulta = sConsulta & vbLf & "DECLARE @LNKSERVER VARCHAR(200)"
'    sConsulta = sConsulta & vbLf & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID"
'    sConsulta = sConsulta & vbLf & "--SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "OPEN curRELCIAS"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'    sConsulta = sConsulta & vbLf & "SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''"
'    sConsulta = sConsulta & vbLf & "EXEC SP_EXECUTESQL @SQLSTRING"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "CLOSE CURRELCIAS"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE CURRELCIAS"
'    sConsulta = sConsulta & vbLf & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS_MON WHERE CIA = @ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS_COM WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS_PORT WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM USU_PORT WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM USU WHERE CIA=@ID"
'    sConsulta = sConsulta & vbLf & "DELETE FROM CIAS WHERE ID=@ID"
'
'
'    CrearSP_ELIMINAR_COMPANIA = sConsulta
'End Function
'
'
'Private Function CrearSP_COMPROBAR_EMAIL() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_COMPROBAR_EMAIL(@FSP_CIA VARCHAR(20),@EMAIL VARCHAR(100),@EXISTE INTEGER OUTPUT) AS"
'    sConsulta = sConsulta & vbLf & "DECLARE   @CIA INTEGER"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SET @CIA = (SELECT ID FROM CIAS WHERE COD=@FSP_CIA)"
'    sConsulta = sConsulta & vbLf & "SET @EXISTE = (SELECT COUNT(*)  FROM USU INNER JOIN USU_PORT ON USU.CIA = USU_PORT.CIA AND USU.ID = USU_PORT.USU  WHERE USU.CIA=@CIA AND USU.EMAIL=@EMAIL)"
'    sConsulta = sConsulta & vbLf & "END"
'
'    CrearSP_COMPROBAR_EMAIL = sConsulta
'End Function
'
'Private Function CrearSP_DESAUTORIZAR_PREMIUM() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_DESAUTORIZAR_PREMIUM @ID INT AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)"
'    sConsulta = sConsulta & vbLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'    sConsulta = sConsulta & vbLf & "DECLARE @LNKSERVER VARCHAR(200)"
'    sConsulta = sConsulta & vbLf & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID"
'    sConsulta = sConsulta & vbLf & "SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "OPEN curRELCIAS"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'    sConsulta = sConsulta & vbLf & "SELECT @SQLSTRING = @LNKSERVER + 'SP_DESACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'"
'    sConsulta = sConsulta & vbLf & "EXEC SP_EXECUTESQL @SQLSTRING"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "CLOSE CURRELCIAS"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE CURRELCIAS"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS SET PREMIUM = 3  WHERE ID=@ID"
'    sConsulta = sConsulta & vbLf & "UPDATE REL_CIAS SET PREM_ACTIVO = 0 WHERE CIA_PROVE = @ID"
'
'
'    CrearSP_DESAUTORIZAR_PREMIUM = sConsulta
'End Function
'
'
'Private Function CrearSP_AUTORIZAR_PREMIUM() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_AUTORIZAR_PREMIUM @ID INT AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)"
'    sConsulta = sConsulta & vbLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'    sConsulta = sConsulta & vbLf & "DECLARE @LNKSERVER VARCHAR(200)"
'    sConsulta = sConsulta & vbLf & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID"
'    sConsulta = sConsulta & vbLf & "SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "OPEN curRELCIAS"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'    sConsulta = sConsulta & vbLf & "SELECT @SQLSTRING = @LNKSERVER + 'SP_ACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'"
'    sConsulta = sConsulta & vbLf & "EXEC SP_EXECUTESQL @SQLSTRING"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "CLOSE CURRELCIAS"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE CURRELCIAS"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS SET PREMIUM =2  WHERE ID=@ID"
'    sConsulta = sConsulta & vbLf & "UPDATE REL_CIAS SET PREM_ACTIVO =1 WHERE CIA_PROVE = @ID"
'
'    CrearSP_AUTORIZAR_PREMIUM = sConsulta
'End Function
'
'Private Function CrearSP_ACT_PENDIENTE() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_ACT_PENDIENTE  @CIA int,@IDI varchar(20)"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)"
'    sConsulta = sConsulta & vbLf & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1, act2, act3, act4, act5 ) select act1, act2, act3, act4, act5  from cias_act5 a"
'    sConsulta = sConsulta & vbLf & "                   where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act4 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "and a.act4 = b.act4 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act3 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = c.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = c.act3 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act2 d"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = d.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = d.act2)"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act1 e"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = e.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1, act2, act3, act4 ) select act1, act2, act3, act4  from cias_act4 a"
'    sConsulta = sConsulta & vbLf & "                   where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act3 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = b.act3 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act2 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = c.act2 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act1 d"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'     sConsulta = sConsulta & vbLf & "and a.act1 = d.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1, act2, act3 ) select act1, act2, act3  from cias_act3 a"
'    sConsulta = sConsulta & vbLf & "                   where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act2 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act1 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1, act2 ) select act1, act2 from cias_act2 a"
'    sConsulta = sConsulta & vbLf & "                   where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act1 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1) select act1 from cias_act1 where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "if (select count(*) from mcias_act5 where cia=@CIA)>0"
'    sConsulta = sConsulta & vbLf & "begin"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1, act2, act3, act4, act5 ) select act1, act2, act3, act4, act5  from mcias_act5 a"
'    sConsulta = sConsulta & vbLf & "                   where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act4 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "and a.act4 = b.act4 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act3 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = c.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = c.act3 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act2 d"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = d.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = d.act2)"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act1 e"
'    sConsulta = sConsulta & vbLf & "                                      where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "        and a.act1 = e.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1, act2, act3, act4 ) select act1, act2, act3, act4  from mcias_act4 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act3 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = b.act3 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act2 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = c.act2 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act1 d"
'    sConsulta = sConsulta & vbLf & "      where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "                                        and a.act1 = d.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1, act2, act3 ) select act1, act2, act3  from mcias_act3 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act2 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act1 c"
'    sConsulta = sConsulta & vbLf & "      where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "                                        and a.act1 = c.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1, act2 ) select act1, act2 from mcias_act2 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "       from mcias_act1 b"
'    sConsulta = sConsulta & vbLf & "                                      where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1) select act1 from mcias_act1 where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz select * from #raiz"
'    sConsulta = sConsulta & vbLf & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1,act2,act3,act4,act5)"
'    sConsulta = sConsulta & vbLf & "select act1,act2,act3,act4,min(act5) from (select * from #raiz where act5 is not null"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "           select * from #mraiz where act5 is not null) a"
'    sConsulta = sConsulta & vbLf & "group by act1,act2,act3,act4"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1,act2,act3,act4)"
'    sConsulta = sConsulta & vbLf & "select act1,act2,act3,min(act4) from (select * from #raiz where act4 is not null"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "           select * from #mraiz where act4 is not null) a"
'    sConsulta = sConsulta & vbLf & "group by act1,act2,act3"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1,act2,act3)"
'    sConsulta = sConsulta & vbLf & "select act1,act2,min(act3) from (select * from #raiz where act3 is not null"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "           select * from #mraiz where act3 is not null) a"
'    sConsulta = sConsulta & vbLf & "group by act1,act2"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1,act2)"
'    sConsulta = sConsulta & vbLf & "select act1,min(act2) from (select * from #raiz where act2 is not null"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "           select * from #mraiz where act2 is not null) a"
'    sConsulta = sConsulta & vbLf & "group by act1"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1)"
'    sConsulta = sConsulta & vbLf & "select min(act1) from (select * from #raiz"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "select * from #mraiz) a"
'    sConsulta = sConsulta & vbLf & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1,Act2,Act3,act4,act5)"
'    sConsulta = sConsulta & vbLf & "select a.act1,a.act2,a.act3,a.act4,a.id"
'    sConsulta = sConsulta & vbLf & "from act5 a inner join #base b on a.act1=b.act1 and a.act2=b.act2 and a.act3 = b.act3 and a.act4 = b.act4"
'    sConsulta = sConsulta & vbLf & "Where b.act5 Is Not Null"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1,Act2,Act3,act4)"
'    sConsulta = sConsulta & vbLf & "select a.act1,a.act2,a.act3,a.id"
'    sConsulta = sConsulta & vbLf & "from act4 a inner join #base b on a.act1=b.act1 and a.act2=b.act2 and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "Where b.act5 Is Null And b.act4 Is Not Null"
'    sConsulta = sConsulta & vbLf & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.act2 = c.act2 and a.act3=c.act3 and a.id=c.act4)"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1,Act2,Act3)"
'    sConsulta = sConsulta & vbLf & "select a.act1,a.act2,a.id"
'    sConsulta = sConsulta & vbLf & "from act3 a inner join #base b on a.act1=b.act1 and a.act2=b.act2"
'    sConsulta = sConsulta & vbLf & "Where b.act4 Is Null And b.act3 Is Not Null"
'    sConsulta = sConsulta & vbLf & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.act2 = c.act2 and a.id=c.act3)"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1,Act2)"
'    sConsulta = sConsulta & vbLf & "select a.act1,a.id"
'    sConsulta = sConsulta & vbLf & "from act2 a inner join #base b on a.act1=b.act1"
'    sConsulta = sConsulta & vbLf & "Where b.act3 Is Null And b.act2 Is Not Null"
'    sConsulta = sConsulta & vbLf & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.id = c.act2)"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1)"
'    sConsulta = sConsulta & vbLf & "select a.id"
'    sConsulta = sConsulta & vbLf & "from act1 a"
'    sConsulta = sConsulta & vbLf & "where not exists (SELECT * FROM #Estructura c where a.id=c.act1)"
'    sConsulta = sConsulta & vbLf & "update #Estructura set actAsign=0 ,actPend=0"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actasign = 1"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1"
'    sConsulta = sConsulta & vbLf & "Where b.act2 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actasign = 2"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "Where b.act3 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actasign = 3"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "Where b.act4 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actasign = 4"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4"
'    sConsulta = sConsulta & vbLf & "Where b.act5 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actasign = 5"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actpend = 1"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1"
'    sConsulta = sConsulta & vbLf & "Where b.act2 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actpend = 2"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "Where b.act3 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actpend = 3"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "Where b.act4 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actpend = 4"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4"
'    sConsulta = sConsulta & vbLf & "Where b.act5 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actpend = 5"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5"
'    sConsulta = sConsulta & vbLf & "if @IDI='SPA'"
'    sConsulta = sConsulta & vbLf & "select #Estructura .act1 act1, act1.cod cod1, act1.den_spa denact1 , #Estructura.act2 act2, act2.cod cod2,  act2.den_spa denact2, #Estructura.act3 act3, act3.cod cod3,  act3.den_spa denact3, #Estructura.act4 act4,  act4.cod cod4, act4.den_spa denact4,  #Estructura.act5,  act5.cod cod5, act5.den_spa denact5, actasign, actpend"
'    sConsulta = sConsulta & vbLf & "from #Estructura  left join act1 on #Estructura.act1 = act1.id"
'    sConsulta = sConsulta & vbLf & "left join act2 on #Estructura.act1=act2.act1 and  #Estructura.act2 = act2.id"
'    sConsulta = sConsulta & vbLf & "left join act3 on #Estructura.act1=act3.act1 and  #Estructura.act2 = act3.act2 and #Estructura.act3 = act3.id"
'    sConsulta = sConsulta & vbLf & "left join act4 on #Estructura.act1=act4.act1 and  #Estructura.act2 = act4.act2 and #Estructura.act3 = act4.act3 and #Estructura.act4 = act4.id"
'    sConsulta = sConsulta & vbLf & "left join act5 on #Estructura.act1=act5.act1 and  #Estructura.act2 = act5.act2 and #Estructura.act3 = act5.act3 and #Estructura.act4 = act5.act4 and #Estructura.act5= act5.id"
'    sConsulta = sConsulta & vbLf & "Where actasign > 0 And actpend > 0"
'    sConsulta = sConsulta & vbLf & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "select #Estructura.act1 act1, act1.cod cod1, act1.den_eng denact1 , #Estructura.act2 act2, act2.cod cod2,  act2.den_eng denact2, #Estructura.act3 act3, act3.cod cod3,  act3.den_eng denact3, #Estructura.act4 act4,  act4.cod cod4, act4.den_eng denact4,  #Estructura.act5,  act5.cod cod5, act5.den_eng denact5, actasign, actpend"
'    sConsulta = sConsulta & vbLf & "from #Estructura left join act1 on #Estructura.act1 = act1.id"
'    sConsulta = sConsulta & vbLf & "left join act2 on #Estructura.act1=act2.act1 and  #Estructura.act2 = act2.id"
'    sConsulta = sConsulta & vbLf & "left join act3 on #Estructura.act1=act3.act1 and  #Estructura.act2 = act3.act2 and #Estructura.act3 = act3.id"
'    sConsulta = sConsulta & vbLf & "left join act4 on #Estructura.act1=act4.act1 and  #Estructura.act2 = act4.act2 and #Estructura.act3 = act4.act3 and #Estructura.act4 = act4.id"
'    sConsulta = sConsulta & vbLf & "left join act5 on #Estructura.act1=act5.act1 and  #Estructura.act2 = act5.act2 and #Estructura.act3 = act5.act3 and #Estructura.act4 = act5.act4 and #Estructura.act5= act5.id"
'    sConsulta = sConsulta & vbLf & "Where actasign > 0 And actpend > 0"
'    sConsulta = sConsulta & vbLf & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5"
'
'
'    CrearSP_ACT_PENDIENTE = sConsulta
'
'End Function
'
'Private Function CrearSP_ACT_VISIBLE_2_0_4A2_0_5() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_ACT_VISIBLE @CIA int,@IDI varchar(20)"
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)"
'    sConsulta = sConsulta & vbLf & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1, act2, act3, act4, act5 ) select act1, act2, act3, act4, act5  from cias_act5 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act4 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "and a.act4 = b.act4 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act3 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = c.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = c.act3 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act2 d"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = d.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = d.act2)"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act1 e"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = e.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1, act2, act3, act4 ) select act1, act2, act3, act4  from cias_act4 a"
'    sConsulta = sConsulta & vbLf & "                   where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act3 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = b.act3 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act2 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = c.act2 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act1 d"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = d.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1, act2, act3 ) select act1, act2, act3  from cias_act3 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act2 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act1 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1, act2 ) select act1, act2 from cias_act2 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from cias_act1 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #raiz (act1) select act1 from cias_act1 where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "if (select count(*) from mcias_act5 where cia=@CIA)>0"
'    sConsulta = sConsulta & vbLf & "begin"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1, act2, act3, act4, act5 ) select act1, act2, act3, act4, act5  from mcias_act5 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act4 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "and a.act4 = b.act4 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act3 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = c.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = c.act3 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act2 d"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = d.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = d.act2)"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act1 e"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = e.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1, act2, act3, act4 ) select act1, act2, act3, act4  from mcias_act4 a"
'    sConsulta = sConsulta & vbLf & "                   where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act3 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "and a.act3 = b.act3 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act2 c"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = c.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = c.act2 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act1 d"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "                                        and a.act1 = d.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1, act2, act3 ) select act1, act2, act3  from mcias_act3 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act2 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1"
'    sConsulta = sConsulta & vbLf & "and a.act2 = b.act2 )"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act1 c"
'    sConsulta = sConsulta & vbLf & "      where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "                                        and a.act1 = c.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1, act2 ) select act1, act2 from mcias_act2 a"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and not exists (select *"
'    sConsulta = sConsulta & vbLf & "from mcias_act1 b"
'    sConsulta = sConsulta & vbLf & "where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "and a.act1 = b.act1)"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz (act1) select act1 from mcias_act1 where cia=@CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "insert into #mraiz select * from #raiz"
'    sConsulta = sConsulta & vbLf & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1,act2,act3,act4,act5)"
'    sConsulta = sConsulta & vbLf & "select act1,act2,act3,act4,min(act5) from (select * from #raiz where act5 is not null"
'    sConsulta = sConsulta & vbLf & "            Union"
'    sConsulta = sConsulta & vbLf & "select * from #mraiz where act5 is not null) a"
'    sConsulta = sConsulta & vbLf & "group by act1,act2,act3,act4"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1,act2,act3,act4)"
'    sConsulta = sConsulta & vbLf & "select act1,act2,act3,min(act4) from (select * from #raiz where act4 is not null"
'    sConsulta = sConsulta & vbLf & "            Union"
'    sConsulta = sConsulta & vbLf & "select * from #mraiz where act4 is not null) a"
'    sConsulta = sConsulta & vbLf & "group by act1,act2,act3"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1,act2,act3)"
'    sConsulta = sConsulta & vbLf & "select act1,act2,min(act3) from (select * from #raiz where act3 is not null"
'    sConsulta = sConsulta & vbLf & "            Union"
'    sConsulta = sConsulta & vbLf & "select * from #mraiz where act3 is not null) a"
'    sConsulta = sConsulta & vbLf & "group by act1,act2"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1,act2)"
'    sConsulta = sConsulta & vbLf & "select act1,min(act2) from (select * from #raiz where act2 is not null"
'    sConsulta = sConsulta & vbLf & "            Union"
'    sConsulta = sConsulta & vbLf & "select * from #mraiz where act2 is not null) a"
'    sConsulta = sConsulta & vbLf & "group by act1"
'    sConsulta = sConsulta & vbLf & "insert into #base (act1)"
'    sConsulta = sConsulta & vbLf & "select min(act1) from (select * from #raiz"
'    sConsulta = sConsulta & vbLf & "Union"
'    sConsulta = sConsulta & vbLf & "select * from #mraiz) a"
'    sConsulta = sConsulta & vbLf & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1,Act2,Act3,act4,act5)"
'    sConsulta = sConsulta & vbLf & "select a.act1,a.act2,a.act3,a.act4,a.id"
'    sConsulta = sConsulta & vbLf & "from act5 a inner join #base b on a.act1=b.act1 and a.act2=b.act2 and a.act3 = b.act3 and a.act4 = b.act4"
'    sConsulta = sConsulta & vbLf & "Where b.act5 Is Not Null"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1,Act2,Act3,act4)"
'    sConsulta = sConsulta & vbLf & "select a.act1,a.act2,a.act3,a.id"
'    sConsulta = sConsulta & vbLf & "from act4 a inner join #base b on a.act1=b.act1 and a.act2=b.act2 and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "Where b.act5 Is Null And b.act4 Is Not Null"
'    sConsulta = sConsulta & vbLf & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.act2 = c.act2 and a.act3=c.act3 and a.id=c.act4)"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1,Act2,Act3)"
'    sConsulta = sConsulta & vbLf & "select a.act1,a.act2,a.id"
'    sConsulta = sConsulta & vbLf & "from act3 a inner join #base b on a.act1=b.act1 and a.act2=b.act2"
'    sConsulta = sConsulta & vbLf & "Where b.act4 Is Null And b.act3 Is Not Null"
'    sConsulta = sConsulta & vbLf & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.act2 = c.act2 and a.id=c.act3)"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1,Act2)"
'    sConsulta = sConsulta & vbLf & "select a.act1,a.id"
'    sConsulta = sConsulta & vbLf & "from act2 a inner join #base b on a.act1=b.act1"
'    sConsulta = sConsulta & vbLf & "Where b.act3 Is Null And b.act2 Is Not Null"
'    sConsulta = sConsulta & vbLf & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.id = c.act2)"
'    sConsulta = sConsulta & vbLf & "insert into #Estructura (Act1)"
'    sConsulta = sConsulta & vbLf & "select a.id"
'    sConsulta = sConsulta & vbLf & "from act1 a"
'    sConsulta = sConsulta & vbLf & "where not exists (SELECT * FROM #Estructura c where a.id=c.act1)"
'    sConsulta = sConsulta & vbLf & "update #Estructura set actAsign=0 ,actPend=0"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actAsign = 1"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1"
'    sConsulta = sConsulta & vbLf & "Where b.act2 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actAsign = 2"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "Where b.act3 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actAsign = 3"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "Where b.act4 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actAsign = 4"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4"
'    sConsulta = sConsulta & vbLf & "Where b.act5 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actAsign = 5"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actPend = 1"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1"
'    sConsulta = sConsulta & vbLf & "Where b.act2 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actPend = 2"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2"
'    sConsulta = sConsulta & vbLf & "Where b.act3 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actPend = 3"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3"
'    sConsulta = sConsulta & vbLf & "Where b.act4 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actPend = 4"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4"
'    sConsulta = sConsulta & vbLf & "Where b.act5 Is Null"
'    sConsulta = sConsulta & vbLf & "update #Estructura"
'    sConsulta = sConsulta & vbLf & "Set actPend = 5"
'    sConsulta = sConsulta & vbLf & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5"
'    sConsulta = sConsulta & vbLf & "SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "if @IDI='SPA'"
'    sConsulta = sConsulta & vbLf & "select #Estructura .act1 act1, act1.cod cod1, act1.den_spa denact1 , #Estructura.act2 act2, act2.cod cod2,  act2.den_spa denact2, #Estructura.act3 act3, act3.cod cod3,  act3.den_spa denact3, #Estructura.act4 act4,  act4.cod cod4, act4.den_spa denact4,  #Estructura.act5,  act5.cod cod5, act5.den_spa denact5, actasign, actpend"
'    sConsulta = sConsulta & vbLf & "from #Estructura  left join act1 on #Estructura.act1 = act1.id"
'    sConsulta = sConsulta & vbLf & "left join act2 on #Estructura.act1=act2.act1 and  #Estructura.act2 = act2.id"
'    sConsulta = sConsulta & vbLf & "left join act3 on #Estructura.act1=act3.act1 and  #Estructura.act2 = act3.act2 and #Estructura.act3 = act3.id"
'    sConsulta = sConsulta & vbLf & "left join act4 on #Estructura.act1=act4.act1 and  #Estructura.act2 = act4.act2 and #Estructura.act3 = act4.act3 and #Estructura.act4 = act4.id"
'    sConsulta = sConsulta & vbLf & "left join act5 on #Estructura.act1=act5.act1 and  #Estructura.act2 = act5.act2 and #Estructura.act3 = act5.act3 and #Estructura.act4 = act5.act4 and #Estructura.act5= act5.id"
'    sConsulta = sConsulta & vbLf & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5"
'    sConsulta = sConsulta & vbLf & "Else"
'    sConsulta = sConsulta & vbLf & "select #Estructura.act1 act1, act1.cod cod1, act1.den_eng denact1 , #Estructura.act2 act2, act2.cod cod2,  act2.den_eng denact2, #Estructura.act3 act3, act3.cod cod3,  act3.den_eng denact3, #Estructura.act4 act4,  act4.cod cod4, act4.den_eng denact4,  #Estructura.act5,  act5.cod cod5, act5.den_eng denact5, actasign, actpend"
'    sConsulta = sConsulta & vbLf & "from #Estructura left join act1 on #Estructura.act1 = act1.id"
'    sConsulta = sConsulta & vbLf & "left join act2 on #Estructura.act1=act2.act1 and  #Estructura.act2 = act2.id"
'    sConsulta = sConsulta & vbLf & "left join act3 on #Estructura.act1=act3.act1 and  #Estructura.act2 = act3.act2 and #Estructura.act3 = act3.id"
'    sConsulta = sConsulta & vbLf & "left join act4 on #Estructura.act1=act4.act1 and  #Estructura.act2 = act4.act2 and #Estructura.act3 = act4.act3 and #Estructura.act4 = act4.id"
'    sConsulta = sConsulta & vbLf & "left join act5 on #Estructura.act1=act5.act1 and  #Estructura.act2 = act5.act2 and #Estructura.act3 = act5.act3 and #Estructura.act4 = act5.act4 and #Estructura.act5= act5.id"
'    sConsulta = sConsulta & vbLf & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5"
'
'    CrearSP_ACT_VISIBLE_2_0_4A2_0_5 = sConsulta
'End Function
'
'
'Private Function CrearTablaMCIAS_ACT5() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[MCIAS_ACT5] ("
'    sConsulta = sConsulta & vbLf & "[CIA] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT1] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT2] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT3] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT4] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT5] [int] NOT NULL"
'    sConsulta = sConsulta & vbLf & ")"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[MCIAS_ACT5] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_MCIAS_ACT5] PRIMARY KEY  CLUSTERED"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA],"
'    sConsulta = sConsulta & vbLf & "[ACT1],"
'    sConsulta = sConsulta & vbLf & "[ACT2],"
'    sConsulta = sConsulta & vbLf & "[ACT3],"
'    sConsulta = sConsulta & vbLf & "[ACT4],"
'    sConsulta = sConsulta & vbLf & "[ACT5]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'
'    CrearTablaMCIAS_ACT5 = sConsulta
'End Function
'
'Private Function CrearTablaMCIAS_ACT4() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[MCIAS_ACT4] ("
'    sConsulta = sConsulta & vbLf & "[CIA] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT1] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT2] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT3] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT4] [int] NOT NULL"
'    sConsulta = sConsulta & vbLf & ")"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[MCIAS_ACT4] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_MCIAS_ACT4] PRIMARY KEY  CLUSTERED"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA],"
'    sConsulta = sConsulta & vbLf & "[ACT1],"
'    sConsulta = sConsulta & vbLf & "[ACT2],"
'    sConsulta = sConsulta & vbLf & "[ACT3],"
'    sConsulta = sConsulta & vbLf & "[ACT4]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'
'    CrearTablaMCIAS_ACT4 = sConsulta
'End Function
'
'
'Private Function CrearTablaMCIAS_ACT3() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[MCIAS_ACT3] ("
'    sConsulta = sConsulta & vbLf & "[CIA] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT1] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT2] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT3] [int] NOT NULL"
'    sConsulta = sConsulta & vbLf & ")"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[MCIAS_ACT3] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_MCIAS_ACT3] PRIMARY KEY  CLUSTERED"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA],"
'    sConsulta = sConsulta & vbLf & "[ACT1],"
'    sConsulta = sConsulta & vbLf & "[ACT2],"
'    sConsulta = sConsulta & vbLf & "[ACT3]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'
'    CrearTablaMCIAS_ACT3 = sConsulta
'End Function
'
'
'Private Function CrearTablaMCIAS_ACT2() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[MCIAS_ACT2] ("
'    sConsulta = sConsulta & vbLf & "[CIA] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT1] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT2] [int] NOT NULL"
'    sConsulta = sConsulta & vbLf & ")"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[MCIAS_ACT2] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_MCIAS_ACT2] PRIMARY KEY  CLUSTERED"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA],"
'    sConsulta = sConsulta & vbLf & "[ACT1],"
'    sConsulta = sConsulta & vbLf & "[ACT2]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'
'    CrearTablaMCIAS_ACT2 = sConsulta
'End Function
'
'Private Function CrearTablaMCIAS_ACT1() As String
'Dim sConsulta As String
'
'    sConsulta = "CREATE TABLE [dbo].[MCIAS_ACT1] ("
'    sConsulta = sConsulta & vbLf & "[CIA] [int] NOT NULL ,"
'    sConsulta = sConsulta & vbLf & "[ACT1] [int] NOT NULL"
'    sConsulta = sConsulta & vbLf & ")"
'    sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[MCIAS_ACT1] WITH NOCHECK ADD"
'    sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_MCIAS_ACT1] PRIMARY KEY  CLUSTERED"
'    sConsulta = sConsulta & vbLf & "("
'    sConsulta = sConsulta & vbLf & "[CIA],"
'    sConsulta = sConsulta & vbLf & "[ACT1]"
'    sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"
'
'    CrearTablaMCIAS_ACT1 = sConsulta
'End Function
'
'Private Function CrearSP_MODIFICAR_CODIGO_CIA() As String
'    Dim sConsulta As String
'
'    sConsulta = "CREATE PROCEDURE SP_MODIFICAR_CODIGO_CIA @CIA INTEGER, @COD VARCHAR(20) "
'    sConsulta = sConsulta & vbLf & "AS"
'    sConsulta = sConsulta & vbLf & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)"
'    sConsulta = sConsulta & vbLf & "DECLARE @COMPRADOR BIT"
'    sConsulta = sConsulta & vbLf & "DECLARE @LNKSERVER VARCHAR(200)"
'    sConsulta = sConsulta & vbLf & "DECLARE @SQLSTRING NVARCHAR(500)"
'    sConsulta = sConsulta & vbLf & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @CIA"
'    sConsulta = sConsulta & vbLf & "SET CONCAT_NULL_YIELDS_NULL ON"
'    sConsulta = sConsulta & vbLf & "SET NOCOUNT ON"
'    sConsulta = sConsulta & vbLf & "SELECT @COMPRADOR = CASE WHEN FCEST>1 THEN 1 ELSE 0 END,  @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA"
'    sConsulta = sConsulta & vbLf & "IF @COMPRADOR = 1 AND @LNKSERVER IS NOT NULL"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SELECT @SQLSTRING =  @LNKSERVER + 'SP_MODIFICAR_CODIGO_PORTAL @COD=''' + @COD +  ''''"
'    sConsulta = sConsulta & vbLf & "EXEC SP_EXECUTESQL @SQLSTRING"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "OPEN curRELCIAS"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'    sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
'    sConsulta = sConsulta & vbLf & "BEGIN"
'    sConsulta = sConsulta & vbLf & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP"
'    sConsulta = sConsulta & vbLf & "SELECT @SQLSTRING = @LNKSERVER + 'SP_MODIFICAR_FSP_COD @PROVE=''' + @COD_PROVE_CIA + ''',@COD = ''' + @COD_PROVE_CIA + ''''"
'    sConsulta = sConsulta & vbLf & "EXEC SP_EXECUTESQL @SQLSTRING"
'    sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA"
'    sConsulta = sConsulta & vbLf & "End"
'    sConsulta = sConsulta & vbLf & "Close CURRELCIAS"
'    sConsulta = sConsulta & vbLf & "DEALLOCATE CURRELCIAS"
'    sConsulta = sConsulta & vbLf & "UPDATE CIAS SET COD=@COD  WHERE ID=@CIA"
'
'    CrearSP_MODIFICAR_CODIGO_CIA = sConsulta
'End Function

Public Function CrearJobs() As Boolean
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo Error
        
        
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

        frmProgreso.lblDetalle = "Actualizaciones para JOB proveedores"
        frmProgreso.lblDetalle.Refresh
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

        If Not CrearJobActuProveedores Then
            If bTransaccionEnCurso Then
                gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
            End If
            CrearJobs = False
            Exit Function
        End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
        
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Actualizaciones para el EliminarSesionCaducada"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
    If Not CrearJobEliminarSesionCaducada Then
        If bTransaccionEnCurso Then
            gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
        End If
        CrearJobs = False
        Exit Function
    End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Actualizaciones para el truncar log"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
    If Not CrearJobTruncarLog Then
        If bTransaccionEnCurso Then
            gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
        End If
        CrearJobs = False
        Exit Function
    End If

    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False
    
    CrearJobs = True
    Exit Function
Error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
     
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CrearJobs = False
End Function


Private Function CrearStoredProcedureFS_TRUNCAR_LOG(ByVal strBase As String) As String
    Dim sConsulta As String
    
    sConsulta = sConsulta & "CREATE PROCEDURE FS_TRUNCAR_LOG AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "BACKUP LOG " & strBase & " WITH TRUNCATE_ONLY" & vbCrLf
    sConsulta = sConsulta & "DBCC SHRINKDATABASE (" & strBase & ", 25)" & vbCrLf
    
    CrearStoredProcedureFS_TRUNCAR_LOG = sConsulta
End Function

Public Function CrearJobTruncarLog() As Boolean
    Dim StartYear, StartMonth, StartDay As String
    Dim sfecha As String
    Dim lFecha As Long
    Dim sConsulta As String
    Dim sLogin As String
    Dim sContra As String
    Dim adocommAdo As ADODB.Command
    Dim adoparamPar As ADODB.Parameter
    Dim oADOConnection As ADODB.Connection

    On Error GoTo Ir_Error:
    
    
    'Crea el proc.almacenado FS_TRUNCAR_LOG_strBD (si existe primero lo borra)
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[FS_TRUNCAR_LOG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[FS_TRUNCAR_LOG]"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = CrearStoredProcedureFS_TRUNCAR_LOG(gBD)
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Conexion
    
    sLogin = gUID
    sContra = gPWD
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", sLogin, sContra
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    Set adocommAdo = New ADODB.Command
    Set adocommAdo.ActiveConnection = oADOConnection
    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "SP_JOB_CREARJOBTRUNCARLOG"
   
    
''    '****** Si no existe el job lo crea **********************
    'Nombre del Job
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("TRUNCAR_LOG_" & gBD))
    adocommAdo.Parameters.Append adoparamPar

    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("@NOMBRE_SCH", adVarChar, adParamInput, 50, "S_TRUNCAR_LOG")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 8) 'SQLDMOFreq_Weekly
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1) 'SQLDMO_WEEKDAY_TYPE.SQLDMOWeek_Sunday

    adocommAdo.Parameters.Append adoparamPar

    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sfecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sfecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sfecha = "120000"
    lFecha = CLng(sfecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "FS_TRUNCAR_LOG")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("FRECRECFACT", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar


    adocommAdo.Execute

    CrearJobTruncarLog = True
    Set oADOConnection = Nothing
    Exit Function

Ir_Error:
    CrearJobTruncarLog = False
    
End Function


Public Function CrearJobActuProveedores() As Boolean
    Dim StartYear, StartMonth, StartDay As String
    Dim sfecha As String
    Dim lFecha As Long
    Dim sLogin As String
    Dim sContra As String
    Dim adocommAdo As ADODB.Command
    Dim adoparamPar As ADODB.Parameter
    Dim oADOConnection As ADODB.Connection

    On Error GoTo Ir_Error:
        
    'Conexion
    
    sLogin = gUID
    sContra = gPWD
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", sLogin, sContra
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    Set adocommAdo = New ADODB.Command
    Set adocommAdo.ActiveConnection = oADOConnection
    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "SP_JOB_CREARJOBACTPROVE"
   
''    '****** Si no existe el job lo crea **********************
    'Nombre del Job
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("JOBACTPROVE_" & gBD))
    adocommAdo.Parameters.Append adoparamPar

    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("@NOMBRE_SCH", adVarChar, adParamInput, 50, "SCHED_JOBACTPROVE_" & gBD)
    adocommAdo.Parameters.Append adoparamPar

    
    
    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("TIPOPERIODO", adInteger, adParamInput, , 8)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("FRECUENCIA", adInteger, adParamInput, , 2)
    adocommAdo.Parameters.Append adoparamPar
        

    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sfecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sfecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sfecha = "000001"
    lFecha = CLng(sfecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sfecha = "235959"
    lFecha = CLng(sfecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEFIN", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP_JOBACTPROVE_" & gBD)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "SP_JOB_ACTUALIZAR_PROV_GS")
    adocommAdo.Parameters.Append adoparamPar
    

    adocommAdo.Execute

    CrearJobActuProveedores = True
    Set oADOConnection = Nothing
    Exit Function

Ir_Error:
    CrearJobActuProveedores = False
    
End Function


Public Function Actualizar(ByVal DeVersion As String, AVersion As String) As Boolean
Dim NumVersion As String
    
    Dim DeVerSinPuntos As String
    Dim AVerSinPuntos As String
      
    DeVerSinPuntos = Replace(DeVersion, ".", "")
    AVerSinPuntos = Replace(AVersion, ".", "")
    
    If DeVersion = "3.00.18.12.1" Then
    Else
        ' Precondici�n
        If CDbl(DeVerSinPuntos) > CDbl(AVerSinPuntos) Then
            Actualizar = True
            Exit Function
        End If
    End If
    
    If CDbl(DeVerSinPuntos) = CDbl(AVerSinPuntos) Then
        Actualizar = True
        Exit Function
    End If
    
    frmProgreso.lblDetalle = "Actualizando de versi�n : " & CStr(DeVersion) & " a versi�n " & CStr(AVersion)
    
    frmProgreso.Show
    
    DoEvents
    
    frmProgreso.ProgressBar1.Value = 1
    
    Select Case DeVersion
         Case "3.00.20.07", "3.00.20.08"
            NumVersion = "3.00.21.00"
            If Not CodigoDeActualizacion32100_01_207_A32100_02_21_00 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.21.00"
            NumVersion = "3.00.21.01"
            If Not CodigoDeActualizacion32100_02_21_00_A32100_02_21_01 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.21.01"
            NumVersion = "3.00.21.02"
            If Not CodigoDeActualizacion32100_02_21_01_A32100_02_21_02 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.21.02"
            NumVersion = "3.00.21.03"
            If Not CodigoDeActualizacion32100_02_21_02_A32100_02_21_03 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.21.03"
            NumVersion = "3.00.21.04"
            If Not CodigoDeActualizacion32100_02_21_03_A32100_02_21_04 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.21.04"
            NumVersion = "3.00.21.05"
            If Not CodigoDeActualizacion32100_02_21_04_A32100_02_21_05 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.21.05"
            NumVersion = "3.00.22.00"
            If Not CodigoDeActualizacion32100_02_21_05_A32100_02_22_00 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.22.00"
            NumVersion = "3.00.22.01"
            If Not CodigoDeActualizacion32100_02_22_00_A32100_02_22_01 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.22.01"
            NumVersion = "3.00.22.02"
            If Not CodigoDeActualizacion32100_03_22_01_A32100_03_22_02 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.22.02"
            NumVersion = "3.00.22.03"
            If Not CodigoDeActualizacion32100_03_22_02_A32100_03_22_03 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.22.03"
            NumVersion = "3.00.22.04"
            If Not CodigoDeActualizacion32100_03_22_03_A32100_03_22_04 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.22.04", "3.00.22.05", "3.00.22.06"
            NumVersion = "3.00.23.00"
            If Not CodigoDeActualizacion32100_04_22_04_A32100_04_23_00 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.23.00"
            NumVersion = "3.00.23.01"
            If Not CodigoDeActualizacion32100_04_23_00_A32100_04_23_01 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.23.01"
            NumVersion = "3.00.23.02"
            If Not CodigoDeActualizacion32100_04_23_01_A32100_04_23_02 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.23.02"
            NumVersion = "3.00.23.03"
            If Not CodigoDeActualizacion32100_04_23_02_A32100_04_23_03 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.23.03"
            NumVersion = "3.00.23.04"
            If Not CodigoDeActualizacion32100_04_23_03_A32100_04_23_04 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.23.04"
            NumVersion = "3.00.23.05"
            If Not CodigoDeActualizacion32100_04_23_04_A32100_04_23_05 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
        Case "3.00.23.05"
            NumVersion = "3.00.23.06"
            If Not CodigoDeActualizacion32100_04_23_05_A32100_04_23_06 Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
            If Not Actualizar(NumVersion, AVersion) Then
                MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
                Actualizar = False
                Exit Function
            End If
    Case "3.00.23.06"
        NumVersion = "3.00.23.07"
        If Not CodigoDeActualizacion32100_04_23_06_A32100_04_23_07 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.23.07"
        NumVersion = "3.00.24.01"
        If Not CodigoDeActualizacion32100_04_24_00_A32100_04_24_01 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.24.01"
        NumVersion = "3.00.24.02"
        If Not CodigoDeActualizacion32100_04_24_01_A32100_04_24_02 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.24.02"
         NumVersion = "3.00.25.01"
        If Not CodigoDeActualizacion32100_07_25_00_A32100_07_25_01 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.25.01"
         NumVersion = "3.00.25.02"
        If Not CodigoDeActualizacion32100_07_25_01_A32100_07_25_02 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.25.02"
        NumVersion = "3.00.26.01"
        If Not CodigoDeActualizacion32100_07_26_02_A32100_07_26_01 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
     Case "3.00.26.01"
        NumVersion = "3.00.26.02"
        If Not CodigoDeActualizacion32100_07_26_01_A32100_07_26_02 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.26.02"
         NumVersion = "3.00.27.01"
        If Not CodigoDeActualizacion32200_01_26_00_A32200_01_26_01 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.27.01"
        NumVersion = "3.00.27.02"
        If Not CodigoDeActualizacion32200_01_27_01_A32200_01_27_02 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.27.02"
        NumVersion = "3.00.27.03"
        If Not CodigoDeActualizacion32200_01_27_02_A32200_01_27_03 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.27.03"
        NumVersion = "3.00.27.04"
        If Not CodigoDeActualizacion32200_01_27_03_A32200_01_27_04 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.27.04", "3.00.27.05", "3.00.27.06"
        NumVersion = "3.00.28.01"
        If Not CodigoDeActualizacion32200_01_28_00_A32200_01_28_01 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.28.01"
        NumVersion = "3.00.28.02"
        If Not CodigoDeActualizacion32200_01_28_01_A32200_01_28_02 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.28.02"
        NumVersion = "3.00.29.01"
        If Not CodigoDeActualizacion32200_01_29_00_A32200_01_29_01 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.29.01"
        NumVersion = "3.00.29.02"
        If Not CodigoDeActualizacion32200_01_29_01_A32200_01_29_02 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.29.02", "3.00.29.03"
        NumVersion = "3.00.30.01"
        If Not CodigoDeActualizacion32200_01_30_00_A32200_01_30_01 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.30.01"
        NumVersion = "3.00.36.02"
        If Not CodigoDeActualizacion3_3_3001_A3_3_3602 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.36.02"
        NumVersion = "3.00.36.03"
        If Not CodigoDeActualizacion3_3_3602_A3_3_3603 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.36.03"
        NumVersion = "3.00.36.04"
        If Not CodigoDeActualizacion3_3_3603_A3_3_3604 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.36.04"
        NumVersion = "3.00.36.10"
        If Not CodigoDeActualizacion3_3_3604_A3_3_3610 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.36.10"
        NumVersion = "3.00.36.11"
        If Not CodigoDeActualizacion3_3_3610_A3_3_3611 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.36.11"
        NumVersion = "3.00.37.01"
        If Not CodigoDeActualizacion3_3_3700_A3_3_3701 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.37.01"
        NumVersion = "3.00.37.02"
        If Not CodigoDeActualizacion3_3_3701_A3_3_3702 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
    Case "3.00.37.02"
        NumVersion = "3.00.37.03"
        If Not CodigoDeActualizacion3_3_3702_A3_3_3703 Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
        If Not Actualizar(NumVersion, AVersion) Then
            MsgBox "Imposible actualizar a la versi�n " & AVersion & vbLf & "Ultima actualizaci�n:" & DeVersion
            Actualizar = False
            Exit Function
        End If
End Select
    
    Actualizar = True
    
End Function


Public Function CrearJobEliminarSesionCaducada() As Boolean
Dim adocommAdo As ADODB.Command
Dim adoparamPar As ADODB.Parameter
Dim sfecha As String
Dim lFecha As Long
Dim StartYear, StartMonth, StartDay As String
Dim oADOConnection As ADODB.Connection
Dim sLogin As String
Dim sContra As String

On Error GoTo Ir_Error:

    sLogin = gUID
    sContra = gPWD

    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", sLogin, sContra
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120

    Set adocommAdo = New ADODB.Command

    Set adocommAdo.ActiveConnection = oADOConnection

    adocommAdo.CommandType = adCmdStoredProc

    adocommAdo.CommandText = "SP_JOB_CREARJOBELIMSESION_CADUCADA"

     'Nombre del Job

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("ELIM_SESION_CADUCADA_" & gBD))

    adocommAdo.Parameters.Append adoparamPar
     'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "SCHED_JOBELIMSESION_" & gBD)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("TIPO2", adInteger, adParamInput, , 8)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO2", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar
    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay
    sfecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sfecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar
    sfecha = "000000"
    lFecha = CLng(sfecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP_JOBELIMSESION_" & gBD)
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "FSP_ELIM_SESION_CADUCADA")
    adocommAdo.Parameters.Append adoparamPar
    adocommAdo.Execute
    CrearJobEliminarSesionCaducada = True
    Exit Function
Ir_Error:
    CrearJobEliminarSesionCaducada = False
End Function
