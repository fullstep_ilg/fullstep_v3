Attribute VB_Name = "bas_V_32100_4"
Public Function CodigoDeActualizacion32100_04_22_04_A32100_04_23_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_4_Tablas_000
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_4_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.23.00'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_04_22_04_A32100_04_23_00 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_04_22_04_A32100_04_23_00 = False
End Function

Private Sub V_32100_4_Tablas_000()
Dim sConsulta As String

sConsulta = "IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'ORDEN' AND Object_ID = Object_ID(N'MON'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON ADD ORDEN SMALLINT NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32100_4_Storeds_000()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ACCIONES_ROL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ACCIONES_ROL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_FIELDSCALC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_FIELDSCALC]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETFIELDS_INSTANCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTANCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTANCIAS_PADRE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTANCIAS_PADRE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETREQUEST]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETREQUEST]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADESTADOSINSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADESTADOSINSTANCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_VER_DETALLE_PERSONA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_VER_DETALLE_PERSONA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADINSTFIELDSCALC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADINSTFIELDSCALC]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOAD_ETAPAS_REALIZADAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOAD_ETAPAS_REALIZADAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOAD_ACCION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOAD_ACCION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADFIELDSCALC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADFIELDSCALC]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADFIELDSDESGLOSECALC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADFIELDSDESGLOSECALC]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARTICIPANTES_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARTICIPANTES_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADCERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADCERTIFICADO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADCORRESPONDENCIACAMPOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADCORRESPONDENCIACAMPOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_GET_PAGOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_GET_PAGOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_OBT_DATOS_FACTURA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_OBT_DATOS_FACTURA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_OBT_DATOS_RECEPTOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_OBT_DATOS_RECEPTOR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_PROVES_ERP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_PROVES_ERP]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_REGISTRO_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_REGISTRO_EMAIL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ACTUALIZAR_EN_PROCESO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ACTUALIZAR_EN_PROCESO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ACTUALIZAR_MONITORIZACION_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ACTUALIZAR_MONITORIZACION_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PERSONAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PERSONAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_04_23_00_A32100_04_23_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
   
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_4_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.23.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_04_23_00_A32100_04_23_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_04_23_00_A32100_04_23_01 = False
End Function

Private Sub V_32100_4_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETTIPOSOLICITUD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETTIPOSOLICITUD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS] @TIPO INT ,@PAGINA NVARCHAR(2000) ,@FPETICION VARCHAR(50), @FECHA varchar(50),@IDSESION VARCHAR(40),@USUCOD VARCHAR(50),@IPDIR VARCHAR(50),@CIACODGS VARCHAR(50)= NULL,@CIACOD VARCHAR(50)=NULL ,@PRODUCTO VARCHAR(50),@PAGINAORIGEN NVARCHAR(2000)=NULL,@POSTBACK INT,@NAVEGADOR NVARCHAR(200),@IDREGISTRO VARCHAR(30),@QUERYSTRING NVARCHAR(2000)=NULL,@OUTPUT INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "/************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/*** Descripci?n: Inserta y Modifica registros en FSAL_ACCESOS (FSGS), llamado desde el WebService FSAL_RegistrarAccesos.asmx (PMPortal)    *****/" & vbCrLf
sConsulta = sConsulta & "/*** Par?metros: En funcion de @TIPO, se hace una insercion o una modificacion                                                             *****/" & vbCrLf
sConsulta = sConsulta & "/***             Devuelve un parametro de salida <> 0 en caso de error y 0 en caso corrrecto para evaluarlo en la llamada                  *****/" & vbCrLf
sConsulta = sConsulta & "/************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500);" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLPARAMS NVARCHAR(MAX);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_REGISTRAR_ACCESO_ASPX" & vbCrLf
sConsulta = sConsulta & "                               @TIPO" & vbCrLf
sConsulta = sConsulta & "                               ,@PAGINA" & vbCrLf
sConsulta = sConsulta & "                               ,@FPETICION" & vbCrLf
sConsulta = sConsulta & "                               ,@FECHA" & vbCrLf
sConsulta = sConsulta & "                               ,@IDSESION" & vbCrLf
sConsulta = sConsulta & "                               ,@USUCOD" & vbCrLf
sConsulta = sConsulta & "                               ,@IPDIR" & vbCrLf
sConsulta = sConsulta & "                               ,@CIACODGS" & vbCrLf
sConsulta = sConsulta & "                               ,@CIACOD" & vbCrLf
sConsulta = sConsulta & "                               ,@PRODUCTO" & vbCrLf
sConsulta = sConsulta & "                               ,@PAGINAORIGEN" & vbCrLf
sConsulta = sConsulta & "                               ,@POSTBACK" & vbCrLf
sConsulta = sConsulta & "                               ,@NAVEGADOR" & vbCrLf
sConsulta = sConsulta & "                               ,@IDREGISTRO" & vbCrLf
sConsulta = sConsulta & "                               ,@QUERYSTRING" & vbCrLf
sConsulta = sConsulta & "                               ,@OUTPUT OUTPUT';" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQLPARAMS = N'@TIPO INT" & vbCrLf
sConsulta = sConsulta & "                             ,@PAGINA NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "                             ,@FPETICION VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@FECHA varchar(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@IDSESION VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "                             ,@USUCOD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@IPDIR VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@CIACODGS VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@CIACOD VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@PRODUCTO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@PAGINAORIGEN NVARCHAR(2000)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@POSTBACK INT" & vbCrLf
sConsulta = sConsulta & "                             ,@NAVEGADOR NVARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "                             ,@IDREGISTRO VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "                             ,@QUERYSTRING NVARCHAR(2000)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@OUTPUT INT OUTPUT';" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, @SQLPARAMS," & vbCrLf
sConsulta = sConsulta & "               @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "               ,@PAGINA=@PAGINA" & vbCrLf
sConsulta = sConsulta & "               ,@FPETICION=@FPETICION" & vbCrLf
sConsulta = sConsulta & "               ,@FECHA=@FECHA" & vbCrLf
sConsulta = sConsulta & "               ,@IDSESION=@IDSESION" & vbCrLf
sConsulta = sConsulta & "               ,@USUCOD=@USUCOD" & vbCrLf
sConsulta = sConsulta & "               ,@IPDIR=@IPDIR" & vbCrLf
sConsulta = sConsulta & "               ,@CIACODGS=@CIACODGS" & vbCrLf
sConsulta = sConsulta & "               ,@CIACOD=@CIACOD" & vbCrLf
sConsulta = sConsulta & "               ,@PRODUCTO=@PRODUCTO" & vbCrLf
sConsulta = sConsulta & "               ,@PAGINAORIGEN=@PAGINAORIGEN" & vbCrLf
sConsulta = sConsulta & "               ,@POSTBACK=@POSTBACK" & vbCrLf
sConsulta = sConsulta & "               ,@NAVEGADOR=@NAVEGADOR" & vbCrLf
sConsulta = sConsulta & "               ,@IDREGISTRO=@IDREGISTRO" & vbCrLf
sConsulta = sConsulta & "               ,@QUERYSTRING=@QUERYSTRING" & vbCrLf
sConsulta = sConsulta & "               ,@OUTPUT=@OUTPUT OUTPUT;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           RETURN" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @OUTPUT = 1;" & vbCrLf
sConsulta = sConsulta & "           RETURN" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_04_23_01_A32100_04_23_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
   
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_4_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.23.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_04_23_01_A32100_04_23_02 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_04_23_01_A32100_04_23_02 = False
End Function

Private Sub V_32100_4_Storeds_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETADJUNTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETADJUNTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTADJUNTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB] @ID int, @AMBITO tinyint, @GRUPO varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM=(SELECT CAMBIO FROM PROCE_OFE WITH(NOLOCK) WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EL CAMBIO QUE DEVUELVE ES EL CAMBIO DE LA MONEDA EN QUE SE GUARD? LA OFERTA RESPECTO A LA MONEDA DEL PROCESO" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "   SELECT G.ATRIB_ID, G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL,G.IMPPARCIAL,@CAM CAMBIO,PO.MON" & vbCrLf
sConsulta = sConsulta & "FROM OFE_ATRIB G WITH(NOLOCK)      " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=G.ID         " & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID= @ID" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "   SELECT G.ATRIB_ID, G.GRUPO,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL,@CAM CAMBIO ,IP.IMPPARCIAL,PO.MON" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=G.ID" & vbCrLf
sConsulta = sConsulta & "       left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "   SELECT G.ATRIB_ID, G.GRUPO,G.ITEM,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL,@CAM CAMBIO,IP.IMPPARCIAL,PO.MON" & vbCrLf
sConsulta = sConsulta & "FROM OFE_ITEM_ATRIB G WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=G.ID" & vbCrLf
sConsulta = sConsulta & "       left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta



End Sub

Public Function CodigoDeActualizacion32100_04_23_02_A32100_04_23_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
   
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_4_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.23.03'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_04_23_02_A32100_04_23_03 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_04_23_02_A32100_04_23_03 = False
End Function

Private Sub V_32100_4_Storeds_003()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LISTADOS_ROL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LISTADOS_ROL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_DATOS_GRABAR_CERTIF]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_DATOS_GRABAR_CERTIF]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETFIELD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETFIELD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTFIELD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTFIELD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETDESGLOSE] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDESGLOSE @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD, @PORTAL=1  '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_04_23_03_A32100_04_23_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
   
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_4_Storeds_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.23.04'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_04_23_03_A32100_04_23_04 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_04_23_03_A32100_04_23_04 = False
End Function

Private Sub V_32100_4_Storeds_004()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS] @TIPO INT ,@PAGINA NVARCHAR(400) ,@FPETICION VARCHAR(50), @FECHA varchar(50),@IDSESION VARCHAR(40),@USUCOD VARCHAR(50),@IPDIR VARCHAR(50),@CIACODGS VARCHAR(50)= NULL,@CIACOD VARCHAR(50)=NULL ,@PRODUCTO NVARCHAR(20),@PAGINAORIGEN NVARCHAR(2000)=NULL,@POSTBACK INT,@NAVEGADOR NVARCHAR(500),@IDREGISTRO VARCHAR(30),@QUERYSTRING NVARCHAR(2000)=NULL,@OUTPUT INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "/************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/*** Descripci?n: Inserta y Modifica registros en FSAL_ACCESOS (FSGS), llamado desde el WebService FSAL_RegistrarAccesos.asmx (PMPortal)    *****/" & vbCrLf
sConsulta = sConsulta & "/*** Par?metros: En funcion de @TIPO, se hace una insercion o una modificacion                                                             *****/" & vbCrLf
sConsulta = sConsulta & "/***             Devuelve un parametro de salida <> 0 en caso de error y 0 en caso corrrecto para evaluarlo en la llamada                  *****/" & vbCrLf
sConsulta = sConsulta & "/************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500);" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLPARAMS NVARCHAR(MAX);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_REGISTRAR_ACCESO_ASPX" & vbCrLf
sConsulta = sConsulta & "                               @TIPO" & vbCrLf
sConsulta = sConsulta & "                               ,@PAGINA" & vbCrLf
sConsulta = sConsulta & "                               ,@FPETICION" & vbCrLf
sConsulta = sConsulta & "                               ,@FECHA" & vbCrLf
sConsulta = sConsulta & "                               ,@IDSESION" & vbCrLf
sConsulta = sConsulta & "                               ,@USUCOD" & vbCrLf
sConsulta = sConsulta & "                               ,@IPDIR" & vbCrLf
sConsulta = sConsulta & "                               ,@CIACODGS" & vbCrLf
sConsulta = sConsulta & "                               ,@CIACOD" & vbCrLf
sConsulta = sConsulta & "                               ,@PRODUCTO" & vbCrLf
sConsulta = sConsulta & "                               ,@PAGINAORIGEN" & vbCrLf
sConsulta = sConsulta & "                               ,@POSTBACK" & vbCrLf
sConsulta = sConsulta & "                               ,@NAVEGADOR" & vbCrLf
sConsulta = sConsulta & "                               ,@IDREGISTRO" & vbCrLf
sConsulta = sConsulta & "                               ,@QUERYSTRING" & vbCrLf
sConsulta = sConsulta & "                               ,@OUTPUT OUTPUT';" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQLPARAMS = N'@TIPO INT" & vbCrLf
sConsulta = sConsulta & "                             ,@PAGINA NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "                             ,@FPETICION VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@FECHA varchar(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@IDSESION VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "                             ,@USUCOD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@IPDIR VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@CIACODGS VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@CIACOD VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@PRODUCTO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@PAGINAORIGEN NVARCHAR(2000)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@POSTBACK INT" & vbCrLf
sConsulta = sConsulta & "                             ,@NAVEGADOR NVARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "                             ,@IDREGISTRO VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "                             ,@QUERYSTRING NVARCHAR(2000)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@OUTPUT INT OUTPUT';" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, @SQLPARAMS," & vbCrLf
sConsulta = sConsulta & "               @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "               ,@PAGINA=@PAGINA" & vbCrLf
sConsulta = sConsulta & "               ,@FPETICION=@FPETICION" & vbCrLf
sConsulta = sConsulta & "               ,@FECHA=@FECHA" & vbCrLf
sConsulta = sConsulta & "               ,@IDSESION=@IDSESION" & vbCrLf
sConsulta = sConsulta & "               ,@USUCOD=@USUCOD" & vbCrLf
sConsulta = sConsulta & "               ,@IPDIR=@IPDIR" & vbCrLf
sConsulta = sConsulta & "               ,@CIACODGS=@CIACODGS" & vbCrLf
sConsulta = sConsulta & "               ,@CIACOD=@CIACOD" & vbCrLf
sConsulta = sConsulta & "               ,@PRODUCTO=@PRODUCTO" & vbCrLf
sConsulta = sConsulta & "               ,@PAGINAORIGEN=@PAGINAORIGEN" & vbCrLf
sConsulta = sConsulta & "               ,@POSTBACK=@POSTBACK" & vbCrLf
sConsulta = sConsulta & "               ,@NAVEGADOR=@NAVEGADOR" & vbCrLf
sConsulta = sConsulta & "               ,@IDREGISTRO=@IDREGISTRO" & vbCrLf
sConsulta = sConsulta & "               ,@QUERYSTRING=@QUERYSTRING" & vbCrLf
sConsulta = sConsulta & "               ,@OUTPUT=@OUTPUT OUTPUT;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           RETURN" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @OUTPUT = 1;" & vbCrLf
sConsulta = sConsulta & "           RETURN" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_04_23_04_A32100_04_23_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
   
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_4_Storeds_005
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.23.05'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_04_23_04_A32100_04_23_05 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_04_23_04_A32100_04_23_05 = False
End Function

Private Sub V_32100_4_Storeds_005()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ERRORES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ERRORES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ESTADISTICAS_ERRORES]" & vbCrLf
sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
sConsulta = sConsulta & "@entorno varchar(15)," & vbCrLf
sConsulta = sConsulta & "@pyme varchar(50)=null" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAFIN DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAINI DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@FECHAINI DATETIME, @FECHAFIN DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "--La fecha/hora @fecha que luego se asigna a @FECHAINI viene en UTC desde el servicio FSALPortalWinService," & vbCrLf
sConsulta = sConsulta & "--por lo que @FECHAFIN tambien la pasaremos en UTC." & vbCrLf
sConsulta = sConsulta & "--Criterio general: * Los datos de origen para estadisticas registrados en UTC(FSAL_ACCESOS) -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "--                 * Los datos de origen que no esten en UTC(LOG, TIPO_SOLICITUDES -> FSAL_EVENTOS, ERRORES -> FSAL_ERRORES) ->" & vbCrLf
sConsulta = sConsulta & "--                   -> rango de traspaso en UTC y fecha insertada con el traspaso en UTC. -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "SET @FECHAFIN=GETUTCDATE()" & vbCrLf
sConsulta = sConsulta & "SET @FECHAINI=@fecha" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'EXECUTE ' + @FSGS + 'FSAL_ESTADISTICAS_ERRORES @FECHAINI, @FECHAFIN, @ENTORNO, @PYME'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION, @FECHAINI=@FECHAINI, @FECHAFIN=@FECHAFIN, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_KPIS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_KPIS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ESTADISTICAS_KPIS] @fecha DATETIME,@entorno varchar(15),@pyme varchar(15)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
sConsulta = sConsulta & "--La fecha/hora @DESDE viene en UTC desde el servicio FSALPortalWinService," & vbCrLf
sConsulta = sConsulta & "--por lo que @HASTA tambien la pasaremos en UTC." & vbCrLf
sConsulta = sConsulta & "--Criterio general: * Los datos de origen para estadisticas registrados en UTC(FSAL_ACCESOS) -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "--                 * Los datos de origen que no esten en UTC(LOG, TIPO_SOLICITUDES -> FSAL_EVENTOS, ERRORES -> FSAL_ERRORES) ->" & vbCrLf
sConsulta = sConsulta & "--                   -> rango de traspaso en UTC y fecha insertada con el traspaso en UTC. -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "SET @HASTA=GETUTCDATE()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_ESTADISTICAS_KPIS @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)', @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX_FSGS] @TIPO INT ,@PAGINA NVARCHAR(400) ,@FPETICION VARCHAR(50), @FECHA varchar(50),@IDSESION VARCHAR(40),@USUCOD VARCHAR(50),@IPDIR VARCHAR(50),@CIACODGS VARCHAR(50)= NULL,@CIACOD VARCHAR(50)=NULL ,@PRODUCTO NVARCHAR(20),@PAGINAORIGEN NVARCHAR(2000)=NULL,@POSTBACK INT,@NAVEGADOR NVARCHAR(500),@IDREGISTRO VARCHAR(30),@QUERYSTRING NVARCHAR(2000)=NULL,@OUTPUT INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "/************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/*** Descripci?n: Inserta y Modifica registros en FSAL_ACCESOS (FSGS), llamado desde el WebService FSAL_RegistrarAccesos.asmx (PMPortal)    *****/" & vbCrLf
sConsulta = sConsulta & "/*** Par?metros: En funcion de @TIPO, se hace una insercion o una modificacion                                                             *****/" & vbCrLf
sConsulta = sConsulta & "/***             Devuelve un parametro de salida <> 0 en caso de error y 0 en caso corrrecto para evaluarlo en la llamada                  *****/" & vbCrLf
sConsulta = sConsulta & "/************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(MAX);" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLPARAMS NVARCHAR(MAX);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_REGISTRAR_ACCESO_ASPX" & vbCrLf
sConsulta = sConsulta & "                               @TIPO" & vbCrLf
sConsulta = sConsulta & "                               ,@PAGINA" & vbCrLf
sConsulta = sConsulta & "                               ,@FPETICION" & vbCrLf
sConsulta = sConsulta & "                               ,@FECHA" & vbCrLf
sConsulta = sConsulta & "                               ,@IDSESION" & vbCrLf
sConsulta = sConsulta & "                               ,@USUCOD" & vbCrLf
sConsulta = sConsulta & "                               ,@IPDIR" & vbCrLf
sConsulta = sConsulta & "                               ,@CIACODGS" & vbCrLf
sConsulta = sConsulta & "                               ,@CIACOD" & vbCrLf
sConsulta = sConsulta & "                               ,@PRODUCTO" & vbCrLf
sConsulta = sConsulta & "                               ,@PAGINAORIGEN" & vbCrLf
sConsulta = sConsulta & "                               ,@POSTBACK" & vbCrLf
sConsulta = sConsulta & "                               ,@NAVEGADOR" & vbCrLf
sConsulta = sConsulta & "                               ,@IDREGISTRO" & vbCrLf
sConsulta = sConsulta & "                               ,@QUERYSTRING" & vbCrLf
sConsulta = sConsulta & "                               ,@OUTPUT OUTPUT';" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQLPARAMS = N'@TIPO INT" & vbCrLf
sConsulta = sConsulta & "                             ,@PAGINA NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "                             ,@FPETICION VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@FECHA varchar(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@IDSESION VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "                             ,@USUCOD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@IPDIR VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@CIACODGS VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@CIACOD VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@PRODUCTO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "                             ,@PAGINAORIGEN NVARCHAR(2000)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@POSTBACK INT" & vbCrLf
sConsulta = sConsulta & "                             ,@NAVEGADOR NVARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "                             ,@IDREGISTRO VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "                             ,@QUERYSTRING NVARCHAR(2000)=NULL" & vbCrLf
sConsulta = sConsulta & "                             ,@OUTPUT INT OUTPUT';" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, @SQLPARAMS," & vbCrLf
sConsulta = sConsulta & "               @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "               ,@PAGINA=@PAGINA" & vbCrLf
sConsulta = sConsulta & "               ,@FPETICION=@FPETICION" & vbCrLf
sConsulta = sConsulta & "               ,@FECHA=@FECHA" & vbCrLf
sConsulta = sConsulta & "               ,@IDSESION=@IDSESION" & vbCrLf
sConsulta = sConsulta & "               ,@USUCOD=@USUCOD" & vbCrLf
sConsulta = sConsulta & "               ,@IPDIR=@IPDIR" & vbCrLf
sConsulta = sConsulta & "               ,@CIACODGS=@CIACODGS" & vbCrLf
sConsulta = sConsulta & "               ,@CIACOD=@CIACOD" & vbCrLf
sConsulta = sConsulta & "               ,@PRODUCTO=@PRODUCTO" & vbCrLf
sConsulta = sConsulta & "               ,@PAGINAORIGEN=@PAGINAORIGEN" & vbCrLf
sConsulta = sConsulta & "               ,@POSTBACK=@POSTBACK" & vbCrLf
sConsulta = sConsulta & "               ,@NAVEGADOR=@NAVEGADOR" & vbCrLf
sConsulta = sConsulta & "               ,@IDREGISTRO=@IDREGISTRO" & vbCrLf
sConsulta = sConsulta & "               ,@QUERYSTRING=@QUERYSTRING" & vbCrLf
sConsulta = sConsulta & "               ,@OUTPUT=@OUTPUT OUTPUT;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           RETURN" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @OUTPUT = 1;" & vbCrLf
sConsulta = sConsulta & "           RETURN" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_04_23_05_A32100_04_23_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32100_4_Storeds_006

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.23.06'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32100_04_23_05_A32100_04_23_06 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32100_04_23_05_A32100_04_23_06 = False
End Function


Private Sub V_32100_4_Storeds_006()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GETPARAMETROS_PASSWORDPOLICY]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GETPARAMETROS_PASSWORDPOLICY]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GETPARAMETROS_PASSWORDPOLICY]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   SELECT HISTORICO_PWD,EDAD_MAX_PWD,EDAD_MIN_PWD,COMPLEJIDAD_PWD,MIN_SIZE_PWD,ENCRYPT_ONE_WAY" & vbCrLf
sConsulta = sConsulta & "   FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_04_23_06_A32100_04_23_07() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32100_4_Tablas_007

    sConsulta = "UPDATE VERSION SET NUM ='3.00.23.07'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32100_04_23_06_A32100_04_23_07 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32100_04_23_06_A32100_04_23_07 = False
End Function

Private Sub V_32100_4_Tablas_007()
    Dim sConsulta As String
    sConsulta = "ALTER TABLE ADM ALTER COLUMN PWD NVARCHAR(128) NOT NULL" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

End Sub

