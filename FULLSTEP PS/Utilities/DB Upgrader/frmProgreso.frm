VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmProgreso 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FSPS Actualizador"
   ClientHeight    =   1965
   ClientLeft      =   1275
   ClientTop       =   2730
   ClientWidth     =   5115
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1965
   ScaleWidth      =   5115
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraProgreso 
      Caption         =   "Progreso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4815
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   435
         Left            =   120
         TabIndex        =   1
         Top             =   1080
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   767
         _Version        =   393216
         Appearance      =   1
         Max             =   1000
      End
      Begin VB.Label lblDetalle 
         Appearance      =   0  'Flat
         Caption         =   "De 1.0 a 1.1 ..."
         ForeColor       =   &H00800000&
         Height          =   675
         Left            =   120
         TabIndex        =   2
         Top             =   300
         Width           =   4575
      End
   End
End
Attribute VB_Name = "frmProgreso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
