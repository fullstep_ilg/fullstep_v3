Attribute VB_Name = "bas_V_32200_2"
Public Function CodigoDeActualizacion32200_01_28_00_A32200_01_28_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32200_2_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.28.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32200_01_28_00_A32200_01_28_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32200_01_28_00_A32200_01_28_01 = False
End Function

Private Sub V_32200_2_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_DATOS_PORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_DATOS_PORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_DATOS_PORTAL]" & vbCrLf
sConsulta = sConsulta & "@PORTAL VARCHAR(20) " & vbCrLf
sConsulta = sConsulta & " AS" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " BEGIN     " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON; " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "       SELECT CODIGO, REMITENTE, URL FROM PORTAL WITH (NOLOCK) WHERE CODIGO =  @PORTAL" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
sConsulta = sConsulta & "@CIA VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
sConsulta = sConsulta & "              CIAS.USUPPAL AS USUPPAL,CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
sConsulta = sConsulta & "              USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
sConsulta = sConsulta & "              USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "              USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "              USU.DATEFMT DATEFMT,USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "              IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
sConsulta = sConsulta & "              USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA,MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU," & vbCrLf
sConsulta = sConsulta & "              CIAS.DEN AS CIADEN,CIAS.NIF,IDI.LANGUAGETAG,USU.NIF USUNIF,ISNULL(CON.ID,0) IDCONTACTO" & vbCrLf
sConsulta = sConsulta & "           FROM USU WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN USU_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND USU.CIA=USU_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.CIA=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN CIAS_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.CIA=CIAS_PORT.CIA AND CIAS_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON CIAS.MON=MON.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON MON2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.MON=MON2.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IDI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'PROVE PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON CIAS.COD=PROVE.FSP_COD" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN ' + @FSGS + 'CON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.ID=CON.ID_PORT AND PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "           WHERE USU.COD=''' + @COD + ''' AND CIAS.COD=''' + @CIA + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32200_01_28_01_A32200_01_28_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32200_2_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.28.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32200_01_28_01_A32200_01_28_02 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32200_01_28_01_A32200_01_28_02 = False
End Function

Private Sub V_32200_2_Storeds_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
sConsulta = sConsulta & "@IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "@CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "@FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "@NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "@PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMFACTURA VARCHAR(20) = NULL," & vbCrLf
sConsulta = sConsulta & "@NOM_PORTAL NVARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO,@NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                                                                  @NUMFACTURA=@NUMFACTURA,@NOM_PORTAL=@NOM_PORTAL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT,@NUMPEDIDO INT,@NUMORDEN INT,@NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME," & vbCrLf
sConsulta = sConsulta & "@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50),@PARTIDASPRES NVARCHAR(MAX)," & vbCrLf
sConsulta = sConsulta & "@NUMFACTURA VARCHAR(20),@NOM_PORTAL NVARCHAR(50)'," & vbCrLf
sConsulta = sConsulta & "@IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "@PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "@ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "@NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "@NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "@NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "@FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "@FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "@NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "@NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "@ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "@CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "@CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "@PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "@NUMFACTURA=@NUMFACTURA," & vbCrLf
sConsulta = sConsulta & "@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub
