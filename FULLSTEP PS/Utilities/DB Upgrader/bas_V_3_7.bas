Attribute VB_Name = "bas_V_3_7"
Public Function CodigoDeActualizacion3_3_3700_A3_3_3701() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_7_Tablas_001
    
    frmProgreso.lblDetalle = "Cambios en triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_7_Triggers_001
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_7_Datos_001
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_7_Storeds_001
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.37.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion3_3_3700_A3_3_3701 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion3_3_3700_A3_3_3701 = False
End Function

Private Sub V_3_7_Datos_001()
Dim sConsulta As String
sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (10,'Login_Usu')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (20,'Cia_Sel')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (30,'Cia_Anyadir')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (40,'Cia_Eliminar')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (50,'Cia_Modif_DatosGen')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (60,'Cia_Modif_Acceso')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (70,'Cia_Enlazar_GS')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (80,'Cia_Desenlazar_GS')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (90,'Cia_Modif_Cod')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (100,'Cia_Modif_Activ')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (110,'Cia_Modif_Obs')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (120,'Cia_Archivo_Anyadir')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (130,'Cia_Archivo_Eliminar')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (140,'Cia_Archivo_Modif')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (150,'Cia_Archivo_GuardarEnDisco')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (160,'Cia_Archivo_Abrir')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (170,'Usu_Anyadir')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (180,'Usu_Eliminar')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (190,'Usu_Modif_Datos')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (200,'Usu_Modif_Acceso')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (210,'Usu_Modif_Principal')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (220,'Activ_Sel_Cia')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (230,'Activ_Confirmar')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (240,'Activ_Deshacer')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (250,'RegMail_Buscar_Notif_Cia')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (260,'RegMail_Ver_Notif_Cia')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (270,'RegMail_Reenviar_Notif_Cia')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (280,'Param_Acceso')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (290,'Param_Modif')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC (ACC,CODIGO) VALUES (300,'Cambio_Pwd')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (10,'SPA','Login usuario')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (20,'SPA','Compa�ias: Seleccionar una compa��a')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (30,'SPA','Compa�ias: A�adir compa��a')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (40,'SPA','Compa�ias: Eliminar compa��a')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (50,'SPA','Compa�ias: Modificaci�n datos generales')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (60,'SPA','Compa�ias: Modificaci�n acceso')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (70,'SPA','Compa�ias: Enlazar proveedor GS')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (80,'SPA','Compa�ias: Desenlazar proveedor GS')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (90,'SPA','Compa�ias: Cambio c�digo')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (100,'SPA','Compa�ias: Modificar actividades')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (110,'SPA','Compa�ias: Modificar observaciones')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (120,'SPA','Compa�ias: Adjuntar archivo')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (130,'SPA','Compa�ias: Eliminar archivo')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (140,'SPA','Compa�ias: Modificar archivo')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (150,'SPA','Compa�ias: Guardar archivo en disco')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (160,'SPA','Compa�ias: Abrir archivo')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (170,'SPA','Usuarios: A�adir usuario')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (180,'SPA','Usuarios: Eliminar usuario')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (190,'SPA','Usuarios: Modificar datos usuario')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (200,'SPA','Usuarios: Modificar acceso usuario')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (210,'SPA','Usuarios: Modificar usuario principal')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (220,'SPA','Solicitudes de cambio de actividad: Seleccionar una compa�ia')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (230,'SPA','Solicitudes de cambio de actividad: Confirmar')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (240,'SPA','Solicitudes de cambio de actividad: Deshacer')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (250,'SPA','Registro de comunicaciones: Buscar notificaciones de una compa��a')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (260,'SPA','Registro de comunicaciones: Ver notificaci�n')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (270,'SPA','Registro de comunicaciones: Reenviar notificaci�n')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (280,'SPA','Par�metros: Acceso a par�metros')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (290,'SPA','Par�metros: Modificaci�n de par�metros')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO LOG_ACC_DEN (ACC,IDIOMA, DEN) VALUES (300,'SPA','Cambio de constrase�a')" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub
Private Sub V_3_7_Tablas_001()
Dim sConsulta As String

sConsulta = "DECLARE @jobId binary(16)" & vbCrLf
sConsulta = sConsulta & "SELECT @jobId = job_id FROM msdb.dbo.sysjobs WHERE (name = N'JOBACTPROVE_" & gBD & "')" & vbCrLf
sConsulta = sConsulta & "IF (@jobId IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXEC msdb.dbo.sp_delete_job @jobId" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARGEN_GEST DROP CONSTRAINT DF_PARGEN_GEST_PROV_REINTENTOS" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARGEN_GEST DROP CONSTRAINT DF_PARGEN_GEST_PROV_CICLOS" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARGEN_GEST DROP CONSTRAINT DF_PARGEN_GEST_PROV_PASADAS" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * From SYS.OBJECTS WHERE NAME ='DF_PARGEN_GEST_PROV_FECULT')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP CONSTRAINT DF_PARGEN_GEST_PROV_FECULT" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * From SYS.OBJECTS WHERE NAME ='DF_PARGEN_GEST_PROV_PERIODICIDAD')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP CONSTRAINT DF_PARGEN_GEST_PROV_PERIODICIDAD" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * From SYS.OBJECTS WHERE NAME ='DF_PARGEN_GEST_PROv_UNAHORA_1')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP CONSTRAINT DF_PARGEN_GEST_PROv_UNAHORA_1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_REINTENTOS' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_REINTENTOS" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_CICLOS' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_CICLOS" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_PASADAS' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_PASADAS" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_FECULT' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_FECULT" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_PERIODICIDAD' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_PERIODICIDAD" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_HORA_INICIO' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_HORA_INICIO" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_HORA_FIN' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_HORA_FIN" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_CADAMIN' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_CADAMIN" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PROV_UNA_HORA' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST DROP COLUMN PROV_UNA_HORA" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[LOG_ACC](" & vbCrLf
sConsulta = sConsulta & "[ACC] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "[CODIGO] [nvarchar](50) NOT NULL," & vbCrLf
sConsulta = sConsulta & "[FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_LOG_ACC] PRIMARY KEY CLUSTERED" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "[ACC] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[LOG_ACC_DEN](" & vbCrLf
sConsulta = sConsulta & "[ACC] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "[IDIOMA] [varchar](3) NOT NULL," & vbCrLf
sConsulta = sConsulta & "[DEN] [nvarchar](255) NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_LOG_ACC_DEN] PRIMARY KEY CLUSTERED" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "[ACC] ASC," & vbCrLf
sConsulta = sConsulta & "[IDIOMA] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[LOG](" & vbCrLf
sConsulta = sConsulta & "[ID] [int] IDENTITY(1,1) NOT NULL," & vbCrLf
sConsulta = sConsulta & "[USU] [varchar](20) NOT NULL," & vbCrLf
sConsulta = sConsulta & "[ACC] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "[DAT_SPA] [nvarchar](2000) NULL," & vbCrLf
sConsulta = sConsulta & "[FEC] [datetime] NOT NULL," & vbCrLf
sConsulta = sConsulta & "[CIA] [int] NULL," & vbCrLf
sConsulta = sConsulta & "[REGMAIL] [int] NULL," & vbCrLf
sConsulta = sConsulta & "[DAT_ENG] [nvarchar](2000) NULL," & vbCrLf
sConsulta = sConsulta & "[DAT_FRA] [nvarchar](2000) NULL," & vbCrLf
sConsulta = sConsulta & "[DAT_GER] [nvarchar](2000) NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_LOG] PRIMARY KEY CLUSTERED" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "[ID] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PM_VINCULADA' AND Object_ID = Object_ID(N'CIAS'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS ADD PM_VINCULADA  int NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_3_7_Triggers_001()
Dim sConsulta As String

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[USU_TG_UPD]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[USU_TG_UPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[USU_TG_UPD] ON [dbo].[USU]" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@CIA INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_USU_Upd CURSOR FOR SELECT ID,FCEST,CIA FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_USU_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET FECLASTUPD=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID AND CIA=@CIA)" & vbCrLf
sConsulta = sConsulta & "IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET FECACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @ESTOLD=3 AND @EST<>3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET FECDESACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_USU_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_USU_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualiza OnLine Los Contactos de GS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100),@PORT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_NIF VARCHAR(30),@IDI VARCHAR(50),@TIPOEMAIL SMALLINT,@PRINCIPAL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1), @DECIMALFMT CHAR(1), @PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT CIA, ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @CIA,@ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ENLAZADA =(SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "                       FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_PORT --Debe estar Fpest=3, si no luego la select q dice cual actualizar en GS devuelve, nada." & vbCrLf
sConsulta = sConsulta & "ON INSERTED.ID=USU_PORT.USU" & vbCrLf
sConsulta = sConsulta & "                           AND INSERTED.CIA=USU_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "AND USU_PORT.FPEST=3" & vbCrLf
sConsulta = sConsulta & "AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_CIAS --En cod_prove_cia el codigo proveedor gs" & vbCrLf
sConsulta = sConsulta & "ON INSERTED.CIA=REL_CIAS.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "AND REL_CIAS.EST=3" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS --La q tiene los datos del servidor Fsgs" & vbCrLf
sConsulta = sConsulta & "ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "AND CIAS.FCEST=3" & vbCrLf
sConsulta = sConsulta & "WHERE INSERTED.ID=@ID" & vbCrLf
sConsulta = sConsulta & "AND INSERTED.CIA=@CIA) " & vbCrLf
sConsulta = sConsulta & "   IF @ENLAZADA >0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @CIA_COMP=CIA_COMP,@COD_PROVE_CIA=COD_PROVE_CIA FROM REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'FSGS_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF," & vbCrLf
sConsulta = sConsulta & "                       @PRINCIPAL=@PRINCIPAL'      " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ID_P INT, @IDI VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                   @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "                   @DECIMALFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "                   @PRECISIONFMT TINYINT," & vbCrLf
sConsulta = sConsulta & "                   @DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                   @USU_NIF VARCHAR(30)," & vbCrLf
sConsulta = sConsulta & "                   @PRINCIPAL TINYINT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @PRINCIPAL = NULL" & vbCrLf
sConsulta = sConsulta & "       SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "       @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=USU.NIF," & vbCrLf
sConsulta = sConsulta & "       @PRINCIPAL=CASE WHEN CIAS.ID IS NOT NULL THEN 1 ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CIAS WITH(NOLOCK) ON CIAS.ID=USU.CIA AND CIAS.USUPPAL=USU.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA AND USU.ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID," & vbCrLf
sConsulta = sConsulta & "                           @IDI = @IDI," & vbCrLf
sConsulta = sConsulta & "                           @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "                           @DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "                           @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF," & vbCrLf
sConsulta = sConsulta & "                           @PRINCIPAL=@PRINCIPAL                                   " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C1 INTO @CIA,@ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[USU_PORT_TG_UPD]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[USU_PORT_TG_UPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[USU_PORT_TG_UPD] ON [dbo].[USU_PORT]" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@USU INT,@PORT INT,@EST INT,@ESTOLD INT,@ID INT,@USU_CIA INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE CURTG_USU_PORT_UPD CURSOR LOCAL FOR SELECT CIA,USU,PORT,FPEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN CURTG_USU_PORT_UPD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CURTG_USU_PORT_UPD INTO @CIA,@USU,@PORT,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_PORT SET FECLASTUPD=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "SET @ESTOLD=(SELECT FPEST FROM DELETED  WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT)" & vbCrLf
sConsulta = sConsulta & "IF @ESTOLD<>3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_PORT SET FECACTFP=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @ESTOLD=3 AND @EST<> 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_PORT SET FECDESACTFP=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CURTG_USU_PORT_UPD INTO @CIA,@USU,@PORT,@EST" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE CURTG_USU_PORT_UPD" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURTG_USU_PORT_UPD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZA ONLINE LOS CONTACTOS DE GS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_NIF VARCHAR(30),@IDI VARCHAR(50),@TIPOEMAIL SMALLINT,@PRINCIPAL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1), @DECIMALFMT CHAR(1), @PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT USU, CIA FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ID, @CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ENLAZADA =(SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "FROM USU" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSERTED --Debe estar Fpest=3, si no luego la select q dice cual actualizar en GS devuelve, nada." & vbCrLf
sConsulta = sConsulta & "                       ON USU.ID=INSERTED.USU" & vbCrLf
sConsulta = sConsulta & "AND USU.CIA=INSERTED.CIA" & vbCrLf
sConsulta = sConsulta & "AND (INSERTED.FPEST=3)" & vbCrLf
sConsulta = sConsulta & "AND INSERTED.PORT=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_CIAS --En cod_prove_cia el codigo proveedor gs" & vbCrLf
sConsulta = sConsulta & "ON USU.CIA=REL_CIAS.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "AND REL_CIAS.EST=3" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS --La q tiene los datos del servidor Fsgs" & vbCrLf
sConsulta = sConsulta & "ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "AND CIAS.FCEST=3" & vbCrLf
sConsulta = sConsulta & "WHERE USU.ID=INSERTED.USU" & vbCrLf
sConsulta = sConsulta & "AND USU.CIA=INSERTED.CIA)" & vbCrLf
sConsulta = sConsulta & "   IF @ENLAZADA > 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@CIA,@ID,'U',1,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT @CIA_COMP=CIA_COMP,@COD_PROVE_CIA=COD_PROVE_CIA FROM REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'FSGS_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI =@IDI," & vbCrLf
sConsulta = sConsulta & "                       @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF," & vbCrLf
sConsulta = sConsulta & "                       @PRINCIPAL=@PRINCIPAL'      " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ID_P INT, @IDI VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                   @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "                   @DECIMALFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "                   @PRECISIONFMT TINYINT," & vbCrLf
sConsulta = sConsulta & "                   @DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                   @USU_NIF VARCHAR(30)," & vbCrLf
sConsulta = sConsulta & "                   @PRINCIPAL TINYINT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "       SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "       SET @PRINCIPAL=NULL" & vbCrLf
sConsulta = sConsulta & "       SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "       @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=USU.NIF," & vbCrLf
sConsulta = sConsulta & "       @PRINCIPAL=CASE WHEN CIAS.ID IS NOT NULL THEN 1 ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CIAS WITH(NOLOCK) ON CIAS.ID=USU.CIA AND CIAS.USUPPAL=USU.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA AND USU.ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID," & vbCrLf
sConsulta = sConsulta & "                           @IDI = @IDI," & vbCrLf
sConsulta = sConsulta & "                           @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "                           @DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "                           @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF," & vbCrLf
sConsulta = sConsulta & "                           @PRINCIPAL=@PRINCIPAL                               " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C1 INTO @ID, @CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[USU_PORT_TG_DEL]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[USU_PORT_TG_DEL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[USU_PORT_TG_DEL] ON [dbo].[USU_PORT]" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLAZADA INT,@CIA INT,@ID INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZA ONLINE LOS CONTACTOS DE GS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT USU, CIA FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ID, @CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ENLAZADA =(SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "                   FROM USU" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DELETED --Debe estar Fpest=3 o Fpest=2" & vbCrLf
sConsulta = sConsulta & "                       ON USU.ID=DELETED.USU" & vbCrLf
sConsulta = sConsulta & "                       AND USU.CIA=DELETED.CIA" & vbCrLf
sConsulta = sConsulta & "                       AND (DELETED.FPEST=3 OR DELETED.FPEST=2)" & vbCrLf
sConsulta = sConsulta & "                       AND DELETED.PORT=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_CIAS --En cod_prove_cia el codigo proveedor gs" & vbCrLf
sConsulta = sConsulta & "ON USU.CIA=REL_CIAS.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "AND REL_CIAS.EST=3" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS --La q tiene los datos del servidor Fsgs" & vbCrLf
sConsulta = sConsulta & "                       ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "AND CIAS.FCEST=3" & vbCrLf
sConsulta = sConsulta & "WHERE USU.ID=@ID" & vbCrLf
sConsulta = sConsulta & "AND USU.CIA=@CIA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENLAZADA > 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@CIA,@ID,'D',1,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT @CIA_COMP=CIA_COMP,@COD_PROVE_CIA=COD_PROVE_CIA FROM REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING='EXECUTE ' + @SFGS + 'FSGS_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@ID" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C1 INTO @ID, @CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[CIAS_TG_UPD]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[CIAS_TG_UPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[CIAS_TG_UPD] ON [dbo].[CIAS]" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@INDICE INT,@ENLAZADA INT, @USUPPAL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_NIF VARCHAR(30),@IDI VARCHAR(50),@TIPOEMAIL SMALLINT,@PRINCIPAL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1), @DECIMALFMT CHAR(1), @PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_Upd CURSOR LOCAL FOR SELECT ID,FCEST,USUPPAL FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST,@USUPPAL" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(FCEST) AND (SELECT UNA_COMPRADORA FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @EST=3" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT count(*) FROM CIAS WHERE CIAS.FCEST=3) > 1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               exec sp_addmessage 50005 , 16, 'Only one buyer is allowed with actual configuration!'" & vbCrLf
sConsulta = sConsulta & "               RAISERROR (50005,16,1)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS SET FECLASTUPD=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "   SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS SET FECACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @ESTOLD=3 AND @EST<>3" & vbCrLf
sConsulta = sConsulta & "           UPDATE CIAS SET FECDESACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @CIA_COMP=CIA_COMP,@COD_PROVE_CIA=COD_PROVE_CIA FROM REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "   SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.',@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(DEN) OR UPDATE(DIR) OR UPDATE(CP) OR UPDATE(POB) OR UPDATE(PROVI) OR UPDATE(NIF) OR UPDATE(PAI) OR UPDATE(MON) OR UPDATE(URLCIA)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @ENLAZADA =(SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN CIAS" & vbCrLf
sConsulta = sConsulta & "                           ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "                           AND CIAS.FCEST=3" & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "                       AND EST=3)" & vbCrLf
sConsulta = sConsulta & "       IF @ENLAZADA >0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           --INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@ID,NULL,'U',0,GETDATE())" & vbCrLf
sConsulta = sConsulta & "           IF @FCEST=3" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SELECT @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "               @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "               @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "               FROM CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "               WHERE CIAS.ID= @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'FSGS_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "END                " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(USUPPAL)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @ENLAZADA =(SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN CIAS" & vbCrLf
sConsulta = sConsulta & "                           ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "                           AND CIAS.FCEST=3" & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "                       AND EST=3)" & vbCrLf
sConsulta = sConsulta & "       IF @ENLAZADA >0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @USUPPAL IS NULL" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE ' + @SFGS + 'CON SET PRINCIPAL=0 WHERE PROVE=@COD_PROVE_CIA'" & vbCrLf
sConsulta = sConsulta & "               EXEC sp_executesql @SQLSTRING,N'@COD_PROVE_CIA VARCHAR(200)',@COD_PROVE_CIA=@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'EXEC ' + @SFGS + 'FSGS_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF," & vbCrLf
sConsulta = sConsulta & "                       @PRINCIPAL=@PRINCIPAL'      " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "               SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                       @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                       @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                       @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                       @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                       @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                       @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                       @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                       @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                       @ID_P INT, @IDI VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                       @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                       @THOUSANFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "                       @DECIMALFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "                       @PRECISIONFMT TINYINT," & vbCrLf
sConsulta = sConsulta & "                       @DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                       @USU_NIF VARCHAR(30)," & vbCrLf
sConsulta = sConsulta & "                       @PRINCIPAL TINYINT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "               SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "               SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "               SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "               SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "               SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRINCIPAL=NULL" & vbCrLf
sConsulta = sConsulta & "               SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "               @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "               @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=USU.NIF," & vbCrLf
sConsulta = sConsulta & "               @PRINCIPAL=CASE WHEN CIAS.ID IS NOT NULL THEN 1 ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "               FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN CIAS WITH(NOLOCK) ON CIAS.ID=USU.CIA AND CIAS.USUPPAL=USU.ID" & vbCrLf
sConsulta = sConsulta & "               WHERE USU_PORT.FPEST=3 AND USU.CIA = @ID AND USU.ID=@USUPPAL" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@USUPPAL," & vbCrLf
sConsulta = sConsulta & "                           @IDI = @IDI," & vbCrLf
sConsulta = sConsulta & "                           @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "                           @DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "                           @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF," & vbCrLf
sConsulta = sConsulta & "                           @PRINCIPAL=@PRINCIPAL   " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST, @USUPPAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Upd" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[CIAS_ESP_TG_UPD]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[CIAS_ESP_TG_UPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[CIAS_ESP_TG_UPD] ON [dbo].[CIAS_ESP]" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500),@SQLSTRING NVARCHAR(1000),@PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT, @COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT CIA, ID, NOM, COM FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @CIA, @ID, @NOM, @COM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @FECACT=GETDATE()" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS_ESP SET FECACT=@FECACT WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ENLAZADA =(SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "                   FROM REL_CIAS" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN CIAS" & vbCrLf
sConsulta = sConsulta & "                   ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "                   AND CIAS.FCEST=3" & vbCrLf
sConsulta = sConsulta & "                   WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
sConsulta = sConsulta & "   IF @ENLAZADA > 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) VALUES  (@INDICE,@CIA,NULL,'U',@ID,2,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT @CIA_COMP=CIA_COMP,@COD_PROVE_CIA=COD_PROVE_CIA FROM REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'EXEC ' + @SFGS + 'FSGS_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "@COM =@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ID" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C1 INTO @CIA, @ID, @NOM, @COM" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[CIAS_ESP_TG_INS]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[CIAS_ESP_TG_INS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[CIAS_ESP_TG_INS] ON [dbo].[CIAS_ESP]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@FECACT DATETIME, @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500),@SQLSTRING NVARCHAR(1000),@PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT, @COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT CIA, ID, NOM, COM  FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @CIA, @ID, @NOM, @COM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @DATASIZE=(SELECT DATALENGTH(DATA) FROM CIAS_ESP WHERE ID=@ID AND CIA=@CIA)" & vbCrLf
sConsulta = sConsulta & "   SET @FECACT=GETDATE()" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS_ESP SET FECACT=@FECACT WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ENLAZADA =(SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "               FROM REL_CIAS" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN CIAS" & vbCrLf
sConsulta = sConsulta & "               ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "               AND CIAS.FCEST=3" & vbCrLf
sConsulta = sConsulta & "               WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
sConsulta = sConsulta & "   IF @ENLAZADA > 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) VALUES  (@INDICE,@CIA,NULL,'I',@ID,2,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT @CIA_COMP=CIA_COMP,@COD_PROVE_CIA=COD_PROVE_CIA FROM REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                       @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "                       @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'FSGS_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ID," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C1 INTO @CIA, @ID, @NOM, @COM" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE c1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[CIAS_ESP_TG_DEL]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[CIAS_ESP_TG_DEL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[CIAS_ESP_TG_DEL] ON [dbo].[CIAS_ESP]" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500),@SQLSTRING NVARCHAR(1000),@PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT, @COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT CIA, ID FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @CIA, @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ENLAZADA =(SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "                   FROM REL_CIAS" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN CIAS" & vbCrLf
sConsulta = sConsulta & "                   ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "                   AND CIAS.FCEST=3" & vbCrLf
sConsulta = sConsulta & "                   WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
sConsulta = sConsulta & "   IF @ENLAZADA > 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) VALUES  (@INDICE,@CIA,NULL,'D',@ID,2,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT @CIA_COMP=CIA_COMP,@COD_PROVE_CIA=COD_PROVE_CIA FROM REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200),@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQLSTRING = 'EXEC ' + @SFGS + 'FSGS_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA,@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "           @ESPEC=@ID" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C1 INTO @CIA, @ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[CIAS_ESP_TG_INSUPD]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[CIAS_ESP_TG_INSUPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub
Private Sub V_3_7_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_JOB_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_JOB_ACTUALIZAR_PROV_GS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PROV_REINTENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_MVTOS_PROV]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_MVTOS_PROV]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_COMPANIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_REGISTRAR_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_REGISTRAR_USUARIO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_ELIMINAR_COMPANIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_ELIMINAR_COMPANIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_ELIMINAR_COMPANIA] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = @LNKSERVER + 'FSGS_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL_ADJUN WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_PWD WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM FSP_CONF_VISOR_RECEPCIONES WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_REGISTRAR_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_REGISTRAR_USUARIO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_REGISTRAR_USUARIO](" & vbCrLf
sConsulta = sConsulta & "@CIA INT," & vbCrLf
sConsulta = sConsulta & "@IDPORTAL INT," & vbCrLf
sConsulta = sConsulta & "@COD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@NIF NVARCHAR(30)," & vbCrLf
sConsulta = sConsulta & "@NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "@FCEST TINYINT," & vbCrLf
sConsulta = sConsulta & "@FPEST TINYINT," & vbCrLf
sConsulta = sConsulta & "@ORIGINS VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@PRINCIPAL BIT," & vbCrLf
sConsulta = sConsulta & "@PWD VARCHAR(128)=NULL," & vbCrLf
sConsulta = sConsulta & "@FECHA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "@SALT VARCHAR(512)=NULL" & vbCrLf
sConsulta = sConsulta & ")AS" & vbCrLf
sConsulta = sConsulta & "IF @FECHA=NULL" & vbCrLf
sConsulta = sConsulta & "SET @FECHA=GETDATE()" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIDI INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU=ISNULL(MAX(ID),0) + 1" & vbCrLf
sConsulta = sConsulta & "FROM USU" & vbCrLf
sConsulta = sConsulta & "WHERE CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIDI = ID" & vbCrLf
sConsulta = sConsulta & "FROM IDI WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE COD = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PWD IS NULL" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU (CIA,PWD,FECPWD,ID,COD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
sConsulta = sConsulta & "FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL,[PWD_HASH],SALT,NIF)" & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA,NULL,@FECHA,@IDUSU,@COD,@APE,@NOM,@DEP,@CAR,@TFNO,@TFNO2," & vbCrLf
sConsulta = sConsulta & "@FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL,NEWID(),@SALT,@NIF)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU (CIA,PWD,FECPWD,ID,COD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
sConsulta = sConsulta & "FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL,[PWD_HASH],SALT,NIF)" & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA,@PWD,@FECHA,@IDUSU,@COD,@APE,@NOM,@DEP,@CAR,@TFNO,@TFNO2," & vbCrLf
sConsulta = sConsulta & "@FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL,NULL,@SALT,@NIF)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU_PORT (CIA,USU,PORT,FPEST,ORIGINS)" & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA,@IDUSU,@IDPORTAL,@FPEST,@ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRINCIPAL = 1" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL=@IDUSU WHERE ID=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_REGISTRAR_ACCION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_REGISTRAR_ACCION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_REGISTRAR_ACCION]" & vbCrLf
sConsulta = sConsulta & "   @USU VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @ACC INT," & vbCrLf
sConsulta = sConsulta & "   @DAT_SPA NVARCHAR(2000)," & vbCrLf
sConsulta = sConsulta & "   @DAT_ENG NVARCHAR(2000)," & vbCrLf
sConsulta = sConsulta & "   @DAT_FRA NVARCHAR(2000)," & vbCrLf
sConsulta = sConsulta & "   @DAT_GER NVARCHAR(2000)," & vbCrLf
sConsulta = sConsulta & "   @FEC DATETIME," & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @REGMAIL INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG (USU,ACC,DAT_SPA,DAT_ENG,DAT_FRA,DAT_GER,FEC,CIA,REGMAIL) VALUES (@USU,@ACC,@DAT_SPA,@DAT_ENG,@DAT_FRA,@DAT_GER,@FEC,CASE WHEN @CIA= 0 THEN NULL ELSE @CIA END, CASE WHEN @REGMAIL=0 THEN NULL ELSE @REGMAIL END)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3701_A3_3_3702() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_7_Tablas_002
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_7_Storeds_002

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.37.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion3_3_3701_A3_3_3702 = True
    Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3701_A3_3_3702 = False
End Function

Private Sub V_3_7_Tablas_002()
    Dim sConsulta As String
sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'FECUSU' AND Object_ID = Object_ID(N'ADM'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ADM DROP COLUMN  FECUSU" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PM_VINCULADA' AND Object_ID = Object_ID(N'CIAS'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS DROP COLUMN  PM_VINCULADA" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'INSTANCIA_VINCULADA' AND Object_ID = Object_ID(N'CIAS'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS ADD INSTANCIA_VINCULADA  INT NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_3_7_Storeds_002()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USU_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[USU_LOGIN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[USU_LOGIN](" & vbCrLf
sConsulta = sConsulta & "@USUCOD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@USUNEW VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@PWDNEW VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@FECPWD VARCHAR(512)=NULL," & vbCrLf
sConsulta = sConsulta & "@SALT VARCHAR(512)=NULL" & vbCrLf
sConsulta = sConsulta & ")AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHA DATETIME" & vbCrLf
sConsulta = sConsulta & "IF @FECPWD=NULL" & vbCrLf
sConsulta = sConsulta & "SET @FECHA=GETDATE()" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @FECHA=CONVERT(DATETIME,@FECPWD,103)" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ADM SET USU=@USUNEW,PWD=@PWDNEW,FECPWD=@FECHA,SALT=@SALT" & vbCrLf
sConsulta = sConsulta & "WHERE USU=@USUCOD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3702_A3_3_3703() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim iRespuesta As Integer
    Dim oADOConnection As ADODB.Connection
    
    On Error GoTo Error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_7_Datos_003 oADOConnection

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.37.03'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"

    bTransaccionEnCurso = False
    
    CodigoDeActualizacion3_3_3702_A3_3_3703 = True
    Exit Function
    
Error:

    If NumTransaccionesAbiertasADO(oADOConnection) > 0 Then
        ExecuteADOSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If
        
    MsgBox Err.Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3702_A3_3_3703 = False
End Function

Private Sub V_3_7_Datos_003(ByVal oADOConnection As ADODB.Connection)
Dim sConsulta As String

sConsulta = "SELECT USU,FECPWD FROM ADM WITH(NOLOCK)"
Set adores = New ADODB.Recordset
adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly

While Not adores.EOF
    sConsulta = "UPDATE ADM SET USU='" & EncriptarAES_319008("", adores("USU"), False, adores("FECPWD"), 1) & "' WHERE USU='" & adores("USU") & "'" & vbCrLf
    
    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = oADOConnection
    adocom.CommandType = adCmdText
    adocom.CommandText = sConsulta
    adocom.Execute
    
    adores.MoveNext
Wend
End Sub
