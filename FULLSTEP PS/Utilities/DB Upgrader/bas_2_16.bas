Attribute VB_Name = "bas_2_16"
Option Explicit

Public Function CodigoDeActualizacion2_15_00_05A2_16_00_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    
    V_2_16_Tablas_0
    
    frmProgreso.lblDetalle = "Cambios en stored procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    
    V_2_16_Storeds_0
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.16.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_15_00_05A2_16_00_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_15_00_05A2_16_00_00 = False
End Function


Private Sub V_2_16_Storeds_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                       FROM USU " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                        WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                       FROM USU " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE @CIA_COMP INT = NULL, @ANYO INT = NULL, @GMN1 VARCHAR(20) = NULL, @PROCE INT = NULL, @PROVE VARCHAR(50) = NULL, @ID INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @ID = ID " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP = @CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   AND ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TOTALADJUNTOS = ISNULL(SUM(DATASIZE),0) " & vbCrLf
sConsulta = sConsulta & "  FROM" & vbCrLf
sConsulta = sConsulta & "    (SELECT ISNULL(SUM(DATASIZE), 0) DATASIZE" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
sConsulta = sConsulta & "       FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
sConsulta = sConsulta & "       FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID) A" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ID, PO.CIA_COMP, PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.CIA_PROVE, PO.USU, PO.FECREC, PO.FECVAL, PO.MON,PO.CAMBIO, PO.OBS, PO.OBSADJUN, PO.NUMOBJ, PO.ADJUN,  @TOTALADJUNTOS TOTALADJUNTOS, @IMPORTEOFERTA IMPORTEOFERTA, U.NOM + ' ' + U.APE NOMUSU, PO.ENVIANDOSE" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN USU U " & vbCrLf
sConsulta = sConsulta & "            ON U.CIA = PO.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "           AND U.COD = PO.USU" & vbCrLf
sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_16_Tablas_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_OFE ADD" & vbCrLf
sConsulta = sConsulta & "   ENVIANDOSE tinyint NOT NULL CONSTRAINT DF_PROCE_OFE_ENVIANDOSE DEFAULT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
