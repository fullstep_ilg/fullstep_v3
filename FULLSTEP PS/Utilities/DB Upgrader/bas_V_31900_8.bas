Attribute VB_Name = "bas_V_31900_8"
Option Explicit

Public Function CodigoDeActualizacion31900_04_17_12A31900_04_18_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_000
    
     frmProgreso.lblDetalle = "Cambios en Triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Triggers_000
            
    frmProgreso.lblDetalle = "Cambios en datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Datos_000
        
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_12A31900_04_18_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_12A31900_04_18_00 = False
End Function

Private Sub V_31900_8_Storeds_000()


Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PROCE_OFE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PROCE_OFE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PROCE_OFE] @CIA INT, @CIA_PROVE INT, @FECOFEDESDE DATETIME = NULL, @FECOFEHASTA DATETIME = NULL, @ANYO INT = NULL, @GMN1 VARCHAR(50) = NULL, @PROCE INT = NULL, @DEN_PROCE VARCHAR(200) = NULL, @COD_ART VARCHAR(50) = NULL, @DEN_ART VARCHAR(200) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--''' <summary>" & vbCrLf
sConsulta = sConsulta & "--''' Devuelve los datos para el buscador de ofertas historicas." & vbCrLf
sConsulta = sConsulta & "--''' </summary>" & vbCrLf
sConsulta = sConsulta & "--''' <returns>Todas las ofertas que cumplen con los filtros</returns>" & vbCrLf
sConsulta = sConsulta & "--''' <remarks>Llamada desde:PMPortalDatabaseServer: root.vb->OfertasHistoricas_Get; Tiempo m�ximo:0,1</remarks>" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "SELECT @PROVE=COD FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSP_GET_PROCE_OFE @PROVE=@PROVE, @FECOFEDESDE=@FECOFEDESDE, @FECOFEHASTA=@FECOFEHASTA, @ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE, @DEN_PROCE=@DEN_PROCE, @COD_ART=@COD_ART, @DEN_ART=@DEN_ART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50), @FECOFEDESDE DATETIME, @FECOFEHASTA DATETIME, @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @DEN_PROCE VARCHAR(200), @COD_ART VARCHAR(50), @DEN_ART VARCHAR(200)', @PROVE=@PROVE, @FECOFEDESDE=@FECOFEDESDE, @FECOFEHASTA=@FECOFEHASTA, @ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE, @DEN_PROCE=@DEN_PROCE, @COD_ART=@COD_ART, @DEN_ART=@DEN_ART" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PROCE_OFE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PROCE_OFE_ADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PROCE_OFE_ADJUN] @CIA INT, @CIA_PROVE INT, @ANYO INT=NULL, @GMN1 VARCHAR(3)=NULL, @PROCE INT=NULL, @NUMOFE INT=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--''' <summary>" & vbCrLf
sConsulta = sConsulta & "--''' Devuelve los datos para el buscador de ofertas historicas." & vbCrLf
sConsulta = sConsulta & "--''' </summary>" & vbCrLf
sConsulta = sConsulta & "--''' <returns>Los datos de los adjuntos apuntados por los filtros</returns>" & vbCrLf
sConsulta = sConsulta & "--''' <remarks>Llamada desde:PMPortalDatabaseServer: root.vb->OfertaHistoricaAdjun_Get; Tiempo m�ximo:0,1</remarks>" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "SELECT @PROVE=COD FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "print @FSGS" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSP_GET_PROCE_OFE_ADJUN @PROVE=@PROVE, @ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE, @NUMOFE=@NUMOFE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50), @ANYO INT, @GMN1 VARCHAR(3), @PROCE INT, @NUMOFE INT', @PROVE=@PROVE, @ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE, @NUMOFE=@NUMOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO] @ID INT, @COSTEDESC int = null AS" & vbCrLf
sConsulta = sConsulta & "--''' <summary>" & vbCrLf
sConsulta = sConsulta & "--'''  Devuelve los importes parciales de los grupos de la oferta" & vbCrLf
sConsulta = sConsulta & "--''' </summary>" & vbCrLf
sConsulta = sConsulta & "--''' <returns> </returns>" & vbCrLf
sConsulta = sConsulta & "--''' <revisado>JVS 17/06/2011</revisado>" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT GRUPO, ATRIB_ID COSTEDESC, IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & " FROM OFE_GR_COSTESDESC WITH (NOLOCK)        " & vbCrLf
sConsulta = sConsulta & "WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  AND ATRIB_ID = CASE WHEN @COSTEDESC IS NULL THEN ATRIB_ID ELSE @COSTEDESC END" & vbCrLf
sConsulta = sConsulta & "ORDER BY GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_31900_8_Triggers_000()
Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[UPLOADEXT_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[UPLOADEXT_TG_INS]"
ExecuteSQL gRDOCon, sConsulta

 sConsulta = "CREATE TRIGGER [dbo].[UPLOADEXT_TG_INS] ON [dbo].[UPLOADEXT]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE UPLOADEXT SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM UPLOADEXT  IO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON IO.EXT = I.EXT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_8_Tablas_000()
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[UPLOADEXT](" & vbCrLf
sConsulta = sConsulta & "   [EXT] [nvarchar](10) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [BLOQUEADA] [tinyint] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UPLOADEXT] ADD  CONSTRAINT [DF_UPLOADEXT_BLOQUEADA]  DEFAULT ((0)) FOR [BLOQUEADA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD " & vbCrLf
sConsulta = sConsulta & "[IMPORTE_ALBARAN_VISIBLE] TINYINT NOT NULL" & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [DF_FSP_CONF_VISOR_RECEPCIONES_IMPORTE_ALBARAN_VISIBLE]  DEFAULT 1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD " & vbCrLf
sConsulta = sConsulta & "[IMPORTE_ALBARAN_WIDTH] FLOAT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
End Sub

Private Sub V_31900_8_Datos_000()
    Dim sConsulta As String
    
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('txt',0) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('doc',0) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('docx',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('xls',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('xlsx',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('mdb',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('accdb',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('ppt',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('pptx',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('pps',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('ppsx',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('dif',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('msg',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('odt',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('ods',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('odp',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('odg',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('odg',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('odf',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('odb',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('odm',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('pdf',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('wri',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('log',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('xps',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('bmp',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('gif',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('dib',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('jpg',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('png',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('tga',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('tif',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('tiff',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('pcx',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('pic',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('emf',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('ico',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('cdr',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('cdt',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('cgm',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('cpt',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('psd',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('psp',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('fh11',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('wav',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('mid',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('midi',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('mp3',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('ra',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('ash',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('aif',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('cda',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('snd',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('voc',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('amf',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('avi',0) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('mov',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('mpeg',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('mpg',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('divx',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('dvd',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('wmv',0) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('rpm',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('wob',0) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('qtl',0) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('zip',0) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = sConsulta & "INSERT INTO UPLOADEXT (EXT,BLOQUEADA) VALUES('rar',0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    
End Sub
    
Public Function CodigoDeActualizacion31900_04_18_00A31900_04_18_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_001
        
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_18_00A31900_04_18_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_18_00A31900_04_18_01 = False
End Function

Private Sub V_31900_8_Tablas_001()

Dim sConsulta As String

sConsulta = "IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'AUTOFACTURA' and Object_ID = Object_ID(N'SESION'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SESION ADD AUTOFACTURA TINYINT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS(SELECT name FROM sys.types WHERE name='Tabla_FSAL_IDS')" & vbCrLf
sConsulta = sConsulta & "DROP TYPE [Tabla_FSAL_IDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TYPE [dbo].[Tabla_FSAL_IDS] AS TABLE(" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT column_name " & vbCrLf
sConsulta = sConsulta & "FROM INFORMATION_SCHEMA.COLUMNS AS C1 " & vbCrLf
sConsulta = sConsulta & "WHERE C1.column_name = 'LOGNOELIMINAR' AND C1.table_name = 'PARGEN_INTERNO')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_INTERNO ADD LOGNOELIMINAR TINYINT NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_LOGNOELIMINAR] DEFAULT 0 " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_8_Storeds_001()

Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_FSAL_REGISTRAR_ACCESO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_FSAL_REGISTRAR_ACCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_FSAL_REGISTRAR_ACCESO] @TIPO INT ,@PAGINA NVARCHAR(2000) ,@FPETICION varchar(50), @FECHA varchar(50),@IDSESION VARCHAR(40),@PRODUCTO NVARCHAR(20),@PAGINAORIGEN NVARCHAR(2000),@IP VARCHAR(50),@NAVEGADOR NVARCHAR(200) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IPDIR VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIACODGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIACOD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USUCOD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FCEST=3 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @FSGS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @IDSESION IS NULL OR @IDSESION=''" & vbCrLf
sConsulta = sConsulta & "           BEGIN " & vbCrLf
sConsulta = sConsulta & "           SET @USUCOD ='Registro'" & vbCrLf
sConsulta = sConsulta & "           SET @IPDIR =@IP" & vbCrLf
sConsulta = sConsulta & "           SET @CIACOD='Registro'" & vbCrLf
sConsulta = sConsulta & "           SET @CIACODGS ='Registro'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           SELECT TOP 1 @USUCOD= ISNULL(USUCOD,'USUCOD'),@IPDIR=ISNULL(IPDIR,''),@CIACODGS=ISNULL(CIACODGS,''),@CIACOD=ISNULL(CIACOD,'') FROM SESION WITH (NOLOCK)WHERE ACTIVA=1 AND ID=@IDSESION" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_REGISTRAR_ACCESO @TIPO=@TIPO,@PAGINA=@PAGINA,@FPETICION=@FPETICION,@FECHA=@FECHA,@IDSESION=@IDSESION,@USUCOD=@USUCOD,@IPDIR=@IPDIR,@CIACODGS=@CIACODGS,@CIACOD=@CIACOD,@PRODUCTO=@PRODUCTO,@PAGINAORIGEN=@PAGINAORIGEN,@NAVEGADOR=@NAVEGADOR '" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@TIPO  INT, @PAGINA NVARCHAR(2000),@FPETICION VARCHAR(50),@FECHA VARCHAR(50), @IDSESION VARCHAR(40) ,@USUCOD VARCHAR(20) ,@IPDIR VARCHAR(50) ,@CIACODGS VARCHAR(50) ,@CIACOD VARCHAR(50) ,@PRODUCTO NVARCHAR(20) ,@PAGINAORIGEN NVARCHAR(2000) ,@NAVEGADOR NVARCHAR(200) ', @TIPO=@TIPO,@PAGINA=@PAGINA,@FPETICION=@FPETICION,@FECHA=@FECHA,@IDSESION=@IDSESION,@USUCOD=@USUCOD,@IPDIR=@IPDIR,@CIACODGS=@CIACODGS,@CIACOD=@CIACOD,@PRODUCTO=@PRODUCTO,@PAGINAORIGEN=@PAGINAORIGEN,@NAVEGADOR=@NAVEGADOR" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  " & vbCrLf
sConsulta = sConsulta & "    @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMFACTURA VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO, @NUMPEDIDO=@NUMPEDIDO,@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE,@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP,@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO,@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA,@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE,@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE,@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES,@NUMFACTURA=@NUMFACTURA'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT, @NUMPEDIDO INT,@NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME,@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200),@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME,@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50), @PARTIDASPRES NVARCHAR(MAX), @NUMFACTURA VARCHAR(20)', " & vbCrLf
sConsulta = sConsulta & "                   @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                   @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                   @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                   @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                   @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                   @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                   @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                   @NUMFACTURA = @NUMFACTURA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]  " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT * FROM ' + @FSGS + 'FSAL_ACCESOS WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE' + @FSGS + 'FSAL_ACCESOS SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSTRATADOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSTRATADOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSTRATADOS] @TAB AS dbo.Tabla_FSAL_IDS READONLY,@LOGNOELIMINAR INT, @OK bit  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null  AND FCEST=3" & vbCrLf
sConsulta = sConsulta & "set @SQLSTRING='EXEC ' + @FSGS + 'FSAL_ACCESOSTRATADOS @TAB,@LOGNOELIMINAR,@OK'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING, N'@TAB dbo.Tabla_FSAL_IDS, @LOGNOELIMINAR INT, @OK BIT', @TAB=@TAB, @LOGNOELIMINAR=@LOGNOELIMINAR,@OK=@OK" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_FSAL_REGISTRAR_ACCESO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_FSAL_REGISTRAR_ACCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_FSAL_REGISTRAR_ACCESO] @TIPO INT ,@PAGINA NVARCHAR(2000) ,@FPETICION varchar(50), @FECHA varchar(50),@IDSESION VARCHAR(40),@PRODUCTO NVARCHAR(20),@PAGINAORIGEN NVARCHAR(2000),@IP VARCHAR(50),@NAVEGADOR NVARCHAR(200) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IPDIR VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIACODGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIACOD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USUCOD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FCEST=3 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @FSGS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @IDSESION IS NULL OR @IDSESION=''" & vbCrLf
sConsulta = sConsulta & "           BEGIN " & vbCrLf
sConsulta = sConsulta & "           SET @USUCOD ='Registro'" & vbCrLf
sConsulta = sConsulta & "           SET @IPDIR =@IP" & vbCrLf
sConsulta = sConsulta & "           SET @CIACOD='Registro'" & vbCrLf
sConsulta = sConsulta & "           SET @CIACODGS ='Registro'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           SELECT TOP 1 @USUCOD= ISNULL(USUCOD,'USUCOD'),@IPDIR=ISNULL(IPDIR,''),@CIACODGS=ISNULL(CIACODGS,''),@CIACOD=ISNULL(CIACOD,'') FROM SESION WITH (NOLOCK)WHERE ACTIVA=1 AND ID=@IDSESION" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_REGISTRAR_ACCESO @TIPO=@TIPO,@PAGINA=@PAGINA,@FPETICION=@FPETICION,@FECHA=@FECHA,@IDSESION=@IDSESION,@USUCOD=@USUCOD,@IPDIR=@IPDIR,@CIACODGS=@CIACODGS,@CIACOD=@CIACOD,@PRODUCTO=@PRODUCTO,@PAGINAORIGEN=@PAGINAORIGEN,@NAVEGADOR=@NAVEGADOR '" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@TIPO  INT, @PAGINA NVARCHAR(2000),@FPETICION VARCHAR(50),@FECHA VARCHAR(50), @IDSESION VARCHAR(40) ,@USUCOD VARCHAR(20) ,@IPDIR VARCHAR(50) ,@CIACODGS VARCHAR(50) ,@CIACOD VARCHAR(50) ,@PRODUCTO NVARCHAR(20) ,@PAGINAORIGEN NVARCHAR(2000) ,@NAVEGADOR NVARCHAR(200) ', @TIPO=@TIPO,@PAGINA=@PAGINA,@FPETICION=@FPETICION,@FECHA=@FECHA,@IDSESION=@IDSESION,@USUCOD=@USUCOD,@IPDIR=@IPDIR,@CIACODGS=@CIACODGS,@CIACOD=@CIACOD,@PRODUCTO=@PRODUCTO,@PAGINAORIGEN=@PAGINAORIGEN,@NAVEGADOR=@NAVEGADOR" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub
Public Function CodigoDeActualizacion31900_04_18_01A31900_04_18_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
   
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_18_01A31900_04_18_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_18_01A31900_04_18_02 = False
End Function

Private Sub V_31900_8_Storeds_002()

Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_COMPANIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL_ADJUN WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM FSP_CONF_VISOR_RECEPCIONES WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_02A31900_04_18_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Claves"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Actualizar_Claves oADOConnection
    
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_02A31900_04_18_03 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_02A31900_04_18_03 = False
    Resume Salir
End Function

Private Sub V_31900_8_Actualizar_Claves(ByRef oADOConnection As ADODB.Connection)
    Dim sConsulta As String
    Dim adoCom As ADODB.Connection
    Dim adores As ADODB.Recordset
    Dim oCom As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim sConsultaUpd As String
    Dim sCadena As String
    Dim TextoUpd As String
    Dim Texto As String
    Dim TextoUpd2 As String
    Dim Texto2  As String
    Dim diacrypt As Integer
    
    Set adores = New ADODB.Recordset
    
    sConsulta = "SELECT CIA, ID, PWD, FECPWD FROM USU WITH(NOLOCK)"
    
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
        
    While Not adores.EOF
        Texto = EncriptarAES("", adores("PWD").Value, False, adores("FECPWD").Value, 1)
        TextoUpd = EncriptarAES_319008("", Texto, True, adores("FECPWD").Value, 1)
        
        Set oCom = New ADODB.Command
        Set oCom.ActiveConnection = oADOConnection
        oCom.CommandType = adCmdText
        
        sConsultaUpd = "UPDATE USU SET PWD= ? "
        sConsultaUpd = sConsultaUpd & " WHERE CIA=" & adores("CIA").Value & vbCrLf
        sConsultaUpd = sConsultaUpd & " AND ID=" & adores("ID").Value & vbCrLf
        oCom.CommandText = sConsultaUpd
        
        Set adoParam = oCom.CreateParameter("PWD", adVarChar, adParamInput, 128, TextoUpd)
        oCom.Parameters.Append adoParam
        
        oCom.Execute

        '
        adores.MoveNext
    Wend
           
    adores.Close
    Set adores = Nothing
        
    
    '-----------------------------------
    Set adores = New ADODB.Recordset
    
    sConsulta = "SELECT ID, P_PWD,PWD,FECPWD FROM PARGEN_MAIL WITH(NOLOCK)"
    
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    If Not adores.EOF Then
        If Not IsNull(adores("P_PWD").Value) Then
            Texto = EncriptarAES("PWD", adores("P_PWD").Value, False, DateSerial(1974, 3, 5), 1)
            TextoUpd = EncriptarAES_319008("PWD", Texto, True, DateSerial(1974, 3, 5), 1)
            
            sConsultaUpd = "UPDATE PARGEN_MAIL SET P_PWD= ? " & vbCrLf
            sConsultaUpd = sConsultaUpd & " WHERE ID=" & adores("ID").Value & vbCrLf
                    
            Set oCom = New ADODB.Command
            Set oCom.ActiveConnection = oADOConnection
            oCom.CommandType = adCmdText
            oCom.CommandText = sConsultaUpd
            Set adoParam = oCom.CreateParameter("PWD", adVarChar, adParamInput, 100, TextoUpd)
            oCom.Parameters.Append adoParam
            oCom.Execute
            
        End If
        
        If Not IsNull(adores("PWD").Value) Then
            Texto = EncriptarAES("PWD", adores("PWD").Value, False, adores("FECPWD").Value, 3)
            TextoUpd = EncriptarAES_319008("PWD", Texto, True, DateSerial(1974, 3, 5), 3)
            
            sConsultaUpd = "UPDATE PARGEN_MAIL SET PWD= ? " & vbCrLf
            sConsultaUpd = sConsultaUpd & " WHERE ID=" & adores("ID").Value & vbCrLf
                    
            Set oCom = New ADODB.Command
            Set oCom.ActiveConnection = oADOConnection
            oCom.CommandType = adCmdText
            oCom.CommandText = sConsultaUpd
            Set adoParam = oCom.CreateParameter("PWD", adVarChar, adParamInput, 100, TextoUpd)
            oCom.Parameters.Append adoParam
            oCom.Execute
            
        End If
    End If
    
    adores.Close
    Set adores = Nothing
    
    '-----------------------------------
    Set adores = New ADODB.Recordset
    
    sConsulta = "SELECT USU, PWD, FECPWD FROM ADM WITH(NOLOCK)"
    
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    If Not adores.EOF Then
        If Not IsNull(adores("USU").Value) Then
            Texto = EncriptarAES("", adores("USU").Value, False, adores("FECPWD").Value, 1)
            TextoUpd = EncriptarAES_319008("", Texto, True, adores("FECPWD").Value, 1)

            sConsultaUpd = "UPDATE ADM SET USU= ? " & vbCrLf

            Set oCom = New ADODB.Command
            Set oCom.ActiveConnection = oADOConnection
            oCom.CommandType = adCmdText
            
            oCom.CommandText = sConsultaUpd
            
            Set adoParam = oCom.CreateParameter("USU", adVarChar, adParamInput, 100, TextoUpd)
            oCom.Parameters.Append adoParam
            oCom.Execute
        End If
        If Not IsNull(adores("PWD").Value) Then
            Texto = EncriptarAES("", adores("PWD").Value, False, adores("FECPWD").Value, 1)
            TextoUpd = EncriptarAES_319008("", Texto, True, adores("FECPWD").Value, 1)

            sConsultaUpd = "UPDATE ADM SET PWD= ? " & vbCrLf

            Set oCom = New ADODB.Command
            Set oCom.ActiveConnection = oADOConnection
            oCom.CommandType = adCmdText
            oCom.CommandText = sConsultaUpd
            Set adoParam = oCom.CreateParameter("PWD", adVarChar, adParamInput, 100, TextoUpd)
            oCom.Parameters.Append adoParam
            oCom.Execute
        End If
    End If
    
    
    
    '-----------------------------------
    Set adores = New ADODB.Recordset
    sConsulta = "SELECT USU, PWD, FECPWD FROM OBJSADM WITH(NOLOCK)"
    
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    If Not adores.EOF Then
        If Not IsNull(adores("USU").Value) Then
            Texto = EncriptarAES("", adores("USU").Value, False, adores("FECPWD").Value, 1)
            TextoUpd = EncriptarAES_319008("", Texto, True, adores("FECPWD").Value, 1)

            sConsultaUpd = "UPDATE OBJSADM SET USU= ? " & vbCrLf

            Set oCom = New ADODB.Command
            Set oCom.ActiveConnection = oADOConnection
            oCom.CommandType = adCmdText
            oCom.CommandText = sConsultaUpd
            Set adoParam = oCom.CreateParameter("USU", adVarChar, adParamInput, 100, TextoUpd)
            oCom.Parameters.Append adoParam
            oCom.Execute
        End If
        If Not IsNull(adores("PWD").Value) Then
            Texto = EncriptarAES("", adores("PWD").Value, False, adores("FECPWD").Value, 1)
            TextoUpd = EncriptarAES_319008("", Texto, True, adores("FECPWD").Value, 1)

            sConsultaUpd = "UPDATE OBJSADM SET PWD= ? " & vbCrLf

            Set oCom = New ADODB.Command
            Set oCom.ActiveConnection = oADOConnection
            oCom.CommandType = adCmdText
            oCom.CommandText = sConsultaUpd
            Set adoParam = oCom.CreateParameter("PWD", adVarChar, adParamInput, 100, TextoUpd)
            oCom.Parameters.Append adoParam
            oCom.Execute
        End If
    End If
End Sub


Public Function CodigoDeActualizacion31900_04_18_03A31900_04_18_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_004
    
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_03A31900_04_18_04 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_03A31900_04_18_04 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_004()

Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_BUSCAR_CENTROCOSTE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_BUSCAR_CENTROCOSTE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_BUSCAR_CENTROCOSTE] @CIA INT, @IDI VARCHAR(20),@COD VARCHAR(10)  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT A.COD,A.DEN,A.CENTRO_SM  from(SELECT CSU.UON1 + case when csu.uon2 is not null then ''#'' else '''' end  + CSU.UON2 + case when csu.uon3 is not null then ''#'' else '''' end + CSU.UON3 + case when csu.uon4 is not null then ''#'' else '''' end + CSU.UON4 as COD,COALESCE(CSU.UON4, CSU.UON3, CSU.UON2, CSU.UON1) COD_UON,COALESCE(CSU.UON4, CSU.UON3, CSU.UON2, CSU.UON1) + '' - '' + SD.DEN AS DEN, CSU.CENTRO_SM " & vbCrLf
sConsulta = sConsulta & "   FROM ' + @FSGS + 'CENTRO_SM_UON CSU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'CENTRO_SM_DEN SD WITH(NOLOCK)ON CSU.CENTRO_SM=SD.CENTRO_SM AND IDI=@IDI) A " & vbCrLf
sConsulta = sConsulta & "   WHERE A.COD_UON = @COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@IDI VARCHAR(20),@COD VARCHAR(10)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM ,@IDI=@IDI,@COD=@COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_04_18_04A31900_04_18_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_005
    
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_04A31900_04_18_05 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_04A31900_04_18_05 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_005()

Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARGEN_INTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT PI.ACCESO_FSSM, PI.VER_NUMEXT, PI.ACCESO_FSGS, ACCESO_FSFA,FIRMA_DIG_OFERTAS, PED_DIR_ENVIO_EMAIL FROM ' + @FSGS + 'PARGEN_INTERNO PI WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_05A31900_04_18_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Triggers_006
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Datos_006
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_006
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_05A31900_04_18_06 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_05A31900_04_18_06 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_006()

Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PARAMETROS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PARAMETROS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PARAMETROS]  @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @CIA IS NULL AND @CIA <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ACCESO_FSSM FROM ' + @FSGS + 'PARGEN_INTERNO WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT OBLCODPEDDIR,OBLCODPEDIDO FROM ' + @FSGS + 'PARGEN_PED WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,IDI,DEN FROM ' + @FSGS + 'PARGEN_LIT WITH (NOLOCK) ORDER BY ID, IDI'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_8_Datos_006()
Dim sConsulta As String

gRDOCon.QueryTimeout = 7200

sConsulta = "UPDATE PROCE_OFE SET FECACT=GETDATE() WHERE FECACT IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_8_Triggers_006()
Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[PROCE_OFE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)    " & vbCrLf
sConsulta = sConsulta & "   drop trigger [dbo].[PROCE_OFE_TG_INSUPD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[PROCE_OFE_TG_INSUPD] ON [dbo].[PROCE_OFE]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET FECACT = GETDATE() FROM PROCE_OFE  IO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I ON  IO.ID = I.ID" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_06A31900_08_18_07() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_007
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_06A31900_08_18_07 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_06A31900_08_18_07 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_007()
Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL, @RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, @ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, @FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, @EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, @ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "@PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, @NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, @FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, @FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, @FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, @ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "@CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, @CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, @CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, @UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, @PREC_VISIBLE int = NULL,@PREC_WIDTH FLOAT =NULL, @IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, @IMPREC_VISIBLE int = NULL," & vbCrLf
sConsulta = sConsulta & "@IMPREC_WIDTH FLOAT = NULL, @IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, @MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, @CC1_VISIBLE int = NULL,@CC1_WIDTH FLOAT = NULL,@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL,@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL,@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, @PARTIDA1_VISIBLE int = NULL," & vbCrLf
sConsulta = sConsulta & "@PARTIDA1_WIDTH FLOAT = NULL, @PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, @PARTIDA3_VISIBLE int = NULL,@PARTIDA3_WIDTH FLOAT = NULL, @PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL, @BLOQ_FACTURA_VISIBLE INT = NULL, @BLOQ_FACTURA_WIDTH FLOAT = NULL, @NUM_LINEA_VISIBLE INT = NULL, @NUM_LINEA_WIDTH FLOAT = NULL, @FACTURA_VISIBLE INT = NULL," & vbCrLf
sConsulta = sConsulta & "@FACTURA_WIDTH FLOAT = NULL, @RECEPTOR_VISIBLE INT = NULL, @RECEPTOR_WIDTH FLOAT = NULL, @IMPORTE_ALBARAN_VISIBLE INT = NULL, @IMPORTE_ALBARAN_WIDTH FLOAT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la informaci�n del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tama�o suficiente para ser visible pero no molestar" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_VISIBLE = @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_WIDTH = @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_WIDTH = @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT, @ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT," & vbCrLf
sConsulta = sConsulta & " @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT, @PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT," & vbCrLf
sConsulta = sConsulta & "@FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT, @FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT, @CANTPED_VISIBLE INT," & vbCrLf
sConsulta = sConsulta & "@CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, @UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT," & vbCrLf
sConsulta = sConsulta & "@IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT, @CC2_WIDTH FLOAT," & vbCrLf
sConsulta = sConsulta & "@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT," & vbCrLf
sConsulta = sConsulta & "@PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT,@BLOQ_FACTURA_VISIBLE INT, @BLOQ_FACTURA_WIDTH FLOAT, @NUM_LINEA_VISIBLE INT, @NUM_LINEA_WIDTH FLOAT, @FACTURA_VISIBLE INT, @FACTURA_WIDTH FLOAT, @RECEPTOR_VISIBLE INT," & vbCrLf
sConsulta = sConsulta & "@RECEPTOR_WIDTH FLOAT, @ANCHODEFECTO INT, @IMPORTE_ALBARAN_VISIBLE INT, @IMPORTE_ALBARAN_WIDTH FLOAT'," & vbCrLf
sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_VISIBLE = @FACTURA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_WIDTH = @FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_WIDTH = @RECEPTOR_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT [RETRASADO_VISIBLE],[RETRASADO_WIDTH],[ALBARAN_VISIBLE],[ALBARAN_WIDTH],[FRECEP_VISIBLE],[FRECEP_WIDTH],[EMP_VISIBLE],[EMP_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[ERP_VISIBLE],[ERP_WIDTH],[PEDIDO_VISIBLE],[PEDIDO_WIDTH],[NUMPEDPROVE_VISIBLE],[NUMPEDPROVE_WIDTH],[FEMISION_VISIBLE],[FEMISION_WIDTH],[FENTREGA_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[FENTREGA_WIDTH],[FENTREGAPROVE_VISIBLE],[FENTREGAPROVE_WIDTH],[ART_VISIBLE],[ART_WIDTH],[CANTPED_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[CANTPED_WIDTH],[CANTREC_VISIBLE],[CANTREC_WIDTH],[CANTPEND_VISIBLE],[CANTPEND_WIDTH],[UNI_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[UNI_WIDTH],[PREC_VISIBLE],[PREC_WIDTH],[IMPPED_VISIBLE],[IMPPED_WIDTH],[IMPREC_VISIBLE],[IMPREC_WIDTH],[IMPPEND_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[IMPPEND_WIDTH],[MON_VISIBLE],[MON_WIDTH],[CC1_VISIBLE],[CC1_WIDTH],[CC2_VISIBLE],[CC2_WIDTH],[CC3_VISIBLE],[CC3_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[CC4_VISIBLE],[CC4_WIDTH],[PARTIDA1_VISIBLE],[PARTIDA1_WIDTH],[PARTIDA2_VISIBLE],[PARTIDA2_WIDTH],[PARTIDA3_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[PARTIDA3_WIDTH],[PARTIDA4_VISIBLE],[PARTIDA4_WIDTH], [BLOQ_FACTURA_VISIBLE],[BLOQ_FACTURA_WIDTH],[NUM_LINEA_VISIBLE],[NUM_LINEA_WIDTH],[FACTURA_VISIBLE],[FACTURA_WIDTH],[RECEPTOR_VISIBLE],[RECEPTOR_WIDTH],[IMPORTE_ALBARAN_VISIBLE],[IMPORTE_ALBARAN_WIDTH]" & vbCrLf
sConsulta = sConsulta & "FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE USU = @IDUSU AND CIA = @CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_07A31900_08_18_08() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_008
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_07A31900_08_18_08 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_07A31900_08_18_08 = False
    Resume Salir
End Function


Private Sub V_31900_8_Storeds_008()
Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_TIPOSPEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_TIPOSPEDIDO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_TIPOSPEDIDO] @CIA INT, @IDI VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TIPOSPEDIDO @IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @IDI VARCHAR(20)', @IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PROVES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PROVES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PROVES] @CIA INT, @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50)='SPA', @ORGCOMPRAS VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVES @COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU, @IDI=@IDI,@ORGCOMPRAS =@ORGCOMPRAS '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50) ,@ORGCOMPRAS VARCHAR(4)',@COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU,@IDI=@IDI,@ORGCOMPRAS =@ORGCOMPRAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_04_18_08A31900_08_18_09() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_009
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_08A31900_08_18_09 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_08A31900_08_18_09 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_009()
Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADINSTFIELDSCALC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADINSTFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADINSTFIELDSCALC] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
sConsulta = sConsulta & "   @INSTANCIA INT, " & vbCrLf
sConsulta = sConsulta & "   @VERSION INT, " & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)='SPA', " & vbCrLf
sConsulta = sConsulta & "   @PROVE varchar(50)=null, " & vbCrLf
sConsulta = sConsulta & "   @NUEVO_WORKFL TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VERSION=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'EXEC '+@FSGS+'FSWS_LOADINSTFIELDSCALC @INSTANCIA=@INSTANCIA,@VERSION=@VERSION,@IDI=@IDI,@PROVE=@PROVE' " & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, " & vbCrLf
sConsulta = sConsulta & "       N'@INSTANCIA INT,@VERSION INT,@IDI VARCHAR(50),@PROVE VARCHAR(50)', " & vbCrLf
sConsulta = sConsulta & "       @INSTANCIA=@INSTANCIA,@VERSION=@VERSION,@IDI=@IDI,@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'EXEC '+@FSGS+'FSPM_LOADINSTFIELDSCALC @INSTANCIA=@INSTANCIA,@VERSION=@VERSION,@IDI=@IDI,@PROVE=@PROVE,@WORKFLOW=@WORKFLOW'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, " & vbCrLf
sConsulta = sConsulta & "       N'@INSTANCIA INT,@VERSION INT,@IDI VARCHAR(50),@PROVE VARCHAR(50),@WORKFLOW BIT', " & vbCrLf
sConsulta = sConsulta & "       @INSTANCIA=@INSTANCIA,@VERSION=@VERSION,@IDI=@IDI,@PROVE=@PROVE,@WORKFLOW=@NUEVO_WORKFL" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSTRATADOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSTRATADOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]  " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @HOY DATE" & vbCrLf
sConsulta = sConsulta & "DECLARE @AYER DATE" & vbCrLf
sConsulta = sConsulta & "SET @HOY= GETDATE()" & vbCrLf
sConsulta = sConsulta & "SET @AYER= DATEADD(d,-1,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD * TIENE QUE SER AS�" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'SELECT * ,(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK))[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE (TRASPASO = 0 AND FFS IS NOT NULL)OR (TRASPASO=0 AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER))'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'UPDATE' + @FSGS + 'SES SET TRASPASO=1 WHERE (TRASPASO = 0 AND FFS IS NOT NULL)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_04_18_09A31900_08_18_10() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_010
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_09A31900_08_18_10 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_09A31900_08_18_10 = False
    Resume Salir
End Function

Private Sub V_31900_8_Tablas_010()

Dim sConsulta As String

sConsulta = "IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'CIACODGS' and Object_ID = Object_ID(N'SESION'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [DBO].[SESION] ALTER COLUMN [CIACODGS] VARCHAR(100) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_04_18_10A31900_08_18_11() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_011
    '-------------------------------------------------------------------------------------------

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_011
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_10A31900_08_18_11 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_10A31900_08_18_11 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_011()
Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_PARAM_CONFIG_SERVICIOEMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_PARAM_CONFIG_SERVICIOEMAIL]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_PARAM_CONFIG_SERVICIOEMAIL]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,TIPOCLTEMAIL,SERVIDOR,PUERTO,[SSL],AUTENTICACION,BASICA_GS,USU,PWD,USAR_CUENTA,CUENTAMAIL,MOSTRARMAIL,ACUSERECIBO,FECPWD" & vbCrLf
sConsulta = sConsulta & ",CC,CCO,RESPUESTAMAIL,P_SERVIDOR,P_AUTENTICACION,P_USU,P_PWD,P_FROM,P_FROMNAME ,P_PUERTO,P_SSL,P_DOMINIO, P_METODO_ENTREGA, DOMINIO, METODO_ENTREGA" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_MAIL WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_INSERT_REGISTRO_MAIL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_INSERT_REGISTRO_MAIL]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_INSERT_REGISTRO_MAIL] @CIA INT, @SUBJECT NVARCHAR (200),@PARA NVARCHAR (100),@CC NVARCHAR (100),@CCO NVARCHAR (100)," & vbCrLf
sConsulta = sConsulta & "@DIR_RESPUESTA NVARCHAR(100),@CUERPO NTEXT,@ACUSE_RECIBO TINYINT,@TIPO TINYINT,@PRODUCTO INT ,@USU INT,@KO TINYINT," & vbCrLf
sConsulta = sConsulta & "@TEXTO_ERROR NVARCHAR(200), @ID_ERROR INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID=MAX(ISNULL(ID,0))+1 FROM REGISTRO_EMAIL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO REGISTRO_EMAIL (CIA, ID, SUBJECT, PARA ,CC ,CCO ,DIR_RESPUESTA ,CUERPO ,ACUSE_RECIBO ,TIPO ,PRODUCTO ,USU ,KO ,TEXTO_ERROR, ID_ERROR)" & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA, @ID, @SUBJECT , @PARA ,@CC ,@CCO ,@DIR_RESPUESTA ,@CUERPO ,@ACUSE_RECIBO ,@TIPO ,@PRODUCTO ,@USU ,@KO ,@TEXTO_ERROR, @ID_ERROR)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_8_Tablas_011()

Dim sConsulta As String

sConsulta = "IF NOT EXISTS(SELECT * FROM sys.columns WHERE (Name = N'P_PUERTO'  and Object_ID = Object_ID(N'PARGEN_MAIL')) or (Name = N'P_SSL'  and Object_ID = Object_ID(N'PARGEN_MAIL')) or (Name = N'P_DOMINIO'  and Object_ID = Object_ID(N'PARGEN_MAIL')) or (Name = N'P_METODO_ENTREGA'  and Object_ID = Object_ID(N'PARGEN_MAIL')) or (Name = N'DOMINIO'  and Object_ID = Object_ID(N'PARGEN_MAIL')) or (Name = N'METODO_ENTREGA'  and Object_ID = Object_ID(N'PARGEN_MAIL')))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_MAIL ADD P_PUERTO int NULL, P_SSL tinyint NULL, P_DOMINIO varchar(100) NULL, P_METODO_ENTREGA tinyint NULL, DOMINIO varchar(100) NULL, METODO_ENTREGA tinyint NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'SUBJECT' and Object_ID = Object_ID(N'REGISTRO_EMAIL'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [DBO].[REGISTRO_EMAIL] ALTER COLUMN [SUBJECT] [VARCHAR] (200) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'ID_ERROR' and Object_ID = Object_ID(N'REGISTRO_EMAIL'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.REGISTRO_EMAIL ADD ID_ERROR int NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_11A31900_08_18_12() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_012
    '-------------------------------------------------------------------------------------------

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_012
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.12'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_11A31900_08_18_12 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_11A31900_08_18_12 = False
    Resume Salir
End Function

Private Sub V_31900_8_Tablas_012()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PROCE_OFE]') AND name = N'UC_PROCE_OFE')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROCE_OFE] DROP CONSTRAINT [UC_PROCE_OFE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE ALTER COLUMN PROCE INT NOT NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/****** Object:  Index [UC_PROCE_OFE]    Script Date: 07/17/2013 13:32:21 ******/" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROCE_OFE] ADD  CONSTRAINT [UC_PROCE_OFE] UNIQUE NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "      [CIA_COMP] ASC," & vbCrLf
sConsulta = sConsulta & "      [ANYO] ASC," & vbCrLf
sConsulta = sConsulta & "      [GMN1] ASC," & vbCrLf
sConsulta = sConsulta & "      [PROCE] ASC," & vbCrLf
sConsulta = sConsulta & "      [PROVE] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_8_Storeds_012()
Dim sConsulta As String
gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS] " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @PROCE INT," & vbCrLf
sConsulta = sConsulta & "   @COD_OLD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @COD_NEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @OFE_GR_ATRIB TABLE (ID INT,GRUPO VARCHAR(50),ATRIB_ID INT,VALOR_NUM FLOAT,VALOR_TEXT VARCHAR(800)," & vbCrLf
sConsulta = sConsulta & "                                VALOR_FEC DATETIME,VALOR_BOOL TINYINT,IMPPARCIAL FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   --Antes del borrado se hace una inserci�n en la var. tabla @OFE_GR_ATRIB porque GRUPO es una foreign key a GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO @OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ATRIB     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- GRUPO_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE GRUPO_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM GRUPO_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=GRUPO_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND GRUPO_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- ITEM_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE ITEM_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM ITEM_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=ITEM_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND ITEM_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ADJUN ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ADJUN,OGA.NOM,OGA.COM,OGA.IDPORTAL,OGA.IDGS,OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ADJUN     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ---------- " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,OGA.GRUPO,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM @OFE_GR_ATRIB OGA      " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_COSTESDESC ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "   SELECT OGC.ID,@COD_NEW,OGC.ATRIB_ID,OGC.IMPPARCIAL,OGC.FECACT" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_COSTESDESC    " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_ITEM_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ITEM_ATRIB SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OFE_ITEM_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OFE_ITEM_ATRIB.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER, " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE INT  , " & vbCrLf
sConsulta = sConsulta & "   @GRUPO VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_GRUPO @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT, @GMN1 VARCHAR(50), @PROCE INT  , @GRUPO VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER, " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE INT , " & vbCrLf
sConsulta = sConsulta & "   @ITEM SMALLINT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_ITEM @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @ITEM = @ITEM'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT, @GMN1 VARCHAR(50), @PROCE INT  , @ITEM SMALLINT'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]  " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER, " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_PROCESO @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT, @GMN1 VARCHAR(50), @PROCE INT'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER, " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE INT, " & vbCrLf
sConsulta = sConsulta & "   @AMBITO TINYINT, " & vbCrLf
sConsulta = sConsulta & "   @GRUPO VARCHAR(50)=null, " & vbCrLf
sConsulta = sConsulta & "   @ATRIB INT = null, " & vbCrLf
sConsulta = sConsulta & "   @ITEM INT = null " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @AMBITO = @AMBITO, @GRUPO = @GRUPO, @ATRIB = @ATRIB, @ITEM = @ITEM'" & vbCrLf
sConsulta = sConsulta & "   SET @PARMDEFINITION = N'@ANYO SMALLINT, @GMN1 VARCHAR(50), @PROCE INT, @AMBITO TINYINT, @GRUPO VARCHAR(50)=NULL, @ATRIB INT, @ITEM INT'" & vbCrLf
sConsulta = sConsulta & "   EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @AMBITO = @AMBITO, @GRUPO = @GRUPO, @ATRIB = @ATRIB, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "   RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_PROCE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_PROCE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_PROCE] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curELIMPROCE CURSOR LOCAL FOR SELECT ID FROM  PROCE_OFE WITH(NOLOCK) WHERE CIA_COMP=@CIA_COMP AND ANYO=@ANYO AND  GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curELIMPROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curELIMPROCE INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ITEM_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ITEM_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_GR_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_GR_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM PROCE_OFE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM ITEM_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM GRUPO_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM PROCE_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curELIMPROCE INTO @ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curELIMPROCE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curELIMPROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_ADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_ADJUNTO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_121A31900_08_18_13() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_013
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Datos_013
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_013
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.13'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_121A31900_08_18_13 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_121A31900_08_18_13 = False
    Resume Salir
End Function

Public Sub V_31900_8_Tablas_013()
    Dim sConsulta As String
    
    sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
    sConsulta = sConsulta & "      HISTORICO_PWD tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_HISTORICO_PWD DEFAULT (0)," & vbCrLf
    sConsulta = sConsulta & "      EDAD_MIN_PWD tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_EDAD_MIN_PWD DEFAULT (0)," & vbCrLf
    sConsulta = sConsulta & "      EDAD_MAX_PWD tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_EDAD_MAX_PWD DEFAULT (0)," & vbCrLf
    sConsulta = sConsulta & "      COMPLEJIDAD_PWD bit NOT NULL CONSTRAINT DF_PARGEN_GEST_COMPLEJIDAD_PWD DEFAULT (1)," & vbCrLf
    sConsulta = sConsulta & "      MIN_SIZE_PWD int NOT NULL CONSTRAINT DF_PARGEN_GEST_MIN_SIZE_PWD DEFAULT (6)," & vbCrLf
    sConsulta = sConsulta & "LOGPREBLOQ tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_LOGPREBLOQ DEFAULT (0)," & vbCrLf
    sConsulta = sConsulta & "ENCRYPT_ONE_WAY bit NOT NULL CONSTRAINT DF_PARGEN_GEST_ENCRYPT_ONE_WAY DEFAULT (0)," & vbCrLf
    sConsulta = sConsulta & "USUIMPERSONATE nvarchar(100) NULL," & vbCrLf
    sConsulta = sConsulta & "PWDIMPERSONATE nvarchar(100) NULL, " & vbCrLf
    sConsulta = sConsulta & "DOMINIOIMPERSONATE nvarchar(100) NULL," & vbCrLf
    sConsulta = sConsulta & "URLPWDCHANGEWCF nvarchar(100) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "ALTER TABLE USU ALTER COLUMN PWD VARCHAR(128) NULL" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE USU ALTER COLUMN FECPWD DATETIME NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "ALTER TABLE USU ADD " & vbCrLf
    sConsulta = sConsulta & "      PWD_HASH nvarchar(50) NULL," & vbCrLf
    sConsulta = sConsulta & "NUMACCESOSERROR tinyint NOT NULL CONSTRAINT DF_USU_NUMACCESOSERROR DEFAULT (0)," & vbCrLf
    sConsulta = sConsulta & "      BLOQ bit NOT NULL CONSTRAINT DF_USU_BLOQ DEFAULT (0)," & vbCrLf
    sConsulta = sConsulta & "      SALT varchar(512) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "ALTER TABLE dbo.ADM ADD " & vbCrLf
    sConsulta = sConsulta & "      SALT varchar(512) NULL," & vbCrLf
    sConsulta = sConsulta & "      FECUSU datetime NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
    sConsulta = sConsulta & "      URL_CHANGE_PWD nvarchar(500) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TABLE [dbo].[USU_PWD](" & vbCrLf
    sConsulta = sConsulta & "      [CIA] [int] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "      [ID] [int] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "      [PWD] [varchar](512) NOT NULL," & vbCrLf
    sConsulta = sConsulta & "      [FEC_PWD] [datetime] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "      [SALT] [varchar](512) NULL," & vbCrLf
    sConsulta = sConsulta & "CONSTRAINT [PK_USU_PWD] PRIMARY KEY CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "(" & vbCrLf
    sConsulta = sConsulta & "      [CIA] ASC," & vbCrLf
    sConsulta = sConsulta & "      [ID] ASC" & vbCrLf
    sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "ALTER TABLE [dbo].[USU_PWD]  WITH CHECK ADD  CONSTRAINT [FK_USU_PWD_USU] FOREIGN KEY([CIA], [ID])" & vbCrLf
    sConsulta = sConsulta & "REFERENCES [dbo].[USU] ([CIA], [ID])" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE [dbo].[USU_PWD] CHECK CONSTRAINT [FK_USU_PWD_USU]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    
    sConsulta = "ALTER TABLE IDI ADD LANGUAGETAG NVARCHAR(50) NOT NULL DEFAULT 'es-ES';" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub



Private Sub V_31900_8_Storeds_013()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,PAGINA,FSAL_ENTORNO,PRODUCTO,FICLIPREPAR,FFCLIPREPAR,FISERVER,FFSERVER," & vbCrLf
    sConsulta = sConsulta & "                                   FICLIRESP,FFCLIRESP,TPREPAR,TUPLOAD,TSERVER,TDOWNLOAD,TRESPUESTA,TTOTAL," & vbCrLf
    sConsulta = sConsulta & "                                   PAGINAORIGEN,USUARIO,PROVE,IP,TRASPASO,FECACT,POSTBACK,HTTP_USER_AGENT," & vbCrLf
    sConsulta = sConsulta & "                                   FSAL_PYME FROM ' + @FSGS + 'FSAL_ACCESOS WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_ACCESOS SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,ANYO,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_ANYO WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_ANYO SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT * FROM ' + @FSGS + 'FSAL_ACCESOS WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_ACCESOS SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIA WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_DIA SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDISANYOATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDISANYOATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSDISANYOATRASLADAR_FSGS]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,ANYO,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_ANYO WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_DIS_ANYO SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDISMESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDISMESATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSDISMESATRASLADAR_FSGS]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,FECHA,ANYO,MES,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_MES WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_DIS_MES SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDISSEMANAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDISSEMANAATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSDISSEMANAATRASLADAR_FSGS]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_SEMANA WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_DIS_SEMANA SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_HORA WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_HORA SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,FECHA,ANYO,MES,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MES WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_MES SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,FECHA,ANYO,MES,DIA,HORA,MIN,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MINUTO WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_MINUTO SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,FECHA,ANYO,MES,DIA,ENTORNO,PYME,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_SEMANA WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_SEMANA SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
    sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
    sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECHAFIN DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECHAINI DATETIME" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
    sConsulta = sConsulta & "SET @FECHAFIN=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @FECHAINI=@fecha" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING= @FSGS + 'FSAL_ESTADISTICAS_EVENTOS @FECHAINI, @FECHAFIN, @ENTORNO, @PYME'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, N'@FECHAINI DATE, @FECHAFIN DATE, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)', @FECHAINI =@FECHAINI, @FECHAFIN=@FECHAFIN, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
    sConsulta = sConsulta & "  end              " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_EVENTOS_HISTATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_EVENTOS_HISTATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_EVENTOS_HISTATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT * FROM ' + @FSGS + 'FSAL_EVENTOS_HIST WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_EVENTOS_HIST SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_EVENTOSATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_EVENTOSATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_EVENTOSATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT * FROM ' + @FSGS + 'FSAL_EVENTOS WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_EVENTOS SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_OBTENER_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_OBTENER_ENTORNO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_OBTENER_ENTORNO]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT TOP 1 COD FROM '+@FSGS +' FSAL_ENTORNOS WITH (NOLOCK)'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_OBTENER_PYME]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_OBTENER_PYME]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_OBTENER_PYME]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT TOP 1 COD FROM '+ @FSGS + ' FSAL_PYMES WITH (NOLOCK)'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
    sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
    sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECHAFIN DATETIME" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
    sConsulta = sConsulta & "SET @FECHAFIN=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING= @FSGS + 'FSAL_ESTADISTICAS_ENTORNO'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, N'@FECHAINI DATE, @FECHAFIN DATE, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)', @FECHAINI  =@fecha, @FECHAFIN =@FECHAFIN, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
    sConsulta = sConsulta & "  end              " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta



End Sub

Public Sub V_31900_8_Datos_013()
    Dim sConsulta As String

    sConsulta = "UPDATE ADM SET FECUSU=FECPWD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "UPDATE DBO.IDI" & vbCrLf
    sConsulta = sConsulta & "SET LANGUAGETAG='en-US'" & vbCrLf
    sConsulta = sConsulta & "WHERE COD='ENG';" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "UPDATE DBO.IDI" & vbCrLf
    sConsulta = sConsulta & "SET LANGUAGETAG='de-DE'" & vbCrLf
    sConsulta = sConsulta & "WHERE COD='GER';" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "UPDATE DBO.IDI" & vbCrLf
    sConsulta = sConsulta & "SET LANGUAGETAG='es-ES'" & vbCrLf
    sConsulta = sConsulta & "WHERE COD='SPA';" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
End Sub

Public Function CodigoDeActualizacion31900_04_18_13A31900_08_18_14() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_014
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.14'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_13A31900_08_18_14 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_13A31900_08_18_14 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_014()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_LOGIN] @CIA VARCHAR(50), @COD VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
    sConsulta = sConsulta & "       CIAS.USUPPAL AS USUPPAL, CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
    sConsulta = sConsulta & "       USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
    sConsulta = sConsulta & "       USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "       USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
    sConsulta = sConsulta & "       USU.DATEFMT DATEFMT, USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "       IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
    sConsulta = sConsulta & "       USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA , MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU," & vbCrLf
    sConsulta = sConsulta & "       CIAS.DEN AS CIADEN, CIAS.NIF,IDI.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "  FROM USU WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "        LEFT JOIN USU_PORT WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.ID=USU_PORT.USU" & vbCrLf
    sConsulta = sConsulta & "              AND USU_PORT.PORT=0" & vbCrLf
    sConsulta = sConsulta & "              AND USU.CIA=USU_PORT.CIA" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.CIA=CIAS.ID" & vbCrLf
    sConsulta = sConsulta & "        LEFT JOIN CIAS_PORT WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.CIA=CIAS_PORT.CIA" & vbCrLf
    sConsulta = sConsulta & "              AND CIAS_PORT.PORT=0" & vbCrLf
    sConsulta = sConsulta & "        LEFT JOIN MON WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON CIAS.MON=MON.ID" & vbCrLf
    sConsulta = sConsulta & "        LEFT JOIN MON MON2 WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.MON=MON2.ID" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN IDI WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.IDI=IDI.ID" & vbCrLf
    sConsulta = sConsulta & " WHERE USU.COD=@COD" & vbCrLf
    sConsulta = sConsulta & "   AND CIAS.COD=@CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GETPARAMETROS_PASSWORDPOLICY]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GETPARAMETROS_PASSWORDPOLICY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_GETPARAMETROS_PASSWORDPOLICY]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "   SELECT HISTORICO_PWD,EDAD_MAX_PWD,EDAD_MIN_PWD,COMPLEJIDAD_PWD,MIN_SIZE_PWD " & vbCrLf
    sConsulta = sConsulta & "   FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GETPASSWORD_HISTORY]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GETPASSWORD_HISTORY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_GETPASSWORD_HISTORY]" & vbCrLf
    sConsulta = sConsulta & "   @NUM_HISTORY INT," & vbCrLf
    sConsulta = sConsulta & "   @CIA INT," & vbCrLf
    sConsulta = sConsulta & "   @ID INT," & vbCrLf
    sConsulta = sConsulta & "   @NEWPWD NVARCHAR(128)" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=N';WITH PASSWORD_HISTORY AS ('" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+N'SELECT TOP '+CAST(@NUM_HISTORY AS NVARCHAR(3))+N' PWD '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM USU_PWD WITH(NOLOCK) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+N'WHERE CIA=@CIA AND ID=@ID '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+N'ORDER BY FEC_PWD DESC) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+N'SELECT COUNT(PWD) COINCIDENCIAS FROM PASSWORD_HISTORY WHERE PWD=@NEWPWD'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@CIA INT,@ID INT,@NEWPWD NVARCHAR(128)'," & vbCrLf
    sConsulta = sConsulta & "   @CIA=@CIA,@ID=@ID,@NEWPWD=@NEWPWD" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_PASSWORD_CHANGE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_PASSWORD_CHANGE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_PASSWORD_CHANGE]" & vbCrLf
    sConsulta = sConsulta & "   @IDCIA INT," & vbCrLf
    sConsulta = sConsulta & "   @IDUSU INT," & vbCrLf
    sConsulta = sConsulta & "   @NEWPWD VARCHAR(128)," & vbCrLf
    sConsulta = sConsulta & "   @FECPWD DATE," & vbCrLf
    sConsulta = sConsulta & "   @SALT VARCHAR(512)=NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO USU_PWD " & vbCrLf
    sConsulta = sConsulta & "   SELECT CIA,ID,PWD,FECPWD,SALT " & vbCrLf
    sConsulta = sConsulta & "   FROM USU WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   WHERE CIA=@IDCIA AND ID=@IDUSU " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   UPDATE USU SET PWD=@NEWPWD,FECPWD=@FECPWD,SALT=@SALT,PWD_HASH=NULL" & vbCrLf
    sConsulta = sConsulta & "   WHERE CIA=@IDCIA AND ID=@IDUSU" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTADJUNDATA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTADJUNDATA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTADJUNDATA] @CIA INT=NULL, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @TIPO=@TIPO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @TIPO INT = 0',@ID=@ID, @TIPO=@TIPO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ADJUNTO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LEER_ADJUNTO] @CIA INT, @ID INT = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TIPO INT=NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LEER_ADJUNTO @ID=@ID,@TIPO=@TIPO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT = NULL,@TIPO INT=NULL', @ID=@ID,@TIPO=@TIPO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_ADJUN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_ADJUN] @CIA INT, @ID_ADJUN INT OUTPUT,@PER NVARCHAR(50)=NULL,@PROVE NVARCHAR(50),@NOM NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & ",  @COMENT NVARCHAR(4000), @DATASIZE INT=NULL,@IDIOMA NVARCHAR(10)='' AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_SAVE_ADJUN @ID_ADJUN=@ID_ADJUN OUTPUT,@PER=@PER,@PROVE=@PROVE,@NOM=@NOM,@COMENT=@COMENT,@DATASIZE=@DATASIZE,@IDIOMA=@IDIOMA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID_ADJUN INT OUTPUT,@PER NVARCHAR(50)=NULL,@PROVE NVARCHAR(50),@NOM NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & ",  @COMENT NVARCHAR(4000), @DATASIZE INT=NULL,@IDIOMA NVARCHAR(10)=''''', @ID_ADJUN=@ID_ADJUN OUTPUT,@PER=@PER,@PROVE=@PROVE,@NOM=@NOM,@COMENT=@COMENT,@DATASIZE=@DATASIZE,@IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    

End Sub
    
Public Function CodigoDeActualizacion31900_04_18_12A31900_08_18_121() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_0121
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.18.12.1'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT TRANSACTION"
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_12A31900_08_18_121 = True
    
Salir:
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_12A31900_08_18_121 = False
    Resume Salir
End Function
Private Sub V_31900_8_Storeds_0121()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS] " & vbCrLf
    sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
    sConsulta = sConsulta & "   @ID INT " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINVISIBLEFIELDS @ID=@ID,@SOLIC_PROVE=2 '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub
Public Function CodigoDeActualizacion31900_04_18_14A31900_08_18_15() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_015
    '-------------------------------------------------------------------------------------------
    
    frmProgreso.lblDetalle = "Cambios en Triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Triggers_015
    '-------------------------------------------------------------------------------------------

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.15'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    bTransaccionEnCurso = False
        
     CodigoDeActualizacion31900_04_18_14A31900_08_18_15 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_14A31900_08_18_15 = False
    Resume Salir
End Function
Public Sub V_31900_8_Tablas_015()
    Dim sConsulta As String
    
    sConsulta = "CREATE TABLE [dbo].[URL_EXTERNAS](" & vbCrLf
    sConsulta = sConsulta & "      [ID] [int] IDENTITY(1,1) NOT NULL," & vbCrLf
    sConsulta = sConsulta & "      [MENU_PADRE] [int] NULL," & vbCrLf
    sConsulta = sConsulta & "      [TIPO_LLAMADA] [smallint] NULL," & vbCrLf
    sConsulta = sConsulta & "      [PAGINA_CUSTOM] [nvarchar](100) NULL," & vbCrLf
    sConsulta = sConsulta & "      [URL] [nvarchar](500) NULL," & vbCrLf
    sConsulta = sConsulta & "      [WEBSERVICE_URL] [nvarchar](500) NULL," & vbCrLf
    sConsulta = sConsulta & "      [WEBSERVICE_USU] [nvarchar](100) NULL," & vbCrLf
    sConsulta = sConsulta & "      [WEBSERVICE_PWD] [nvarchar](100) NULL," & vbCrLf
    sConsulta = sConsulta & "      [FECACT] [datetime] NULL," & vbCrLf
    sConsulta = sConsulta & "CONSTRAINT [PK_URL_EXTERNAS] PRIMARY KEY CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "(" & vbCrLf
    sConsulta = sConsulta & "      [ID] ASC" & vbCrLf
    sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TABLE [dbo].[URL_EXTERNAS_DEN](" & vbCrLf
    sConsulta = sConsulta & "      [ID_URL_EXTERNA] [int] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "      [IDIOMA] [varchar](3) NOT NULL," & vbCrLf
    sConsulta = sConsulta & "      [DEN] [nvarchar](100) NOT NULL," & vbCrLf
    sConsulta = sConsulta & "      [FECACT] [datetime] NULL," & vbCrLf
    sConsulta = sConsulta & "CONSTRAINT [PK_URL_EXTERNAS_DEN] PRIMARY KEY CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "(" & vbCrLf
    sConsulta = sConsulta & "      [ID_URL_EXTERNA] ASC," & vbCrLf
    sConsulta = sConsulta & "      [IDIOMA] ASC" & vbCrLf
    sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub
Private Sub V_31900_8_Triggers_015()
Dim sConsulta As String

sConsulta = "CREATE TRIGGER [dbo].[URL_EXTERNAS_TG_INSUPD] ON [dbo].[URL_EXTERNAS]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE URL_EXTERNAS SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM URL_EXTERNAS U WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I   ON U.ID=I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[URL_EXTERNAS_DEN_TG_INSUPD] ON [dbo].[URL_EXTERNAS_DEN]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE URL_EXTERNAS_DEN SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM URL_EXTERNAS_DEN U WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I   ON U.ID_URL_EXTERNA=I.ID_URL_EXTERNA AND U.IDIOMA=I.IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_15A31900_08_18_16() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_016
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.16'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_15A31900_08_18_16 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_15A31900_08_18_16 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_016()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIA WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_USUARIOS_DIA SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
    sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
    sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECHAFIN DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
    sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
    sConsulta = sConsulta & "SET @FECHAFIN=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@DESDE DATE, @HASTA DATE, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING='EXECUTE '+ @FSGS + 'FSAL_ESTADISTICAS_ENTORNO @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION , @DESDE=@DESDE, @HASTA=@FECHAFIN, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
    sConsulta = sConsulta & "  end           " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
    sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
    sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@DESDE DATE, @HASTA DATE, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
    sConsulta = sConsulta & "SET @HASTA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING='EXECUTE ' + @FSGS + 'FSAL_ESTADISTICAS_EVENTOS @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION, @DESDE =@DESDE, @HASTA=@HASTA, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
    sConsulta = sConsulta & "  end              " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_04_18_16A31900_08_18_17() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_017
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.17'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_16A31900_08_18_17 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_16A31900_08_18_17 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_017()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_REGISTRAR_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_REGISTRAR_USUARIO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_REGISTRAR_USUARIO](" & vbCrLf
    sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
    sConsulta = sConsulta & "   @IDPORTAL INT, " & vbCrLf
    sConsulta = sConsulta & "   @COD VARCHAR(50), " & vbCrLf
    sConsulta = sConsulta & "    @APE VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @NOM VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @DEP VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @CAR VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO2 VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @FAX VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @EMAIL VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @IDI VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO_MOVIL VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TIPOEMAIL SMALLINT, " & vbCrLf
    sConsulta = sConsulta & "    @FCEST TINYINT, " & vbCrLf
    sConsulta = sConsulta & "    @FPEST TINYINT, " & vbCrLf
    sConsulta = sConsulta & "    @ORIGINS VARCHAR(50),  " & vbCrLf
    sConsulta = sConsulta & "    @PRINCIPAL BIT," & vbCrLf
    sConsulta = sConsulta & "    @PWD VARCHAR(128)=NULL, " & vbCrLf
    sConsulta = sConsulta & "    @FECHA DATETIME=NULL," & vbCrLf
    sConsulta = sConsulta & "    @SALT VARCHAR(512)=NULL " & vbCrLf
    sConsulta = sConsulta & ")AS" & vbCrLf
    sConsulta = sConsulta & "IF @FECHA=NULL" & vbCrLf
    sConsulta = sConsulta & "   SET @FECHA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDUSU INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDIDI INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @IDUSU=ISNULL(MAX(ID),0) + 1 " & vbCrLf
    sConsulta = sConsulta & "FROM USU" & vbCrLf
    sConsulta = sConsulta & "WHERE CIA=@CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
    sConsulta = sConsulta & "FROM IDI WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE COD = @IDI" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO USU (CIA,PWD,FECPWD,ID,COD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
    sConsulta = sConsulta & "    FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL,[PWD_HASH],SALT) " & vbCrLf
    sConsulta = sConsulta & "VALUES (@CIA,@PWD,@FECHA,@IDUSU,@COD,@APE,@NOM,@DEP,@CAR,@TFNO,@TFNO2," & vbCrLf
    sConsulta = sConsulta & "    @FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL,NEWID(),@SALT)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO USU_PORT (CIA,USU,PORT,FPEST,ORIGINS) " & vbCrLf
    sConsulta = sConsulta & "VALUES (@CIA,@IDUSU,@IDPORTAL,@FPEST,@ORIGINS)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PRINCIPAL = 1 " & vbCrLf
    sConsulta = sConsulta & "  UPDATE CIAS SET USUPPAL=@IDUSU WHERE ID=@CIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta



    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_RECUPERAR_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_RECUPERAR_USUARIO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_RECUPERAR_USUARIO]" & vbCrLf
    sConsulta = sConsulta & "   @CIACOD NVARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @USUCOD NVARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @EMAIL NVARCHAR(300)" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @CIAID INT" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @USUID INT" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SELECT @CIAID=USU.CIA,@USUID=USU.ID" & vbCrLf
    sConsulta = sConsulta & "   FROM USU WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CIAS WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       ON CIAS.ID=USU.CIA AND CIAS.COD=@CIACOD AND USU.COD=@USUCOD AND USU.EMAIL=@EMAIL" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   UPDATE USU SET PWD=NULL,FECPWD=GETDATE(),SALT=NULL,PWD_HASH=NEWID() WHERE ID=@USUID AND CIA=@CIAID" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    
End Sub


Public Function CodigoDeActualizacion31900_04_18_17A31900_08_18_18() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_018
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_018
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.18'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_17A31900_08_18_18 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_17A31900_08_18_18 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_018()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRANSFERIR_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRANSFERIR_EVENTOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRANSFERIR_EVENTOS]" & vbCrLf
    sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
    sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
    sConsulta = sConsulta & "SET @HASTA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING='EXECUTE ' + @FSGS + 'FSAL_TRANSFERIR_EVENTOS @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION, @DESDE =@DESDE, @HASTA=@HASTA, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
    sConsulta = sConsulta & "  end              " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
    sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
    sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
    sConsulta = sConsulta & "SET @HASTA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING='EXECUTE ' + @FSGS + 'FSAL_ESTADISTICAS_EVENTOS @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION, @DESDE =@DESDE, @HASTA=@HASTA, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
    sConsulta = sConsulta & "  end              " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
    sConsulta = sConsulta & "@fecha nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECHAFIN DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
    sConsulta = sConsulta & "SET @DESDE=convert(datetime,@fecha)" & vbCrLf
    sConsulta = sConsulta & "SET @FECHAFIN=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING='EXECUTE '+ @FSGS + 'FSAL_ESTADISTICAS_ENTORNO @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION , @DESDE=@DESDE, @HASTA=@FECHAFIN, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
    sConsulta = sConsulta & "  end  " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


End Sub

Public Sub V_31900_8_Tablas_018()
    Dim sConsulta As String
    
    sConsulta = "ALTER TABLE dbo.PARGEN_GEST" & vbCrLf
    sConsulta = sConsulta & "      DROP CONSTRAINT DF_PARGEN_GEST_EDAD_MIN_PWD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE PARGEN_GEST ALTER COLUMN EDAD_MIN_PWD INT NOT NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD CONSTRAINT" & vbCrLf
    sConsulta = sConsulta & "      DF_PARGEN_GEST_EDAD_MIN_PWD DEFAULT ((0)) FOR EDAD_MIN_PWD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE dbo.PARGEN_GEST" & vbCrLf
    sConsulta = sConsulta & "      DROP CONSTRAINT DF_PARGEN_GEST_EDAD_MAX_PWD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE PARGEN_GEST ALTER COLUMN EDAD_MAX_PWD INT NOT NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD CONSTRAINT" & vbCrLf
    sConsulta = sConsulta & "      DF_PARGEN_GEST_EDAD_MAX_PWD DEFAULT ((0)) FOR EDAD_MAX_PWD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_04_18_18A31900_08_18_19() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_019
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.19'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_18A31900_08_18_19 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_18A31900_08_18_19 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_019()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADFIELDSCALC]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADFIELDSCALC]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADFIELDSCALC] @CIA INT,@FORMULARIO INT,  @IDI VARCHAR(50)='SPA', @SOLICITUD INT  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADFIELDSCALC @IDI = @IDI, @SOLICITUD=@SOLICITUD, @SOLIC_PROVE=2 '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(20), @SOLICITUD  INT ', @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    
    
End Sub

Public Function CodigoDeActualizacion31900_04_18_19A31900_08_18_20() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_020
    '-------------------------------------------------------------------------------------------
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Datos_020
    '-------------------------------------------------------------------------------------------
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_020
    '-------------------------------------------------------------------------------------------

    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.20'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_19A31900_08_18_20 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_19A31900_08_18_20 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_020()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_JDE_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_JDE_LOGIN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_JDE_LOGIN] " & vbCrLf
sConsulta = sConsulta & "   @SESIONID NVARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SRV_GS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @BD_GS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT @SRV_GS=FSGS_SRV,@BD_GS=FSGS_BD FROM CIAS WITH(NOLOCK) WHERE FCEST=3" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT JDE.COD,JDE.PWD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM SESION S WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN CIAS C WITH(NOLOCK) ON C.COD=S.CIACOD AND S.ID=@SESIONID ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N' INNER JOIN ' + @SRV_GS + N'.' + @BD_GS + N'.dbo.PROVE_USU_JDE JDE WITH(NOLOCK) ON JDE.NIF=C.NIF'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@SESIONID NVARCHAR(200)',@SESIONID=@SESIONID " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GETPASSWORD_HISTORY]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GETPASSWORD_HISTORY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSP_GETPASSWORD_HISTORY]" & vbCrLf
sConsulta = sConsulta & "   @NUM_HISTORY INT," & vbCrLf
sConsulta = sConsulta & "   @IDCIA INT," & vbCrLf
sConsulta = sConsulta & "   @IDUSU INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT TOP '+CAST(@NUM_HISTORY AS NVARCHAR(3))+N' PWD,FEC_PWD,SALT '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM USU_PWD WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'WHERE CIA=@IDCIA AND ID=@IDUSU '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'ORDER BY FEC_PWD DESC '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDCIA INT,@IDUSU INT',@IDCIA=@IDCIA,@IDUSU=@IDUSU" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Sub V_31900_8_Datos_020()
    Dim sConsulta As String
    
    sConsulta = "UPDATE FSP_CONF_VISOR_RECEPCIONES  SET IMPORTE_ALBARAN_WIDTH =0 WHERE IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Sub V_31900_8_Tablas_020()
    Dim sConsulta As String
    
    sConsulta = "ALTER TABLE URL_EXTERNAS" & vbCrLf
    sConsulta = sConsulta & "DROP COLUMN WEBSERVICE_URL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE URL_EXTERNAS" & vbCrLf
    sConsulta = sConsulta & "DROP COLUMN WEBSERVICE_USU" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE URL_EXTERNAS" & vbCrLf
    sConsulta = sConsulta & "DROP COLUMN WEBSERVICE_PWD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USU_PWD]') AND name = N'PK_USU_PWD')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[USU_PWD] DROP CONSTRAINT [PK_USU_PWD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    

End Sub
    
Public Function CodigoDeActualizacion31900_04_18_20A31900_08_18_21() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_021
    '-------------------------------------------------------------------------------------------
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_021
    '-------------------------------------------------------------------------------------------
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.21'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_20A31900_08_18_21 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_20A31900_08_18_21 = False
    Resume Salir
End Function

Public Sub V_31900_8_Tablas_021()
    Dim sConsulta As String
    
    sConsulta = "ALTER TABLE dbo.PARGEN_GEST" & vbCrLf
    sConsulta = sConsulta & "DROP CONSTRAINT DF_PARGEN_GEST_COMPLEJIDAD_PWD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD CONSTRAINT" & vbCrLf
    sConsulta = sConsulta & "DF_PARGEN_GEST_COMPLEJIDAD_PWD DEFAULT ((0)) FOR COMPLEJIDAD_PWD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_31900_8_Storeds_021()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_PASSWORD_CHANGE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_PASSWORD_CHANGE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_PASSWORD_CHANGE]" & vbCrLf
    sConsulta = sConsulta & "   @IDCIA INT," & vbCrLf
    sConsulta = sConsulta & "   @IDUSU INT," & vbCrLf
    sConsulta = sConsulta & "   @NEWPWD VARCHAR(128)," & vbCrLf
    sConsulta = sConsulta & "   @FECPWD DATE," & vbCrLf
    sConsulta = sConsulta & "   @SALT VARCHAR(512)=NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO USU_PWD " & vbCrLf
    sConsulta = sConsulta & "   SELECT CIA,ID,PWD,FECPWD,SALT " & vbCrLf
    sConsulta = sConsulta & "   FROM USU WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   WHERE CIA=@IDCIA AND ID=@IDUSU AND PWD IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   UPDATE USU SET PWD=@NEWPWD,FECPWD=@FECPWD,SALT=@SALT,PWD_HASH=NULL" & vbCrLf
    sConsulta = sConsulta & "   WHERE CIA=@IDCIA AND ID=@IDUSU" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
 
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_REGISTRAR_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_REGISTRAR_USUARIO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[SP_REGISTRAR_USUARIO](" & vbCrLf
    sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
    sConsulta = sConsulta & "   @IDPORTAL INT, " & vbCrLf
    sConsulta = sConsulta & "   @COD VARCHAR(50), " & vbCrLf
    sConsulta = sConsulta & "    @APE VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @NOM VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @DEP VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @CAR VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO2 VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @FAX VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @EMAIL VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @IDI VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO_MOVIL VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TIPOEMAIL SMALLINT, " & vbCrLf
    sConsulta = sConsulta & "    @FCEST TINYINT, " & vbCrLf
    sConsulta = sConsulta & "    @FPEST TINYINT, " & vbCrLf
    sConsulta = sConsulta & "    @ORIGINS VARCHAR(50),  " & vbCrLf
    sConsulta = sConsulta & "    @PRINCIPAL BIT," & vbCrLf
    sConsulta = sConsulta & "    @PWD VARCHAR(128)=NULL, " & vbCrLf
    sConsulta = sConsulta & "    @FECHA DATETIME=NULL," & vbCrLf
    sConsulta = sConsulta & "    @SALT VARCHAR(512)=NULL " & vbCrLf
    sConsulta = sConsulta & ")AS" & vbCrLf
    sConsulta = sConsulta & "IF @FECHA=NULL" & vbCrLf
    sConsulta = sConsulta & "   SET @FECHA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDUSU INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDIDI INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @IDUSU=ISNULL(MAX(ID),0) + 1 " & vbCrLf
    sConsulta = sConsulta & "FROM USU" & vbCrLf
    sConsulta = sConsulta & "WHERE CIA=@CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
    sConsulta = sConsulta & "FROM IDI WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE COD = @IDI" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PWD IS NULL" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO USU (CIA,PWD,FECPWD,ID,COD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
    sConsulta = sConsulta & "       FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL,[PWD_HASH],SALT) " & vbCrLf
    sConsulta = sConsulta & "   VALUES (@CIA,NULL,@FECHA,@IDUSU,@COD,@APE,@NOM,@DEP,@CAR,@TFNO,@TFNO2," & vbCrLf
    sConsulta = sConsulta & "       @FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL,NEWID(),@SALT)" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO USU (CIA,PWD,FECPWD,ID,COD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
    sConsulta = sConsulta & "       FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL,[PWD_HASH],SALT) " & vbCrLf
    sConsulta = sConsulta & "   VALUES (@CIA,@PWD,@FECHA,@IDUSU,@COD,@APE,@NOM,@DEP,@CAR,@TFNO,@TFNO2," & vbCrLf
    sConsulta = sConsulta & "       @FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL,NULL,@SALT)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO USU_PORT (CIA,USU,PORT,FPEST,ORIGINS) " & vbCrLf
    sConsulta = sConsulta & "VALUES (@CIA,@IDUSU,@IDPORTAL,@FPEST,@ORIGINS)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PRINCIPAL = 1 " & vbCrLf
    sConsulta = sConsulta & "  UPDATE CIAS SET USUPPAL=@IDUSU WHERE ID=@CIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_04_18_21A31900_08_18_22() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_022
    '-------------------------------------------------------------------------------------------
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.22'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_21A31900_08_18_22 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_21A31900_08_18_22 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_022()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETREQUESTADJUNDATA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETREQUESTADJUNDATA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETREQUESTADJUNDATA] @CIA INT, @ID INT, @INIT INT = 0, @OFFSET INT = 0 ,@TIPO INT = 0  AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUESTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0 ',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
 
End Sub

Public Function CodigoDeActualizacion31900_04_18_22A31900_08_18_23() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_023
    '-------------------------------------------------------------------------------------------
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_023
    '-------------------------------------------------------------------------------------------
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.23'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_22A31900_08_18_23 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_22A31900_08_18_23 = False
    Resume Salir
End Function

Public Sub V_31900_8_Tablas_023()
    Dim sConsulta As String

    sConsulta = "ALTER TABLE USU_PWD ADD IDHISTORY INT NOT NULL IDENTITY (1, 1)"
    ExecuteSQL gRDOCon, sConsulta
    
End Sub

Private Sub V_31900_8_Storeds_023()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GETPASSWORD_HISTORY]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GETPASSWORD_HISTORY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_GETPASSWORD_HISTORY]" & vbCrLf
    sConsulta = sConsulta & "   @NUM_HISTORY INT," & vbCrLf
    sConsulta = sConsulta & "   @IDCIA INT," & vbCrLf
    sConsulta = sConsulta & "   @IDUSU INT" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=N'SELECT TOP '+CAST(@NUM_HISTORY AS NVARCHAR(3))+N' PWD,FEC_PWD,SALT '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM USU_PWD WITH(NOLOCK) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+N'WHERE CIA=@IDCIA AND ID=@IDUSU '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+N'ORDER BY FEC_PWD DESC,IDHISTORY DESC '" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDCIA INT,@IDUSU INT',@IDCIA=@IDCIA,@IDUSU=@IDUSU" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
 
End Sub


Public Function CodigoDeActualizacion31900_04_18_23A31900_08_18_24() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_024
    '-------------------------------------------------------------------------------------------
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.24'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_23A31900_08_18_24 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_23A31900_08_18_24 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_024()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ERRORES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ERRORES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ERRORES]" & vbCrLf
    sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
    sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECHAFIN DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECHAINI DATETIME" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@FECHAINI DATETIME, @FECHAFIN DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
    sConsulta = sConsulta & "SET @FECHAFIN=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @FECHAINI=@fecha" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING= 'EXECUTE ' + @FSGS + 'FSAL_ESTADISTICAS_ERRORES @FECHAINI, @FECHAFIN, @ENTORNO, @PYME'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION, @FECHAINI =@FECHAINI, @FECHAFIN=@FECHAFIN, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
    sConsulta = sConsulta & "  end              " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    
End Sub
Public Function CodigoDeActualizacion31900_04_18_24A31900_08_18_25() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Datos_025
    '-------------------------------------------------------------------------------------------
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.25'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_24A31900_08_18_25 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_24A31900_08_18_25 = False
    Resume Salir
End Function

Public Sub V_31900_8_Datos_025()
Dim sConsulta As String

sConsulta = "UPDATE CIAS SET COD=RTRIM(COD)" & vbCrLf
sConsulta = sConsulta & "WHERE COD LIKE '% '" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE REL_CIAS SET COD_PROVE_CIA=RTRIM(COD_PROVE_CIA)" & vbCrLf
sConsulta = sConsulta & "WHERE COD_PROVE_CIA LIKE '% '" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_04_18_25A31900_08_18_26() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_0266
    '-------------------------------------------------------------------------------------------
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.26'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_25A31900_08_18_26 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
     CodigoDeActualizacion31900_04_18_25A31900_08_18_26 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_0266()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HOY DATE" & vbCrLf
    sConsulta = sConsulta & "DECLARE @AYER DATE" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TAB AS TABLE(ID INTEGER, USU VARCHAR(100), TS VARCHAR(100), FIS DATETIME, FFS DATETIME, TRASPASO INTEGER, FSAL_ENTORNO VARCHAR(100))" & vbCrLf
    sConsulta = sConsulta & "SET @HOY= GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @AYER= DATEADD(d,-1,GETDATE())" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
    sConsulta = sConsulta & "           --CALIDAD * TIENE QUE SER AS�" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           set @SQLSTRING=N'INSERT INTO ' +@FSGS+ 'FSAL_ACCESOS(PAGINA,PRODUCTO, FISERVER, FFSERVER, TSERVER, USUARIO, TRASPASO, FECACT,FSAL_ENTORNO) SELECT ''FULLSTEP GS'' AS PAGINA, ''FULLSTEP GS'' AS PRODUCTO , FIS, FFS, DATEDIFF(MILLISECOND,FIS,FFS),USU, 0, GETDATE(),(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK))[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE TRASPASO = 0 AND FFS IS NOT NULL AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER)' " & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'SELECT * ,(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK))[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE (TRASPASO = 0 AND FFS IS NOT NULL) AND (NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER))'" & vbCrLf
    sConsulta = sConsulta & "          EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'UPDATE ' + @FSGS + 'SES SET TRASPASO = 1 WHERE TRASPASO = 0 AND FFS IS NOT NULL AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER)'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY =@HOY, @AYER =@AYER          " & vbCrLf
    sConsulta = sConsulta & "          " & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    
End Sub

Public Function CodigoDeActualizacion31900_04_18_26A31900_08_18_27() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_025
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_025
    '-------------------------------------------------------------------------------------------
    
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.27'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_26A31900_08_18_27 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_26A31900_08_18_27 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_025()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_PROVES_ERP]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_PROVES_ERP]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_PROVES_ERP] @CIA INT,@PROVE NVARCHAR(50)=NULL,@ORGCOMPRAS NVARCHAR(50)=NULL,@COD_ERP NVARCHAR(50)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_PROVES_ERP @PROVE=@PROVE,@ORGCOMPRAS=@ORGCOMPRAS,@COD_ERP=@COD_ERP'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE NVARCHAR(50)=NULL,@ORGCOMPRAS NVARCHAR(50)=NULL,@COD_ERP NVARCHAR(50)=NULL', @PROVE=@PROVE,@ORGCOMPRAS=@ORGCOMPRAS,@COD_ERP=@COD_ERP" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_REGISTRAR_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_REGISTRAR_USUARIO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_REGISTRAR_USUARIO](" & vbCrLf
    sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
    sConsulta = sConsulta & "   @IDPORTAL INT, " & vbCrLf
    sConsulta = sConsulta & "   @COD VARCHAR(50), " & vbCrLf
    sConsulta = sConsulta & "    @APE VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "    @NIF NVARCHAR(30), " & vbCrLf
    sConsulta = sConsulta & "    @NOM VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @DEP VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @CAR VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO2 VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @FAX VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @EMAIL VARCHAR(100), " & vbCrLf
    sConsulta = sConsulta & "    @IDI VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TFNO_MOVIL VARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "    @TIPOEMAIL SMALLINT, " & vbCrLf
    sConsulta = sConsulta & "    @FCEST TINYINT, " & vbCrLf
    sConsulta = sConsulta & "    @FPEST TINYINT, " & vbCrLf
    sConsulta = sConsulta & "    @ORIGINS VARCHAR(50),  " & vbCrLf
    sConsulta = sConsulta & "    @PRINCIPAL BIT," & vbCrLf
    sConsulta = sConsulta & "    @PWD VARCHAR(128)=NULL, " & vbCrLf
    sConsulta = sConsulta & "    @FECHA DATETIME=NULL," & vbCrLf
    sConsulta = sConsulta & "    @SALT VARCHAR(512)=NULL " & vbCrLf
    sConsulta = sConsulta & ")AS" & vbCrLf
    sConsulta = sConsulta & "IF @FECHA=NULL" & vbCrLf
    sConsulta = sConsulta & "   SET @FECHA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDUSU INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDIDI INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @IDUSU=ISNULL(MAX(ID),0) + 1 " & vbCrLf
    sConsulta = sConsulta & "FROM USU" & vbCrLf
    sConsulta = sConsulta & "WHERE CIA=@CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
    sConsulta = sConsulta & "FROM IDI WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE COD = @IDI" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PWD IS NULL" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO USU (CIA,PWD,FECPWD,ID,COD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
    sConsulta = sConsulta & "       FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL,[PWD_HASH],SALT,NIF) " & vbCrLf
    sConsulta = sConsulta & "   VALUES (@CIA,NULL,@FECHA,@IDUSU,@COD,@APE,@NOM,@DEP,@CAR,@TFNO,@TFNO2," & vbCrLf
    sConsulta = sConsulta & "       @FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL,NEWID(),@SALT,@NIF)" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO USU (CIA,PWD,FECPWD,ID,COD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
    sConsulta = sConsulta & "       FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL,[PWD_HASH],SALT,NIF) " & vbCrLf
    sConsulta = sConsulta & "   VALUES (@CIA,@PWD,@FECHA,@IDUSU,@COD,@APE,@NOM,@DEP,@CAR,@TFNO,@TFNO2," & vbCrLf
    sConsulta = sConsulta & "       @FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL,NULL,@SALT,@NIF)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO USU_PORT (CIA,USU,PORT,FPEST,ORIGINS) " & vbCrLf
    sConsulta = sConsulta & "VALUES (@CIA,@IDUSU,@IDPORTAL,@FPEST,@ORIGINS)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PRINCIPAL = 1 " & vbCrLf
    sConsulta = sConsulta & "  UPDATE CIAS SET USUPPAL=@IDUSU WHERE ID=@CIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_USUARIO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_USUARIO] @CIA VARCHAR(50), @USU INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT U.COD, U.PWD, U.FECPWD, U.APE, U.NOM, U.DEP, U.CAR, U.TFNO, U.TFNO2, U.FAX, U.EMAIL, U.IDI, U.FCEST, U.TFNO_MOVIL, " & vbCrLf
    sConsulta = sConsulta & "       U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, U.DATEFMT, U.MOSTRARFMT, U.TIPOEMAIL, U.MON, I.COD IDICOD, U.NIF" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  FROM USU U" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN CIAS C" & vbCrLf
    sConsulta = sConsulta & "             ON U.CIA = C.ID" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN IDI I" & vbCrLf
    sConsulta = sConsulta & "             ON U.IDI = I.ID  " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & " WHERE C.COD = @CIA" & vbCrLf
    sConsulta = sConsulta & "   AND U.ID = @USU" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Sub V_31900_8_Tablas_025()
    Dim sConsulta As String
    
    sConsulta = "IF NOT EXISTS (SELECT * FROM sys.columns WHERE Name = N'NIF' AND Object_ID = Object_ID(N'USU'))"
    sConsulta = sConsulta & "ALTER TABLE USU ADD NIF NVARCHAR(30) NULL"
    ExecuteSQL gRDOCon, sConsulta
    
End Sub

Public Function CodigoDeActualizacion31900_04_18_27A31900_08_18_28() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
        
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_027
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Datos_027
    '-------------------------------------------------------------------------------------------
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.28'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
    
        
    CodigoDeActualizacion31900_04_18_27A31900_08_18_28 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_27A31900_08_18_28 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_027()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HOY DATE" & vbCrLf
    sConsulta = sConsulta & "DECLARE @AYER DATE" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TAB AS TABLE(ID INTEGER, USU VARCHAR(100), TS VARCHAR(100), FIS DATETIME, FFS DATETIME, TRASPASO INTEGER, FSAL_ENTORNO VARCHAR(100))" & vbCrLf
    sConsulta = sConsulta & "SET @HOY= GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @AYER= DATEADD(d,-1,GETDATE())" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
    sConsulta = sConsulta & "           --CALIDAD * TIENE QUE SER AS�" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           set @SQLSTRING=N'INSERT INTO ' +@FSGS+ 'FSAL_ACCESOS(PAGINA,PRODUCTO, FISERVER, FFSERVER, TSERVER, USUARIO, TRASPASO, FECACT,FSAL_ENTORNO) SELECT ''FULLSTEP GS'' AS PAGINA, ''FULLSTEP GS'' AS PRODUCTO , FIS, FFS, DATEDIFF(MILLISECOND,FIS,FFS),USU, 0, GETDATE(),(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK))[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE TRASPASO = 0 AND FFS IS NOT NULL AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER)' " & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'SELECT * ,(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK))[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE (TRASPASO = 0 AND FFS IS NOT NULL) AND (NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER))'" & vbCrLf
    sConsulta = sConsulta & "          EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'UPDATE ' + @FSGS + 'SES SET TRASPASO = 1 WHERE TRASPASO = 0 AND FFS IS NOT NULL AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER)'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY =@HOY, @AYER =@AYER          " & vbCrLf
    sConsulta = sConsulta & "          " & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Sub V_31900_8_Datos_027()
    Dim sConsulta As String
    
    sConsulta = "UPDATE CIAS SET COD=RTRIM(COD)" & vbCrLf
    sConsulta = sConsulta & "WHERE COD LIKE '% '" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
    sConsulta = "UPDATE REL_CIAS SET COD_PROVE_CIA=RTRIM(COD_PROVE_CIA)" & vbCrLf
    sConsulta = sConsulta & "WHERE COD_PROVE_CIA LIKE '% '" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_04_18_28A31900_08_18_29() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
        
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_028
    '-------------------------------------------------------------------------------------------
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.29'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_28A31900_08_18_29 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_28A31900_08_18_29 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_028()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_OBTENER_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_OBTENER_ENTORNO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_OBTENER_ENTORNO]" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT TOP 1 COD FROM '+@FSGS +' FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HOY DATE" & vbCrLf
    sConsulta = sConsulta & "DECLARE @AYER DATE" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TAB AS TABLE(ID INTEGER, USU VARCHAR(100), TS VARCHAR(100), FIS DATETIME, FFS DATETIME, TRASPASO INTEGER, FSAL_ENTORNO VARCHAR(100))" & vbCrLf
    sConsulta = sConsulta & "SET @HOY= GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @AYER= DATEADD(d,-1,GETDATE())" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
    sConsulta = sConsulta & "           --CALIDAD * TIENE QUE SER AS�" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           set @SQLSTRING=N'INSERT INTO ' +@FSGS+ 'FSAL_ACCESOS(PAGINA,PRODUCTO, FISERVER, FFSERVER, TSERVER, USUARIO, TRASPASO, FECACT,FSAL_ENTORNO) SELECT ''FULLSTEP GS'' AS PAGINA, ''FULLSTEP GS'' AS PRODUCTO , FIS, FFS, DATEDIFF(MILLISECOND,FIS,FFS),USU, 0, GETDATE(),(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK))[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE TRASPASO = 0 AND FFS IS NOT NULL AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER)' " & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'SELECT * ,(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC)[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE (TRASPASO = 0 AND FFS IS NOT NULL) AND (NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER))'" & vbCrLf
    sConsulta = sConsulta & "          EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'UPDATE ' + @FSGS + 'SES SET TRASPASO = 1 WHERE TRASPASO = 0 AND FFS IS NOT NULL AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER)'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY =@HOY, @AYER =@AYER          " & vbCrLf
    sConsulta = sConsulta & "          " & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END        " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_04_18_29A31900_08_18_30() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
        
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_029
    '-------------------------------------------------------------------------------------------
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.30'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_29A31900_08_18_30 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_29A31900_08_18_30 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_029()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ERRORESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ERRORESATRASLADAR_FSGS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ERRORESATRASLADAR_FSGS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT * FROM ' + @FSGS + 'FSAL_ERRORES WITH(NOLOCK)WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = 'UPDATE ' + @FSGS + 'FSAL_ERRORES SET TRASPASO=1 WHERE TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HOY DATE" & vbCrLf
    sConsulta = sConsulta & "DECLARE @AYER DATE" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TAB AS TABLE(ID INTEGER, USU VARCHAR(100), TS VARCHAR(100), FIS DATETIME, FFS DATETIME, TRASPASO INTEGER, FSAL_ENTORNO VARCHAR(100))" & vbCrLf
    sConsulta = sConsulta & "SET @HOY= GETDATE()" & vbCrLf
    sConsulta = sConsulta & "SET @AYER= DATEADD(d,-1,GETDATE())" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
    sConsulta = sConsulta & "           --CALIDAD * TIENE QUE SER AS�" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           set @SQLSTRING=N'INSERT INTO ' +@FSGS+ 'FSAL_ACCESOS(PAGINA,PRODUCTO, FISERVER, FFSERVER, TSERVER, USUARIO, TRASPASO, FECACT,FSAL_ENTORNO) SELECT ''FULLSTEP GS'' AS PAGINA, ''FULLSTEP GS'' AS PRODUCTO , FIS, FFS, DATEDIFF(MILLISECOND,FIS,FFS),USU, 0, GETDATE(),(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK))[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE TRASPASO = 0 AND FFS IS NOT NULL AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER)' " & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'SELECT * ,(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC)[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE (TRASPASO = 0 AND FFS IS NOT NULL) AND (NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER))'" & vbCrLf
    sConsulta = sConsulta & "          EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'UPDATE ' + @FSGS + 'SES SET TRASPASO = 1 WHERE TRASPASO = 0 AND FFS IS NOT NULL AND NOT(CAST(FIS AS DATE)= @HOY) AND NOT(CAST(FIS AS DATE)= @AYER)'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY =@HOY, @AYER =@AYER          " & vbCrLf
    sConsulta = sConsulta & "          " & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END        " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_04_18_30A31900_08_18_31() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
        
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_031
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.31'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_30A31900_08_18_31 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_30A31900_08_18_31 = False
    Resume Salir
End Function

Public Sub V_31900_8_Tablas_031()
    Dim sConsulta As String
            
    sConsulta = "IF NOT EXISTS (SELECT * FROM sys.columns WHERE Name = N'PLANTILLA_NIF' AND Object_ID = Object_ID(N'URL_EXTERNAS'))" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE URL_EXTERNAS ADD  PLANTILLA_NIF nvarchar(1000) " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF NOT EXISTS (SELECT * FROM sys.columns WHERE Name = N'USUARIO' AND Object_ID = Object_ID(N'URL_EXTERNAS'))" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE URL_EXTERNAS ADD  USUARIO nvarchar(500) " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF NOT EXISTS (SELECT * FROM sys.columns WHERE Name = N'PWD' AND Object_ID = Object_ID(N'URL_EXTERNAS'))" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE URL_EXTERNAS ADD  PWD nvarchar(500)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE name = 'FK_URL_EXTERNAS_URL_EXTERNAS' and type='F')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[URL_EXTERNAS]  WITH CHECK ADD  CONSTRAINT [FK_URL_EXTERNAS_URL_EXTERNAS] FOREIGN KEY([MENU_PADRE]) REFERENCES [dbo].[URL_EXTERNAS] ([ID]) " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[URL_EXTERNAS] CHECK CONSTRAINT [FK_URL_EXTERNAS_URL_EXTERNAS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_04_18_31A31900_08_18_32() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
        
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_032
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.32'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_31A31900_08_18_32 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_31A31900_08_18_32 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_032()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100),@USU_NIF VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
sConsulta = sConsulta & "   -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD SIN WITH(NOLOCK) modificado desde el 09/10/2012 versi�n 31900.6 tarea 2519" & vbCrLf
sConsulta = sConsulta & "   --De poner  WITH(NOLOCK) se obtendr�a este error" & vbCrLf
sConsulta = sConsulta & "   --      Msg 16941, Level 16, State 1, Procedure SP_ACTUALIZAR_PROV_GS, Line 416" & vbCrLf
sConsulta = sConsulta & "   --      Cursor updates are not allowed on tables opened with the NOLOCK option." & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT, @USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                        WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                               @DATEFMT=@DATEFMT, @USU_NIF=@USU_NIF'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "                                     @PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                                     @USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "                                  @DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "                                  @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NIF=@USU_NIF'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50),@USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "                                     @PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                                     @USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_32A31900_08_18_33() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
        
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_033
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.33'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_32A31900_08_18_33 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_32A31900_08_18_33 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_033()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]  " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @HOY DATE" & vbCrLf
sConsulta = sConsulta & "DECLARE @AYER DATE" & vbCrLf
sConsulta = sConsulta & "DECLARE @TAB AS TABLE(ID INTEGER, USU VARCHAR(100), TS VARCHAR(100), FIS DATETIME, FFS DATETIME, TRASPASO INTEGER, FSAL_ENTORNO VARCHAR(100))" & vbCrLf
sConsulta = sConsulta & "SET @HOY= GETDATE()" & vbCrLf
sConsulta = sConsulta & "SET @AYER= DATEADD(d,-1,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "          --Se tienen que pasar desde la tabla Sesiones a FSAL_ACCESOS Aquellas sesiones que consideremos activas." & vbCrLf
sConsulta = sConsulta & "          --Eso significa que se trasladan todas aquellas sesiones que no se hayan marcado como trasladadas, es decir el campo Traslado = 0" & vbCrLf
sConsulta = sConsulta & "          --las fechas de la tabla SES ya est�n en UTC" & vbCrLf
sConsulta = sConsulta & "          --Se registra una entrada en FSAL_Accesos  con fecha de inicio y de fin similares simplemente a modo de control" & vbCrLf
sConsulta = sConsulta & "          --Si existe fecha de fin de sesi�n se utiliza la de fin de sesi�n, sino, se utiliza la de ejecuci�n de la comprobaci�n getUTCdate()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           set @SQLSTRING=N'INSERT INTO ' +@FSGS+ 'FSAL_ACCESOS(PAGINA,PRODUCTO, FISERVER, FFSERVER, TSERVER, USUARIO, TRASPASO, FECACT,FSAL_ENTORNO) SELECT ''FULLSTEP GS'' AS PAGINA, ''FULLSTEP GS'' AS PRODUCTO , Isnull(FFS,GETUTCDATE()), Isnull(FFS,GETUTCDATE()), 0,USU, 0, GETDATE(),(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK))[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE TRASPASO = 0' " & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          --Se obtienen para su traspaso a HOST(Corporate) los IDs de las entradas nuevas" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD * TIENE QUE SER AS�" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'SELECT * ,(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC)[FSAL_ENTORNO]FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE (TRASPASO = 0)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY  =@HOY, @AYER =@AYER" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          --Se ponen como procesadas aquellas sesiones finalizadas (Previamente transferidas), y aquellas que llevan mas de 12 horas abiertas." & vbCrLf
sConsulta = sConsulta & "          --" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'UPDATE ' + @FSGS + 'SES SET TRASPASO = 1 WHERE TRASPASO = 0 AND ((FFS IS NOT NULL AND FFS < GETUTCDATE()) OR (FFS IS NULL AND DATEDIFF(hh,FIS,GETUTCDATE())>=12))'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@HOY DATE,@AYER DATE', @HOY =@HOY, @AYER =@AYER          " & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END        " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    
End Sub


Public Function CodigoDeActualizacion31900_04_18_33A31900_08_18_34() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
        
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_034
        
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Datos_034
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_034
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.34'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_33A31900_08_18_34 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_33A31900_08_18_34 = False
    Resume Salir
End Function

Public Sub V_31900_8_Datos_034()
   Dim sConsulta As String

    sConsulta = "IF EXISTS(SELECT EXT FROM UPLOADEXT WHERE EXT='XLSM') " & vbCrLf
    sConsulta = sConsulta & "                UPDATE UPLOADEXT SET BLOQUEADA = 1 WHERE EXT='XLSM'" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "                INSERT INTO UPLOADEXT (EXT,BLOQUEADA,FECACT) VALUES ('XLSM',1,GETDATE())" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_8_Storeds_034()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PROCE_OFE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PROCE_OFE_ADJUN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PROCE_OFE_ADJUN] @CIA INT, @CIA_PROVE INT, @ANYO INT=NULL, @GMN1 VARCHAR(3)=NULL, @PROCE INT=NULL, @NUMOFE INT=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVE=COD FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_PROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSP_GET_PROCE_OFE_ADJUN @PROVE=@PROVE, @ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE, @NUMOFE=@NUMOFE'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50), @ANYO INT, @GMN1 VARCHAR(3), @PROCE INT, @NUMOFE INT', @PROVE=@PROVE, @ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE, @NUMOFE=@NUMOFE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta



    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USU_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[USU_LOGIN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[USU_LOGIN](" & vbCrLf
    sConsulta = sConsulta & "   @USUCOD VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @USUNEW VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @PWDNEW VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @FECUSU VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @FECPWD VARCHAR(512)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @SALT VARCHAR(512)=NULL " & vbCrLf
    sConsulta = sConsulta & ")AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECHA DATETIME" & vbCrLf
    sConsulta = sConsulta & "IF @FECPWD=NULL" & vbCrLf
    sConsulta = sConsulta & "   SET @FECHA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   SET @FECHA=CONVERT(DATETIME,@FECPWD,103)" & vbCrLf
    sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
    sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   UPDATE ADM SET USU=@USUNEW,PWD=@PWDNEW,FECPWD=@FECHA,SALT=@SALT,FECUSU=CONVERT(DATETIME,@FECUSU,103)" & vbCrLf
    sConsulta = sConsulta & "   WHERE USU=@USUCOD" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "   IF @@ERROR=0" & vbCrLf
    sConsulta = sConsulta & "     begin        " & vbCrLf
    sConsulta = sConsulta & "       COMMIT TRANSACTION" & vbCrLf
    sConsulta = sConsulta & "     end" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET XACT_ABORT OFF" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta




    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_FACTURAS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_FACTURAS_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_FACTURAS_PROVE] @CIA INT, @IDI NVARCHAR(3)," & vbCrLf
sConsulta = sConsulta & "@PROVE NVARCHAR(50),@ICONTACTO INT,@EMP INT=0,@NUM_FACTURA NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@ESTADO INT=0," & vbCrLf
sConsulta = sConsulta & "@FECHA_CONTA_DESDE DATETIME=NULL, @FECHA_CONTA_HASTA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "@PEDIDO_ERP NVARCHAR(50)=NULL, @FACTURA_ERP NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@IMPORTE_DESDE FLOAT=NULL, @IMPORTE_HASTA FLOAT=NULL," & vbCrLf
sConsulta = sConsulta & "@ALBARAN NVARCHAR(100)=NULL,@ARTICULO NVARCHAR(100)=NULL,@PARTIDAS Tabla_Uons_Partidas READONLY," & vbCrLf
sConsulta = sConsulta & "@GESTOR VARCHAR(30)=NULL,@CENTRO_COSTE NVARCHAR(50)=NULL,@NUM_CESTA INT=0,@NUM_PEDIDO INT=0,@ANYO_PEDIDO INT=0,@FAC_RECTIFICATIVA INT=0,@FAC_ORIGINAL INT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_LINEAS_PEDIDO_FACTURA INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_LINEAS_PEDIDO INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_LINEAS_PED_IMPUTACION INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_ORDEN_ENTREGA INT=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Tablas basicas para las consultas " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = N' FROM ' + @FSGS + 'FACTURA F WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN ' + @FSGS + 'EST_FACTURA EF WITH(NOLOCK) ON EF.ID = F.ESTADO" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN ' + @FSGS + 'EST_FACTURA_DEN EFD WITH(NOLOCK) ON EFD.EST_FACTURA=F.ESTADO AND EFD.IDI=''' + @IDI + '''" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN ' + @FSGS + 'INSTANCIA I WITH (NOLOCK) ON F.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN( SELECT INSTANCIA, BLOQUE FROM  ' + @FSGS + 'INSTANCIA_BLOQUE WITH (NOLOCK) WHERE ESTADO=1) IB1 ON I.ID = IB1.INSTANCIA " & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ' + @FSGS + 'PM_COPIA_BLOQUE  PCB ON PCB.ID =IB1.BLOQUE " & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ' + @FSGS + 'PM_COPIA_BLOQUE_DEN PCBD ON PCB.ID=PCBD.BLOQUE AND PCBD.IDI=@IDI " & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN (SELECT ID_FACTURA, COUNT(1) AS PAGOS FROM ' + @FSGS + 'PAGO WITH(NOLOCK) GROUP BY ID_FACTURA) P ON P.ID_FACTURA = F.ID" & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ' + @FSGS + 'EMP WITH (NOLOCK) ON F.EMPRESA=EMP.ID " & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ' + @FSGS + 'INSTANCIA_ADICIONAL_PROVE IAP WITH(NOLOCK) ON I.ID=IAP.INSTANCIA AND IAP.PROVE=@PROVE AND IAP.CONTACTO=@ICONTACTO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE =' WHERE 1=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--- CRITERIOS DE SELECCION" & vbCrLf
sConsulta = sConsulta & "--- FALTAN: PEDIDO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL AND @PROVE <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @EMP IS NOT NULL AND @EMP <> 0" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.EMPRESA=@EMP'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM_FACTURA IS NOT NULL AND @NUM_FACTURA <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.NUM=@NUM_FACTURA'" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_DESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE =@SWHERE + ' AND F.FECHA >= @FECHA_DESDE'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_HASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.FECHA <= @FECHA_HASTA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NOT NULL AND @ESTADO <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.ESTADO=@ESTADO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA_CONTA_DESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE =@SWHERE + ' AND F.FEC_CONTA >= @FECHA_CONTA_DESDE'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_CONTA_HASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE =@SWHERE + ' AND F.FEC_CONTA <= @FECHA_CONTA_HASTA'" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "IF @FACTURA_ERP IS NOT NULL AND @FACTURA_ERP <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.NUM_ERP=@FACTURA_ERP'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "IF @IMPORTE_DESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.IMPORTE>=@IMPORTE_DESDE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTE_HASTA IS NOT NULL AND @IMPORTE_HASTA<>0" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.IMPORTE<=@IMPORTE_HASTA'  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FAC_RECTIFICATIVA =1 AND @FAC_ORIGINAL =1" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.TIPO IN(1,2)'   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FAC_RECTIFICATIVA =0 AND @FAC_ORIGINAL =1" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.TIPO=1 '    " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @FAC_RECTIFICATIVA =1 AND @FAC_ORIGINAL =0" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.TIPO=2 '    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ALBARAN IS NOT NULL AND @ALBARAN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' ' + @FSGS + 'INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH(NOLOCK) ON LPF.FACTURA = F.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND LPF.ALBARAN=@ALBARAN'" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PEDIDO_ERP IS NOT NULL AND @PEDIDO_ERP <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' ' + @FSGS + 'INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID' " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' ' + @FSGS + 'INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' ' + @FSGS + 'INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.PEDIDO=LP.PEDIDO'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND NUM_PED_ERP=@PEDIDO_ERP '" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_ORDEN_ENTREGA=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ARTICULO IS NOT NULL AND @ARTICULO <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID' " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND ART_INT=@ARTICULO'" & vbCrLf
sConsulta = sConsulta & "END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMPARTIDAS AS INT = 0" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMPARTIDAS = COUNT(*) FROM @PARTIDAS" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPARTIDAS > 0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID " & vbCrLf
sConsulta = sConsulta & "                                           INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO'  " & vbCrLf
sConsulta = sConsulta & "                   SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "                   SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + '" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN @PARTIDAS PRES" & vbCrLf
sConsulta = sConsulta & "           ON PRES.UON1 = LPI.UON1" & vbCrLf
sConsulta = sConsulta & "           AND PRES.UON2 = LPI.UON2" & vbCrLf
sConsulta = sConsulta & "           AND PRES.UON3 = LPI.UON3" & vbCrLf
sConsulta = sConsulta & "           AND PRES.UON4 = LPI.UON4" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES1 = LPI.PRES1" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES2 = LPI.PRES2" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES3 = LPI.PRES3" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES4 = LPI.PRES4'" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GESTOR IS NOT NULL AND @GESTOR <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0 AND @JOIN_LINEAS_PED_IMPUTACION=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID " & vbCrLf
sConsulta = sConsulta & "                                           INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO '     " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA =1" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @JOIN_LINEAS_PEDIDO_FACTURA=1 AND @JOIN_LINEAS_PED_IMPUTACION=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO ' " & vbCrLf
sConsulta = sConsulta & "               SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' LEFT JOIN ' + @FSGS + 'PRES5_IMPORTES PRES5 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               ON PRES5.ID=LPI.PRES5_IMP '" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND PRES5.GESTOR=@GESTOR '" & vbCrLf
sConsulta = sConsulta & "END        " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CENTRO_COSTE  IS NOT NULL AND @CENTRO_COSTE  <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SEARCH INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON1 NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON2 NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON3 NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON4 NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TEXTO NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INDEX INT=1" & vbCrLf
sConsulta = sConsulta & "   DECLARE @STARTCOSTE INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SEARCH=CHARINDEX('#',@CENTRO_COSTE,0)" & vbCrLf
sConsulta = sConsulta & "   WHILE @SEARCH > 0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @TEXTO=SUBSTRING (@CENTRO_COSTE,@STARTCOSTE+1,(@SEARCH-@STARTCOSTE)-1) " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @INDEX =1" & vbCrLf
sConsulta = sConsulta & "               SET @UON1=@TEXTO " & vbCrLf
sConsulta = sConsulta & "           IF @INDEX = 2" & vbCrLf
sConsulta = sConsulta & "               SET @UON2=@TEXTO " & vbCrLf
sConsulta = sConsulta & "           IF @INDEX = 3" & vbCrLf
sConsulta = sConsulta & "               SET @UON3=@TEXTO " & vbCrLf
sConsulta = sConsulta & "           IF @INDEX = 4" & vbCrLf
sConsulta = sConsulta & "               SET @UON4=@TEXTO " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @STARTCOSTE =@SEARCH " & vbCrLf
sConsulta = sConsulta & "           SET @SEARCH=CHARINDEX('#',@CENTRO_COSTE,@SEARCH +1)" & vbCrLf
sConsulta = sConsulta & "           SET @INDEX=@INDEX + 1" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0 AND @JOIN_LINEAS_PED_IMPUTACION=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID " & vbCrLf
sConsulta = sConsulta & "                                           INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO '                                 " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA =1" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @JOIN_LINEAS_PEDIDO_FACTURA=1 AND @JOIN_LINEAS_PED_IMPUTACION=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO ' " & vbCrLf
sConsulta = sConsulta & "               SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND LPI.UON1=@UON1 AND LPI.UON2=@UON2 AND LPI.UON3=@UON3 AND LPI.UON4=@UON4 ' " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @NUM_PEDIDO  IS NOT NULL AND @NUM_PEDIDO <>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID'        " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO=0 AND @JOIN_ORDEN_ENTREGA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.PEDIDO=LP.PEDIDO'   " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_ORDEN_ENTREGA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND OE.NUM=@NUM_PEDIDO '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ANYO_PEDIDO IS NOT NULL AND @ANYO_PEDIDO<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID' " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO=0 AND @JOIN_ORDEN_ENTREGA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.PEDIDO=LP.PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_ORDEN_ENTREGA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND OE.ANYO=@ANYO_PEDIDO '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM_CESTA IS NOT NULL AND @NUM_CESTA<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID'" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'PEDIDO P WITH(NOLOCK) ON LP.PEDIDO=P.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND P.NUM=@NUM_CESTA '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' LEFT JOIN (SELECT LFD.FACTURA, COUNT(LFD.LINEA) NUMDISCR'" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' FROM ' + @FSGS + 'LIN_FACTURA_DISCRP LFD WITH(NOLOCK) GROUP BY LFD.FACTURA) DISCR ON DISCR.FACTURA = F.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=' SELECT DISTINCT 0 AS ICONO,0 AS ETAPA_PETICIONARIO,0 AS OBV,0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR,F.ID ID_FACTURA,F.NUM NUM_FACTURA ' " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ',EMP.DEN EMPRESA, F.PROVE COD_PROVE,F.FECHA,F.FEC_CONTA,F.NUM_ERP,F.IMPORTE,EFD.DEN DEN_ESTADO,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + 'I.ESTADO,PCBD.DEN SITUACION_ACTUAL,I.TRASLADADA,I.EN_PROCESO,I.ID INSTANCIA,F.TIPO, P.PAGOS, EF.ANULADO, ISNULL(IAP.SEG,0) as SEG'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ', CASE WHEN NUMDISCR > 0 THEN 1 ELSE 0 END TIENE_DISCREP '" & vbCrLf
sConsulta = sConsulta & " + @SQLFROM + @SWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PRIMERO LAS QUE ESTAN EN ESTADO BORRADOR" & vbCrLf
sConsulta = sConsulta & "--*******************************************" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_FACTURAS_BORRADOR AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Le a�adimos que este en estado peticionario y le indicamos que es una factura pendiente con 1 as Icono" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_BORRADOR=REPLACE(@SQL,'ON F.INSTANCIA=I.ID','ON F.INSTANCIA=I.ID AND I.ESTADO=0 ') " & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_BORRADOR=REPLACE(@SQL_FACTURAS_BORRADOR,'0 AS ICONO','1 AS ICONO')" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_BORRADOR=REPLACE(@SQL_FACTURAS_BORRADOR,'0 AS ETAPA_PETICIONARIO','1 AS ETAPA_PETICIONARIO')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- LUEGO LAS QUE ESTAN PENDIENTES DE ACCION POR PARTE DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "--*******************************************************************" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_FACTURAS_PENDIENTES AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM_FACTURAS_PENDIENTES AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHERE_FACTURAS_PENDIENTES AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM_FACTURAS_PENDIENTES=' INNER JOIN ' + @FSGS + 'INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID = IB.INSTANCIA  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'PM_COPIA_ACCIONES CA WITH (NOLOCK) ON IB.BLOQUE = CA.BLOQUE " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN ' + @FSGS + 'PM_COPIA_ROL_ACCION CRA WITH (NOLOCK) ON CA.ID = CRA.ACCION " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN ' + @FSGS + 'PM_COPIA_ROL CR WITH (NOLOCK) ON CRA.ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ' + @FSGS + 'INSTANCIA_EST IE WITH (NOLOCK) ON IE.ROL=CR.ID  ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Que no este trasladada y que este en la etapa PROVEEDOR, o que este trasladada al proveedor        " & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE_FACTURAS_PENDIENTES= ' AND ((IB.TRASLADADA <> 1 AND IB.ESTADO=1 AND CR.TIPO = 2 AND CR.PROVE =@PROVE) OR ( IB.TRASLADADA =1 AND IE.DESTINATARIO_PROV=@PROVE AND IB.ESTADO=1 )) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_PENDIENTES=REPLACE(@SQL,'ON F.INSTANCIA=I.ID','ON F.INSTANCIA=I.ID ' + @SQLFROM_FACTURAS_PENDIENTES )" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_PENDIENTES=REPLACE(@SQL_FACTURAS_PENDIENTES,'0 AS ICONO','1 AS ICONO') " & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_PENDIENTES=@SQL_FACTURAS_PENDIENTES + @SQLWHERE_FACTURAS_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- LUEGO EL RESTO DE FACTURAS" & vbCrLf
sConsulta = sConsulta & "--*******************************************************************" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_RESTO_FACTURAS AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHERE_RESTO_FACTURAS AS NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_ORDER_RESTO_FACTURAS AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE_RESTO_FACTURAS= ' AND I.ESTADO<>0 ' --Que no esten en etapa BORRADOR" & vbCrLf
sConsulta = sConsulta & "SET @SQL_RESTO_FACTURAS=REPLACE(@SQL,'ON F.INSTANCIA=I.ID','ON F.INSTANCIA=I.ID ' + @SQLFROM_FACTURAS_PENDIENTES )" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE_RESTO_FACTURAS= @SQLWHERE_RESTO_FACTURAS + ' AND NOT ((IB.TRASLADADA <> 1 AND IB.ESTADO=1 AND CR.TIPO = 2 AND CR.PROVE =@PROVE) OR ( IB.TRASLADADA =1 AND IE.DESTINATARIO_PROV=@PROVE AND IB.ESTADO=1 )) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL_ORDER_RESTO_FACTURAS = ' ORDER BY F.FECHA,F.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL_RESTO_FACTURAS=@SQL_RESTO_FACTURAS + @SQLWHERE_RESTO_FACTURAS + @SQL_ORDER_RESTO_FACTURAS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SE HACE LA SELECT FINAL CON LAS 3 UNION PARA SACAR ORDENADOS LOS DATOS, 1� LAS" & vbCrLf
sConsulta = sConsulta & "--FACTURAS EN ESTADO BORRADOR, LUEGO LAS QUE TIENE PENDIENTE DE ACCION Y LUEGO EL RESTO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_FINAL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FACTURAS_BORRADOR " & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FINAL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FINAL + @SQL_FACTURAS_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FINAL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FINAL + @SQL_RESTO_FACTURAS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@IDI NVARCHAR(3),@PROVE NVARCHAR(50),@ICONTACTO INT,@EMP INT=0,@NUM_FACTURA NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@ESTADO INT=0," & vbCrLf
sConsulta = sConsulta & "@FECHA_CONTA_DESDE DATETIME=NULL, @FECHA_CONTA_HASTA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "@PEDIDO_ERP NVARCHAR(50)=NULL, @FACTURA_ERP NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@IMPORTE_DESDE FLOAT=NULL, @IMPORTE_HASTA FLOAT=NULL," & vbCrLf
sConsulta = sConsulta & "@ALBARAN NVARCHAR(100)=NULL,@ARTICULO NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "@PARTIDAS Tabla_Uons_Partidas READONLY," & vbCrLf
sConsulta = sConsulta & "@GESTOR VARCHAR(30)=NULL,@CENTRO_COSTE NVARCHAR(50)=NULL,@NUM_CESTA INT=0,@NUM_PEDIDO INT=0,@ANYO_PEDIDO INT=0,@UON1 NVARCHAR(5),@UON2 NVARCHAR(5),@UON3 NVARCHAR(5),@UON4 NVARCHAR(5) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL_FINAL" & vbCrLf
sConsulta = sConsulta & ", @PARAM" & vbCrLf
sConsulta = sConsulta & ",@IDI=@IDI,@PROVE=@PROVE,@EMP=@EMP,@NUM_FACTURA=@NUM_FACTURA,@FECHA_DESDE=@FECHA_DESDE," & vbCrLf
sConsulta = sConsulta & "@FECHA_HASTA=@FECHA_HASTA,@ESTADO=@ESTADO,@FECHA_CONTA_DESDE=@FECHA_CONTA_DESDE,@PEDIDO_ERP=@PEDIDO_ERP," & vbCrLf
sConsulta = sConsulta & "@FACTURA_ERP=@FACTURA_ERP,@IMPORTE_DESDE=@IMPORTE_DESDE,@IMPORTE_HASTA=@IMPORTE_HASTA,@ALBARAN=@ALBARAN,@ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "@ICONTACTO=@ICONTACTO,@PARTIDAS=@PARTIDAS,@GESTOR=@GESTOR,@CENTRO_COSTE=@CENTRO_COSTE,@NUM_CESTA=@NUM_CESTA," & vbCrLf
sConsulta = sConsulta & "@NUM_PEDIDO=@NUM_PEDIDO,@ANYO_PEDIDO=@ANYO_PEDIDO,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4   " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @IDI AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CERTIFICADOS_PROVE @PROVE= @PROVE, @IDI = @IDI'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@IDI VARCHAR(3)',@PROVE= @PROVE, @IDI = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =118 AND ((W.ID>=9 AND W.ID<=13) OR W.ID=21 OR W.ID=27) ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  " & vbCrLf
sConsulta = sConsulta & "    @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMFACTURA VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO, @NUMPEDIDO=@NUMPEDIDO,@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE,@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP,@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO,@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA,@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE,@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE,@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES,@NUMFACTURA=@NUMFACTURA'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT, @NUMPEDIDO INT,@NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME,@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200),@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME,@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50), @PARTIDASPRES NVARCHAR(MAX), @NUMFACTURA VARCHAR(20)', " & vbCrLf
sConsulta = sConsulta & "                   @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                   @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                   @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                   @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                   @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                   @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                   @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                   @NUMFACTURA = @NUMFACTURA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT [RETRASADO_VISIBLE],[RETRASADO_WIDTH],[ALBARAN_VISIBLE],[ALBARAN_WIDTH],[FRECEP_VISIBLE],[FRECEP_WIDTH],[EMP_VISIBLE],[EMP_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[ERP_VISIBLE],[ERP_WIDTH],[PEDIDO_VISIBLE],[PEDIDO_WIDTH],[NUMPEDPROVE_VISIBLE],[NUMPEDPROVE_WIDTH],[FEMISION_VISIBLE],[FEMISION_WIDTH],[FENTREGA_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[FENTREGA_WIDTH],[FENTREGAPROVE_VISIBLE],[FENTREGAPROVE_WIDTH],[ART_VISIBLE],[ART_WIDTH],[CANTPED_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[CANTPED_WIDTH],[CANTREC_VISIBLE],[CANTREC_WIDTH],[CANTPEND_VISIBLE],[CANTPEND_WIDTH],[UNI_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[UNI_WIDTH],[PREC_VISIBLE],[PREC_WIDTH],[IMPPED_VISIBLE],[IMPPED_WIDTH],[IMPREC_VISIBLE],[IMPREC_WIDTH],[IMPPEND_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[IMPPEND_WIDTH],[MON_VISIBLE],[MON_WIDTH],[CC1_VISIBLE],[CC1_WIDTH],[CC2_VISIBLE],[CC2_WIDTH],[CC3_VISIBLE],[CC3_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[CC4_VISIBLE],[CC4_WIDTH],[PARTIDA1_VISIBLE],[PARTIDA1_WIDTH],[PARTIDA2_VISIBLE],[PARTIDA2_WIDTH],[PARTIDA3_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[PARTIDA3_WIDTH],[PARTIDA4_VISIBLE],[PARTIDA4_WIDTH], [BLOQ_FACTURA_VISIBLE],[BLOQ_FACTURA_WIDTH],[NUM_LINEA_VISIBLE],[NUM_LINEA_WIDTH],[FACTURA_VISIBLE],[FACTURA_WIDTH],[RECEPTOR_VISIBLE],[RECEPTOR_WIDTH],[IMPORTE_ALBARAN_VISIBLE],[IMPORTE_ALBARAN_WIDTH],[CODRECEPERP_VISIBLE],[CODRECEPERP_WIDTH]" & vbCrLf
sConsulta = sConsulta & "FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE USU = @IDUSU AND CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  " & vbCrLf
sConsulta = sConsulta & "@USU NVARCHAR(50) = NULL, @CIA INT = NULL, " & vbCrLf
sConsulta = sConsulta & "@RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@PREC_VISIBLE int = NULL, @PREC_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "@PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@BLOQ_FACTURA_VISIBLE INT = NULL, @BLOQ_FACTURA_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@NUM_LINEA_VISIBLE INT = NULL, @NUM_LINEA_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@FACTURA_VISIBLE INT = NULL, @FACTURA_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@RECEPTOR_VISIBLE INT = NULL, @RECEPTOR_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@IMPORTE_ALBARAN_VISIBLE INT = NULL, @IMPORTE_ALBARAN_WIDTH FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "@CODRECEPERP_VISIBLE INT=NULL, @CODRECEPERP_WIDTH FLOAT=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la informaci�n del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tama�o suficiente para ser visible pero no molestar" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               IF @CODRECEPERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CODRECEPERP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CODRECEPERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CODRECEPERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CODRECEPERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_VISIBLE = @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_WIDTH = @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_WIDTH = @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "IF NOT @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "IF NOT @CODRECEPERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CODRECEPERP_VISIBLE= @CODRECEPERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "IF NOT @CODRECEPERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CODRECEPERP_WIDTH = @CODRECEPERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT, @ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT, @PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT, @FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT, @CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, @UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT,"
sConsulta = sConsulta & "@CC2_WIDTH FLOAT,@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @BLOQ_FACTURA_VISIBLE INT, @BLOQ_FACTURA_WIDTH FLOAT, @NUM_LINEA_VISIBLE INT, @NUM_LINEA_WIDTH FLOAT, @FACTURA_VISIBLE INT, @FACTURA_WIDTH FLOAT, @RECEPTOR_VISIBLE INT, @RECEPTOR_WIDTH FLOAT, @IMPORTE_ALBARAN_VISIBLE INT, @IMPORTE_ALBARAN_WIDTH FLOAT,@CODRECEPERP_VISIBLE INT, @CODRECEPERP_WIDTH FLOAT,@ANCHODEFECTO INT'," & vbCrLf
sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_VISIBLE = @FACTURA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_WIDTH = @FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_WIDTH = @RECEPTOR_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @CODRECEPERP_VISIBLE = @CODRECEPERP_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @CODRECEPERP_WIDTH = @CODRECEPERP_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_31900_8_Tablas_034()
Dim sConsulta As String

sConsulta = "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD CODRECEPERP_VISIBLE TINYINT NOT NULL CONSTRAINT DF_FSP_CONF_VISOR_RECEPCIONES_CODRECEPERP_VISIBLE DEFAULT 1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD CODRECEPERP_WIDTH TINYINT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_34A31900_08_18_35() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
                
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_035
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.35'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_34A31900_08_18_35 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_34A31900_08_18_35 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_035()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE] " & vbCrLf
    sConsulta = sConsulta & "   @CIA INT," & vbCrLf
    sConsulta = sConsulta & "   @PROVE VARCHAR(50), " & vbCrLf
    sConsulta = sConsulta & "   @IDI AS VARCHAR(20)" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @IDPROVE INT" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CERTIFICADOS_PROVE @PROVE= @PROVE, @IDI = @IDI'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@IDI VARCHAR(3)',@PROVE= @PROVE, @IDI = @IDI" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =118 AND ((W.ID>=9 AND W.ID<=13) OR W.ID=21 OR W.ID=27 OR W.ID=28) ORDER BY W.ID ASC'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_35A31900_08_18_36() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
          
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Tablas_036
          
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_036
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.36'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_35A31900_08_18_36 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_35A31900_08_18_36 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_036()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  " & vbCrLf
    sConsulta = sConsulta & "@USU NVARCHAR(50) = NULL, @CIA INT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PREC_VISIBLE int = NULL, @PREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@BLOQ_FACTURA_VISIBLE INT = NULL, @BLOQ_FACTURA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@NUM_LINEA_VISIBLE INT = NULL, @NUM_LINEA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FACTURA_VISIBLE INT = NULL, @FACTURA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@RECEPTOR_VISIBLE INT = NULL, @RECEPTOR_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPORTE_ALBARAN_VISIBLE INT = NULL, @IMPORTE_ALBARAN_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CODRECEPERP_VISIBLE INT=NULL, @CODRECEPERP_WIDTH FLOAT=NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
    sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
    sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
    sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
    sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la informaci�n del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tama�o suficiente para ser visible pero no molestar" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
    sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
    sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUM_LINEA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUM_LINEA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUM_LINEA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FACTURA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @RECEPTOR_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RECEPTOR_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @RECEPTOR_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RECEPTOR_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "               IF @CODRECEPERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CODRECEPERP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CODRECEPERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CODRECEPERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CODRECEPERP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CODRECEPERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
    sConsulta = sConsulta & "           END" & vbCrLf
    sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUM_LINEA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_VISIBLE = @FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_WIDTH = @FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RECEPTOR_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RECEPTOR_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_WIDTH = @RECEPTOR_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @CODRECEPERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CODRECEPERP_VISIBLE= @CODRECEPERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @CODRECEPERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CODRECEPERP_WIDTH = @CODRECEPERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
    sConsulta = sConsulta & "           END" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
    sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT,"
    sConsulta = sConsulta & "@ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT,"
    sConsulta = sConsulta & "@PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, "
    sConsulta = sConsulta & "@FEMISION_WIDTH FLOAT, @FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, "
    sConsulta = sConsulta & "@ART_VISIBLE INT, @ART_WIDTH FLOAT, @CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, "
    sConsulta = sConsulta & "@CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, @UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT, @CC2_WIDTH FLOAT,@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @BLOQ_FACTURA_VISIBLE INT, @BLOQ_FACTURA_WIDTH FLOAT, @NUM_LINEA_VISIBLE INT, @NUM_LINEA_WIDTH FLOAT, @FACTURA_VISIBLE INT, @FACTURA_WIDTH FLOAT, @RECEPTOR_VISIBLE INT, @RECEPTOR_WIDTH FLOAT, @IMPORTE_ALBARAN_VISIBLE INT, @IMPORTE_ALBARAN_WIDTH FLOAT,@CODRECEPERP_VISIBLE INT, @CODRECEPERP_WIDTH FLOAT,@ANCHODEFECTO INT'," & vbCrLf
    sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
    sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
    sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @FACTURA_VISIBLE = @FACTURA_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @FACTURA_WIDTH = @FACTURA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @RECEPTOR_WIDTH = @RECEPTOR_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @CODRECEPERP_VISIBLE = @CODRECEPERP_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @CODRECEPERP_WIDTH = @CODRECEPERP_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_8_Tablas_036()
    Dim sConsulta As String
    
    sConsulta = "IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'CODRECEPERP_WIDTH' AND Object_ID = Object_ID(N'FSP_CONF_VISOR_RECEPCIONES'))" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ALTER COLUMN CODRECEPERP_WIDTH float null" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_36A31900_08_18_37() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
          
          
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_037
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.37'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_36A31900_08_18_37 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_36A31900_08_18_37 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_037()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETDESGLOSE] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDESGLOSE @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD, @PORTAL=1'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_04_18_37A31900_08_18_38() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
          
          
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_038
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.38'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_37A31900_08_18_38 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_37A31900_08_18_38 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_038()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(MAX),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100),@USU_NIF VARCHAR(30)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT) " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
    sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
    sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
    sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
    sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
    sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
    sConsulta = sConsulta & "   -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --CALIDAD SIN WITH(NOLOCK) modificado desde el 09/10/2012 versi�n 31900.6 tarea 2519" & vbCrLf
    sConsulta = sConsulta & "   --De poner  WITH(NOLOCK) se obtendr�a este error" & vbCrLf
    sConsulta = sConsulta & "   --      Msg 16941, Level 16, State 1, Procedure SP_ACTUALIZAR_PROV_GS, Line 416" & vbCrLf
    sConsulta = sConsulta & "   --      Cursor updates are not allowed on tables opened with the NOLOCK option." & vbCrLf
    sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
    sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
    sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
    sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
    sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
    sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
    sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
    sConsulta = sConsulta & "                       FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
    sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "          BEGIN" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
    sConsulta = sConsulta & "            BEGIN" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
    sConsulta = sConsulta & "                BEGIN" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
    sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
    sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
    sConsulta = sConsulta & "                   FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
    sConsulta = sConsulta & "                        LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
    sConsulta = sConsulta & "                        LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
    sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
    sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "                    BEGIN" & vbCrLf
    sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
    sConsulta = sConsulta & "                    " & vbCrLf
    sConsulta = sConsulta & "                                                   " & vbCrLf
    sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
    sConsulta = sConsulta & "                       " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
    sConsulta = sConsulta & "                 END       " & vbCrLf
    sConsulta = sConsulta & "              END                                          " & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "                " & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "      ELSE  " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
    sConsulta = sConsulta & "         BEGIN" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
    sConsulta = sConsulta & "             BEGIN" & vbCrLf
    sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
    sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "             END " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
    sConsulta = sConsulta & "             BEGIN" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @USU_NIF = NULL" & vbCrLf
    sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
    sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT, @USU_NIF=NIF" & vbCrLf
    sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
    sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
    sConsulta = sConsulta & "                        WHERE " & vbCrLf
    sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
    sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
    sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
    sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
    sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
    sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
    sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
    sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
    sConsulta = sConsulta & "                               @DATEFMT=@DATEFMT, @USU_NIF=@USU_NIF'    " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
    sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_NIF VARCHAR(30)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
    sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "                                     @PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
    sConsulta = sConsulta & "                                     @USU_NIF=@USU_NIF" & vbCrLf
    sConsulta = sConsulta & "                 END                " & vbCrLf
    sConsulta = sConsulta & "           END " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @USU_NIF = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
    sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=NIF" & vbCrLf
    sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
    sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
    sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
    sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
    sConsulta = sConsulta & "                 BEGIN" & vbCrLf
    sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
    sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
    sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT," & vbCrLf
    sConsulta = sConsulta & "                                  @DECIMALFMT=@DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "                                  @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
    sConsulta = sConsulta & "                                  @DATEFMT=@DATEFMT," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_NIF=@USU_NIF'    " & vbCrLf
    sConsulta = sConsulta & "                                       " & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
    sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
    sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50),@USU_NIF VARCHAR(30)'" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
    sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "                                     @PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
    sConsulta = sConsulta & "                                     @USU_NIF=@USU_NIF" & vbCrLf
    sConsulta = sConsulta & "                   END                       " & vbCrLf
    sConsulta = sConsulta & "               END " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           END " & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
    sConsulta = sConsulta & "     BEGIN   " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
    sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
    sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
    sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
    sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
    sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
    sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
    sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
    sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
    sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
    sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
    sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
    sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
    sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
    sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
    sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
    sConsulta = sConsulta & "                   END        " & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
    sConsulta = sConsulta & "                  BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
    sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
    sConsulta = sConsulta & "                                      " & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
    sConsulta = sConsulta & "                                                           " & vbCrLf
    sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                  END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
    sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
    sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
    sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
    sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
    sConsulta = sConsulta & "                                       " & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
    sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
    sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
    sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
    sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
    sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
    sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
    sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
    sConsulta = sConsulta & "                   END     " & vbCrLf
    sConsulta = sConsulta & "                END" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "           END " & vbCrLf
    sConsulta = sConsulta & "            " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
    sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
    sConsulta = sConsulta & "             ELSE" & vbCrLf
    sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "        " & vbCrLf
    sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
    sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "     " & vbCrLf
    sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
    sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_18_38A31900_08_18_39() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
          
          
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_8_Storeds_039
        
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.39'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_04_18_38A31900_08_18_39 = True
    
Salir:
    Set oADOConnection = Nothing
    Exit Function
error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_04_18_38A31900_08_18_39 = False
    Resume Salir
End Function

Private Sub V_31900_8_Storeds_039()
    Dim sConsulta As String
    gRDOCon.QueryTimeout = 7200

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           --si no estan traspasadas (TRASPASO=0) a FSAL_ACCESOS" & vbCrLf
    sConsulta = sConsulta & "           set @SQLSTRING=N'INSERT INTO ' +@FSGS+ 'FSAL_ACCESOS (PAGINA,PRODUCTO,FISERVER,FFSERVER, TSERVER, USUARIO, TRASPASO, FECACT,FSAL_ENTORNO) SELECT  ''FULLSTEP GS'' AS PAGINA, ''FULLSTEP GS'' AS PRODUCTO, FIS AS FISERVER, Isnull(FFS,null) AS FFSERVER, 0 as TSERVER, USU as USUARIO, 0 as TRASPASO, GETDATE() as FECACT,(SELECT TOP 1 COD FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC)[FSAL_ENTORNO] FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE FIS is not null AND TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           --Si est�n traspasadas (TRASPASO=2) pero se ha cerrado la sesion o han pasado mas de 8 horas de esa sesion" & vbCrLf
    sConsulta = sConsulta & "           set @SQLSTRING=N'UPDATE ' + @FSGS + 'FSAL_ACCESOS SET FFSERVER=ISNULL(SES.FFS,DATEADD(HH,8,FSAL_ACCESOS.FISERVER)), TRASPASO=0 FROM ' + @FSGS + 'FSAL_ACCESOS INNER JOIN ' + @FSGS + 'SES ON SES.TRASPASO=2 AND FSAL_ACCESOS.PAGINA=''FULLSTEP GS'' AND FSAL_ACCESOS.PRODUCTO =''FULLSTEP GS'' AND FSAL_ACCESOS.FSAL_ENTORNO=(SELECT TOP 1 COD FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC) AND FSAL_ACCESOS.USUARIO=SES.USU AND FSAL_ACCESOS.FISERVER = SES.FIS WHERE ((SES.TRASPASO =2 AND (SES.FFS IS NOT NULL)) OR ((SES.TRASPASO =2 AND (SES.FFS IS NULL) AND (datediff(HH,SES.FIS,getdate())>=8))))'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           --select @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           --CALIDAD * TIENE QUE SER AS�       " & vbCrLf
    sConsulta = sConsulta & "           ---cojo los campos que aun no se hayan traspasado (traspaso=0) o los que tengan la fecha fin y el traspaso = 2" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'SELECT * ,(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC)[FSAL_ENTORNO] FROM ' + @FSGS + 'SES WITH(NOLOCK) WHERE FIS is not null AND TRASPASO = 0 OR ((TRASPASO =2 AND (FFS IS NOT NULL)) OR ((TRASPASO =2 AND (FFS IS NULL) AND (datediff(HH,FIS,getdate())>=8))))' --datediff de gestdate y ffis sea mayor igual que 8" & vbCrLf
    sConsulta = sConsulta & "           --select @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'UPDATE' + @FSGS + 'SES SET TRASPASO=TRASPASO+1 WHERE FIS is not null AND TRASPASO = 0 OR ((TRASPASO =2 AND (FFS IS NOT NULL)) OR ((TRASPASO =2 AND (FFS IS NULL) AND (datediff(HH,FIS,getdate())>=8))))' --datediff de gestdate y ffis sea mayor igual que 8" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           --select @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO] @fecha DATETIME,@entorno nvarchar(50),@pyme nvarchar(50)=null AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
    sConsulta = sConsulta & "SET @HASTA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_ESTADISTICAS_ENTORNO @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)',@DESDE=@DESDE,@HASTA=@HASTA,@ENTORNO=@ENTORNO,@PYME=@PYME" & vbCrLf
    sConsulta = sConsulta & "  END  " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    
End Sub
