Attribute VB_Name = "bas_V_2_16_5"
Option Explicit
Public vMaxAdjun As Variant
Private dblMaxAdjun As Double

Public Function CodigoDeActualizacion2_16_00_00A2_16_05_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

On Error GoTo 0
    frmMaxAdjun.Show 1
    
    If IsNull(vMaxAdjun) Or IsEmpty(vMaxAdjun) Or vMaxAdjun = "" Then
        CodigoDeActualizacion2_16_00_00A2_16_05_00 = False
        Exit Function
    End If

    dblMaxAdjun = CDbl(vMaxAdjun) * 1024

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    
    V_2_16_5_Tablas_0
    
    frmProgreso.lblDetalle = "Cambios en stored procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    
    V_2_16_5_Storeds_0
    
    frmProgreso.lblDetalle = "Cambios en stored procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    
    V_2_16_5_Triggers_0
    

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.16.05.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_16_00_00A2_16_05_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_00_00A2_16_05_00 = False
End Function


Private Sub V_2_16_5_Storeds_0()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ACT1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ACT1_COD (@ID INT,@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT1 SET COD=@NEW WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ACT2_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ACT2_COD (@ACT1 INT,@ID INT,@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT2 SET COD=@NEW WHERE ACT1=@ACT1 AND ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ACT3_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ACT3_COD (@ACT1 INT,@ACT2 INT,@ID INT,@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT3 SET COD=@NEW WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ACT4_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ACT4_COD (@ACT1 INT,@ACT2 INT,@ACT3 INT,@ID INT,@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT4 SET COD=@NEW WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3 AND ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIAS_ACT1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIAS_ACT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_CIAS_ACT1 (@CIACOD varchar(50), @GMN1 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT1 (CIA,ACT1) VALUES (@ID_CIA ,@ID_ACT1)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIAS_ACT2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIAS_ACT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_CIAS_ACT2 (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT2 (CIA,ACT1,ACT2) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIAS_ACT3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIAS_ACT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_CIAS_ACT3 (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT3=(SELECT ID FROM ACT3 WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT3 (CIA,ACT1,ACT2,ACT3) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 , @ID_ACT3 )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIAS_ACT4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIAS_ACT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_CIAS_ACT4 (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50),@GMN4 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT4 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT3=(SELECT ID FROM ACT3 WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT4=(SELECT ID FROM ACT4 WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND COD=@GMN4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT4 (CIA,ACT1,ACT2,ACT3,ACT4) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 , @ID_ACT3 , @ID_ACT4)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_CIAS_ACT1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_CIAS_ACT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIM_CIAS_ACT1 (@CIACOD varchar(50), @GMN1 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_CIAS_ACT2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_CIAS_ACT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIM_CIAS_ACT2 (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL " & vbCrLf
sConsulta = sConsulta & " DELETE FROM CIAS_ACT2 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @ID_ACT2=(SELECT ID FROM ACT2 WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM CIAS_ACT2 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_CIAS_ACT3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_CIAS_ACT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIM_CIAS_ACT3 (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin    " & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT2=(SELECT ID FROM ACT2 WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "      IF @GMN3 IS NULL" & vbCrLf
sConsulta = sConsulta & "         begin" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2" & vbCrLf
sConsulta = sConsulta & "         end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         begin" & vbCrLf
sConsulta = sConsulta & "   SET @ID_ACT3=(SELECT ID FROM ACT3 WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3" & vbCrLf
sConsulta = sConsulta & "         end" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_CIAS_ACT4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_CIAS_ACT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIM_CIAS_ACT4 (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50),@GMN4 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT4 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT2=(SELECT ID FROM ACT2 WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      IF @GMN3 IS NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT3=(SELECT ID FROM ACT3 WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "        IF @GMN4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 " & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "            SET @ID_ACT4=(SELECT ID FROM ACT4 WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND COD=@GMN4)" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND ACT4=@ID_ACT4" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_ESPECIFICACION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_ESPECIFICACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DOWNLOAD_ESPECIFICACION @INIT INT, @OFFSET INT, @ID INT,@CIA INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM CIAS_ESP WHERE CIA=@CIA AND ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT CIAS_ESP.data @textpointer @INIT  @LOFFSET" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NUEVA_ESP_CIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NUEVA_ESP_CIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_NUEVA_ESP_CIA @CIA int, @DATA IMAGE,@NOM VARCHAR(300),@COM VARCHAR(600), @MAXID int OUTPUT, @DATASIZE int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MAXID = 1" & vbCrLf
sConsulta = sConsulta & "select @MAXID = ISNULL(ID,0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM CIAS_ESP " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA,ID,NOM, COM,DATA) VALUES (@CIA,@MAXID,@NOM, @COM,@DATA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WHERE ID = @MAXID AND CIA=@CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_5_Tablas_0()
Dim sConsulta As String

    'Tabla PARGEN_INTERNO
    sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD" & vbCrLf
    sConsulta = sConsulta & "     [ACTIVAR_REGISTRO_ACTIV] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_ACTIVAR_REGISTRO_ACTIV] DEFAULT 0" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    'Tabla PARGEN_GEST
    sConsulta = "  ALTER TABLE [dbo].[PARGEN_GEST] ADD" & vbCrLf
    sConsulta = sConsulta & "    [MAX_ADJUN_CIAS] [float] NOT NULL CONSTRAINT [DF_PARGEN_GEST_MAX_ADJUN_CIAS] DEFAULT (0) " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    'Tabla CIAS
    sConsulta = "  ALTER TABLE [dbo].[CIAS] ADD" & vbCrLf
    sConsulta = sConsulta & "    [MAX_ADJUN] [float] NOT NULL CONSTRAINT [DF_CIAS_MAX_ADJUN] DEFAULT (0) " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = " UPDATE PARGEN_GEST SET  MAX_ADJUN_CIAS=" & DblToSQLFloat(dblMaxAdjun)
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = " UPDATE CIAS SET  MAX_ADJUN=" & DblToSQLFloat(dblMaxAdjun)
    ExecuteSQL gRDOCon, sConsulta
    
End Sub

Private Sub V_2_16_5_Triggers_0()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [CIAS_TG_INS] ON dbo.CIAS " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_Ins CURSOR FOR SELECT ID,FCEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Ins INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT UNA_COMPRADORA FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @EST=3 " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT count(*) FROM CIAS WHERE CIAS.FCEST=3) > 1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           RAISERROR (50005,16,1)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS SET MAX_ADJUN=(SELECT MAX_ADJUN_CIAS FROM PARGEN_GEST)" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS SET FECINS=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_Ins INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
