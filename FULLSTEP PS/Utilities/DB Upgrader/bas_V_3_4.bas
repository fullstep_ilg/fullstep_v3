Attribute VB_Name = "bas_V_3_4"
Public Function CodigoDeActualizacion32200_01_29_00_A32200_01_29_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_4_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.29.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32200_01_29_00_A32200_01_29_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32200_01_29_00_A32200_01_29_01 = False
End Function

Private Sub V_3_4_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMNS_PROVE_PORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMNS_PROVE_PORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GMNS_PROVE_PORTAL] @GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) = 'SPA' AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COD, COD + '' - '' + DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "FROM ACT1 WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD, ACT2.COD + '' - '' + ACT2.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT2.ACT1 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD GMN2, ACT3.COD, ACT3.COD + '' - '' + ACT3.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "FROM ACT3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 WITH(NOLOCK) ON ACT2.ID=ACT3.ACT2 AND ACT2.ACT1=ACT3.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT3.ACT1 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD GMN2, ACT3.COD GMN3, ACT4.COD, ACT4.COD + '' - '' + ACT4.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "FROM ACT4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 WITH(NOLOCK) ON ACT3.ID=ACT4.ACT3 AND ACT3.ACT2=ACT4.ACT2 AND ACT3.ACT1=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 WITH(NOLOCK) ON ACT2.ID=ACT3.ACT2 AND ACT2.ACT1=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT4.ACT1 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub
Public Function CodigoDeActualizacion32200_01_29_01_A32200_01_29_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_4_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.29.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32200_01_29_01_A32200_01_29_02 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32200_01_29_01_A32200_01_29_02 = False
End Function

Private Sub V_3_4_Storeds_002()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARGEN_INTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET @SQL='SELECT PI.ACCESO_FSSM, PI.VER_NUMEXT, PI.ACCESO_FSGS FROM ' + @FSGS + 'PARGEN_INTERNO PI WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------------" & vbCrLf
sConsulta = sConsulta & "--     LO Q ESTA CHECK PIDE ACCESO_FSFA,   LO Q HAB?A ES LO DE ARRIBA" & vbCrLf
sConsulta = sConsulta & "-------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT PI.ACCESO_FSSM, PI.VER_NUMEXT, PI.ACCESO_FSGS, ACCESO_FSFA,FIRMA_DIG_OFERTAS,MOSTRAR_TODOS_NIVELES_MATERIAL FROM ' + @FSGS + 'PARGEN_INTERNO PI WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32200_01_29_01_A32200_01_29_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_4_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.29.03'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32200_01_29_02_A32200_01_29_03 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32200_01_29_02_A32200_01_29_03 = False
End Function

Private Sub V_3_4_Storeds_003()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMNS_PROVE_PORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMNS_PROVE_PORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GMNS_PROVE_PORTAL]" & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN2 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN3 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN4 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50) = 'SPA'" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT COD, COD + '' - '' + DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR COD=@GMN1)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD, ACT2.COD + '' - '' + ACT2.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT2.ACT1" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD GMN2, ACT3.COD, ACT3.COD + '' - '' + ACT3.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT2 WITH(NOLOCK) ON ACT2.ID=ACT3.ACT2 AND ACT2.ACT1=ACT3.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT3.ACT1" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2) AND (@GMN3 IS NULL OR ACT3.COD=@GMN3)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD GMN2, ACT3.COD GMN3, ACT4.COD, ACT4.COD + '' - '' + ACT4.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT3 WITH(NOLOCK) ON ACT3.ID=ACT4.ACT3 AND ACT3.ACT2=ACT4.ACT2 AND ACT3.ACT1=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT2 WITH(NOLOCK) ON ACT2.ID=ACT3.ACT2 AND ACT2.ACT1=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2) AND (@GMN3 IS NULL OR ACT3.COD=@GMN3) AND (@GMN4 IS NULL OR ACT4.COD=@GMN4)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


End Sub
