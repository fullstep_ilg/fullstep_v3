VERSION 5.00
Begin VB.Form frmMaxAdjun 
   BackColor       =   &H00808000&
   Caption         =   "Tama�o m�ximo para adjuntos "
   ClientHeight    =   1680
   ClientLeft      =   4890
   ClientTop       =   5130
   ClientWidth     =   4860
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   1680
   ScaleWidth      =   4860
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2610
      TabIndex        =   3
      Top             =   1170
      Width           =   1470
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   870
      TabIndex        =   2
      Top             =   1170
      Width           =   1470
   End
   Begin VB.TextBox txtCod 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   2580
      TabIndex        =   0
      Top             =   675
      Width           =   1575
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Introduzca el tama�o m�ximo para los adjuntos de las compa��as actuales"
      ForeColor       =   &H00FFFFFF&
      Height          =   465
      Left            =   135
      TabIndex        =   4
      Top             =   90
      Width           =   4650
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Tama�o m�ximo (Kb):"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   600
      TabIndex        =   1
      Top             =   720
      Width           =   1890
   End
End
Attribute VB_Name = "frmMaxAdjun"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
If txtCod.Text = "" Then
    MsgBox "Se deben itroducir el dato", vbCritical, "FULLSTEP"
    Exit Sub
End If
If Not IsNumeric(txtCod.Text) Then
    MsgBox "El dato debe ser num�rico", vbCritical, "FULLSTEP"
    Exit Sub
End If

vMaxAdjun = Me.txtCod

Unload Me

End Sub

Private Sub cmdCancelar_Click()

vMaxAdjun = ""

Unload Me

End Sub
