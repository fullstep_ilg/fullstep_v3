Attribute VB_Name = "bas_V_31900_7"
Option Explicit

Public Function CodigoDeActualizacion31900_03_16_03A31900_04_17_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Tablas_000
        
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_03A31900_04_17_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_03A31900_04_17_00 = False
End Function

Private Sub V_31900_7_Tablas_000()
Dim sConsulta As String

sConsulta = "IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TablaPartidas' AND ss.name = N'dbo')" & vbCrLf
sConsulta = sConsulta & "   CREATE TYPE TablaPartidas AS TABLE (PRES0 NVARCHAR(200),  PRES1 NVARCHAR(200),  PRES2 NVARCHAR(200),  PRES3 NVARCHAR(200),  PRES4 NVARCHAR(200))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD " & vbCrLf
sConsulta = sConsulta & "[BLOQ_FACTURA_VISIBLE] TINYINT NOT NULL DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "[BLOQ_FACTURA_WIDTH] FLOAT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD " & vbCrLf
sConsulta = sConsulta & "[NUM_LINEA_VISIBLE] TINYINT NOT NULL DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "[NUM_LINEA_WIDTH] FLOAT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD " & vbCrLf
sConsulta = sConsulta & "[FACTURA_VISIBLE] TINYINT NOT NULL DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "[FACTURA_WIDTH] FLOAT NULL," & vbCrLf
sConsulta = sConsulta & "[RECEPTOR_VISIBLE] TINYINT NOT NULL DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "[RECEPTOR_WIDTH] FLOAT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'Tabla_Uons_Partidas' AND ss.name = N'dbo')" & vbCrLf
sConsulta = sConsulta & "      CREATE TYPE [dbo].[Tabla_Uons_Partidas] AS TABLE(" & vbCrLf
sConsulta = sConsulta & "      [UON1] [nvarchar](200) NULL," & vbCrLf
sConsulta = sConsulta & "      [UON2] [nvarchar](200) NULL," & vbCrLf
sConsulta = sConsulta & "      [UON3] [nvarchar](200) NULL," & vbCrLf
sConsulta = sConsulta & "      [UON4] [nvarchar](200) NULL," & vbCrLf
sConsulta = sConsulta & "      [PRES0] [nvarchar](200) NULL," & vbCrLf
sConsulta = sConsulta & "      [PRES1] [nvarchar](200) NULL," & vbCrLf
sConsulta = sConsulta & "      [PRES2] [nvarchar](200) NULL," & vbCrLf
sConsulta = sConsulta & "      [PRES3] [nvarchar](200) NULL," & vbCrLf
sConsulta = sConsulta & "      [PRES4] [nvarchar](200) NULL" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub

Private Sub V_31900_7_Storeds_000()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  @IDIOMA NVARCHAR(20)='SPA', @CIA NVARCHAR(100) = NULL, @PROVE NVARCHAR(100) = NULL, @ANYO INT = NULL, @NUMPEDIDO INT = NULL, @NUMORDEN INT = NULL, @NUMPEDIDOERP NVARCHAR(50) = NULL, @FECRECEPDESDE DATETIME = NULL, @FECRECEPHASTA DATETIME = NULL, @VERPEDIDOSSINRECEP BIT = 1, @NUMALBARAN VARCHAR(100) = NULL, @NUMPEDPROVE VARCHAR(100) = NULL, @ARTICULO VARCHAR(200) = NULL, @FECENTREGASOLICITDESDE DATETIME = NULL, @FECENTREGASOLICITHASTA DATETIME = NULL, @NUMFACTURA VARCHAR(100) = NULL, @CODEMPRESA INT = NULL, @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL, @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL, @FECEMISIONDESDE DATETIME = NULL, @FECEMISIONHASTA DATETIME = NULL, @CENTRO VARCHAR(50) = NULL, @PARTIDASPRES TablaPartidas READONLY" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECRECEPHASTA_MAS1 DATETIME " & vbCrLf
sConsulta = sConsulta & "SET @FECRECEPHASTA_MAS1 = DATEADD(dd,1,@FECRECEPHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECENTREGASOLICITHASTA = DATEADD(dd,1,@FECENTREGASOLICITHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECENTREGAINDICADAPROVEHASTA = DATEADD(dd,1,@FECENTREGAINDICADAPROVEHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECEMISIONHASTA = DATEADD(dd,1,@FECEMISIONHASTA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) -- BASE DE DATOS DEL CLIENTE" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLQUERY AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_SELECT01 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_SELECT02 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_FIN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_PARTIDA_COD AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_NIVEL_DEN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_CCCOD AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_CCDEN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLWHERE AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_SELECT01 = 'SELECT " & vbCrLf
sConsulta = sConsulta & "       LP.ID LINEAPEDID, LR.ID LINEARECEPID'   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_SELECT02 = '" & vbCrLf
sConsulta = sConsulta & "       ,PR.ALBARAN, " & vbCrLf
sConsulta = sConsulta & "       PR.FECHA FECHARECEP, " & vbCrLf
sConsulta = sConsulta & "       PR.IM_BLOQUEO, PR.IM_BLOQUEO_OBS," & vbCrLf
sConsulta = sConsulta & "       OE.EMPRESA IDEMPRESA, E.DEN DENEMPRESA, " & vbCrLf
sConsulta = sConsulta & "       OE.NUM_PED_ERP, " & vbCrLf
sConsulta = sConsulta & "       OE.ANYO, PE.NUM PEDIDONUM, OE.NUM ORDENNUM, " & vbCrLf
sConsulta = sConsulta & "       CAST(OE.ANYO as VARCHAR(20)) + CAST(''/'' as VARCHAR(3)) + CAST(PE.NUM as VARCHAR(100)) + CAST(''/'' as VARCHAR(3)) + CAST(OE.NUM as VARCHAR(100)) ANYOPEDIDOORDEN, " & vbCrLf
sConsulta = sConsulta & "       OE.ID ORDENID, OE.NUMEXT NUMPEDPROVE, " & vbCrLf
sConsulta = sConsulta & "       OE.RECEPTOR RECEPTOR," & vbCrLf
sConsulta = sConsulta & "       PE.FECHA FECHAEMISION," & vbCrLf
sConsulta = sConsulta & "       LP.NUM NUMLINEA," & vbCrLf
sConsulta = sConsulta & "       LP.FECENTREGAPROVE FECENTREGAINDICADAPROVE," & vbCrLf
sConsulta = sConsulta & "       LP.FECENTREGA FECENTREGASOLICITADA," & vbCrLf
sConsulta = sConsulta & "       LP.ABONO_FAC FACTURAID," & vbCrLf
sConsulta = sConsulta & "       F.NUM FACTURA," & vbCrLf
sConsulta = sConsulta & "       F.INSTANCIA INSTANCIA," & vbCrLf
sConsulta = sConsulta & "       I.ART ARTCOD, I.DESCR ARTDEN," & vbCrLf
sConsulta = sConsulta & "       LP.CANT_PED CANTLINEAPEDIDATOTAL," & vbCrLf
sConsulta = sConsulta & "       LR.CANT CANTLINEARECIBIDAPARCIAL," & vbCrLf
sConsulta = sConsulta & "       (LP.CANT_PED - SUMAPORLINEA.CANTRECIBIDATOTAL) CANTLINEAPENDIENTETOTAL," & vbCrLf
sConsulta = sConsulta & "       LP.UP UNICOD, UD.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "       LP.PREC_UP," & vbCrLf
sConsulta = sConsulta & "       LP.FC * LP.PREC_UP * LP.CANT_PED IMPORTELINEATOTAL," & vbCrLf
sConsulta = sConsulta & "       LP.FC * LP.PREC_UP * LR.CANT IMPORTELINEARECIBIDOPARCIAL," & vbCrLf
sConsulta = sConsulta & "       (LP.FC * LP.PREC_UP * LP.CANT_PED - (LP.FC * LP.PREC_UP * SUMAPORLINEA.CANTRECIBIDATOTAL)) IMPORTELINEAPENDIENTETOTAL," & vbCrLf
sConsulta = sConsulta & "       OE.MON MONCOD, MD.DEN MONDEN," & vbCrLf
sConsulta = sConsulta & "       OE.EST'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_PARTIDA_COD = '" & vbCrLf
sConsulta = sConsulta & "       ,''PARTIDA_COD_'' + LPI.PRES0 PRES0_PARTIDA_COD" & vbCrLf
sConsulta = sConsulta & "       ,COALESCE(LPI.PRES0,'''') + ''#'' + COALESCE(LPI.PRES1,'''') + ''#'' + COALESCE(LPI.PRES2,'''') + ''#'' + COALESCE(LPI.PRES3,'''') + ''#'' + COALESCE(LPI.PRES4,'''') PARTIDA_COD'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_NIVEL_DEN = '" & vbCrLf
sConsulta = sConsulta & "       ,''NIVEL_DEN_'' + LPI.PRES0 PRES0_NIVEL_DEN" & vbCrLf
sConsulta = sConsulta & "       ,CASE COALESCE (LPI.PRES4, LPI.PRES3, LPI.PRES2, LPI.PRES1)" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES2 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I2.DEN" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES3 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I3.DEN" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES4 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I4.DEN" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           P5I1.DEN" & vbCrLf
sConsulta = sConsulta & "       END NIVEL_DEN' " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_CCCOD = '" & vbCrLf
sConsulta = sConsulta & "       ,''CCCOD_'' + LPI.PRES0 PRES0_CCCOD" & vbCrLf
sConsulta = sConsulta & "       ,LPI_CSM.CENTRO_SM AS CCCOD' " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_CCDEN = '" & vbCrLf
sConsulta = sConsulta & "       ,''CCDEN_'' + LPI.PRES0 PRES0_CCDEN" & vbCrLf
sConsulta = sConsulta & "       ,COALESCE (LPI.UON4, LPI.UON3, LPI.UON2, LPI.UON1) + '' - '' + LPI_CSM.DEN AS CCDEN'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = ' " & vbCrLf
sConsulta = sConsulta & "   FROM ' + @FSGS + 'ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'PEDIDO PE WITH(NOLOCK) ON OE.PEDIDO = PE.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LP.ID=LPI.LINEA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUMPARTIDAS AS INT = 0" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMPARTIDAS = COUNT(*) FROM @PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPARTIDAS > 0" & vbCrLf
sConsulta = sConsulta & "       SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN @PARTIDASPRES PRES" & vbCrLf
sConsulta = sConsulta & "       ON PRES.PRES0 = COALESCE(LPI.PRES0,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES1 = COALESCE(LPI.PRES1,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES2 = COALESCE(LPI.PRES2,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES3 = COALESCE(LPI.PRES3,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES4 = COALESCE(LPI.PRES4,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN  + '" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'ITEM I WITH(NOLOCK) ON LP.ANYO = I.ANYO AND LP.GMN1 = I.GMN1_PROCE AND LP.PROCE = I.PROCE AND LP.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'LINEAS_RECEP LR WITH(NOLOCK) ON LR.LINEA = LP.ID AND LR.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PEDIDO_RECEP PR WITH(NOLOCK) ON PR.ID = LR.PEDIDO_RECEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'MON_DEN MD WITH (NOLOCK) ON MD.MON = OE.MON AND MD.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'UNI_DEN UD WITH (NOLOCK) ON UD.UNI = LP.UP AND UD.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'EMP E WITH(NOLOCK) ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT ORDEN, LINEA, SUM(CANT) CANTRECIBIDATOTAL " & vbCrLf
sConsulta = sConsulta & "               FROM ' + @FSGS + 'LINEAS_RECEP LR1 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ' + @FSGS + 'ORDEN_ENTREGA OE1 WITH(NOLOCK) ON OE1.ID = LR1.ORDEN " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ' + @FSGS + 'PEDIDO PE1 WITH(NOLOCK) ON PE1.ID = OE1.PEDIDO" & vbCrLf
sConsulta = sConsulta & "              GROUP BY ORDEN, LINEA, PROVE" & vbCrLf
sConsulta = sConsulta & "              HAVING OE1.PROVE = @PROVECOD" & vbCrLf
sConsulta = sConsulta & "             ) SUMAPORLINEA ON SUMAPORLINEA.ORDEN = LR.ORDEN AND SUMAPORLINEA.LINEA = LR.LINEA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "       SELECT CSM.CENTRO_SM, CSM.DEN, LPI.LINEA, LPI.UON1, LPI.UON2, LPI.UON3, LPI.UON4" & vbCrLf
sConsulta = sConsulta & "       FROM ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN ' + @FSGS + 'CENTRO_SM_UON CSM_UON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON ISNULL(LPI.UON1,'''') = ISNULL(CSM_UON.UON1,'''') AND ISNULL(LPI.UON2,'''') = ISNULL(CSM_UON.UON2,'''') AND ISNULL(LPI.UON3,'''') = ISNULL(CSM_UON.UON3,'''') AND ISNULL(LPI.UON4,'''') = ISNULL(CSM_UON.UON4,'''')" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'CENTRO_SM_DEN CSM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON CSM.CENTRO_SM = CSM_UON.CENTRO_SM AND CSM.IDI = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   ) LPI_CSM ON LP.ID = LPI_CSM.LINEA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I0 WITH (NOLOCK) ON P5I0.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.PRES1 IS NULL AND P5I0.PRES2 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.PRES3 IS NULL AND P5I0.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I1 WITH (NOLOCK) ON P5I1.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.PRES1 = LPI.PRES1" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.PRES2 IS NULL AND P5I1.PRES3 IS NULL AND P5I1.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I2 WITH (NOLOCK) ON P5I2.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I2.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.PRES3 IS NULL AND P5I2.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I3 WITH (NOLOCK) ON P5I3.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I3.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I3.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND LPI.PRES3 = P5I3.PRES3 AND P5I3.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I3.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I4 WITH (NOLOCK) ON P5I4.PRES0 = LPI.PRES0 " & vbCrLf
sConsulta = sConsulta & "       AND P5I4.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I4.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND LPI.PRES3 = P5I4.PRES3 AND LPI.PRES4 = P5I4.PRES4" & vbCrLf
sConsulta = sConsulta & "       AND P5I4.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'FACTURA F WITH(NOLOCK) ON F.ID = LP.ABONO_FAC" & vbCrLf
sConsulta = sConsulta & "       '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = ' " & vbCrLf
sConsulta = sConsulta & "   WHERE" & vbCrLf
sConsulta = sConsulta & "       OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "   IF @ANYO<>0 AND @ANYO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.ANYO = @ANYO'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDIDO<>0 AND @NUMPEDIDO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.NUM = @NUMPEDIDO'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMORDEN<>0 AND @NUMORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUM = @NUMORDEN'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDIDOERP<>'' AND @NUMPEDIDOERP IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUM_PED_ERP = @NUMPEDIDOERP'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --  Restricciones de PEDIDO_RECEP   -----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   IF @VERPEDIDOSSINRECEP = 0 --SE MUESTRAN S�LO LOS PEDIDOS CON RECEPCIONES" & vbCrLf
sConsulta = sConsulta & "       SET @SQlWHERE = @SQLWHERE + ' AND PR.ID IS NOT NULL' " & vbCrLf
sConsulta = sConsulta & "   ELSE --SE MUESTRAN LOS PEDIDOS SIN RECEPCIONES CUYA FECHA DE ENTREGA SOLICITADA EST� COMPRENDIDA ENTRE las fechas FECRECEPDESDE y FECRECEPHASTA" & vbCrLf
sConsulta = sConsulta & "        --y SE MUESTRAN LOS PEDIDOS CON RECEPCIONES QUE CUMPLAN LAS RESTRICCIONES DE FECHA Y ALBAR�N INDICADAS" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQlWHERE = @SQLWHERE + ' " & vbCrLf
sConsulta = sConsulta & "       AND (" & vbCrLf
sConsulta = sConsulta & "            (PR.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "           --LP.FECENTREGA ES UN CAMPO OPCIONAL POR LO QUE PUEDE SER NULO. VAMOS A MOSTRAR LOS VALORES QUE SEAN NULOS." & vbCrLf
sConsulta = sConsulta & "           IF @FECRECEPDESDE<>'' AND @FECRECEPDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "               SET @SQLWHERE = @SQLWHERE + ' AND COALESCE(LP.FECENTREGA,@FECRECEPDESDE) >= @FECRECEPDESDE'" & vbCrLf
sConsulta = sConsulta & "           IF @FECRECEPHASTA<>'' AND @FECRECEPHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "               SET @SQLWHERE = @SQLWHERE + ' AND COALESCE(LP.FECENTREGA,@FECRECEPHASTA) < @FECRECEPHASTA_MAS1'" & vbCrLf
sConsulta = sConsulta & "           SET @SQLWHERE = @SQLWHERE + ')" & vbCrLf
sConsulta = sConsulta & "            OR" & vbCrLf
sConsulta = sConsulta & "            (NOT PR.ID IS NULL' " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @FECRECEPDESDE<>'' AND @FECRECEPDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.FECHA >= @FECRECEPDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECRECEPHASTA<>'' AND @FECRECEPHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.FECHA < @FECRECEPHASTA_MAS1'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMALBARAN<>'' AND @NUMALBARAN IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.ALBARAN = @NUMALBARAN'" & vbCrLf
sConsulta = sConsulta & "   IF @VERPEDIDOSSINRECEP = 1 --CERRAMOS EL PAR�NTESIS QUE HAB�AMOS ABIERTO" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' )" & vbCrLf
sConsulta = sConsulta & "           )'" & vbCrLf
sConsulta = sConsulta & "   --  FIN Restricciones de PEDIDO_RECEP   -----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDPROVE<>'' AND @NUMPEDPROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUMEXT = @NUMPEDPROVE'" & vbCrLf
sConsulta = sConsulta & "   IF @ARTICULO<>'' AND @ARTICULO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND (I.ART LIKE ''%'' + @ARTICULO + ''%''  OR I.DESCR LIKE ''%'' + @ARTICULO + ''%'')'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGASOLICITDESDE<>'' AND @FECENTREGASOLICITDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGA >= @FECENTREGASOLICITDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGASOLICITHASTA<>'' AND @FECENTREGASOLICITHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGA < @FECENTREGASOLICITHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @CODEMPRESA<>'' AND @CODEMPRESA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.EMPRESA = @CODEMPRESA'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGAINDICADAPROVEDESDE<>'' AND @FECENTREGAINDICADAPROVEDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGAPROVE >= @FECENTREGAINDICADAPROVEDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGAINDICADAPROVEHASTA<>'' AND @FECENTREGAINDICADAPROVEHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGAPROVE < @FECENTREGAINDICADAPROVEHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @FECEMISIONDESDE<>'' AND @FECEMISIONDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.FECHA >= @FECEMISIONDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECEMISIONHASTA<>'' AND @FECEMISIONHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.FECHA < @FECEMISIONHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @CENTRO<>'' AND @CENTRO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LPI_CSM.CENTRO_SM = @CENTRO'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMFACTURA<>'' AND @NUMFACTURA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND F.NUM = @NUMFACTURA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Vamos a obtener todas las partidas presupuestarias de nivel 0 para poder usarlas luego" & vbCrLf
sConsulta = sConsulta & "   --como columnas al mostrar datos de partidas y centros de coste para cada recepci�n/entrega" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PRES0 NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #PRES5(" & vbCrLf
sConsulta = sConsulta & "       COD VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PRES0 = 'INSERT INTO #PRES5 SELECT COD FROM ' + @FSGS + 'PRES5_NIV0 WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXEC(@PRES0)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @PRES0  = STUFF(( " & vbCrLf
sConsulta = sConsulta & "                       SELECT DISTINCT '],[' + ltrim(#PRES5.COD)" & vbCrLf
sConsulta = sConsulta & "                           FROM #PRES5" & vbCrLf
sConsulta = sConsulta & "                           ORDER BY '],[' + ltrim(#PRES5.COD)" & vbCrLf
sConsulta = sConsulta & "                           FOR XML PATH('')" & vbCrLf
sConsulta = sConsulta & "                       ), 1, 2, '') + ']'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DROP TABLE #PRES5" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Esta select * no llama a una tabla sino a una consulta, por lo que devuelve " & vbCrLf
sConsulta = sConsulta & "   --todos los registros de la consulta, no de la tabla" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = '" & vbCrLf
sConsulta = sConsulta & "   SELECT * FROM ('" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ----DENOMINACION DE LA PARTIDA PRESUPUESTARIA" & vbCrLf
sConsulta = sConsulta & "   SET  @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_SELECT02 + @SUBSQLQUERY_NIVEL_DEN + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + @SUBSQLQUERY" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC1" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(NIVEL_DEN) FOR PRES0_NIVEL_DEN" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[NIVEL_DEN_') + ')) AS PVT1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --CODIGO UON COMPUESTO DE LA PARTIDA (UON1+UON2+UON3+UON4)" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_PARTIDA_COD + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC3" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(PARTIDA_COD) FOR PRES0_PARTIDA_COD" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[PARTIDA_COD_') + ')) AS PVT3" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT3.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT3.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --COD CENTRO DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_CCCOD + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC5" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(CCCOD) FOR PRES0_CCCOD" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[CCCOD_') + ')) AS PVT5" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT5.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT5.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --UON + DEN CENTRO DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_CCDEN + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC6" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(CCDEN) FOR PRES0_CCDEN" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[CCDEN_') + ')) AS PVT6" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT6.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT6.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --ORDER BY" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ORDER BY FECHARECEP DESC'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20), @PROVECOD NVARCHAR(100), @ANYO INT, @NUMPEDIDO INT, @NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50), @FECRECEPDESDE DATETIME, @FECRECEPHASTA DATETIME, @VERPEDIDOSSINRECEP INT, @NUMALBARAN VARCHAR(100), @NUMPEDPROVE VARCHAR(100), @ARTICULO VARCHAR(200), @FECENTREGASOLICITDESDE DATETIME, @FECENTREGASOLICITHASTA DATETIME, @NUMFACTURA VARCHAR(100), @CODEMPRESA INT, @FECENTREGAINDICADAPROVEDESDE DATETIME, @FECENTREGAINDICADAPROVEHASTA DATETIME, @FECEMISIONDESDE DATETIME, @FECEMISIONHASTA DATETIME, @CENTRO VARCHAR(50), @PARTIDASPRES TablaPartidas READONLY, @FECRECEPHASTA_MAS1 DATETIME', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                       @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                       @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                       @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                       @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                       @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                       @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                       @NUMFACTURA=@NUMFACTURA," & vbCrLf
sConsulta = sConsulta & "                       @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEDESDE =@FECENTREGAINDICADAPROVEDESDE, " & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEHASTA = @FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                       @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA_MAS1=@FECRECEPHASTA_MAS1" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "   --TABLA CON LOS NOMBRES DE LAS PARTIDAS" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = 'SELECT * FROM(SELECT P5.COD PRES0," & vbCrLf
sConsulta = sConsulta & "       CASE PSM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "           WHEN 2 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL2,'''')" & vbCrLf
sConsulta = sConsulta & "           WHEN 3 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL3,'''')" & vbCrLf
sConsulta = sConsulta & "           WHEN 4 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL4,'''')" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL1,'''')" & vbCrLf
sConsulta = sConsulta & "           END PARTIDA_DEN" & vbCrLf
sConsulta = sConsulta & "   FROM  ' + @FSGS + 'PRES5_NIV0 P5 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  ' + @FSGS + 'PARGEN_SM PSM WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON P5.COD = PSM.PRES5" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN  ' + @FSGS + 'PRES5_IDIOMAS P5I0 WITH (NOLOCK) ON P5I0.PRES0 = P5.COD" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.PRES1 IS NULL AND P5I0.PRES2 IS NULL" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.PRES3 IS NULL AND P5I0.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.IDIOMA = @IDIOMA)SRC2" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(PARTIDA_DEN) FOR PRES0" & vbCrLf
sConsulta = sConsulta & "   IN (' + @PRES0 + ')) AS PVT2'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20)', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --TABLA CON LOS CENTROS DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = 'SELECT DISTINCT" & vbCrLf
sConsulta = sConsulta & "       LPI_CSM.CENTRO_SM AS CCCOD, " & vbCrLf
sConsulta = sConsulta & "       COALESCE (LPI.UON4, LPI.UON3, LPI.UON2, LPI.UON1) + '' - '' + LPI_CSM.DEN AS CCDEN'" & vbCrLf
sConsulta = sConsulta & "       + @SUBSQLQUERY_FIN +" & vbCrLf
sConsulta = sConsulta & "       ' AND NOT LPI_CSM.CENTRO_SM IS NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20), @PROVECOD NVARCHAR(100), @ANYO INT, @NUMPEDIDO INT, @NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50), @FECRECEPDESDE DATETIME, @FECRECEPHASTA DATETIME, @VERPEDIDOSSINRECEP INT, @NUMALBARAN VARCHAR(100), @NUMPEDPROVE VARCHAR(100), @ARTICULO VARCHAR(200), @FECENTREGASOLICITDESDE DATETIME, @FECENTREGASOLICITHASTA DATETIME, @NUMFACTURA VARCHAR(100), @CODEMPRESA INT, @FECENTREGAINDICADAPROVEDESDE DATETIME, @FECENTREGAINDICADAPROVEHASTA DATETIME, @FECEMISIONDESDE DATETIME, @FECEMISIONHASTA DATETIME, @CENTRO VARCHAR(50), @PARTIDASPRES TablaPartidas READONLY, @FECRECEPHASTA_MAS1 DATETIME', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                       @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                       @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                       @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                       @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                       @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                       @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                       @NUMFACTURA=@NUMFACTURA," & vbCrLf
sConsulta = sConsulta & "                       @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEDESDE =@FECENTREGAINDICADAPROVEDESDE, " & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEHASTA = @FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                       @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA_MAS1=@FECRECEPHASTA_MAS1" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL, @RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, @ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, @FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, @EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, @ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, @PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, @NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, @FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, @FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, @FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, @ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, @CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, @CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, @CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, @UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, @PREC_VISIBLE int = NULL," & vbCrLf
sConsulta = sConsulta & "@PREC_WIDTH FLOAT = NULL, @IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, @IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, @IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, @MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, @CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL,@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL,@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL,@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, @PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL, @PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, @PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, @PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL, @BLOQ_FACTURA_VISIBLE INT = NULL, @BLOQ_FACTURA_WIDTH FLOAT = NULL, @NUM_LINEA_VISIBLE INT = NULL, @NUM_LINEA_WIDTH FLOAT = NULL, @FACTURA_VISIBLE INT = NULL, @FACTURA_WIDTH FLOAT = NULL, @RECEPTOR_VISIBLE INT = NULL, @RECEPTOR_WIDTH FLOAT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la informaci�n del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tama�o suficiente para ser visible pero no molestar" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_VISIBLE = @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_WIDTH = @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_WIDTH = @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT, @ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT, @PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT, @FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT, @CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, @UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT," & vbCrLf
sConsulta = sConsulta & "               @CC2_WIDTH FLOAT,@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @BLOQ_FACTURA_VISIBLE INT, @BLOQ_FACTURA_WIDTH FLOAT, @NUM_LINEA_VISIBLE INT, @NUM_LINEA_WIDTH FLOAT, @ANCHODEFECTO INT'," & vbCrLf
sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_VISIBLE = @FACTURA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_WIDTH = @FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_WIDTH = @RECEPTOR_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU = ID FROM USU WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT [RETRASADO_VISIBLE],[RETRASADO_WIDTH],[ALBARAN_VISIBLE],[ALBARAN_WIDTH],[FRECEP_VISIBLE],[FRECEP_WIDTH],[EMP_VISIBLE],[EMP_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[ERP_VISIBLE],[ERP_WIDTH],[PEDIDO_VISIBLE],[PEDIDO_WIDTH],[NUMPEDPROVE_VISIBLE],[NUMPEDPROVE_WIDTH],[FEMISION_VISIBLE],[FEMISION_WIDTH],[FENTREGA_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[FENTREGA_WIDTH],[FENTREGAPROVE_VISIBLE],[FENTREGAPROVE_WIDTH],[ART_VISIBLE],[ART_WIDTH],[CANTPED_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[CANTPED_WIDTH],[CANTREC_VISIBLE],[CANTREC_WIDTH],[CANTPEND_VISIBLE],[CANTPEND_WIDTH],[UNI_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[UNI_WIDTH],[PREC_VISIBLE],[PREC_WIDTH],[IMPPED_VISIBLE],[IMPPED_WIDTH],[IMPREC_VISIBLE],[IMPREC_WIDTH],[IMPPEND_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[IMPPEND_WIDTH],[MON_VISIBLE],[MON_WIDTH],[CC1_VISIBLE],[CC1_WIDTH],[CC2_VISIBLE],[CC2_WIDTH],[CC3_VISIBLE],[CC3_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[CC4_VISIBLE],[CC4_WIDTH],[PARTIDA1_VISIBLE],[PARTIDA1_WIDTH],[PARTIDA2_VISIBLE],[PARTIDA2_WIDTH],[PARTIDA3_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[PARTIDA3_WIDTH],[PARTIDA4_VISIBLE],[PARTIDA4_WIDTH], [BLOQ_FACTURA_VISIBLE],[BLOQ_FACTURA_WIDTH],[NUM_LINEA_VISIBLE],[NUM_LINEA_WIDTH],[FACTURA_VISIBLE],[FACTURA_WIDTH],[RECEPTOR_VISIBLE],[RECEPTOR_WIDTH]" & vbCrLf
sConsulta = sConsulta & "FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE USU = @IDUSU AND CIA = @CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARGEN_INTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT ACCESO_FSSM, VER_NUMEXT, ACCESO_FSGS, ACCESO_FSFA FROM ' + @FSGS + 'PARGEN_INTERNO WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_OBT_DATOS_RECEPTOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_OBT_DATOS_RECEPTOR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_OBT_DATOS_RECEPTOR] @CIA INT, @RECEPTOR VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSEP_OBT_DATOS_RECEPTOR @RECEPTOR  =@RECEPTOR '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@RECEPTOR VARCHAR(20)', @RECEPTOR  =@RECEPTOR" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_OBT_DATOS_FACTURA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_OBT_DATOS_FACTURA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_OBT_DATOS_FACTURA] @CIA INT, @FACTURAID INT, @IDIOMA VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSEP_OBT_DATOS_FACTURA @FACTURAID  =@FACTURAID ,@IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@FACTURAID INT, @IDIOMA VARCHAR(20)', @FACTURAID =@FACTURAID , @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_DEVOLVER_CENTROS_COSTE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_DEVOLVER_CENTROS_COSTE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSSM_DEVOLVER_CENTROS_COSTE] @CIA INT,@VERUON TINYINT,@IDI VARCHAR(20),@USU VARCHAR(50)=NULL,@DESDE NVARCHAR(1) ='' AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSSM_DEVOLVER_CENTROS_COSTE  @VERUON =@VERUON ,@IDI =@IDI,@USU=@USU,@DESDE=@DESDE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@VERUON TINYINT,@IDI VARCHAR(20),@USU VARCHAR(50)=NULL,@DESDE NVARCHAR(1) =''''', @VERUON =@VERUON ,@IDI =@IDI,@USU=@USU,@DESDE=@DESDE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_DEVOLVER_PARTIDAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_DEVOLVER_PARTIDAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSSM_DEVOLVER_PARTIDAS] @CIA INT,@PRES5 VARCHAR(20),@IDI VARCHAR(20),@USU VARCHAR(50)=NULL,@UON1 VARCHAR(10)=NULL,@UON2 VARCHAR(10)=NULL,@UON3 VARCHAR(10)=NULL,@UON4 VARCHAR(10)=NULL,@VIGENTES TINYINT=1,@FECHAINICIODESDE DATETIME=NULL,@FECHAFINHASTA DATETIME=NULL,@FORMATOFECHA AS VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSSM_DEVOLVER_PARTIDAS  @PRES5 =@PRES5 ,@IDI =@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@FORMATOFECHA=@FORMATOFECHA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 VARCHAR(20),@IDI VARCHAR(20),@USU VARCHAR(50)=NULL,@UON1 VARCHAR(10)=NULL,@UON2 VARCHAR(10)=NULL,@UON3 VARCHAR(10)=NULL,@UON4 VARCHAR(10)=NULL,@VIGENTES TINYINT=1,@FECHAINICIODESDE DATETIME=NULL,@FECHAFINHASTA DATETIME=NULL,@FORMATOFECHA AS VARCHAR(50)=NULL', @PRES5 =@PRES5 ,@IDI =@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 



sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_GET_PAGOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_GET_PAGOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_GET_PAGOS] @CIA INT,@LINEA_PEDIDO integer = NULL, @IDIOMA VARCHAR(20) = 'SPA'," & vbCrLf
sConsulta = sConsulta & "@FACTURA INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSEP_GET_PAGOS @LINEA_PEDIDO =@LINEA_PEDIDO,@IDIOMA=@IDIOMA,@FACTURA=@FACTURA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@LINEA_PEDIDO integer = NULL, @IDIOMA VARCHAR(20) = ''SPA''," & vbCrLf
sConsulta = sConsulta & "@FACTURA INT = NULL', @LINEA_PEDIDO =@LINEA_PEDIDO,@IDIOMA=@IDIOMA,@FACTURA=@FACTURA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PARAMETROS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PARAMETROS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PARAMETROS]  @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @CIA IS NULL AND @CIA <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ACCESO_FSSM FROM ' + @FSGS + 'PARGEN_INTERNO WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT OBLCODPEDDIR FROM ' + @FSGS + 'PARGEN_PED WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,IDI,DEN FROM ' + @FSGS + 'PARGEN_LIT WITH (NOLOCK) ORDER BY ID, IDI'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ACTUALIZAR_MONITORIZACION_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ACTUALIZAR_MONITORIZACION_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_ACTUALIZAR_MONITORIZACION_PROVE] @CIA INT, @ID INT, @SEG TINYINT,@PROVE AS VARCHAR(50),@IDCONTACTO AS INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ACTUALIZAR_MONITORIZACION_PROVE @ID=@ID, @SEG=@SEG,@PROVE=@PROVE,@IDCONTACTO=@IDCONTACTO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @SEG TINYINT,@PROVE AS VARCHAR(50),@IDCONTACTO AS INT',@ID=@ID, @SEG=@SEG,@PROVE=@PROVE,@IDCONTACTO=@IDCONTACTO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_BUSCAR_CENTROCOSTE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_BUSCAR_CENTROCOSTE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_BUSCAR_CENTROCOSTE] @CIA INT, @IDI VARCHAR(20),@COD VARCHAR(10)  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT CSU.UON1 + case when csu.uon2 is not null then ''#'' else '''' end  + CSU.UON2 + case when csu.uon3 is not null then ''#'' else '''' end + CSU.UON3 + case when csu.uon4 is not null then ''#'' else '''' end + CSU.UON4 as COD,CSU.CENTRO_SM + '' - '' + SD.DEN AS DEN, CSU.CENTRO_SM " & vbCrLf
sConsulta = sConsulta & "   FROM ' + @FSGS + 'CENTRO_SM_UON CSU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'CENTRO_SM_DEN SD WITH(NOLOCK)ON CSU.CENTRO_SM=SD.CENTRO_SM AND IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   WHERE csu.centro_sm=@COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@IDI VARCHAR(20),@COD VARCHAR(10)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM ,@IDI=@IDI,@COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_PARTIDAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_PARTIDAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_PARTIDAS] @CIA INT, @IDIOMA NVARCHAR(4) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_PARTIDAS @IDIOMA =@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @IDIOMA NVARCHAR(4)', @IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ESTADOS_FACTURA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ESTADOS_FACTURA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_ESTADOS_FACTURA] @CIA INT,  @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ESTADOS_FACTURA @IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50) ', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_EN_PROCESO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_EN_PROCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_EN_PROCESO] @CIA INT,@ID INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_EN_PROCESO @ID = @ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT',  @ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_FACTURAS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_FACTURAS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_FACTURAS_PROVE] @CIA INT, @IDI NVARCHAR(3)," & vbCrLf
sConsulta = sConsulta & "@PROVE NVARCHAR(50),@ICONTACTO INT,@EMP INT=0,@NUM_FACTURA NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@ESTADO INT=0," & vbCrLf
sConsulta = sConsulta & "@FECHA_CONTA_DESDE DATETIME=NULL, @FECHA_CONTA_HASTA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "@PEDIDO_ERP NVARCHAR(50)=NULL, @FACTURA_ERP NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@IMPORTE_DESDE FLOAT=NULL, @IMPORTE_HASTA FLOAT=NULL," & vbCrLf
sConsulta = sConsulta & "@ALBARAN NVARCHAR(100)=NULL,@ARTICULO NVARCHAR(100)=NULL,@PARTIDAS Tabla_Uons_Partidas READONLY," & vbCrLf
sConsulta = sConsulta & "@GESTOR VARCHAR(30)=NULL,@CENTRO_COSTE NVARCHAR(50)=NULL,@NUM_CESTA INT=0,@NUM_PEDIDO INT=0,@ANYO_PEDIDO INT=0,@FAC_RECTIFICATIVA INT=0,@FAC_ORIGINAL INT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_LINEAS_PEDIDO_FACTURA INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_LINEAS_PEDIDO INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_LINEAS_PED_IMPUTACION INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_ORDEN_ENTREGA INT=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Tablas basicas para las consultas " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = N' FROM ' + @FSGS + 'FACTURA F WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN ' + @FSGS + 'EST_FACTURA EF WITH(NOLOCK) ON EF.ID = F.ESTADO" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN ' + @FSGS + 'EST_FACTURA_DEN EFD WITH(NOLOCK) ON EFD.EST_FACTURA=F.ESTADO AND EFD.IDI=''' + @IDI + '''" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN ' + @FSGS + 'INSTANCIA I WITH (NOLOCK) ON F.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN( SELECT INSTANCIA, BLOQUE FROM  ' + @FSGS + 'INSTANCIA_BLOQUE WITH (NOLOCK) WHERE ESTADO=1) IB1 ON I.ID = IB1.INSTANCIA " & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ' + @FSGS + 'PM_COPIA_BLOQUE  PCB ON PCB.ID =IB1.BLOQUE " & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ' + @FSGS + 'PM_COPIA_BLOQUE_DEN PCBD ON PCB.ID=PCBD.BLOQUE AND PCBD.IDI=@IDI " & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN (SELECT ID_FACTURA, COUNT(1) AS PAGOS FROM ' + @FSGS + 'PAGO WITH(NOLOCK) GROUP BY ID_FACTURA) P ON P.ID_FACTURA = F.ID" & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ' + @FSGS + 'EMP WITH (NOLOCK) ON F.EMPRESA=EMP.ID " & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ' + @FSGS + 'INSTANCIA_ADICIONAL_PROVE IAP WITH(NOLOCK) ON I.ID=IAP.INSTANCIA AND IAP.PROVE=@PROVE AND IAP.CONTACTO=@ICONTACTO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE =' WHERE 1=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--- CRITERIOS DE SELECCION" & vbCrLf
sConsulta = sConsulta & "--- FALTAN: PEDIDO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL AND @PROVE <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @EMP IS NOT NULL AND @EMP <> 0" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.EMPRESA=@EMP'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM_FACTURA IS NOT NULL AND @NUM_FACTURA <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.NUM=@NUM_FACTURA'" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_DESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE =@SWHERE + ' AND F.FECHA >= @FECHA_DESDE'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_HASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.FECHA <= @FECHA_HASTA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NOT NULL AND @ESTADO <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.ESTADO=@ESTADO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA_CONTA_DESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE =@SWHERE + ' AND F.FECHA_CONTA >= @FECHA_CONTA_DESDE'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_CONTA_HASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE =@SWHERE + ' AND F.FECHA_CONTA >= @FECHA_CONTA_HASTA'" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "IF @FACTURA_ERP IS NOT NULL AND @FACTURA_ERP <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND F.NUM_ERP=@FACTURA_ERP'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "IF @IMPORTE_DESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.IMPORTE>=@IMPORTE_DESDE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTE_HASTA IS NOT NULL AND @IMPORTE_HASTA<>0" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.IMPORTE<=@IMPORTE_HASTA'  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FAC_RECTIFICATIVA =1 AND @FAC_ORIGINAL =1" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.TIPO IN(1,2)'   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FAC_RECTIFICATIVA =0 AND @FAC_ORIGINAL =1" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.TIPO=1 '    " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @FAC_RECTIFICATIVA =1 AND @FAC_ORIGINAL =0" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND F.TIPO=2 '    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ALBARAN IS NOT NULL AND @ALBARAN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' ' + @FSGS + 'INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH(NOLOCK) ON LPF.FACTURA = F.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND LPF.ALBARAN=@ALBARAN'" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PEDIDO_ERP IS NOT NULL AND @PEDIDO_ERP <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' ' + @FSGS + 'INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID' " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' ' + @FSGS + 'INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' ' + @FSGS + 'INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.PEDIDO=LP.PEDIDO'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND NUM_PED_ERP=@PEDIDO_ERP '" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_ORDEN_ENTREGA=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ARTICULO IS NOT NULL AND @ARTICULO <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID' " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND ART_INT=@ARTICULO'" & vbCrLf
sConsulta = sConsulta & "END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMPARTIDAS AS INT = 0" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMPARTIDAS = COUNT(*) FROM @PARTIDAS" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPARTIDAS > 0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID " & vbCrLf
sConsulta = sConsulta & "                                           INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO'  " & vbCrLf
sConsulta = sConsulta & "                   SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "                   SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + '" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN @PARTIDAS PRES" & vbCrLf
sConsulta = sConsulta & "           ON PRES.UON1 = LPI.UON1" & vbCrLf
sConsulta = sConsulta & "           AND PRES.UON2 = LPI.UON2" & vbCrLf
sConsulta = sConsulta & "           AND PRES.UON3 = LPI.UON3" & vbCrLf
sConsulta = sConsulta & "           AND PRES.UON4 = LPI.UON4" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES1 = LPI.PRES1" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES2 = LPI.PRES2" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES3 = LPI.PRES3" & vbCrLf
sConsulta = sConsulta & "           AND PRES.PRES4 = LPI.PRES4'" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GESTOR IS NOT NULL AND @GESTOR <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0 AND @JOIN_LINEAS_PED_IMPUTACION=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID " & vbCrLf
sConsulta = sConsulta & "                                           INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO '     " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA =1" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @JOIN_LINEAS_PEDIDO_FACTURA=1 AND @JOIN_LINEAS_PED_IMPUTACION=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO ' " & vbCrLf
sConsulta = sConsulta & "               SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' LEFT JOIN ' + @FSGS + 'PRES5_IMPORTES PRES5 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               ON PRES5.ID=LPI.PRES5_IMP '" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND PRES5.GESTOR=@GESTOR '" & vbCrLf
sConsulta = sConsulta & "END        " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CENTRO_COSTE  IS NOT NULL AND @CENTRO_COSTE  <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SEARCH INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON1 NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON2 NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON3 NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON4 NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TEXTO NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INDEX INT=1" & vbCrLf
sConsulta = sConsulta & "   DECLARE @STARTCOSTE INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SEARCH=CHARINDEX('#',@CENTRO_COSTE,0)" & vbCrLf
sConsulta = sConsulta & "   WHILE @SEARCH > 0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @TEXTO=SUBSTRING (@CENTRO_COSTE,@STARTCOSTE+1,(@SEARCH-@STARTCOSTE)-1) " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @INDEX =1" & vbCrLf
sConsulta = sConsulta & "               SET @UON1=@TEXTO " & vbCrLf
sConsulta = sConsulta & "           IF @INDEX = 2" & vbCrLf
sConsulta = sConsulta & "               SET @UON2=@TEXTO " & vbCrLf
sConsulta = sConsulta & "           IF @INDEX = 3" & vbCrLf
sConsulta = sConsulta & "               SET @UON3=@TEXTO " & vbCrLf
sConsulta = sConsulta & "           IF @INDEX = 4" & vbCrLf
sConsulta = sConsulta & "               SET @UON4=@TEXTO " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @STARTCOSTE =@SEARCH " & vbCrLf
sConsulta = sConsulta & "           SET @SEARCH=CHARINDEX('#',@CENTRO_COSTE,@SEARCH +1)" & vbCrLf
sConsulta = sConsulta & "           SET @INDEX=@INDEX + 1" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0 AND @JOIN_LINEAS_PED_IMPUTACION=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID " & vbCrLf
sConsulta = sConsulta & "                                           INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO '                                 " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA =1" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @JOIN_LINEAS_PEDIDO_FACTURA=1 AND @JOIN_LINEAS_PED_IMPUTACION=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LPF.LINEA_PEDIDO ' " & vbCrLf
sConsulta = sConsulta & "               SET @JOIN_LINEAS_PED_IMPUTACION=1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND LPI.UON1=@UON1 AND LPI.UON2=@UON2 AND LPI.UON3=@UON3 AND LPI.UON4=@UON4 ' " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @NUM_PEDIDO  IS NOT NULL AND @NUM_PEDIDO <>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID'        " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO=0 AND @JOIN_ORDEN_ENTREGA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.PEDIDO=LP.PEDIDO'   " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_ORDEN_ENTREGA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND OE.NUM=@NUM_PEDIDO '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ANYO_PEDIDO IS NOT NULL AND @ANYO_PEDIDO<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID' " & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO=0 AND @JOIN_ORDEN_ENTREGA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.PEDIDO=LP.PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_ORDEN_ENTREGA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND OE.ANYO=@ANYO_PEDIDO '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM_CESTA IS NOT NULL AND @NUM_CESTA<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO_FACTURA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK) ON LPF.FACTURA=F.ID'" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO_FACTURA=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @JOIN_LINEAS_PEDIDO=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPF.LINEA_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "           SET @JOIN_LINEAS_PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @FSGS + 'PEDIDO P WITH(NOLOCK) ON LP.PEDIDO=P.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND P.NUM=@NUM_CESTA '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' LEFT JOIN (SELECT LFD.FACTURA, COUNT(LFD.LINEA) NUMDISCR'" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' FROM ' + @FSGS + 'LIN_FACTURA_DISCRP LFD WITH(NOLOCK) GROUP BY LFD.FACTURA) DISCR ON DISCR.FACTURA = F.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=' SELECT DISTINCT 0 AS ICONO,0 AS ETAPA_PETICIONARIO,0 AS OBV,0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR,F.ID ID_FACTURA,F.NUM NUM_FACTURA ' " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ',EMP.DEN EMPRESA, F.PROVE COD_PROVE,F.FECHA,F.FEC_CONTA,F.NUM_ERP,F.IMPORTE,EFD.DEN DEN_ESTADO,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + 'I.ESTADO,PCBD.DEN SITUACION_ACTUAL,I.TRASLADADA,I.EN_PROCESO,I.ID INSTANCIA,F.TIPO, P.PAGOS, EF.ANULADO, ISNULL(IAP.SEG,0) as SEG'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ', CASE WHEN NUMDISCR > 0 THEN 1 ELSE 0 END TIENE_DISCREP '" & vbCrLf
sConsulta = sConsulta & " + @SQLFROM + @SWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PRIMERO LAS QUE ESTAN EN ESTADO BORRADOR" & vbCrLf
sConsulta = sConsulta & "--*******************************************" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_FACTURAS_BORRADOR AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Le a�adimos que este en estado peticionario y le indicamos que es una factura pendiente con 1 as Icono" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_BORRADOR=REPLACE(@SQL,'ON F.INSTANCIA=I.ID','ON F.INSTANCIA=I.ID AND I.ESTADO=0 ') " & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_BORRADOR=REPLACE(@SQL_FACTURAS_BORRADOR,'0 AS ICONO','1 AS ICONO')" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_BORRADOR=REPLACE(@SQL_FACTURAS_BORRADOR,'0 AS ETAPA_PETICIONARIO','1 AS ETAPA_PETICIONARIO')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- LUEGO LAS QUE ESTAN PENDIENTES DE ACCION POR PARTE DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "--*******************************************************************" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_FACTURAS_PENDIENTES AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM_FACTURAS_PENDIENTES AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHERE_FACTURAS_PENDIENTES AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM_FACTURAS_PENDIENTES=' INNER JOIN ' + @FSGS + 'INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID = IB.INSTANCIA  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'PM_COPIA_ACCIONES CA WITH (NOLOCK) ON IB.BLOQUE = CA.BLOQUE " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN ' + @FSGS + 'PM_COPIA_ROL_ACCION CRA WITH (NOLOCK) ON CA.ID = CRA.ACCION " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN ' + @FSGS + 'PM_COPIA_ROL CR WITH (NOLOCK) ON CRA.ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ' + @FSGS + 'INSTANCIA_EST IE WITH (NOLOCK) ON IE.ROL=CR.ID  ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Que no este trasladada y que este en la etapa PROVEEDOR, o que este trasladada al proveedor        " & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE_FACTURAS_PENDIENTES= ' AND ((IB.TRASLADADA <> 1 AND IB.ESTADO=1 AND CR.TIPO = 2 AND CR.PROVE =@PROVE) OR ( IB.TRASLADADA =1 AND IE.DESTINATARIO_PROV=@PROVE AND IB.ESTADO=1 )) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_PENDIENTES=REPLACE(@SQL,'ON F.INSTANCIA=I.ID','ON F.INSTANCIA=I.ID ' + @SQLFROM_FACTURAS_PENDIENTES )" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_PENDIENTES=REPLACE(@SQL_FACTURAS_PENDIENTES,'0 AS ICONO','1 AS ICONO') " & vbCrLf
sConsulta = sConsulta & "SET @SQL_FACTURAS_PENDIENTES=@SQL_FACTURAS_PENDIENTES + @SQLWHERE_FACTURAS_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- LUEGO EL RESTO DE FACTURAS" & vbCrLf
sConsulta = sConsulta & "--*******************************************************************" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_RESTO_FACTURAS AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHERE_RESTO_FACTURAS AS NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_ORDER_RESTO_FACTURAS AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE_RESTO_FACTURAS= ' AND I.ESTADO<>0 ' --Que no esten en etapa BORRADOR" & vbCrLf
sConsulta = sConsulta & "SET @SQL_RESTO_FACTURAS=REPLACE(@SQL,'ON F.INSTANCIA=I.ID','ON F.INSTANCIA=I.ID ' + @SQLFROM_FACTURAS_PENDIENTES )" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE_RESTO_FACTURAS= @SQLWHERE_RESTO_FACTURAS + ' AND NOT ((IB.TRASLADADA <> 1 AND IB.ESTADO=1 AND CR.TIPO = 2 AND CR.PROVE =@PROVE) OR ( IB.TRASLADADA =1 AND IE.DESTINATARIO_PROV=@PROVE AND IB.ESTADO=1 )) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL_ORDER_RESTO_FACTURAS = ' ORDER BY F.FECHA,F.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL_RESTO_FACTURAS=@SQL_RESTO_FACTURAS + @SQLWHERE_RESTO_FACTURAS + @SQL_ORDER_RESTO_FACTURAS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SE HACE LA SELECT FINAL CON LAS 3 UNION PARA SACAR ORDENADOS LOS DATOS, 1� LAS" & vbCrLf
sConsulta = sConsulta & "--FACTURAS EN ESTADO BORRADOR, LUEGO LAS QUE TIENE PENDIENTE DE ACCION Y LUEGO EL RESTO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_FINAL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FACTURAS_BORRADOR " & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FINAL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FINAL + @SQL_FACTURAS_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FINAL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL_FINAL=@SQL_FINAL + @SQL_RESTO_FACTURAS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@IDI NVARCHAR(3),@PROVE NVARCHAR(50),@ICONTACTO INT,@EMP INT=0,@NUM_FACTURA NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@ESTADO INT=0," & vbCrLf
sConsulta = sConsulta & "@FECHA_CONTA_DESDE DATETIME=NULL, @FECHA_CONTA_HASTA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "@PEDIDO_ERP NVARCHAR(50)=NULL, @FACTURA_ERP NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@IMPORTE_DESDE FLOAT=NULL, @IMPORTE_HASTA FLOAT=NULL," & vbCrLf
sConsulta = sConsulta & "@ALBARAN NVARCHAR(100)=NULL,@ARTICULO NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "@PARTIDAS Tabla_Uons_Partidas READONLY," & vbCrLf
sConsulta = sConsulta & "@GESTOR VARCHAR(30)=NULL,@CENTRO_COSTE NVARCHAR(50)=NULL,@NUM_CESTA INT=0,@NUM_PEDIDO INT=0,@ANYO_PEDIDO INT=0,@UON1 NVARCHAR(5),@UON2 NVARCHAR(5),@UON3 NVARCHAR(5),@UON4 NVARCHAR(5) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "print @SQL_FINAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL_FINAL" & vbCrLf
sConsulta = sConsulta & ", @PARAM" & vbCrLf
sConsulta = sConsulta & ",@IDI=@IDI,@PROVE=@PROVE,@EMP=@EMP,@NUM_FACTURA=@NUM_FACTURA,@FECHA_DESDE=@FECHA_DESDE," & vbCrLf
sConsulta = sConsulta & "@FECHA_HASTA=@FECHA_HASTA,@ESTADO=@ESTADO,@FECHA_CONTA_DESDE=@FECHA_CONTA_DESDE,@PEDIDO_ERP=@PEDIDO_ERP," & vbCrLf
sConsulta = sConsulta & "@FACTURA_ERP=@FACTURA_ERP,@IMPORTE_DESDE=@IMPORTE_DESDE,@IMPORTE_HASTA=@IMPORTE_HASTA,@ALBARAN=@ALBARAN,@ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "@ICONTACTO=@ICONTACTO,@PARTIDAS=@PARTIDAS,@GESTOR=@GESTOR,@CENTRO_COSTE=@CENTRO_COSTE,@NUM_CESTA=@NUM_CESTA," & vbCrLf
sConsulta = sConsulta & "@NUM_PEDIDO=@NUM_PEDIDO,@ANYO_PEDIDO=@ANYO_PEDIDO,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4   " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_ATRIBUTOS_BUSCADOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_ATRIBUTOS_BUSCADOR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_ATRIBUTOS_BUSCADOR] @CIA INT,@TIPO TINYINT, " & vbCrLf
sConsulta = sConsulta & "@COD NVARCHAR(50)=NULL, @DEN NVARCHAR(200)=NULL, @CODART NVARCHAR(40)=NULL, @GMN1 NVARCHAR(10)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN2 NVARCHAR(10)=NULL,@GMN3 NVARCHAR(10)=NULL,@GMN4 NVARCHAR(10)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_ATRIBUTOS_BUSCADOR @TIPO=@TIPO,@COD=@COD,@DEN=@DEN,@CODART=@CODART,@GMN1=@GMN1,@GMN2=@GMN2,@GMN3=@GMN3,@GMN4=@GMN4'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO TINYINT, " & vbCrLf
sConsulta = sConsulta & "@COD NVARCHAR(50)=NULL, @DEN NVARCHAR(200)=NULL, @CODART NVARCHAR(40)=NULL, @GMN1 NVARCHAR(10)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN2 NVARCHAR(10)=NULL,@GMN3 NVARCHAR(10)=NULL,@GMN4 NVARCHAR(10)=NULL', @TIPO=@TIPO,@COD=@COD,@DEN=@DEN,@CODART=@CODART,@GMN1=@GMN1,@GMN2=@GMN2,@GMN3=@GMN3,@GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_EMPRESAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_EMPRESAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_EMPRESAS] @CIA INT, @IDI VARCHAR(50),@NIF VARCHAR(15)=NULL,@DEN VARCHAR(200)=NULL,@DIR VARCHAR(200)=NULL,@CP VARCHAR(5)=NULL,@PAI VARCHAR(10)=NULL,@PROVI VARCHAR(10)=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_EMPRESAS_USUARIO @IDI=@IDI,@NIF=@NIF,@DEN=@DEN,@DIR=@DIR,@CP=@CP,@PAI=@PAI,@PROVI=@PROVI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50),@NIF VARCHAR(15)=NULL,@DEN VARCHAR(200)=NULL,@DIR VARCHAR(200)=NULL,@CP VARCHAR(5)=NULL,@PAI VARCHAR(10)=NULL,@PROVI VARCHAR(10)=NULL', @IDI=@IDI,@NIF=@NIF,@DEN=@DEN,@DIR=@DIR,@CP=@CP,@PAI=@PAI,@PROVI=@PROVI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_FACTURA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_FACTURA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_FACTURA] @CIA INT, @ID INT,@IDIOMA NVARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_FACTURA @ID =@ID, @IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT,@IDIOMA NVARCHAR(50)', @ID =@ID, @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_CONF_CUMP_FACTURA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_CONF_CUMP_FACTURA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_CONF_CUMP_FACTURA] @CIA INT,@ID INT,@VERSION INT,@USU VARCHAR(50)=NULL,@PROVE VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE  @PROVEGS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_CONF_CUMP_FACTURA @ID=@ID,@VERSION=@VERSION,@USU=@USU,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@VERSION INT,@USU VARCHAR(50)=NULL,@PROVE VARCHAR(50) = NULL ', @ID=@ID,@VERSION=@VERSION,@USU=@USU,@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_MOTIVO_ANULACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_MOTIVO_ANULACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_MOTIVO_ANULACION] @CIA INT,@FACTURA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_MOTIVO_ANULACION @FACTURA =@FACTURA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@FACTURA AS INT', @FACTURA =@FACTURA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_FACTURAS_CD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_FACTURAS_CD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_FACTURAS_CD] @CIA INT,@PROVE NVARCHAR(100),@TIPO TINYINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_FACTURAS_CD @PROVE=@PROVE,@TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE NVARCHAR(100),@TIPO TINYINT', @PROVE=@PROVE,@TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_EMPRESA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_EMPRESA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_EMPRESA] @CIA INT,@ID INT,@IDI NVARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_EMPRESA @ID=@ID,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI NVARCHAR(20)', @ID=@ID,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_IMPUESTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_IMPUESTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_IMPUESTOS] @CIA INT, @IDI NVARCHAR(5),@COD NVARCHAR(50)='',@DESCR NVARCHAR(300)='',@PAIS NVARCHAR(3)='',@PROV NVARCHAR(3)='',@ART NVARCHAR(50)='',@GMN1 NVARCHAR(10)='',@GMN2 NVARCHAR(10)='',@GMN3 NVARCHAR(10)='',@GMN4 NVARCHAR(10)='',@REPERCUTIDO INT,@RETENIDO INT AS   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_IMPUESTOS @IDI=@IDI,@COD=@COD,@DESCR=@DESCR,@PAIS=@PAIS,@PROV=@PROV,@ART=@ART,@GMN1=@GMN1,@GMN2=@GMN2,@GMN3=@GMN3,@GMN4=@GMN4,@REPERCUTIDO=@REPERCUTIDO,@RETENIDO=@RETENIDO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI NVARCHAR(5),@COD NVARCHAR(50)='''',@DESCR NVARCHAR(300)='''',@PAIS NVARCHAR(3)='''',@PROV NVARCHAR(3)='''',@ART NVARCHAR(50)='''',@GMN1 NVARCHAR(10)='''',@GMN2 NVARCHAR(10)='''',@GMN3 NVARCHAR(10)='''',@GMN4 NVARCHAR(10)='''',@REPERCUTIDO INT,@RETENIDO INT',  @IDI=@IDI,@COD=@COD,@DESCR=@DESCR,@PAIS=@PAIS,@PROV=@PROV,@ART=@ART,@GMN1=@GMN1,@GMN2=@GMN2,@GMN3=@GMN3,@GMN4=@GMN4,@REPERCUTIDO=@REPERCUTIDO,@RETENIDO=@RETENIDO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_LIN_FACTURAS_CD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_LIN_FACTURAS_CD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_LIN_FACTURAS_CD] @CIA INT, @PROVE NVARCHAR(100),@TIPO TINYINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_LIN_FACTURAS_CD @PROVE =@PROVE ,@TIPO =@TIPO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE NVARCHAR(100),@TIPO TINYINT', @PROVE =@PROVE ,@TIPO =@TIPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_DETALLE_ALBARAN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_DETALLE_ALBARAN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_DETALLE_ALBARAN] @CIA INT, @FACTURA INT, @ALBARAN NVARCHAR(100)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_DETALLE_ALBARAN @FACTURA=@FACTURA,@ALBARAN=@ALBARAN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@FACTURA INT, @ALBARAN NVARCHAR(100) ', @FACTURA=@FACTURA,@ALBARAN=@ALBARAN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_HAY_INTEGRACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_HAY_INTEGRACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_HAY_INTEGRACION] @CIA INT,@SENTIDO INT = NULL, @TABLA INT = NULL, @RESP INT=NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_INTEGRACION @SENTIDO =@SENTIDO,@TABLA=@TABLA,@RESP=@RESP OUTPUT '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SENTIDO INT = NULL, @TABLA INT = NULL,@RESP INT=NULL OUTPUT', @SENTIDO =@SENTIDO,@TABLA=@TABLA,@RESP=@RESP OUTPUT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta





End Sub

Public Function CodigoDeActualizacion31900_04_17_00A31900_04_17_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_00A31900_04_17_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_00A31900_04_17_01 = False
End Function


Private Sub V_31900_7_Storeds_001()
Dim sConsulta As String

'Storeds a Eliminar pq nadie los usa
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_CARGAR_CONFIG_PROCE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_CARGAR_CONFIG_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
'FIN Storeds a Eliminar pq nadie los usa

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_COMPROBAR_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_COMPROBAR_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_COMPROBAR_EMAIL](@FSP_CIA VARCHAR(20),@EMAIL VARCHAR(100),@EXISTE INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE   @CIA INTEGER" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CIA = (SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@FSP_CIA)" & vbCrLf
sConsulta = sConsulta & "SET @EXISTE = (SELECT COUNT(*)  FROM USU WITH(NOLOCK) INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA = USU_PORT.CIA AND USU.ID = USU_PORT.USU  WHERE USU.CIA=@CIA AND USU.EMAIL=@EMAIL)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB] @CIA_COMP INTEGER, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @ATRIB int = null, @ITEM int = null AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @AMBITO = @AMBITO, @GRUPO = @GRUPO, @ATRIB = @ATRIB, @ITEM = @ITEM'" & vbCrLf
sConsulta = sConsulta & "   SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @ATRIB int, @ITEM int'" & vbCrLf
sConsulta = sConsulta & "   EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @AMBITO = @AMBITO, @GRUPO = @GRUPO, @ATRIB = @ATRIB, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "   RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_MON]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  [dbo].[SP_ELIM_MON]  (@MON_COD NVARCHAR(50),@CODMONCENTRAL NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXISTE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMON INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMONCEN INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDMON=(SELECT ID FROM MON WITH(NOLOCK) WHERE COD=@MON_COD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDMON IS NOT NULL " & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "    SET @IDMONCEN=(SELECT ID FROM MON WITH(NOLOCK) WHERE COD=@CODMONCENTRAL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM CIAS WITH(NOLOCK) WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM PAI WITH(NOLOCK) WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE PAI SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM USU WITH(NOLOCK) WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM MON_DEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM MON WHERE ID=@IDMON" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PROV_REINTENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_PROV_REINTENTOS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR,@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(1000),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(100),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(100),@CIAS_PROVI VARCHAR(100),@CIAS_CP VARCHAR(100),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(50),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100),@PORT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@ESPEC INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CICLOS= (SELECT PROV_CICLOS FROM PARGEN_GEST WITH(NOLOCK)) " & vbCrLf
sConsulta = sConsulta & "SET @REINTENTOS = (SELECT PROV_REINTENTOS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = (SELECT PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_P INT,@IDI VARCHAR(50),@TIPOEMAIL SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1), @DECIMALFMT CHAR(1), @PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CICLOS <> 0 AND @PASADAS%@CICLOS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "    SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "    WHERE MVTOS_PROV_EST.EST = 1 AND MVTOS_PROV_EST.INTENTOS < @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "    OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                     SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                     FROM REL_CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "      SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "            FROM CIAS WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "            WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          END                                          " & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "        ELSE  " & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@USU' " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= '@COD_PROVE_CIA VARCHAR(200),@ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "          END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT FPEST FROM USU_PORT WITH(NOLOCK) WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3 " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                         @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "              FROM USU WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN IDI AS IDI WITH(NOLOCK) ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "                                         WHERE USU.CIA = @CIA_PROVE AND USU.ID= @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                               @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                               @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                               @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                               @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                               @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                               @DATEFMT=@DATEFMT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_DEP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                           @ID_P INT," & vbCrLf
sConsulta = sConsulta & "                           @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "                           @THOUSANFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                           @DECIMALFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                           @PRECISIONFMT TINYINT, " & vbCrLf
sConsulta = sConsulta & "                           @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "              EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                   @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                   @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                   @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                   @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                   @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                   @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                   @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                   @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                   @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                   @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            END     " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT FPEST FROM USU_PORT WITH(NOLOCK) WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3 " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                         @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "               FROM USU WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN IDI AS IDI WITH(NOLOCK) ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "               WHERE  USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "              SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                  @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @DATEFMT=@DATEFMT'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "              SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT," & vbCrLf
sConsulta = sConsulta & "                              @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                              @DECIMALFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                              @PRECISIONFMT TINYINT, " & vbCrLf
sConsulta = sConsulta & "                              @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                      @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                      @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "            END                                        " & vbCrLf
sConsulta = sConsulta & "          END " & vbCrLf
sConsulta = sConsulta & "        END " & vbCrLf
sConsulta = sConsulta & "        ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "        BEGIN   " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                      @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "            @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "        END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR <> 0            " & vbCrLf
sConsulta = sConsulta & "        BEGIN " & vbCrLf
sConsulta = sConsulta & "          SET @INTENTO = @INTENTO +1" & vbCrLf
sConsulta = sConsulta & "          IF @INTENTO = @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "            SET @EST_FILA = 2" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @EST_FILA = 3            " & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "    CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "  UPDATE PARGEN_GEST SET PROV_PASADAS = @PASADAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub

Public Function CodigoDeActualizacion31900_04_17_01A31900_04_17_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Tablas_002
        
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_01A31900_04_17_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_01A31900_04_17_02 = False
End Function

Private Sub V_31900_7_Tablas_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TBPARTIDAS' AND ss.name = N'dbo')" & vbCrLf
sConsulta = sConsulta & "DROP TYPE [dbo].[TBPARTIDAS] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_7_Storeds_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  " & vbCrLf
sConsulta = sConsulta & "   @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMFACTURA VARCHAR(100) = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO, @NUMPEDIDO=@NUMPEDIDO,@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE,@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP,@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO,@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA,@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE,@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE,@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES,@NUMFACTURA=@NUMFACTURA'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT, @NUMPEDIDO INT,@NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME,@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200),@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME,@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50), @PARTIDASPRES NVARCHAR(MAX), @NUMFACTURA VARCHAR(100)', " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                   @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                   @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                   @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                   @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                   @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                   @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                   @NUMFACTURA = @NUMFACTURA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'/****** Object:  StoredProcedure [dbo].[FSQA_VALIDARCERTIFICADO]    Script Date: 09/27/2012 17:29:29 ******/
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_VALIDARCERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_VALIDARCERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'/****** Object:  StoredProcedure [dbo].[FSQA_VER_PUNTUACION_CALIDAD]    Script Date: 09/27/2012 17:29:29 ******/
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_VER_PUNTUACION_CALIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_VER_PUNTUACION_CALIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_02A31900_04_17_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_02A31900_04_17_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_02A31900_04_17_03 = False
End Function

Private Sub V_31900_7_Storeds_003()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ELIMINAR_FACTURA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ELIMINAR_FACTURA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_ELIMINAR_FACTURA] @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "--''' <summary>" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ELIMINAR_FACTURA @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT',@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_ELIMINAR_INSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_ELIMINAR_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_ELIMINAR_INSTANCIA] @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "--''' <summary>" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ELIMINAR_INSTANCIA @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT',@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_VIASPAGO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_VIASPAGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_VIASPAGO] @CIA INT,  @COD VARCHAR(50) =NULL, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_VIASPAGO @COD =@COD,@IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =NULL,@IDIOMA VARCHAR(50)',@COD =@COD,@IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_HISTORICO_NOTIFICACIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_HISTORICO_NOTIFICACIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_HISTORICO_NOTIFICACIONES] @CIA INT, @INSTANCIA INT,  @PROVE NVARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_HISTORICO_NOTIFICACIONES @INSTANCIA=@INSTANCIA,@PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT,@PROVE NVARCHAR(50)',@INSTANCIA=@INSTANCIA,@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_03A31900_04_17_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Storeds_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_03A31900_04_17_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_03A31900_04_17_04 = False
End Function

Private Sub V_31900_7_Storeds_004()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLUCION_INSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLUCION_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLUCION_INSTANCIA] @CIA INT,  @CIAPROVE INT, @INSTANCIA INT,@PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT ', @INSTANCIA=@INSTANCIA, @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE] @CIA INT, @INSTANCIA INT,@IDCAMPO INT,@VERSION INT,@IDI VARCHAR(20)='SPA',  @USU VARCHAR(50)=NULL,   @PROVE VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTDESGLOSEPADREVISIBLE @INSTANCIA=@INSTANCIA, @IDCAMPO = @IDCAMPO, @VERSION=@VERSION,@IDI=@IDI, @USU = @USU, @PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDCAMPO  INT, @VERSION INT, @IDI VARCHAR(50),@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL', @INSTANCIA=@INSTANCIA, @IDCAMPO = @IDCAMPO, @VERSION=@VERSION, @IDI=@IDI, @USU = @USU, @PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_REALIZAR_ACCION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_REALIZAR_ACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_REALIZAR_ACCION] @CIA INT,  @CIAPROVE INT, @INSTANCIA INT, @INSTANCIABLOQUE INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL ,@DEVOLUCION TINYINT=0, @ETAPA INT , @USUNOM VARCHAR(500)=NULL,@IDIOMAPROVE VARCHAR(3)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @INSTANCIABLOQUE=@INSTANCIABLOQUE, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE, @DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @INSTANCIABLOQUE INT,  @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT,@DEVOLUCION TINYINT,@ETAPA INT , @USUNOM VARCHAR(500),@IDIOMAPROVE VARCHAR(3)=NULL  ', @INSTANCIA=@INSTANCIA,  @INSTANCIABLOQUE=@INSTANCIABLOQUE,  @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE,@DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEVOLUCION = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Por programa SET XACT_ABORT ON si falla FSPM_TRASPASAR_INSTANCIA aqui no llega" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET SOLIC_ACT= SOLIC_ACT - 1,SOLIC_NUE= SOLIC_NUE - 1 WHERE CIA_COMP = @CIA AND CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL, @RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, @ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, @FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, @EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, @ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, @PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, @NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, @FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, @FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, @FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, @ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, @CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, @CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, @CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, @UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, @PREC_VISIBLE int = NULL," & vbCrLf
sConsulta = sConsulta & " @PREC_WIDTH FLOAT = NULL, @IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, @IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, @IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, @MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, @CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL,@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL,@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL,@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, @PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL, @PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, @PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, @PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL, @BLOQ_FACTURA_VISIBLE INT = NULL, @BLOQ_FACTURA_WIDTH FLOAT = NULL, @NUM_LINEA_VISIBLE INT = NULL, @NUM_LINEA_WIDTH FLOAT = NULL, @FACTURA_VISIBLE INT = NULL, @FACTURA_WIDTH FLOAT = NULL, @RECEPTOR_VISIBLE INT = NULL, @RECEPTOR_WIDTH FLOAT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la información del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tamaño suficiente para ser visible pero no molestar" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_VISIBLE = @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_WIDTH = @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_WIDTH = @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT, @ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT, @PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT, @FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT, @CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, @UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT," & vbCrLf
sConsulta = sConsulta & " @CC2_WIDTH FLOAT,@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @BLOQ_FACTURA_VISIBLE INT, @BLOQ_FACTURA_WIDTH FLOAT, @NUM_LINEA_VISIBLE INT, @NUM_LINEA_WIDTH FLOAT, @ANCHODEFECTO INT'," & vbCrLf
sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_VISIBLE = @FACTURA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_WIDTH = @FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_WIDTH = @RECEPTOR_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT [RETRASADO_VISIBLE],[RETRASADO_WIDTH],[ALBARAN_VISIBLE],[ALBARAN_WIDTH],[FRECEP_VISIBLE],[FRECEP_WIDTH],[EMP_VISIBLE],[EMP_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[ERP_VISIBLE],[ERP_WIDTH],[PEDIDO_VISIBLE],[PEDIDO_WIDTH],[NUMPEDPROVE_VISIBLE],[NUMPEDPROVE_WIDTH],[FEMISION_VISIBLE],[FEMISION_WIDTH],[FENTREGA_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[FENTREGA_WIDTH],[FENTREGAPROVE_VISIBLE],[FENTREGAPROVE_WIDTH],[ART_VISIBLE],[ART_WIDTH],[CANTPED_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[CANTPED_WIDTH],[CANTREC_VISIBLE],[CANTREC_WIDTH],[CANTPEND_VISIBLE],[CANTPEND_WIDTH],[UNI_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[UNI_WIDTH],[PREC_VISIBLE],[PREC_WIDTH],[IMPPED_VISIBLE],[IMPPED_WIDTH],[IMPREC_VISIBLE],[IMPREC_WIDTH],[IMPPEND_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[IMPPEND_WIDTH],[MON_VISIBLE],[MON_WIDTH],[CC1_VISIBLE],[CC1_WIDTH],[CC2_VISIBLE],[CC2_WIDTH],[CC3_VISIBLE],[CC3_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[CC4_VISIBLE],[CC4_WIDTH],[PARTIDA1_VISIBLE],[PARTIDA1_WIDTH],[PARTIDA2_VISIBLE],[PARTIDA2_WIDTH],[PARTIDA3_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[PARTIDA3_WIDTH],[PARTIDA4_VISIBLE],[PARTIDA4_WIDTH], [BLOQ_FACTURA_VISIBLE],[BLOQ_FACTURA_WIDTH],[NUM_LINEA_VISIBLE],[NUM_LINEA_WIDTH],[FACTURA_VISIBLE],[FACTURA_WIDTH],[RECEPTOR_VISIBLE],[RECEPTOR_WIDTH]" & vbCrLf
sConsulta = sConsulta & "FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE USU = @IDUSU AND CIA = @CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_DETALLE_ACTIVO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_DETALLE_ACTIVO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSSM_DETALLE_ACTIVO @CIA INT,@IDIOMA VARCHAR(20), @CODACTIVO NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSSM_DETALLE_ACTIVO @IDIOMA=@IDIOMA,@CODACTIVO=@CODACTIVO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(20), @CODACTIVO NVARCHAR(20)',@IDIOMA=@IDIOMA,@CODACTIVO=@CODACTIVO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PERSONAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PERSONAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PERSONAS] @CIA INT, @COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL,@BAJALOG AS TINYINT=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PERSONAS @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP, @BAJALOG = @BAJALOG '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL,@BAJALOG AS TINYINT=1 ', @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP , @BAJALOG = @BAJALOG" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_REGISTRO_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_REGISTRO_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_REGISTRO_EMAIL] @CIA INT,@IDMAIL INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_REGISTRO_EMAIL @IDMAIL=@IDMAIL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDMAIL INT', @IDMAIL=@IDMAIL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
sConsulta = sConsulta & "   -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST WITH(NOLOCK) INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                        WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_04A31900_04_17_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Storeds_005
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_04A31900_04_17_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_04A31900_04_17_05 = False
End Function

Private Sub V_31900_7_Storeds_005()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PERSONAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PERSONAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PERSONAS] @CIA INT, @COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL,@BAJALOG AS TINYINT=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PERSONAS @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP, @BAJALOG = @BAJALOG '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL,@BAJALOG AS TINYINT=1 ', @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP , @BAJALOG = @BAJALOG" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_05A31900_04_17_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Storeds_006
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_05A31900_04_17_06 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_05A31900_04_17_06 = False
End Function

Private Sub V_31900_7_Storeds_006()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_CREATE_NEW_CERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_CREATE_NEW_CERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_PARAM_LITERAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_PARAM_LITERAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_PARAM_LITERAL]  @IDLIT INT, @IDIOMA NVARCHAR(50) = 'SPA', @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @CIA IS NULL AND @CIA <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS with (nolock) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT DEN FROM ' + @FSGS + 'PARGEN_LIT WITH(NOLOCK) WHERE ID = @IDLIT AND IDI=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDLIT INT, @IDIOMA NVARCHAR(50)', @IDLIT=@IDLIT, @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARGEN_INTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT ACCESO_FSSM, VER_NUMEXT, ACCESO_FSGS, ACCESO_FSFA FROM ' + @FSGS + 'PARGEN_INTERNO WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS] " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @PROCE SMALLINT," & vbCrLf
sConsulta = sConsulta & "   @COD_OLD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @COD_NEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @OFE_GR_ATRIB TABLE (ID INT,GRUPO VARCHAR(50),ATRIB_ID INT,VALOR_NUM FLOAT,VALOR_TEXT VARCHAR(800)," & vbCrLf
sConsulta = sConsulta & "                                VALOR_FEC DATETIME,VALOR_BOOL TINYINT,IMPPARCIAL FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   --Antes del borrado se hace una inserci�n en la var. tabla @OFE_GR_ATRIB porque GRUPO es una foreign key a GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO @OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ATRIB     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- GRUPO_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE GRUPO_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM GRUPO_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=GRUPO_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND GRUPO_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- ITEM_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE ITEM_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM ITEM_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=ITEM_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND ITEM_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ADJUN ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ADJUN,OGA.NOM,OGA.COM,OGA.IDPORTAL,OGA.IDGS,OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ADJUN     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ---------- " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,OGA.GRUPO,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM @OFE_GR_ATRIB OGA      " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_COSTESDESC ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "   SELECT OGC.ID,@COD_NEW,OGC.ATRIB_ID,OGC.IMPPARCIAL,OGC.FECACT" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_COSTESDESC    " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_ITEM_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ITEM_ATRIB SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OFE_ITEM_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OFE_ITEM_ATRIB.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END    " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_06A31900_04_17_07() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
      '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Tablas_003
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Triggers_000
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_06A31900_04_17_07 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_06A31900_04_17_07 = False
End Function

Private Sub V_31900_7_Tablas_003()

Dim sConsulta As String


sConsulta = "IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'FECACT' and Object_ID = Object_ID(N'PROCE_OFE')) " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "ADD " & vbCrLf
sConsulta = sConsulta & "[FECACT] DATETIME NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_7_Triggers_000()
Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[PROCE_OFE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_INSUPD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[PROCE_OFE_TG_INSUPD] ON [dbo].[PROCE_OFE]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE  IO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I ON" & vbCrLf
sConsulta = sConsulta & "              IO.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update proce_ofe set fecact=getdate()" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_04_17_07A31900_04_17_08() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Storeds_007
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_07A31900_04_17_08 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_07A31900_04_17_08 = False
End Function

Private Sub V_31900_7_Storeds_007()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
sConsulta = sConsulta & "   -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD SIN WITH(NOLOCK) modificado desde el 09/10/2012 versi�n 31900.6 tarea 2519" & vbCrLf
sConsulta = sConsulta & "   --De poner  WITH(NOLOCK) se obtendr�a este error" & vbCrLf
sConsulta = sConsulta & "   --      Msg 16941, Level 16, State 1, Procedure SP_ACTUALIZAR_PROV_GS, Line 416" & vbCrLf
sConsulta = sConsulta & "   --      Cursor updates are not allowed on tables opened with the NOLOCK option." & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                        WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_04_17_08A31900_04_17_09() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Datos_009
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_08A31900_04_17_09 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_08A31900_04_17_09 = False
End Function

Private Sub V_31900_7_Datos_009()
Dim sConsulta As String

gRDOCon.QueryTimeout = 7200

sConsulta = "UPDATE FSP_CONF_VISOR_RECEPCIONES  SET BLOQ_FACTURA_WIDTH =0 WHERE BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE FSP_CONF_VISOR_RECEPCIONES  SET RECEPTOR_WIDTH =0 WHERE RECEPTOR_WIDTH IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE FSP_CONF_VISOR_RECEPCIONES  SET FACTURA_WIDTH =0 WHERE FACTURA_WIDTH IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE FSP_CONF_VISOR_RECEPCIONES  SET NUM_LINEA_WIDTH =0 WHERE NUM_LINEA_WIDTH IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_09A31900_04_17_10() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Storeds_010
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_09A31900_04_17_10 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_09A31900_04_17_10 = False
End Function

Private Sub V_31900_7_Storeds_010()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_ACT_REL_CIAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_ACT_REL_CIAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_SOLPENDIENTES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_SOLPENDIENTES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_SOLPENDIENTES] @CIACOMP FLOAT, @CIAPROVE FLOAT, @USU  VARCHAR(20)='', @TIPO AS INT=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMORDEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMORDENNUE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIACOMP  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = RC.COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) WHERE RC.CIA_COMP = @CIACOMP AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSEP_NUM_PED @PROVE=@PROVE, @TIPO=@TIPO, @NUMORDEN=@NUMORDEN OUTPUT, @NUMORDENNUE=@NUMORDENNUE OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @TIPO INT, @NUMORDEN INT OUTPUT, @NUMORDENNUE INT OUTPUT', @PROVE=@PROVEGS, @TIPO=@TIPO, @NUMORDEN =@NUMORDEN OUTPUT, @NUMORDENNUE=@NUMORDENNUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USU<>''" & vbCrLf
sConsulta = sConsulta & "   SELECT GETDATE() AS DIA, USU.NOM + ' ' + USU.APE AS NOMBRE, @NUMORDEN AS ORDEN_ACT, @NUMORDENNUE AS ORDEN_NUE" & vbCrLf
sConsulta = sConsulta & "   FROM REL_CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN USU WITH(NOLOCK) ON USU.CIA = REL_CIAS.CIA_PROVE AND COD=@USU" & vbCrLf
sConsulta = sConsulta & "   WHERE CIA_COMP = @CIACOMP AND CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT  @NUMORDEN AS ORDEN_ACT, @NUMORDENNUE AS ORDEN_NUE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_04_17_10A31900_04_17_11() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Storeds_011
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_10A31900_04_17_11 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_10A31900_04_17_11 = False
End Function


Private Sub V_31900_7_Storeds_011()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  " & vbCrLf
sConsulta = sConsulta & "    @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMFACTURA VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS_MPF @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO, @NUMPEDIDO=@NUMPEDIDO,@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE,@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP,@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO,@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA,@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE,@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE,@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES,@NUMFACTURA=@NUMFACTURA'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT, @NUMPEDIDO INT,@NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME,@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200),@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME,@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50), @PARTIDASPRES NVARCHAR(MAX), @NUMFACTURA VARCHAR(20)', " & vbCrLf
sConsulta = sConsulta & "                   @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                   @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                   @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                   @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                   @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                   @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                   @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                   @NUMFACTURA = @NUMFACTURA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_IMPUESTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_IMPUESTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_IMPUESTOS] @CIA INT, @IDI NVARCHAR(5),@COD NVARCHAR(50)='',@DESCR NVARCHAR(300)='',@PAIS NVARCHAR(3)='',@PROV NVARCHAR(3)='',@ART NVARCHAR(50)='',@GMN1 NVARCHAR(10)='',@GMN2 NVARCHAR(10)='',@GMN3 NVARCHAR(10)='',@GMN4 NVARCHAR(10)='',@REPERCUTIDO INT,@RETENIDO INT, @CONCEPTO INT AS    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_IMPUESTOS @IDI=@IDI,@COD=@COD,@DESCR=@DESCR,@PAIS=@PAIS,@PROV=@PROV,@ART=@ART,@GMN1=@GMN1,@GMN2=@GMN2,@GMN3=@GMN3,@GMN4=@GMN4,@REPERCUTIDO=@REPERCUTIDO,@RETENIDO=@RETENIDO, @CONCEPTO=@CONCEPTO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI NVARCHAR(5),@COD NVARCHAR(50)='''',@DESCR NVARCHAR(300)='''',@PAIS NVARCHAR(3)='''',@PROV NVARCHAR(3)='''',@ART NVARCHAR(50)='''',@GMN1 NVARCHAR(10)='''',@GMN2 NVARCHAR(10)='''',@GMN3 NVARCHAR(10)='''',@GMN4 NVARCHAR(10)='''',@REPERCUTIDO INT,@RETENIDO INT, @CONCEPTO INT',  @IDI=@IDI,@COD=@COD,@DESCR=@DESCR,@PAIS=@PAIS,@PROV=@PROV,@ART=@ART,@GMN1=@GMN1,@GMN2=@GMN2,@GMN3=@GMN3,@GMN4=@GMN4,@REPERCUTIDO=@REPERCUTIDO,@RETENIDO=@RETENIDO, @CONCEPTO=@CONCEPTO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL, @RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, @ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, @FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, @EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, @ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, @PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, @NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, @FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, @FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, @FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, @ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, @CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, @CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, @CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, @UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, " & vbCrLf
sConsulta = sConsulta & "@PREC_VISIBLE int = NULL, @PREC_WIDTH FLOAT = NULL, @IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, @IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, @IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, @MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, @CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL,@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL,@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL,@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, @PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL, @PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, @PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, @PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL, @BLOQ_FACTURA_VISIBLE INT = NULL, @BLOQ_FACTURA_WIDTH FLOAT = NULL, @NUM_LINEA_VISIBLE INT = NULL, @NUM_LINEA_WIDTH FLOAT = NULL, @FACTURA_VISIBLE INT = NULL, @FACTURA_WIDTH FLOAT = NULL, @RECEPTOR_VISIBLE INT = NULL, @RECEPTOR_WIDTH FLOAT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la información del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tamaño suficiente para ser visible pero no molestar" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUM_LINEA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RECEPTOR_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUM_LINEA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_VISIBLE = @FACTURA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FACTURA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_WIDTH = @FACTURA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RECEPTOR_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_WIDTH = @RECEPTOR_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT, @ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT, @PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT, @FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT, @CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, @UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT," & vbCrLf
sConsulta = sConsulta & "@CC2_VISIBLE INT, @CC2_WIDTH FLOAT,@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @BLOQ_FACTURA_VISIBLE INT, @BLOQ_FACTURA_WIDTH FLOAT, @NUM_LINEA_VISIBLE INT, @NUM_LINEA_WIDTH FLOAT, @FACTURA_VISIBLE INT, @FACTURA_WIDTH FLOAT, @RECEPTOR_VISIBLE INT, @RECEPTOR_WIDTH FLOAT, @ANCHODEFECTO INT'," & vbCrLf
sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_VISIBLE = @FACTURA_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @FACTURA_WIDTH = @FACTURA_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE," & vbCrLf
sConsulta = sConsulta & "               @RECEPTOR_WIDTH = @RECEPTOR_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_11A31900_04_17_12() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Storeds_012
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.12'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_11A31900_04_17_12 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_11A31900_04_17_12 = False
End Function

Private Sub V_31900_7_Storeds_012()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  " & vbCrLf
sConsulta = sConsulta & "    @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMFACTURA VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO, @NUMPEDIDO=@NUMPEDIDO,@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE,@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP,@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO,@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA,@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE,@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE,@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES,@NUMFACTURA=@NUMFACTURA'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT, @NUMPEDIDO INT,@NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME,@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200),@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME,@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50), @PARTIDASPRES NVARCHAR(MAX), @NUMFACTURA VARCHAR(20)', " & vbCrLf
sConsulta = sConsulta & "                   @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                   @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                   @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                   @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                   @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                   @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                   @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                   @NUMFACTURA = @NUMFACTURA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE] @CIA INT,@PROVE VARCHAR(50), @IDI VARCHAR(50), @TIPO INT =0 , @FECHA  VARCHAR(10)=NULL, @INSTANCIA INT=0, @FILAESTADO INT =0, @FORMATOFECHA VARCHAR(20)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETWORKFLOW_PROVE @PRV=@PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@FILAESTADO=@FILAESTADO,@FORMATOFECHA=@FORMATOFECHA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @IDI VARCHAR(50) , @TI INT,@FEC  VARCHAR(10),@INS INT, @FILAESTADO INT,@FORMATOFECHA  VARCHAR(20)', @PRV=@PROVEGS,@IDI=@IDI, @TI=@TIPO,@FEC=@FECHA,@INS=@INSTANCIA,@FILAESTADO=@FILAESTADO,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub

Public Function CodigoDeActualizacion31900_04_17_12A31900_04_17_13() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_7_Tablas_004
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Storeds_013
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.13'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_12A31900_04_17_13 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_12A31900_04_17_13 = False
End Function

Private Sub V_31900_7_Storeds_013()

Dim sConsulta As String

gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETDESGLOSE] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDESGLOSE @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD, @PORTAL=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARGEN_INTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT PI.ACCESO_FSSM, PI.VER_NUMEXT, PI.ACCESO_FSGS, ACCESO_FSFA, PED_DIR_ENVIO_EMAIL FROM ' + @FSGS + 'PARGEN_INTERNO PI WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_31900_7_Tablas_004()
Dim sConsulta As String

sConsulta = "IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'AUTOFACTURA' and Object_ID = Object_ID(N'SESION'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SESION ADD AUTOFACTURA TINYINT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_13A31900_04_17_14() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Storeds_014
    '-------------------------------------------------------------------------------------------
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.14'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_13A31900_04_17_14 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_13A31900_04_17_14 = False
End Function


Private Sub V_31900_7_Storeds_014()

Dim sConsulta As String

gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_BUSCAR_CENTROCOSTE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_BUSCAR_CENTROCOSTE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_BUSCAR_CENTROCOSTE] @CIA INT, @IDI VARCHAR(20),@COD VARCHAR(10)  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT A.COD,A.DEN,A.CENTRO_SM  from(SELECT CSU.UON1 + case when csu.uon2 is not null then ''#'' else '''' end  + CSU.UON2 + case when csu.uon3 is not null then ''#'' else '''' end + CSU.UON3 + case when csu.uon4 is not null then ''#'' else '''' end + CSU.UON4 as COD,COALESCE(CSU.UON4, CSU.UON3, CSU.UON2, CSU.UON1) COD_UON,COALESCE(CSU.UON4, CSU.UON3, CSU.UON2, CSU.UON1) + '' - '' + SD.DEN AS DEN, CSU.CENTRO_SM " & vbCrLf
sConsulta = sConsulta & "   FROM ' + @FSGS + 'CENTRO_SM_UON CSU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'CENTRO_SM_DEN SD WITH(NOLOCK)ON CSU.CENTRO_SM=SD.CENTRO_SM AND IDI=@IDI) A " & vbCrLf
sConsulta = sConsulta & "   WHERE A.COD_UON = @COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@IDI VARCHAR(20),@COD VARCHAR(10)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM ,@IDI=@IDI,@COD=@COD" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_04_17_14A31900_04_17_15() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
     '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Datos_015
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Triggers_015
    '-------------------------------------------------------------------------------------------
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.15'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_14A31900_04_17_15 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_14A31900_04_17_15 = False
End Function


Private Sub V_31900_7_Triggers_015()
Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[PROCE_OFE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)    " & vbCrLf
sConsulta = sConsulta & "   drop trigger [dbo].[PROCE_OFE_TG_INSUPD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[PROCE_OFE_TG_INSUPD] ON [dbo].[PROCE_OFE]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET FECACT = GETDATE() FROM PROCE_OFE  IO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I ON  IO.ID = I.ID" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_7_Datos_015()
Dim sConsulta As String

gRDOCon.QueryTimeout = 7200

sConsulta = "UPDATE PROCE_OFE SET FECACT=GETDATE() WHERE FECACT IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_04_17_15A31900_04_17_16() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
     '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_7_Storeds_016
    '-------------------------------------------------------------------------------------------
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.17.16'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_17_15A31900_04_17_16 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_17_15A31900_04_17_16 = False
End Function

Private Sub V_31900_7_Storeds_016()

Dim sConsulta As String

gRDOCon.QueryTimeout = 7200

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PARAMETROS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PARAMETROS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PARAMETROS]  @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @CIA IS NULL AND @CIA <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ACCESO_FSSM FROM ' + @FSGS + 'PARGEN_INTERNO WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT OBLCODPEDDIR,OBLCODPEDIDO FROM ' + @FSGS + 'PARGEN_PED WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT ID,IDI,DEN FROM ' + @FSGS + 'PARGEN_LIT WITH (NOLOCK) ORDER BY ID, IDI'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
