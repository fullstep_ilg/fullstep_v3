Attribute VB_Name = "bas_V_3_0"
Option Explicit

Public Function CodigoDeActualizacion2_17_00_00A3_00_00_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   sConsulta = "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, sConsulta
   sConsulta = "SET XACT_ABORT OFF"
   ExecuteSQL gRDOCon, sConsulta
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambio a la versi�n 3.00"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
       frmProgreso.ProgressBar1.Value = 1
   End If
   
   frmProgreso.lblDetalle = "Cambios en las tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
       frmProgreso.ProgressBar1.Value = 1
   End If
   V_3_0_Tablas_0
   
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
       frmProgreso.ProgressBar1.Value = 1
   End If
   V_3_0_Storeds_0
   
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   sConsulta = "COMMIT TRANSACTION"
   ExecuteSQL gRDOCon, sConsulta
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion2_17_00_00A3_00_00_00 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion2_17_00_00A3_00_00_00 = False
End Function

Private Function V_3_0_Tablas_0()

Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_COPIA_CAMPO_ADJUN_COPIA_CAMPO]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_CAMPO_ADJUN] DROP CONSTRAINT FK_COPIA_CAMPO_ADJUN_COPIA_CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_COPIA_LINEA_DESGLOSE_ADJUN_COPIA_LINEA_DESGLOSE]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_LINEA_DESGLOSE_ADJUN] DROP CONSTRAINT FK_COPIA_LINEA_DESGLOSE_ADJUN_COPIA_LINEA_DESGLOSE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ACT_REL_CIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ACT_REL_CIAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARBOLPRESUPUESTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARBOLPRESUPUESTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DESTINOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DESTINOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLUCION_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLUCION_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_EXECREMOTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_EXECREMOTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_FORMASPAGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_FORMASPAGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETDICTIONARYDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETDICTIONARYDATA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELDS_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNDATA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTFIELD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTFIELD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETREQUEST]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETREQUEST]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETREQUESTADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETREQUESTADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETREQUESTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETREQUESTADJUNDATA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMNS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMNS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_MONEDAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_MONEDAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PAISES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PAISES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRES1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRES1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRES2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRES2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRES3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRES3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRES4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRES4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SAVE_INSTANCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SAVE_INSTANCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SETREQUESTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SETREQUESTADJUNDATA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UNIDADES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UNIDADES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UONSPRES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UONSPRES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSP_LOGIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSP_LOGIN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_ADJUN]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_ADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_CAMPO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_CAMPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_CAMPO_ADJUN]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_CAMPO_ADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_LINEA_DESGLOSE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_LINEA_DESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_LINEA_DESGLOSE_ADJUN]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_LINEA_DESGLOSE_ADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WEBFSPMTEXT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[WEBFSPMTEXT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[COPIA_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATA] [image] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECALTA] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [nvarchar] (300) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT] [nvarchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN_ORIGEN] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[COPIA_CAMPO] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [INSTANCIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [nvarchar] (4000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ES_SUBCAMPO] [tinyint] NULL CONSTRAINT [DF_COPIA_CAMPO_ES_SUBCAMPO] DEFAULT (0)  " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[COPIA_CAMPO_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_GS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [nvarchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (20) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT] [nvarchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECALTA] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [RUTA] [nvarchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN_PORTAL] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[COPIA_LINEA_DESGLOSE] (" & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_PADRE] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_HIJO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [nvarchar] (4000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA_OLD] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[COPIA_LINEA_DESGLOSE_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_GS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_PADRE] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_HIJO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [nvarchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (20) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT] [nvarchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECALTA] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [RUTA] [nvarchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN_PORTAL] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[REL_CIAS] " & vbCrLf
sConsulta = sConsulta & " ADD " & vbCrLf
sConsulta = sConsulta & "   [SOLIC_ACT] [int] NOT NULL CONSTRAINT DF_REL_CIAS_SOLIC_ACT DEFAULT 0," & vbCrLf
sConsulta = sConsulta & "   [SOLIC_NUE] [int] NOT NULL CONSTRAINT DF_REL_CIAS_SOLIC_NUE DEFAULT 0 " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE TABLE [dbo].[WEBFSPMTEXT] (" & vbCrLf
sConsulta = sConsulta & "   [MODULO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [TEXT_SPA] [nvarchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [TEXT_ENG] [nvarchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [TEXT_GER] [nvarchar] (2000) NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_CAMPO] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_CAMPO] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_CAMPO_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_CAMPO_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_LINEA_DESGLOSE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_LINEA_DESGLOSE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [LINEA]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_PADRE]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_HIJO]" & vbCrLf
sConsulta = sConsulta & "   ) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_LINEA_DESGLOSE_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_LINEA_DESGLOSE_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[WEBFSPMTEXT] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_WEBFSPMTEXT] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MODULO]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_CAMPO_ADJUN] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_COPIA_CAMPO_ADJUN_COPIA_CAMPO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CAMPO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[COPIA_CAMPO] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_LINEA_DESGLOSE_ADJUN] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_COPIA_LINEA_DESGLOSE_ADJUN_COPIA_LINEA_DESGLOSE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [LINEA]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_PADRE]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_HIJO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[COPIA_LINEA_DESGLOSE] (" & vbCrLf
sConsulta = sConsulta & "       [LINEA]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_PADRE]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_HIJO]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Function

Private Function V_3_0_Storeds_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_ACT_REL_CIAS(@COMP VARCHAR(50),@PROVE VARCHAR(50),@ACTIVAS INTEGER,@NUEVAS INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP AS INT" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET SOLIC_ACT=@ACTIVAS,SOLIC_NUE=@NUEVAS " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP=@ID_COMP " & vbCrLf
sConsulta = sConsulta & "   AND COD_PROVE_CIA=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_ARBOLPRESUPUESTOS @CIA INT, @TIPO INTEGER, @IDI VARCHAR(50), @ANYO INTEGER, @UON1 AS VARCHAR(50) = NULL, @UON2 AS VARCHAR(50) = NULL, @UON3 AS VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARBOLPRESUPUESTOS @TIPO=@TIPO, @IDI =@IDI , @ANYO =@ANYO , @UON1 =@UON1 , @UON2 =@UON2 , @UON3 =@UON3 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INTEGER, @IDI VARCHAR(50), @ANYO INTEGER, @UON1 AS VARCHAR(50) = NULL, @UON2 AS VARCHAR(50) = NULL, @UON3 AS VARCHAR(50) = NULL', @TIPO=@TIPO, @IDI =@IDI , @ANYO =@ANYO , @UON1 =@UON1 , @UON2 =@UON2 , @UON3 =@UON3" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_ARTICULOS @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_DESTINOS @CIA INT, @PER VARCHAR(50) =NULL, @COD VARCHAR(50) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DESTINOS @PER =@PER ,@COD =@COD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PER VARCHAR(50) =NULL, @COD VARCHAR(50) =NULL', @PER =@PER ,@COD =@COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_DEVOLUCION_INSTANCIA @CIA INT, @INSTANCIA INT,@PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "PRINT 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT ', @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE" & vbCrLf
sConsulta = sConsulta & "PRINT 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_EXECREMOTO @CIA INT, @STMT NTEXT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'SP_EXECUTESQL @STMT = @STMT '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@STMT NTEXT', @STMT =@STMT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_FORMASPAGO @CIA INT,  @COD VARCHAR(50) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_FORMASPAGO @COD =@COD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =NULL',@COD =@COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETADJUN @CIA INT, @TIPO INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUN @TIPO =@TIPO, @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @TIPO INT, @ID INT', @TIPO =@TIPO, @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE dbo.FSPM_GETDICTIONARYDATA @MODULEID int,@LANGUAGE varchar(3)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT ID,TEXT_' + @LANGUAGE + ' FROM WEBFSPMTEXT WHERE MODULO=@MODULEID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@MODULEID INT', @MODULEID=@MODULEID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETFIELDS_INSTANCIA @CIA INT, @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL ,@PROVE VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT * FROM COPIA_CAMPO WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT C.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE FROM COPIA_CAMPO_ADJUN A INNER JOIN COPIA_CAMPO C ON A.CAMPO = C.ID " & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA=@ID AND C.ES_SUBCAMPO=0 AND IDIOMA =ISNULL(@IDI,IDIOMA) ORDER BY A.CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTADJUN @CIA INT, @TIPO INT, @ID INT, @PORTAL TINYINT  = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@TIPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "1: El adjunto est� guardado en la BD del gs en COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "2,4: El adjunto est� guardado en la BD del portal en COPIA_ADJUN (lo ha metido el proveedor y todav�a no ha sido trasferido al gs o todav�a no ha guardado la instancia)" & vbCrLf
sConsulta = sConsulta & "3: El adjunto est� guardado en la BD del gs en COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @TIPO=1 OR @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUN @TIPO =@TIPO, @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N' @TIPO INT, @ID INT', @TIPO =@TIPO, @ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "       SELECT  A.ID, A.FECALTA, U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM, DATALENGTH(DATA) DATASIZE , A.COMENT" & vbCrLf
sConsulta = sConsulta & "         FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "              LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                     ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                    AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID = @ID" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @TIPO=1" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT  A.ID, A.FECALTA, A.PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, A.DATASIZE , A.COMENT, A.IDIOMA " & vbCrLf
sConsulta = sConsulta & "         FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                               LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                      ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                     AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                               ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID = @ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO=3" & vbCrLf
sConsulta = sConsulta & "        SELECT  A.ID, A.FECALTA, A.PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, A.DATASIZE , A.COMENT, A.IDIOMA " & vbCrLf
sConsulta = sConsulta & "          FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                   LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                   ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                  AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                          ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "         WHERE A.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT  A.ID, A.FECALTA, U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM, DATALENGTH(DATA) DATASIZE , A.COMENT" & vbCrLf
sConsulta = sConsulta & "          FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                   ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                  AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "         WHERE A.ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTADJUNDATA @CIA INT=NULL, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0, @PORTAL TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @TIPO=1 OR @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN  WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "      if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "        SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "      ELSE " & vbCrLf
sConsulta = sConsulta & "        BEGIN " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "          SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "        END " & vbCrLf
sConsulta = sConsulta & "      READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SELECT @IDGS = ID_GS FROM COPIA_CAMPO_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "          IF @IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA INNER JOIN COPIA_CAMPO_ADJUN  CCA ON CA.ID = CCA.ADJUN_PORTAL WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "              if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "              ELSE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "              READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "              EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@IDGS, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SELECT @IDGS = ID_GS FROM COPIA_LINEA_DESGLOSE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "          IF @IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN  CCA ON CA.ID = CCA.ADJUN_PORTAL WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "              if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "              ELSE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "              READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "              EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@IDGS, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL , @PORTAL tinyint = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @PORTAL = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & " IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "   SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA, 0 PORTAL" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "      IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                        ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & " IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "   SELECT  3 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA, 1 PORTAL" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "     ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  3 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                  WHERE A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  4 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0)  DATASIZE , A.COMENT, @IDI IDIOMA, A.ID ADJUN, A.CAMPO,  NULL RUTA" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                  WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                      ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "PRINT @SQL" & vbCrLf
sConsulta = sConsulta & "     EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTANCIA @CIA INT, @ID INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTANCIA @ID=@ID, @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI AS VARCHAR(20) =NULL', @ID=@ID, @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTDESGLOSE @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50) ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT  * FROM COPIA_CAMPO WHERE ID = @COPIA_CAMPO)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD  WHERE LD.CAMPO_PADRE=@COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  ' ' NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN LDA " & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_PADRE = @COPIA_CAMPO ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTFIELD @CIA INT, @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT, @PORTAL TINYINT = 0 OUTPUT  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTFIELD @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT', @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT * FROM COPIA_CAMPO WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETREQUEST @CIA INT, @ID INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUEST @ID=@ID, @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI AS VARCHAR(20) =NULL', @ID=@ID, @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETREQUESTADJUN @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUESTADJUN @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT ',@ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETREQUESTADJUNDATA @CIA INT, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUESTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0 ',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GMN1 @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN1 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50) ,  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GMN2 @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN2 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50), @GMN2 VARCHAR(50) ,  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GMN3 @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL, @GMN2 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN3 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 , @GMN2 = @GMN2 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50), @GMN2 VARCHAR(50) ,  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @GMN2 = @GMN2 ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GMN4 @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL, @GMN2 VARCHAR(50) = NULL,  @GMN3 VARCHAR(50) = NULL, @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN4 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 , @GMN2 = @GMN2 , @GMN3 =@GMN3 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50), @GMN2 VARCHAR(50) ,  @GMN3 VARCHAR(50) = NULL, @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @GMN2 = @GMN2 ,@GMN3 =@GMN3  ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GMNS @CIA INT,@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) = 'SPA' AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMNS @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) ', @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_LOADINSTFIELDSCALC @CIA INT, @INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)='SPA' AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSCALC @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)', @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_LOADINSTFIELDSDESGLOSECALC @CIA INT, @CAMPO INT AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSDESGLOSECALC @CAMPO=@CAMPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT', @CAMPO=@CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_MONEDAS @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_MONEDAS @COD =@COD , @DEN =@DEN , @COINCID =@COINCID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50), @DEN VARCHAR(2000) , @COINCID TINYINT ', @COD =@COD , @DEN =@DEN , @COINCID =@COINCID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_PAISES @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PAISES @COD =@COD , @DEN =@DEN ,@COINCID  =@COINCID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @COINCID TINYINT  ', @COD =@COD , @DEN =@DEN , @COINCID  =@COINCID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_PRES1 @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES1 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_PRES2 @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES2 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_PRES3 @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES3 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_PRES4 @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES4 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_PROVE @CIA INT, @PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVE @PROVE =@PROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50) ', @PROVE =@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_PROVES @CIA INT, @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVES @COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL ',@COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_PROVIS @CIA INT,@PAI VARCHAR(50),@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @USALIKE TINYINT =null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVIS @PAI=@PAI,@COD =@COD , @DEN =@DEN ,@USALIKE  =@USALIKE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PAI VARCHAR(50),@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT', @COD =@COD , @DEN =@DEN , @USALIKE  =@USALIKE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_SAVE_INSTANCE @CIA INT, @INSTANCIA INT, @PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @ENVIAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_SAVE_INSTANCE @INSTANCIA=@INSTANCIA, @PETICIONARIO =@PETICIONARIO , @COMPRADOR =@COMPRADOR , @MON =@MON , @ENVIAR =@ENVIAR , @IMPORTE =@IMPORTE , @PEDIDO_AUT =@PEDIDO_AUT '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @ENVIAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0 ', @INSTANCIA=@INSTANCIA, @PETICIONARIO =@PETICIONARIO , @COMPRADOR =@COMPRADOR , @MON =@MON , @ENVIAR =@ENVIAR , @IMPORTE =@IMPORTE , @PEDIDO_AUT =@PEDIDO_AUT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_SETREQUESTADJUNDATA @CIA INT,  @ID INT, @INIT INT, @OFFSET INT, @DATA IMAGE AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_SETREQUESTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @ID INT, @INIT INT, @OFFSET INT, @DATA IMAGE ',@ID = @ID, @INIT=@INIT, @OFFSET=@OFFSET, @DATA=@DATA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_UNIDADES @CIA INT,@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT = 1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UNIDADES @COD =@COD , @DEN =@DEN ,@USALIKE  =@USALIKE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT  ', @COD =@COD , @DEN =@DEN , @USALIKE  =@USALIKE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_UONSPRES @CIA INT,@TIPO INT, @ANYO INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UONSPRES @TIPO=@TIPO, @ANYO = @ANYO , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @ANYO INT, @IDI VARCHAR(50) ', @TIPO=@TIPO, @ANYO = @ANYO , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_UPLOAD_ADJUN @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @CIA INT =NULL, @USU INT =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO COPIA_ADJUN (DATA, NOM, COMENT, FECALTA,CIA,USU) VALUES (NULL, @NOM, @COMENT, GETDATE(),@CIA, @USU)" & vbCrLf
sConsulta = sConsulta & "    SET @ID = (SELECT MAX(ID) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    UPDATE COPIA_ADJUN SET NOM = @NOM , COMENT = @COMENT WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
sConsulta = sConsulta & "    UPDATE COPIA_ADJUN SET DATA = @DATA WHERE ID =@ID " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    UPDATETEXT COPIA_ADJUN.DATA @textpointer @init 0 @data" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_UPLOAD_ADJUN_GS @CIA INT,   @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UPLOAD_ADJUN @INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL  ',@INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSP_LOGIN @CIA VARCHAR(50), @COD VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
sConsulta = sConsulta & "       CIAS.USUPPAL AS USUPPAL, CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
sConsulta = sConsulta & "       USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
sConsulta = sConsulta & "       USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "       USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "       USU.DATEFMT DATEFMT, USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "       IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
sConsulta = sConsulta & "       USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA , MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU" & vbCrLf
sConsulta = sConsulta & "  FROM USU " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN USU_PORT " & vbCrLf
sConsulta = sConsulta & "               ON USU.ID=USU_PORT.USU " & vbCrLf
sConsulta = sConsulta & "              AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "              AND USU.CIA=USU_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "               ON USU.CIA=CIAS.ID " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN CIAS_PORT " & vbCrLf
sConsulta = sConsulta & "               ON USU.CIA=CIAS_PORT.CIA " & vbCrLf
sConsulta = sConsulta & "              AND CIAS_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN MON " & vbCrLf
sConsulta = sConsulta & "               ON CIAS.MON=MON.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN MON MON2 " & vbCrLf
sConsulta = sConsulta & "               ON USU.MON=MON2.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN IDI " & vbCrLf
sConsulta = sConsulta & "               ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & " WHERE USU.COD=@COD" & vbCrLf
sConsulta = sConsulta & "   AND CIAS.COD=@CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function


Public Function CodigoDeActualizacion3_00_00_00A3_00_00_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   sConsulta = "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, sConsulta
   sConsulta = "SET XACT_ABORT OFF"
   ExecuteSQL gRDOCon, sConsulta
   bTransaccionEnCurso = True
       
   
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
       frmProgreso.ProgressBar1.Value = 1
   End If
   V_3_0_Storeds_1
   
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   sConsulta = "COMMIT TRANSACTION"
   ExecuteSQL gRDOCon, sConsulta
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_00A3_00_00_01 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_00A3_00_00_01 = False
End Function
Private Sub V_3_0_Storeds_1()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLUCION_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLUCION_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELDS_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_DEVOLUCION_INSTANCIA @CIA INT, @INSTANCIA INT,@PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT ', @INSTANCIA=@INSTANCIA, @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETFIELDS_INSTANCIA @CIA INT, @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL ,@PROVE VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT * FROM COPIA_CAMPO WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT C.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE FROM COPIA_CAMPO_ADJUN A INNER JOIN COPIA_CAMPO C ON A.CAMPO = C.ID " & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA=@ID AND C.ES_SUBCAMPO=0 ORDER BY A.CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTDESGLOSE @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50) ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT  * FROM COPIA_CAMPO WHERE ID = @COPIA_CAMPO)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD  WHERE LD.CAMPO_PADRE=@COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  ' ' NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN LDA " & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_PADRE = @COPIA_CAMPO ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_01A3_00_00_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   V_3_0_Tablas_2
   
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   V_3_0_Storeds_2
   
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_01A3_00_00_02 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_01A3_00_00_02 = False
End Function

Private Sub V_3_0_Storeds_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_REL_CIAS_NEW_CERTIF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_REL_CIAS_NEW_CERTIF]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ACT_REL_CIAS_NEW_CERTIF(@COMP VARCHAR(50),@PROVE VARCHAR(50),@NUEVAS INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP AS INT" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET CERT_NUE=@NUEVAS " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP=@ID_COMP " & vbCrLf
sConsulta = sConsulta & "   AND COD_PROVE_CIA=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR @INSTANCIA INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WHERE COD_PROVE_CIA='EUS' AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA INNER JOIN COPIA_CAMPO CC ON CLDA.CIA=CC.CIA AND CLDA.CAMPO_PADRE=CC.ID " & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE FROM COPIA_LINEA_DESGLOSE CLD INNER JOIN COPIA_CAMPO CC ON CLD.CIA=CC.CIA AND CLD.CAMPO_PADRE=CC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA INNER JOIN COPIA_CAMPO CC ON CCA.CIA=CC.CIA AND CCA.CAMPO=CC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO WHERE INSTANCIA=@INSTANCIA AND CIA=@ID_CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_RESPUESTAS_SIN_ENVIAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_RESPUESTAS_SIN_ENVIAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_RESPUESTAS_SIN_ENVIAR @INSTANCIA INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WHERE COD_PROVE_CIA='EUS' AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT * FROM COPIA_CAMPO WHERE INSTANCIA=@INSTANCIA AND CIA=@ID_CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_DEVOLVER_SOLICITYAPROB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_DEVOLVER_SOLICITYAPROB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_DEVOLVER_SOLICITYAPROB @CIA INT,  @INSTANCIA INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_DEVOLVER_SOLICITYAPROB @INSTANCIA = @INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT = NULL ',@INSTANCIA =@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ENVIAR_CERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ENVIAR_CERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ENVIAR_CERTIFICADO @CIA INT,  @CIAPROVE INT,  @CERTIFICADO INT, @INSTANCIA INT, @VERSION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_ENVIAR_CERTIFICADO  @PROVE =@PROVE  ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @CERTIFICADO INT, @INSTANCIA INT, @VERSION INT ', @PROVE =@PROVEGS ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADCERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADCERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_LOADCERTIFICADO @CIA INT,@PROVE VARCHAR(50),  @CERTIFICADO INT = NULL , @PORTAL TINYINT =NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADCERTIFICADO @CERTIFICADO =@CERTIFICADO, @INSTANCIA=@INSTANCIA OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT =NULL,  @INSTANCIA INT = NULL OUTPUT',@CERTIFICADO =@CERTIFICADO,  @INSTANCIA=@INSTANCIA OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT * FROM COPIA_CAMPO WHERE INSTANCIA =@INSTANCIA AND CIA = @IDPROVE)" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADCORRESPONDENCIACAMPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADCORRESPONDENCIACAMPOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_LOADCORRESPONDENCIACAMPOS @CIA INT, @CERTIFICADO INT , @VERSION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADCORRESPONDENCIACAMPOS @CERTIFICADO =@CERTIFICADO , @VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @CERTIFICADO INT , @VERSION INT', @CERTIFICADO =@CERTIFICADO , @VERSION =@VERSION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADRESPUESTASCERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADRESPUESTASCERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_LOADRESPUESTASCERTIFICADO @CIA INT,@PROVE VARCHAR(50),  @CERTIFICADO INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADRESPUESTASCERTIFICADO @PROVE=@PROVE, @CERTIFICADO =@CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT =NULL, @PROVE VARCHAR(50)',@CERTIFICADO =@CERTIFICADO, @PROVE=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VALIDARCERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VALIDARCERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_VALIDARCERTIFICADO @CIA INT, @CERTIFICADO INT , @VERSION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_VALIDARCERTIFICADO @CERTIFICADO =@CERTIFICADO , @VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @CERTIFICADO INT , @VERSION INT', @CERTIFICADO =@CERTIFICADO , @VERSION =@VERSION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PERSONAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PERSONAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PERSONAS @CIA INT, @COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PERSONAS @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL ', @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLUCION_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLUCION_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLUCION_INSTANCIA @CIA INT,  @CIAPROVE INT, @INSTANCIA INT,@PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT ', @INSTANCIA=@INSTANCIA, @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELDS_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETFIELDS_INSTANCIA @CIA INT,@CIAPROVE INT,  @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL ,@PROVE VARCHAR(50)=NULL ,@RECENV TINYINT = 1 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECENV = 1 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT * FROM COPIA_CAMPO WHERE INSTANCIA = -1000 AND CIA = -1000" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT C.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE FROM COPIA_CAMPO_ADJUN A INNER JOIN COPIA_CAMPO C ON A.CAMPO = C.ID " & vbCrLf
sConsulta = sConsulta & "    WHERE C.INSTANCIA=-1000 AND  C.CIA = -1000 AND C.ES_SUBCAMPO=0 ORDER BY A.CAMPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT * FROM COPIA_CAMPO WHERE INSTANCIA = @ID AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT C.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE FROM COPIA_CAMPO_ADJUN A INNER JOIN COPIA_CAMPO C ON A.CAMPO = C.ID " & vbCrLf
sConsulta = sConsulta & "    WHERE C.INSTANCIA=@ID AND  C.CIA = @CIAPROVE AND C.ES_SUBCAMPO=0 ORDER BY A.CAMPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PROVE @CIA INT, @PROVE VARCHAR(50), @PORTAL TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 1 " & vbCrLf
sConsulta = sConsulta & "  SELECT @PROVEGS = COD_PROVE_CIA   FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @PROVEGS = @PROVE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVE @PROVE =@PROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50) ', @PROVE =@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Function V_3_0_Tablas_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_COPIA_CAMPO_ADJUN_COPIA_CAMPO]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_CAMPO_ADJUN] DROP CONSTRAINT FK_COPIA_CAMPO_ADJUN_COPIA_CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_COPIA_LINEA_DESGLOSE_ADJUN_COPIA_LINEA_DESGLOSE]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[COPIA_LINEA_DESGLOSE_ADJUN] DROP CONSTRAINT FK_COPIA_LINEA_DESGLOSE_ADJUN_COPIA_LINEA_DESGLOSE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_CAMPO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_CAMPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_CAMPO_ADJUN]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_CAMPO_ADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_LINEA_DESGLOSE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_LINEA_DESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COPIA_LINEA_DESGLOSE_ADJUN]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[COPIA_LINEA_DESGLOSE_ADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[COPIA_CAMPO] (" & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [INSTANCIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [nvarchar] (4000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ES_SUBCAMPO] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[COPIA_CAMPO_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_GS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [nvarchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (20) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT] [nvarchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECALTA] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [RUTA] [nvarchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN_PORTAL] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[COPIA_LINEA_DESGLOSE] (" & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_PADRE] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_HIJO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [nvarchar] (4000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA_OLD] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[COPIA_LINEA_DESGLOSE_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_GS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_PADRE] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_HIJO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [nvarchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (20) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT] [nvarchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECALTA] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [RUTA] [nvarchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN_PORTAL] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[COPIA_CAMPO] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_CAMPO] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[COPIA_CAMPO] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_COPIA_CAMPO_ES_SUBCAMPO] DEFAULT (0) FOR [ES_SUBCAMPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[COPIA_CAMPO_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_CAMPO_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[COPIA_CAMPO_ADJUN] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_COPIA_CAMPO_ADJUN_COPIA_CAMPO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[COPIA_CAMPO] (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[COPIA_LINEA_DESGLOSE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_LINEA_DESGLOSE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [LINEA]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_PADRE]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_HIJO]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[COPIA_LINEA_DESGLOSE_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_COPIA_LINEA_DESGLOSE_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[COPIA_LINEA_DESGLOSE_ADJUN] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_COPIA_LINEA_DESGLOSE_ADJUN_COPIA_LINEA_DESGLOSE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [LINEA]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_PADRE]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_HIJO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[COPIA_LINEA_DESGLOSE] (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [LINEA]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_PADRE]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_HIJO]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[REL_CIAS] " & vbCrLf
sConsulta = sConsulta & " ADD " & vbCrLf
sConsulta = sConsulta & "   [CERT_ACT] [int] NOT NULL CONSTRAINT DF_REL_CIAS_CERT_ACT DEFAULT 0," & vbCrLf
sConsulta = sConsulta & "   [CERT_NUE] [int] NOT NULL CONSTRAINT DF_REL_CIAS_CERT_NUE DEFAULT 0 " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Public Function CodigoDeActualizacion3_00_00_02A3_00_00_03()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   V_3_0_Storeds_3
   
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_02A3_00_00_03 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_02A3_00_00_03 = False
End Function

Private Sub V_3_0_Storeds_3()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_FORMATO_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_FORMATO_CONTACTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_FORMATO_CONTACTO @CON_PORTAL INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50),@DATEFMT VARCHAR(50) OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WHERE COD_PROVE_CIA=@PROVE AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @DATEFMT=(SELECT DATEFMT FROM USU WHERE CIA=@ID_CIA AND ID=@CON_PORTAL)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_RESPUESTAS_SIN_ENVIAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_RESPUESTAS_SIN_ENVIAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_RESPUESTAS_SIN_ENVIAR @INSTANCIA INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WHERE COD_PROVE_CIA=@PROVE AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT * FROM COPIA_CAMPO WHERE INSTANCIA=@INSTANCIA AND CIA=@ID_CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR @INSTANCIA INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WHERE COD_PROVE_CIA=@PROVE AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA INNER JOIN COPIA_CAMPO CC ON CLDA.CIA=CC.CIA AND CLDA.CAMPO_PADRE=CC.ID " & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE FROM COPIA_LINEA_DESGLOSE CLD INNER JOIN COPIA_CAMPO CC ON CLD.CIA=CC.CIA AND CLD.CAMPO_PADRE=CC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA INNER JOIN COPIA_CAMPO CC ON CCA.CIA=CC.CIA AND CCA.CAMPO=CC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO WHERE INSTANCIA=@INSTANCIA AND CIA=@ID_CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PROVIS @CIA INT,@PAI VARCHAR(50),@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @USALIKE TINYINT =null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVIS @PAI=@PAI,@COD =@COD , @DEN =@DEN ,@USALIKE  =@USALIKE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PAI VARCHAR(50),@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT', @PAI=@PAI,@COD =@COD , @DEN =@DEN , @USALIKE  =@USALIKE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion3_00_00_03A3_00_00_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   V_3_0_Storeds_4
   
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_03A3_00_00_04 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_03A3_00_00_04 = False
End Function

Private Sub V_3_0_Storeds_4()
Dim sConsulta As String
 sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL , @PORTAL tinyint = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @PORTAL = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "               SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                 FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                   LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                          ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                         AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                    ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "    IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA, 0 PORTAL" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "       ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                        ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT  3 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA, 1 PORTAL" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "       ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  3 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                  WHERE A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  4 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0)  DATASIZE , A.COMENT, @IDI IDIOMA, A.ID ADJUN, A.CAMPO,  NULL RUTA" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                  WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                      ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_04A3_00_00_05()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   V_3_0_Storeds_5
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_04A3_00_00_05 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_04A3_00_00_05 = False
End Function


Private Sub V_3_0_Storeds_5()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL , @PORTAL tinyint = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @PORTAL = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =null , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "               SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                 FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                   LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                          ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                         AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                    ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "    IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA, 0 PORTAL" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "       ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                        ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT  3 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA, 1 PORTAL" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "       ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  3 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                  WHERE A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  4 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0)  DATASIZE , A.COMENT, @IDI IDIOMA, A.ID ADJUN, A.CAMPO,  NULL RUTA" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                  WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                      ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_05A3_00_00_06()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   V_3_0_Storeds_6
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_05A3_00_00_06 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_05A3_00_00_06 = False
End Function

Public Function CodigoDeActualizacion3_00_00_06A3_00_00_07()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   'est� bien aunque ponga 6 lleva tema del 7
   V_3_0_Storeds_6
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_06A3_00_00_07 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_06A3_00_00_07 = False
End Function


Private Sub V_3_0_Storeds_6()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_ARTICULOS @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA' AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50)',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTDESGLOSE @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL , @VERSION INT AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50), @VERSION INT ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS, @VERSION = @VERSION " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT  * FROM COPIA_CAMPO WHERE ID = @COPIA_CAMPO)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD  WHERE LD.CAMPO_PADRE=@COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  ' ' NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN LDA " & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_PADRE = @COPIA_CAMPO ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub



Public Function CodigoDeActualizacion3_00_00_07A3_00_00_08()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   
   V_3_0_Storeds_8
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_07A3_00_00_08 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_07A3_00_00_08 = False
End Function


Private Sub V_3_0_Storeds_8()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNDATA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTADJUNDATA @CIA INT=NULL, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0, @PORTAL TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @TIPO=1 OR @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN  WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "          SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "          SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "        READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SELECT @IDGS = ID_GS FROM COPIA_CAMPO_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "          IF @IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA INNER JOIN COPIA_CAMPO_ADJUN  CCA ON CA.ID = CCA.ADJUN_PORTAL WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "              if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "              ELSE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "              READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "              EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@IDGS, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        IF @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @IDGS = ID_GS FROM COPIA_LINEA_DESGLOSE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "            IF @IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN  CCA ON CA.ID = CCA.ADJUN_PORTAL WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "                if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "                  SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "                ELSE " & vbCrLf
sConsulta = sConsulta & "                  SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "                READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "                EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@IDGS, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "            if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "              SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "            READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion3_00_00_08A3_00_00_09()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   
   V_3_0_Storeds_9
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_08A3_00_00_09 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_08A3_00_00_09 = False
End Function

Private Sub V_3_0_Storeds_9()
Dim sConsulta As String

 sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_LOADINSTFIELDSCALC @CIA INT, @INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)='SPA', @PROVE varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSCALC @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50),@PROVE varchar(50)', @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_LOADINSTFIELDSDESGLOSECALC @CIA INT, @CAMPO INT , @INSTANCIA INT = NULL ,  @PROVE VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSDESGLOSECALC @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT,@INSTANCIA INT = NULL ,  @PROVE VARCHAR(50) = NULL', @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion3_00_00_09A3_00_00_10()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   
   V_3_0_Storeds_10
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_09A3_00_00_10 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_09A3_00_00_10 = False
End Function

Private Sub V_3_0_Storeds_10()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                        WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion3_00_00_10A3_00_00_11()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   
   V_3_0_Storeds_11
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_10A3_00_00_11 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_10A3_00_00_11 = False
End Function


Private Sub V_3_0_Storeds_11()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ESTADOS_NOCONF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ESTADOS_NOCONF]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADESTADOSINSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADESTADOSINSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ENVIAR_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ENVIAR_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADNOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADNOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADRESPUESTASNOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADRESPUESTASNOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NOCONF_ACC]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[NOCONF_ACC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NOCONF_ACC]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[NOCONF_ACC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[NOCONF_ACC] (" & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO_PADRE] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOCONFORMIDAD] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [INSTANCIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VERSION] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ESTADO] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   [ESTADO_INT] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[NOCONF_ACC] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_NOCONF_ACC] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [CAMPO_PADRE]," & vbCrLf
sConsulta = sConsulta & "       [LINEA]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "alter TABLE [dbo].[REL_CIAS] add" & vbCrLf
sConsulta = sConsulta & "   [NOCONF_ACT] [int] NOT NULL CONSTRAINT [DF_REL_CIAS_NOCONF_ACT] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   [NOCONF_NUE] [int] NOT NULL CONSTRAINT [DF_REL_CIAS_NOCONF_NUE] DEFAULT (0) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_ESTADOS_NOCONF @CIA INT,@INSTANCIA INT , @COD VARCHAR(50)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ESTADOS_NOCONF @INSTANCIA = @INSTANCIA, @COD =@COD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT , @COD VARCHAR(50)=null ',  @INSTANCIA =  @INSTANCIA , @COD =@COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_GETINSTINVISIBLEFIELDS @CIA INT,  @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL    AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL ',  @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_LOADESTADOSINSTANCIA @CIA INT,@INSTANCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADESTADOSINSTANCIA @INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT ', @INSTANCIA=@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD(@COMP VARCHAR(50),@PROVE VARCHAR(50),@NUEVAS INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP AS INT" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET NOCONF_NUE=@NUEVAS " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP=@ID_COMP " & vbCrLf
sConsulta = sConsulta & "   AND COD_PROVE_CIA=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSQA_ENVIAR_NOCONFORMIDAD  @CIA INT,@CIAPROVE INT, @NOCONFORMIDAD INT, @INSTANCIA INT, @VERSION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_ENVIAR_NOCONFORMIDAD  @PROVE =@PROVE  ,@NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @NOCONFORMIDAD INT, @INSTANCIA INT, @VERSION INT ', @PROVE =@PROVEGS ,@NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM NOCONF_ACC" & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSQA_LOADNOCONFORMIDAD @CIA INT,@PROVE VARCHAR(50),  @NOCONFORMIDAD INT = NULL , @PORTAL TINYINT =NULL OUTPUT , @IDI VARCHAR(3), @VERSION INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADNOCONFORMIDAD @NOCONFORMIDAD =@NOCONFORMIDAD, @INSTANCIA=@INSTANCIA OUTPUT, @IDI = @IDI , @VERSION = @VERSION'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NOCONFORMIDAD INT =NULL,  @INSTANCIA INT = NULL OUTPUT,@IDI VARCHAR(3), @VERSION INT',@NOCONFORMIDAD =@NOCONFORMIDAD,  @INSTANCIA=@INSTANCIA OUTPUT, @IDI = @IDI, @VERSION = @VERSION " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT * FROM COPIA_CAMPO WHERE INSTANCIA =@INSTANCIA AND CIA = @IDPROVE)" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT CC.GRUPO, NA.*" & vbCrLf
sConsulta = sConsulta & "  FROM NOCONF_ACC NA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO CC ON NA.CAMPO_PADRE = CC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE NA.NOCONFORMIDAD = @NOCONFORMIDAD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSQA_LOADRESPUESTASNOCONFORMIDAD @CIA INT,@PROVE VARCHAR(50),  @NOCONFORMIDAD INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADRESPUESTASNOCONFORMIDAD @PROVE=@PROVE, @NOCONFORMIDAD =@NOCONFORMIDAD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NOCONFORMIDAD INT =NULL, @PROVE VARCHAR(50)',@NOCONFORMIDAD =@NOCONFORMIDAD, @PROVE=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR @INSTANCIA INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WHERE COD_PROVE_CIA=@PROVE AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA INNER JOIN COPIA_CAMPO CC ON CLDA.CIA=CC.CIA AND CLDA.CAMPO_PADRE=CC.ID " & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE FROM COPIA_LINEA_DESGLOSE CLD INNER JOIN COPIA_CAMPO CC ON CLD.CIA=CC.CIA AND CLD.CAMPO_PADRE=CC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA INNER JOIN COPIA_CAMPO CC ON CCA.CIA=CC.CIA AND CCA.CAMPO=CC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO WHERE INSTANCIA=@INSTANCIA AND CIA=@ID_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM NOCONF_ACC FROM NOCONF_ACC NC INNER JOIN COPIA_CAMPO CC ON NC.CIA=CC.CIA AND NC.CAMPO_PADRE=CC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CC.INSTANCIA=@INSTANCIA AND CC.CIA=@ID_CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub


Public Function CodigoDeActualizacion3_00_00_11A3_00_00_12()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   
   V_3_0_Storeds_12
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.12'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_11A3_00_00_12 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_11A3_00_00_12 = False
End Function

Private Sub V_3_0_Storeds_12()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UONS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UONS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSPM_UONS @CIA INT,@TIPO INT, @PER VARCHAR(50) = NULL, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UONS @TIPO=@TIPO, @PER = @PER , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT,@PER VARCHAR(50), @IDI VARCHAR(50) ', @TIPO=@TIPO, @PER = @PER , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ENVIAR_CERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ENVIAR_CERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ENVIAR_CERTIFICADO @CIA INT,  @CIAPROVE INT,  @CERTIFICADO INT, @INSTANCIA INT, @VERSION INT, @USUNOM AS NVARCHAR(500) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_ENVIAR_CERTIFICADO  @PROVE =@PROVE  ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION, @USUNOM=@USUNOM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @CERTIFICADO INT, @INSTANCIA INT, @VERSION INT,@USUNOM NVARCHAR(500) ', @PROVE =@PROVEGS ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION, @USUNOM=@USUNOM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ENVIAR_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ENVIAR_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ENVIAR_NOCONFORMIDAD  @CIA INT,@CIAPROVE INT, @NOCONFORMIDAD INT, @INSTANCIA INT, @VERSION INT, @USUNOM AS NVARCHAR(500) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_ENVIAR_NOCONFORMIDAD  @PROVE =@PROVE  ,@NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION, @USUNOM=@USUNOM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @NOCONFORMIDAD INT, @INSTANCIA INT, @VERSION INT,@USUNOM NVARCHAR(500) ', @PROVE =@PROVEGS ,@NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION, @USUNOM=@USUNOM " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM NOCONF_ACC" & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_SAVE_PUNTUACION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_SAVE_PUNTUACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_SAVE_PUNTUACION @CIA INT,  @PROVE varchar(50),  @VARCAL INT=0, @NIVEL SMALLINT=0, @FECHA DATETIME=NULL,@PUNT FLOAT=NULL,@CAL INT=NULL,@ID_FORMULA INT=NULL, @USUNOM AS NVARCHAR(500)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_UPDATE_PROVE_PUNT  @PROVE =@PROVE  ,@VARCAL=@VARCAL ,@NIVEL =@NIVEL ,@FECHA=@FECHA, @PUNT=@PUNT,@CAL=@CAL,@ID_FORMULA=@ID_FORMULA,@USUNOM=@USUNOM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE AS VARCHAR(50),@VARCAL AS INTEGER, @NIVEL AS SMALLINT, @FECHA AS DATETIME, @PUNT AS FLOAT, @CAL AS INT, @ID_FORMULA AS INT, @USUNOM NVARCHAR(500)  ', @PROVE =@PROVE  ,@VARCAL=@VARCAL ,@NIVEL =@NIVEL ,@FECHA=@FECHA, @PUNT=@PUNT,@CAL=@CAL,@ID_FORMULA=@ID_FORMULA,@USUNOM=@USUNOM " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VARIABLES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VARIABLES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_GET_VARIABLES @CIA INT, @SOLICITUD INT,@NIVEL SMALLINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VARIABLES  @SOLICITUD =@SOLICITUD, @NIVEL =@NIVEL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SOLICITUD INT,@NIVEL SMALLINT ', @SOLICITUD =@SOLICITUD, @NIVEL =@NIVEL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VARIABLES_HIJAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VARIABLES_HIJAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_GET_VARIABLES_HIJAS @CIA INT, @PROVE VARCHAR(50),@VARCAL INTEGER, @NIVEL SMALLINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VARIABLES_HIJAS  @PROVE =@PROVE, @VARCAL=@VARCAL, @NIVEL =@NIVEL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@VARCAL INTEGER, @NIVEL SMALLINT ',  @PROVE =@PROVE, @VARCAL =@VARCAL, @NIVEL =@NIVEL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VALIDAR_VARIABLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VALIDAR_VARIABLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_VALIDAR_VARIABLE @CIA INT, @PROVE VARCHAR(50),@VARCAL INT,@NIVEL SMALLINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_VALIDAR_VARIABLE  @PROVE =@PROVE, @VARCAL=@VARCAL, @NIVEL =@NIVEL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@VARCAL INT, @NIVEL SMALLINT ',  @PROVE =@PROVE, @VARCAL =@VARCAL, @NIVEL =@NIVEL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VARCERT_POND]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VARCERT_POND]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_GET_VARCERT_POND @CIA INT, @VARCAL AS INT,@NIVEL AS SMALLINT, @PROVE AS VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VARCERT_POND @VARCAL=@VARCAL, @NIVEL =@NIVEL, @PROVE =@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@VARCAL AS INT,@NIVEL AS SMALLINT, @PROVE AS VARCHAR(50) ',   @VARCAL =@VARCAL, @NIVEL =@NIVEL,@PROVE =@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VARNOCONF_POND]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VARNOCONF_POND]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_GET_VARNOCONF_POND @CIA INT, @VARCAL AS INT,@NIVEL AS SMALLINT, @PROVE AS VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VARNOCONF_POND @VARCAL=@VARCAL, @NIVEL =@NIVEL, @PROVE =@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@VARCAL AS INT,@NIVEL AS SMALLINT, @PROVE AS VARCHAR(50) ',   @VARCAL =@VARCAL, @NIVEL =@NIVEL,@PROVE =@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_CALIFICATION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_CALIFICATION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_GET_CALIFICATION @CIA INT, @VARCAL AS INT,@NIVEL AS SMALLINT, @VALUE AS FLOAT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CALIFICATION @VARCAL=@VARCAL, @NIVEL =@NIVEL, @VALUE =@VALUE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@VARCAL AS INT,@NIVEL AS SMALLINT, @VALUE AS FLOAT ',   @VARCAL =@VARCAL, @NIVEL =@NIVEL,@VALUE =@VALUE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_12A3_00_00_13()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'On Error GoTo 0
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   
   V_3_0_Storeds_13
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.13'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_12A3_00_00_13 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_12A3_00_00_13 = False
End Function

Private Sub V_3_0_Storeds_13()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_FORMATO_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_FORMATO_CONTACTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSQA_FORMATO_CONTACTO @CON_PORTAL INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50),@DATEFMT VARCHAR(50) OUTPUT, @THOUSANFMT CHAR(1) OUTPUT, @DECIMALFMT CHAR(1) OUTPUT, @PRECISIONFMT TINYINT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WHERE COD_PROVE_CIA=@PROVE AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET @DATEFMT=(SELECT DATEFMT FROM USU WHERE CIA=@ID_CIA AND ID=@CON_PORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT @DATEFMT=DATEFMT, @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT FROM USU WHERE CIA=@ID_CIA AND ID=@CON_PORTAL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Public Function CodigoDeActualizacion3_00_00_13A3_00_00_14()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en los storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   
   V_3_0_Storeds_14
   

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.14'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_13A3_00_00_14 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_13A3_00_00_14 = False
End Function
Private Sub V_3_0_Storeds_14()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_FORMATO_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_FORMATO_CONTACTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSQA_FORMATO_CONTACTO @CON_PORTAL INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50),@DATEFMT VARCHAR(50) OUTPUT, @THOUSANFMT CHAR(1) =NULL OUTPUT, @DECIMALFMT CHAR(1)=NULL OUTPUT, @PRECISIONFMT TINYINT =NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WHERE COD_PROVE_CIA=@PROVE AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET @DATEFMT=(SELECT DATEFMT FROM USU WHERE CIA=@ID_CIA AND ID=@CON_PORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT @DATEFMT=DATEFMT, @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT FROM USU WHERE CIA=@ID_CIA AND ID=@CON_PORTAL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_14A3_00_00_15()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'''Version 31200
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
   sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[PARGEN_INTERNO]') and name=N'IMPRIMIR_POLITICA_PEDIDOS')" & vbCrLf
   sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_INTERNO] " & vbCrLf
   sConsulta = sConsulta & " ADD " & vbCrLf
   sConsulta = sConsulta & "   [IMPRIMIR_POLITICA_PEDIDOS] [tinyint] NOT NULL CONSTRAINT DF_PARGEN_INTERNO_IMPRIMIR_POLITICA_PEDIDOS DEFAULT 0" & vbCrLf
   ExecuteSQL gRDOCon, sConsulta
   
    frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

    V_3_0_Storeds_15
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.15'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_14A3_00_00_15 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_14A3_00_00_15 = False
End Function


Private Sub V_3_0_Storeds_15()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VER_PUNTUACION_CALIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VER_PUNTUACION_CALIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_VER_PUNTUACION_CALIDAD @CODPROVE NVARCHAR(100),@CODCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CODCIA  AND PORT =0  --las tablas de calidad est�n en el GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_VER_PUNTUACION_CALIDAD  @PROVE=@CODPROVE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--PRINT @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CODPROVE varchar(50) ', @CODPROVE=@CODPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion3_00_00_15A3_00_00_16()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'''Version 31600
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   
    V_3_0_Tablas_16

   
    frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

    V_3_0_Storeds_16
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.16'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_15A3_00_00_16 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_15A3_00_00_16 = False
End Function

Private Function V_3_0_Tablas_16()
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PARTICIPANTES] (" & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [INSTANCIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ROL] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVE] [varchar] (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   [CON] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [TIPO] [smallint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PARTICIPANTES] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PARTICIPANTES] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [INSTANCIA]," & vbCrLf
sConsulta = sConsulta & "       [ROL]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Private Sub V_3_0_Storeds_16()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_COMENT_ESTADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_COMENT_ESTADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_COMENT_ESTADO @CIA INT,@ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DEVOLVER_COMENT_ESTADO @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_CONTACTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_CONTACTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_CONTACTOS @CIA INT,@PROVE VARCHAR(50), @SOLOPORTAL TINYINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_DEVOLVER_CONTACTOS_QA @PROVE=@PROVE,@SOLOPORTAL=@SOLOPORTAL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE  VARCHAR(50), @SOLOPORTAL TINYINT ', @PROVE=@PROVE,@SOLOPORTAL=@SOLOPORTAL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_DIAGBLOQUES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_DIAGBLOQUES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_DIAGBLOQUES @CIA INT,@INSTANCIA INT, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_DIAGBLOQUES @INSTANCIA=@INSTANCIA,@IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDIOMA VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_DIAGENLACES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_DIAGENLACES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_DIAGENLACES @CIA INT,@BLOQUE INT, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_DIAGENLACES @BLOQUE=@BLOQUE,@IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @IDIOMA VARCHAR(50) ', @BLOQUE=@BLOQUE,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_ETAPAS_BLOQUEO @CIA INT,@BLOQUE INT , @IDI VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPAS_BLOQUEO @BLOQUE=@BLOQUE,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @IDI VARCHAR(50) ', @BLOQUE=@BLOQUE,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_ETAPA_ACTUAL @CIA INT,@INSTANCIA INT , @IDI VARCHAR(50) , @PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPA_ACTUAL @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50),@PROVE VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_HISTORICO_INSTANCIA @CIA INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_HISTORICO_INSTANCIA @INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETWORKFLOW_PROVE @CIA INT,@PROVE VARCHAR(50), @IDI VARCHAR(50), @TIPO INT =0 , @FECHA  DATETIME=NULL, @INSTANCIA INT=0, @FILAESTADO INT =0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETWORKFLOW_PROVE @PROVE=@PROVE,@IDI=@IDI,@TIPO=@TIPO,@FECHA=@FECHA,@INSTANCIA=@INSTANCIA,@FILAESTADO=@FILAESTADO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @IDI VARCHAR(50) , @TIPO INT,@FECHA  DATETIME,@INSTANCIA INT, @FILAESTADO INT', @PROVE=@PROVEGS,@IDI=@IDI, @TIPO=@TIPO,@FECHA=@FECHA,@INSTANCIA=@INSTANCIA,@FILAESTADO=@FILAESTADO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADSOLICITUDES_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADSOLICITUDES_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADSOLICITUDES_PROVE @CIA INT,@PROVE VARCHAR(50), @TIPO INT =0, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADSOLICITUDES_PROVE @PROVE=@PROVE,@TIPO=@TIPO,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @TIPO INT, @IDI VARCHAR(50) ', @PROVE=@PROVEGS, @TIPO=@TIPO,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOAD_ACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOAD_ACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOAD_ACCION @CIA INT,@ACCION INT , @IDI VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOAD_ACCION @ACCION=@ACCION,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ACCION INT, @IDI VARCHAR(50) ', @ACCION=@ACCION,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOAD_ETAPAS_REALIZADAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOAD_ETAPAS_REALIZADAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOAD_ETAPAS_REALIZADAS @CIA INT,@ACCION INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOAD_ETAPAS_REALIZADAS @ACCION=@ACCION,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ACCION INT, @IDI VARCHAR(50) ', @ACCION=@ACCION,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PARTICIPANTES_PER]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PARTICIPANTES_PER]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PARTICIPANTES_PER @CIA INT,@ROL INT , @INSTANCIA INT, @FILNOM VARCHAR(500), @FILAPE VARCHAR(500), @FILUON1 VARCHAR(50), @FILUON2 VARCHAR(50), @FILUON3 VARCHAR(50) , @FILDEP VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PARTICIPANTES_PER @ROL=@ROL,@INSTANCIA=@INSTANCIA,@FILNOM=@FILNOM,@FILAPE=@FILAPE,@FILUON1=@FILUON1,@FILUON2=@FILUON2,@FILUON3=@FILUON3,@FILDEP=@FILDEP'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ROL INT ,@INSTANCIA INT @FILNOM VARCHAR(500), @FILAPE VARCHAR(500), @FILUON1 VARCHAR(50), @FILUON2 VARCHAR(50), @FILUON3 VARCHAR(50) , @FILDEP VARCHAR(50) ', @ROL=@ROL,@INSTANCIA=@INSTANCIA,@FILNOM=@FILNOM,@FILAPE=@FILAPE,@FILUON1=@FILUON1,@FILUON2=@FILUON2,@FILUON3=@FILUON3,@FILDEP=@FILDEP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PARTICIPANTES_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PARTICIPANTES_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PARTICIPANTES_PROVE @CIA INT,@ROL INT , @USU VARCHAR(50), @INSTANCIA INT, @COD VARCHAR(50), @DEN VARCHAR(500), @NIF VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PARTICIPANTES_PROVE @ROL=@ROL,@USU=@USU,@INSTANCIA=@INSTANCIA,@COD=@COD,@DEN=@DEN,@NIF=@NIF'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ROL INT , @USU VARCHAR(50), @INSTANCIA INT, @COD VARCHAR(50), @DEN VARCHAR(500), @NIF VARCHAR(50) ', @ROL=@ROL,@USU=@USU,@INSTANCIA=@INSTANCIA,@COD=@COD,@DEN=@DEN,@NIF=@NIF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_REALIZAR_ACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_REALIZAR_ACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_REALIZAR_ACCION @CIA INT,  @CIAPROVE INT, @INSTANCIA INT,@PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL ,@DEVOLUCION TINYINT=0, @ETAPA INT , @USUNOM VARCHAR(500)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE, @DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT,@DEVOLUCION TINYINT,@ETAPA INT , @USUNOM VARCHAR(500) ', @INSTANCIA=@INSTANCIA, @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE,@DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ROL_PARTICIPANTES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ROL_PARTICIPANTES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ROL_PARTICIPANTES @CIA INT,@BLOQUE INT,@ROL INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ROL_PARTICIPANTES @BLOQUE=@BLOQUE,@ROL=@ROL,@INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @ROL INT, @INSTANCIA INT, @IDI VARCHAR(50) ',@BLOQUE=@BLOQUE,@ROL=@ROL,@INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELDS_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETFIELDS_INSTANCIA @CIA INT,@CIAPROVE INT,  @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL ,@PROVE VARCHAR(50)=NULL ,@RECENV TINYINT = 1, @NUEVO_WORKFLOW TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA_31600 @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECENV = 1 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT * FROM COPIA_CAMPO WHERE INSTANCIA = -1000 AND CIA = -1000" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT C.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE FROM COPIA_CAMPO_ADJUN A INNER JOIN COPIA_CAMPO C ON A.CAMPO = C.ID " & vbCrLf
sConsulta = sConsulta & "    WHERE C.INSTANCIA=-1000 AND  C.CIA = -1000 AND C.ES_SUBCAMPO=0 ORDER BY A.CAMPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT * FROM COPIA_CAMPO WHERE INSTANCIA = @ID AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT C.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE FROM COPIA_CAMPO_ADJUN A INNER JOIN COPIA_CAMPO C ON A.CAMPO = C.ID " & vbCrLf
sConsulta = sConsulta & "    WHERE C.INSTANCIA=@ID AND  C.CIA = @CIAPROVE AND C.ES_SUBCAMPO=0 ORDER BY A.CAMPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_UO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_UO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_UO @CIA INT,@IDI VARCHAR(50) ,@USU VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DEVOLVER_UO @IDI=@IDI, @USU=@USU'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50) ,@USU VARCHAR(50)', @IDI=@IDI, @USU=@USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETDEPARTAMENTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETDEPARTAMENTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETDEPARTAMENTOS @CIA INT,@NIVEL INT , @UON1 VARCHAR(20) , @UON2 VARCHAR(20), @UON3 VARCHAR(20), @USU VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETDEPARTAMENTOS @NIVEL=@NIVEL,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@USU=@USU'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NIVEL INT , @UON1 VARCHAR(20) , @UON2 VARCHAR(20), @UON3 VARCHAR(20), @USU VARCHAR(20) ', @NIVEL=@NIVEL,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@USU=@USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_16A3_00_00_17()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

'''Version 31600
   
   'On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
       
   frmProgreso.lblDetalle = "Cambios en tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
      
   V_3_0_Tablas_17
   

   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_17
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.17'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_16A3_00_00_17 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_16A3_00_00_17 = False
End Function

Public Function CodigoDeActualizacion3_00_00_17A3_00_00_18()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
          

   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_18
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.18'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_17A3_00_00_18 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_17A3_00_00_18 = False
End Function

Public Sub V_3_0_Storeds_17()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTINVISIBLEFIELDS @CIA INT,  @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL,@NUEVO_WORKFL TINYINT=0    AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL ',  @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_PRECONDICIONES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_PRECONDICIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE PROCEDURE FSPM_GET_PRECONDICIONES @CIA INT, @BSOLICITUD BIT, @IDACC INTEGER, @IDI VARCHAR(3) = 'SPA', @INSTANCIA INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_PRECONDICIONES @BSOLICITUD=@BSOLICITUD,@IDACC=@IDACC,@IDI=@IDI,@INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BSOLICITUD BIT, @IDACC INTEGER, @IDI VARCHAR(3), @INSTANCIA INT ', @BSOLICITUD=@BSOLICITUD,@IDACC=@IDACC,@IDI=@IDI,@INSTANCIA=@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADINSTFIELDSCALC @CIA INT, @INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)='SPA', @PROVE varchar(50)=null, @NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTFIELDSCALC @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSCALC @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50),@PROVE varchar(50)', @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADINSTFIELDSDESGLOSECALC @CIA INT, @CAMPO INT , @INSTANCIA INT = NULL ,  @PROVE VARCHAR(50) = NULL,@NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTFIELDSDESGLOSECALC @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSDESGLOSECALC @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT,@INSTANCIA INT = NULL ,  @PROVE VARCHAR(50) = NULL', @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PARTICIPANTES_PER]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PARTICIPANTES_PER]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PARTICIPANTES_PER @CIA INT,@ROL INT , @INSTANCIA INT, @FILNOM VARCHAR(500)=NULL, @FILAPE VARCHAR(500)=NULL, @FILUON1 VARCHAR(50)=NULL, @FILUON2 VARCHAR(50)=NULL, @FILUON3 VARCHAR(50)=NULL , @FILDEP VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PARTICIPANTES_PER @ROL=@ROL,@INSTANCIA=@INSTANCIA,@FILNOM=@FILNOM,@FILAPE=@FILAPE,@FILUON1=@FILUON1,@FILUON2=@FILUON2,@FILUON3=@FILUON3,@FILDEP=@FILDEP'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ROL INT ,@INSTANCIA INT ,@FILNOM VARCHAR(500), @FILAPE VARCHAR(500), @FILUON1 VARCHAR(50), @FILUON2 VARCHAR(50), @FILUON3 VARCHAR(50) , @FILDEP VARCHAR(50) ', @ROL=@ROL,@INSTANCIA=@INSTANCIA,@FILNOM=@FILNOM,@FILAPE=@FILAPE,@FILUON1=@FILUON1,@FILUON2=@FILUON2,@FILUON3=@FILUON3,@FILDEP=@FILDEP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Sub V_3_0_Storeds_18()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_ETAPA_ACTUAL @CIA INT,@INSTANCIA INT , @IDI VARCHAR(50) , @PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE  @PROVEGS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPA_ACTUAL @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50),@PROVE VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PROVE=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PROVES @CIA INT, @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS AS VARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @COD " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVES @COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL ',@COD=@PROVEGS, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SOLICITUD_DATOS_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SOLICITUD_DATOS_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_SOLICITUD_DATOS_EMAIL @CIA INT, @ID INT,@TYPE INT,@CONTACTO INT=NULL,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_SOLICITUD_DATOS_EMAIL @ID=@ID, @TYPE=@TYPE, @CONTACTO=@CONTACTO, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT,@TYPE INT,@CONTACTO INT=NULL,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0 ',@ID=@ID, @TYPE=@TYPE, @CONTACTO=@CONTACTO, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SOLICITUD_DATOS_MENSAJE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SOLICITUD_DATOS_MENSAJE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_SOLICITUD_DATOS_MENSAJE @CIA INT,@CIA_PROVE INT, @ID INT,@TYPE INT,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0,@COD_USU VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =  N'EXEC ' + @FSGS + 'FSPM_SOLICITUD_DATOS_MENSAJE @ID=@ID, @TYPE=@TYPE, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT,@TYPE INT,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0 ',@ID=@ID, @TYPE=@TYPE, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtener los datos del Usuario de la Compa�ia" & vbCrLf
sConsulta = sConsulta & "SELECT U.COD COD_USU,U.NOM NOM_USU,U.APE APE_USU,U.TFNO TFNO1_USU,U.EMAIL EMAIL_USU,U.FAX FAX_USU, C.COD COD_COM,C.DEN DEN_COM,C.NIF NIF_COM" & vbCrLf
sConsulta = sConsulta & "FROM USU U" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS C ON U.CIA = C.ID WHERE C.ID =@CIA_PROVE AND U.COD = @COD_USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ENLACESACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ENLACESACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLVER_ENLACESACCION @CIA INT, @ACCION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'SELECT ENLACE FROM ' +@FSGS +'INSTANCIA_CAMINO WHERE ACCION = @ACCION'" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL, N'@ACCION INT', @ACCION =@ACCION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETWORKFLOW_PROVE @CIA INT,@PROVE VARCHAR(50), @IDI VARCHAR(50), @TIPO INT =0 , @FECHA  VARCHAR(10)=NULL, @INSTANCIA INT=0, @FILAESTADO INT =0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETWORKFLOW_PROVE @PROVE=@PROVE,@IDI=@IDI,@TIPO=@TIPO,@FECHA=@FECHA,@INSTANCIA=@INSTANCIA,@FILAESTADO=@FILAESTADO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @IDI VARCHAR(50) , @TIPO INT,@FECHA  VARCHAR(10),@INSTANCIA INT, @FILAESTADO INT', @PROVE=@PROVEGS,@IDI=@IDI, @TIPO=@TIPO,@FECHA=@FECHA,@INSTANCIA=@INSTANCIA,@FILAESTADO=@FILAESTADO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GMN2 @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN2 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50),  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ACCIONES_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ACCIONES_ROL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ACCIONES_ROL @CIA INT,@BLOQUE INT,@ROL INT, @IDI VARCHAR(50),@COPIA TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ACCIONES_ROL @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @ROL INT, @IDI VARCHAR(50), @COPIA TINYINT ',@BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub
Private Function V_3_0_Tablas_17()
Dim sConsulta As String

sConsulta = "ALTER TABLE [dbo].[PARGEN_INTERNO] " & vbCrLf
sConsulta = sConsulta & " ADD " & vbCrLf
sConsulta = sConsulta & "   [COMPROBAR_COD_CIA] [int] NOT NULL CONSTRAINT DF_PARGEN_INTERNO_COMPRUEBA_COD_CIA DEFAULT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Public Function CodigoDeActualizacion3_00_00_18A3_00_00_19()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
          
   frmProgreso.lblDetalle = "Cambios en Tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Tablas_19

   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_19
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.19'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_18A3_00_00_19 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_18A3_00_00_19 = False
End Function


Private Function V_3_0_Tablas_19()
Dim sConsulta As String

sConsulta = "ALTER TABLE [dbo].[COPIA_CAMPO] " & vbCrLf
sConsulta = sConsulta & " ALTER COLUMN   [INSTANCIA] [int] NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[COPIA_CAMPO] " & vbCrLf
sConsulta = sConsulta & " ALTER COLUMN   [GRUPO] [int] NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[COPIA_CAMPO] " & vbCrLf
sConsulta = sConsulta & " ADD " & vbCrLf
sConsulta = sConsulta & "   [IDCREATE] [int] NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[COPIA_CAMPO_ADJUN] " & vbCrLf
sConsulta = sConsulta & " ALTER COLUMN " & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [int] NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Public Sub V_3_0_Storeds_19()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINVISIBLEFIELDS @CIA INT, @ID INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINVISIBLEFIELDS @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACTUALIZAINSTANCIANULA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACTUALIZAINSTANCIANULA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ACTUALIZAINSTANCIANULA @CIA INT, @INSTANCIA FLOAT, @IDCREATE INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO SET INSTANCIA=@INSTANCIA WHERE CIA=@CIA AND INSTANCIA IS NULL AND IDCREATE=@IDCREATE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETFIELD @CIA INT,  @ID INT, @IDI VARCHAR(20) = NULL, @ATTACH AS TINYINT=0, @SOLICITUD AS INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELD  @ID=@ID, @IDI = @IDI , @ATTACH =@ATTACH, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @ATTACH TINYINT=0, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI , @ATTACH =@ATTACH,@SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETDESGLOSE @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDESGLOSE @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFORM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFORM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETFORM @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFORM @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADFIELDSCALC @CIA INT,@FORMULARIO INT,  @IDI VARCHAR(50)='SPA', @SOLICITUD INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADFIELDSCALC @FORMULARIO=@FORMULARIO, @IDI = @IDI, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@FORMULARIO INT,@IDI VARCHAR(20), @SOLICITUD  INT ', @FORMULARIO=@FORMULARIO, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADFIELDSDESGLOSECALC @CIA INT,@CAMPO INT, @SOLICITUD INT,  @PER VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADFIELDSDESGLOSECALC @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT, @SOLICITUD  INT,@PER VARCHAR(50) ', @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_CREATE_NEW_CERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_CREATE_NEW_CERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_CREATE_NEW_CERTIFICADO @CIA INT" & vbCrLf
sConsulta = sConsulta & ", @TIPO_CERTIF INT, @PROVE VARCHAR(50),  @FEC_DESPUB DATETIME=NULL, @PETICIONARIO VARCHAR(50),@CONTACTO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & ",  @RENOVACION INT=0, @CERT_PUB VARCHAR(4000) ='' OUTPUT, @CERT_WORKFLOW VARCHAR(4000) ='' OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_CREATE_TEMP_NEW_CERTIFICADO @TIPO_CERTIF=@TIPO_CERTIF,@PROVE=@PROVE,@FEC_DESPUB=@FEC_DESPUB,@PETICIONARIO=@PETICIONARIO,@CONTACTO=@CONTACTO, @RENOVACION = @RENOVACION, @CERT_PUB=@CERT_PUB, @CERT_WORKFLOW=@CERT_WORKFLOW '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO_CERTIF INT, @PROVE VARCHAR(50), @FEC_DESPUB DATETIME, @PETICIONARIO VARCHAR(50), @CONTACTO VARCHAR(50), @RENOVACION  INT,@CERT_PUB VARCHAR(4000) OUTPUT,@CERT_WORKFLOW VARCHAR(4000) OUTPUT'" & vbCrLf
sConsulta = sConsulta & ",@TIPO_CERTIF=@TIPO_CERTIF,@PROVE=@PROVE,@FEC_DESPUB=@FEC_DESPUB,@PETICIONARIO=@PETICIONARIO,@CONTACTO=@CONTACTO, @RENOVACION = @RENOVACION, @CERT_PUB=@CERT_PUB, @CERT_WORKFLOW=@CERT_WORKFLOW" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_SOLPENDIENTES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_SOLPENDIENTES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_SOLPENDIENTES @CIACOMP FLOAT, @CIAPROVE FLOAT, @USU  VARCHAR(20), @IDI VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT GETDATE() AS DIA, USU.NOM + ' ' + USU.APE AS NOMBRE, REL_CIAS.* " & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS INNER JOIN USU ON USU.CIA = REL_CIAS.CIA_PROVE AND COD=@USU" & vbCrLf
sConsulta = sConsulta & "WHERE CIA_COMP = @CIACOMP AND CIA_PROVE = @CIAPROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_UPLOAD_ADJUN_GS @CIA INT,   @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOBRA TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @SOBRA=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UPLOAD_ADJUN @INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @SOBRA=@SOBRA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL, @SOBRA TINYINT ',@INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @SOBRA=@SOBRA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_TIPONOCONFORMIDADES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_TIPONOCONFORMIDADES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_GET_TIPONOCONFORMIDADES @CIA INT,@USU VARCHAR(50), @IDI VARCHAR(20), @COMBO TINYINT=0, @TIPO INT=NULL, @DESDESEGUIMIENTO INT=NULL, @NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos los tipos de NoConformidades" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GetRequests @USU= @USU, @IDI =@IDI, @COMBO=@COMBO,@TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @USU AS VARCHAR(50), @IDI AS VARCHAR(20),@COMBO AS TINYINT, @TIPO AS INT', @USU=@USU, @IDI =@IDI, @COMBO =@COMBO, @TIPO =@TIPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_LOADNOCONFORMIDADESCERRADAS @CIA INT, @PROVE VARCHAR(50), @IDI VARCHAR(20) =NULL, @ID INT=0, @TIPO INT=0, @FECHA VARCHAR(10)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos el servidor y la base de datos del GS." & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos el c�digo y el identificador del PROVEEDOR en el GS." & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos las NOCONFORMIDADES cerradas." & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_NOCONFORMIDADES_CERRADAS @PROVE =@PROVE, @IDI=@IDI, @ID = @ID , @TIPO = @TIPO, @FECHA = @FECHA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @IDI VARCHAR(10), @ID INT, @TIPO INT, @FECHA VARCHAR(10)' , @PROVE = @PROVEGS, @IDI=@IDI, @ID=@ID, @TIPO=@TIPO, @FECHA=@FECHA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_19A3_00_00_20()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
          
   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_20
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.20'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_19A3_00_00_20 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_19A3_00_00_20 = False
End Function

Public Sub V_3_0_Storeds_20()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTDESGLOSE @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL , @VERSION INT,@NUEVO_WORKFLOW INT=0  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_31600 @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50), @VERSION INT ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS, @VERSION = @VERSION " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT  * FROM COPIA_CAMPO WHERE ID = @COPIA_CAMPO)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD  WHERE LD.CAMPO_PADRE=@COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  ' ' NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN LDA " & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_PADRE = @COPIA_CAMPO ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETTIPOSOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETTIPOSOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETTIPOSOLICITUD @CIA INT,@ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos El tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETTIPOSOLICITUD @ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID AS INT', @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_20A3_00_00_21()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
          
   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_21
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.21'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_20A3_00_00_21 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_20A3_00_00_21 = False
End Function

Public Sub V_3_0_Storeds_21()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_FIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_FIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_FIELDSCALC @CIA INT, @GRUPO INT, @CASO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos los campos calculados del grupo" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_FIELDSCALC @GRUPO=@GRUPO,@CASO =@CASO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@GRUPO AS INT, @CASO AS INT', @GRUPO=@GRUPO,@CASO=@CASO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRESUP_LIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRESUP_LIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PRESUP_LIT @CIA INT ,@TIPO INT, @IDI VARCHAR(50) ='SPA'  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos El tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PRESUP_LIT @TIPO=@TIPO,@IDI =@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@TIPO AS INT, @IDI AS VARCHAR(50)', @TIPO=@TIPO,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARBOLPRESUPUESTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARBOLPRESUPUESTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ARBOLPRESUPUESTOS @CIA INT, @TIPO INTEGER, @IDI VARCHAR(50), @ANYO INTEGER, @UON1 AS VARCHAR(50) = NULL, @UON2 AS VARCHAR(50) = NULL, @UON3 AS VARCHAR(50) = NULL, @CODIGO AS VARCHAR(50) = NULL, @DENOMINACION AS VARCHAR(50) = NULL, @UONVacia AS TINYINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ARBOLPRESUPUESTOS @TIPO=@TIPO, @IDI =@IDI , @ANYO =@ANYO , @UON1 =@UON1 , @UON2 =@UON2 , @UON3 =@UON3, @CODIGO=@CODIGO, @DENOMINACION=@DENOMINACION, @UONVacia= @UONVacia '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INTEGER, @IDI VARCHAR(50), @ANYO INTEGER, @UON1 AS VARCHAR(50) = NULL, @UON2 AS VARCHAR(50) = NULL, @UON3 AS VARCHAR(50) = NULL, @CODIGO AS VARCHAR(50) = NULL, @DENOMINACION AS VARCHAR(50) = NULL, @UONVacia TINYINT', @TIPO=@TIPO, @IDI =@IDI , @ANYO =@ANYO , @UON1 =@UON1 , @UON2 =@UON2 , @UON3 =@UON3, @CODIGO=@CODIGO, @DENOMINACION=@DENOMINACION, @UONVacia=@UONVacia" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTDESGLOSE @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL , @VERSION INT, @NUEVO_WORKFLOW INT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAPROVE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA,@CIAPROVE=RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_31600 @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50), @VERSION INT ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS, @VERSION = @VERSION " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT  * FROM COPIA_CAMPO WHERE ID = @COPIA_CAMPO AND CIA=@CIAPROVE)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD  WHERE LD.CAMPO_PADRE=@COPIA_CAMPO AND CIA=@CIAPROVE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  ' ' NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN LDA " & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_PADRE = @COPIA_CAMPO AND CIA=@CIAPROVE ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTFIELD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTFIELD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTFIELD @CIA INT, @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT, @CIAPROV INT = NULL , @PORTAL TINYINT = 0 OUTPUT  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTFIELD @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT', @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT * FROM COPIA_CAMPO WHERE ID = @ID  AND CIA=ISNULL(@CIAPROV,CIA))" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_21A3_00_00_22()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
          
   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_22
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.22'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_21A3_00_00_22 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_21A3_00_00_22 = False
End Function


Public Sub V_3_0_Storeds_22()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_PROCE_PUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_PROCE_PUB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ACT_PROCE_PUB (@CIA_COMP SMALLINT,@CIA_PROVE VARCHAR(50), @NUM_PUB INT, @NUM_PUB_SINOFE INT,@NUM_PUB_AREV INT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE  AS INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @IDPROVE= (SELECT ID FROM CIAS WITH (NOLOCK) WHERE COD=@CIA_PROVE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET PROCE_ACT =@NUM_PUB , PROCE_NUE=@NUM_PUB_SINOFE , PROCE_REV=@NUM_PUB_AREV  WHERE CIA_COMP=@CIA_COMP AND CIA_PROVE=@IDPROVE" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETDEPARTAMENTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETDEPARTAMENTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETDEPARTAMENTOS  @CIA INT,@NIVEL INT=NULL,@UON1 VARCHAR(50)=NULL,@UON2 VARCHAR(50)=NULL,@UON3 VARCHAR(50)=NULL,@USU VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDEPARTAMENTOS @NIVEL=@NIVEL,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@USU=@USU'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NIVEL INT=null , @UON1 VARCHAR(20)=null , @UON2 VARCHAR(20)=null, @UON3 VARCHAR(20)=null, @USU VARCHAR(20)=null ', @NIVEL=@NIVEL,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@USU=@USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETUONS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETUONS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETUONS  @CIA AS INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETUONS @IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI AS VARCHAR(50)', @IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETALMACENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETALMACENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETALMACENES  @CIA AS INT,@COD VARCHAR(4)=NULL,@COD_CENTRO VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las Almacenes" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETAlmacenes @COD,@COD_CENTRO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL,@COD_CENTRO VARCHAR(4)=NULL', @COD=@COD,@COD_CENTRO=@COD_CENTRO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETORGANIZACIONESCOMPRAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETORGANIZACIONESCOMPRAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETORGANIZACIONESCOMPRAS @CIA int, @COD VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las ORGANIZACIONES de COMPRA" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETORGANIZACIONESCOMPRAS @COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL', @COD=@COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETCENTROS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETCENTROS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETCENTROS @CIA INT,  @COD VARCHAR(4)=NULL,@COD_ORGCOMPRAS VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos los CENTROS" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETCENTROS  @COD,@COD_ORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL,@COD_ORGCOMPRAS VARCHAR(4)=NULL', @COD = @COD,@COD_ORGCOMPRAS=@COD_ORGCOMPRAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Public Function CodigoDeActualizacion3_00_00_22A3_00_00_23()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
          
   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_23
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.23'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_22A3_00_00_23 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_22A3_00_00_23 = False
End Function

Public Sub V_3_0_Storeds_23()
Dim sConsulta As String

'FSPM_GETFORM

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFORM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFORM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETFORM @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL, @SOLICPROVE INT =1  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFORM @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD, @SOLICPROVE=@SOLICPROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL, @SOLICPROVE INT =1', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD,@SOLICPROVE=@SOLICPROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ESTADOS_NOCONF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ESTADOS_NOCONF]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ESTADOS_NOCONF @CIA INT,@INSTANCIA INT , @COD VARCHAR(50)=null , @IDI AS VARCHAR(4)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ESTADOS_NOCONF @INSTANCIA = @INSTANCIA, @COD =@COD ,@IDI=@IDI '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT , @COD VARCHAR(50)=null ,@IDI VARCHAR(4)',  @INSTANCIA = @INSTANCIA , @COD =@COD,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion3_00_00_23A3_00_00_24()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
          
   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_24
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.24'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_23A3_00_00_24 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_23A3_00_00_24 = False
End Function

Public Sub V_3_0_Storeds_24()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTDESGLOSE @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL , @VERSION INT, @NUEVO_WORKFLOW INT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAPROVE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA,@CIAPROVE=RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_31600 @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50), @VERSION INT ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS, @VERSION = @VERSION " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT  * FROM COPIA_CAMPO WHERE ID = @COPIA_CAMPO AND CIA=@CIAPROVE)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD  WHERE LD.CAMPO_PADRE=@COPIA_CAMPO AND CIA=@CIAPROVE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "   SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  ' ' NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN LDA " & vbCrLf
sConsulta = sConsulta & "   WHERE CAMPO_PADRE = @COPIA_CAMPO AND CIA=@CIAPROVE ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --para sacar el valor del campo estado" & vbCrLf
sConsulta = sConsulta & "   SELECT * FROM NOCONF_ACC WITH (NOLOCK)  WHERE CAMPO_PADRE = @COPIA_CAMPO AND INSTANCIA=@INSTANCIA  AND CIA=@CIAPROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_24A3_00_00_25()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True

   frmProgreso.lblDetalle = "Cambios en Tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_3_0_Tablas_25

   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_3_0_Storeds_25
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.25'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_24A3_00_00_25 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_24A3_00_00_25 = False
End Function

Private Sub V_3_0_Tablas_25()
Dim sConsulta As String

sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[ACT1]') and name=N'PYME')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT1] ADD [PYME] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_3_0_Storeds_25()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_VISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_VISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ACT_VISIBLE @CIA int,@IDI varchar(20),@PYME varchar(3) = ''" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #mraiz contendr� las actividades pendientes de confirmar por el gerente del portal y en caso" & vbCrLf
sConsulta = sConsulta & "--de que no haya nada pendiente, contendr� una copia de #raiz" & vbCrLf
sConsulta = sConsulta & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4, act5 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
sConsulta = sConsulta & "  from cias_act5 a" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act4 b" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act3 c" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 d" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 e" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
sConsulta = sConsulta & "  from cias_act4 a" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act3 b" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 c" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 d" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
sConsulta = sConsulta & "  from cias_act3 a" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 b" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 c" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2 " & vbCrLf
sConsulta = sConsulta & "  from cias_act2 a" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 b" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1) " & vbCrLf
sConsulta = sConsulta & "select act1 " & vbCrLf
sConsulta = sConsulta & "  from cias_act1 " & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Insertamos los datos correspondientes a #mraiz" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4, act5 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act5 a" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act4 b" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA " & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act3 c" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 d" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 e" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act4 a" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act3 b" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 c" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 d" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act3 a" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 b" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 c" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2 " & vbCrLf
sConsulta = sConsulta & "  from mcias_act2 a" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 b" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1) " & vbCrLf
sConsulta = sConsulta & "select act1 " & vbCrLf
sConsulta = sConsulta & "  from mcias_act1 " & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si no hab�a actividades pendientes de confirmar, se copia #raiz en #mraiz" & vbCrLf
sConsulta = sConsulta & "if (select count(*) from #mraiz)=0" & vbCrLf
sConsulta = sConsulta & "  insert into #mraiz select * from #raiz" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--La tabla #base va a contener las actividades de las cuales se obtendr�n la " & vbCrLf
sConsulta = sConsulta & "--estructura completa (con asignadas, pendientes y hermanas de estas)" & vbCrLf
sConsulta = sConsulta & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,act3,act4,min(act5) " & vbCrLf
sConsulta = sConsulta & "    from (select * from #raiz where act5 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select * from #mraiz where act5 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3,act4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,act3,min(act4) " & vbCrLf
sConsulta = sConsulta & "    from (select * from #raiz where act4 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select * from #mraiz where act4 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,min(act3) " & vbCrLf
sConsulta = sConsulta & "    from (select * from #raiz where act3 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select * from #mraiz where act3 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2)" & vbCrLf
sConsulta = sConsulta & "  select act1,min(act2) " & vbCrLf
sConsulta = sConsulta & "    from (select * from #raiz where act2 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select * from #mraiz where act2 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1)" & vbCrLf
sConsulta = sConsulta & "  select min(act1) " & vbCrLf
sConsulta = sConsulta & "    from (select * from #raiz" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select * from #mraiz) a" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--La tabla #Estructura contendr� toda la estructura completa de actividades asignadas, pendientes y hermanas" & vbCrLf
sConsulta = sConsulta & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.act4,a.id" & vbCrLf
sConsulta = sConsulta & "  from act5 a " & vbCrLf
sConsulta = sConsulta & "     inner join #base b " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
sConsulta = sConsulta & "            and a.act3 = b.act3 " & vbCrLf
sConsulta = sConsulta & "            and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & " Where b.act5 Is Not Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.id" & vbCrLf
sConsulta = sConsulta & "  from act4 a " & vbCrLf
sConsulta = sConsulta & "     inner join #base b " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
sConsulta = sConsulta & "            and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & " Where b.act5 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act4 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
sConsulta = sConsulta & "                      and a.act3=c.act3 " & vbCrLf
sConsulta = sConsulta & "                      and a.id=c.act4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.id" & vbCrLf
sConsulta = sConsulta & "  from act3 a " & vbCrLf
sConsulta = sConsulta & "     inner join #base b " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2" & vbCrLf
sConsulta = sConsulta & " Where b.act4 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act3 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
sConsulta = sConsulta & "                      and a.id=c.act3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.id" & vbCrLf
sConsulta = sConsulta & "  from act2 a " & vbCrLf
sConsulta = sConsulta & "     inner join #base b " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & " Where b.act3 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act2 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.id = c.act2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if (@PYME='')" & vbCrLf
sConsulta = sConsulta & "  insert into #Estructura (Act1)" & vbCrLf
sConsulta = sConsulta & "  select a.id" & vbCrLf
sConsulta = sConsulta & "    from act1 a" & vbCrLf
sConsulta = sConsulta & "   where not exists (SELECT * " & vbCrLf
sConsulta = sConsulta & "                       FROM #Estructura c " & vbCrLf
sConsulta = sConsulta & "                      where a.id=c.act1)" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  insert into #Estructura (Act1)" & vbCrLf
sConsulta = sConsulta & "  select a.id" & vbCrLf
sConsulta = sConsulta & "    from act1 a" & vbCrLf
sConsulta = sConsulta & "   where not exists (SELECT * " & vbCrLf
sConsulta = sConsulta & "                       FROM #Estructura c " & vbCrLf
sConsulta = sConsulta & "                      where a.id=c.act1)" & vbCrLf
sConsulta = sConsulta & "  and a.PYME = @PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizamos los campos actAsign y actPend con el valor correspondiente. Estos campos" & vbCrLf
sConsulta = sConsulta & "--contendr�n el nivel al que la actividad est� asignada (actAsign) o en el caso de que est� pendiente (actPend), el nivel" & vbCrLf
sConsulta = sConsulta & "-- al que lo est�" & vbCrLf
sConsulta = sConsulta & "update #Estructura set actAsign=0 ,actPend=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "select #Estructura.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
sConsulta = sConsulta & "       actasign, actpend" & vbCrLf
sConsulta = sConsulta & "from #Estructura  " & vbCrLf
sConsulta = sConsulta & "     left join act1 " & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "     left join act2 " & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act2.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "     left join act3 " & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act3.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act3.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act3.id     left join act4 " & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act4.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act4.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act4.act3 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act4 = act4.id " & vbCrLf
sConsulta = sConsulta & "     left join act5 " & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act5.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act5.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act5.act3 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act4 = act5.act4 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_25A3_00_00_26()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True

'   frmProgreso.lblDetalle = "Cambios en Tablas"
'   frmProgreso.lblDetalle.Refresh
'   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
'   V_3_0_Tablas_26

   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_3_0_Storeds_26
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.26'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_25A3_00_00_26 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_25A3_00_00_26 = False
End Function

Private Sub V_3_0_Storeds_26()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_ADJUNTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DOWNLOAD_ADJUNTO @INIT INT, @OFFSET INT, @ID INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM ADJUN WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET >= @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_ESPECIFICACION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_ESPECIFICACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DOWNLOAD_ESPECIFICACION @INIT INT, @OFFSET INT, @ID INT,@CIA INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM CIAS_ESP WHERE CIA=@CIA AND ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET >= @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT CIAS_ESP.data @textpointer @INIT  @LOFFSET" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion3_00_00_26A3_00_00_27()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_3_0_Storeds_27
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.27'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_26A3_00_00_27 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_26A3_00_00_27 = False
End Function


Private Sub V_3_0_Storeds_27()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_UPLOAD_ADJUN_GS @CIA INT,   @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL,@SOBRA TINYINT=1 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UPLOAD_ADJUN @INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @SOBRA=@SOBRA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL, @SOBRA TINYINT ',@INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @SOBRA=@SOBRA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_00_27A3_00_00_28()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True

   frmProgreso.lblDetalle = "Cambios en Tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
'   V_3_0_Tablas_28

   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_3_0_Storeds_28
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.28'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_27A3_00_00_28 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_27A3_00_00_28 = False
End Function

Private Sub V_3_0_Storeds_28()
Dim sConsulta As String

'FSQA_SOLPENDIENTES
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_SOLPENDIENTES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_SOLPENDIENTES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE FSQA_SOLPENDIENTES @CIACOMP FLOAT, @CIAPROVE FLOAT, @USU  VARCHAR(20), @IDI VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT GETDATE() AS DIA, USU.NOM + ' ' + USU.APE AS NOMBRE, CIAS.COD AS PROVEENCIA, REL_CIAS.* " & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU ON USU.CIA = REL_CIAS.CIA_PROVE AND COD=@USU" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS ON CIAS.ID=@CIAPROVE AND CIAS.ID=CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "WHERE CIA_COMP = @CIACOMP AND CIA_PROVE = @CIAPROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion3_00_00_28A3_00_00_29()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True

   frmProgreso.lblDetalle = "Cambios en Tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
'   V_3_0_Tablas_28

   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_3_0_Storeds_29
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.29'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_28A3_00_00_29 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_28A3_00_00_29 = False
End Function

Private Sub V_3_0_Storeds_29()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ARTICULOS @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA' ,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion3_00_00_29A3_00_00_30()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo Error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
          
   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1

   V_3_0_Storeds_30
   
   frmProgreso.lblDetalle = "Actualiza vistas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
   frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
   frmProgreso.ProgressBar1.Refresh
   V_3_0_Vistas_30
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.00.30'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_29A3_00_00_30 = True
   Exit Function
   
Error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_29A3_00_00_30 = False
End Function

Public Sub V_3_0_Storeds_30()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ARTICULOS @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA' ,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE    PROCEDURE SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL @SDEN VARCHAR(8),@SBUSCAR VARCHAR(50),@SCOMP INT AS" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA= 'x.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "edu" & vbCrLf
sConsulta = sConsulta & "cuidado." & vbCrLf
sConsulta = sConsulta & "la sentencia SQL ocupaba mas de 4000 caracteres y provocaba errores impredecibles y diferentes en ejecucion" & vbCrLf
sConsulta = sConsulta & "segun la longitud del texto a buscar, se truncaba hasta una determinada posicion." & vbCrLf
sConsulta = sConsulta & "Se han creado 5 vistas VIEW_ACT1 a VIEW_ACT5 que realizan la relacion del arbol con sus niveles superiores." & vbCrLf
sConsulta = sConsulta & "Con eso se simplifica el query y ahora ocupa la mitad, aunque sigue siendo un tocho de cuidado." & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL = '" & vbCrLf
sConsulta = sConsulta & "SELECT x.ID ACT1, NULL ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA + ' DEN ,COUNT(J.CIA) AS PROVE, CODACT1, NULL CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT1 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1 FROM CIAS_ACT1 UNION SELECT CIA, ACT1 FROM CIAS_ACT2 UNION SELECT CIA, ACT1 FROM CIAS_ACT3 UNION SELECT CIA, ACT1 FROM CIAS_ACT4 UNION SELECT CIA, ACT1 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT1=x.ID " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%'' " & vbCrLf
sConsulta = sConsulta & "GROUP BY   x.ID, CODACT1, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1, x.ID ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT2 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2 FROM CIAS_ACT2 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=PP.CIA) J ON J.ACT2=x.ID  AND J.ACT1=x.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY  x.ACT1, x.ID, CODACT1, CODACT2,' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ID ACT3, NULL ACT4, NULL ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT3 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT3=x.ID AND J.ACT2=x.ACT2 AND J.ACT1=x.ACT1 " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ID, CODACT1, CODACT2, CODACT3, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ACT3, x.ID ACT4, NULL ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3,CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT4 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA)  J ON J.ACT4=x.ID AND J.ACT3=x.ACT3 AND J.ACT2=x.ACT2 AND J.ACT1=x.ACT1  " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ACT3,x.ID, CODACT1, CODACT2, CODACT3, CODACT4, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ACT3, x.ACT4, x.ID ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3, CODACT4 , CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT5 x" & vbCrLf
sConsulta = sConsulta & "LEFT  JOIN CIAS_ACT5 J ON J.ACT5=x.ID AND J.ACT4=x.ACT4 AND J.ACT3=x.ACT3 AND J.ACT2=x.ACT2  AND J.ACT1=x.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=J.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=J.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA + ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ACT3,x.ACT4,x.ID, CODACT1, CODACT2, CODACT3, CODACT4, CODACT5, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "ORDER by 1, 2, 3, 4, 5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PROVES @CIA INT, @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @ORGCOMPRAS VARCHAR(4)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS AS VARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @COD " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVES @COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU  ,@ORGCOMPRAS =@ORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL,@ORGCOMPRAS VARCHAR(4)=NULL ',@COD=@PROVEGS, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU, @ORGCOMPRAS =@ORGCOMPRAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ARTICULOS @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA' ,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_3_0_Vistas_30()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT1]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT1 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, x.COD CODACT1" & vbCrLf
sConsulta = sConsulta & "FROM ACT1 as x" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT2]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT2 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, ACT1.COD CODACT1, x.COD CODACT2" & vbCrLf
sConsulta = sConsulta & "FROM ACT2 as x" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT3]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT3 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, ACT1.COD CODACT1, ACT2.COD CODACT2, x.COD CODACT3" & vbCrLf
sConsulta = sConsulta & "FROM ACT3 as x" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON x.ACT1 = ACT2.ACT1 AND x.ACT2 = ACT2.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT4]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT4 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, x.COD CODACT4 " & vbCrLf
sConsulta = sConsulta & "FROM ACT4 as x" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON x.ACT1 = ACT2.ACT1 AND x.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 ON x.ACT1 = ACT3.ACT1 AND x.ACT2 = ACT3.ACT2 AND x.ACT3 = ACT3.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT5]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT5]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT5 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, ACT4.COD CODACT4, x.COD CODACT5 " & vbCrLf
sConsulta = sConsulta & "FROM ACT5 as x" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON x.ACT1 = ACT2.ACT1 AND x.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 ON x.ACT1 = ACT3.ACT1 AND x.ACT2 = ACT3.ACT2 AND x.ACT3 = ACT3.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT4 ON x.ACT1 = ACT4.ACT1 AND x.ACT2 = ACT4.ACT2 AND x.ACT3 = ACT4.ACT3 AND x.ACT3 = ACT4.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

