Attribute VB_Name = "bas_V_2_11"
Option Explicit


Public Function CodigoDeActualizacion2_10_50_00A2_11_00_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Actualizamos la estructura de tablas a la versi�n 2.10
    frmProgreso.lblDetalle = "Se actualizar� a la versi�n 2.11"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_10_50_00A2_11_00_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_50_00A2_11_00_00 = False
End Function

Public Function CodigoDeActualizacion2_11_00_00A2_11_00_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Actualizamos la estructura de tablas a la versi�n 2.10
    frmProgreso.lblDetalle = "Nuevo stored procedure para la obtenci�n de los datos del usuario"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
        
    V_2_11_1_Storeds
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_00_00A2_11_00_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_00A2_11_00_01 = False
End Function

Public Function CodigoDeActualizacion2_11_00_01A2_11_00_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Actualizamos la estructura de tablas a la versi�n 2.10
    frmProgreso.lblDetalle = "Stored procedure para la b�squeda de proveedores"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
        
    V_2_11_2_Storeds
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_00_01A2_11_00_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_01A2_11_00_02 = False
End Function


Private Sub V_2_11_1_Storeds()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_USUARIO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_USUARIO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_USUARIO @CIA VARCHAR(50), @USU INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT U.COD, U.PWD, U.FECPWD, U.APE, U.NOM, U.DEP, U.CAR, U.TFNO, U.TFNO2, U.FAX, U.EMAIL, U.IDI, U.FCEST, U.TFNO_MOVIL, " & vbCrLf
sConsulta = sConsulta & "       U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, U.DATEFMT, U.MOSTRARFMT, U.TIPOEMAIL, U.MON, I.COD IDICOD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  FROM USU U" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN CIAS C" & vbCrLf
sConsulta = sConsulta & "             ON U.CIA = C.ID" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN IDI I" & vbCrLf
sConsulta = sConsulta & "             ON U.IDI = I.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " WHERE C.COD = @CIA" & vbCrLf
sConsulta = sConsulta & "   AND U.ID = @USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Function V_2_11_2_Storeds()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL @SDEN VARCHAR(8),@SBUSCAR VARCHAR(50),@SCOMP INT AS" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA1 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA2 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA3 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA4 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA5 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA1= 'ACT1.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA2= 'ACT2.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA3= 'ACT3.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA4= 'ACT4.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA5= 'ACT5.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "set @SQL = '" & vbCrLf
sConsulta = sConsulta & "SELECT ACT1.ID ACT1, NULL ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA1 + ' DEN ,COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, NULL CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1 FROM CIAS_ACT1 UNION SELECT CIA, ACT1 FROM CIAS_ACT2 UNION SELECT CIA, ACT1 FROM CIAS_ACT3 UNION SELECT CIA, ACT1 FROM CIAS_ACT4 UNION SELECT CIA, ACT1 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT1=ACT1.ID " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA1+ ' LIKE ''%' + @SBUSCAR + '%'' " & vbCrLf
sConsulta = sConsulta & "GROUP BY   ACT1.ID, ACT1.COD, ' + @IDIOMA1 + '" & vbCrLf
sConsulta = sConsulta & "union" & vbCrLf
sConsulta = sConsulta & "SELECT ACT2.ACT1, ACT2.ID ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA2 + ' DEN, COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, ACT2.COD CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2 FROM CIAS_ACT2 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=PP.CIA) J ON J.ACT2=ACT2.ID  AND J.ACT1=ACT2.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON ACT2.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA2+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY  ACT2.ACT1, ACT2.ID, ACT1.COD,ACT2.COD,' + @IDIOMA2 + ' " & vbCrLf
sConsulta = sConsulta & "union" & vbCrLf
sConsulta = sConsulta & "SELECT ACT3.ACT1,ACT3.ACT2, ACT3.ID ACT3, NULL ACT4, NULL ACT5, ' + @IDIOMA3 + ' DEN, COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT3=ACT3.ID AND J.ACT2=ACT3.ACT2 AND J.ACT1=ACT3.ACT1 " & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON ACT3.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON ACT3.ACT1 = ACT2.ACT1 AND ACT3.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA3+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY ACT3.ACT1,ACT3.ACT2,ACT3.ID, ACT1.COD, ACT2.COD, ACT3.COD, ' + @IDIOMA3 + '" & vbCrLf
sConsulta = sConsulta & "union" & vbCrLf
sConsulta = sConsulta & "SELECT ACT4.ACT1,ACT4.ACT2, ACT4.ACT3, ACT4.ID ACT4, NULL ACT5, ' + @IDIOMA4 + ' DEN, COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, ACT4.COD CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT4" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA)  J ON J.ACT4=ACT4.ID AND J.ACT3=ACT4.ACT3 AND J.ACT2=ACT4.ACT2 AND J.ACT1=ACT4.ACT1  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON ACT4.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON ACT4.ACT1 = ACT2.ACT1 AND ACT4.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 ON ACT4.ACT1 = ACT3.ACT1 AND ACT4.ACT2 = ACT3.ACT2 AND ACT4.ACT3 = ACT3.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA4+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID, ACT1.COD, ACT2.COD, ACT3.COD, ACT4.COD, ' + @IDIOMA4 + '" & vbCrLf
sConsulta = sConsulta & "union" & vbCrLf
sConsulta = sConsulta & "SELECT ACT5.ACT1,ACT5.ACT2, ACT5.ACT3, ACT5.ACT4, ACT5.ID ACT5, ' + @IDIOMA5 + ' DEN, COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, ACT4.COD CODACT4, ACT5.COD CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT5" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CIAS_ACT5 J ON J.ACT5=ACT5.ID AND J.ACT4=ACT5.ACT4 AND J.ACT3=ACT5.ACT3 AND J.ACT2=ACT5.ACT2  AND J.ACT1=ACT5.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=J.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=J.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON ACT5.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON ACT5.ACT1 = ACT2.ACT1 AND ACT5.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 ON ACT5.ACT1 = ACT3.ACT1 AND ACT5.ACT2 = ACT3.ACT2 AND ACT5.ACT3 = ACT3.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT4 ON ACT5.ACT1 = ACT4.ACT1 AND ACT5.ACT2 = ACT4.ACT2 AND ACT5.ACT3 = ACT4.ACT3 AND ACT5.ACT3 = ACT4.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA5+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY ACT5.ACT1,ACT5.ACT2,ACT5.ACT3,ACT5.ACT4,ACT5.ID, ACT1.COD, ACT2.COD, ACT3.COD, ACT4.COD, ACT5.COD, ' + @IDIOMA5 + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY 1,2,3,4,5'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Function

