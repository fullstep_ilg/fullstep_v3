Attribute VB_Name = "basUtilidades"
Option Explicit
Public Const ClaveSQLtod = "aldj�w45u9'2JIHO^h+'31t4mV� H hfo fdGAAFKWETJ�51FSjD>GH18071998"
Public Const ClaveOBJtod = "jldfadf40'8946jnm, E�.Z -QS-    2e  -cy..0.qqvj,opry,j,18071998"
Public Const ClaveADMpar = "pruiopjanjup9858 99NTB=TBFS FI S.^�*^P?�afad19068jGIuyg18071998"
Public Const ClaveADMimp = "ljkdagga60TYIOGOIguIugh9p87tygp54854edWQ$""`=H�jl�ni�n�18071998"
Public Const ClaveUSUpar = "agkag�m=,5^**^P*43u9ihoHPGH)�(YIPGBITaDNGFGNDNK�D  ip&?18071998"
Public Const ClaveUSUimp = "h+hlL_^P*:K*M)(YN7HGN7tngn)GFNGNAagag8u998gd\=\]\]]\|||18071998"

Public Function DblQuote(ByVal StrToDblQuote As String) As String
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function
Function DateToSQLDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDate = "NULL"
        Else
            DateToSQLDate = "'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy") & "'"
        End If
    End If
End If

End Function
Public Function DblToSQLFloat(DblToConvert) As String
    Dim SeekPos As Long
    Dim pos As Long
    
    Dim TmpFloat As String
    Dim StrToConvert As String
    
    Dim sDecimal As String
    Dim sThousand As String
        
    If IsEmpty(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If IsNull(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If DblToConvert = "" Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    StrToConvert = CStr(DblToConvert)
    
    sThousand = "."
    
    sDecimal = ","
    
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sThousand)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1)
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

    StrToConvert = DblToSQLFloat
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sDecimal)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1) & "."
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

End Function
Public Function NullToDbl0(ByVal ValueThatCanBeNull As Variant) As Variant
  
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0 = 0
    Else
        NullToDbl0 = ValueThatCanBeNull
    End If

End Function



Function StrToSQLNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULL = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function

Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function




Public Sub ExecuteSQL(ByRef rdoCon As rdoConnection, ByVal SQL As String)

#If ADISCO Then
    Open App.Path & "\sql.sql" For Append As #1
    Print #1, SQL
    Print #1, "GO"
    Close #1
#Else
    rdoCon.Execute SQL, rdExecDirect
#End If
If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
    frmProgreso.ProgressBar1.Value = 1
End If
DoEvents
End Sub


Public Sub ExecuteADOSQL(ByRef adoCon As ADODB.Connection, ByVal SQL As String)

#If ADISCO Then
    Open App.Path & "\sql.sql" For Append As #1
    Print #1, SQL
    Print #1, "GO"
    Close #1
#Else
    adoCon.Execute SQL
#End If
If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
    frmProgreso.ProgressBar1.Value = 1
End If
DoEvents
End Sub

'Public Sub EliminarDefaults(ByRef rdoCon As rdoConnection, ByRef oBD As SQLDMO.Database, ByVal sTable As String)
'
'Dim oTable As SQLDMO.Table
'
'Dim oCol As SQLDMO.Column
'On Error Resume Next
'
'Set oTable = oBD.Tables(sTable)
'If Err <> 0 Then
'    Err.Clear
'Else
'On Error GoTo 0
'Dim sDef As String
'Dim sConsulta As String
'Dim oDRIDef As DRIDefault
'
'For Each oCol In oTable.Columns
'    sDef = oCol.Default
'    If sDef = "" Then
'        Set oDRIDef = oCol.DRIDefault
'        If oDRIDef.Name <> "" Then
'            sDef = oDRIDef.Name
'        End If
'    End If
'    If sDef <> "" Then
'        sConsulta = ""
'        sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" & sDef & "]') )" & vbCrLf
'            sConsulta = sConsulta & "ALTER TABLE [dbo].[" & oTable.Name & "] DROP CONSTRAINT " & sDef & vbCrLf
'        ExecuteSQL rdoCon, sConsulta
'    End If
'Next
'
'End If
'
'End Sub


Public Function NumTransaccionesAbiertas(oCon As rdo.rdoConnection) As Integer

Dim sConsulta As String
Dim rdores As rdo.rdoResultset

sConsulta = "SELECT @@TRANCOUNT NUMTRAN"

    
Set rdores = oCon.OpenResultset(sConsulta, rdOpenKeyset, rdConcurRowVer)

NumTransaccionesAbiertas = rdores("NUMTRAN").Value

rdores.Close
Set rdores = Nothing

End Function

Public Function NumTransaccionesAbiertasADO(oCon As ADODB.Connection) As Integer

Dim sConsulta As String
Dim adoRS As ADODB.Recordset

sConsulta = "SELECT @@TRANCOUNT NUMTRAN"

    
Set adoRS = New ADODB.Recordset

adoRS.Open sConsulta, oCon, adOpenForwardOnly

NumTransaccionesAbiertasADO = adoRS("NUMTRAN").Value

adoRS.Close
Set adoRS = Nothing

End Function

Public Function EncriptarAES_319008(ByVal Seed As String, ByVal dato As String, ByVal Encriptando As Boolean, ByVal Fecha As Variant _
, ByVal Quien As Integer, Optional ByVal Tipo As Integer) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As cCryptAES_319008
    
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
        
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New cCryptAES_319008
    
    oCrypt2.InBuffer = dato
    
    If Quien = 0 Then
        oCrypt2.Codigo = Left(ClaveSQLtod, 6)
    ElseIf Quien = 1 Then
        diacrypt = Day(Fecha)
        
        If IsMissing(Tipo) Then 'CUsuario.IBaseDatos_CambiarCodigo
            If diacrypt Mod 2 = 0 Then
                oCrypt2.Codigo = Seed & Left(ClaveUSUpar, 6)
            Else
                oCrypt2.Codigo = Seed & Left(ClaveUSUimp, 6)
            End If
        Else
            If diacrypt Mod 2 = 0 Then
                If Tipo = 1 Then
                    oCrypt2.Codigo = Seed & Left(ClaveADMpar, 6)
                Else
                    oCrypt2.Codigo = Seed & Left(ClaveUSUpar, 6)
                End If
            Else
                If Tipo = 1 Then
                    oCrypt2.Codigo = Seed & Left(ClaveADMimp, 6)
                Else
                    oCrypt2.Codigo = Seed & Left(ClaveUSUimp, 6)
                End If
            End If
        End If
    Else
        oCrypt2.Codigo = Seed & Left(ClaveOBJtod, 6)
    End If
        
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
    
    EncriptarAES_319008 = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function

