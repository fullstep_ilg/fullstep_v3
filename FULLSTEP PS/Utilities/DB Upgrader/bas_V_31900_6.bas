Attribute VB_Name = "bas_V_31900_6"
Option Explicit

Private Const Clavepar = "agkag�m=,5^**^P*43u9ihoHPGH)�(YIPGBITaDNGFGNDNK�D  ip&?18071998"
Private Const Claveimp = "h+hlL_^P*:K*M)(YN7HGN7tngn)GFNGNAagag8u998gd\=\]\]]\|||18071998"

Private Const ClaveSQLtod = "aldj�w45u9'2JIHO^h+'31t4mV� H hfo fdGAAFKWETJ�51FSjD>GH18071998"

Private Const ClaveUSUpar = "agkag�m=,5^**^P*43u9ihoHPGH)�(YIPGBITaDNGFGNDNK�D  ip&?18071998"
Private Const ClaveUSUimp = "h+hlL_^P*:K*M)(YN7HGN7tngn)GFNGNAagag8u998gd\=\]\]]\|||18071998"

Public Function CodigoDeActualizacion31900_03_15_10A31900_04_16_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    
    On Error GoTo error

    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 7200
    
    ExecuteADOSQL oADOConnection, "BEGIN TRANSACTION"
    ExecuteADOSQL oADOConnection, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Tablas_000 oADOConnection
    
    frmProgreso.lblDetalle = "Cambios en datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Datos_000 oADOConnection
     
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteADOSQL oADOConnection, sConsulta
    
    ExecuteADOSQL oADOConnection, "COMMIT"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_10A31900_04_16_00 = True
    Exit Function
    
error:
    If NumTransaccionesAbiertasADO(oADOConnection) > 0 Then
        ExecuteADOSQL oADOConnection, "ROLLBACK TRANSACTION"
    End If
        
    MsgBox Err.Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_10A31900_04_16_00 = False
End Function

Private Sub V_31900_6_Tablas_000(ByVal oADOConnection As ADODB.Connection)
Dim sConsulta As String

sConsulta = "ALTER TABLE [dbo].[ADM] " & vbCrLf
sConsulta = sConsulta & " ALTER COLUMN [USU] [VARCHAR] (100) NOT NULL" & vbCrLf
ExecuteADOSQL oADOConnection, sConsulta

sConsulta = "ALTER TABLE [dbo].[OBJSADM] " & vbCrLf
sConsulta = sConsulta & " ALTER COLUMN [USU] [VARCHAR] (100) NOT NULL" & vbCrLf
ExecuteADOSQL oADOConnection, sConsulta

End Sub

Private Sub V_31900_6_Datos_000(ByVal oADOConnection As ADODB.Connection)
    Dim sConsulta As String
    Dim adoCom As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim adores As ADODB.Recordset
    Dim sConsultaUpd, TextoUpd, Texto As String
    Dim diacrypt As Integer
    
    '-----------------------------------
    Set adores = New ADODB.Recordset
    
    sConsulta = "SELECT CIA, ID, PWD, FECPWD FROM USU WITH(NOLOCK)"
    
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
        
    While Not adores.EOF
        Texto = DesEncriptar(0, adores("PWD").Value, adores("FECPWD").Value)
        
        diacrypt = Day(adores("FECPWD").Value)
            
        If diacrypt Mod 2 = 0 Then
            TextoUpd = Encrypt(adores("FECPWD").Value, "agkag�", Texto) 'Clavepar
        Else
            TextoUpd = Encrypt(adores("FECPWD").Value, "h+hlL_", Texto) 'Claveimp
        End If
        
        Set adoCom = New ADODB.Command
        Set adoCom.ActiveConnection = oADOConnection
        adoCom.CommandType = adCmdText
        
        sConsultaUpd = "UPDATE USU SET PWD= ? "
        sConsultaUpd = sConsultaUpd & " WHERE CIA=" & adores("CIA").Value & vbCrLf
        sConsultaUpd = sConsultaUpd & " AND ID=" & adores("ID").Value & vbCrLf
        adoCom.CommandText = sConsultaUpd
        
        Set adoParam = adoCom.CreateParameter("PWD", adVarChar, adParamInput, 128, TextoUpd)
        adoCom.Parameters.Append adoParam
        
        adoCom.Execute

        '
        adores.MoveNext
    Wend
           
    adores.Close
    Set adores = Nothing
        
    
    '-----------------------------------
    Set adores = New ADODB.Recordset
    
    sConsulta = "SELECT ID, P_PWD,PWD,FECPWD FROM PARGEN_MAIL WITH(NOLOCK)"
    
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    If Not adores.EOF Then
        If Not IsNull(adores("P_PWD").Value) Then
            Texto = DesEncriptar(1, adores("P_PWD").Value)
            
            TextoUpd = Encrypt(DateSerial(1974, 3, 5), "PWD", Texto)
            
            sConsultaUpd = "UPDATE PARGEN_MAIL SET P_PWD= ? " & vbCrLf
            sConsultaUpd = sConsultaUpd & " WHERE ID=" & adores("ID").Value & vbCrLf
                    
            Set adoCom = New ADODB.Command
            Set adoCom.ActiveConnection = oADOConnection
            adoCom.CommandType = adCmdText
            adoCom.CommandText = sConsultaUpd
            Set adoParam = adoCom.CreateParameter("PWD", adVarChar, adParamInput, 100, TextoUpd)
            adoCom.Parameters.Append adoParam
            adoCom.Execute
            
        End If
        
        If Not IsNull(adores("PWD").Value) Then
            Texto = DesEncriptar(3, adores("PWD").Value, adores("FECPWD").Value)
            
            TextoUpd = Encrypt(DateSerial(1974, 3, 5), "PWD", Texto)
            
            sConsultaUpd = "UPDATE PARGEN_MAIL SET PWD= ? " & vbCrLf
            sConsultaUpd = sConsultaUpd & " WHERE ID=" & adores("ID").Value & vbCrLf
                    
            Set adoCom = New ADODB.Command
            Set adoCom.ActiveConnection = oADOConnection
            adoCom.CommandType = adCmdText
            adoCom.CommandText = sConsultaUpd
            Set adoParam = adoCom.CreateParameter("PWD", adVarChar, adParamInput, 100, TextoUpd)
            adoCom.Parameters.Append adoParam
            adoCom.Execute
            
        End If
    End If
    
    adores.Close
    Set adores = Nothing
    
    '-----------------------------------
    Set adores = New ADODB.Recordset
    
    sConsulta = "SELECT USU, PWD, FECPWD FROM ADM WITH(NOLOCK)"
    
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    If Not adores.EOF Then
        If Not IsNull(adores("USU").Value) Then
            Texto = DesEncriptar(0, adores("USU").Value, adores("FECPWD").Value)
            
            diacrypt = Day(adores("FECPWD").Value)

            If diacrypt Mod 2 = 0 Then
                TextoUpd = Encrypt(adores("FECPWD").Value, "agkag�", Texto) 'Clavepar
            Else
                TextoUpd = Encrypt(adores("FECPWD").Value, "h+hlL_", Texto) 'Claveimp
            End If

            sConsultaUpd = "UPDATE ADM SET USU= ? " & vbCrLf

            Set adoCom = New ADODB.Command
            Set adoCom.ActiveConnection = oADOConnection
            adoCom.CommandType = adCmdText
            
            adoCom.CommandText = sConsultaUpd
            
            Set adoParam = adoCom.CreateParameter("USU", adVarChar, adParamInput, 100, TextoUpd)
            adoCom.Parameters.Append adoParam
            adoCom.Execute
        End If
        If Not IsNull(adores("PWD").Value) Then
            Texto = DesEncriptar(0, adores("PWD").Value, adores("FECPWD").Value)
            
            diacrypt = Day(adores("FECPWD").Value)

            If diacrypt Mod 2 = 0 Then
                TextoUpd = Encrypt(adores("FECPWD").Value, "agkag�", Texto) 'Clavepar
            Else
                TextoUpd = Encrypt(adores("FECPWD").Value, "h+hlL_", Texto) 'Claveimp
            End If

            sConsultaUpd = "UPDATE ADM SET PWD= ? " & vbCrLf

            Set adoCom = New ADODB.Command
            Set adoCom.ActiveConnection = oADOConnection
            adoCom.CommandType = adCmdText
            adoCom.CommandText = sConsultaUpd
            Set adoParam = adoCom.CreateParameter("PWD", adVarChar, adParamInput, 100, TextoUpd)
            adoCom.Parameters.Append adoParam
            adoCom.Execute
        End If
    End If
    
    
    
    '-----------------------------------
    Set adores = New ADODB.Recordset
    sConsulta = "SELECT USU, PWD, FECPWD FROM OBJSADM WITH(NOLOCK)"
    
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    If Not adores.EOF Then
        If Not IsNull(adores("USU").Value) Then
            Texto = DesEncriptar(0, adores("USU").Value, adores("FECPWD").Value)
            
            diacrypt = Day(adores("FECPWD").Value)

            If diacrypt Mod 2 = 0 Then
                TextoUpd = Encrypt(adores("FECPWD").Value, "agkag�", Texto) 'Clavepar
            Else
                TextoUpd = Encrypt(adores("FECPWD").Value, "h+hlL_", Texto) 'Claveimp
            End If

            sConsultaUpd = "UPDATE OBJSADM SET USU= ? " & vbCrLf

            Set adoCom = New ADODB.Command
            Set adoCom.ActiveConnection = oADOConnection
            adoCom.CommandType = adCmdText
            adoCom.CommandText = sConsultaUpd
            Set adoParam = adoCom.CreateParameter("USU", adVarChar, adParamInput, 100, TextoUpd)
            adoCom.Parameters.Append adoParam
            adoCom.Execute
        End If
        If Not IsNull(adores("PWD").Value) Then
            Texto = DesEncriptar(0, adores("PWD").Value, adores("FECPWD").Value)
            
            diacrypt = Day(adores("FECPWD").Value)

            If diacrypt Mod 2 = 0 Then
                TextoUpd = Encrypt(adores("FECPWD").Value, "agkag�", Texto) 'Clavepar
            Else
                TextoUpd = Encrypt(adores("FECPWD").Value, "h+hlL_", Texto) 'Claveimp
            End If

            sConsultaUpd = "UPDATE OBJSADM SET PWD= ? " & vbCrLf

            Set adoCom = New ADODB.Command
            Set adoCom.ActiveConnection = oADOConnection
            adoCom.CommandType = adCmdText
            adoCom.CommandText = sConsultaUpd
            Set adoParam = adoCom.CreateParameter("PWD", adVarChar, adParamInput, 100, TextoUpd)
            adoCom.Parameters.Append adoParam
            adoCom.Execute
        End If
    End If
End Sub

Private Function DesEncriptar(ByVal Quien As Integer, ByVal Dato As String, Optional ByVal Fecha As Variant, Optional ByVal cadena As String) As String
Dim fechahoracrypt
Dim pwdcrypt
Dim diacrypt
Dim oCrypt1 As CCrypt1
Dim oCrypt2 As CCrypt2
Dim sKeyString
        
        Dato = SustituirCaracteresInvalidos(Dato, False)
        
        Set oCrypt1 = New CCrypt1
        
        If Quien = 0 Then
            If IsMissing(Fecha) Then
                Fecha = DateSerial(1974, 3, 5)
            Else
                If Fecha = "" Then
                    Fecha = DateSerial(1974, 3, 5)
                End If
            End If
                                    
            diacrypt = Day(Fecha)
        
            fechahoracrypt = Format(Fecha, "DD\/MM\/YYYY HH\:NN\:SS")
        
            If diacrypt Mod 2 = 0 Then
                oCrypt1.KeyString = Clavepar & fechahoracrypt
            Else
                oCrypt1.KeyString = Claveimp & fechahoracrypt
            End If
        
            pwdcrypt = sKeyString & oCrypt1.Encrypt("KING" & sKeyString)
        ElseIf Quien = 1 Then
            oCrypt1.KeyString = ClaveSQLtod
            
            pwdcrypt = oCrypt1.Encrypt("PWD")
        ElseIf Quien = 2 Then
            If Len(cadena) Mod 2 = 0 Then
                oCrypt1.KeyString = ClaveUSUpar & cadena
            Else
                oCrypt1.KeyString = ClaveUSUimp & cadena
            End If
            
            pwdcrypt = cadena & oCrypt1.Encrypt(cadena)
        ElseIf Quien = 3 Then 'es como el 1 pero el formato de fecha es diferente. No he encontrado una bbdd con pargen_mail relleno ... sin probar si es lo mismo o no -> 2 casos e vez de uno
            fechahoracrypt = Format(Fecha, "DD/MM/YYYY HH:MM:SS")
            
            diacrypt = Day(Fecha)
            
            If diacrypt Mod 2 = 0 Then
                oCrypt1.KeyString = Clavepar & fechahoracrypt
            Else
                oCrypt1.KeyString = Claveimp & fechahoracrypt
            End If
            
            pwdcrypt = sKeyString & oCrypt1.Encrypt("KING" & sKeyString)
        End If
        
        Set oCrypt1 = Nothing
        
        Set oCrypt2 = New CCrypt2
        
        oCrypt2.InBuffer = Dato
        oCrypt2.Password = pwdcrypt
        oCrypt2.Encrypt
        
        'Datos
        DesEncriptar = oCrypt2.OutBuffer
                
        Set oCrypt2 = Nothing
        
End Function

Private Function Encrypt(ByVal dFecha As Date, ByVal sSeed As String, ByVal sPwd As String) As String
    Dim ObjCrypt As FSNAES
    Set ObjCrypt = New FSNAES
        
    Encrypt = ObjCrypt.EncryptStringToString(dFecha, sSeed, sPwd)
End Function

Private Function SustituirCaracteresInvalidos(ByVal s As String, ByVal Encriptar As Boolean) As String
Dim temp1 As String
Dim i As Integer
Dim bSumar As Boolean

    temp1 = ""
    
    
    
    If Encriptar Then
        temp1 = Replace(s, Chr(0), Chr(255) & Chr(255) & Chr(255))
    Else
        temp1 = Replace(s, Chr(255) & Chr(255) & Chr(255), Chr(0))
    End If
    
    
SustituirCaracteresInvalidos = temp1

End Function

Public Function CodigoDeActualizacion31900_03_16_00A31900_04_16_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_00A31900_04_16_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_00A31900_04_16_01 = False
End Function


Private Sub V_31900_6_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_GETADJUNTO_CONTRATO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO] @CIA INT, @ID INT =NULL, @ID_CONTRATO INT =NULL, @ID_CAMPO INT=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTO_CONTRATO @ID=@ID,@ID_CONTRATO=@ID_CONTRATO,@ID_CAMPO=@ID_CAMPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID  INT, @ID_CONTRATO INT, @ID_CAMPO INT ', @ID=@ID,@ID_CONTRATO=@ID_CONTRATO,@ID_CAMPO=@ID_CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31900_03_16_01A31900_04_16_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_01A31900_04_16_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_01A31900_04_16_02 = False
End Function

Private Sub V_31900_6_Storeds_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AUTORIZAR_PREMIUM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_AUTORIZAR_PREMIUM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_AUTORIZAR_PREMIUM] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT @SQLSTRING = @LNKSERVER + 'SP_ACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET PREMIUM =2  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PREM_ACTIVO =1 WHERE CIA_PROVE = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_COMPANIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL_ADJUN WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "--- 2.9.5" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "------------" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DESAUTORIZAR_PREMIUM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DESAUTORIZAR_PREMIUM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DESAUTORIZAR_PREMIUM] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   SELECT @SQLSTRING = @LNKSERVER + 'SP_DESACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET PREMIUM = 3  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PREM_ACTIVO = 0 WHERE CIA_PROVE = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_03_16_02A31900_04_16_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Tablas_003
    
    frmProgreso.lblDetalle = "Cambios en Triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Triggers_003
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_02A31900_04_16_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_02A31900_04_16_03 = False
End Function

Private Sub V_31900_6_Tablas_003()
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[SESION](" & vbCrLf
sConsulta = sConsulta & "   [ID] [varchar](40) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [CIACOMP] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [PYME] [varchar](3) NULL," & vbCrLf
sConsulta = sConsulta & "   [CIAID] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [CIACOD] [varchar](100) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [CIACODGS] [varchar](100) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [USUID] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [USUCOD] [varchar](100) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [IPDIR] [varchar](20) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [USUPPAL] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "   [CN] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "   [PEDIDOS] [int] NULL," & vbCrLf
sConsulta = sConsulta & "   [PEDPENDIENTES] [int] NULL," & vbCrLf
sConsulta = sConsulta & "   [PMSOLICITUDES] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "   [CERTIFICADOS] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "   [NOCONFORMIDADES] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "   [NOCONFNUEVAS] [int] NULL," & vbCrLf
sConsulta = sConsulta & "   [FSGAIDUSU] [int] NULL," & vbCrLf
sConsulta = sConsulta & "   [IDORDEN] [int] NULL," & vbCrLf
sConsulta = sConsulta & "   [BAJACALIDAD] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "   [QAVARCAL] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "   [CSRFToken] [varchar](20) NULL," & vbCrLf
sConsulta = sConsulta & "   [FECINISES] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "   [FECFINSES] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "   [ACTIVA] [tinyint] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [PERSISTID] [varchar](40) NULL," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NOT NULL," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_SESION] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [ID] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[SESION] ADD  CONSTRAINT [DF_SESION_ACTIVA]  DEFAULT ((1)) FOR [ACTIVA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PARGEN_INTERNO] ADD CADSESION SMALLINT NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_CADSESION] DEFAULT 20"
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_6_Triggers_003()
Dim sConsulta As String

sConsulta = "CREATE TRIGGER [dbo].[SESION_TG_INSUPD] ON [dbo].[SESION] FOR INSERT, UPDATE AS " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE SESION SET FECACT=GETDATE() " & vbCrLf
sConsulta = sConsulta & "   FROM SESION A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSERTED I WITH (NOLOCK) ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_31900_6_Storeds_003()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_LOGIN_SOBRETABLA_SESION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_LOGIN_SOBRETABLA_SESION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_LOGIN_SOBRETABLA_SESION] @SESIONID VARCHAR(40),@IPDIR VARCHAR(40),@PERSISTID VARCHAR(40) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT CIACOMP,CIACOD,CIAID,USUCOD, USU.PWD, USU.FECPWD,SESION.CN,SESION.CIACODGS" & vbCrLf
sConsulta = sConsulta & "   FROM SESION WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN USU  WITH(NOLOCK) ON USU.CIA=SESION.CIAID AND USU.COD=SESION.USUCOD" & vbCrLf
sConsulta = sConsulta & "   WHERE SESION.ID= @SESIONID AND  SESION.IPDIR= @IPDIR AND SESION.PERSISTID= @PERSISTID" & vbCrLf
sConsulta = sConsulta & "   AND FECFINSES > GETDATE() AND ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @CADSESION INT" & vbCrLf
sConsulta = sConsulta & "   SET @CADSESION = (SELECT TOP 1 CADSESION FROM PARGEN_INTERNO WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE SESION " & vbCrLf
sConsulta = sConsulta & "   SET FECFINSES = DATEADD(MINUTE, @CADSESION,GETDATE())" & vbCrLf
sConsulta = sConsulta & "   WHERE SESION.ID= @SESIONID AND  SESION.IPDIR= @IPDIR AND SESION.PERSISTID= @PERSISTID" & vbCrLf
sConsulta = sConsulta & "   AND FECFINSES > GETDATE() AND ACTIVA=1  " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_ELIM_SESION_CADUCADA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_ELIM_SESION_CADUCADA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_ELIM_SESION_CADUCADA] AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CADSESION INT" & vbCrLf
sConsulta = sConsulta & "   SET @CADSESION = (SELECT TOP 1 CADSESION FROM PARGEN_INTERNO WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE SESION WHERE ACTIVA =1 AND DATEADD(MINUTE, @CADSESION,FECFINSES)<GETDATE()" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_JOB_CREARJOBELIMSESION_CADUCADA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_JOB_CREARJOBELIMSESION_CADUCADA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_JOB_CREARJOBELIMSESION_CADUCADA] " & vbCrLf
sConsulta = sConsulta & "(@NOMBRE_JOB VARCHAR(50),@NOMBRE_SCH VARCHAR(50),@TIPO INT,@INTERVALO INT,@TIPO2 INT,@INTERVALO2 INT, @FECINI INT,@TIMEINI INT,@NOMBRE_STEP VARCHAR(50),@NOMBRE_PROC VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "   DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SERVNAME AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @RET_STAT INT " & vbCrLf
sConsulta = sConsulta & "   DECLARE @ContadorTareas int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SELECT @SERVNAME=@@SERVERNAME" & vbCrLf
sConsulta = sConsulta & "      SET @BD=SUBSTRING(@NOMBRE_JOB,22,LEN(@NOMBRE_JOB)-18)" & vbCrLf
sConsulta = sConsulta & "      --Comprobacion de si existe la tarea." & vbCrLf
sConsulta = sConsulta & "      SELECT @ContadorTareas = COUNT(Name) FROM msdb.dbo.SysJobs with (nolock) WHERE Name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "       IF @ContadorTareas<>0" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "       --Si existe la tarea..." & vbCrLf
sConsulta = sConsulta & "       EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO, @freq_subday_type=@TIPO2,@freq_subday_interval=@INTERVALO2, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      else" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "          EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "          IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                SET @COMANDO=@NOMBRE_PROC" & vbCrLf
sConsulta = sConsulta & "                EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'TSQL', @command =@COMANDO,@database_name=@BD" & vbCrLf
sConsulta = sConsulta & "                IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE_JOB,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "                    IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                          EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPO2,@freq_subday_interval=@INTERVALO2, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_03_16_03A31900_04_16_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_03A31900_04_16_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_03A31900_04_16_04 = False
End Function

Private Sub V_31900_6_Storeds_004()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_COMPROBAR_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_COMPROBAR_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_COMPROBAR_EMAIL](@FSP_CIA VARCHAR(20),@EMAIL VARCHAR(100),@EXISTE INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE   @CIA INTEGER" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CIA = (SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@FSP_CIA)" & vbCrLf
sConsulta = sConsulta & "SET @EXISTE = (SELECT COUNT(*)  FROM USU WITH(NOLOCK) INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA = USU_PORT.CIA AND USU.ID = USU_PORT.USU  WHERE USU.CIA=@CIA AND USU.EMAIL=@EMAIL)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB] @CIA_COMP INTEGER, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @ATRIB int = null, @ITEM int = null AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @AMBITO = @AMBITO, @GRUPO = @GRUPO, @ATRIB = @ATRIB, @ITEM = @ITEM'" & vbCrLf
sConsulta = sConsulta & "   SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @ATRIB int, @ITEM int'" & vbCrLf
sConsulta = sConsulta & "   EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @AMBITO = @AMBITO, @GRUPO = @GRUPO, @ATRIB = @ATRIB, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "   RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_MON]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  [dbo].[SP_ELIM_MON]  (@MON_COD NVARCHAR(50),@CODMONCENTRAL NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXISTE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMON INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMONCEN INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDMON=(SELECT ID FROM MON WITH(NOLOCK) WHERE COD=@MON_COD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDMON IS NOT NULL " & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "    SET @IDMONCEN=(SELECT ID FROM MON WITH(NOLOCK) WHERE COD=@CODMONCENTRAL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM CIAS WITH(NOLOCK) WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM PAI WITH(NOLOCK) WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE PAI SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM USU WITH(NOLOCK) WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM MON_DEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM MON WHERE ID=@IDMON" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PROV_REINTENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_PROV_REINTENTOS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR,@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(1000),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(100),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(100),@CIAS_PROVI VARCHAR(100),@CIAS_CP VARCHAR(100),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(50),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100),@PORT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@ESPEC INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CICLOS= (SELECT PROV_CICLOS FROM PARGEN_GEST WITH(NOLOCK)) " & vbCrLf
sConsulta = sConsulta & "SET @REINTENTOS = (SELECT PROV_REINTENTOS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = (SELECT PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_P INT,@IDI VARCHAR(50),@TIPOEMAIL SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1), @DECIMALFMT CHAR(1), @PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CICLOS <> 0 AND @PASADAS%@CICLOS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "    SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "    WHERE MVTOS_PROV_EST.EST = 1 AND MVTOS_PROV_EST.INTENTOS < @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "    OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                     SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                     FROM REL_CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "      SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "            FROM CIAS WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "            WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          END                                          " & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "        ELSE  " & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@USU' " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= '@COD_PROVE_CIA VARCHAR(200),@ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "          END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT FPEST FROM USU_PORT WITH(NOLOCK) WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3 " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                         @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "              FROM USU WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN IDI AS IDI WITH(NOLOCK) ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "                                         WHERE USU.CIA = @CIA_PROVE AND USU.ID= @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                               @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                               @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                               @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                               @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                               @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                               @DATEFMT=@DATEFMT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_DEP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                           @ID_P INT," & vbCrLf
sConsulta = sConsulta & "                           @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "                           @THOUSANFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                           @DECIMALFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                           @PRECISIONFMT TINYINT, " & vbCrLf
sConsulta = sConsulta & "                           @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "              EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                   @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                   @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                   @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                   @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                   @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                   @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                   @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                   @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                   @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                   @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            END     " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT FPEST FROM USU_PORT WITH(NOLOCK) WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3 " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                         @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "               FROM USU WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN IDI AS IDI WITH(NOLOCK) ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "               WHERE  USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "              SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                  @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @DATEFMT=@DATEFMT'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "              SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT," & vbCrLf
sConsulta = sConsulta & "                              @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                              @DECIMALFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                              @PRECISIONFMT TINYINT, " & vbCrLf
sConsulta = sConsulta & "                              @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                      @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                      @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "            END                                        " & vbCrLf
sConsulta = sConsulta & "          END " & vbCrLf
sConsulta = sConsulta & "        END " & vbCrLf
sConsulta = sConsulta & "        ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "        BEGIN   " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                      @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "            @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "        END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR <> 0            " & vbCrLf
sConsulta = sConsulta & "        BEGIN " & vbCrLf
sConsulta = sConsulta & "          SET @INTENTO = @INTENTO +1" & vbCrLf
sConsulta = sConsulta & "          IF @INTENTO = @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "            SET @EST_FILA = 2" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @EST_FILA = 3            " & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "    CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "  UPDATE PARGEN_GEST SET PROV_PASADAS = @PASADAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_CARGAR_CONFIG_PROCE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_CARGAR_CONFIG_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_03_16_04A31900_04_16_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_005
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_04A31900_04_16_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_04A31900_04_16_05 = False
End Function

Private Sub V_31900_6_Storeds_005()
Dim sConsulta As String

'/****** Object:  StoredProcedure [dbo].[FSQA_VALIDARCERTIFICADO]    Script Date: 09/27/2012 17:29:29 ******/
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_VALIDARCERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_VALIDARCERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'/****** Object:  StoredProcedure [dbo].[FSQA_VER_PUNTUACION_CALIDAD]    Script Date: 09/27/2012 17:29:29 ******/
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_VER_PUNTUACION_CALIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_VER_PUNTUACION_CALIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_16_05A31900_04_16_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Tablas_006
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_006
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_05A31900_04_16_06 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_05A31900_04_16_06 = False
End Function

Private Sub V_31900_6_Tablas_006()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TBPARTIDAS' AND ss.name = N'dbo')" & vbCrLf
sConsulta = sConsulta & "DROP TYPE [dbo].[TBPARTIDAS] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_6_Storeds_006()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  " & vbCrLf
sConsulta = sConsulta & "   @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO, @NUMPEDIDO=@NUMPEDIDO,@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE,@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP,@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO,@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA,@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE,@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE,@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT, @NUMPEDIDO INT,@NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME,@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200),@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME,@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50), @PARTIDASPRES NVARCHAR(MAX)', " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                   @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                   @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                   @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                   @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                   @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                   @PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_16_06A31900_04_16_07() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_007
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_06A31900_04_16_07 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_06A31900_04_16_07 = False
End Function

Private Sub V_31900_6_Storeds_007()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
sConsulta = sConsulta & "   -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST WITH(NOLOCK) INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                        WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PERSONAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PERSONAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PERSONAS] @CIA INT, @COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL,@BAJALOG AS TINYINT=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PERSONAS @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP, @BAJALOG = @BAJALOG '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL,@BAJALOG AS TINYINT=1 ', @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP , @BAJALOG = @BAJALOG" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLUCION_INSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLUCION_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLUCION_INSTANCIA] @CIA INT,  @CIAPROVE INT, @INSTANCIA INT,@PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT ', @INSTANCIA=@INSTANCIA, @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE] @CIA INT, @INSTANCIA INT,@IDCAMPO INT,@VERSION INT,@IDI VARCHAR(20)='SPA',  @USU VARCHAR(50)=NULL,   @PROVE VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTDESGLOSEPADREVISIBLE @INSTANCIA=@INSTANCIA, @IDCAMPO = @IDCAMPO, @VERSION=@VERSION,@IDI=@IDI, @USU = @USU, @PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDCAMPO  INT, @VERSION INT, @IDI VARCHAR(50),@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL', @INSTANCIA=@INSTANCIA, @IDCAMPO = @IDCAMPO, @VERSION=@VERSION, @IDI=@IDI, @USU = @USU, @PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_REALIZAR_ACCION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_REALIZAR_ACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_REALIZAR_ACCION] @CIA INT,  @CIAPROVE INT, @INSTANCIA INT, @INSTANCIABLOQUE INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL ,@DEVOLUCION TINYINT=0, @ETAPA INT , @USUNOM VARCHAR(500)=NULL,@IDIOMAPROVE VARCHAR(3)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @INSTANCIABLOQUE=@INSTANCIABLOQUE, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE, @DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @INSTANCIABLOQUE INT,  @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT,@DEVOLUCION TINYINT,@ETAPA INT , @USUNOM VARCHAR(500),@IDIOMAPROVE VARCHAR(3)=NULL  ', @INSTANCIA=@INSTANCIA,  @INSTANCIABLOQUE=@INSTANCIABLOQUE,  @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE,@DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEVOLUCION = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Por programa SET XACT_ABORT ON si falla FSPM_TRASPASAR_INSTANCIA aqui no llega" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET SOLIC_ACT= SOLIC_ACT - 1,SOLIC_NUE= SOLIC_NUE - 1 WHERE CIA_COMP = @CIA AND CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL, @RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, @ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, @FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, @EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, @ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, @PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, @NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, @FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, @FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, @FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, @ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, @CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, @CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, @CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, @UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & " @PREC_VISIBLE int = NULL, @PREC_WIDTH FLOAT = NULL, @IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, @IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, @IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, @MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, @CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL,@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL,@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL,@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, @PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL, @PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, @PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, @PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la informaci�n del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tama�o suficiente para ser visible pero no molestar" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT, @ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT, @PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT, @FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT, @CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, @UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT," & vbCrLf
sConsulta = sConsulta & " @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT, @CC2_WIDTH FLOAT,@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @ANCHODEFECTO INT'," & vbCrLf
sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT [RETRASADO_VISIBLE],[RETRASADO_WIDTH],[ALBARAN_VISIBLE],[ALBARAN_WIDTH],[FRECEP_VISIBLE],[FRECEP_WIDTH],[EMP_VISIBLE],[EMP_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[ERP_VISIBLE],[ERP_WIDTH],[PEDIDO_VISIBLE],[PEDIDO_WIDTH],[NUMPEDPROVE_VISIBLE],[NUMPEDPROVE_WIDTH],[FEMISION_VISIBLE],[FEMISION_WIDTH],[FENTREGA_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[FENTREGA_WIDTH],[FENTREGAPROVE_VISIBLE],[FENTREGAPROVE_WIDTH],[ART_VISIBLE],[ART_WIDTH],[CANTPED_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[CANTPED_WIDTH],[CANTREC_VISIBLE],[CANTREC_WIDTH],[CANTPEND_VISIBLE],[CANTPEND_WIDTH],[UNI_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[UNI_WIDTH],[PREC_VISIBLE],[PREC_WIDTH],[IMPPED_VISIBLE],[IMPPED_WIDTH],[IMPREC_VISIBLE],[IMPREC_WIDTH],[IMPPEND_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[IMPPEND_WIDTH],[MON_VISIBLE],[MON_WIDTH],[CC1_VISIBLE],[CC1_WIDTH],[CC2_VISIBLE],[CC2_WIDTH],[CC3_VISIBLE],[CC3_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[CC4_VISIBLE],[CC4_WIDTH],[PARTIDA1_VISIBLE],[PARTIDA1_WIDTH],[PARTIDA2_VISIBLE],[PARTIDA2_WIDTH],[PARTIDA3_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[PARTIDA3_WIDTH],[PARTIDA4_VISIBLE],[PARTIDA4_WIDTH]" & vbCrLf
sConsulta = sConsulta & "FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE USU = @IDUSU AND CIA = @CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_DETALLE_ACTIVO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_DETALLE_ACTIVO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSSM_DETALLE_ACTIVO @CIA INT,@IDIOMA VARCHAR(20), @CODACTIVO NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSSM_DETALLE_ACTIVO @IDIOMA=@IDIOMA,@CODACTIVO=@CODACTIVO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(20), @CODACTIVO NVARCHAR(20)',@IDIOMA=@IDIOMA,@CODACTIVO=@CODACTIVO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_16_07A31900_04_16_08() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_008
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_07A31900_04_16_08 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_07A31900_04_16_08 = False
End Function

Private Sub V_31900_6_Storeds_008()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_CREATE_NEW_CERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_CREATE_NEW_CERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PERSONAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PERSONAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "/*" & vbCrLf
sConsulta = sConsulta & "''' Revisado por: Mpf. Fecha: 17/10/2012" & vbCrLf
sConsulta = sConsulta & "''' <summary>" & vbCrLf
sConsulta = sConsulta & "''' Recupera los datos de la persona que no esten dados de baja" & vbCrLf
sConsulta = sConsulta & "''' </summary>" & vbCrLf
sConsulta = sConsulta & "''' <param name=""@COD"">codigo de la persona</param>" & vbCrLf
sConsulta = sConsulta & "''' <param name=""@NOM"">Nombre de la persona</param>" & vbCrLf
sConsulta = sConsulta & "''' <param name=""@APE"">apellido de la persona</param>" & vbCrLf
sConsulta = sConsulta & "''' <param name=""@UON1"">Unidad Organizativa nivel 1</param>" & vbCrLf
sConsulta = sConsulta & "''' <param name=""@UON2"">Unidad Organizativa nivel 2</param>" & vbCrLf
sConsulta = sConsulta & "''' <param name=""@UON3"">Unidad Organizativa nivel  3</param>" & vbCrLf
sConsulta = sConsulta & "''' <param name=""@DEP"">Departamento de la pesonana</param>                        " & vbCrLf
sConsulta = sConsulta & "''' <param name=""@BAJALOG"">si se controla o no la bajalogica</param>   " & vbCrLf
sConsulta = sConsulta & "''' <remarks>Llamada desde:ROOT.VB --> Personas_Load; Tiempo m�ximo: </remarks>      " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PERSONAS] @CIA INT, @COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL,@BAJALOG AS TINYINT=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PERSONAS @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP, @BAJALOG = @BAJALOG '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD AS VARCHAR(50)=NULL,@NOM AS VARCHAR(50)=NULL,@APE AS VARCHAR(100)=NULL,@UON1 AS VARCHAR(20)=NULL,@UON2 AS VARCHAR(20)=NULL,@UON3 AS VARCHAR(20)=NULL,@DEP AS VARCHAR(20)=NULL,@BAJALOG AS TINYINT=1 ', @COD =@COD, @NOM =@NOM, @APE = @APE ,@UON1 =@UON1, @UON2 =@UON2 ,@UON3 =@UON3 , @DEP = @DEP , @BAJALOG = @BAJALOG" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARGEN_INTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "-- =============================================" & vbCrLf
sConsulta = sConsulta & "-- Revisado por: NSA. Fecha: 15/11/2012" & vbCrLf
sConsulta = sConsulta & "-- Cargar toda la informaci�n interna (tablas pargen_xxx) de la plataforma Portal        " & vbCrLf
sConsulta = sConsulta & "-- <param name=""CIA"">Id de la compa�ia</param>" & vbCrLf
sConsulta = sConsulta & "-- <returns>Informacion requerida </returns>" & vbCrLf
sConsulta = sConsulta & "-- <remarks>Llamada desde: FULSETPPORTAL/SERVER => CRAIZ/devolverParametrosGenerales() </remarks>" & vbCrLf
sConsulta = sConsulta & "-- =============================================" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT PI.ACCESO_FSSM, PI.VER_NUMEXT, PI.ACCESO_FSGS FROM ' + @FSGS + 'PARGEN_INTERNO PI WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_PARAM_LITERAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_PARAM_LITERAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "/*  ''' Revisado por: blp. Fecha 28/10/2011" & vbCrLf
sConsulta = sConsulta & "    ''' <summary>" & vbCrLf
sConsulta = sConsulta & "   ''' Funci�n que devuelve un texto concreto de la tabla PARGEN_LIT para el idioma requerido" & vbCrLf
sConsulta = sConsulta & "   ''' </summary>" & vbCrLf
sConsulta = sConsulta & "   ''' <param name=""@IDIOMA"">Idioma en que se quiere recuperar el literal</param>" & vbCrLf
sConsulta = sConsulta & "   ''' <param name=""IDLIT"">Id del literal que se quiere recuperar</param>" & vbCrLf
sConsulta = sConsulta & "   ''' <param name=""@CIA"">ID la empresa</param>" & vbCrLf
sConsulta = sConsulta & "    ''' <remarks>" & vbCrLf
sConsulta = sConsulta & "    ''' Llamada desde: PMPortalDatabaseServer\Root.vb --> Parametros_CargarLiteralParametros()" & vbCrLf
sConsulta = sConsulta & "    ''' Tiempo m�ximo: 0,1 sec.</remarks>" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSP_OBT_PARAM_LITERAL]  @IDLIT INT, @IDIOMA NVARCHAR(50) = 'SPA', @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @CIA IS NULL AND @CIA <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS with (nolock) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT DEN FROM ' + @FSGS + 'PARGEN_LIT WITH(NOLOCK) WHERE ID = @IDLIT AND IDI=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDLIT INT, @IDIOMA NVARCHAR(50)', @IDLIT=@IDLIT, @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_03_16_08A31900_04_16_09() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_009
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_08A31900_04_16_09 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_08A31900_04_16_09 = False
End Function

Private Sub V_31900_6_Storeds_009()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "-- <summary>Cambio deL c�digo de un grupo en las ofertas del grupo</summary>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@ANYO"">c�digo nuevo</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@GMN1"">c�digo nuevo</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@PROCE"">c�digo nuevo</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@COD_OLD"">c�digo nuevo</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@COD_NEW"">c�digo nuevo</param>" & vbCrLf
sConsulta = sConsulta & "-- <remarks>Llamada desde: FSGSServer.CGrupo.IBaseDatos_CambiarCodigo</remarks>" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS] " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @PROCE SMALLINT," & vbCrLf
sConsulta = sConsulta & "   @COD_OLD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @COD_NEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @OFE_GR_ATRIB TABLE (ID INT,GRUPO VARCHAR(50),ATRIB_ID INT,VALOR_NUM FLOAT,VALOR_TEXT VARCHAR(800)," & vbCrLf
sConsulta = sConsulta & "                                VALOR_FEC DATETIME,VALOR_BOOL TINYINT,IMPPARCIAL FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   --Antes del borrado se hace una inserci�n en la var. tabla @OFE_GR_ATRIB porque GRUPO es una foreign key a GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO @OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ATRIB     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- GRUPO_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE GRUPO_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM GRUPO_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=GRUPO_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND GRUPO_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- ITEM_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE ITEM_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM ITEM_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=ITEM_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND ITEM_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ADJUN ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ADJUN,OGA.NOM,OGA.COM,OGA.IDPORTAL,OGA.IDGS,OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ADJUN     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ---------- " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,OGA.GRUPO,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM @OFE_GR_ATRIB OGA      " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_COSTESDESC ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "   SELECT OGC.ID,@COD_NEW,OGC.ATRIB_ID,OGC.IMPPARCIAL,OGC.FECACT" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_COSTESDESC    " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_ITEM_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ITEM_ATRIB SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OFE_ITEM_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OFE_ITEM_ATRIB.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_16_09A31900_04_16_10() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_010
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_09A31900_04_16_10 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_09A31900_04_16_10 = False
End Function

Private Sub V_31900_6_Storeds_010()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
sConsulta = sConsulta & "   @ID INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINVISIBLEFIELDS @ID=@ID,@SOLIC_PROVE=2 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_03_16_10A31900_04_16_11() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_6_Storeds_011
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.16.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_16_10A31900_04_16_11 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_16_10A31900_04_16_11 = False
End Function

Private Sub V_31900_6_Storeds_011()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
sConsulta = sConsulta & "   -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD SIN WITH(NOLOCK) modificado desde el 09/10/2012 versi�n 31900.6 tarea 2519" & vbCrLf
sConsulta = sConsulta & "   --De poner  WITH(NOLOCK) se obtendr�a este error" & vbCrLf
sConsulta = sConsulta & "   --      Msg 16941, Level 16, State 1, Procedure SP_ACTUALIZAR_PROV_GS, Line 416" & vbCrLf
sConsulta = sConsulta & "   --      Cursor updates are not allowed on tables opened with the NOLOCK option." & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                        WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub

