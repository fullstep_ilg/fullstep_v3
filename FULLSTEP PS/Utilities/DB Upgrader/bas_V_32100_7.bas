Attribute VB_Name = "bas_V_32100_7"
Public Function CodigoDeActualizacion32100_07_25_00_A32100_07_25_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_7_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.25.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_07_25_00_A32100_07_25_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_07_25_00_A32100_07_25_01 = False
End Function

Private Sub V_32100_7_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ARTICULOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ARTICULOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_BUSQREGEXTERNA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_BUSQREGEXTERNA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_COMPROBAR_LINEA_ELIMINAR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_COMPROBAR_LINEA_ELIMINAR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEFTABLAEXTERNA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEFTABLAEXTERNA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DESTINOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DESTINOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_COMENT_ESTADO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_COMENT_ESTADO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_DIAGBLOQUES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_DIAGBLOQUES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_DIAGENLACES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_DIAGENLACES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_07_25_01_A32100_07_25_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_7_Tablas_002
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_7_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.25.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_07_25_01_A32100_07_25_02 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_07_25_01_A32100_07_25_02 = False
End Function

Private Sub V_32100_7_Storeds_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_ACCESOEXTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_ACCESOEXTERNO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_ACCESOEXTERNO]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SELECT ACCESO_EXTERNO FROM PARGEN_INTERNO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32100_7_Tablas_002()
Dim sConsulta As String

sConsulta = "IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'ACCESO_EXTERNO' AND Object_ID = Object_ID(N'PARGEN_INTERNO'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_INTERNO ADD ACCESO_EXTERNO TINYINT NOT NULL CONSTRAINT DF_PARGEN_INTERNO_ACCESO_EXTERNO DEFAULT (0)" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub
