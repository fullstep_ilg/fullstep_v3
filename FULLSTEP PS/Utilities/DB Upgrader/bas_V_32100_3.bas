Attribute VB_Name = "bas_V_32100_3"
Public Function CodigoDeActualizacion32100_02_21_05_A32100_02_22_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
     frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_3_Tablas_000
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_3_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.22.00'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_02_21_05_A32100_02_22_00 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_02_21_05_A32100_02_22_00 = False
End Function

Private Sub V_32100_3_Tablas_000()
Dim sConsulta As String

sConsulta = "IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'VALOR_TEXT' AND Object_ID = Object_ID(N'OFE_ATRIB'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB ALTER COLUMN VALOR_TEXT NVARCHAR(MAX) NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'VALOR_TEXT' AND Object_ID = Object_ID(N'OFE_GR_ATRIB'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB ALTER COLUMN VALOR_TEXT NVARCHAR(MAX) NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'VALOR_TEXT' AND Object_ID = Object_ID(N'OFE_ITEM_ATRIB'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB ALTER COLUMN VALOR_TEXT NVARCHAR(MAX) NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32100_3_Storeds_000()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]" & vbCrLf
sConsulta = sConsulta & "@ANYO SMALLINT," & vbCrLf
sConsulta = sConsulta & "@GMN1 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@PROCE INT," & vbCrLf
sConsulta = sConsulta & "@COD_OLD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@COD_NEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE_GR_ATRIB TABLE (ID INT,GRUPO VARCHAR(50),ATRIB_ID INT,VALOR_NUM FLOAT,VALOR_TEXT VARCHAR(MAX)," & vbCrLf
sConsulta = sConsulta & "VALOR_FEC DATETIME,VALOR_BOOL TINYINT,IMPPARCIAL FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------- OFE_GR_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "--Se hace un borrado de los registros existentes porque el grupo est? en la clave" & vbCrLf
sConsulta = sConsulta & "--Antes del borrado se hace una inserci?n en la var. tabla @OFE_GR_ATRIB porque GRUPO es una foreign key a GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "INSERT INTO @OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "SELECT OGA.ID,@COD_NEW,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Se hace un borrado de los registros existentes" & vbCrLf
sConsulta = sConsulta & "DELETE OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------- GRUPO_OFE ----------" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "FROM GRUPO_OFE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=GRUPO_OFE.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND GRUPO_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------- ITEM_OFE ----------" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=ITEM_OFE.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND ITEM_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------- OFE_GR_ADJUN ----------" & vbCrLf
sConsulta = sConsulta & "--Se hace un borrado de los registros existentes y una inserci?n con el nuevo valor porque el grupo est? en la clave" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "SELECT OGA.ID,@COD_NEW,OGA.ADJUN,OGA.NOM,OGA.COM,OGA.IDPORTAL,OGA.IDGS,OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------- OFE_GR_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "--Se hace un borrado de los registros existentes y una inserci?n con el nuevo valor porque el grupo est? en la clave" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "SELECT OGA.ID,OGA.GRUPO,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "FROM @OFE_GR_ATRIB OGA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------- OFE_GR_COSTESDESC ----------" & vbCrLf
sConsulta = sConsulta & "--Se hace un borrado de los registros existentes y una inserci?n con el nuevo valor porque el grupo est? en la clave" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "SELECT OGC.ID,@COD_NEW,OGC.ATRIB_ID,OGC.IMPPARCIAL,OGC.FECACT" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------- OFE_ITEM_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "FROM OFE_ITEM_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OFE_ITEM_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OFE_ITEM_ATRIB.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_02_22_00_A32100_02_22_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_3_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.22.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_02_22_00_A32100_02_22_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_02_22_00_A32100_02_22_01 = False
End Function

Private Sub V_32100_3_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_RECUPERAR_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_RECUPERAR_USUARIO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_RECUPERAR_USUARIO]" & vbCrLf
sConsulta = sConsulta & "@CIACOD NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@USUCOD NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@EMAIL NVARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@EXISTE TINYINT=1 OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USUID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @CIAID=USU.CIA,@USUID=USU.ID" & vbCrLf
sConsulta = sConsulta & "FROM USU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON CIAS.ID=USU.CIA AND CIAS.COD=@CIACOD AND USU.COD=@USUCOD AND USU.EMAIL=@EMAIL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PWD=NULL,FECPWD=GETDATE(),SALT=NULL,PWD_HASH=NEWID() WHERE ID=@USUID AND CIA=@CIAID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @USUID IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @EXISTE=0" & vbCrLf
sConsulta = sConsulta & "   ELSE    " & vbCrLf
sConsulta = sConsulta & "       SET @EXISTE=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_03_22_01_A32100_03_22_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_3_Tablas_002
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_3_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.22.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_03_22_01_A32100_03_22_02 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_03_22_01_A32100_03_22_02 = False
End Function

Private Sub V_32100_3_Tablas_002()
Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'LOGNOELIMINAR' AND Object_ID = Object_ID(N'PARGEN_INTERNO'))" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARGEN_INTERNO DROP CONSTRAINT DF_PARGEN_INTERNO_LOGNOELIMINAR;" & vbCrLf
sConsulta = sConsulta & "   EXEC sp_RENAME 'PARGEN_INTERNO.LOGNOELIMINAR' , 'LOGELIMINAR', 'COLUMN';" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARGEN_INTERNO ADD  CONSTRAINT DF_PARGEN_INTERNO_LOGELIMINAR DEFAULT ((0)) FOR LOGELIMINAR;" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32100_3_Storeds_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSTRATADOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSTRATADOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CATALOGAR_KPIS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CATALOGAR_KPIS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_EVENTOS_HISTATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_EVENTOS_HISTATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_REGISTRAR_ACCESO_ASPX]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_FSAL_ESTADISTICAS_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_FSAL_REGISTRAR_ACCESO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_FSAL_REGISTRAR_ACCESO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSATRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSATRASLADARLOG_GS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSATRASLADARLOG_GS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Se tienen que pasar desde la tabla SES a FSAL_ACCESOS Aquellas sesiones que consideremos activas." & vbCrLf
sConsulta = sConsulta & "--Las sesiones activas son las que tiene el TRASPADO=0 o TRASPASO=2" & vbCrLf
sConsulta = sConsulta & "--las fechas de la tabla SES ya est?n en UTC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "           --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "           --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000. " & vbCrLf
sConsulta = sConsulta & "           --Cogemos los registros que aun no se hayan traspasado (traspaso=0) o los que tengan la fecha fin y el traspaso = 2" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'SELECT TOP 1000 ID, USU, FIS, FFS, TRASPASO, (SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH(NOLOCK) ORDER BY ID ASC) AS FSAL_ENTORNO, (SELECT TOP 1 COD FROM ' + @FSGS + 'FSAL_PYMES WITH(NOLOCK) ORDER BY ID ASC) AS FSAL_PYME FROM ' + @FSGS + 'SES WITH(NOLOCK) WHERE (FIS IS NOT NULL AND TRASPASO = 0) OR ((TRASPASO=2 AND (FFS IS NOT NULL)) OR ((TRASPASO=2 AND (FFS IS NULL) AND (DATEDIFF(HH,FIS,GETUTCDATE())>=8)))) ORDER BY FIS ASC' --datediff de gestdate y ffis sea mayor igual que 8" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'SES WITH(NOLOCK) WHERE (FIS IS NOT NULL AND TRASPASO = 0) OR ((TRASPASO=2 AND (FFS IS NOT NULL)) OR ((TRASPASO=2 AND (FFS IS NULL) AND (DATEDIFF(HH,FIS,GETUTCDATE())>=8)))) ORDER BY FIS ASC) UPDATE CTE SET TRASPASO=TRASPASO+1' --datediff de gestdate y ffis sea mayor igual que 8" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_BACKBORRADO_ACCESOS_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_BACKBORRADO_ACCESOS_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_BACKBORRADO_ACCESOS_FSGS] @PERIODO INT,@BORRAR_BACK INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_BACKBORRADO_ACCESOS_FSGS @PERIODO=@PERIODO, @BORRAR_BACK=@BORRAR_BACK'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQLSTRING, N'@PERIODO INT, @BORRAR_BACK INT', @PERIODO=@PERIODO, @BORRAR_BACK=@BORRAR_BACK" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_BACKBORRADO_ERRORES_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_BACKBORRADO_ERRORES_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_BACKBORRADO_ERRORES_FSGS] @PERIODO INT,@BORRAR_BACK INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_BACKBORRADO_ERRORES_FSGS @PERIODO=@PERIODO, @BORRAR_BACK=@BORRAR_BACK'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQLSTRING, N'@PERIODO INT, @BORRAR_BACK INT', @PERIODO=@PERIODO, @BORRAR_BACK=@BORRAR_BACK" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_BACKBORRADO_EVENTOS_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_BACKBORRADO_EVENTOS_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_BACKBORRADO_EVENTOS_FSGS] @PERIODO INT,@BORRAR_BACK INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_BACKBORRADO_EVENTOS_FSGS @PERIODO=@PERIODO, @BORRAR_BACK=@BORRAR_BACK'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQLSTRING, N'@PERIODO INT, @BORRAR_BACK INT', @PERIODO=@PERIODO, @BORRAR_BACK=@BORRAR_BACK" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CATALOGAR_PAGINAS_POR_KPI]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CATALOGAR_PAGINAS_POR_KPI]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CATALOGAR_PAGINAS_POR_KPI]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "       SET @SQLSTRING= @FSGS + 'FSAL_CATALOGAR_PAGINAS_POR_KPI'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_KPIS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_KPIS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ESTADISTICAS_KPIS] @fecha DATETIME,@entorno nvarchar(50),@pyme nvarchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
sConsulta = sConsulta & "--La fecha/hora @DESDE viene en UTC desde el servicio FSALPortalWinService," & vbCrLf
sConsulta = sConsulta & "--por lo que @HASTA tambien la pasaremos en UTC." & vbCrLf
sConsulta = sConsulta & "--Criterio general: * Los datos de origen para estadisticas registrados en UTC(FSAL_ACCESOS) -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "--                 * Los datos de origen que no esten en UTC(LOG, TIPO_SOLICITUDES -> FSAL_EVENTOS, ERRORES -> FSAL_ERRORES) ->" & vbCrLf
sConsulta = sConsulta & "--                   -> rango de traspaso en UTC y fecha insertada con el traspaso en UTC. -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "SET @HASTA=GETUTCDATE()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_ESTADISTICAS_KPIS @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)', @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_EVENTOS_ESTATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_EVENTOS_ESTATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_EVENTOS_ESTATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA_EVENTO,ANYO,MES,DIA,HORA,MIN,NUM_EVENTOS,TIPO_EVENTO,FSAL_ENTORNO,FSAL_PYME,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_EVENTOS_ESTADISTICAS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_EVENTOS_ESTADISTICAS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISANYOATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISANYOATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISANYOATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,ANYO,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISDIAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISDIAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISDIAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_DIA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_DIA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISHORAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISHORAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISHORAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_HORA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_HORA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISMESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISMESATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISMESATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISMINATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISMINATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISMINATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,HORA,MIN,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_MINUTO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_MINUTO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISSEMANAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISSEMANAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISSEMANAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,ANYO,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,PAGINA,FSAL_ENTORNO,PRODUCTO,FICLIPREPAR,FFCLIPREPAR,FISERVER,FFSERVER,FICLIRESP,FFCLIRESP,TPREPAR,TUPLOAD,TSERVER,TDOWNLOAD,TRESPUESTA,TTOTAL,PAGINAORIGEN,USUARIO,PROVE,IP,TRASPASO,FECACT,POSTBACK,HTTP_USER_AGENT,FSAL_PYME,KPI,CATALOGADO,IDREGISTRO,QUERYSTRING,UON FROM ' + @FSGS + 'FSAL_ACCESOS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_ACCESOS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDISANYOATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDISANYOATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSDISANYOATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,ANYO,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDISMESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDISMESATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSDISMESATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDISSEMANAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDISSEMANAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSDISSEMANAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_HORA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_HORA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,HORA,MIN,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MINUTO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MINUTO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,ENTORNO,PYME,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ERRORESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ERRORESATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ERRORESATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FSAL_ENTORNO,FSAL_PYME,FECHA_ERROR,ID_ERROR,EX_FULLNAME,DATA,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_ERRORES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_ERRORES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO] @fecha DATETIME,@entorno nvarchar(50),@pyme nvarchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
sConsulta = sConsulta & "--La fecha/hora @DESDE viene en UTC desde el servicio FSALPortalWinService," & vbCrLf
sConsulta = sConsulta & "--por lo que @HASTA tambien la pasaremos en UTC." & vbCrLf
sConsulta = sConsulta & "--Criterio general: * Los datos de origen para estadisticas registrados en UTC(FSAL_ACCESOS) -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "--                 * Los datos de origen que no esten en UTC(LOG, TIPO_SOLICITUDES -> FSAL_EVENTOS, ERRORES -> FSAL_ERRORES) ->" & vbCrLf
sConsulta = sConsulta & "--                   -> rango de traspaso en UTC y fecha insertada con el traaspaso en UTC. -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "SET @HASTA=GETUTCDATE()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_ESTADISTICAS_ENTORNO @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)', @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ERRORES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ERRORES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ESTADISTICAS_ERRORES]" & vbCrLf
sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAFIN DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAINI DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@FECHAINI DATETIME, @FECHAFIN DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "--La fecha/hora @fecha que luego se asigna a @FECHAINI viene en UTC desde el servicio FSALPortalWinService," & vbCrLf
sConsulta = sConsulta & "--por lo que @FECHAFIN tambien la pasaremos en UTC." & vbCrLf
sConsulta = sConsulta & "--Criterio general: * Los datos de origen para estadisticas registrados en UTC(FSAL_ACCESOS) -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "--                 * Los datos de origen que no esten en UTC(LOG, TIPO_SOLICITUDES -> FSAL_EVENTOS, ERRORES -> FSAL_ERRORES) ->" & vbCrLf
sConsulta = sConsulta & "--                   -> rango de traspaso en UTC y fecha insertada con el traspaso en UTC. -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "SET @FECHAFIN=GETUTCDATE()" & vbCrLf
sConsulta = sConsulta & "SET @FECHAINI=@fecha" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'EXECUTE ' + @FSGS + 'FSAL_ESTADISTICAS_ERRORES @FECHAINI, @FECHAFIN, @ENTORNO, @PYME'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION, @FECHAINI=@FECHAINI, @FECHAFIN=@FECHAFIN, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "--La fecha/hora @fecha que luego se asigna a @DESDE viene en UTC desde el servicio FSALPortalWinService," & vbCrLf
sConsulta = sConsulta & "--por lo que @HASTA tambien la pasaremos en UTC." & vbCrLf
sConsulta = sConsulta & "--Criterio general: * Los datos de origen para estadisticas registrados en UTC(FSAL_ACCESOS) -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "--                 * Los datos de origen que no esten en UTC(LOG, TIPO_SOLICITUDES -> FSAL_EVENTOS, ERRORES -> FSAL_ERRORES) ->" & vbCrLf
sConsulta = sConsulta & "--                   -> rango de traspaso en UTC y fecha insertada con el traspaso en UTC. -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "SET @HASTA=GETUTCDATE()" & vbCrLf
sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING='EXECUTE ' + @FSGS + 'FSAL_ESTADISTICAS_EVENTOS @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION, @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_EVENTOSATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_EVENTOSATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_EVENTOSATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FSAL_ENTORNO,FECHA_EVENTO,TIPO_EVENTO,DATA,FECACT,FSAL_PYME,TRASPASO FROM ' + @FSGS + 'FSAL_EVENTOS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_EVENTOS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY FECACT ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_OBTENER_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_OBTENER_ENTORNO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_OBTENER_ENTORNO]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1 COD FROM '+@FSGS +' FSAL_ENTORNOS WITH(NOLOCK) ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_OBTENER_PYME]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_OBTENER_PYME]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_OBTENER_PYME]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1 COD FROM '+ @FSGS + ' FSAL_PYMES WITH(NOLOCK) ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRANSFERIR_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRANSFERIR_EVENTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_TRANSFERIR_EVENTOS]" & vbCrLf
sConsulta = sConsulta & "@fecha DATETIME," & vbCrLf
sConsulta = sConsulta & "@entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "@pyme nvarchar(50)=null" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SET @PARAMDEFINITION = N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PYM VARCHAR(50)= ISNULL(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "--La fecha/hora @fecha que luego se asigna a @DESDE viene en UTC desde el servicio FSALPortalWinService," & vbCrLf
sConsulta = sConsulta & "--por lo que @HASTA tambien la pasaremos en UTC." & vbCrLf
sConsulta = sConsulta & "--Criterio general: * Los datos de origen para estadisticas registrados en UTC(FSAL_ACCESOS) -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "--                 * Los datos de origen que no esten en UTC(LOG, TIPO_SOLICITUDES -> FSAL_EVENTOS, ERRORES -> FSAL_ERRORES) ->" & vbCrLf
sConsulta = sConsulta & "--                   -> rango de traspaso en UTC y fecha insertada con el traaspaso en UTC. -> rangos para estadisticas en UTC." & vbCrLf
sConsulta = sConsulta & "SET @HASTA=GETUTCDATE()" & vbCrLf
sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "       SET @SQLSTRING='EXECUTE ' + @FSGS + 'FSAL_TRANSFERIR_EVENTOS @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQLSTRING, @PARAMDEFINITION, @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@entorno, @pyme=@PYM" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_03_22_02_A32100_03_22_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_3_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.22.03'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_03_22_02_A32100_03_22_03 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_03_22_02_A32100_03_22_03 = False
End Function

Private Sub V_32100_3_Storeds_003()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_SAVE_PERSONAFISICA_INFO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_SAVE_PERSONAFISICA_INFO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_SAVE_PERSONAFISICA_INFO]" & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @USUCOD NVARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "   @NOMBRE NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @PRIMERAPELLIDO NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "   @SEGUNDOAPELLIDO NVARCHAR(100)=NULL,    " & vbCrLf
sConsulta = sConsulta & "   @USUNIF NVARCHAR(100)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS" & vbCrLf
sConsulta = sConsulta & "   SET PERSONAFISICA_NOMBRE=@NOMBRE," & vbCrLf
sConsulta = sConsulta & "       PERSONAFISICA_APELLIDO1=@PRIMERAPELLIDO," & vbCrLf
sConsulta = sConsulta & "       PERSONAFISICA_APELLIDO2=@SEGUNDOAPELLIDO," & vbCrLf
sConsulta = sConsulta & "       ACEPTACION_CONDICIONES=1" & vbCrLf
sConsulta = sConsulta & "   WHERE ID=@CIA" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @USUNIF IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU" & vbCrLf
sConsulta = sConsulta & "           SET NIF=@USUNIF" & vbCrLf
sConsulta = sConsulta & "       WHERE CIA=@CIA AND COD=@USUCOD" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_03_22_03_A32100_03_22_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32100_3_Storeds_004

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.22.04'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32100_03_22_03_A32100_03_22_04 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32100_03_22_03_A32100_03_22_04 = False
End Function

Private Sub V_32100_3_Storeds_004()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_COMPROBAR_LINEA_ELIMINAR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_COMPROBAR_LINEA_ELIMINAR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_COMPROBAR_LINEA_ELIMINAR] @CIA INT, @IDDESGLOSE INT, @NUMLINEA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_COMPROBAR_LINEA_ELIMINAR @IDDESGLOSE =@IDDESGLOSE, @NUMLINEA=@NUMLINEA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDDESGLOSE INT, @NUMLINEA INT ', @IDDESGLOSE =@IDDESGLOSE, @NUMLINEA=@NUMLINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSANYOATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,ANYO,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,PAGINA,FSAL_ENTORNO,PRODUCTO,FICLIPREPAR,FFCLIPREPAR,FISERVER,FFSERVER,FICLIRESP,FFCLIRESP,TPREPAR,TUPLOAD,TSERVER,TDOWNLOAD,TRESPUESTA,TTOTAL,PAGINAORIGEN,USUARIO,PROVE,IP,TRASPASO,FECACT,POSTBACK,HTTP_USER_AGENT,FSAL_PYME,KPI,CATALOGADO,IDREGISTRO,QUERYSTRING,UON FROM ' + @FSGS + 'FSAL_ACCESOS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_ACCESOS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSDIAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_DIA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSHORAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_HORA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_HORA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSMESATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSMINATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,HORA,MIN,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MINUTO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_MINUTO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ACCESOSSEMANAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,ENTORNO,PYME,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_USUARIOS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ERRORESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ERRORESATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_ERRORESATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FSAL_ENTORNO,FSAL_PYME,FECHA_ERROR,ID_ERROR,EX_FULLNAME,DATA,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_ERRORES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_ERRORES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_EVENTOS_ESTATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_EVENTOS_ESTATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_EVENTOS_ESTATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA_EVENTO,ANYO,MES,DIA,HORA,MIN,NUM_EVENTOS,TIPO_EVENTO,FSAL_ENTORNO,FSAL_PYME,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_EVENTOS_ESTADISTICAS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_EVENTOS_ESTADISTICAS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_EVENTOSATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_EVENTOSATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_EVENTOSATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FSAL_ENTORNO,FECHA_EVENTO,TIPO_EVENTO,DATA,FECACT,FSAL_PYME,TRASPASO FROM ' + @FSGS + 'FSAL_EVENTOS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_EVENTOS WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISANYOATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISANYOATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISANYOATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,ANYO,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_ANYO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISDIAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISDIAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISDIAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_DIA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_DIA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISHORAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISHORAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISHORAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_HORA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_HORA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISMESATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISMESATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISMESATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_MES WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISMINATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISMINATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISMINATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,HORA,MIN,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_MINUTO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_MINUTO WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_KPISDISSEMANAATRASLADAR_FSGS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_KPISDISSEMANAATRASLADAR_FSGS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[FSAL_KPISDISSEMANAATRASLADAR_FSGS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "          --Seleccionamos un maximo de 1000 registros ya que al pasar un gran volumen de registros al webservice a traves del datatable, da error." & vbCrLf
sConsulta = sConsulta & "          --Entendemos que en momentos de menor actividad, las continuas ejecuciones del servicio de traspaso permiten que se lleguen a traspasar todos los registros pendientes, aun siendo de 1000 en 1000." & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT TOP 1000 ID,FECHA,ANYO,MES,DIA,ENTORNO,PYME,KPI,TARGET,PONDERACION,ACCESOS,MAX_TACCESO,MIN_TACCESO,AVG_TACCESO,RENDIMIENTO,FECACT,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "          SET @SQLSTRING = N'WITH CTE AS (SELECT TOP 1000 ID,TRASPASO FROM ' + @FSGS + 'FSAL_KPIS_DIS_SEMANA WITH(NOLOCK) WHERE TRASPASO = 0 ORDER BY ID ASC) UPDATE CTE SET TRASPASO=1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_03_22_04_A32100_03_22_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32100_3_Storeds_22_005

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.22.05'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32100_03_22_04_A32100_03_22_05 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32100_03_22_04_A32100_03_22_05 = False
End Function

Private Sub V_32100_3_Storeds_22_005()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETDESGLOSE] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDESGLOSE @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_03_22_05_A32100_03_22_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32100_3_Storeds_22_006

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.22.06'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32100_03_22_05_A32100_03_22_06 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32100_03_22_05_A32100_03_22_06 = False
End Function

Private Sub V_32100_3_Storeds_22_006()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETDESGLOSE] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDESGLOSE @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD, @PORTAL=1  '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub
