Attribute VB_Name = "bas_V_31900_3"
Option Explicit

Public Function CodigoDeActualizacion31900_02_12_01A31900_03_13_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Tablas_000
        
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_02_12_01A31900_03_13_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_02_12_01A31900_03_13_00 = False
End Function

Sub V_31900_3_Storeds_000()
    Dim sConsulta As String
        
    sConsulta = "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PEND_NOTIF]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PEND_NOTIF]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PEND_NOTIF] (@CIACOD VARCHAR(50),@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER," & vbCrLf
    sConsulta = sConsulta & "@GRUPOITEM INTEGER,@PROVE VARCHAR(50),@PROCEDEN VARCHAR(500),@GRUPOITEMDEN VARCHAR(500),@EMAIL VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "@PROVEDEN VARCHAR(500),@CONTACTO VARCHAR(500),@USU INT) AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @EMAIL2 AS VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CONTACTO2 AS VARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @USU2 INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CIACOMP INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CIAPROVE INT" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "exec @CIACOMP = SP_SELECT_ID @CIACOD" & vbCrLf
    sConsulta = sConsulta & "SELECT TOP 1 @CIAPROVE=CIA_PROVE FROM REL_CIAS WITH(NOLOCK) WHERE CIA_COMP=@CIACOMP AND COD_PROVE_CIA=@PROVE" & vbCrLf
    sConsulta = sConsulta & "/*Si el email o el contacto vienen como nulos se obtienen de la tabla USU del portal*/" & vbCrLf
    sConsulta = sConsulta & "IF @EMAIL IS NULL OR @CONTACTO IS NULL" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @USU IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SELECT @EMAIL2=EMAIL,@CONTACTO2= NOM + ' ' + APE,@USU2=ID  FROM USU WITH(NOLOCK) WHERE CIA=@CIAPROVE AND ID=@USU" & vbCrLf
    sConsulta = sConsulta & "       IF @EMAIL2 IS NULL OR @CONTACTO2 IS NULL " & vbCrLf
    sConsulta = sConsulta & "            SELECT @EMAIL2=EMAIL, @CONTACTO2 = NOM + ' ' + APE,@USU2=USU.ID  FROM USU WITH(NOLOCK) INNER JOIN CIAS WITH(NOLOCK) ON USU.CIA=CIAS.ID AND USU.ID=CIAS.USUPPAL" & vbCrLf
    sConsulta = sConsulta & "            WHERE CIA=@CIACOMP" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SELECT @EMAIL2=EMAIL, @CONTACTO2 = NOM + ' ' + APE,@USU2=USU.ID  FROM USU WITH(NOLOCK) INNER JOIN CIAS WITH(NOLOCK) ON USU.CIA=CIAS.ID AND USU.ID=CIAS.USUPPAL" & vbCrLf
    sConsulta = sConsulta & "            WHERE CIA=@CIACOMP" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO PROVENOTIF (CIACOMP,ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO,USU) VALUES" & vbCrLf
    sConsulta = sConsulta & "            (@CIACOMP,@ANYO,@GMN1,@PROCE,@PROCEDEN,@GRUPOITEM,@GRUPOITEMDEN,@PROVE,@PROVEDEN,@EMAIL2,@CONTACTO2,@USU2)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @USU IS NULL" & vbCrLf
    sConsulta = sConsulta & "       SELECT TOP 1 @USU=ID  FROM USU WITH(NOLOCK) WHERE CIA=@CIAPROVE AND EMAIL=@EMAIL" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO PROVENOTIF (CIACOMP,ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO,USU) VALUES" & vbCrLf
    sConsulta = sConsulta & "      (@CIACOMP,@ANYO,@GMN1,@PROCE,@PROCEDEN,@GRUPOITEM,@GRUPOITEMDEN,@PROVE,@PROVEDEN,@EMAIL,@CONTACTO,@USU)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "RETURN" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
End Sub

Sub V_31900_3_Tablas_000()
    Dim sConsulta As String
    
    sConsulta = "if not exists (select 1 from syscolumns where id = object_id('PARGEN_INTERNO') and name = 'DATOSUSU')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
    sConsulta = sConsulta & "   DATOSUSU tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_DATOSUSU DEFAULT 1" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists (select 1 from syscolumns where id = object_id('PARGEN_INTERNO') and name = 'DATOSCIA')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
    sConsulta = sConsulta & "   DATOSCIA tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_DATOSCIA DEFAULT 1" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists (select 1 from syscolumns where id = object_id('PARGEN_INTERNO') and name = 'ACTIVCIA')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
    sConsulta = sConsulta & "   ACTIVCIA tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_ACTIVCIA DEFAULT 1" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists (select 1 from syscolumns where id = object_id('PARGEN_INTERNO') and name = 'ADJUNCIA')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
    sConsulta = sConsulta & "   ADJUNCIA tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_ADJUNCIA DEFAULT 1" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
    sConsulta = "if not exists (select 1 from syscolumns where id = object_id('PARGEN_INTERNO') and name = 'ADMINUSU')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
    sConsulta = sConsulta & "   ADMINUSU tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_ADMINUSU DEFAULT 1" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_03_13_00A31900_03_13_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_00A31900_03_13_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_00A31900_03_13_01 = False
End Function

Sub V_31900_3_Storeds_001()
    Dim sConsulta As String
        
    sConsulta = "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FSPM_GETINSTDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE] @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL , @VERSION INT, @NUEVO_WORKFLOW INT=0 , @DEFECTO TINYINT= 0, @VERSION_BD_CERT INT =0 AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CIAPROVE AS INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA,@CIAPROVE=RC.CIA_PROVE FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1 " & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_31600 @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION, @DEFECTO=@DEFECTO '" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION, @DEFECTO=@DEFECTO, @VERSION_BD_CERT=@VERSION_BD_CERT '   " & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50), @VERSION INT, @DEFECTO TINYINT, @VERSION_BD_CERT INT ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS, @VERSION = @VERSION , @DEFECTO=@DEFECTO,@VERSION_BD_CERT=@VERSION_BD_CERT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTINVISIBLEFIELDS]"
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTINVISIBLEFIELDS] @CIA INT,  @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL,@NUEVO_WORKFL TINYINT=0    AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL ',  @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    

    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINVISIBLEFIELDS]"
    ExecuteSQL gRDOCon, sConsulta

    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS] @CIA INT, @ID INT  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINVISIBLEFIELDS @ID=@ID '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_13_01A31900_03_13_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_01A31900_03_13_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_01A31900_03_13_02 = False
End Function

Sub V_31900_3_Storeds_002()
    Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSCALC]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADFIELDSCALC] @CIA INT,@FORMULARIO INT,  @IDI VARCHAR(50)='SPA', @SOLICITUD INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADFIELDSCALC @IDI = @IDI, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(20), @SOLICITUD  INT ', @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_03_13_02A31900_03_13_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_02A31900_03_13_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_02A31900_03_13_03 = False
End Function

Sub V_31900_3_Storeds_003()
    Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFE_ATRIB]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB] @ID int, @AMBITO tinyint, @GRUPO varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EL CAMBIO QUE DEVUELVE ES EL CAMBIO DE LA MONEDA EN QUE SE GUARD� LA OFERTA RESPECTO A LA MONEDA DEL PROCESO" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL, G.IMPPARCIAL , @CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE ID= @ID" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO ,IP.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_GR_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.ITEM,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO ,IP.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_13_03A31900_03_13_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_03A31900_03_13_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_03A31900_03_13_04 = False
End Function

Sub V_31900_3_Storeds_004()
    Dim sConsulta As String
        
'Eliminar tres storeds
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CAMBIAR_AMBITO_ATRIB_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CAMBIAR_AMBITO_ATRIB_OFE]"
ExecuteSQL gRDOCon, sConsulta
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_VALOR_ATRIB_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_VALOR_ATRIB_OFE]"
ExecuteSQL gRDOCon, sConsulta
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_NUM_OFERTAS_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_NUM_OFERTAS_ATRIB]"
ExecuteSQL gRDOCon, sConsulta
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSP_CAMBIAR_AMBITO_ATRIB_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSP_CAMBIAR_AMBITO_ATRIB_OFE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_CAMBIAR_AMBITO_ATRIB_OFE] (@AMBITOANTIGUO INTEGER, @AMBITONUEVO INTEGER, @ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @IDATRIB INTEGER, @GRUPO VARCHAR(50)='') AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Cogemos el registro del atributo a eliminar del �mbito antiguo." & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(ID) FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @AMBITOANTIGUO=1 -- Ambito proceso    " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @AMBITONUEVO=2 -- Ambito grupo" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO OFE_GR_ATRIB (ID,GRUPO,ATRIB_ID,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "             SELECT P.ID,G.GRUPO,O.ATRIB_ID,O.VALOR_NUM,O.VALOR_TEXT,O.VALOR_FEC,O.VALOR_BOOL FROM PROCE_OFE P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN GRUPO_OFE G WITH (NOLOCK) ON G.ID=P.ID" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN OFE_ATRIB O WITH (NOLOCK) ON G.ID=O.ID and O.ATRIB_ID=@IDATRIB" & vbCrLf
sConsulta = sConsulta & "              WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITONUEVO=3 -- Ambito ITEM" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO OFE_ITEM_ATRIB (ID,ITEM,ATRIB_ID,GRUPO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                 SELECT P.ID,G.ITEM,O.ATRIB_ID,G.GRUPO,O.VALOR_NUM,O.VALOR_TEXT,O.VALOR_FEC,O.VALOR_BOOL FROM PROCE_OFE P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN ITEM_OFE G WITH (NOLOCK) ON P.ID=G.ID" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN OFE_ATRIB O WITH (NOLOCK) ON G.ID=O.ID and O.ATRIB_ID=@IDATRIB" & vbCrLf
sConsulta = sConsulta & "                  WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE OFE_ATRIB FROM OFE_ATRIB A WITH (NOLOCK) INNER JOIN PROCE_OFE P WITH (NOLOCK) ON A.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND A.ATRIB_ID=@IDATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "  IF @AMBITOANTIGUO=2 -- Ambito grupo" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @AMBITONUEVO=3 -- Ambito ITEM" & vbCrLf
sConsulta = sConsulta & "         IF @GRUPO='' " & vbCrLf
sConsulta = sConsulta & "              INSERT INTO OFE_ITEM_ATRIB (ID,ITEM,ATRIB_ID,GRUPO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                 SELECT P.ID,G.ITEM,O.ATRIB_ID,G.GRUPO,O.VALOR_NUM,O.VALOR_TEXT,O.VALOR_FEC,O.VALOR_BOOL FROM PROCE_OFE P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN ITEM_OFE G WITH (NOLOCK) ON P.ID=G.ID" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN OFE_GR_ATRIB O WITH (NOLOCK) ON G.ID=O.ID and O.ATRIB_ID=@IDATRIB " & vbCrLf
sConsulta = sConsulta & "                  WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO OFE_ITEM_ATRIB (ID,ITEM,ATRIB_ID,GRUPO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL) " & vbCrLf
sConsulta = sConsulta & "                 SELECT P.ID,G.ITEM,O.ATRIB_ID,G.GRUPO,O.VALOR_NUM,O.VALOR_TEXT,O.VALOR_FEC,O.VALOR_BOOL FROM PROCE_OFE P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN ITEM_OFE G WITH (NOLOCK) ON P.ID=G.ID" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN OFE_GR_ATRIB O WITH (NOLOCK) ON G.ID=O.ID and O.ATRIB_ID=@IDATRIB AND O.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                  WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     DELETE OFE_GR_ATRIB FROM OFE_GR_ATRIB A WITH (NOLOCK) INNER JOIN PROCE_OFE P WITH (NOLOCK) ON A.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND A.ATRIB_ID=@IDATRIB         " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSP_ELIMINAR_VALOR_ATRIB_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSP_ELIMINAR_VALOR_ATRIB_OFE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_ELIMINAR_VALOR_ATRIB_OFE] (@AMBITOANTIGUO INTEGER, @ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @IDATRIB INTEGER, @GRUPO VARCHAR(50)='') AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @AMBITOANTIGUO=1 -- Ambito proceso" & vbCrLf
sConsulta = sConsulta & "            DELETE OFE_ATRIB FROM OFE_ATRIB A WITH (NOLOCK) INNER JOIN PROCE_OFE P WITH (NOLOCK) ON A.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND A.ATRIB_ID = @IDATRIB " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "      IF @AMBITOANTIGUO=2 -- Ambito grupo" & vbCrLf
sConsulta = sConsulta & "            DELETE OFE_GR_ATRIB FROM OFE_GR_ATRIB A WITH (NOLOCK) INNER JOIN PROCE_OFE P WITH (NOLOCK) ON A.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND ATRIB_ID = @IDATRIB " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @AMBITOANTIGUO=3 -- Ambito item" & vbCrLf
sConsulta = sConsulta & "            IF @GRUPO='' -- Para los items de todos los grupos del proceso." & vbCrLf
sConsulta = sConsulta & "               DELETE OFE_ITEM_ATRIB FROM OFE_ITEM_ATRIB A WITH (NOLOCK) INNER JOIN PROCE_OFE P WITH (NOLOCK) ON A.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND ATRIB_ID = @IDATRIB " & vbCrLf
sConsulta = sConsulta & "            ELSE -- Para los items de un grupo determinado del proceso." & vbCrLf
sConsulta = sConsulta & "               DELETE OFE_ITEM_ATRIB FROM OFE_ITEM_ATRIB A WITH (NOLOCK) INNER JOIN PROCE_OFE P WITH (NOLOCK) ON A.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND ATRIB_ID = @IDATRIB AND A.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "-- Tambi�n se elimina de OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "      IF @GRUPO='' -- Se elimina ese atributo para todos los grupos." & vbCrLf
sConsulta = sConsulta & "            DELETE OFE_GR_COSTESDESC FROM OFE_GR_COSTESDESC A WITH (NOLOCK) INNER JOIN PROCE_OFE P WITH (NOLOCK) ON A.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND ATRIB_ID = @IDATRIB " & vbCrLf
sConsulta = sConsulta & "      ELSE -- Se elimina ese atributo para grupo concreto." & vbCrLf
sConsulta = sConsulta & "            DELETE OFE_GR_COSTESDESC FROM OFE_GR_COSTESDESC A WITH (NOLOCK) INNER JOIN PROCE_OFE P WITH (NOLOCK) ON A.ID=P.ID WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND ATRIB_ID = @IDATRIB AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_CONTRATO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_CONTRATO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_CONTRATO] @CIA INT, @ID INT, @IDIOMA VARCHAR(3) = 'SPA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_CONTRATO @ID=@ID,@IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,  @IDIOMA VARCHAR(50)', @ID=@ID,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_IDACCIONGUARDAR_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_IDACCIONGUARDAR_ROL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_IDACCIONGUARDAR_ROL] @BLOQUE INT,@ROL INT,@CIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_IDACCIONGUARDAR_ROL @BLOQUE=@BLOQUE,@ROL=@ROL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,  N'@BLOQUE INT,@ROL INT', @BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO] @CIA INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO @INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA  INT, @IDI VARCHAR(50)', @INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LISTADOS_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LISTADOS_ROL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LISTADOS_ROL] @CIA INT,@BLOQUE INT,@ROL INT , @IDI VARCHAR(3),@COPIA TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LISTADOS_ROL @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT,@ROL INT ,@IDI VARCHAR(3),@COPIA TINYINT', @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES] @CIA INT, @INSTANCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_NUM_VINCULACIONES @INSTANCIA =@INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @INSTANCIA INT', @INSTANCIA =@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSP_DEVOLVER_NUM_OFERTAS_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSP_DEVOLVER_NUM_OFERTAS_ATRIB]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_NUM_OFERTAS_ATRIB] (@AMBITO int, @ANYO int, @GMN1 varchar(50),@PROCE int,@ATRIBID int ) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO=1 --Ambito proceso" & vbCrLf
sConsulta = sConsulta & "   SELECT COUNT(*) FROM PROCE_OFE PO WITH (NOLOCK) INNER JOIN OFE_ATRIB OA WITH (NOLOCK) ON PO.ID=OA.ID " & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OA.ATRIB_ID=@ATRIBID and OA.VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO=2 --Ambito grupo" & vbCrLf
sConsulta = sConsulta & "   SELECT COUNT(*) FROM PROCE_OFE PO WITH (NOLOCK) INNER JOIN OFE_GR_ATRIB OA WITH (NOLOCK) ON PO.ID=OA.ID " & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OA.ATRIB_ID=@ATRIBID and OA.VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @AMBITO=3 --Ambito item" & vbCrLf
sConsulta = sConsulta & "   SELECT COUNT(*) FROM PROCE_OFE PO WITH (NOLOCK) INNER JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON PO.ID=OA.ID " & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OA.ATRIB_ID=@ATRIBID and OA.VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_13_04A31900_03_13_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_005
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_04A31900_03_13_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_04A31900_03_13_05 = False
End Function

Sub V_31900_3_Storeds_005()
    Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GUARDARCOMENTARIOADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GUARDARCOMENTARIOADJUNTO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GUARDARCOMENTARIOADJUNTO] @CIA INT, @ID INT,  @TIPO INT =0, @INSTANCIA INT =0, @COMENT VARCHAR(4000) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GUARDARCOMENTARIOADJUNTO @ID=@ID,@TIPO=@TIPO,@INSTANCIA=@INSTANCIA ,@COMENT=@COMENT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@TIPO INT =0, @INSTANCIA INT =0, @COMENT VARCHAR(4000) = NULL',@ID =@ID,@TIPO=@TIPO,@INSTANCIA=@INSTANCIA,@COMENT=@COMENT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_13_05A31900_03_13_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_006
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_05A31900_03_13_06 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_05A31900_03_13_06 = False
End Function

Sub V_31900_3_Storeds_006()
    Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_CREATE_NEW_CERTIFICADO_PREV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_CREATE_NEW_CERTIFICADO_PREV]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_CREATE_NEW_CERTIFICADO_PREV] @CIA INT, @TIPO_CERTIF INT, @PROVE VARCHAR(50), @USUNOM VARCHAR(500) AS" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "   ''' Revisado por: Jbg. Fecha: 15/06/2011" & vbCrLf
sConsulta = sConsulta & "    '*** Descripci�n: Funci�n que crea la primera versi�n del certificado autom�tico" & vbCrLf
sConsulta = sConsulta & "    '*** Par�metros de entrada: " & vbCrLf
sConsulta = sConsulta & "    '***    @TIPO_CERTIF: Tipo de Certificado (o Solicitud)" & vbCrLf
sConsulta = sConsulta & "    '***    @PROVE: C�digo de proveedor de GS" & vbCrLf
sConsulta = sConsulta & "    '***    @USUNOM: Usuario de proveedor de GS" & vbCrLf
sConsulta = sConsulta & "    '*** Par�metros de salida: No tiene" & vbCrLf
sConsulta = sConsulta & "    '*** Llamada desde: El procedimiento GuardarSinWorkflow de GuardarInstancia.aspx del PMPortalWeb" & vbCrLf
sConsulta = sConsulta & "    '*** Tiempo m�ximo: 0 sec            " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_CREATE_TEMP_NEW_CERTIFICADO_PREV @TIPO_CERTIF=@TIPO_CERTIF,@PROVE=@PROVE,@USUNOM=@USUNOM'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO_CERTIF INT, @PROVE VARCHAR(50), @USUNOM VARCHAR(500)'" & vbCrLf
sConsulta = sConsulta & ",@TIPO_CERTIF=@TIPO_CERTIF,@PROVE=@PROVE,@USUNOM=@USUNOM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_03_13_06A31900_03_13_07() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_007
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_06A31900_03_13_07 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_06A31900_03_13_07 = False
End Function

Sub V_31900_3_Storeds_007()
    Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "/****** Object:  StoredProcedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]    Script Date: 11/08/2010 10:45:10 ******/" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_OFERTA_PROVE] @CIA_COMP INT = NULL, @ANYO INT = NULL, @GMN1 VARCHAR(20) = NULL, @PROCE INT = NULL, @PROVE VARCHAR(50) = NULL, @ID INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- <summary>" & vbCrLf
sConsulta = sConsulta & "-- Carga la oferta de los grupos de un proveedor para un determinado proceso" & vbCrLf
sConsulta = sConsulta & "-- </summary>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@CIA_COMP"">Comprador</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@ANYO"">A�o del proceso</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@GMN1"">Gmn1 del proceso</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@PROCE"">Id del proceso</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@PROVE"">Proveedor</param>" & vbCrLf
sConsulta = sConsulta & "-- <param name=""@ID"">Oferta que queremos cargar</param>" & vbCrLf
sConsulta = sConsulta & "-- <return> Llamado desde GS CProceso-->CargarUltimaOfertaProveedor, Tiempo m�ximo: 0,1</return>" & vbCrLf
sConsulta = sConsulta & "-- <revisado>JVS 17/06/2011</revisado>" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @ID = ID " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP = @CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   AND ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TOTALADJUNTOS = ISNULL(SUM(DATASIZE),0) " & vbCrLf
sConsulta = sConsulta & "  FROM" & vbCrLf
sConsulta = sConsulta & "    (SELECT ISNULL(SUM(DATASIZE), 0) DATASIZE" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_OFE_ADJUN WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
sConsulta = sConsulta & "       FROM OFE_GR_ADJUN WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
sConsulta = sConsulta & "       FROM OFE_ITEM_ADJUN WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID) A" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "--SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "--  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "-- WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ID, PO.CIA_COMP, PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.CIA_PROVE, PO.USU, PO.FECREC, PO.FECVAL, PO.MON,PO.CAMBIO, PO.OBS, PO.OBSADJUN, PO.NUMOBJ, PO.ADJUN,  @TOTALADJUNTOS TOTALADJUNTOS, PO.IMPORTE IMPORTEOFERTA, U.NOM + ' ' + U.APE NOMUSU, PO.ENVIANDOSE, PO.IMPORTE_BRUTO IMPORTEBRUTO" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN USU U WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            ON U.CIA = PO.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "           AND U.COD = PO.USU" & vbCrLf
sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFE_ATRIB]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB] @ID int, @AMBITO tinyint, @GRUPO varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Devuelve los atributos de la oferta                  ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :                                       ******/" & vbCrLf
sConsulta = sConsulta & "/******                        - @ID: Id de la oferta                                    ******/" & vbCrLf
sConsulta = sConsulta & "/******                        - @AMBITO: �mbito de los atributos                                    ******/" & vbCrLf
sConsulta = sConsulta & "/******                        - @GRUPO: Grupo (opcional)                                    ******/" & vbCrLf
sConsulta = sConsulta & "/******       Revisado :  JVS 17/06/2011                                           ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EL CAMBIO QUE DEVUELVE ES EL CAMBIO DE LA MONEDA EN QUE SE GUARD� LA OFERTA RESPECTO A LA MONEDA DEL PROCESO" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL, G.IMPPARCIAL , @CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE ID= @ID" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO ,IP.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_GR_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.ITEM,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO ,IP.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "/****** Object:  StoredProcedure [dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO]    Script Date: 11/08/2010 10:44:31 ******/" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO] @ID INT, @COSTEDESC int = null AS" & vbCrLf
sConsulta = sConsulta & "--''' <summary>" & vbCrLf
sConsulta = sConsulta & "--'''  Devuelve los importes parciales de los grupos de la oferta" & vbCrLf
sConsulta = sConsulta & "--''' </summary>" & vbCrLf
sConsulta = sConsulta & "--''' <param name=""ID"">Id de oferta</param>" & vbCrLf
sConsulta = sConsulta & "--''' <param name=""COSTEDESC"">Coste/Descuento</param>" & vbCrLf
sConsulta = sConsulta & "--''' <returns> </returns>" & vbCrLf
sConsulta = sConsulta & "--''' <revisado>JVS 17/06/2011</revisado>" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT GRUPO, ATRIB_ID COSTEDESC, IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & " FROM OFE_GR_COSTESDESC WITH (NOLOCK)        " & vbCrLf
sConsulta = sConsulta & "WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  AND ATRIB_ID = CASE WHEN @COSTEDESC IS NULL THEN ATRIB_ID ELSE @COSTEDESC END" & vbCrLf
sConsulta = sConsulta & "ORDER BY GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31900_03_13_07A31900_03_13_08() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_008
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.08'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_07A31900_03_13_08 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_07A31900_03_13_08 = False
End Function


Sub V_31900_3_Storeds_008()
    Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_COMPROBAR_PRECIOS_ITEMS_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_COMPROBAR_PRECIOS_ITEMS_GRUPO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_COMPROBAR_PRECIOS_ITEMS_GRUPO] @ID INT, @GRUPO VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT COUNT(ID) as NUMITEMS" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND (PRECIO IS NOT NULL OR PRECIO2 IS NOT NULL OR PRECIO3 IS NOT NULL)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_13_08A31900_03_13_09() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_009
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.09'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_08A31900_03_13_09 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_08A31900_03_13_09 = False
End Function

Sub V_31900_3_Storeds_009()
    Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD] @CIA INT, @ID_ADJUN INT OUTPUT,@PER NVARCHAR(50) =NULL,@PROVE NVARCHAR(50),@NOM NVARCHAR(100), @ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000) AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000) " & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_SAVE_ADJUN_CONTRATO_WIZARD @ID_ADJUN=@ID_ADJUN OUTPUT,@PER=@PER,@PROVE=@PROVE, @NOM=@NOM,@ID_CONTRATO=@ID_CONTRATO,@COMENT=@COMENT '"
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID_ADJUN INT OUTPUT, @PER NVARCHAR(50),@PROVE NVARCHAR(50),@NOM NVARCHAR(100),@ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000) ', @ID_ADJUN=@ID_ADJUN OUTPUT,@PER=@PER,@PROVE=@PROVE, @NOM=@NOM,@ID_CONTRATO=@ID_CONTRATO,@COMENT=@COMENT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

  
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETADJUNTO_CONTRATO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETADJUNTO_CONTRATO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO] @CIA INT, @ID INT =NULL, @ID_CONTRATO INT =NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000) " & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTO_CONTRATO @ID=@ID,@ID_CONTRATO=@ID_CONTRATO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID  INT, @ID_CONTRATO INT ', @ID=@ID,@ID_CONTRATO=@ID_CONTRATO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
  
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LEER_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LEER_ADJUNTO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LEER_ADJUNTO] @CIA INT, @ID INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000) " & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LEER_ADJUNTO @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT = NULL', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_03_13_09A31900_03_13_10() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_010
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.10'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_09A31900_03_13_10 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_09A31900_03_13_10 = False
End Function

Sub V_31900_3_Storeds_010()
Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS] @CIA INT, @ADJUNTOSACT NVARCHAR(50), @ADJUNTOSNEW NVARCHAR(50),@LAST_ADJUNTO INT OUTPUT, @DEFECTO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_CREAR_ADJUNTOS_COPIADOS @ADJUNTOSACT =@ADJUNTOSACT ,@ADJUNTOSNEW =@ADJUNTOSNEW,@LAST_ADJUNTO=@LAST_ADJUNTO OUTPUT,@DEFECTO=@DEFECTO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ADJUNTOSACT NVARCHAR(50), @ADJUNTOSNEW NVARCHAR(50),@LAST_ADJUNTO INT OUTPUT, @DEFECTO INT', @ADJUNTOSACT =@ADJUNTOSACT ,@ADJUNTOSNEW =@ADJUNTOSNEW,@LAST_ADJUNTO=@LAST_ADJUNTO OUTPUT,@DEFECTO=@DEFECTO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_03_13_10A31900_03_13_11() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_011
    V_31900_3_Tablas_011
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.11'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_10A31900_03_13_11 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_10A31900_03_13_11 = False
End Function

Sub V_31900_3_Storeds_011()
Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE    PROCEDURE [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL] " & vbCrLf
sConsulta = sConsulta & "   @SDEN VARCHAR(8)," & vbCrLf
sConsulta = sConsulta & "   @SBUSCAR VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @SCOMP INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA= 'x.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL = '" & vbCrLf
sConsulta = sConsulta & "SELECT x.ID ACT1, NULL ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA + ' DEN ,COUNT(J.CIA) AS PROVE, CODACT1, NULL CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT1 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.CIA,PP.ACT1 " & vbCrLf
sConsulta = sConsulta & "FROM (SELECT CIA, ACT1 FROM CIAS_ACT1 WITH (NOLOCK) UNION SELECT CIA, ACT1 FROM CIAS_ACT2 WITH (NOLOCK) UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT CIA, ACT1 FROM CIAS_ACT3 WITH (NOLOCK) UNION SELECT CIA, ACT1 FROM CIAS_ACT4 WITH (NOLOCK) UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT CIA, ACT1 FROM CIAS_ACT5 WITH (NOLOCK)) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT1=x.ID " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%'' " & vbCrLf
sConsulta = sConsulta & "GROUP BY   x.ID, CODACT1, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1, x.ID ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT2 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.CIA,PP.ACT1,PP.ACT2 " & vbCrLf
sConsulta = sConsulta & "FROM (SELECT CIA, ACT1, ACT2 FROM CIAS_ACT2 WITH (NOLOCK) UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT3 WITH (NOLOCK) UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT CIA, ACT1, ACT2 FROM CIAS_ACT4 WITH (NOLOCK) UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT5 WITH (NOLOCK)) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=PP.CIA) J ON J.ACT2=x.ID  AND J.ACT1=x.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 WITH (NOLOCK) ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY  x.ACT1, x.ID, CODACT1, CODACT2,' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ID ACT3, NULL ACT4, NULL ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT3 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.CIA,PP.ACT1,PP.ACT2,PP.ACT3 " & vbCrLf
sConsulta = sConsulta & "FROM (SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT3 WITH (NOLOCK) UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT4 WITH (NOLOCK) UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT5 WITH (NOLOCK)) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT3=x.ID AND J.ACT2=x.ACT2 AND J.ACT1=x.ACT1 " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ID, CODACT1, CODACT2, CODACT3, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ACT3, x.ID ACT4, NULL ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3,CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT4 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.CIA,PP.ACT1,PP.ACT2,PP.ACT3,PP.ACT4 " & vbCrLf
sConsulta = sConsulta & "FROM (SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT4 WITH (NOLOCK) UNION SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT5 WITH (NOLOCK)) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA)  J ON J.ACT4=x.ID AND J.ACT3=x.ACT3 AND J.ACT2=x.ACT2 AND J.ACT1=x.ACT1  " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ACT3,x.ID, CODACT1, CODACT2, CODACT3, CODACT4, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ACT3, x.ACT4, x.ID ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3, CODACT4 , CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT5 x" & vbCrLf
sConsulta = sConsulta & "LEFT  JOIN CIAS_ACT5 J WITH (NOLOCK) ON J.ACT5=x.ID AND J.ACT4=x.ACT4 AND J.ACT3=x.ACT3 AND J.ACT2=x.ACT2  AND J.ACT1=x.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=J.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=J.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA + ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ACT3,x.ACT4,x.ID, CODACT1, CODACT2, CODACT3, CODACT4, CODACT5, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "ORDER by 1, 2, 3, 4, 5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRANSFER_ADJUN_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRANSFER_ADJUN_OFE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_TRANSFER_ADJUN_OFE] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
sConsulta = sConsulta & "   @IDADJUN INT," & vbCrLf
sConsulta = sConsulta & "   @INIT INT," & vbCrLf
sConsulta = sConsulta & "   @SIZE INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM ADJUN WITH (NOLOCK) WHERE CIA_COMP=@CIA AND ID=@IDADJUN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  ADJUN.DATA @textpointer @INIT   @LSIZE HOLDLOCK" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROVE_ESP]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROVE_ESP] " & vbCrLf
sConsulta = sConsulta & "   @IDPROVE int output, " & vbCrLf
sConsulta = sConsulta & "   @IDESP int = NULL, " & vbCrLf
sConsulta = sConsulta & "   @CODPROVE varchar(50) = null " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @IDPROVE is null" & vbCrLf
sConsulta = sConsulta & "   select @IDPROVE = ID FROM CIAS WITH (NOLOCK) WHERE COD = @CODPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDESP = NULL" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact , datalength(data) TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact, datalength(data)  TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "   AND ID = @IDESP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRANSFER_PROVE_ESP]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_TRANSFER_PROVE_ESP] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
sConsulta = sConsulta & "   @ESP INT," & vbCrLf
sConsulta = sConsulta & "   @INIT INT," & vbCrLf
sConsulta = sConsulta & "   @SIZE INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM CIAS_ESP WITH (NOLOCK) WHERE CIA=@CIA AND ID=@ESP " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  CIAS_ESP.DATA @textpointer @INIT  @LSIZE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSP_LOGIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSP_LOGIN]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_LOGIN] @CIA VARCHAR(50), @COD VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
sConsulta = sConsulta & "       CIAS.USUPPAL AS USUPPAL, CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
sConsulta = sConsulta & "       USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
sConsulta = sConsulta & "       USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "       USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "       USU.DATEFMT DATEFMT, USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "       IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
sConsulta = sConsulta & "       USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA , MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU," & vbCrLf
sConsulta = sConsulta & "       CIAS.DEN AS CIADEN, CIAS.NIF" & vbCrLf
sConsulta = sConsulta & "  FROM USU WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN USU_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               ON USU.ID=USU_PORT.USU" & vbCrLf
sConsulta = sConsulta & "              AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "              AND USU.CIA=USU_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               ON USU.CIA=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN CIAS_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               ON USU.CIA=CIAS_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "              AND CIAS_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN MON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               ON CIAS.MON=MON.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN MON MON2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               ON USU.MON=MON2.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN IDI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & " WHERE USU.COD=@COD" & vbCrLf
sConsulta = sConsulta & "   AND CIAS.COD=@CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ACCIONES_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ACCIONES_ROL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_ACCIONES_ROL] @CIA INT,@BLOQUE INT,@ROL INT, @IDI VARCHAR(50),@COPIA TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ACCIONES_ROL @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @ROL INT, @IDI VARCHAR(50), @COPIA TINYINT ',@BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ACT_REL_CIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ACT_REL_CIAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ACT_REL_CIAS](@COMP VARCHAR(50),@PROVE VARCHAR(50),@ACTIVAS INTEGER,@NUEVAS INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP AS INT" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WITH (NOLOCK) WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET SOLIC_ACT=@ACTIVAS,SOLIC_NUE=@NUEVAS " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP=@ID_COMP " & vbCrLf
sConsulta = sConsulta & "   AND COD_PROVE_CIA=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ACTUALIZAR_EN_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ACTUALIZAR_EN_PROCESO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ACTUALIZAR_EN_PROCESO] @CIA INT, @ID INT, @BLOQUE INT = 0, @EN_PROCESO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ACTUALIZAR_EN_PROCESO @ID=@ID, @BLOQUE=@BLOQUE,@EN_PROCESO=@EN_PROCESO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @BLOQUE INT, @EN_PROCESO INT', @ID=@ID,@BLOQUE=@BLOQUE,@EN_PROCESO=@EN_PROCESO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARBOLPRESUPUESTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARBOLPRESUPUESTOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ARBOLPRESUPUESTOS] @CIA INT, @TIPO INTEGER, @IDI VARCHAR(50), @ANYO INTEGER, @UON1 AS VARCHAR(50) = NULL, @UON2 AS VARCHAR(50) = NULL, @UON3 AS VARCHAR(50) = NULL, @CODIGO AS VARCHAR(50) = NULL, @DENOMINACION AS VARCHAR(50) = NULL, @UONVacia AS TINYINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ARBOLPRESUPUESTOS @TIPO=@TIPO, @IDI =@IDI , @ANYO =@ANYO , @UON1 =@UON1 , @UON2 =@UON2 , @UON3 =@UON3, @CODIGO=@CODIGO, @DENOMINACION=@DENOMINACION, @UONVacia= @UONVacia '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INTEGER, @IDI VARCHAR(50), @ANYO INTEGER, @UON1 AS VARCHAR(50) = NULL, @UON2 AS VARCHAR(50) = NULL, @UON3 AS VARCHAR(50) = NULL, @CODIGO AS VARCHAR(50) = NULL, @DENOMINACION AS VARCHAR(50) = NULL, @UONVacia TINYINT', @TIPO=@TIPO, @IDI =@IDI , @ANYO =@ANYO , @UON1 =@UON1 , @UON2 =@UON2 , @UON3 =@UON3, @CODIGO=@CODIGO, @DENOMINACION=@DENOMINACION, @UONVacia=@UONVacia" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ARTICULOS] @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA', @INSTANCIAMONEDA VARCHAR(3)='EUR',@CARGAR_ULT_ADJ TINYINT=0 ,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL, @CARGAR_ORG TINYINT=0  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT,@CARGAR_ORG=@CARGAR_ORG'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL, @CARGAR_ORG TINYINT',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT,@CARGAR_ORG=@CARGAR_ORG" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS_GMN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS_GMN]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ARTICULOS_GMN] @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA', @INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT AS    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS_GMN @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS] @CIA INT, @ADJUNTOSACT NVARCHAR(50), @ADJUNTOSNEW NVARCHAR(50),@LAST_ADJUNTO INT OUTPUT, @DEFECTO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_CREAR_ADJUNTOS_COPIADOS @ADJUNTOSACT =@ADJUNTOSACT ,@ADJUNTOSNEW =@ADJUNTOSNEW,@LAST_ADJUNTO=@LAST_ADJUNTO OUTPUT,@DEFECTO=@DEFECTO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ADJUNTOSACT NVARCHAR(50), @ADJUNTOSNEW NVARCHAR(50),@LAST_ADJUNTO INT OUTPUT, @DEFECTO INT', @ADJUNTOSACT =@ADJUNTOSACT ,@ADJUNTOSNEW =@ADJUNTOSNEW,@LAST_ADJUNTO=@LAST_ADJUNTO OUTPUT,@DEFECTO=@DEFECTO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEFTABLAEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEFTABLAEXTERNA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEFTABLAEXTERNA] @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEFTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DESTINOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DESTINOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DESTINOS] @CIA INT, @PER VARCHAR(50) =NULL, @COD VARCHAR(50) =NULL, @IDI VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DESTINOS @PER =@PER ,@COD =@COD, @IDI=@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PER VARCHAR(50) =NULL, @COD VARCHAR(50) =NULL,@IDI VARCHAR(20)', @PER =@PER ,@COD =@COD,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLUCION_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLUCION_INSTANCIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLUCION_INSTANCIA] @CIA INT,  @CIAPROVE INT, @INSTANCIA INT,@PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT ', @INSTANCIA=@INSTANCIA, @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_COMENT_ESTADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_COMENT_ESTADO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_COMENT_ESTADO] @CIA INT,@ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DEVOLVER_COMENT_ESTADO @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_CONTACTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_CONTACTOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_CONTACTOS] @CIA INT,@PROVE VARCHAR(50), @SOLOPORTAL TINYINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_DEVOLVER_CONTACTOS_QA @PROVE=@PROVE,@SOLOPORTAL=@SOLOPORTAL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE  VARCHAR(50), @SOLOPORTAL TINYINT ', @PROVE=@PROVE,@SOLOPORTAL=@SOLOPORTAL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_DIAGBLOQUES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_DIAGBLOQUES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_DIAGBLOQUES] @CIA INT,@INSTANCIA INT, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_DIAGBLOQUES @INSTANCIA=@INSTANCIA,@IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDIOMA VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_DIAGENLACES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_DIAGENLACES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_DIAGENLACES] @CIA INT,@BLOQUE INT, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_DIAGENLACES @BLOQUE=@BLOQUE,@IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @IDIOMA VARCHAR(50) ', @BLOQUE=@BLOQUE,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ENLACESACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ENLACESACCION]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ENLACESACCION] @CIA INT, @ACCION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'SELECT ENLACE FROM ' +@FSGS +'INSTANCIA_CAMINO WITH (NOLOCK) WHERE ACCION = @ACCION'" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL, N'@ACCION INT', @ACCION =@ACCION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL] @CIA INT,@INSTANCIA INT , @IDI VARCHAR(50) , @PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE  @PROVEGS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPA_ACTUAL @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50),@PROVE VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PROVE=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO] @CIA INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO @INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA  INT, @IDI VARCHAR(50)', @INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO] @CIA INT,@BLOQUE INT , @IDI VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPAS_BLOQUEO @BLOQUE=@BLOQUE,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @IDI VARCHAR(50) ', @BLOQUE=@BLOQUE,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA] @CIA INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_HISTORICO_INSTANCIA @INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES] @CIA INT, @INSTANCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_NUM_VINCULACIONES @INSTANCIA =@INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @INSTANCIA INT', @INSTANCIA =@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLVER_UO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLVER_UO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_UO] @CIA INT,@IDI VARCHAR(50) ,@USU VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DEVOLVER_UO @IDI=@IDI, @USU=@USU'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50) ,@USU VARCHAR(50)', @IDI=@IDI, @USU=@USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ESTADOS_NOCONF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ESTADOS_NOCONF]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ESTADOS_NOCONF] @CIA INT,@INSTANCIA INT , @DEN VARCHAR(50)=null,  @IDI AS VARCHAR(4)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ESTADOS_NOCONF @INSTANCIA = @INSTANCIA, @DEN=@DEN , @IDI=@IDI '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT , @DEN VARCHAR(50)=null ,@IDI VARCHAR(4)',  @INSTANCIA = @INSTANCIA , @DEN=@DEN,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_EXECREMOTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_EXECREMOTO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_EXECREMOTO] @CIA INT, @STMT NTEXT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'SP_EXECUTESQL @STMT = @STMT '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@STMT NTEXT', @STMT =@STMT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_FIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_FIELDSCALC]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_FIELDSCALC] @CIA INT, @ID INT, @CASO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos los campos calculados del grupo" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_FIELDSCALC @ID=@ID,@CASO =@CASO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@ID AS INT, @CASO AS INT', @ID=@ID,@CASO=@CASO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_FORMASPAGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_FORMASPAGO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_FORMASPAGO] @CIA INT,  @COD VARCHAR(50) =NULL, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_FORMASPAGO @COD =@COD,@IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =NULL,@IDIOMA VARCHAR(50)',@COD =@COD,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_CONTRATO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_CONTRATO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GET_CONTRATO] @CIA INT, @ID INT, @IDIOMA VARCHAR(3) = 'SPA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_CONTRATO @ID=@ID,@IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,  @IDIOMA VARCHAR(50)', @ID=@ID,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_PRECONDICIONES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_PRECONDICIONES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GET_PRECONDICIONES] @CIA INT, @BSOLICITUD BIT, @IDACC INTEGER, @IDI VARCHAR(3) = 'SPA', @INSTANCIA INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_PRECONDICIONES @BSOLICITUD=@BSOLICITUD,@IDACC=@IDACC,@IDI=@IDI,@INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BSOLICITUD BIT, @IDACC INTEGER, @IDI VARCHAR(3), @INSTANCIA INT ', @BSOLICITUD=@BSOLICITUD,@IDACC=@IDACC,@IDI=@IDI,@INSTANCIA=@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE] @CIA INT,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_SOLICITUDES_PDTES_PROVE @PRV=@PRV'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50)', @PRV=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETADJUN]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETADJUN] @CIA INT, @TIPO INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUN @TIPO =@TIPO, @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @TIPO INT, @ID INT', @TIPO =@TIPO, @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETADJUNTOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETADJUNTOS] @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETALMACENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETALMACENES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETALMACENES]  @CIA AS INT,@COD VARCHAR(4)=NULL,@COD_CENTRO VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las Almacenes" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETAlmacenes @COD,@COD_CENTRO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL,@COD_CENTRO VARCHAR(4)=NULL', @COD=@COD,@COD_CENTRO=@COD_CENTRO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETCENTROS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETCENTROS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETCENTROS] @CIA INT,  @COD VARCHAR(4)=NULL,@COD_ORGCOMPRAS VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos los CENTROS" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETCENTROS  @COD,@COD_ORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL,@COD_ORGCOMPRAS VARCHAR(4)=NULL', @COD = @COD,@COD_ORGCOMPRAS=@COD_ORGCOMPRAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETDEPARTAMENTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETDEPARTAMENTOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETDEPARTAMENTOS]  @CIA INT,@NIVEL INT=NULL,@UON1 VARCHAR(50)=NULL,@UON2 VARCHAR(50)=NULL,@UON3 VARCHAR(50)=NULL,@USU VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDEPARTAMENTOS @NIVEL=@NIVEL,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@USU=@USU'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NIVEL INT=null , @UON1 VARCHAR(20)=null , @UON2 VARCHAR(20)=null, @UON3 VARCHAR(20)=null, @USU VARCHAR(20)=null ', @NIVEL=@NIVEL,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@USU=@USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETDESGLOSE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETDESGLOSE] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDESGLOSE @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETDICTIONARYDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETDICTIONARYDATA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETDICTIONARYDATA] @MODULEID int,@LANGUAGE varchar(3)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT ID,TEXT_' + @LANGUAGE + ' FROM WEBFSPMTEXT WITH (NOLOCK) WHERE MODULO=@MODULEID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@MODULEID INT', @MODULEID=@MODULEID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETFIELD] @CIA INT,  @ID INT, @IDI VARCHAR(20) = NULL, @ATTACH AS TINYINT=0, @SOLICITUD AS INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELD  @ID=@ID, @IDI = @IDI , @ATTACH =@ATTACH, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @ATTACH TINYINT=0, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI , @ATTACH =@ATTACH,@SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFORM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFORM]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETFORM] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL, @SOLICPROVE INT =1  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFORM @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD, @SOLICPROVE=@SOLICPROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL, @SOLICPROVE INT =1', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD,@SOLICPROVE=@SOLICPROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUN]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTADJUN] @CIA INT, @TIPO INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUN @TIPO =@TIPO, @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @TIPO INT, @ID INT', @TIPO =@TIPO, @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNDATA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTADJUNDATA] @CIA INT=NULL, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTANCIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTANCIA] @CIA INT, @ID INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTANCIA @ID=@ID, @IDI =@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI AS VARCHAR(20) =NULL', @ID=@ID, @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTANCIAS_PADRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTANCIAS_PADRE]"
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTANCIAS_PADRE] @CIA INT, @ID INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETINSTANCIAS_PADRE @ID=@ID, @IDI =@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI AS VARCHAR(20) =NULL', @ID=@ID, @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO] @CIA INT, @SOLICITUD INT = 0, @ID INT= 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_ADJUNTODEFECTO @SOLICITUD=@SOLICITUD, @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SOLICITUD INT=0, @ID INT', @SOLICITUD=@SOLICITUD, @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTFIELD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTFIELD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTFIELD] @CIA INT, @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT, @CIAPROV INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTFIELD @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT', @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETORGANIZACIONESCOMPRAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETORGANIZACIONESCOMPRAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETORGANIZACIONESCOMPRAS] @CIA int, @COD VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las ORGANIZACIONES de COMPRA" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETORGANIZACIONESCOMPRAS @COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL', @COD=@COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETREQUEST]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETREQUEST]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETREQUEST] @CIA INT, @ID INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUEST @ID=@ID, @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI AS VARCHAR(20) =NULL', @ID=@ID, @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETREQUESTADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETREQUESTADJUN]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETREQUESTADJUN] @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUESTADJUN @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT ',@ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETREQUESTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETREQUESTADJUNDATA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETREQUESTADJUNDATA] @CIA INT, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUESTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0 ',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETTABLAEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETTABLAEXTERNA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETTABLAEXTERNA] @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETTIPOSOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETTIPOSOLICITUD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETTIPOSOLICITUD] @CIA INT,@ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos El tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETTIPOSOLICITUD @ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID AS INT', @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETUONS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETUONS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETUONS]  @CIA AS INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETUONS @IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI AS VARCHAR(50)', @IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETWORKFLOW_PROVE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE] @CIA INT,@PROVE VARCHAR(50), @IDI VARCHAR(50), @TIPO INT =0 , @FECHA  VARCHAR(10)=NULL, @INSTANCIA INT=0, @FILAESTADO INT =0, @FORMATOFECHA VARCHAR(20)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETWORKFLOW_PROVE @PRV=@PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@FILAESTADO=@FILAESTADO,@FORMATOFECHA=@FORMATOFECHA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @IDI VARCHAR(50) , @TI INT,@FEC  VARCHAR(10),@INS INT, @FILAESTADO INT,@FORMATOFECHA  VARCHAR(20)', @PRV=@PROVEGS,@IDI=@IDI, @TI=@TIPO,@FEC=@FECHA,@INS=@INSTANCIA,@FILAESTADO=@FILAESTADO,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN1]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMN1] @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN1 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50) ,  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN2]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMN2] @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN2 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50),  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN3]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMN3] @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL, @GMN2 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN3 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 , @GMN2 = @GMN2 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50), @GMN2 VARCHAR(50) ,  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @GMN2 = @GMN2 ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMN4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMN4]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMN4] @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL, @GMN2 VARCHAR(50) = NULL,  @GMN3 VARCHAR(50) = NULL, @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN4 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 , @GMN2 = @GMN2 , @GMN3 =@GMN3 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50), @GMN2 VARCHAR(50) ,  @GMN3 VARCHAR(50) = NULL, @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @GMN2 = @GMN2 ,@GMN3 =@GMN3  ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GMNS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GMNS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMNS] @CIA INT,@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) = 'SPA' AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMNS @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) ', @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GUARDARCOMENTARIOADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GUARDARCOMENTARIOADJUNTO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GUARDARCOMENTARIOADJUNTO] @CIA INT, @ID INT,  @TIPO INT =0, @INSTANCIA INT =0, @COMENT VARCHAR(4000) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GUARDARCOMENTARIOADJUNTO @ID=@ID,@TIPO=@TIPO,@INSTANCIA=@INSTANCIA ,@COMENT=@COMENT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@TIPO INT =0, @INSTANCIA INT =0, @COMENT VARCHAR(4000) = NULL',@ID =@ID,@TIPO=@TIPO,@INSTANCIA=@INSTANCIA,@COMENT=@COMENT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_IDACCIONGUARDAR_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_IDACCIONGUARDAR_ROL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_IDACCIONGUARDAR_ROL] @BLOQUE INT,@ROL INT,@CIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_IDACCIONGUARDAR_ROL @BLOQUE=@BLOQUE,@ROL=@ROL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,  N'@BLOQUE INT,@ROL INT', @BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LEER_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LEER_ADJUNTO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LEER_ADJUNTO] @CIA INT, @ID INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LEER_ADJUNTO @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT = NULL', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LISTADOS_ROL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LISTADOS_ROL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LISTADOS_ROL] @CIA INT,@BLOQUE INT,@ROL INT , @IDI VARCHAR(3),@COPIA TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LISTADOS_ROL @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT,@ROL INT ,@IDI VARCHAR(3),@COPIA TINYINT', @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOAD_ACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOAD_ACCION]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOAD_ACCION] @CIA INT,@ACCION INT , @IDI VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOAD_ACCION @ACCION=@ACCION,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ACCION INT, @IDI VARCHAR(50) ', @ACCION=@ACCION,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOAD_ETAPAS_REALIZADAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOAD_ETAPAS_REALIZADAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOAD_ETAPAS_REALIZADAS] @CIA INT,@ACCION INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOAD_ETAPAS_REALIZADAS @ACCION=@ACCION,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ACCION INT, @IDI VARCHAR(50) ', @ACCION=@ACCION,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADESTADOSINSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADESTADOSINSTANCIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADESTADOSINSTANCIA] @CIA INT,@INSTANCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADESTADOSINSTANCIA @INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT ', @INSTANCIA=@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSDESGLOSECALC]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADFIELDSDESGLOSECALC] @CIA INT,@CAMPO INT, @SOLICITUD INT,  @PER VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADFIELDSDESGLOSECALC @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT, @SOLICITUD  INT,@PER VARCHAR(50) ', @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSCALC]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADINSTFIELDSCALC] @CIA INT, @INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)='SPA', @PROVE varchar(50)=null, @NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTFIELDSCALC @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSCALC @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50),@PROVE varchar(50)', @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC] @CIA INT, @CAMPO INT , @INSTANCIA INT = NULL ,  @PROVE VARCHAR(50) = NULL,@NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTFIELDSDESGLOSECALC @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSDESGLOSECALC @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT,@INSTANCIA INT = NULL ,  @PROVE VARCHAR(50) = NULL', @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADNOCONFORMIDADES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADNOCONFORMIDADES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADNOCONFORMIDADES] @CIA INT,@PROVE VARCHAR(50)=NULL, @USU VARCHAR(50), @TIPO INT=NULL, @FECHA AS VARCHAR(10)=NULL, @FILAESTADO AS INT=0, @ID AS INT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el c�digo del proveedor en el GS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS RC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CIAS C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON RC.CIA_PROVE = C.ID" & vbCrLf
sConsulta = sConsulta & "WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las NoConformidades" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_NO_CONFORMIDADES @USU= @USU, @TIPO=@TIPO, @FECHA=@FECHA, @FILAESTADO=@FILAESTADO, @PROVE='+@PROVEGS+', @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @USU AS VARCHAR(50), @TIPO AS INT, @FECHA AS VARCHAR(10), @FILAESTADO AS INT, @ID AS INT, @PROVE AS VARCHAR(50)', @USU=@USU, @TIPO =@TIPO, @FECHA=@FECHA, @FILAESTADO=@FILAESTADO, @ID=@ID, @PROVE=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADSOLICITUDES_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADSOLICITUDES_PROVE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADSOLICITUDES_PROVE] @CIA INT,@PROVE VARCHAR(50), @TIPO INT =0, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADSOLICITUDES_PROVE @PROVE=@PROVE,@TIPO=@TIPO,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @TIPO INT, @IDI VARCHAR(50) ', @PROVE=@PROVEGS, @TIPO=@TIPO,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_MONEDAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_MONEDAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_MONEDAS] @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1, @IDIOMA VARCHAR(20),@ALLDATOS TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_MONEDAS @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA,@ALLDATOS=@ALLDATOS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50), @DEN VARCHAR(2000) , @COINCID TINYINT,  @IDIOMA VARCHAR (20), @ALLDATOS TINYINT', @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA, @ALLDATOS=@ALLDATOS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PAISES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PAISES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PAISES] @IDIOMA VARCHAR(20), @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PAISES @COD=@COD ,@DEN=@DEN ,@COINCID=@COINCID, @IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @COINCID TINYINT, @IDIOMA VARCHAR(20)', @COD=@COD , @DEN=@DEN , @COINCID=@COINCID, @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PARGEN_INTERNO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PARGEN_INTERNO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT * FROM ' + @FSGS + 'PARGEN_INTERNO WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PARTICIPANTES_PER]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PARTICIPANTES_PER]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PARTICIPANTES_PER] @CIA INT,@ROL INT , @INSTANCIA INT, @FILNOM VARCHAR(500)=NULL, @FILAPE VARCHAR(500)=NULL, @FILUON1 VARCHAR(50)=NULL, @FILUON2 VARCHAR(50)=NULL, @FILUON3 VARCHAR(50)=NULL , @FILDEP VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PARTICIPANTES_PER @ROL=@ROL,@INSTANCIA=@INSTANCIA,@FILNOM=@FILNOM,@FILAPE=@FILAPE,@FILUON1=@FILUON1,@FILUON2=@FILUON2,@FILUON3=@FILUON3,@FILDEP=@FILDEP'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ROL INT ,@INSTANCIA INT ,@FILNOM VARCHAR(500), @FILAPE VARCHAR(500), @FILUON1 VARCHAR(50), @FILUON2 VARCHAR(50), @FILUON3 VARCHAR(50) , @FILDEP VARCHAR(50) ', @ROL=@ROL,@INSTANCIA=@INSTANCIA,@FILNOM=@FILNOM,@FILAPE=@FILAPE,@FILUON1=@FILUON1,@FILUON2=@FILUON2,@FILUON3=@FILUON3,@FILDEP=@FILDEP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PARTICIPANTES_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PARTICIPANTES_PROVE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PARTICIPANTES_PROVE] @CIA INT,@ROL INT , @USU VARCHAR(50), @INSTANCIA INT, @COD VARCHAR(50), @DEN VARCHAR(500), @NIF VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PARTICIPANTES_PROVE @ROL=@ROL,@USU=@USU,@INSTANCIA=@INSTANCIA,@COD=@COD,@DEN=@DEN,@NIF=@NIF'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ROL INT , @USU VARCHAR(50), @INSTANCIA INT, @COD VARCHAR(50), @DEN VARCHAR(500), @NIF VARCHAR(50) ', @ROL=@ROL,@USU=@USU,@INSTANCIA=@INSTANCIA,@COD=@COD,@DEN=@DEN,@NIF=@NIF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRES1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRES1]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRES1] @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES1 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRES2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRES2]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRES2] @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES2 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRES3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRES3]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRES3] @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES3 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRES4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRES4]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRES4] @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES4 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PRESUP_LIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PRESUP_LIT]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRESUP_LIT] @CIA INT ,@TIPO INT, @IDI VARCHAR(50) ='SPA'  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos El tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PRESUP_LIT @TIPO=@TIPO,@IDI =@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@TIPO AS INT, @IDI AS VARCHAR(50)', @TIPO=@TIPO,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PROVE] @CIA INT, @PROVE VARCHAR(50), @PORTAL TINYINT = 0, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 1 " & vbCrLf
sConsulta = sConsulta & "  SELECT @PROVEGS = COD_PROVE_CIA   FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @PROVEGS = @PROVE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVE @PROVE =@PROVE, @IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50), @IDIOMA VARCHAR(50)  ', @PROVE =@PROVEGS, @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PROVES] @CIA INT, @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50)='SPA', @ORGCOMPRAS VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS AS VARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @COD " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVES @COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU, @IDI=@IDI,@ORGCOMPRAS =@ORGCOMPRAS '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50) ,@ORGCOMPRAS VARCHAR(4)',@COD=@PROVEGS, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU,@IDI=@IDI,@ORGCOMPRAS =@ORGCOMPRAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVIS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PROVIS] @CIA INT,@PAI VARCHAR(50),@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @USALIKE TINYINT =null, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVIS @PAI=@PAI,@COD=@COD,@DEN =@DEN,@USALIKE=@USALIKE,@IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PAI VARCHAR(50),@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT, @IDIOMA VARCHAR(50)', @PAI=@PAI,@COD=@COD,@DEN =@DEN,@USALIKE=@USALIKE,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_REALIZAR_ACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_REALIZAR_ACCION]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_REALIZAR_ACCION] @CIA INT,  @CIAPROVE INT, @INSTANCIA INT, @INSTANCIABLOQUE INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL ,@DEVOLUCION TINYINT=0, @ETAPA INT , @USUNOM VARCHAR(500)=NULL,@IDIOMAPROVE VARCHAR(3)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @INSTANCIABLOQUE=@INSTANCIABLOQUE, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE, @DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @INSTANCIABLOQUE INT,  @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT,@DEVOLUCION TINYINT,@ETAPA INT , @USUNOM VARCHAR(500),@IDIOMAPROVE VARCHAR(3)=NULL  ', @INSTANCIA=@INSTANCIA,  @INSTANCIABLOQUE=@INSTANCIABLOQUE,  @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE,@DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEVOLUCION = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Por programa SET XACT_ABORT ON si falla FSPM_TRASPASAR_INSTANCIA aqui no llega" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET SOLIC_ACT= SOLIC_ACT - 1,SOLIC_NUE= SOLIC_NUE - 1 WHERE CIA_COMP = @CIA AND CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ROL_PARTICIPANTES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ROL_PARTICIPANTES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ROL_PARTICIPANTES] @CIA INT,@BLOQUE INT,@ROL INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ROL_PARTICIPANTES @BLOQUE=@BLOQUE,@ROL=@ROL,@INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @ROL INT, @INSTANCIA INT, @IDI VARCHAR(50) ',@BLOQUE=@BLOQUE,@ROL=@ROL,@INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD] @CIA INT, @ID_ADJUN INT OUTPUT,@PER NVARCHAR(50) =NULL,@PROVE NVARCHAR(50),@NOM NVARCHAR(100), @ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_SAVE_ADJUN_CONTRATO_WIZARD @ID_ADJUN=@ID_ADJUN OUTPUT,@PER=@PER,@PROVE=@PROVE, @NOM=@NOM,@ID_CONTRATO=@ID_CONTRATO,@COMENT=@COMENT '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID_ADJUN INT OUTPUT, @PER NVARCHAR(50),@PROVE NVARCHAR(50),@NOM NVARCHAR(100),@ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000) ', @ID_ADJUN=@ID_ADJUN OUTPUT,@PER=@PER,@PROVE=@PROVE, @NOM=@NOM,@ID_CONTRATO=@ID_CONTRATO,@COMENT=@COMENT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SAVE_INSTANCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SAVE_INSTANCE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SAVE_INSTANCE] @CIA INT, @INSTANCIA INT, @PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @ENVIAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_SAVE_INSTANCE @INSTANCIA=@INSTANCIA, @PETICIONARIO =@PETICIONARIO , @COMPRADOR =@COMPRADOR , @MON =@MON , @ENVIAR =@ENVIAR , @IMPORTE =@IMPORTE , @PEDIDO_AUT =@PEDIDO_AUT '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @ENVIAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0 ', @INSTANCIA=@INSTANCIA, @PETICIONARIO =@PETICIONARIO , @COMPRADOR =@COMPRADOR , @MON =@MON , @ENVIAR =@ENVIAR , @IMPORTE =@IMPORTE , @PEDIDO_AUT =@PEDIDO_AUT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SETREQUESTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SETREQUESTADJUNDATA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SETREQUESTADJUNDATA] @CIA INT,  @ID INT, @INIT INT, @OFFSET INT, @DATA IMAGE AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_SETREQUESTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @ID INT, @INIT INT, @OFFSET INT, @DATA IMAGE ',@ID = @ID, @INIT=@INIT, @OFFSET=@OFFSET, @DATA=@DATA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SOLICITUD_DATOS_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SOLICITUD_DATOS_EMAIL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_EMAIL] @CIA INT, @ID INT,@TYPE INT,@CONTACTO INT=NULL,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_SOLICITUD_DATOS_EMAIL @ID=@ID, @TYPE=@TYPE, @CONTACTO=@CONTACTO, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT,@TYPE INT,@CONTACTO INT=NULL,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0 ',@ID=@ID, @TYPE=@TYPE, @CONTACTO=@CONTACTO, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SOLICITUD_DATOS_MENSAJE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SOLICITUD_DATOS_MENSAJE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_MENSAJE] @CIA INT,@CIA_PROVE INT, @ID INT,@TYPE INT,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0,@COD_USU VARCHAR(50), @IDI VARCHAR(20)='SPA',@FORMATOFECHA VARCHAR(20)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =  N'EXEC ' + @FSGS + 'FSPM_SOLICITUD_DATOS_MENSAJE @ID=@ID, @TYPE=@TYPE, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE, @IDI=@IDI,@FORMATOFECHA=@FORMATOFECHA  '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT,@TYPE INT,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0, @IDI VARCHAR(20),@FORMATOFECHA VARCHAR(20)',@ID=@ID, @TYPE=@TYPE, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE, @IDI=@IDI,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtener los datos del Usuario de la Compa�ia" & vbCrLf
sConsulta = sConsulta & "SELECT U.COD COD_USU,U.NOM NOM_USU,U.APE APE_USU,U.TFNO TFNO1_USU,U.EMAIL EMAIL_USU,U.FAX FAX_USU, C.COD COD_COM,C.DEN DEN_COM,C.NIF NIF_COM" & vbCrLf
sConsulta = sConsulta & "FROM USU U WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS C WITH (NOLOCK) ON U.CIA = C.ID WHERE C.ID =@CIA_PROVE AND U.COD = @COD_USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UNIDADES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UNIDADES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UNIDADES] @CIA INT,@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT = 1, @IDIOMA VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UNIDADES @COD =@COD , @DEN =@DEN ,@USALIKE  =@USALIKE , @IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT, @IDIOMA VARCHAR(20)  ', @COD =@COD , @DEN =@DEN , @USALIKE  =@USALIKE, @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UONS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UONS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UONS] @CIA INT,@TIPO INT, @PER VARCHAR(50) = NULL, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UONS @TIPO=@TIPO, @PER = @PER , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT,@PER VARCHAR(50), @IDI VARCHAR(50) ', @TIPO=@TIPO, @PER = @PER , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UONSPRES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UONSPRES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UONSPRES] @CIA INT,@TIPO INT, @ANYO INT, @IDI VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_UONSPRES @TIPO=@TIPO, @ANYO = @ANYO , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @ANYO INT, @IDI VARCHAR(50) ', @TIPO=@TIPO, @ANYO = @ANYO , @IDI =@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UPLOAD_ADJUN] @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @CIA INT =NULL, @USU INT =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO COPIA_ADJUN (DATA, NOM, COMENT, FECALTA,CIA,USU) VALUES (NULL, @NOM, @COMENT, GETDATE(),@CIA, @USU)" & vbCrLf
sConsulta = sConsulta & "    SET @ID = (SELECT MAX(ID) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    UPDATE COPIA_ADJUN SET NOM = @NOM , COMENT = @COMENT WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN WITH (NOLOCK) WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
sConsulta = sConsulta & "    UPDATE COPIA_ADJUN SET DATA = @DATA WHERE ID =@ID " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    UPDATETEXT COPIA_ADJUN.DATA @textpointer @init 0 @data" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN_GS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UPLOAD_ADJUN_GS] @CIA INT, @CIAPROVE INT,  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(20)=NULL, @USUNOM VARCHAR(500)=NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOBRA TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @SOBRA=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UPLOAD_ADJUN @INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @PROVE=@PROVE, @SOBRA=@SOBRA, @USUNOM=@USUNOM '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL, @PROVE VARCHAR(50)=NULL, @SOBRA TINYINT, @USUNOM VARCHAR(500)=NULL ',@INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @PROVE = @PROVEGS, @SOBRA=@SOBRA, @USUNOM=@USUNOM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_VER_DETALLE_PERSONA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_VER_DETALLE_PERSONA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_VER_DETALLE_PERSONA] @PER  VARCHAR(50), @INSTANCIA INT, @CIA INT, @PROVE INT = 1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_VER_DETALLE_PERSONA @PER=@PER, @INSTANCIA = @INSTANCIA , @PROVE =@PROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PER VARCHAR(50), @INSTANCIA INT, @PROVE INT ',@PER = @PER , @INSTANCIA=@INSTANCIA, @PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSSM_DEVOLVER_DETALLE_PARTIDA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSSM_DEVOLVER_DETALLE_PARTIDA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSSM_DEVOLVER_DETALLE_PARTIDA] @CIA INT, @IDI VARCHAR(20),@PRES0 VARCHAR(10),@PRES1 VARCHAR(10), @PRES2 VARCHAR(10)=NULL, @PRES3 VARCHAR(10)=NULL, @PRES4 VARCHAR(10)=NULL, @UON1 VARCHAR(10),@UON2 VARCHAR(10)=NULL,@UON3 VARCHAR(10)=NULL,@UON4 VARCHAR(10)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSSM_DEVOLVER_DETALLE_PARTIDA @IDI=@IDI,@PRES0=@PRES0,@PRES1=@PRES1,@PRES2=@PRES2,@PRES3=@PRES3,@PRES4=@PRES4,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(20),@PRES0 VARCHAR(10),@PRES1 VARCHAR(10), @PRES2 VARCHAR(10)=NULL, @PRES3 VARCHAR(10)=NULL, @PRES4 VARCHAR(10)=NULL, @UON1 VARCHAR(10),@UON2 VARCHAR(10)=NULL,@UON3 VARCHAR(10)=NULL,@UON4 VARCHAR(10)=NULL',@IDI=@IDI,@PRES0=@PRES0,@PRES1=@PRES1,@PRES2=@PRES2,@PRES3=@PRES3,@PRES4=@PRES4,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETADJUNTO_CONTRATO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETADJUNTO_CONTRATO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO] @CIA INT, @ID INT =NULL, @ID_CONTRATO INT =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTO_CONTRATO @ID=@ID,@ID_CONTRATO=@ID_CONTRATO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID  INT, @ID_CONTRATO INT ', @ID=@ID,@ID_CONTRATO=@ID_CONTRATO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Eliminaci�n de storeds
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACTUALIZAINSTANCIANULA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACTUALIZAINSTANCIANULA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_CREATE_NEW_CERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_CREATE_NEW_CERTIFICADO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_DEVOLVER_SOLICITYAPROB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_DEVOLVER_SOLICITYAPROB]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ENVIAR_CERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ENVIAR_CERTIFICADO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ENVIAR_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ENVIAR_NOCONFORMIDAD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VARCERT_POND]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VARCERT_POND]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VARIABLES_HIJAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VARIABLES_HIJAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VARNOCONF_POND]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VARNOCONF_POND]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_RESPUESTAS_SIN_ENVIAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_RESPUESTAS_SIN_ENVIAR]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_SAVE_PUNTUACION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_SAVE_PUNTUACION]"
ExecuteSQL gRDOCon, sConsulta

'sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_SOLPENDIENTES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
'sConsulta = sConsulta & "drop procedure [dbo].[FSQA_SOLPENDIENTES]"
'ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VALIDAR_VARIABLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VALIDAR_VARIABLE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VALIDARCERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VALIDARCERTIFICADO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VALIDARNOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VALIDARNOCONFORMIDAD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PAIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PAIS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVI]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_MON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_MON]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_PAIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_PAIS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_PROVI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_PROVI]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_MON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_MON]"
ExecuteSQL gRDOCon, sConsulta


'Fin Eliminaci�n de storeds

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_EN_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_EN_PROCESO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_EN_PROCESO] @CIA INT,@ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_EN_PROCESO @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_CALIFICATION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_CALIFICATION]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_GET_CALIFICATION] @CIA INT, @VARCAL AS INT,@NIVEL AS SMALLINT, @VALUE AS FLOAT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CALIFICATION @VARCAL=@VARCAL, @NIVEL =@NIVEL, @VALUE =@VALUE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@VARCAL AS INT,@NIVEL AS SMALLINT, @VALUE AS FLOAT ',   @VARCAL =@VARCAL, @NIVEL =@NIVEL,@VALUE =@VALUE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_TIPONOCONFORMIDADES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_TIPONOCONFORMIDADES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_GET_TIPONOCONFORMIDADES] @CIA INT,@USU VARCHAR(50), @IDI VARCHAR(20), @COMBO TINYINT=0, @TIPO INT=NULL, @DESDESEGUIMIENTO INT=NULL, @NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos los tipos de NoConformidades" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPMQA_GET_SOLICITUDES @USU= @USU, @IDI =@IDI, @COMBO=@COMBO,@TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @USU AS VARCHAR(50), @IDI AS VARCHAR(20),@COMBO AS TINYINT, @TIPO AS INT', @USU=@USU, @IDI =@IDI, @COMBO =@COMBO, @TIPO =@TIPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VARIABLES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VARIABLES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_GET_VARIABLES] @CIA INT, @SOLICITUD INT,@NIVEL SMALLINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VARIABLES  @SOLICITUD =@SOLICITUD, @NIVEL =@NIVEL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SOLICITUD INT,@NIVEL SMALLINT ', @SOLICITUD =@SOLICITUD, @NIVEL =@NIVEL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VERSION_NOCONF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VERSION_NOCONF]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_GET_VERSION_NOCONF] @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VERSION_NOCONF @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADCORRESPONDENCIACAMPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADCORRESPONDENCIACAMPOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_LOADCORRESPONDENCIACAMPOS] @CIA INT, @CERTIFICADO INT , @VERSION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADCORRESPONDENCIACAMPOS @CERTIFICADO =@CERTIFICADO , @VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @CERTIFICADO INT , @VERSION INT', @CERTIFICADO =@CERTIFICADO , @VERSION =@VERSION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS] @CIA INT, @PROVE VARCHAR(50), @IDI VARCHAR(20) =NULL, @ID INT=0, @TIPO INT=0, @FECHA VARCHAR(10)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos el servidor y la base de datos del GS." & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos el c�digo y el identificador del PROVEEDOR en el GS." & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos las NOCONFORMIDADES cerradas." & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_NOCONFORMIDADES_CERRADAS @PROVE =@PROVE, @IDI=@IDI, @ID = @ID , @TIPO = @TIPO, @FECHA = @FECHA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @IDI VARCHAR(10), @ID INT, @TIPO INT, @FECHA VARCHAR(10)' , @PROVE = @PROVEGS, @IDI=@IDI, @ID=@ID, @TIPO=@TIPO, @FECHA=@FECHA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADRESPUESTASCERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADRESPUESTASCERTIFICADO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_LOADRESPUESTASCERTIFICADO] @CIA INT,@PROVE VARCHAR(50),  @CERTIFICADO INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADRESPUESTASCERTIFICADO @PROVE=@PROVE, @CERTIFICADO =@CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT =NULL, @PROVE VARCHAR(50)',@CERTIFICADO =@CERTIFICADO, @PROVE=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADRESPUESTASNOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADRESPUESTASNOCONFORMIDAD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_LOADRESPUESTASNOCONFORMIDAD] @CIA INT,@PROVE VARCHAR(50),  @NOCONFORMIDAD INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADRESPUESTASNOCONFORMIDAD @PROVE=@PROVE, @NOCONFORMIDAD =@NOCONFORMIDAD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NOCONFORMIDAD INT =NULL, @PROVE VARCHAR(50)',@NOCONFORMIDAD =@NOCONFORMIDAD, @PROVE=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVI_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVI_COD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[PROVI_COD] (@PAIS VARCHAR(50),@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PAIS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_PAIS=(SELECT ID FROM PAI WITH(NOLOCK) WHERE COD=@PAIS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVI SET COD=@NEW WHERE PAI=@ID_PAIS AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIAS_ACT1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIAS_ACT1]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIAS_ACT1] (@CIACOD varchar(50), @GMN1 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT1 (CIA,ACT1) VALUES (@ID_CIA ,@ID_ACT1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIAS_ACT2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIAS_ACT2]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIAS_ACT2] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT2 (CIA,ACT1,ACT2) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIAS_ACT3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIAS_ACT3]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIAS_ACT3] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT3 (CIA,ACT1,ACT2,ACT3) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 , @ID_ACT3 )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIAS_ACT4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIAS_ACT4]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIAS_ACT4] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50),@GMN4 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT4 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT4=(SELECT ID FROM ACT4 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND COD=@GMN4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT4 (CIA,ACT1,ACT2,ACT3,ACT4) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 , @ID_ACT3 , @ID_ACT4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_COMPROBAR_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_COMPROBAR_EMAIL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_COMPROBAR_EMAIL](@FSP_CIA VARCHAR(20),@EMAIL VARCHAR(100),@EXISTE INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE   @CIA INTEGER" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CIA = (SELECT ID FROM CIAS WHERE COD=@FSP_CIA)" & vbCrLf
sConsulta = sConsulta & "SET @EXISTE = (SELECT COUNT(*)  FROM USU WITH(NOLOCK) INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA = USU_PORT.CIA AND USU.ID = USU_PORT.USU  WHERE USU.CIA=@CIA AND USU.EMAIL=@EMAIL)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEL_VOLADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEL_VOLADJ]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEL_VOLADJ](@COMP INT,@PROVE VARCHAR(50), @ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ID = (SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@PROVE)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@COMP AND CIA_PROVE=@ID AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_CIAS_ACT1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_CIAS_ACT1]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_CIAS_ACT1] (@CIACOD varchar(50), @GMN1 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_CIAS_ACT2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_CIAS_ACT2]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_CIAS_ACT2] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL " & vbCrLf
sConsulta = sConsulta & " DELETE FROM CIAS_ACT2 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM CIAS_ACT2 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_CIAS_ACT3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_CIAS_ACT3]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_CIAS_ACT3] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin    " & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "      IF @GMN3 IS NULL" & vbCrLf
sConsulta = sConsulta & "         begin" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2" & vbCrLf
sConsulta = sConsulta & "         end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         begin" & vbCrLf
sConsulta = sConsulta & "   SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3" & vbCrLf
sConsulta = sConsulta & "         end" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_CIAS_ACT4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_CIAS_ACT4]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_CIAS_ACT4] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50),@GMN4 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT4 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      IF @GMN3 IS NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "        IF @GMN4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 " & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "            SET @ID_ACT4=(SELECT ID FROM ACT4 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND COD=@GMN4)" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND ACT4=@ID_ACT4" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_PAIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_PAIS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  [dbo].[SP_ELIM_PAIS]  (@PAI_COD NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PAI_DEN WHERE PAI = (SELECT ID FROM PAI WITH(NOLOCK) WHERE COD=@PAI_COD)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PAI WHERE COD=@PAI_COD" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_PROVI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_PROVI]"
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  [dbo].[SP_ELIM_PROVI]  (@PAI_COD NVARCHAR(50),@PROVI_COD NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PAIS AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PROVI AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ID_PAIS=(SELECT ID FROM PAI WITH(NOLOCK)  WHERE COD=@PAI_COD)" & vbCrLf
sConsulta = sConsulta & "   SET @ID_PROVI=(SELECT ID FROM PROVI WITH(NOLOCK)  WHERE PAI=@ID_PAIS AND COD=@PROVI_COD)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROVI_DEN WHERE PAI=@ID_PAIS AND PROVI=@ID_PROVI" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROVI WHERE PAI=@ID_PAIS AND COD=@PROVI_COD " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_RELCIAS_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_RELCIAS_GS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_RELCIAS_GS] (@CIA_COMP INT, @CIA_PROVE VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE  AS INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @IDPROVE= (SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIA_PROVE)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM REL_CIAS WHERE CIA_COMP=@CIA_COMP AND CIA_PROVE=@IDPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "return" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_PROCE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIMINAR_PROCE] @CIA_COMP SMALLINT, @ANYO SMALLINT, @GMN1 VARCHAR(50), @PROCE SMALLINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curELIMPROCE CURSOR LOCAL FOR SELECT ID FROM  PROCE_OFE WITH(NOLOCK) WHERE CIA_COMP=@CIA_COMP AND ANYO=@ANYO AND  GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curELIMPROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curELIMPROCE INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ITEM_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ITEM_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_GR_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_GR_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM PROCE_OFE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM ITEM_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM GRUPO_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM PROCE_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curELIMPROCE INTO @ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curELIMPROCE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curELIMPROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Sub V_31900_3_Tablas_011()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT1]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "create view VIEW_ACT1 as " & vbCrLf
sConsulta = sConsulta & "SELECT     ID, COD, DEN_SPA, DEN_ENG, FECACT, DEN_GER, PYME, COD AS CODACT1" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT1 AS x WITH (NOLOCK)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT2]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "create view VIEW_ACT2 as " & vbCrLf
sConsulta = sConsulta & "SELECT     x.ACT1, x.ID, x.COD, x.DEN_SPA, x.DEN_ENG, x.FECACT, x.DEN_GER, dbo.ACT1.COD AS CODACT1, x.COD AS CODACT2" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT2 AS x WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT1 WITH (NOLOCK) ON x.ACT1 = dbo.ACT1.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT3]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "create view VIEW_ACT3 as " & vbCrLf
sConsulta = sConsulta & "SELECT     x.ACT1, x.ACT2, x.ID, x.COD, x.DEN_SPA, x.DEN_ENG, x.FECACT, x.DEN_GER, dbo.ACT1.COD AS CODACT1, dbo.ACT2.COD AS CODACT2, " & vbCrLf
sConsulta = sConsulta & "                      x.COD AS CODACT3" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT3 AS x WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT1 WITH (NOLOCK) ON x.ACT1 = dbo.ACT1.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT2 WITH (NOLOCK) ON x.ACT1 = dbo.ACT2.ACT1 AND x.ACT2 = dbo.ACT2.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT4]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "create view VIEW_ACT4 as " & vbCrLf
sConsulta = sConsulta & "SELECT     x.ACT1, x.ACT2, x.ACT3, x.ID, x.COD, x.DEN_SPA, x.DEN_ENG, x.FECACT, x.DEN_GER, dbo.ACT1.COD AS CODACT1, dbo.ACT2.COD AS CODACT2, " & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT3.COD AS CODACT3, x.COD AS CODACT4" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT4 AS x WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT1 WITH (NOLOCK) ON x.ACT1 = dbo.ACT1.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT2 WITH (NOLOCK) ON x.ACT1 = dbo.ACT2.ACT1 AND x.ACT2 = dbo.ACT2.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT3 WITH (NOLOCK) ON x.ACT1 = dbo.ACT3.ACT1 AND x.ACT2 = dbo.ACT3.ACT2 AND x.ACT3 = dbo.ACT3.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT5]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT5]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "create view VIEW_ACT5 as " & vbCrLf
sConsulta = sConsulta & "SELECT     x.ACT1, x.ACT2, x.ACT3, x.ACT4, x.ID, x.COD, x.DEN_SPA, x.DEN_ENG, x.FECACT, x.DEN_GER, dbo.ACT1.COD AS CODACT1, " & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT2.COD AS CODACT2, dbo.ACT3.COD AS CODACT3, dbo.ACT4.COD AS CODACT4, x.COD AS CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT5 AS x WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT1 WITH (NOLOCK) ON x.ACT1 = dbo.ACT1.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT2 WITH (NOLOCK) ON x.ACT1 = dbo.ACT2.ACT1 AND x.ACT2 = dbo.ACT2.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT3 WITH (NOLOCK) ON x.ACT1 = dbo.ACT3.ACT1 AND x.ACT2 = dbo.ACT3.ACT2 AND x.ACT3 = dbo.ACT3.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT4 WITH (NOLOCK) ON x.ACT1 = dbo.ACT4.ACT1 AND x.ACT2 = dbo.ACT4.ACT2 AND x.ACT3 = dbo.ACT4.ACT3 AND x.ACT3 = dbo.ACT4.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_13_11A31900_03_13_12() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_012
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.12'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_11A31900_03_13_12 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_11A31900_03_13_12 = False
End Function


Sub V_31900_3_Storeds_012()
Dim sConsulta As String
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_SOLPENDIENTES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_SOLPENDIENTES]"
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[FSQA_SOLPENDIENTES] @CIACOMP FLOAT, @CIAPROVE FLOAT, @USU  VARCHAR(20), @IDI VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT GETDATE() AS DIA, USU.NOM + ' ' + USU.APE AS NOMBRE, CIAS.COD AS PROVEENCIA" & vbCrLf
sConsulta = sConsulta & ",REL_CIAS.CIA_PROVE,REL_CIAS.CIA_COMP,REL_CIAS.EST,REL_CIAS.COD_PROVE_CIA,REL_CIAS.PROCE_ACT,REL_CIAS.PROCE_NUE" & vbCrLf
sConsulta = sConsulta & ",REL_CIAS.PROCE_REV,REL_CIAS.PREM_ACTIVO,REL_CIAS.ORDEN_ACT,REL_CIAS.ORDEN_NUE,REL_CIAS.SOLIC_ACT,REL_CIAS.SOLIC_NUE" & vbCrLf
sConsulta = sConsulta & ",REL_CIAS.CERT_ACT,REL_CIAS.CERT_NUE,REL_CIAS.NOCONF_ACT,REL_CIAS.NOCONF_NUE " & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU WITH(NOLOCK) ON USU.CIA = REL_CIAS.CIA_PROVE AND COD=@USU" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=@CIAPROVE AND CIAS.ID=CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "WHERE CIA_COMP = @CIACOMP AND CIA_PROVE = @CIAPROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_ITEM]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_ITEM] (@ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @GRUPO VARCHAR(50), @ITEM INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID=ID FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DELETE OFE_ITEM_ADJUN WHERE ID=@ID AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE OFE_ITEM_ATRIB WHERE ID=@ID AND GRUPO=@GRUPO AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "DELETE ITEM_OFE WHERE ID=@ID AND GRUPO=@GRUPO AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_ACT_REL_CIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_ACT_REL_CIAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSEP_ACT_REL_CIAS](@COMP VARCHAR(50),@PROVE VARCHAR(50),@ACTIVAS INTEGER,@NUEVAS INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP AS INT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WITH (NOLOCK) WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET ORDEN_ACT=@ACTIVAS,ORDEN_NUE=@NUEVAS WHERE CIA_COMP=@ID_COMP AND COD_PROVE_CIA=@PROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NUEVO_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NUEVO_ADJUNTO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_NUEVO_ADJUNTO] @CIA_COMP int, @DATA IMAGE, @MAXID int OUTPUT, @DATASIZE int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ADJUN (CIA_COMP, DATA) VALUES (@CIA_COMP, @DATA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD: Sin WITH (NOLOCK) porque es para obtener el nuevo ID" & vbCrLf
sConsulta = sConsulta & "SELECT @MAXID = MAX(ID) FROM ADJUN" & vbCrLf
sConsulta = sConsulta & "SELECT @DATASIZE=DATALENGTH(DATA) FROM ADJUN WITH (NOLOCK) WHERE ID = @MAXID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_ADJUNTO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DOWNLOAD_ADJUNTO] @INIT INT, @OFFSET INT, @ID INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM ADJUN WITH (NOLOCK) WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET >= @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_ESPECIFICACION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_ESPECIFICACION]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DOWNLOAD_ESPECIFICACION] @INIT INT, @OFFSET INT, @ID INT,@CIA INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM CIAS_ESP WITH (NOLOCK) WHERE CIA=@CIA AND ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET >= @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT CIAS_ESP.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NUEVA_ESP_CIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NUEVA_ESP_CIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_NUEVA_ESP_CIA] " & vbCrLf
sConsulta = sConsulta & "   @CIA int, " & vbCrLf
sConsulta = sConsulta & "   @DATA IMAGE," & vbCrLf
sConsulta = sConsulta & "   @NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "   @COM VARCHAR(600), " & vbCrLf
sConsulta = sConsulta & "   @MAXID int OUTPUT, " & vbCrLf
sConsulta = sConsulta & "   @DATASIZE int OUTPUT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MAXID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD: Sin WITH (NOLOCK) porque es para obtener un nuevo ID" & vbCrLf
sConsulta = sConsulta & "select @MAXID = ISNULL(ID,0) + 1 " & vbCrLf
sConsulta = sConsulta & "FROM CIAS_ESP " & vbCrLf
sConsulta = sConsulta & "WHERE CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA,ID,NOM, COM,DATA) VALUES (@CIA,@MAXID,@NOM, @COM,@DATA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD: Sin WITH (NOLOCK) porque es para obtener el tama�o de los datos que se acaban de insertar" & vbCrLf
sConsulta = sConsulta & "SELECT @DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WHERE ID = @MAXID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE  PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER, " & vbCrLf
sConsulta = sConsulta & "   @ANYO smallint, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 varchar(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE smallint  , " & vbCrLf
sConsulta = sConsulta & "   @GRUPO varchar(50) " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_GRUPO @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint  , @GRUPO varchar(50)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PUJAS_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PUJAS_ITEM]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PUJAS_ITEM] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER," & vbCrLf
sConsulta = sConsulta & "   @ANYO INTEGER," & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @PROCE INTEGER ," & vbCrLf
sConsulta = sConsulta & "   @ITEM INTEGER," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50)  " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PUJAS_ITEM_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@ITEM=@ITEM,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER,@PROVE VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @ITEM = @ITEM,@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER, " & vbCrLf
sConsulta = sConsulta & "   @ANYO smallint, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 varchar(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE smallint , " & vbCrLf
sConsulta = sConsulta & "   @ITEM smallint " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_ITEM @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @ITEM = @ITEM'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint  , @ITEM smallint'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ITEMS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  [dbo].[SP_ITEMS]  @ID INT,@GRUPO VARCHAR(50)=null AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            WHERE ID=@ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ITO.ITEM, PRECIO, CANTMAX, PRECIO2, PRECIO3,IA.NUMADJUN, COMENT1, COMENT2, COMENT3, cast(OBSADJUN as varchar(2000)) OBSADJUN, @CAM CAMBIO,ITO.PREC_VALIDO,ITO.IMPORTE" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE ITO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (SELECT IA.ITEM, COUNT(IA.ADJUN) NUMADJUN" & vbCrLf
sConsulta = sConsulta & "                 FROM OFE_ITEM_ADJUN IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                WHERE IA.ID =@ID" & vbCrLf
sConsulta = sConsulta & "             GROUP BY IA.ID, IA.ITEM) IA" & vbCrLf
sConsulta = sConsulta & "      ON ITO.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "WHERE ITO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "  AND ITO.GRUPO=case when @GRUPO is null then ITO.GRUPO else @GRUPO END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ADJUNTOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_ADJUNTOS] " & vbCrLf
sConsulta = sConsulta & "   @ID int, " & vbCrLf
sConsulta = sConsulta & "   @GRUPO varchar(50) = null, " & vbCrLf
sConsulta = sConsulta & "   @ITEM int = null, " & vbCrLf
sConsulta = sConsulta & "   @AMBITO tinyint, " & vbCrLf
sConsulta = sConsulta & "   @SOLOPORTAL int = 0 OUTPUT " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 0" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT 1 AMBITO, POA.ID,POA.ADJUN, NULL GRUPO, NULL ITEM, POA.NOM, POA.COM, POA.IDGS, POA.DATASIZE, POA.IDPORTAL , PO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE_ADJUN POA WITH (NOLOCK) INNER JOIN PROCE_OFE PO WITH (NOLOCK) ON POA.ID = PO.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE POA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT 2 AMBITO, OGA.ID, OGA.ADJUN, OGA.GRUPO, NULL ITEM, OGA.NOM, OGA.COM, OGA.IDGS, OGA.DATASIZE, OGA.IDPORTAL, GO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_GR_ADJUN OGA WITH (NOLOCK) INNER JOIN GRUPO_OFE GO WITH (NOLOCK) ON OGA.ID = GO.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE OGA.ID = @ID " & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT 3 AMBITO, OIA.ID, OIA.ADJUN, NULL GRUPO, OIA.ITEM, OIA.NOM, OIA.COM, OIA.IDGS, OIA.DATASIZE, OIA.IDPORTAL , IO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA WITH (NOLOCK) INNER JOIN ITEM_OFE IO WITH (NOLOCK) ON OIA.ID = IO.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ID =@ID" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT 2 AMBITO, OGA.ID, OGA.ADJUN, OGA.GRUPO, NULL ITEM, OGA.NOM, OGA.COM, OGA.IDGS, OGA.DATASIZE, OGA.IDPORTAL, GO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_GR_ADJUN OGA WITH (NOLOCK) INNER JOIN GRUPO_OFE GO WITH (NOLOCK) ON OGA.ID = GO.ID AND OGA.GRUPO = GO.GRUPO" & vbCrLf
sConsulta = sConsulta & "     WHERE OGA.ID = @ID " & vbCrLf
sConsulta = sConsulta & "       AND OGA.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT 3 AMBITO, OIA.ID, OIA.ADJUN, NULL GRUPO, OIA.ITEM, OIA.NOM, OIA.COM, OIA.IDGS, OIA.DATASIZE, OIA.IDPORTAL , IO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA WITH (NOLOCK) INNER JOIN ITEM_OFE IO WITH (NOLOCK) ON OIA.ID = IO.ID AND IO.ITEM = OIA.ITEM" & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ID =@ID" & vbCrLf
sConsulta = sConsulta & "       AND IO.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM PROCE_OFE WITH (NOLOCK) WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN, NOM,COM, IDGS, DATASIZE, IDPORTAL " & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE_ADJUN WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM GRUPO_OFE WITH (NOLOCK) WHERE ID = @ID AND GRUPO = @GRUPO)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN,GRUPO, NOM, COM, IDGS, DATASIZE, IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_GR_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     WHERE ID = @ID " & vbCrLf
sConsulta = sConsulta & "       AND GRUPO = ISNULL(@GRUPO,GRUPO)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM ITEM_OFE WITH (NOLOCK) WHERE ID = @ID AND ITEM = @ITEM)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN,ITEM,NOM, COM, IDGS, DATASIZE, IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     WHERE ID =@ID" & vbCrLf
sConsulta = sConsulta & "       AND ITEM = ISNULL(@ITEM,ITEM)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFE_ATRIB]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB] @ID int, @AMBITO tinyint, @GRUPO varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EL CAMBIO QUE DEVUELVE ES EL CAMBIO DE LA MONEDA EN QUE SE GUARD� LA OFERTA RESPECTO A LA MONEDA DEL PROCESO" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL, G.IMPPARCIAL , @CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE ID= @ID" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO ,IP.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_GR_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.ITEM,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO,IP.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIB G WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MON_COD]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[MON_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM MON WITH(NOLOCK) WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE MON SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS_MON SET MON_PORTAL=@NEW WHERE MON_PORTAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VOLADJ SET MONPOR=@NEW WHERE MONPOR=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_VISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_VISIBLE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ACT_VISIBLE] @CIA int,@IDI varchar(20),@PYME varchar(3) = ''" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #mraiz contendr� las actividades pendientes de confirmar por el gerente del portal y en caso" & vbCrLf
sConsulta = sConsulta & "--de que no haya nada pendiente, contendr� una copia de #raiz" & vbCrLf
sConsulta = sConsulta & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4, act5 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
sConsulta = sConsulta & "  from cias_act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3,ACT4" & vbCrLf
sConsulta = sConsulta & "                     from cias_act4 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                     from cias_act3 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 e WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
sConsulta = sConsulta & "  from cias_act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                     from cias_act3 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
sConsulta = sConsulta & "  from cias_act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2 " & vbCrLf
sConsulta = sConsulta & "  from cias_act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1) " & vbCrLf
sConsulta = sConsulta & "select act1 " & vbCrLf
sConsulta = sConsulta & "  from cias_act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Insertamos los datos correspondientes a #mraiz" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4, act5 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3,ACT4" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act4 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA " & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act3 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 e WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act3 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2 " & vbCrLf
sConsulta = sConsulta & "  from mcias_act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1) " & vbCrLf
sConsulta = sConsulta & "select act1 " & vbCrLf
sConsulta = sConsulta & "  from mcias_act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si no hab�a actividades pendientes de confirmar, se copia #raiz en #mraiz" & vbCrLf
sConsulta = sConsulta & "if (select count(*) from #mraiz WITH(NOLOCK))=0" & vbCrLf
sConsulta = sConsulta & "  insert into #mraiz select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--La tabla #base va a contener las actividades de las cuales se obtendr�n la " & vbCrLf
sConsulta = sConsulta & "--estructura completa (con asignadas, pendientes y hermanas de estas)" & vbCrLf
sConsulta = sConsulta & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,act3,act4,min(act5) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK) where act5 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK) where act5 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3,act4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,act3,min(act4) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK) where act4 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK) where act4 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,min(act3) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK) where act3 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK) where act3 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2)" & vbCrLf
sConsulta = sConsulta & "  select act1,min(act2) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK) where act2 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK) where act2 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1)" & vbCrLf
sConsulta = sConsulta & "  select min(act1) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK)) a" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--La tabla #Estructura contendr� toda la estructura completa de actividades asignadas, pendientes y hermanas" & vbCrLf
sConsulta = sConsulta & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.act4,a.id" & vbCrLf
sConsulta = sConsulta & "  from act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     inner join #base b WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
sConsulta = sConsulta & "            and a.act3 = b.act3 " & vbCrLf
sConsulta = sConsulta & "            and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & " Where b.act5 Is Not Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.id" & vbCrLf
sConsulta = sConsulta & "  from act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     inner join #base b WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
sConsulta = sConsulta & "            and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & " Where b.act5 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act4 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
sConsulta = sConsulta & "                      and a.act3=c.act3 " & vbCrLf
sConsulta = sConsulta & "                      and a.id=c.act4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.id" & vbCrLf
sConsulta = sConsulta & "  from act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     inner join #base b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2" & vbCrLf
sConsulta = sConsulta & " Where b.act4 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act3 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
sConsulta = sConsulta & "                      and a.id=c.act3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.id" & vbCrLf
sConsulta = sConsulta & "  from act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     inner join #base b WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & " Where b.act3 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act2 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.id = c.act2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if (@PYME='')" & vbCrLf
sConsulta = sConsulta & "  insert into #Estructura (Act1)" & vbCrLf
sConsulta = sConsulta & "  select a.id" & vbCrLf
sConsulta = sConsulta & "    from act1 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                       FROM #Estructura c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                      where a.id=c.act1)" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  insert into #Estructura (Act1)" & vbCrLf
sConsulta = sConsulta & "  select a.id" & vbCrLf
sConsulta = sConsulta & "    from act1 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                       FROM #Estructura c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                      where a.id=c.act1)" & vbCrLf
sConsulta = sConsulta & "  and a.PYME = @PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizamos los campos actAsign y actPend con el valor correspondiente. Estos campos" & vbCrLf
sConsulta = sConsulta & "--contendr�n el nivel al que la actividad est� asignada (actAsign) o en el caso de que est� pendiente (actPend), el nivel" & vbCrLf
sConsulta = sConsulta & "-- al que lo est�" & vbCrLf
sConsulta = sConsulta & "update #Estructura set actAsign=0 ,actPend=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "select #Estructura.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
sConsulta = sConsulta & "       actasign, actpend" & vbCrLf
sConsulta = sConsulta & "from #Estructura WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     left join act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "     left join act2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act2.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "     left join act3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act3.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act3.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "     left join act4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act4.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act4.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act4.act3 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "     left join act5 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act5.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act5.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act5.act3 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act4 = act5.act4 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIA_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIA_ESP]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIA_ESP] @IDCIA int, @NOM varchar(300), @COM varchar(500) , @IDESP int output AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDESP = 1" & vbCrLf
sConsulta = sConsulta & "select @IDESP = ISNULL(ID,0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA, ID, NOM, COM) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA, @IDESP, @NOM, @COM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AUTORIZAR_PREMIUM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AUTORIZAR_PREMIUM]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_AUTORIZAR_PREMIUM] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = @LNKSERVER + 'SP_ACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET PREMIUM =2  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PREM_ACTIVO =1 WHERE CIA_PROVE = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CAMBIO_NIVEL_ACT_CIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CAMBIO_NIVEL_ACT_CIAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_CAMBIO_NIVEL_ACT_CIAS] (@NIVEL INTEGER, @RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL_SUP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL_INF AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERROR AS INTEGER" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL = 2 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_INF =  COUNT( DISTINCT (ACT1))  FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   SELECT  ACT2.ACT1,ACT2.ID FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT3 WITH(NOLOCK) ON ACT3.ACT1 = ACT2.ACT1 AND ACT3.ACT2 = ACT2.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT2.ACT1,ACT2.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL=4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   SELECT ACT3.ACT1,ACT3.ACT2,ACT3.ID FROM ACT3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT4 WITH(NOLOCK) ON ACT4.ACT1 = ACT3.ACT1 AND ACT4.ACT2 = ACT3.ACT2 AND ACT4.ACT3=ACT3.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT3.ACT2,ACT3.ACT1,ACT3.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL=5" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   SELECT ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID FROM ACT4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT5 WITH(NOLOCK) ON ACT5.ACT1 = ACT4.ACT1 AND ACT5.ACT2 = ACT4.ACT2 AND ACT5.ACT3=ACT4.ACT3 AND ACT5.ACT4=ACT4.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT4.ACT3,ACT4.ACT2,ACT4.ACT1,ACT4.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL_INF < @NIVEL_SUP " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ERROR = 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "IF @ERROR = 0 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 2 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                DELETE FROM CIAS_ACT3 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT3" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 5" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                DELETE FROM CIAS_ACT3 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT4 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT4 " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET @RES=@ERROR" & vbCrLf
sConsulta = sConsulta & "RETURN " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DESAUTORIZAR_PREMIUM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DESAUTORIZAR_PREMIUM]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DESAUTORIZAR_PREMIUM] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = @LNKSERVER + 'SP_DESACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET PREMIUM = 3  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PREM_ACTIVO = 0 WHERE CIA_PROVE = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ACTIVIDADES_CIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ACTIVIDADES_CIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_ACTIVIDADES_CIA] @CIA int,@IDI varchar(20),@TODO bit = 0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int , ActAsign smallint)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @TODO = 0" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    --Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, act5, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, act5, 5" & vbCrLf
sConsulta = sConsulta & "      from cias_act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2,ACT3,ACT4" & vbCrLf
sConsulta = sConsulta & "                         from cias_act4 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                          and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                         from cias_act3 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 e WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, 4  " & vbCrLf
sConsulta = sConsulta & "      from cias_act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                         from cias_act3 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, 3" & vbCrLf
sConsulta = sConsulta & "      from cias_act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2 , 2" & vbCrLf
sConsulta = sConsulta & "      from cias_act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, 1 " & vbCrLf
sConsulta = sConsulta & "      from cias_act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    --Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, act5, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, act5, 5" & vbCrLf
sConsulta = sConsulta & "      from cias_act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, 4  " & vbCrLf
sConsulta = sConsulta & "      from cias_act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, 3" & vbCrLf
sConsulta = sConsulta & "      from cias_act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2 , 2" & vbCrLf
sConsulta = sConsulta & "      from cias_act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, 1 " & vbCrLf
sConsulta = sConsulta & "      from cias_act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "select #raiz.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
sConsulta = sConsulta & "       #raiz.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
sConsulta = sConsulta & "       actasign" & vbCrLf
sConsulta = sConsulta & "from #raiz WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     left join act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "     left join act2 WITH(NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act2.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "     left join act3 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act3.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act3.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "     left join act4 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act4.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act4.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act4.act3 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "     left join act5 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act5.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act5.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act5.act3 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act4 = act5.act4 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "order by #raiz.act1, #raiz.act2,#raiz.act3,#raiz.act4,#raiz.act5" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB  @CIA_COMP INTEGER, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_PROCESO @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROCESOS_PROVE] @CIACOMP INT, @PROVE VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,CIA_COMP,ANYO,GMN1,PROCE,PROVE,CIA_PROVE,USU,FECREC,FECVAL,MON,CAMBIO,OBS,OBSADJUN,NUMOBJ,ERROR,ADJUN,ENVIANDOSE,IMPORTE,IMPORTE_BRUTO " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP = @CIACOMP" & vbCrLf
sConsulta = sConsulta & "   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS] @PROVE int  AS" & vbCrLf
sConsulta = sConsulta & "SELECT A.ID IDCIA, A.COD COD, A.DEN DEN," & vbCrLf
sConsulta = sConsulta & "CASE WHEN B.CIA_COMP IS NULL THEN 0 ELSE 1 END REGISTRADA," & vbCrLf
sConsulta = sConsulta & "CASE WHEN C.CIA_COMP IS NULL THEN 0 ELSE 1 END SOLICITADA" & vbCrLf
sConsulta = sConsulta & "FROM CIAS A WITH(NOLOCK) LEFT JOIN REL_COMP B WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON A.ID = B.CIA_COMP" & vbCrLf
sConsulta = sConsulta & "AND @PROVE = B.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MREL_COMP C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON A.ID = C.CIA_COMP" & vbCrLf
sConsulta = sConsulta & "AND @PROVE = C.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "Where A.FCEST = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL] @CIA INT,@ID_REG INT, @ADJUN INT,@INIT INT,@SIZE INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM REGISTRO_EMAIL_ADJUN WITH(NOLOCK) WHERE CIA=@CIA AND ID_REG=@ID_REG AND ID=@ADJUN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  REGISTRO_EMAIL_ADJUN.DATA @textpointer @INIT  @LSIZE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_COMPANIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL_ADJUN WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "--- 2.9.5" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "------------" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_REGISTRAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_REGISTRAR_COMPANIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_REGISTRAR_COMPANIA]  @IDPORTAL int, @COD varchar(50) , @DEN varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @DIR varchar(255), @CP varchar(20), @POB varchar(100), @PAI INT, " & vbCrLf
sConsulta = sConsulta & "                 @PROVI INT, @MON INT, @FCEST tinyint, @FPEST tinyint, @NIF varchar(20)," & vbCrLf
sConsulta = sConsulta & "                 @VOLFACT float, @CLIREF1 varchar(50),@CLIREF2 varchar(50),@CLIREF3 varchar(50),@CLIREF4 varchar(50)," & vbCrLf
sConsulta = sConsulta & "                 @HOM1 varchar(50), @HOM2 varchar(50), @HOM3 varchar(50), @URLCIA varchar(255)," & vbCrLf
sConsulta = sConsulta & "                 @COMENT text, @ORIGINS varchar(50), @PREMIUM int, @TIPOACC tinyint ,@IDI varchar(50), @IDCIA int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIDI int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDCIA=ISNULL(MAX(ID),0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM CIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
sConsulta = sConsulta & "  FROM IDI WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE COD = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS (ID,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,IDI,PORT,FCEST,NIF,PREMIUM,VOLFACT, " & vbCrLf
sConsulta = sConsulta & "                  CLIREF1,CLIREF2,CLIREF3,CLIREF4,HOM1,HOM2,HOM3,URLCIA,COMENT,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA,@COD,@DEN,@DIR,@CP,@POB,@PAI,@PROVI,@MON,@IDIDI,@IDPORTAL,@FCEST,@NIF,@PREMIUM,@VOLFACT ," & vbCrLf
sConsulta = sConsulta & "                  @CLIREF1, @CLIREF2, @CLIREF3, @CLIREF4, @HOM1, @HOM2, @HOM3, @URLCIA, @COMENT, @ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_PORT (CIA,PORT,FPEST,TIPOACC,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA,@IDPORTAL,@FPEST,@TIPOACC, @ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO rel_comp (cia_prove, cia_comp) " & vbCrLf
sConsulta = sConsulta & "SELECT @IDCIA, ID" & vbCrLf
sConsulta = sConsulta & "  FROM CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE FCEST=3" & vbCrLf
sConsulta = sConsulta & "   AND PORT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_REGISTRAR_USUARIO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_REGISTRAR_USUARIO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_REGISTRAR_USUARIO] @CIA int, @IDPORTAL int, @COD varchar(50) , @PWD varchar(128), @FECHA datetime, " & vbCrLf
sConsulta = sConsulta & "                 @APE varchar(100), @NOM varchar(20), @DEP varchar(100), @CAR varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @TFNO varchar(20), @TFNO2 varchar(20), @FAX varchar(20), @EMAIL varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @IDI varchar(20), @TFNO_MOVIL varchar(20), @TIPOEMAIL smallint, @FCEST tinyint, @FPEST tinyint, @ORIGINS varchar(50),  " & vbCrLf
sConsulta = sConsulta & "                 @PRINCIPAL bit AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU int" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIDI int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU=ISNULL(MAX(ID),0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM USU" & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
sConsulta = sConsulta & "  FROM IDI WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE COD = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU (CIA,ID,COD,PWD,FECPWD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
sConsulta = sConsulta & "                 FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL) " & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA, @IDUSU, @COD, @PWD, @FECHA, @APE, @NOM, @DEP, @CAR, @TFNO, @TFNO2," & vbCrLf
sConsulta = sConsulta & "        @FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU_PORT (CIA,USU,PORT,FPEST,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA,@IDUSU,@IDPORTAL,@FPEST,@ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRINCIPAL = 1 " & vbCrLf
sConsulta = sConsulta & "  UPDATE CIAS SET USUPPAL=@IDUSU WHERE ID=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRANSFER_CIA_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRANSFER_CIA_ESP]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_TRANSFER_CIA_ESP] @IDCIA INTEGER, @ESP SMALLINT, @INIT INT, @SIZE INT, @DATA IMAGE AS" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @IDCIA AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
sConsulta = sConsulta & "    UPDATE CIAS_ESP SET DATA = @DATA WHERE CIA = @IDCIA AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    UPDATETEXT CIAS_ESP.DATA @textpointer @init 0 @data" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_13_12A31900_03_13_13() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_013
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.13'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_12A31900_03_13_13 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_12A31900_03_13_13 = False
End Function

Sub V_31900_3_Storeds_013()
Dim sConsulta As String
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTINVISIBLEFIELDS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTINVISIBLEFIELDS] @CIA INT,  @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL,@NUEVO_WORKFL TINYINT=0    AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL ',  @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVEGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_13_13A31900_03_13_14() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_014
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.14'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_13A31900_03_13_14 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_13A31900_03_13_14 = False
End Function

Sub V_31900_3_Storeds_014()
Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_ITEM]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_ITEM] (@ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @GRUPO VARCHAR(50), @ITEM INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ADJUN OA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_OFE P WITH(NOLOCK) ON OA.ID=P.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND OA.ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB OA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_OFE P WITH(NOLOCK) ON OA.ID=P.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND OA.ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "   FROM ITEM_OFE I WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_OFE P WITH(NOLOCK) ON I.ID=P.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND I.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_03_13_14A31900_03_13_15() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_015
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.15'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_14A31900_03_13_15 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_14A31900_03_13_15 = False
End Function

Sub V_31900_3_Storeds_015()
Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS] " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @PROCE SMALLINT," & vbCrLf
sConsulta = sConsulta & "   @COD_OLD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @COD_NEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @OFE_GR_ATRIB TABLE (ID INT,GRUPO VARCHAR(50),ATRIB_ID INT,VALOR_NUM FLOAT,VALOR_TEXT VARCHAR(800)," & vbCrLf
sConsulta = sConsulta & "                                VALOR_FEC DATETIME,VALOR_BOOL TINYINT,IMPPARCIAL FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   --Antes del borrado se hace una inserci�n en la var. tabla @OFE_GR_ATRIB porque GRUPO es una foreign key a GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO @OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ATRIB     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- GRUPO_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE GRUPO_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=GRUPO_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND GRUPO_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- ITEM_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE ITEM_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=ITEM_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND ITEM_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ADJUN ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ADJUN,OGA.NOM,OGA.COM,OGA.IDPORTAL,OGA.IDGS,OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ADJUN     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ---------- " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,OGA.GRUPO,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM @OFE_GR_ATRIB OGA      " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_COSTESDESC ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "   SELECT OGC.ID,@COD_NEW,OGC.ATRIB_ID,OGC.IMPPARCIAL,OGC.FECACT" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_COSTESDESC    " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_ITEM_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ITEM_ATRIB SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OFE_ITEM_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OFE_ITEM_ATRIB.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_03_13_15A31900_03_13_16() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_016
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.16'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_15A31900_03_13_16 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_15A31900_03_13_16 = False
End Function

Sub V_31900_3_Storeds_016()
Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROVE_ESP]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROVE_ESP] " & vbCrLf
sConsulta = sConsulta & "   @IDPROVE int output, " & vbCrLf
sConsulta = sConsulta & "   @IDESP int = NULL, " & vbCrLf
sConsulta = sConsulta & "   @CODPROVE varchar(50) = null " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @IDPROVE is null" & vbCrLf
sConsulta = sConsulta & "   select @IDPROVE = ID FROM CIAS WITH (NOLOCK) WHERE COD = @CODPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDESP IS NULL" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact , datalength(data) TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact, datalength(data)  TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "   AND ID = @IDESP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINVISIBLEFIELDS]"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
sConsulta = sConsulta & "   @ID INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINVISIBLEFIELDS @ID=@ID,@SOLIC_PROVE=2 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_03_13_16A31900_03_13_17() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_017
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.17'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_16A31900_03_13_17 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_16A31900_03_13_17 = False
End Function

Sub V_31900_3_Storeds_017()
Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETADJUNTO_CONTRATO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETADJUNTO_CONTRATO]"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO] @CIA INT, @ID INT =NULL, @ID_CONTRATO INT =NULL, @ID_CAMPO INT=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTO_CONTRATO @ID=@ID,@ID_CONTRATO=@ID_CONTRATO,@ID_CAMPO=@ID_CAMPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID  INT, @ID_CONTRATO INT, @ID_CAMPO INT ', @ID=@ID,@ID_CONTRATO=@ID_CONTRATO,@ID_CAMPO=@ID_CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_03_13_17A31900_03_13_18() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
         
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_3_Storeds_018
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.13.18'" & _
                ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_13_17A31900_03_13_18 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_13_17A31900_03_13_18 = False
End Function

Sub V_31900_3_Storeds_018()
Dim sConsulta As String
        
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_FORMATO_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_FORMATO_CONTACTO]"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSPM_FORMATO_CONTACTO] @CON_PORTAL INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50),@DATEFMT VARCHAR(50) OUTPUT, @THOUSANFMT CHAR(1) =NULL OUTPUT, @DECIMALFMT CHAR(1)=NULL OUTPUT, @PRECISIONFMT TINYINT =NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WITH(NOLOCK) WHERE COD_PROVE_CIA=@PROVE AND CIA_COMP=@ID_COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DATEFMT=DATEFMT, @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT FROM USU WITH(NOLOCK) WHERE CIA=@ID_CIA AND ID=@CON_PORTAL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub
