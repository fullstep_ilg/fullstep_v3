Attribute VB_Name = "bas_V_31800"
Option Explicit

Public Function CodigoDeActualizacion3_00_03_01A3_00_04_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_1_Storeds_1
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.04.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_03_01A3_00_04_01 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_03_01A3_00_04_01 = False
End Function



Private Sub V_31800_1_Storeds_1()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_JOB_CREARJOBACTPROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_JOB_CREARJOBACTPROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_JOB_CREARJOBACTPROVE (@NOMBRE_JOB VARCHAR(50),@OWNER VARCHAR(20),@NOMBRE_SCH VARCHAR(50),@TIPO INT,@INTERVALO INT,@TIPOPERIODO INT,@FRECUENCIA INT, @FECINI INT,@TIMEINI INT,@TIMEFIN INT,@NOMBRE_STEP VARCHAR(50),@NOMBRE_PROC VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SERVNAME AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @RET_STAT as INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION as varchar(26)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ContadorTareas int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @SERVNAME=@@SERVERNAME" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE_JOB,13,LEN(@NOMBRE_JOB)-12)" & vbCrLf
sConsulta = sConsulta & "   --Comprobacion de si existe la tarea." & vbCrLf
sConsulta = sConsulta & "   SELECT @ContadorTareas = COUNT(Name) FROM msdb.dbo.SysJobs with (nolock) WHERE Name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "IF @ContadorTareas<>0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si existe la tarea..." & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPOPERIODO,@freq_subday_interval=@FRECUENCIA, @active_start_date=@FECINI,@active_start_time =@TIMEINI,@active_end_time =@TIMEFIN" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si NO existe la tarea...Lo crea." & vbCrLf
sConsulta = sConsulta & "    EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE_JOB,@owner_login_name = @OWNER" & vbCrLf
sConsulta = sConsulta & "       IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                   SET @version = replace(left(@@version,26),' ','')" & vbCrLf
sConsulta = sConsulta & "                   IF @VERSION = 'MicrosoftSQLServer2000'" & vbCrLf
sConsulta = sConsulta & "                       SET @COMANDO='osql -E -S ' + @SERVNAME + ' -Q ''Exec ' + @BD + '.dbo.' + @NOMBRE_PROC + ''''" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                           SET @COMANDO='sqlcmd -E -S ' + @SERVNAME + ' -Q ''Exec ' + @BD + '.dbo.' + @NOMBRE_PROC + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'CMDEXEC', @command =@COMANDO" & vbCrLf
sConsulta = sConsulta & "                    IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                               EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE_JOB,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "                               IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                                       EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPOPERIODO,@freq_subday_interval=@FRECUENCIA, @active_start_date=@FECINI,@active_start_time =@TIMEINI,@active_end_time =@TIMEFIN" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_JOB_CREARJOBTRUNCARLOG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_JOB_CREARJOBTRUNCARLOG]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE SP_JOB_CREARJOBTRUNCARLOG (@NOMBRE_JOB VARCHAR(50),@NOMBRE_SCH VARCHAR(50),@TIPO INT,@INTERVALO INT, @FECINI INT,@TIMEINI INT,@NOMBRE_STEP VARCHAR(50),@NOMBRE_PROC VARCHAR(50),  @FRECRECFACT INT = 0) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SERVNAME AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "declare @RET_STAT as INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ContadorTareas int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @SERVNAME=@@SERVERNAME" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE_JOB,13,LEN(@NOMBRE_JOB)-12)" & vbCrLf
sConsulta = sConsulta & "   --Comprobacion de si existe la tarea." & vbCrLf
sConsulta = sConsulta & "   SELECT @ContadorTareas = COUNT(Name) FROM msdb.dbo.SysJobs with (nolock) WHERE Name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "    IF @ContadorTareas<>0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si existe la tarea..." & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO, @freq_recurrence_factor=@FRECRECFACT,@active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   else" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       --Si NO existe la tarea...Lo crea." & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "   IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         SET @COMANDO=@NOMBRE_PROC" & vbCrLf
sConsulta = sConsulta & "         EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'TSQL', @command =@COMANDO,@database_name=@BD" & vbCrLf
sConsulta = sConsulta & "         IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE_JOB,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                   EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO, @freq_recurrence_factor=@FRECRECFACT,@active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion3_00_04_01A3_00_04_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_1_Storeds_2
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.04.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_04_01A3_00_04_02 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_04_01A3_00_04_02 = False
End Function



Private Sub V_31800_1_Storeds_2()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_JOB_CREARJOBACTPROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_JOB_CREARJOBACTPROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_JOB_CREARJOBACTPROVE (@NOMBRE_JOB VARCHAR(50),@OWNER VARCHAR(20),@NOMBRE_SCH VARCHAR(50),@TIPO INT,@INTERVALO INT,@TIPOPERIODO INT,@FRECUENCIA INT, @FECINI INT,@TIMEINI INT,@TIMEFIN INT,@NOMBRE_STEP VARCHAR(50),@NOMBRE_PROC VARCHAR(50),@NOMBRE_PROXY VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SERVNAME AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @RET_STAT as INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION as varchar(26)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ContadorTareas int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @SERVNAME=@@SERVERNAME" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE_JOB,13,LEN(@NOMBRE_JOB)-12)" & vbCrLf
sConsulta = sConsulta & "   --Comprobacion de si existe la tarea." & vbCrLf
sConsulta = sConsulta & "   SELECT @ContadorTareas = COUNT(Name) FROM msdb.dbo.SysJobs with (nolock) WHERE Name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "IF @ContadorTareas<>0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si existe la tarea..." & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPOPERIODO,@freq_subday_interval=@FRECUENCIA, @active_start_date=@FECINI,@active_start_time =@TIMEINI,@active_end_time =@TIMEFIN" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si NO existe la tarea...Lo crea." & vbCrLf
sConsulta = sConsulta & "    EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE_JOB,@owner_login_name = @OWNER" & vbCrLf
sConsulta = sConsulta & "       IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                   SET @version = replace(left(@@version,26),' ','')" & vbCrLf
sConsulta = sConsulta & "                   IF @VERSION = 'MicrosoftSQLServer2000'" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SET @COMANDO='osql -E -S ' + @SERVNAME + ' -Q " & Chr(34) & "Exec ' + @BD + '.dbo.' + @NOMBRE_PROC + '" & Chr(34) & "'" & vbCrLf
sConsulta = sConsulta & "               EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'CMDEXEC', @command =@COMANDO" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @COMANDO='sqlcmd -E -S ' + @SERVNAME + ' -Q " & Chr(34) & "Exec ' + @BD + '.dbo.' + @NOMBRE_PROC + '" & Chr(34) & "'" & vbCrLf
sConsulta = sConsulta & "                       EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'CMDEXEC', @command =@COMANDO,@proxy_name=@NOMBRE_PROXY" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                               EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE_JOB,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "                               IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                                       EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPOPERIODO,@freq_subday_interval=@FRECUENCIA, @active_start_date=@FECINI,@active_start_time =@TIMEINI,@active_end_time =@TIMEFIN" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion3_00_04_02A3_00_04_03()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_1_Storeds_3
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.04.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_04_02A3_00_04_03 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_04_02A3_00_04_03 = False
End Function




Private Sub V_31800_1_Storeds_3()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE SP_DEVOLVER_PROCE_ATRIBESP_WEB @CIA_COMP INTEGER, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @ATRIB int = null, @ITEM int = null AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @AMBITO = @AMBITO, @GRUPO = @GRUPO, @ATRIB = @ATRIB, @ITEM = @ITEM'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @ATRIB int, @ITEM int'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @AMBITO = @AMBITO, @GRUPO = @GRUPO, @ATRIB = @ATRIB, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB @CIA_COMP INTEGER, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint  , @GRUPO varchar(50) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_GRUPO @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint  , @GRUPO varchar(50)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB @CIA_COMP INTEGER, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint , @ITEM smallint AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_ITEM @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @ITEM = @ITEM'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint  , @ITEM smallint'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB  @CIA_COMP INTEGER, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_PROCESO @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub




Public Function CodigoDeActualizacion3_00_04_03A3_00_04_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_1_Storeds_4
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.04.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_04_03A3_00_04_04 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_04_03A3_00_04_04 = False
End Function



Private Sub V_31800_1_Storeds_4()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_PROV_REINTENTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_PROV_REINTENTOS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Intenta pasar los movimientos fallidos si toca       ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                         ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :  ------------------                                                           ******/" & vbCrLf
sConsulta = sConsulta & "/******                                        ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR,@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(1000),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(100),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(100),@CIAS_PROVI VARCHAR(100),@CIAS_CP VARCHAR(100),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(50),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100),@PORT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@ESPEC INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CICLOS= (SELECT PROV_CICLOS FROM PARGEN_GEST) " & vbCrLf
sConsulta = sConsulta & "SET @REINTENTOS = (SELECT PROV_REINTENTOS FROM PARGEN_GEST)" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = (SELECT PROV_PASADAS FROM PARGEN_GEST)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_P INT,@IDI VARCHAR(50),@TIPOEMAIL SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1), @DECIMALFMT CHAR(1), @PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CICLOS <> 0 AND @PASADAS%@CICLOS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "    SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "    WHERE MVTOS_PROV_EST.EST = 1 AND MVTOS_PROV_EST.INTENTOS < @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "    OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                     SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                     FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "      SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "            FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          END                                          " & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "        ELSE  " & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@USU' " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= '@COD_PROVE_CIA VARCHAR(200),@ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "          END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT FPEST FROM USU_PORT WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3 " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                         @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "              FROM USU " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN IDI AS IDI ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "                                         WHERE USU.CIA = @CIA_PROVE AND USU.ID= @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                               @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                               @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                               @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                               @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                               @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                               @DATEFMT=@DATEFMT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_DEP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                           @ID_P INT," & vbCrLf
sConsulta = sConsulta & "                           @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "                           @THOUSANFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                           @DECIMALFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                           @PRECISIONFMT TINYINT, " & vbCrLf
sConsulta = sConsulta & "                           @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "              EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                   @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                   @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                   @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                   @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                   @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                   @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                   @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                   @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                   @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                   @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                   @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            END     " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT FPEST FROM USU_PORT WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3 " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                         @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT" & vbCrLf
sConsulta = sConsulta & "              FROM USU INNER JOIN IDI AS IDI ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "                                         WHERE  USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "              SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                  @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                  @DATEFMT=@DATEFMT'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "              SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT," & vbCrLf
sConsulta = sConsulta & "                              @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                              @DECIMALFMT CHAR(1), " & vbCrLf
sConsulta = sConsulta & "                              @PRECISIONFMT TINYINT, " & vbCrLf
sConsulta = sConsulta & "                              @DATEFMT VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                      @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                      @TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @THOUSANFMT=@THOUSANFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @DECIMALFMT=@DECIMALFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                                      @DATEFMT=@DATEFMT" & vbCrLf
sConsulta = sConsulta & "            END                                        " & vbCrLf
sConsulta = sConsulta & "          END " & vbCrLf
sConsulta = sConsulta & "        END " & vbCrLf
sConsulta = sConsulta & "        ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "        BEGIN   " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "                      @DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "            @DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "            EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "            EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "        END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR <> 0            " & vbCrLf
sConsulta = sConsulta & "        BEGIN " & vbCrLf
sConsulta = sConsulta & "          SET @INTENTO = @INTENTO +1" & vbCrLf
sConsulta = sConsulta & "          IF @INTENTO = @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "            SET @EST_FILA = 2" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @EST_FILA = 3            " & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "    CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "  UPDATE PARGEN_GEST SET PROV_PASADAS = @PASADAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion3_00_04_04A3_00_04_05()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_1_Storeds_5
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.04.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_04_04A3_00_04_05 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_04_04A3_00_04_05 = False
End Function


Private Sub V_31800_1_Storeds_5()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VER_PUNTUACION_CALIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VER_PUNTUACION_CALIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE FSQA_VER_PUNTUACION_CALIDAD @CODPROVE NVARCHAR(100),@CODCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODPROVEGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CODCIA  AND PORT =0  --las tablas de calidad est�n en el GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_VER_PUNTUACION_CALIDAD  @PROVE=@CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SELECT @CODPROVEGS=COD_PROVE_CIA FROM REL_CIAS R INNER JOIN CIAS C ON C.ID=R.CIA_PROVE WHERE C.COD=@CODPROVE" & vbCrLf
sConsulta = sConsulta & "--PRINT @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CODPROVE varchar(50) ', @CODPROVE=@CODPROVEGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion3_00_04_05A3_00_04_06()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_1_Storeds_6
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.04.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_04_05A3_00_04_06 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_04_05A3_00_04_06 = False
End Function



Private Sub V_31800_1_Storeds_6()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_JOB_CREARJOBACTPROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_JOB_CREARJOBACTPROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_JOB_CREARJOBACTPROVE (@NOMBRE_JOB VARCHAR(50),@OWNER VARCHAR(20),@NOMBRE_SCH VARCHAR(50),@TIPO INT,@INTERVALO INT,@TIPOPERIODO INT,@FRECUENCIA INT, @FECINI INT,@TIMEINI INT,@TIMEFIN INT,@NOMBRE_STEP VARCHAR(50),@NOMBRE_PROC VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SERVNAME AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @RET_STAT as INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION as varchar(26)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ContadorTareas int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @SERVNAME=@@SERVERNAME" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE_JOB,13,LEN(@NOMBRE_JOB)-12)" & vbCrLf
sConsulta = sConsulta & "   --Comprobacion de si existe la tarea." & vbCrLf
sConsulta = sConsulta & "   SELECT @ContadorTareas = COUNT(Name) FROM msdb.dbo.SysJobs with (nolock) WHERE Name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "IF @ContadorTareas<>0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si existe la tarea..." & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPOPERIODO,@freq_subday_interval=@FRECUENCIA, @active_start_date=@FECINI,@active_start_time =@TIMEINI,@active_end_time =@TIMEFIN" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si NO existe la tarea...Lo crea." & vbCrLf
sConsulta = sConsulta & "    EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE_JOB,@owner_login_name = @OWNER" & vbCrLf
sConsulta = sConsulta & "       IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                   SET @version = replace(left(@@version,26),' ','')" & vbCrLf
sConsulta = sConsulta & "                   IF @VERSION = 'MicrosoftSQLServer2000'" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SET @COMANDO='osql -E -S ' + @SERVNAME + ' -Q " & Chr(34) & "Exec ' + @BD + '.dbo.' + @NOMBRE_PROC + '" & Chr(34) & "'" & vbCrLf
sConsulta = sConsulta & "               EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'CMDEXEC', @command =@COMANDO" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @COMANDO=@NOMBRE_PROC" & vbCrLf
sConsulta = sConsulta & "                                                     EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'TSQL', @command =@COMANDO,@database_name=@BD" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                               EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE_JOB,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "                               IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                                       EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPOPERIODO,@freq_subday_interval=@FRECUENCIA, @active_start_date=@FECINI,@active_start_time =@TIMEINI,@active_end_time =@TIMEFIN" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion3_00_04_06A3_00_04_07()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_1_Storeds_7
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.04.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_04_06A3_00_04_07 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_04_06A3_00_04_07 = False
End Function


Private Sub V_31800_1_Storeds_7()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_JOB_CREARJOBTRUNCARLOG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_JOB_CREARJOBTRUNCARLOG]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_JOB_CREARJOBTRUNCARLOG (@NOMBRE_JOB VARCHAR(50),@NOMBRE_SCH VARCHAR(50),@TIPO INT,@INTERVALO INT, @FECINI INT,@TIMEINI INT,@NOMBRE_STEP VARCHAR(50),@NOMBRE_PROC VARCHAR(50),  @FRECRECFACT INT = 0) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SERVNAME AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "declare @RET_STAT as INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ContadorTareas int" & vbCrLf
sConsulta = sConsulta & "DECLARE @ContadorSchedule int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @SERVNAME=@@SERVERNAME" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE_JOB,13,LEN(@NOMBRE_JOB)-12)" & vbCrLf
sConsulta = sConsulta & "   --Comprobacion de si existe la tarea." & vbCrLf
sConsulta = sConsulta & "   SELECT @ContadorTareas = COUNT(Name) FROM msdb.dbo.SysJobs with (nolock) WHERE Name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "    IF @ContadorTareas<>0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si existe la tarea." & vbCrLf
sConsulta = sConsulta & "     SELECT @ContadorSchedule= Count(SJ.Name) FROM msdb.dbo.SysJobs SJ with (nolock) " & vbCrLf
sConsulta = sConsulta & "                 inner join msdb.dbo.sysjobschedules SJO with (nolock) on SJ.job_id = SJO.job_id" & vbCrLf
sConsulta = sConsulta & "          WHERE SJ.Name=@NOMBRE_JOB --AND SJO.Name=@NOMBRE_SCH" & vbCrLf
sConsulta = sConsulta & "     IF @ContadorSchedule =  0 " & vbCrLf
sConsulta = sConsulta & "     --Si no existe el schedule" & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "        EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO, @freq_recurrence_factor=@FRECRECFACT,@active_start_date=@FECINI,@active_start_time =@TIMEINI   " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO, @freq_recurrence_factor=@FRECRECFACT,@active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   else" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       --Si NO existe la tarea...Lo crea." & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "   IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         SET @COMANDO=@NOMBRE_PROC" & vbCrLf
sConsulta = sConsulta & "         EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'TSQL', @command =@COMANDO,@database_name=@BD" & vbCrLf
sConsulta = sConsulta & "         IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE_JOB,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                   EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO, @freq_recurrence_factor=@FRECRECFACT,@active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
