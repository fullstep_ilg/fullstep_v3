Attribute VB_Name = "bas_V_31900_9"
Option Explicit

Public Function CodigoDeActualizacion31900_04_18_31A31900_08_19_00(Optional ByVal Parcial As Boolean = False) As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    
    If Not Parcial Then
    
        ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
        ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
        bTransaccionEnCurso = True
        
        frmProgreso.lblDetalle = "Cambios en tablas"
        frmProgreso.lblDetalle.Refresh
        If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
        V_31900_9_Tablas_000
    
        'Introducimos el nuevo valor de versi�n
        sConsulta = "UPDATE [dbo].[VERSION] SET NUM='-3.00.19.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        ExecuteSQL gRDOCon, sConsulta
    
        ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
        bTransaccionEnCurso = False
    End If
    
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Datos_000

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Storeds_000

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.19.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_04_18_31A31900_08_19_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_04_18_31A31900_08_19_00 = False
End Function


Private Sub V_31900_9_Storeds_000()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @IDI AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CERTIFICADOS_PROVE @PROVE= @PROVE, @IDI = @IDI'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@IDI VARCHAR(3)',@PROVE= @PROVE, @IDI = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =118 AND ((W.ID>=9 AND W.ID<=13) OR W.ID=21 OR W.ID=27) ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub

Private Sub V_31900_9_Tablas_000()
Dim sConsulta As String

sConsulta = "ALTER TABLE [dbo].[ACT1] ADD [DEN_FRA] [varchar] (1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ACT2] ADD [DEN_FRA] [varchar] (1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ACT3] ADD [DEN_FRA] [varchar] (1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ACT4] ADD [DEN_FRA] [varchar] (1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ACT5] ADD [DEN_FRA] [varchar] (1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[WEBFSPMTEXT] ADD [TEXT_FRA] [nvarchar](2000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[WEBTEXT] ADD [FRA] [TEXT] NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[WEBTEXTHELP] ADD [FRA] [TEXT] NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_9_Datos_000()
Dim sConsulta As String

sConsulta = "UPDATE ACT1 SET DEN_FRA=DEN_SPA" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT1] ALTER COLUMN [DEN_FRA] [varchar] (1000) NOT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE ACT2 SET DEN_FRA=DEN_SPA" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT2] ALTER COLUMN [DEN_FRA] [varchar] (1000) NOT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE ACT3 SET DEN_FRA=DEN_SPA" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT3] ALTER COLUMN [DEN_FRA] [varchar] (1000) NOT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE ACT4 SET DEN_FRA=DEN_SPA" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT4] ALTER COLUMN [DEN_FRA] [varchar] (1000) NOT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE ACT5 SET DEN_FRA=DEN_SPA" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT5] ALTER COLUMN [DEN_FRA] [varchar] (1000) NOT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE WEBFSPMTEXT SET TEXT_FRA=TEXT_SPA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE WEBTEXT SET FRA=SPA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE WEBTEXTHELP SET FRA=SPA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_08_19_00A31900_08_19_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Tablas_001
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Storeds_001
    
    frmProgreso.lblDetalle = "Cambios en Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Datos_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.19.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_08_19_00A31900_08_19_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_08_19_00A31900_08_19_01 = False
End Function

Private Sub V_31900_9_Storeds_001()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETCENTROS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETCENTROS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETCENTROS] @CIA INT,  @COD VARCHAR(4)=NULL,@COD_ORGCOMPRAS VARCHAR(4)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos los CENTROS" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETCENTROS @COD_ORGCOMPRAS'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD_ORGCOMPRAS VARCHAR(4)=NULL',@COD_ORGCOMPRAS=@COD_ORGCOMPRAS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  " & vbCrLf
    sConsulta = sConsulta & "@USU NVARCHAR(50) = NULL, @CIA INT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PREC_VISIBLE int = NULL, @PREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@BLOQ_FACTURA_VISIBLE INT = NULL, @BLOQ_FACTURA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@NUM_LINEA_VISIBLE INT = NULL, @NUM_LINEA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FACTURA_VISIBLE INT = NULL, @FACTURA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@RECEPTOR_VISIBLE INT = NULL, @RECEPTOR_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPORTE_ALBARAN_VISIBLE INT = NULL, @IMPORTE_ALBARAN_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CODRECEPERP_VISIBLE INT=NULL, @CODRECEPERP_WIDTH FLOAT=NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
    sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
    sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
    sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
    sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la informaci�n del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tama�o suficiente para ser visible pero no molestar" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
    sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
    sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUM_LINEA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUM_LINEA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUM_LINEA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FACTURA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @RECEPTOR_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RECEPTOR_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @RECEPTOR_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RECEPTOR_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "               IF @CODRECEPERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CODRECEPERP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CODRECEPERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CODRECEPERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CODRECEPERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
    sConsulta = sConsulta & "           END" & vbCrLf
    sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUM_LINEA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_VISIBLE = @FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_WIDTH = @FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RECEPTOR_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RECEPTOR_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_WIDTH = @RECEPTOR_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @CODRECEPERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CODRECEPERP_VISIBLE= @CODRECEPERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @CODRECEPERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CODRECEPERP_WIDTH = @CODRECEPERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
    sConsulta = sConsulta & "           END" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
    sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT, "
    sConsulta = sConsulta & "@ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT, "
    sConsulta = sConsulta & "@PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT, "
    sConsulta = sConsulta & "@FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT, "
    sConsulta = sConsulta & "@CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, "
    sConsulta = sConsulta & "@UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, "
    sConsulta = sConsulta & "@IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT, "
    sConsulta = sConsulta & "@CC2_WIDTH FLOAT,@CC2_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @BLOQ_FACTURA_VISIBLE INT, @BLOQ_FACTURA_WIDTH FLOAT, @NUM_LINEA_VISIBLE INT, @NUM_LINEA_WIDTH FLOAT, @FACTURA_VISIBLE INT, @FACTURA_WIDTH FLOAT, @RECEPTOR_VISIBLE INT, @RECEPTOR_WIDTH FLOAT, @IMPORTE_ALBARAN_VISIBLE INT, @IMPORTE_ALBARAN_WIDTH FLOAT,@CODRECEPERP_VISIBLE INT, @CODRECEPERP_WIDTH FLOAT,@ANCHODEFECTO INT'," & vbCrLf
    sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
    sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
    sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @FACTURA_VISIBLE = @FACTURA_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @FACTURA_WIDTH = @FACTURA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @RECEPTOR_WIDTH = @RECEPTOR_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @CODRECEPERP_VISIBLE = @CODRECEPERP_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @CODRECEPERP_WIDTH = @CODRECEPERP_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDUSU AS INT = 0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT [RETRASADO_VISIBLE],[RETRASADO_WIDTH],[ALBARAN_VISIBLE],[ALBARAN_WIDTH],[FRECEP_VISIBLE],[FRECEP_WIDTH],[EMP_VISIBLE],[EMP_WIDTH]" & vbCrLf
    sConsulta = sConsulta & ",[ERP_VISIBLE],[ERP_WIDTH],[PEDIDO_VISIBLE],[PEDIDO_WIDTH],[NUMPEDPROVE_VISIBLE],[NUMPEDPROVE_WIDTH],[FEMISION_VISIBLE],[FEMISION_WIDTH],[FENTREGA_VISIBLE]" & vbCrLf
    sConsulta = sConsulta & ",[FENTREGA_WIDTH],[FENTREGAPROVE_VISIBLE],[FENTREGAPROVE_WIDTH],[ART_VISIBLE],[ART_WIDTH],[CANTPED_VISIBLE]" & vbCrLf
    sConsulta = sConsulta & ",[CANTPED_WIDTH],[CANTREC_VISIBLE],[CANTREC_WIDTH],[CANTPEND_VISIBLE],[CANTPEND_WIDTH],[UNI_VISIBLE]" & vbCrLf
    sConsulta = sConsulta & ",[UNI_WIDTH],[PREC_VISIBLE],[PREC_WIDTH],[IMPPED_VISIBLE],[IMPPED_WIDTH],[IMPREC_VISIBLE],[IMPREC_WIDTH],[IMPPEND_VISIBLE]" & vbCrLf
    sConsulta = sConsulta & ",[IMPPEND_WIDTH],[MON_VISIBLE],[MON_WIDTH],[CC1_VISIBLE],[CC1_WIDTH],[CC2_VISIBLE],[CC2_WIDTH],[CC3_VISIBLE],[CC3_WIDTH]" & vbCrLf
    sConsulta = sConsulta & ",[CC4_VISIBLE],[CC4_WIDTH],[PARTIDA1_VISIBLE],[PARTIDA1_WIDTH],[PARTIDA2_VISIBLE],[PARTIDA2_WIDTH],[PARTIDA3_VISIBLE]" & vbCrLf
    sConsulta = sConsulta & ",[PARTIDA3_WIDTH],[PARTIDA4_VISIBLE],[PARTIDA4_WIDTH], [BLOQ_FACTURA_VISIBLE],[BLOQ_FACTURA_WIDTH],[NUM_LINEA_VISIBLE],[NUM_LINEA_WIDTH],[FACTURA_VISIBLE],[FACTURA_WIDTH],[RECEPTOR_VISIBLE],[RECEPTOR_WIDTH],[IMPORTE_ALBARAN_VISIBLE],[IMPORTE_ALBARAN_WIDTH],[CODRECEPERP_VISIBLE],[CODRECEPERP_WIDTH]" & vbCrLf
    sConsulta = sConsulta & "FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE USU = @IDUSU AND CIA = @CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  " & vbCrLf
    sConsulta = sConsulta & "    @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
    sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
    sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
    sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
    sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
    sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
    sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
    sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
    sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
    sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
    sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
    sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
    sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
    sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
    sConsulta = sConsulta & "   @NUMFACTURA VARCHAR(20) = NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) " & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
    sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO, @NUMPEDIDO=@NUMPEDIDO,@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE,@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP,@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO,@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA,@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE,@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE,@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES,@NUMFACTURA=@NUMFACTURA'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT, @NUMPEDIDO INT,@NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME,@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200),@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME,@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50), @PARTIDASPRES NVARCHAR(MAX), @NUMFACTURA VARCHAR(20)', " & vbCrLf
    sConsulta = sConsulta & "                   @IDIOMA=@IDIOMA," & vbCrLf
    sConsulta = sConsulta & "                   @PROVECOD=@PROVECOD," & vbCrLf
    sConsulta = sConsulta & "                   @ANYO=@ANYO," & vbCrLf
    sConsulta = sConsulta & "                   @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
    sConsulta = sConsulta & "                   @NUMORDEN=@NUMORDEN," & vbCrLf
    sConsulta = sConsulta & "                   @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
    sConsulta = sConsulta & "                   @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
    sConsulta = sConsulta & "                   @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
    sConsulta = sConsulta & "                   @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
    sConsulta = sConsulta & "                   @NUMALBARAN=@NUMALBARAN," & vbCrLf
    sConsulta = sConsulta & "                   @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
    sConsulta = sConsulta & "                   @ARTICULO=@ARTICULO," & vbCrLf
    sConsulta = sConsulta & "                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
    sConsulta = sConsulta & "                   @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                   @CODEMPRESA=@CODEMPRESA," & vbCrLf
    sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
    sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
    sConsulta = sConsulta & "                   @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
    sConsulta = sConsulta & "                   @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
    sConsulta = sConsulta & "                   @CENTRO=@CENTRO," & vbCrLf
    sConsulta = sConsulta & "                   @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
    sConsulta = sConsulta & "                   @NUMFACTURA = @NUMFACTURA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE] " & vbCrLf
    sConsulta = sConsulta & "   @CIA INT," & vbCrLf
    sConsulta = sConsulta & "   @PROVE VARCHAR(50), " & vbCrLf
    sConsulta = sConsulta & "   @IDI AS VARCHAR(20)" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @IDPROVE INT" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
    sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CERTIFICADOS_PROVE @PROVE= @PROVE, @IDI = @IDI'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@IDI VARCHAR(3)',@PROVE= @PROVE, @IDI = @IDI" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =118 AND ((W.ID>=9 AND W.ID<=13) OR W.ID=21 OR W.ID=27 OR W.ID=28) ORDER BY W.ID ASC'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_9_Tablas_001()
    Dim sConsulta As String
    
    sConsulta = "IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'CODRECEPERP_VISIBLE' and Object_ID = Object_ID(N'FSP_CONF_VISOR_RECEPCIONES')) " & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD CODRECEPERP_VISIBLE TINYINT NOT NULL CONSTRAINT DF_FSP_CONF_VISOR_RECEPCIONES_CODRECEPERP_VISIBLE DEFAULT 1 " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
    sConsulta = "IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name = N'CODRECEPERP_WIDTH' and Object_ID = Object_ID(N'FSP_CONF_VISOR_RECEPCIONES')) " & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ADD CODRECEPERP_WIDTH TINYINT NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_9_Datos_001()
    Dim sConsulta As String
    
    sConsulta = "IF EXISTS(SELECT EXT FROM UPLOADEXT WHERE EXT='XLSM') " & vbCrLf
    sConsulta = sConsulta & "                UPDATE UPLOADEXT SET BLOQUEADA = 1 WHERE EXT='XLSM'" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "                INSERT INTO UPLOADEXT (EXT,BLOQUEADA,FECACT) VALUES ('XLSM',1,GETDATE())" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_08_19_01A31900_08_19_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Tablas_002
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.19.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_08_19_01A31900_08_19_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_08_19_01A31900_08_19_02 = False
End Function

Private Sub V_31900_9_Tablas_002()
    Dim sConsulta As String
    
    sConsulta = "IF EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'CODRECEPERP_WIDTH' AND Object_ID = Object_ID(N'FSP_CONF_VISOR_RECEPCIONES'))" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE FSP_CONF_VISOR_RECEPCIONES ALTER COLUMN CODRECEPERP_WIDTH float null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_31900_9_Storeds_002()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  " & vbCrLf
    sConsulta = sConsulta & "@USU NVARCHAR(50) = NULL, @CIA INT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PREC_VISIBLE int = NULL, @PREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@BLOQ_FACTURA_VISIBLE INT = NULL, @BLOQ_FACTURA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@NUM_LINEA_VISIBLE INT = NULL, @NUM_LINEA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@FACTURA_VISIBLE INT = NULL, @FACTURA_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@RECEPTOR_VISIBLE INT = NULL, @RECEPTOR_WIDTH FLOAT = NULL, " & vbCrLf
    sConsulta = sConsulta & "@IMPORTE_ALBARAN_VISIBLE INT = NULL, @IMPORTE_ALBARAN_WIDTH FLOAT = NULL," & vbCrLf
    sConsulta = sConsulta & "@CODRECEPERP_VISIBLE INT=NULL, @CODRECEPERP_WIDTH FLOAT=NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
    sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WITH(NOLOCK) WHERE CIA = @CIA AND COD = @USU" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
    sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
    sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
    sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la informaci�n del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tama�o suficiente para ser visible pero no molestar" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
    sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
    sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @BLOQ_FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @BLOQ_FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUM_LINEA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @NUM_LINEA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @NUM_LINEA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUM_LINEA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FACTURA_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FACTURA_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @RECEPTOR_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RECEPTOR_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @RECEPTOR_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @RECEPTOR_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RECEPTOR_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @IMPORTE_ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "               IF @CODRECEPERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CODRECEPERP_VISIBLE = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CODRECEPERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "               IF @CODRECEPERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @CODRECEPERP_WIDTH = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CODRECEPERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
    sConsulta = sConsulta & "           END" & vbCrLf
    sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @BLOQ_FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUM_LINEA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @NUM_LINEA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FACTURA_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_VISIBLE = @FACTURA_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @FACTURA_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FACTURA_WIDTH = @FACTURA_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RECEPTOR_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @RECEPTOR_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RECEPTOR_WIDTH = @RECEPTOR_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               IF NOT @IMPORTE_ALBARAN_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @IMPORTE_ALBARAN_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @CODRECEPERP_VISIBLE IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CODRECEPERP_VISIBLE= @CODRECEPERP_VISIBLE'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "IF NOT @CODRECEPERP_WIDTH IS NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CODRECEPERP_WIDTH = @CODRECEPERP_WIDTH'" & vbCrLf
    sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
    sConsulta = sConsulta & "           END" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
    sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT,"
    sConsulta = sConsulta & "@ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT,"
    sConsulta = sConsulta & "@PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT,"
    sConsulta = sConsulta & "@FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT,"
    sConsulta = sConsulta & "@CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT,"
    sConsulta = sConsulta & "@UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT, @CC2_WIDTH FLOAT,@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @BLOQ_FACTURA_VISIBLE INT, @BLOQ_FACTURA_WIDTH FLOAT, @NUM_LINEA_VISIBLE INT, @NUM_LINEA_WIDTH FLOAT, @FACTURA_VISIBLE INT, @FACTURA_WIDTH FLOAT, @RECEPTOR_VISIBLE INT, @RECEPTOR_WIDTH FLOAT, @IMPORTE_ALBARAN_VISIBLE INT, @IMPORTE_ALBARAN_WIDTH FLOAT,@CODRECEPERP_VISIBLE INT, @CODRECEPERP_WIDTH FLOAT,@ANCHODEFECTO INT'," & vbCrLf
    sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
    sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
    sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @BLOQ_FACTURA_VISIBLE = @BLOQ_FACTURA_VISIBLE, " & vbCrLf
    sConsulta = sConsulta & "               @BLOQ_FACTURA_WIDTH = @BLOQ_FACTURA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @NUM_LINEA_VISIBLE = @NUM_LINEA_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @NUM_LINEA_WIDTH = @NUM_LINEA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @FACTURA_VISIBLE = @FACTURA_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @FACTURA_WIDTH = @FACTURA_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @RECEPTOR_VISIBLE = @RECEPTOR_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @RECEPTOR_WIDTH = @RECEPTOR_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @IMPORTE_ALBARAN_VISIBLE = @IMPORTE_ALBARAN_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @IMPORTE_ALBARAN_WIDTH = @IMPORTE_ALBARAN_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @CODRECEPERP_VISIBLE = @CODRECEPERP_VISIBLE," & vbCrLf
    sConsulta = sConsulta & "               @CODRECEPERP_WIDTH = @CODRECEPERP_WIDTH," & vbCrLf
    sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_08_19_02A31900_08_19_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.19.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_08_19_02A31900_08_19_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_08_19_02A31900_08_19_03 = False
End Function

Private Sub V_31900_9_Storeds_003()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(MAX),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100),@USU_NIF VARCHAR(30)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT) " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
    sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
    sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
    sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
    sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
    sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
    sConsulta = sConsulta & "   -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --CALIDAD SIN WITH(NOLOCK) modificado desde el 09/10/2012 versi�n 31900.6 tarea 2519" & vbCrLf
    sConsulta = sConsulta & "   --De poner  WITH(NOLOCK) se obtendr�a este error" & vbCrLf
    sConsulta = sConsulta & "   --      Msg 16941, Level 16, State 1, Procedure SP_ACTUALIZAR_PROV_GS, Line 416" & vbCrLf
    sConsulta = sConsulta & "   --      Cursor updates are not allowed on tables opened with the NOLOCK option." & vbCrLf
    sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
    sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
    sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
    sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
    sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
    sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
    sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
    sConsulta = sConsulta & "                       FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
    sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "          BEGIN" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
    sConsulta = sConsulta & "            BEGIN" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
    sConsulta = sConsulta & "                BEGIN" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
    sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
    sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
    sConsulta = sConsulta & "                   FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
    sConsulta = sConsulta & "                        LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
    sConsulta = sConsulta & "                        LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
    sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
    sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "                    BEGIN" & vbCrLf
    sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
    sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
    sConsulta = sConsulta & "                    " & vbCrLf
    sConsulta = sConsulta & "                                                   " & vbCrLf
    sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
    sConsulta = sConsulta & "                       " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
    sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
    sConsulta = sConsulta & "                 END       " & vbCrLf
    sConsulta = sConsulta & "              END                                          " & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "                " & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "      ELSE  " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
    sConsulta = sConsulta & "         BEGIN" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
    sConsulta = sConsulta & "             BEGIN" & vbCrLf
    sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
    sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "             END " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
    sConsulta = sConsulta & "             BEGIN" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @USU_NIF = NULL" & vbCrLf
    sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
    sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT, @USU_NIF=NIF" & vbCrLf
    sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
    sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
    sConsulta = sConsulta & "                        WHERE " & vbCrLf
    sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
    sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
    sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
    sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
    sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
    sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
    sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
    sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                               @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
    sConsulta = sConsulta & "                               @DATEFMT=@DATEFMT, @USU_NIF=@USU_NIF'    " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
    sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_NIF VARCHAR(30)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
    sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "                                     @PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
    sConsulta = sConsulta & "                                     @USU_NIF=@USU_NIF" & vbCrLf
    sConsulta = sConsulta & "                 END                " & vbCrLf
    sConsulta = sConsulta & "           END " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @THOUSANFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @DECIMALFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @PRECISIONFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SET @DATEFMT = NULL" & vbCrLf
    sConsulta = sConsulta & "                SET @USU_NIF = NULL" & vbCrLf
    sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
    sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                      @THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=NIF" & vbCrLf
    sConsulta = sConsulta & "                       FROM USU  WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                           INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
    sConsulta = sConsulta & "                           INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 " & vbCrLf
    sConsulta = sConsulta & "                  WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
    sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
    sConsulta = sConsulta & "                 BEGIN" & vbCrLf
    sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
    sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
    sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                                  @THOUSANFMT=@THOUSANFMT," & vbCrLf
    sConsulta = sConsulta & "                                  @DECIMALFMT=@DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "                                  @PRECISIONFMT=@PRECISIONFMT, " & vbCrLf
    sConsulta = sConsulta & "                                  @DATEFMT=@DATEFMT," & vbCrLf
    sConsulta = sConsulta & "                                  @USU_NIF=@USU_NIF'    " & vbCrLf
    sConsulta = sConsulta & "                                       " & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
    sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
    sConsulta = sConsulta & "                              @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50),@USU_NIF VARCHAR(30)'" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
    sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                                     @THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "                                     @PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
    sConsulta = sConsulta & "                                     @USU_NIF=@USU_NIF" & vbCrLf
    sConsulta = sConsulta & "                   END                       " & vbCrLf
    sConsulta = sConsulta & "               END " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           END " & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
    sConsulta = sConsulta & "     BEGIN   " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
    sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
    sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
    sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
    sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
    sConsulta = sConsulta & "                               @ESPEC INT," & vbCrLf
    sConsulta = sConsulta & "           @DATASIZE INT'" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
    sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
    sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
    sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC," & vbCrLf
    sConsulta = sConsulta & "                                                            @DATASIZE=@DATASIZE'" & vbCrLf
    sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
    sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
    sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
    sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                @ESPEC=@ESPEC," & vbCrLf
    sConsulta = sConsulta & "                                @DATASIZE=@DATASIZE" & vbCrLf
    sConsulta = sConsulta & "                   END        " & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
    sConsulta = sConsulta & "                  BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
    sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
    sConsulta = sConsulta & "                                      " & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
    sConsulta = sConsulta & "                                                           " & vbCrLf
    sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                  END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
    sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
    sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
    sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
    sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
    sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
    sConsulta = sConsulta & "                                       " & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
    sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
    sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
    sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
    sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   " & vbCrLf
    sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
    sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
    sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
    sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
    sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
    sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
    sConsulta = sConsulta & "                   END     " & vbCrLf
    sConsulta = sConsulta & "                END" & vbCrLf
    sConsulta = sConsulta & "               " & vbCrLf
    sConsulta = sConsulta & "           END " & vbCrLf
    sConsulta = sConsulta & "            " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
    sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
    sConsulta = sConsulta & "             ELSE" & vbCrLf
    sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "        " & vbCrLf
    sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
    sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "     " & vbCrLf
    sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
    sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    
End Sub

Public Function CodigoDeActualizacion31900_08_19_03A31900_08_19_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Storeds_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.19.04'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_08_19_03A31900_08_19_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_08_19_03A31900_08_19_04 = False
End Function

Private Sub V_31900_9_Storeds_004()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_COMPANIA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA] @ID INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
    sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
    sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
    sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
    sConsulta = sConsulta & "        " & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
    sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL_ADJUN WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
    sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM USU_PWD WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM FSP_CONF_VISOR_RECEPCIONES WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
        
End Sub

Public Function CodigoDeActualizacion31900_08_19_04A31900_08_19_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Storeds_005
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.19.05'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_08_19_04A31900_08_19_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_08_19_04A31900_08_19_05 = False
End Function

Private Sub V_31900_9_Storeds_005()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO] @fecha DATETIME,@entorno nvarchar(50),@pyme nvarchar(50)=null AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DESDE DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HASTA DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @DESDE=@fecha" & vbCrLf
    sConsulta = sConsulta & "SET @HASTA=GETDATE()" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS= FSGS_SRV + '.' +FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SET @SQLSTRING = N'EXEC ' + @FSGS + 'FSAL_ESTADISTICAS_ENTORNO @DESDE=@DESDE, @HASTA=@HASTA, @ENTORNO=@ENTORNO, @PYME=@PYME'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLSTRING, N'@DESDE DATETIME, @HASTA DATETIME, @ENTORNO VARCHAR(50), @PYME VARCHAR(50)',@DESDE=@DESDE,@HASTA=@HASTA,@ENTORNO=@ENTORNO,@PYME=@PYME" & vbCrLf
    sConsulta = sConsulta & "  END   " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


                
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRASLADARLOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
                
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRASLADARLOG_GS]  " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "---------------FSAL_TRASLADAR_LOG_GS------------" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "--Se tienen que pasar desde la tabla SES a FSAL_ACCESOS Aquellas sesiones que consideremos activas." & vbCrLf
    sConsulta = sConsulta & "--Las sesiones activas son las que tiene el TRASPADO=0 o TRASPASO=2" & vbCrLf
    sConsulta = sConsulta & "--las fechas de la tabla SES ya est�n en UTC" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       IF NOT @FSGS IS NULL" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(1000)" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           --si no estan traspasadas (TRASPASO=0) a FSAL_ACCESOS" & vbCrLf
    sConsulta = sConsulta & "           set @SQLSTRING=N'INSERT INTO ' +@FSGS+ 'FSAL_ACCESOS (PAGINA,PRODUCTO,FISERVER,FFSERVER, TSERVER, USUARIO, TRASPASO, FECACT,FSAL_ENTORNO) SELECT  ''FULLSTEP GS'' AS PAGINA, ''FULLSTEP GS'' AS PRODUCTO, FIS AS FISERVER, Isnull(FFS,null) AS FFSERVER, 0 as TSERVER, USU as USUARIO, 0 as TRASPASO, GETDATE() as FECACT,(SELECT TOP 1 COD FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC)[FSAL_ENTORNO] FROM ' + @FSGS + 'SES WITH(NOLOCK)WHERE FIS is not null AND TRASPASO = 0'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           --Si est�n traspasadas (TRASPASO=2) pero se ha cerrado la sesion o han pasado mas de 8 horas de esa sesion" & vbCrLf
    sConsulta = sConsulta & "           set @SQLSTRING=N'UPDATE ' + @FSGS + 'FSAL_ACCESOS SET FFSERVER=ISNULL(SES.FFS,DATEADD(HH,8,FSAL_ACCESOS.FISERVER)), TRASPASO=0 FROM ' + @FSGS + 'FSAL_ACCESOS INNER JOIN ' + @FSGS + 'SES ON SES.TRASPASO=2 AND FSAL_ACCESOS.PAGINA=''FULLSTEP GS'' AND FSAL_ACCESOS.PRODUCTO =''FULLSTEP GS'' AND FSAL_ACCESOS.FSAL_ENTORNO=(SELECT TOP 1 COD FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC) AND FSAL_ACCESOS.USUARIO=SES.USU AND FSAL_ACCESOS.FISERVER = SES.FIS WHERE ((SES.TRASPASO =2 AND (SES.FFS IS NOT NULL)) OR ((SES.TRASPASO =2 AND (SES.FFS IS NULL) AND (datediff(HH,SES.FIS,getdate())>=8))))'" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           --select @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           --CALIDAD * TIENE QUE SER AS�       " & vbCrLf
    sConsulta = sConsulta & "           ---cojo los campos que aun no se hayan traspasado (traspaso=0) o los que tengan la fecha fin y el traspaso = 2" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'SELECT * ,(SELECT TOP 1 COD  FROM ' + @FSGS + 'FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC)[FSAL_ENTORNO] FROM ' + @FSGS + 'SES WITH(NOLOCK) WHERE FIS is not null AND TRASPASO = 0 OR ((TRASPASO =2 AND (FFS IS NOT NULL)) OR ((TRASPASO =2 AND (FFS IS NULL) AND (datediff(HH,FIS,getdate())>=8))))' --datediff de gestdate y ffis sea mayor igual que 8" & vbCrLf
    sConsulta = sConsulta & "           --select @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           SET @SQLSTRING = N'UPDATE' + @FSGS + 'SES SET TRASPASO=TRASPASO+1 WHERE FIS is not null AND TRASPASO = 0 OR ((TRASPASO =2 AND (FFS IS NOT NULL)) OR ((TRASPASO =2 AND (FFS IS NULL) AND (datediff(HH,FIS,getdate())>=8))))' --datediff de gestdate y ffis sea mayor igual que 8" & vbCrLf
    sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "           --select @SQLSTRING" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    
    
End Sub


Public Function CodigoDeActualizacion31900_08_19_05A31900_08_19_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_9_Tablas_006
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.19.06'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_08_19_05A31900_08_19_06 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_08_19_05A31900_08_19_06 = False
End Function

Private Sub V_31900_9_Tablas_006()
Dim sConsulta As String

sConsulta = "ALTER TABLE ACT1 ALTER COLUMN DEN_SPA VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT1 ALTER COLUMN DEN_ENG VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT1 ALTER COLUMN DEN_GER VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT1 ALTER COLUMN DEN_FRA VARCHAR(1000) NULL" & vbCrLf

sConsulta = sConsulta & "ALTER TABLE ACT2 ALTER COLUMN DEN_SPA VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT2 ALTER COLUMN DEN_ENG VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT2 ALTER COLUMN DEN_GER VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT2 ALTER COLUMN DEN_FRA VARCHAR(1000) NULL" & vbCrLf

sConsulta = sConsulta & "ALTER TABLE ACT3 ALTER COLUMN DEN_SPA VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT3 ALTER COLUMN DEN_ENG VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT3 ALTER COLUMN DEN_GER VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT3 ALTER COLUMN DEN_FRA VARCHAR(1000) NULL" & vbCrLf

sConsulta = sConsulta & "ALTER TABLE ACT4 ALTER COLUMN DEN_SPA VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT4 ALTER COLUMN DEN_ENG VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT4 ALTER COLUMN DEN_GER VARCHAR(1000) NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ACT4 ALTER COLUMN DEN_FRA VARCHAR(1000) NULL"
  ExecuteSQL gRDOCon, sConsulta
End Sub


