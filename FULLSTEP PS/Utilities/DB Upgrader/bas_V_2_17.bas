Attribute VB_Name = "bas_V_2_17"
Option Explicit

Public Function CodigoDeActualizacion2_16_05_00A2_17_00_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

On Error GoTo 0
    
    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Cambio a la versi�n 2.17"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.17.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_16_05_00A2_17_00_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_00A2_17_00_00 = False
End Function

Public Function CodigoDeActualizacion2_17_00_00A2_17_00_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

On Error GoTo 0
    
    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    'Tabla PARGEN_INTERNO
    sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD" & vbCrLf
    sConsulta = sConsulta & "     [IMPRIMIR_POLITICA_PEDIDOS] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_IMPRIMIR_POLITICA_PEDIDOS] DEFAULT 0" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.17.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_17_00_00A2_17_00_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_17_00_00A2_17_00_01 = False
End Function



