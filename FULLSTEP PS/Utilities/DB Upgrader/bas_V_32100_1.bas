Attribute VB_Name = "bas_V_32100_1"

Public Function CodigoDeActualizacion31900_08_19_05A32100_01_20_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_1_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.20.00'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_08_19_05A32100_01_20_00 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_08_19_05A32100_01_20_00 = False
End Function

Private Sub V_32100_1_Storeds_000()
Dim sConsulta As String
    
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LCX_GET_FACTURAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LCX_GET_FACTURAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LCX_GET_FACTURAS]" & vbCrLf
sConsulta = sConsulta & "   -- Add the parameters for the stored procedure here" & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT VARCHAR(20)=NULL," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE VARCHAR(20)=null," & vbCrLf
sConsulta = sConsulta & "   @NIF VARCHAR(20)=NULL," & vbCrLf
sConsulta = sConsulta & "   @ID_SITUACION INT=NULL," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE VARCHAR(13)=NULL," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA INT =1" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   -- SET NOCOUNT ON added to prevent extra result sets from" & vbCrLf
sConsulta = sConsulta & "   -- interfering with SELECT statements." & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT     LF.ID,NIF_EMISOR, NOM_PROVE, NUM_FACT, ID_EMP, NIF_EMP, NOM_EMP, FECHA, FECHA_REGISTRO, ID_CLAVE, IMPORTE, SITUACION ID_SITUACION, WT.TEXT_'+ @IDI + ' DEN_SITUACION, FECHA_ABONO, CUENTA_ABONO, CANAL,FECACT" & vbCrLf
sConsulta = sConsulta & "   FROM         LCX_FACTURAS LF WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN WEBFSPMTEXT WT ON WT.MODULO=174 AND WT.ID=LF.SITUACION" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @FECHADESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA>=@FECHADESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAHASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA<=@FECHAHASTA '" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAABONODESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA_ABONO>=@FECHAABONODESDE '" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAABONOHASTA IS NOT NULL     " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA_ABONO <=@FECHAABONOHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @ID_SITUACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (SITUACION = @ID_SITUACION OR @ID_SITUACION IS NULL)'" & vbCrLf
sConsulta = sConsulta & "   IF @NUM_FACT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND NUM_FACT=@NUM_FACT'" & vbCrLf
sConsulta = sConsulta & "   IF @NIF IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (NIF_EMP=@NIF)'" & vbCrLf
sConsulta = sConsulta & "   IF @IMPORTEDESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND IMPORTE>=@IMPORTEDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @IMPORTEHASTA  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (IMPORTE<=@IMPORTEHASTA)'" & vbCrLf
sConsulta = sConsulta & "   IF @ID_CLAVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @CLAVECOMPLETA=1" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL +' AND ID_CLAVE=@ID_CLAVE'    " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL +' AND ID_CLAVE LIKE @ID_CLAVE'   " & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL +' AND NIF_EMISOR=@NIF_PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ORDER BY ID'" & vbCrLf
sConsulta = sConsulta & "   print @sql" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N' @IDI VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA DATE," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE FLOAT," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA FLOAT," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @ID_SITUACION INT," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE VARCHAR(13)," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA INT'," & vbCrLf
sConsulta = sConsulta & "   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE=@FECHADESDE ," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA=@FECHAHASTA ," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE=@FECHAABONODESDE ," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA=@FECHAABONOHASTA ," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE= @IMPORTEDESDE," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA =@IMPORTEHASTA," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT = @NUM_FACT," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE=@NIF_PROVE ," & vbCrLf
sConsulta = sConsulta & "   @NIF=@NIF ," & vbCrLf
sConsulta = sConsulta & "   @ID_SITUACION=@ID_SITUACION," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE=@ID_CLAVE," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA=@CLAVECOMPLETA" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LCX_GET_ESTADOS_FACTURA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LCX_GET_ESTADOS_FACTURA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LCX_GET_ESTADOS_FACTURA]" & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT ID,TEXT_'+ @IDI + ' DEN FROM WEBFSPMTEXT WHERE MODULO=174'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE] @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL , @VERSION INT, @NUEVO_WORKFLOW INT=0 , @DEFECTO TINYINT= 0, @VERSION_BD_CERT INT =0, @SALENUMLINEA TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAPROVE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA,@CIAPROVE=RC.CIA_PROVE FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_31600 @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION, @DEFECTO=@DEFECTO, @SALENUMLINEA=@SALENUMLINEA'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION, @DEFECTO=@DEFECTO, @VERSION_BD_CERT=@VERSION_BD_CERT '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50), @VERSION INT, @DEFECTO TINYINT, @VERSION_BD_CERT INT, @SALENUMLINEA TINYINT', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS, @VERSION = @VERSION , @DEFECTO=@DEFECTO,@VERSION_BD_CERT=@VERSION_BD_CERT, @SALENUMLINEA=@SALENUMLINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UPLOAD_ADJUN_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UPLOAD_ADJUN_GS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UPLOAD_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UPLOAD_ADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_01_20_00A32100_01_20_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_1_Tablas_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.20.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_01_20_00A32100_01_20_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_01_20_00A32100_01_20_01 = False
End Function


Private Sub V_32100_1_Tablas_001()
    Dim sConsulta As String
    
    sConsulta = sConsulta & "ALTER TABLE ACT1 ALTER COLUMN DEN_SPA VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
    sConsulta = sConsulta & "ALTER TABLE ACT1 ALTER COLUMN DEN_ENG VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT1 ALTER COLUMN DEN_GER VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT1 ALTER COLUMN DEN_FRA VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT2 ALTER COLUMN DEN_SPA VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT2 ALTER COLUMN DEN_ENG VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT2 ALTER COLUMN DEN_GER VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT2 ALTER COLUMN DEN_FRA VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT3 ALTER COLUMN DEN_SPA VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT3 ALTER COLUMN DEN_ENG VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT3 ALTER COLUMN DEN_GER VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT3 ALTER COLUMN DEN_FRA VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT4 ALTER COLUMN DEN_SPA VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT4 ALTER COLUMN DEN_ENG VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT4 ALTER COLUMN DEN_GER VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = sConsulta & "ALTER TABLE ACT4 ALTER COLUMN DEN_FRA VARCHAR(1000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_01_20_01A32100_01_20_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_1_Tablas_002
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_1_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.20.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_01_20_01A32100_01_20_02 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_01_20_01A32100_01_20_02 = False
End Function

Private Sub V_32100_1_Tablas_002()
Dim sConsulta As String
    
sConsulta = "EXEC SP_RENAME 'ADJUN.DATA','DATA_OLD', 'COLUMN'" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [ADJUN] ADD [DGUID] [uniqueidentifier] ROWGUIDCOL  NOT NULL UNIQUE DEFAULT (newid())" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [ADJUN] ADD [DATA] [varbinary](max) FILESTREAM  NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "EXEC SP_RENAME 'CIAS_ESP.DATA','DATA_OLD', 'COLUMN'" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [CIAS_ESP] ADD [DGUID] [uniqueidentifier] ROWGUIDCOL  NOT NULL UNIQUE DEFAULT (newid())" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [CIAS_ESP] ADD [DATA] [varbinary](max) FILESTREAM  NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32100_1_Storeds_002()
Dim sConsulta As String
    
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GRABARADJUNTOPORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GRABARADJUNTOPORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GRABARADJUNTOPORTAL] @CIA INT, @ID INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSP_SRV + '.' + FSP_BD +'.dbo.' FROM PARGEN_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GRABARADJUNTOPORTAL @ID=@ID OUTPUT,@CIA=@CIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CIA INT,@ID INT OUTPUT', @CIA=@CIA,@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ADJUNTOPORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ADJUNTOPORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LEER_ADJUNTOPORTAL] @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSP_SRV + '.' + FSP_BD +'.dbo.' FROM PARGEN_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LEER_ADJUNTOPORTAL @ID=@ID,@CIA=@CIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CIA INT,@ID INT', @CIA=@CIA,@ID=@ID" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GRABARESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GRABARESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GRABARESPECIFICACIONCIA] @CIA INT, @ID INT, @NOM NVARCHAR(100), @COMENT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSP_SRV + '.' + FSP_BD +'.dbo.' FROM PARGEN_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GRABARESPECIFICACIONCIA @ID=@ID,@CIA=@CIA,@NOM=@NOM,@COMENT=@COMENT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CIA INT,@ID INT,@NOM NVARCHAR(100),@COMENT NVARCHAR(4000)', @CIA=@CIA,@ID=@ID,@NOM=@NOM,@COMENT=@COMENT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LEER_ESPECIFICACIONCIA] @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSP_SRV + '.' + FSP_BD +'.dbo.' FROM PARGEN_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LEER_ESPECIFICACIONCIA @ID=@ID,@CIA=@CIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CIA INT,@ID INT', @CIA=@CIA,@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENERESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENERESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_OBTENERESPECIFICACIONCIA] @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSP_SRV + '.' + FSP_BD +'.dbo.' FROM PARGEN_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_OBTENERESPECIFICACIONCIA @ID=@ID,@CIA=@CIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CIA INT,@ID INT', @CIA=@CIA,@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENERADJUNTOPORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENERADJUNTOPORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_OBTENERADJUNTOPORTAL] @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSP_SRV + '.' + FSP_BD +'.dbo.' FROM PARGEN_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_OBTENERADJUNTOPORTAL @ID=@ID,@CIA=@CIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CIA INT,@ID INT', @CIA=@CIA,@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_01_20_02A32100_01_20_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_1_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.20.03'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_01_20_02A32100_01_20_03 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_01_20_02A32100_01_20_03 = False
End Function

Private Sub V_32100_1_Storeds_003()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LCX_GET_FACTURAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LCX_GET_FACTURAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LCX_GET_FACTURAS]" & vbCrLf
sConsulta = sConsulta & "   -- Add the parameters for the stored procedure here" & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT VARCHAR(20)=NULL," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE VARCHAR(20)=null," & vbCrLf
sConsulta = sConsulta & "   @NIF VARCHAR(20)=NULL," & vbCrLf
sConsulta = sConsulta & "   @ESTADOS VARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE VARCHAR(13)=NULL," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA INT =1" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   -- SET NOCOUNT ON added to prevent extra result sets from" & vbCrLf
sConsulta = sConsulta & "   -- interfering with SELECT statements." & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT     LF.ID,NIF_EMISOR, NOM_PROVE, NUM_FACT, ID_EMP, NIF_EMP, NOM_EMP, FECHA, FECHA_REGISTRO, ID_CLAVE, IMPORTE, SITUACION ID_SITUACION, WT.TEXT_'+ @IDI + ' DEN_SITUACION, FECHA_ABONO, CUENTA_ABONO, CANAL,FECACT" & vbCrLf
sConsulta = sConsulta & "   FROM         LCX_FACTURAS LF WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN WEBFSPMTEXT WT ON WT.MODULO=174 AND WT.ID=LF.SITUACION" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @FECHADESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA>=@FECHADESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAHASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA<=@FECHAHASTA '" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAABONODESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA_ABONO>=@FECHAABONODESDE '" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAABONOHASTA IS NOT NULL     " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA_ABONO <=@FECHAABONOHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADOS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (SITUACION IN (' + @ESTADOS +'))'" & vbCrLf
sConsulta = sConsulta & "   IF @NUM_FACT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND NUM_FACT=@NUM_FACT'" & vbCrLf
sConsulta = sConsulta & "   IF @NIF IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (NIF_EMP=@NIF)'" & vbCrLf
sConsulta = sConsulta & "   IF @IMPORTEDESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND IMPORTE>=@IMPORTEDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @IMPORTEHASTA  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (IMPORTE<=@IMPORTEHASTA)'" & vbCrLf
sConsulta = sConsulta & "   IF @ID_CLAVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @CLAVECOMPLETA=1" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL +' AND ID_CLAVE=@ID_CLAVE'    " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL +' AND ID_CLAVE LIKE @ID_CLAVE'   " & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL +' AND NIF_EMISOR=@NIF_PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ORDER BY ID'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N' @IDI VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA DATE," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE FLOAT," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA FLOAT," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @ESTADOS VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE VARCHAR(13)," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA INT'," & vbCrLf
sConsulta = sConsulta & "   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE=@FECHADESDE ," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA=@FECHAHASTA ," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE=@FECHAABONODESDE ," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA=@FECHAABONOHASTA ," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE= @IMPORTEDESDE," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA =@IMPORTEHASTA," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT = @NUM_FACT," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE=@NIF_PROVE ," & vbCrLf
sConsulta = sConsulta & "   @NIF=@NIF ," & vbCrLf
sConsulta = sConsulta & "   @ESTADOS=@ESTADOS," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE=@ID_CLAVE," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA=@CLAVECOMPLETA" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_01_20_03A32100_01_20_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_1_Storeds_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.20.04'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_01_20_03A32100_01_20_04 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_01_20_03A32100_01_20_04 = False
End Function

Private Sub V_32100_1_Storeds_004()
Dim sConsulta As String
    
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LCX_GET_ESTADOS_FACTURA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LCX_GET_ESTADOS_FACTURA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LCX_GET_ESTADOS_FACTURA]" & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT ID,TEXT_'+ @IDI + ' DEN FROM WEBFSPMTEXT WITH(NOLOCK) WHERE MODULO=174'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LCX_GET_FACTURAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LCX_GET_FACTURAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LCX_GET_FACTURAS]" & vbCrLf
sConsulta = sConsulta & "   -- Add the parameters for the stored procedure here" & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA FLOAT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT VARCHAR(20)=NULL," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE VARCHAR(20)=null," & vbCrLf
sConsulta = sConsulta & "   @NIF VARCHAR(20)=NULL," & vbCrLf
sConsulta = sConsulta & "   @ESTADOS VARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE VARCHAR(13)=NULL," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA INT =1" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   -- SET NOCOUNT ON added to prevent extra result sets from" & vbCrLf
sConsulta = sConsulta & "   -- interfering with SELECT statements." & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT     LF.ID,NIF_EMISOR, NOM_PROVE, NUM_FACT, ID_EMP, NIF_EMP, NOM_EMP, FECHA, FECHA_REGISTRO, ID_CLAVE, IMPORTE, SITUACION ID_SITUACION, WT.TEXT_'+ @IDI + ' DEN_SITUACION, FECHA_ABONO, CUENTA_ABONO, CANAL,FECACT" & vbCrLf
sConsulta = sConsulta & "   FROM         LCX_FACTURAS LF WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN WEBFSPMTEXT WT WITH(NOLOCK) ON WT.MODULO=174 AND WT.ID=LF.SITUACION" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @FECHADESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA>=@FECHADESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAHASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA<=@FECHAHASTA '" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAABONODESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA_ABONO>=@FECHAABONODESDE '" & vbCrLf
sConsulta = sConsulta & "   IF @FECHAABONOHASTA IS NOT NULL     " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND FECHA_ABONO <=@FECHAABONOHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADOS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (SITUACION IN (' + @ESTADOS +'))'" & vbCrLf
sConsulta = sConsulta & "   IF @NUM_FACT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND NUM_FACT=@NUM_FACT'" & vbCrLf
sConsulta = sConsulta & "   IF @NIF IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (NIF_EMP=@NIF)'" & vbCrLf
sConsulta = sConsulta & "   IF @IMPORTEDESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND IMPORTE>=@IMPORTEDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @IMPORTEHASTA  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL +' AND (IMPORTE<=@IMPORTEHASTA)'" & vbCrLf
sConsulta = sConsulta & "   IF @ID_CLAVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @CLAVECOMPLETA=1" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL +' AND ID_CLAVE=@ID_CLAVE'    " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL +' AND ID_CLAVE LIKE @ID_CLAVE'   " & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL +' AND NIF_EMISOR=@NIF_PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ORDER BY ID'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N' @IDI VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE DATE," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA DATE," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE FLOAT," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA FLOAT," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @ESTADOS VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE VARCHAR(13)," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA INT'," & vbCrLf
sConsulta = sConsulta & "   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "   @FECHADESDE=@FECHADESDE ," & vbCrLf
sConsulta = sConsulta & "   @FECHAHASTA=@FECHAHASTA ," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONODESDE=@FECHAABONODESDE ," & vbCrLf
sConsulta = sConsulta & "   @FECHAABONOHASTA=@FECHAABONOHASTA ," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEDESDE= @IMPORTEDESDE," & vbCrLf
sConsulta = sConsulta & "   @IMPORTEHASTA =@IMPORTEHASTA," & vbCrLf
sConsulta = sConsulta & "   @NUM_FACT = @NUM_FACT," & vbCrLf
sConsulta = sConsulta & "   @NIF_PROVE=@NIF_PROVE ," & vbCrLf
sConsulta = sConsulta & "   @NIF=@NIF ," & vbCrLf
sConsulta = sConsulta & "   @ESTADOS=@ESTADOS," & vbCrLf
sConsulta = sConsulta & "   @ID_CLAVE=@ID_CLAVE," & vbCrLf
sConsulta = sConsulta & "   @CLAVECOMPLETA=@CLAVECOMPLETA" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion32100_01_20_04A32100_01_20_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_1_Storeds_005
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.20.05'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_01_20_04A32100_01_20_05 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_01_20_04A32100_01_20_05 = False
End Function


Private Sub V_32100_1_Storeds_005()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GRABARADJUNTOPORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GRABARADJUNTOPORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GRABARADJUNTOPORTAL] @CIA INT, @ID INT OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ADJUN (CIA_COMP,FECACT,DATA)" & vbCrLf
sConsulta = sConsulta & "   VALUES (@CIA,GETDATE(),(0x))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ID=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ADJUNTOPORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ADJUNTOPORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LEER_ADJUNTOPORTAL] @ID INT, @CIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT  FROM ADJUN  WITH (NOLOCK)  WHERE ID=@ID AND CIA_COMP=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GRABARESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GRABARESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GRABARESPECIFICACIONCIA] @CIA INT, @ID INT, @NOM NVARCHAR(100), @COMENT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA,ID,NOM,COM,FECACT,DATA)" & vbCrLf
sConsulta = sConsulta & "   VALUES (@CIA,@ID,@NOM,@COMENT,GETDATE(),(0x))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LEER_ESPECIFICACIONCIA] @ID INT, @CIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT  FROM CIAS_ESP  WITH (NOLOCK)  WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENERESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENERESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_OBTENERESPECIFICACIONCIA] @ID INT, @CIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DATA.PathName()  FROM CIAS_ESP  WITH (NOLOCK)  WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENERADJUNTOPORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENERADJUNTOPORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_OBTENERADJUNTOPORTAL] @ID INT, @CIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DATA.PathName()  FROM ADJUN  WITH (NOLOCK)  WHERE ID=@ID AND CIA_COMP=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub


Public Function CodigoDeActualizacion32100_01_20_05A32100_01_20_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_1_Storeds_006
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.20.06'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_01_20_05A32100_01_20_06 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_01_20_05A32100_01_20_06 = False
End Function


Private Sub V_32100_1_Storeds_006()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARGEN_INTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET @SQL='SELECT PI.ACCESO_FSSM, PI.VER_NUMEXT, PI.ACCESO_FSGS FROM ' + @FSGS + 'PARGEN_INTERNO PI WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------------" & vbCrLf
sConsulta = sConsulta & "--     LO Q ESTA CHECK PIDE ACCESO_FSFA,   LO Q HAB?A ES LO DE ARRIBA" & vbCrLf
sConsulta = sConsulta & "-------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT PI.ACCESO_FSSM, PI.VER_NUMEXT, PI.ACCESO_FSGS, ACCESO_FSFA,FIRMA_DIG_OFERTAS FROM ' + @FSGS + 'PARGEN_INTERNO PI WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_01_206_A32100_01_207() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32100_1_Tablas_207
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32100_1_Storeds_207

    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.07'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32100_01_206_A32100_01_207 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32100_01_206_A32100_01_207 = False
End Function

Private Sub V_32100_1_Tablas_207()
    Dim sConsulta As String
sConsulta = "alter table PARGEN_INTERNO ADD URL_WEBSERVICE NVARCHAR(500) NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32100_1_Storeds_207()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DOWNLOAD_ESPECIFICACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DOWNLOAD_ESPECIFICACION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UPD_ESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UPD_ESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_UPD_ESPECIFICACIONCIA] @ID INT, @CIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS_ESP SET DATA=cast('' as varbinary(max)) WHERE ID = @ID AND CIA = @CIA" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_01_20_07A32100_01_20_08() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32100_1_Storeds_20_08

    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.08'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32100_01_20_07A32100_01_20_08 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32100_01_20_07A32100_01_20_08 = False
End Function

Private Sub V_32100_1_Storeds_20_08()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENER_SERVICIO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENER_SERVICIO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_OBTENER_SERVICIO]" & vbCrLf
sConsulta = sConsulta & "@SERVICIO NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@CAMPOID NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=1 AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_OBTENER_SERVICIO @SERVICIO=@SERVICIO,@CAMPOID=@CAMPOID,@INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SERVICIO NVARCHAR(50), @CAMPOID NVARCHAR(50),@INSTANCIA INT',@SERVICIO=@SERVICIO,@CAMPOID=@CAMPOID,@INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub
