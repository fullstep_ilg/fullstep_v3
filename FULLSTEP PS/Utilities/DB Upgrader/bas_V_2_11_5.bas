Attribute VB_Name = "bas_V_2_11_5"
Option Explicit


Public Function CodigoDeActualizacion2_11_00_02A2_11_50_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Actualizamos la estructura de tablas a la versi�n 2.10
    frmProgreso.lblDetalle = "Se actualizar� a la versi�n 2.11.5"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_11_5_Tablas
    
    frmProgreso.lblDetalle = "Actualiza stored procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
    V_2_11_5_Storeds
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.50.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_00_02A2_11_50_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_02A2_11_50_00 = False
End Function

Public Function CodigoDeActualizacion2_11_50_00A2_11_50_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Actualizamos la estructura de tablas a la versi�n 2.10
    frmProgreso.lblDetalle = "Se actualizar� a la versi�n 2.11.5.1"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.50.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_50_00A2_11_50_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_00A2_11_50_01 = False

End Function

Public Function CodigoDeActualizacion2_11_50_01A2_11_50_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Actualizamos la estructura de tablas a la versi�n 2.10
    frmProgreso.lblDetalle = "Eliminar campo de envio as�ncrono en el portal"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    sConsulta = "ALTER TABLE PARGEN_INTERNO DROP CONSTRAINT DF_PARGEN_INTERNO_ENVIO_ASINCRONO"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "ALTER TABLE PARGEN_INTERNO DROP COLUMN ENVIO_ASINCRONO"
    ExecuteSQL gRDOCon, sConsulta
            
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.50.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_50_01A2_11_50_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_01A2_11_50_02 = False

End Function

Public Function CodigoDeActualizacion2_11_50_02A2_11_50_03()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Se insertar�n nuevos textos para el portal"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.50.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_50_02A2_11_50_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_02A2_11_50_03 = False

End Function

Public Function CodigoDeActualizacion2_11_50_03A2_11_50_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Modificaciones en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_11_5_04_Tablas
    
        
    frmProgreso.lblDetalle = "Modificaciones en stored procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_11_5_04_Storeds
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.50.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_50_03A2_11_50_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_03A2_11_50_04 = False

End Function

Public Function CodigoDeActualizacion2_11_50_04A2_11_50_05()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
            
        
    frmProgreso.lblDetalle = "Modificaciones en stored procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_11_5_05_Storeds
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.11.50.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_50_04A2_11_50_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_04A2_11_50_05 = False

End Function

Private Sub V_2_11_5_Tablas()
Dim sConsulta As String
'PROCE_OFE (TIENE UNA UC)
sConsulta = "CREATE TABLE [dbo].[PROCE_OFE] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CIA_COMP] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (20) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVE] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CIA_PROVE] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (20) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECREC] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECVAL] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [MON] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMBIO] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBS] [text] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBSADJUN] [text] NULL ," & vbCrLf
sConsulta = sConsulta & "   [NUMOBJ] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ERROR] [tinyint] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_OFE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PROCE_OFE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_OFE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PROCE_OFE_ERROR] DEFAULT (0) FOR [ERROR]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [UC_PROCE_OFE] UNIQUE  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA_COMP]," & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [PROVE]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'GRUPO_OFE
sConsulta = "CREATE TABLE [dbo].[GRUPO_OFE] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBSADJUN] [text] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[GRUPO_OFE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_GRUPO_OFE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[GRUPO_OFE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_GRUPO_OFE_PROCE_OFE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE_OFE] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'ITEM_OFE
sConsulta = "CREATE TABLE [dbo].[ITEM_OFE] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANTMAX] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO2] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO3] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT1] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT2] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT3] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBSADJUN] [text] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT] [float] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ITEM_OFE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_ITEM_OFE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ITEM_OFE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_OFE_PROCE_OFE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE_OFE] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'OFE_ATRIB
sConsulta = "CREATE TABLE [dbo].[OFE_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB_ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[OFE_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_OFE_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB_ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[OFE_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_OFE_ATRIB_PROCE_OFE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE_OFE] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


'OFE_GR_ATRIB
sConsulta = "CREATE TABLE [dbo].[OFE_GR_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB_ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[OFE_GR_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_OFE_GR_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB_ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[OFE_GR_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_OFE_GR_ATRIB_GRUPO_OFE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[GRUPO_OFE] (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'OFE_ITEM_ATRIB
sConsulta = "CREATE TABLE [dbo].[OFE_ITEM_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB_ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[OFE_ITEM_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_OFE_ITEM_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB_ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[OFE_ITEM_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_OFE_ITEM_ATRIB_ITEM_OFE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ITEM_OFE] (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'PROCE_OFE_ADJUN
sConsulta = "CREATE TABLE [dbo].[PROCE_OFE_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDPORTAL] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDGS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_OFE_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PROCE_OFE_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ADJUN]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_OFE_ADJUN] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PROCE_OFE_ADJUN_PROCE_OFE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE_OFE] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'OFE_GR_ADJUN
sConsulta = "CREATE TABLE [dbo].[OFE_GR_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDPORTAL] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDGS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[OFE_GR_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_OFE_GR_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]," & vbCrLf
sConsulta = sConsulta & "       [ADJUN]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'OFE_ITEM_ADJUN
sConsulta = "CREATE TABLE [dbo].[OFE_ITEM_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDPORTAL] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDGS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[OFE_ITEM_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_OFE_ITEM_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]," & vbCrLf
sConsulta = sConsulta & "       [ADJUN]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'A�ADIR EL CAMPO ENVIO_ASINCRONO A PARGEN_INTERNO TINYINT NOT NULL DEFAULT 1
sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD [ENVIO_ASINCRONO] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_ENVIO_ASINCRONO] DEFAULT 1"
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_11_5_Storeds()
Dim sConsulta As String

'SP_DEVOLVER_PROCESOS_PROVE
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_PROVE @CIACOMP INT, @PROVE VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT * " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP = @CIACOMP" & vbCrLf
sConsulta = sConsulta & "   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'SP_DEVOLVER_GRUPO
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ID INT, @GRUPO VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT OBSADJUN, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'SP_DEVOLVER_ADJUNTOS
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ADJUNTOS @ID int, @GRUPO varchar(50) = null, @ITEM int = null, @AMBITO tinyint  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT ID,ADJUN, NOM,COM, IDGS, DATASIZE, IDPORTAL " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "   WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  SELECT ID,ADJUN,GRUPO, NOM, COM, IDGS, DATASIZE, IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "    FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "   WHERE ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND GRUPO = ISNULL(@GRUPO,GRUPO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  SELECT ID,ADJUN,ITEM,NOM, COM, IDGS, DATASIZE, IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "   WHERE ID =@ID" & vbCrLf
sConsulta = sConsulta & "     AND ITEM = ISNULL(@ITEM,ITEM)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'SP_DEVOLVER_OFE_ATRIB
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFE_ATRIB @ID int, @AMBITO tinyint, @GRUPO varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT * " & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "   WHERE ID= @ID" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  SELECT * " & vbCrLf
sConsulta = sConsulta & "    FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "   WHERE ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND GRUPO = ISNULL(@GRUPO,GRUPO)" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  SELECT * " & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "   WHERE ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND GRUPO = ISNULL(@GRUPO,GRUPO)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'SP_ELIMINAR_OFERTA_PROVE
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIMINAR_OFERTA_PROVE @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'SP_ITEMS
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ITEMS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_ITEMS  @ID INT,@GRUPO VARCHAR(50)=null AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ID=@ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ITO.ITEM, PRECIO, CANTMAX, PRECIO2, PRECIO3,IA.NUMADJUN, COMENT1, COMENT2, COMENT3, cast(OBSADJUN as varchar(2000)) OBSADJUN, @CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE ITO" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (SELECT IA.ITEM, COUNT(IA.ADJUN) NUMADJUN" & vbCrLf
sConsulta = sConsulta & "                 FROM OFE_ITEM_ADJUN IA" & vbCrLf
sConsulta = sConsulta & "                WHERE IA.ID =@ID" & vbCrLf
sConsulta = sConsulta & "             GROUP BY IA.ID, IA.ITEM) IA" & vbCrLf
sConsulta = sConsulta & "      ON ITO.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "WHERE ITO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "  AND ITO.GRUPO=case when @GRUPO is null then ITO.GRUPO else @GRUPO END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'SP_DEVOLVER_OFERTA_PROVE
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE @CIA_COMP INT = NULL, @ANYO INT = NULL, @GMN1 VARCHAR(20) = NULL, @PROCE INT = NULL, @PROVE VARCHAR(50) = NULL, @ID INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @ID = ID " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP = @CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   AND ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TOTALADJUNTOS = ISNULL(SUM(DATASIZE),0) " & vbCrLf
sConsulta = sConsulta & "  FROM" & vbCrLf
sConsulta = sConsulta & "    (SELECT ISNULL(SUM(DATASIZE), 0) DATASIZE" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
sConsulta = sConsulta & "       FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
sConsulta = sConsulta & "       FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID) A" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ID, PO.CIA_COMP, PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.CIA_PROVE, PO.USU, PO.FECREC, PO.FECVAL, PO.MON,PO.CAMBIO, PO.OBS, PO.OBSADJUN, PO.NUMOBJ, @TOTALADJUNTOS TOTALADJUNTOS, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_11_5_04_Tablas()
Dim sConsulta As String

sConsulta = "ALTER TABLE DBO.PROCE_OFE ADD ADJUN TINYINT NOT NULL CONSTRAINT [DF_PROCE_OFE_ADJUN] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE DBO.GRUPO_OFE ADD ADJUN TINYINT NOT NULL CONSTRAINT [DF_GRUPO_OFE_ADJUN] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE DBO.ITEM_OFE ADD ADJUN TINYINT NOT NULL CONSTRAINT [DF_ITEM_OFE_ADJUN] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_11_5_04_Storeds()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_ADJUNTOS @ID int, @GRUPO varchar(50) = null, @ITEM int = null, @AMBITO tinyint, @SOLOPORTAL int = 0 OUTPUT  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM PROCE_OFE WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN, NOM,COM, IDGS, DATASIZE, IDPORTAL " & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "     WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM GRUPO_OFE WHERE ID = @ID AND GRUPO = @GRUPO)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN,GRUPO, NOM, COM, IDGS, DATASIZE, IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "     WHERE ID = @ID " & vbCrLf
sConsulta = sConsulta & "       AND GRUPO = ISNULL(@GRUPO,GRUPO)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM ITEM_OFE WHERE ID = @ID AND ITEM = @ITEM)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN,ITEM,NOM, COM, IDGS, DATASIZE, IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "     WHERE ID =@ID" & vbCrLf
sConsulta = sConsulta & "       AND ITEM = ISNULL(@ITEM,ITEM)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE @CIA_COMP INT = NULL, @ANYO INT = NULL, @GMN1 VARCHAR(20) = NULL, @PROCE INT = NULL, @PROVE VARCHAR(50) = NULL, @ID INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @ID = ID " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP = @CIA_COMP" & vbCrLf
sConsulta = sConsulta & "   AND ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TOTALADJUNTOS = ISNULL(SUM(DATASIZE),0) " & vbCrLf
sConsulta = sConsulta & "  FROM" & vbCrLf
sConsulta = sConsulta & "    (SELECT ISNULL(SUM(DATASIZE), 0) DATASIZE" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
sConsulta = sConsulta & "       FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
sConsulta = sConsulta & "       FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "      WHERE ID = @ID) A" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ID, PO.CIA_COMP, PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.CIA_PROVE, PO.USU, PO.FECREC, PO.FECVAL, PO.MON,PO.CAMBIO, PO.OBS, PO.OBSADJUN, PO.NUMOBJ, PO.ADJUN,  @TOTALADJUNTOS TOTALADJUNTOS, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Function V_2_11_5_05_Storeds()
Dim sConsulta  As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_OFE_ATRIB @ID int, @AMBITO tinyint, @GRUPO varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EL CAMBIO QUE DEVUELVE ES EL CAMBIO DE LA MONEDA EN QUE SE GUARD� LA OFERTA RESPECTO A LA MONEDA DEL PROCESO" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT * , @CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "   WHERE ID= @ID" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  SELECT * , @CAM CAMBIO " & vbCrLf
sConsulta = sConsulta & "    FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "   WHERE ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND GRUPO = ISNULL(@GRUPO,GRUPO)" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  SELECT *  , @CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "   WHERE ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND GRUPO = ISNULL(@GRUPO,GRUPO)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function
