Attribute VB_Name = "bas_V_32200_1"
Public Function CodigoDeActualizacion32200_01_26_00_A32200_01_26_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32200_1_Tablas_001

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32200_1_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.27.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32200_01_26_00_A32200_01_26_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32200_01_26_00_A32200_01_26_01 = False
End Function

Private Sub V_32200_1_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS] @CIA INT, @PROVE VARCHAR(50), @IDI VARCHAR(20) =NULL, @ID INT=0, @TIPO INT=0, @FECHA VARCHAR(10)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos el servidor y la base de datos del GS." & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos el c?digo y el identificador del PROVEEDOR en el GS." & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtenemos las NOCONFORMIDADES cerradas." & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_NOCONFORMIDADES_CERRADAS @PROVE =@PROVE, @IDI=@IDI, @ID = @ID , @TIPO = @TIPO, @FECHA = @FECHA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @IDI VARCHAR(10), @ID INT, @TIPO INT, @FECHA VARCHAR(10)' , @PROVE = @PROVEGS, @IDI=@IDI, @ID=@ID, @TIPO=@TIPO, @FECHA=@FECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =32 AND W.ID>=17 AND W.ID<=19 ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PROCE_OFE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PROCE_OFE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PROCE_OFE] @CIA INT, @CIA_PROVE INT, @FECOFEDESDE DATETIME = NULL, @FECOFEHASTA DATETIME = NULL, @ANYO INT = NULL, @GMN1 VARCHAR(50) = NULL, @PROCE INT = NULL, @DEN_PROCE VARCHAR(200) = NULL, @COD_ART VARCHAR(50) = NULL, @DEN_ART VARCHAR(200) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVE=COD FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSP_GET_PROCE_OFE @PROVE=@PROVE, @FECOFEDESDE=@FECOFEDESDE, @FECOFEHASTA=@FECOFEHASTA, @ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE, @DEN_PROCE=@DEN_PROCE, @COD_ART=@COD_ART, @DEN_ART=@DEN_ART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50), @FECOFEDESDE DATETIME, @FECOFEHASTA DATETIME, @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @DEN_PROCE VARCHAR(200), @COD_ART VARCHAR(50), @DEN_ART VARCHAR(200)', @PROVE=@PROVE, @FECOFEDESDE=@FECOFEDESDE, @FECOFEHASTA=@FECOFEHASTA, @ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE, @DEN_PROCE=@DEN_PROCE, @COD_ART=@COD_ART, @DEN_ART=@DEN_ART" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32200_1_Tablas_001()
Dim sConsulta As String

sConsulta = "IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME = N'EMPRESA_PORTAL' AND OBJECT_ID = Object_ID(N'PARGEN_INTERNO'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
sConsulta = sConsulta & "EMPRESA_PORTAL tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_EMPRESA_PORTAL DEFAULT 0" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME = N'MAX_NUM_ACTIVIDADES' AND OBJECT_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_GEST ADD [MAX_NUM_ACTIVIDADES] [TINYINT] NOT NULL CONSTRAINT DF_PARGEN_GEST_MAX_NUM_ACTIVIDADES DEFAULT 0" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32200_01_27_01_A32200_01_27_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32200_1_Storeds_002
    
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.27.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32200_01_27_01_A32200_01_27_02 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32200_01_27_01_A32200_01_27_02 = False
End Function

Private Sub V_32200_1_Storeds_002()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
sConsulta = sConsulta & "   @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMFACTURA VARCHAR(20) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NOM_PORTAL NVARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @PROVE IS NULL AND @PROVE<>0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "       DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "       SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "       SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO,@NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                                                                   @NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                                                                   @FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                                                                   @NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                                                                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                                                                   @CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                                                                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                                                                   @FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES,@NUMFACTURA=@NUMFACTURA'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT,@NUMPEDIDO INT,@NUMORDEN INT,@NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME," & vbCrLf
sConsulta = sConsulta & "                                   @FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                   @FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME," & vbCrLf
sConsulta = sConsulta & "                                   @FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50),@PARTIDASPRES NVARCHAR(MAX)," & vbCrLf
sConsulta = sConsulta & "                                   @NUMFACTURA VARCHAR(20),@NOM_PORTAL NVARCHAR(50)'," & vbCrLf
sConsulta = sConsulta & "                      @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                      @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                      @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                      @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                      @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                      @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                      @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                      @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                      @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                      @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                      @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                      @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                      @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                      @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                      @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                      @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                      @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                      @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                      @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                      @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                      @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                      @NUMFACTURA=@NUMFACTURA," & vbCrLf
sConsulta = sConsulta & "                      @NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_SOLPENDIENTES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_SOLPENDIENTES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_SOLPENDIENTES]" & vbCrLf
sConsulta = sConsulta & "   @CIACOMP FLOAT," & vbCrLf
sConsulta = sConsulta & "   @CIAPROVE FLOAT," & vbCrLf
sConsulta = sConsulta & "   @USU  VARCHAR(20)=''," & vbCrLf
sConsulta = sConsulta & "   @TIPO AS INT=NULL," & vbCrLf
sConsulta = sConsulta & "   @NOM_PORTAL NVARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUMORDEN INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUMORDENNUE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIACOMP  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVEGS = RC.COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) WHERE RC.CIA_COMP = @CIACOMP AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'EXEC '+@FSGS+N'FSEP_NUM_PED @PROVE=@PROVE,@TIPO=@TIPO,@NOM_PORTAL=@NOM_PORTAL,@NUMORDEN=@NUMORDEN OUTPUT,@NUMORDENNUE=@NUMORDENNUE OUTPUT'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@PROVE VARCHAR(50),@TIPO INT,@NOM_PORTAL NVARCHAR(50),@NUMORDEN INT OUTPUT,@NUMORDENNUE INT OUTPUT'," & vbCrLf
sConsulta = sConsulta & "       @PROVE=@PROVEGS,@TIPO=@TIPO,@NOM_PORTAL=@NOM_PORTAL,@NUMORDEN=@NUMORDEN OUTPUT,@NUMORDENNUE=@NUMORDENNUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @USU<>''" & vbCrLf
sConsulta = sConsulta & "      SELECT GETDATE() DIA,USU.NOM+' '+USU.APE NOMBRE,@NUMORDEN ORDEN_ACT,@NUMORDENNUE ORDEN_NUE" & vbCrLf
sConsulta = sConsulta & "      FROM REL_CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN USU WITH(NOLOCK) ON USU.CIA=REL_CIAS.CIA_PROVE AND COD=@USU" & vbCrLf
sConsulta = sConsulta & "      WHERE CIA_COMP=@CIACOMP AND CIA_PROVE=@CIAPROVE" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SELECT  @NUMORDEN AS ORDEN_ACT,@NUMORDENNUE ORDEN_NUE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32200_01_27_02_A32200_01_27_03() As Boolean
     Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32200_1_Storeds_003
    
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.27.03'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32200_01_27_02_A32200_01_27_03 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32200_01_27_02_A32200_01_27_03 = False
End Function

Private Sub V_32200_1_Storeds_003()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PROCE_OFE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PROCE_OFE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PROCE_OFE]" & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @CIA_PROVE INT," & vbCrLf
sConsulta = sConsulta & "   @FECOFEDESDE DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECOFEHASTA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @PROCE INT=NULL," & vbCrLf
sConsulta = sConsulta & "   @DEN_PROCE VARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @COD_ART VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @DEN_ART VARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @NOM_PORTAL NVARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVE=COD FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSP_GET_PROCE_OFE @PROVE=@PROVE,@FECOFEDESDE=@FECOFEDESDE,@FECOFEHASTA=@FECOFEHASTA,@ANYO=@ANYO,@GMN1=@GMN1,@PROCE=@PROCE," & vbCrLf
sConsulta = sConsulta & "                                                   @DEN_PROCE=@DEN_PROCE,@COD_ART=@COD_ART,@DEN_ART=@DEN_ART,@NOM_PORTAL=@NOM_PORTAL'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50),@FECOFEDESDE DATETIME,@FECOFEHASTA DATETIME,@ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@DEN_PROCE VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                           @COD_ART VARCHAR(50),@DEN_ART VARCHAR(200),@NOM_PORTAL NVARCHAR(50)'," & vbCrLf
sConsulta = sConsulta & "                           @PROVE=@PROVE,@FECOFEDESDE=@FECOFEDESDE,@FECOFEHASTA=@FECOFEHASTA,@ANYO=@ANYO,@GMN1=@GMN1,@PROCE=@PROCE," & vbCrLf
sConsulta = sConsulta & "                           @DEN_PROCE=@DEN_PROCE,@COD_ART=@COD_ART,@DEN_ART=@DEN_ART,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]" & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(20)=NULL," & vbCrLf
sConsulta = sConsulta & "   @ID INT=0, @TIPO INT=0," & vbCrLf
sConsulta = sConsulta & "   @FECHA VARCHAR(10)=NULL," & vbCrLf
sConsulta = sConsulta & "   @NOM_PORTAL NVARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Obtenemos el servidor y la base de datos del GS." & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Obtenemos el c?digo y el identificador del PROVEEDOR en el GS." & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Obtenemos las NOCONFORMIDADES cerradas." & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC '+@FSGS+'FSQA_GET_NOCONFORMIDADES_CERRADAS @PROVE=@PROVE,@IDI=@IDI,@ID=@ID,@TIPO=@TIPO,@FECHA=@FECHA,@NOM_PORTAL=@NOM_PORTAL'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@PROVE VARCHAR(50),@IDI VARCHAR(10),@ID INT,@TIPO INT,@FECHA VARCHAR(10),@NOM_PORTAL NVARCHAR(50)'," & vbCrLf
sConsulta = sConsulta & "       @PROVE=@PROVEGS,@IDI=@IDI,@ID=@ID,@TIPO=@TIPO,@FECHA=@FECHA,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =32 AND W.ID>=17 AND W.ID<=19 ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32200_01_27_03_A32200_01_27_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32200_1_Tablas_004
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32200_1_Storeds_004

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.27.04'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32200_01_27_03_A32200_01_27_04 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32200_01_27_03_A32200_01_27_04 = False
End Function

Private Sub V_32200_1_Tablas_004()
    Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PORTAL](" & vbCrLf
sConsulta = sConsulta & "   [CODIGO] [NVARCHAR] (50) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [REMITENTE] [NVARCHAR](100) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [REMITENTE_NOMBRE] [NVARCHAR](200) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [URL] [NVARCHAR](200) NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_PORTAL] PRIMARY KEY NONCLUSTERED" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [CODIGO] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'PORTAL' AND Object_ID = Object_ID(N'CIAS'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS ADD PORTAL NVARCHAR(50) NULL CONSTRAINT DF_CIAS_PORTAL DEFAULT NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'MARCADOR_CARPETA_PLANTILLAS' AND Object_ID = Object_ID(N'PARGEN_INTERNO'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_INTERNO ADD MARCADOR_CARPETA_PLANTILLAS NVARCHAR(100) NULL CONSTRAINT DF_PARGEN_INTERNO_MARCADOR_CARPETA_PLANTILLAS DEFAULT NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32200_1_Storeds_004()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_REGISTRAR_COMPANIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_REGISTRAR_COMPANIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_REGISTRAR_COMPANIA]  @IDPORTAL int, @COD varchar(50) , @DEN varchar(100)," & vbCrLf
sConsulta = sConsulta & "@DIR varchar(255), @CP varchar(20), @POB varchar(100), @PAI INT," & vbCrLf
sConsulta = sConsulta & "@PROVI INT, @MON INT, @FCEST tinyint, @FPEST tinyint, @NIF varchar(20)," & vbCrLf
sConsulta = sConsulta & "@VOLFACT float, @CLIREF1 varchar(50),@CLIREF2 varchar(50),@CLIREF3 varchar(50),@CLIREF4 varchar(50)," & vbCrLf
sConsulta = sConsulta & "@HOM1 varchar(50), @HOM2 varchar(50), @HOM3 varchar(50), @URLCIA varchar(255)," & vbCrLf
sConsulta = sConsulta & "@COMENT text, @ORIGINS varchar(50), @PREMIUM int, @TIPOACC tinyint ,@IDI varchar(50), @IDCIA int OUTPUT, @PORTAL NVARCHAR (50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIDI int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDCIA=ISNULL(MAX(ID),0) + 1" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIDI = ID" & vbCrLf
sConsulta = sConsulta & "FROM IDI WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE COD = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS (ID,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,IDI,PORT,FCEST,NIF,PREMIUM,VOLFACT," & vbCrLf
sConsulta = sConsulta & "CLIREF1,CLIREF2,CLIREF3,CLIREF4,HOM1,HOM2,HOM3,URLCIA,COMENT,ORIGINS,PORTAL)" & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA,@COD,@DEN,@DIR,@CP,@POB,@PAI,@PROVI,@MON,@IDIDI,@IDPORTAL,@FCEST,@NIF,@PREMIUM,@VOLFACT ," & vbCrLf
sConsulta = sConsulta & "@CLIREF1, @CLIREF2, @CLIREF3, @CLIREF4, @HOM1, @HOM2, @HOM3, @URLCIA, @COMENT, @ORIGINS, @PORTAL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_PORT (CIA,PORT,FPEST,TIPOACC,ORIGINS)" & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA,@IDPORTAL,@FPEST,@TIPOACC, @ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO rel_comp (cia_prove, cia_comp)" & vbCrLf
sConsulta = sConsulta & "SELECT @IDCIA, ID" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE FCEST=3" & vbCrLf
sConsulta = sConsulta & "AND PORT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32200_01_27_04_A32200_01_27_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32200_1_Storeds_005

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.27.05'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32200_01_27_04_A32200_01_27_05 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32200_01_27_04_A32200_01_27_05 = False
End Function

Private Sub V_32200_1_Storeds_005()
Dim sConsulta As String
    
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
sConsulta = sConsulta & "@CIA VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
sConsulta = sConsulta & "              CIAS.USUPPAL AS USUPPAL,CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
sConsulta = sConsulta & "              USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
sConsulta = sConsulta & "              USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "              USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "              USU.DATEFMT DATEFMT,USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "              IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
sConsulta = sConsulta & "              USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA,MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU," & vbCrLf
sConsulta = sConsulta & "              CIAS.DEN AS CIADEN,CIAS.NIF,IDI.LANGUAGETAG,USU.NIF USUNIF,ISNULL(CON.ID,0) IDCONTACTO" & vbCrLf
sConsulta = sConsulta & "           FROM USU WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN USU_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND USU.CIA=USU_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.CIA=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN CIAS_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.CIA=CIAS_PORT.CIA AND CIAS_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON CIAS.MON=MON.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON MON2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.MON=MON2.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IDI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'PROVE PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON CIAS.COD=PROVE.FSP_COD" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN ' + @FSGS + 'CON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.ID=CON.ID_PORT AND PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "           WHERE USU.COD=''' + @COD + ''' AND CIAS.COD=''' + @CIA + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_DATOS_PORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_DATOS_PORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_DATOS_PORTAL]" & vbCrLf
sConsulta = sConsulta & "@PORTAL VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT CODIGO, REMITENTE, URL FROM PORTAL WITH (NOLOCK) WHERE CODIGO = @PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

    
End Sub
    
    
Public Function CodigoDeActualizacion32200_01_27_05_A32200_01_27_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_32200_1_Storeds_005

    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.27.06'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion32200_01_27_05_A32200_01_27_06 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion32200_01_27_05_A32200_01_27_06 = False
End Function

Private Sub V_32200_1_Storeds_006()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
sConsulta = sConsulta & "@IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "@CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "@FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "@NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "@CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "@PARTIDASPRES NVARCHAR(MAX) = NULL," & vbCrLf
sConsulta = sConsulta & "@NUMFACTURA VARCHAR(20) = NULL," & vbCrLf
sConsulta = sConsulta & "@NOM_PORTAL NVARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO,@NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                                                                  @NUMFACTURA=@NUMFACTURA,@NOM_PORTAL=@NOM_PORTAL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT,@NUMPEDIDO INT,@NUMORDEN INT,@NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME," & vbCrLf
sConsulta = sConsulta & "@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50),@PARTIDASPRES NVARCHAR(MAX)," & vbCrLf
sConsulta = sConsulta & "@NUMFACTURA VARCHAR(20),@NOM_PORTAL NVARCHAR(50)'," & vbCrLf
sConsulta = sConsulta & "@IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "@PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "@ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "@NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "@NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "@NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "@FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "@FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "@NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "@NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "@ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "@CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "@FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "@CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "@PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "@NUMFACTURA=@NUMFACTURA," & vbCrLf
sConsulta = sConsulta & "@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub
