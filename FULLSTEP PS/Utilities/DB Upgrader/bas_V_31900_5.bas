Attribute VB_Name = "bas_V_31900_5"
Option Explicit

Public Function CodigoDeActualizacion31900_03_14_04A31900_04_15_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Tablas_000
        
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_14_04A31900_04_15_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_14_04A31900_04_15_00 = False
End Function

Private Sub V_31900_5_Tablas_000()
Dim sConsulta As String

sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ITEM_OFEESC]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[ITEM_OFEESC](" & vbCrLf
sConsulta = sConsulta & "     [ID] [integer] NOT NULL," & vbCrLf
sConsulta = sConsulta & "     [ITEM] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "     [ESC] [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "     [PRECIO] [float] NULL," & vbCrLf
sConsulta = sConsulta & "     [PREC_VALIDO] [float] NULL," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_ITEM_OFEESC] PRIMARY KEY NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "     [ID] ASC," & vbCrLf
sConsulta = sConsulta & "     [ITEM] ASC," & vbCrLf
sConsulta = sConsulta & "     [ESC] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (FILLFACTOR = 90) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_OFEESC]  WITH NOCHECK ADD  CONSTRAINT [FK_ITEM_OFEESC_ITEM_OFE] FOREIGN KEY([ID], [ITEM])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[ITEM_OFE] ([ID], [ITEM])" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_OFEESC] CHECK CONSTRAINT [FK_ITEM_OFEESC_ITEM_OFE]" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[OFE_ITEM_ATRIBESC]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[OFE_ITEM_ATRIBESC](" & vbCrLf
sConsulta = sConsulta & "     [ID] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "     [ITEM] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "     [ATRIB_ID] [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "     [ESC] [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "     [VALOR_NUM] [float] NULL," & vbCrLf
sConsulta = sConsulta & "     [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_OFE_ITEM_ATRIBESC] PRIMARY KEY NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "     [ID] ASC," & vbCrLf
sConsulta = sConsulta & "     [ITEM] ASC," & vbCrLf
sConsulta = sConsulta & "     [ATRIB_ID] ASC," & vbCrLf
sConsulta = sConsulta & "     [ESC] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (FILLFACTOR = 90) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[OFE_ITEM_ATRIBESC]  WITH NOCHECK ADD  CONSTRAINT [FK_OFE_ITEM_ATRIBESC_ITEM_OFE] FOREIGN KEY([ID], [ITEM])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[ITEM_OFE] ([ID], [ITEM])" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[OFE_ITEM_ATRIBESC] CHECK CONSTRAINT [FK_OFE_ITEM_ATRIBESC_ITEM_OFE]" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub


Private Sub V_31900_5_Storeds_000()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_PRECIOS_ESCALADOS_GRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_PRECIOS_ESCALADOS_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  [dbo].[FSP_OBT_PRECIOS_ESCALADOS_GRUPO]  @ID INT,@GRUPO VARCHAR(50)=null AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            WHERE ID=@ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ITOE.ITEM, ITOE.ESC, ITOE.PRECIO, ITOE.PREC_VALIDO" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFEESC ITOE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN ITEM_OFE ITO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      ON ITOE.ITEM = ITO.ITEM  and ITO.ID = ITOE.ID " & vbCrLf
sConsulta = sConsulta & "  WHERE ITO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "  AND ITO.GRUPO=case when @GRUPO is null then ITO.GRUPO else @GRUPO " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


          
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_OFE_ATRIB_ESC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_OFE_ATRIB_ESC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_OFE_ATRIB_ESC] @ID int, @GRUPO varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SELECT DISTINCT OIA.ID, OIA.ITEM, OIA.ATRIB_ID ,OIA.ESC , OIA.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIBESC OIA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN OFE_ITEM_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON OIA.ID = g.ID  AND OIA.ATRIB_ID  = g.ATRIB_ID " & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

          
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_OFERTA_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_OFERTA_PROVE] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ofe_gr_costesdesc WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIBESC WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFEESC WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31900_03_15_00A31900_04_15_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_00A31900_04_15_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_00A31900_04_15_01 = False
End Function

Private Sub V_31900_5_Storeds_001()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_OFE_ATRIB_ESC]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_OFE_ATRIB_ESC]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_OFE_ATRIB_ESC] @ID int, @GRUPO varchar(50)=null AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  SELECT DISTINCT OIA.ID, OIA.ITEM, OIA.ATRIB_ID ,OIA.ESC , OIA.VALOR_NUM" & vbCrLf
    sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIBESC OIA WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN OFE_ITEM_ATRIB G WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       ON OIA.ID = g.ID  AND OIA.ATRIB_ID  = g.ATRIB_ID AND OIA.ITEM = G.ITEM" & vbCrLf
    sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
    sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_15_01A31900_04_15_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_01A31900_04_15_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_01A31900_04_15_02 = False
End Function

Private Sub V_31900_5_Storeds_002()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_ACTUALIZAINSTANCIANULA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_ACTUALIZAINSTANCIANULA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_CREATE_NEW_CERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_CREATE_NEW_CERTIFICADO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_DEVOLVER_SOLICITYAPROB]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_DEVOLVER_SOLICITYAPROB]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_ELIMINAR_RESPUESTAS_SIN_ENVIAR]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_ENVIAR_CERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_ENVIAR_CERTIFICADO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_ENVIAR_NOCONFORMIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_ENVIAR_NOCONFORMIDAD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_VARCERT_POND]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_VARCERT_POND]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_VARIABLES_HIJAS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_VARIABLES_HIJAS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_VARNOCONF_POND]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_VARNOCONF_POND]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_RESPUESTAS_SIN_ENVIAR]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_RESPUESTAS_SIN_ENVIAR]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_SAVE_PUNTUACION]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_SAVE_PUNTUACION]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
'    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_SOLPENDIENTES]') AND type in (N'P', N'PC'))" & vbCrLf
'    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_SOLPENDIENTES]" & vbCrLf
'    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_VALIDAR_VARIABLE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_VALIDAR_VARIABLE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_VALIDARCERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_VALIDARCERTIFICADO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_VALIDARNOCONFORMIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_VALIDARNOCONFORMIDAD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_EN_PROCESO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_EN_PROCESO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011" & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Funcion que Comprueba el Estado En proceso de una instancia" & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">Identificador de la compania</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@ID"">Identificador de la instancia</param>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>" & vbCrLf
    sConsulta = sConsulta & "''' Llamada desde: Root/Instancia_ComprobarEnProceso" & vbCrLf
    sConsulta = sConsulta & "''' Tiempo m�ximo: 0,25 seg</remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_EN_PROCESO] @CIA INT,@ID INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_EN_PROCESO @ID=@ID'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CALIFICATION]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CALIFICATION]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011" & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Devuelve el ID y la Denominacion de la calificacion que le corresponden a los puntos" & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">Codigo de compania</param>    " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@VARCAL"">ID variable de calidad</param> " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@NIVEL"">Nivel variable de calidad</param>      " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@VALUE"">Valor de la variable de calidad</param>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>Llamada desde=VariablesCalcular.vb --> ObtenerCalificacion; Tiempo m�ximo=0,1seg.</remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_GET_CALIFICATION] @CIA INT, @VARCAL AS INT,@NIVEL AS SMALLINT, @VALUE AS FLOAT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CALIFICATION @VARCAL=@VARCAL, @NIVEL =@NIVEL, @VALUE =@VALUE'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@VARCAL AS INT,@NIVEL AS SMALLINT, @VALUE AS FLOAT ',   @VARCAL =@VARCAL, @NIVEL =@NIVEL,@VALUE =@VALUE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_TIPONOCONFORMIDADES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_TIPONOCONFORMIDADES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011    " & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Lista de solicitudes q tiene un usuario." & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">Codigo de compania</param>  " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@USU"">usuario</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@IDI"">idioma en q ver los textos</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@COMBO"">sql para cargar combo o no</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@TIPO"">tipo de solicitud</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@DESDESEGUIMIENTO"">la carga del combo de seguimiento mira en vista de peticionario por lo de definir un rol como lista/departamento/etc. El resto mira instancia.peticionario</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@NUEVO_WORKFL"">Saca informaci�n de solicitudes (1) � certificados/no conformidades (0)</param>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>Llamada desde: Solicitudes.vb --> LoadData; Tiempo m�ximo: 0,3 sg</remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_GET_TIPONOCONFORMIDADES] @CIA INT,@USU VARCHAR(50), @IDI VARCHAR(20), @COMBO TINYINT=0, @TIPO INT=NULL, @DESDESEGUIMIENTO INT=NULL, @NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos los tipos de NoConformidades" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPMQA_GET_SOLICITUDES @USU= @USU, @IDI =@IDI, @COMBO=@COMBO,@TIPO=@TIPO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @USU AS VARCHAR(50), @IDI AS VARCHAR(20),@COMBO AS TINYINT, @TIPO AS INT', @USU=@USU, @IDI =@IDI, @COMBO =@COMBO, @TIPO =@TIPO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_VARIABLES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_VARIABLES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011" & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Funcion que Carga todas las variables que usen la solictud @SOLICITUD conmo origen de su calculo" & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">Identificador de la compania</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@SOLICITUD"">Identificador de la solicitud</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@NIVEL"">nivel de la variable de calidad</param>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>" & vbCrLf
    sConsulta = sConsulta & "''' Llamada desde: Root/Variables_Get" & vbCrLf
    sConsulta = sConsulta & "''' Tiempo m�ximo: 0,25 seg</remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_GET_VARIABLES] @CIA INT, @SOLICITUD INT,@NIVEL SMALLINT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VARIABLES  @SOLICITUD =@SOLICITUD, @NIVEL =@NIVEL'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SOLICITUD INT,@NIVEL SMALLINT ', @SOLICITUD =@SOLICITUD, @NIVEL =@NIVEL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_VERSION_NOCONF]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_VERSION_NOCONF]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011     " & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Procedimiento que devuelve la version m�s reciente de una no conformidad" & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">Codigo de compania</param>  " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@ID"">Identificador de la no conformidad</param>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>" & vbCrLf
    sConsulta = sConsulta & "''' Llamada desde: Root/NoConformidad_DevolverVersion  " & vbCrLf
    sConsulta = sConsulta & "''' Tiempo m�ximo: 0,4 seg</remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_GET_VERSION_NOCONF] @CIA INT, @ID INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VERSION_NOCONF @ID=@ID'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADCORRESPONDENCIACAMPOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADCORRESPONDENCIACAMPOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011        " & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Carga los id de copia_campo para la version actual y para la version q se le pasa como parametro" & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">C�digo de la compa�ia</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CERTIFICADO"">Instancia</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@VERSION"">Version</param>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>Llamada desde: Root/NoConformidad_CargarCorrespondenciaCampos Root/Certificado_CargarCorrespondenciaCampos ; Tiempo m�ximo: 0,2</remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_LOADCORRESPONDENCIACAMPOS] @CIA INT, @CERTIFICADO INT , @VERSION INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADCORRESPONDENCIACAMPOS @CERTIFICADO =@CERTIFICADO , @VERSION =@VERSION '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @CERTIFICADO INT , @VERSION INT', @CERTIFICADO =@CERTIFICADO , @VERSION =@VERSION" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011" & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Cargar las no conformidades cerradas de un proveedor seg�n los parametros dados. Se le a�aden un par de " & vbCrLf
    sConsulta = sConsulta & "''' columnas para temas javascript de comilla simple, doble y saltos de linea." & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">Id de la compa�ia</param> " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@PROVE"">Proveedor</param>        " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@IDI"">Idioma</param>  " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@ID"">Id de la No conformidad</param>  " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@TIPO"">Tipo de No conformidad</param>  " & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@FECHA"">Fecha apertura desde de No conformidad</param>     " & vbCrLf
    sConsulta = sConsulta & "''' <returns>Dataset con las no conformidades cerradas</returns>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>Llamada desde: NoConformidades.vb/Load_Data_NoConformidades_Cerradas; Tiempo m�ximo:0,1</remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDADESCERRADAS] @CIA INT, @PROVE VARCHAR(50), @IDI VARCHAR(20) =NULL, @ID INT=0, @TIPO INT=0, @FECHA VARCHAR(10)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-- Obtenemos el servidor y la base de datos del GS." & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-- Obtenemos el c�digo y el identificador del PROVEEDOR en el GS." & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-- Obtenemos las NOCONFORMIDADES cerradas." & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_NOCONFORMIDADES_CERRADAS @PROVE =@PROVE, @IDI=@IDI, @ID = @ID , @TIPO = @TIPO, @FECHA = @FECHA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @IDI VARCHAR(10), @ID INT, @TIPO INT, @FECHA VARCHAR(10)' , @PROVE = @PROVEGS, @IDI=@IDI, @ID=@ID, @TIPO=@TIPO, @FECHA=@FECHA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =32 AND W.ID>=17 AND W.ID<=19 ORDER BY W.ID ASC'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADRESPUESTASCERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADRESPUESTASCERTIFICADO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011        " & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Procedimiento que carga datos de quien realizo las diferentes versiones(respuestas) de un certificado " & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">El id de la compa�ia para obtener donde esta la base de datos de la compa�ia</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@PROVE"">C�digo del proveedor en el portal</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""lId"">Id de certificado</param>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>Llamada desde: root/Certificado_CargarRespuestas; Tiempo m�ximo:0,3 seg </remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_LOADRESPUESTASCERTIFICADO] @CIA INT,@PROVE VARCHAR(50),  @CERTIFICADO INT = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADRESPUESTASCERTIFICADO @PROVE=@PROVE, @CERTIFICADO =@CERTIFICADO '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT =NULL, @PROVE VARCHAR(50)',@CERTIFICADO =@CERTIFICADO, @PROVE=@PROVEGS" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADRESPUESTASNOCONFORMIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADRESPUESTASNOCONFORMIDAD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "/*" & vbCrLf
    sConsulta = sConsulta & "''' Revisado por: Jbg. Fecha: 13/12/2011" & vbCrLf
    sConsulta = sConsulta & "''' <summary>" & vbCrLf
    sConsulta = sConsulta & "''' Procedimiento que carga datos de quien realizo las diferentes versiones(respuestas) de una no conformidad" & vbCrLf
    sConsulta = sConsulta & "''' </summary>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@CIA"">El id de la compa�ia para obtener donde esta la base de datos de la compa�ia</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@PROVE"">C�digo del proveedor en el portal</param>" & vbCrLf
    sConsulta = sConsulta & "''' <param name=""@NOCONFORMIDAD"">Id de no conformidad</param>" & vbCrLf
    sConsulta = sConsulta & "''' <remarks>Llamada desde: root/NoConformidad_CargarRespuestas; Tiempo m�ximo:0,3 seg </remarks>*/" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSQA_LOADRESPUESTASNOCONFORMIDAD] @CIA INT,@PROVE VARCHAR(50),  @NOCONFORMIDAD INT = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADRESPUESTASNOCONFORMIDAD @PROVE=@PROVE, @NOCONFORMIDAD =@NOCONFORMIDAD '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NOCONFORMIDAD INT =NULL, @PROVE VARCHAR(50)',@NOCONFORMIDAD =@NOCONFORMIDAD, @PROVE=@PROVEGS" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_15_02A31900_04_15_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_02A31900_04_15_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_02A31900_04_15_03 = False
End Function

Private Sub V_31900_5_Storeds_003()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisador por : dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSP_LOGIN] @CIA VARCHAR(50), @COD VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "-- Descripci�n: Obtiene los campos de la tabla USU y los datos de la compa��a de la tabla CIAS" & vbCrLf
    sConsulta = sConsulta & "--               que ha hecho el login en la aplicaci�n." & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de entrada:@CIA: C�digo de la compa��a." & vbCrLf
    sConsulta = sConsulta & "--         @COD: C�digo de la persona que se ha validado en la aplicaci�n." & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de salida: Un registro con propiedades del usuario y la compa��a" & vbCrLf
    sConsulta = sConsulta & "-- Tiempo m�ximo: 0 sg                         " & vbCrLf
    sConsulta = sConsulta & "-- Llamada desde: PMPortalDatabaseServer --> Root.vb --> User_LoadUserData" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
    sConsulta = sConsulta & "       CIAS.USUPPAL AS USUPPAL, CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
    sConsulta = sConsulta & "       USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
    sConsulta = sConsulta & "       USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "       USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
    sConsulta = sConsulta & "       USU.DATEFMT DATEFMT, USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "       IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
    sConsulta = sConsulta & "       USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA , MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU," & vbCrLf
    sConsulta = sConsulta & "       CIAS.DEN AS CIADEN, CIAS.NIF" & vbCrLf
    sConsulta = sConsulta & "  FROM USU WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "        LEFT JOIN USU_PORT WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.ID=USU_PORT.USU" & vbCrLf
    sConsulta = sConsulta & "              AND USU_PORT.PORT=0" & vbCrLf
    sConsulta = sConsulta & "              AND USU.CIA=USU_PORT.CIA" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.CIA=CIAS.ID" & vbCrLf
    sConsulta = sConsulta & "        LEFT JOIN CIAS_PORT WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.CIA=CIAS_PORT.CIA" & vbCrLf
    sConsulta = sConsulta & "              AND CIAS_PORT.PORT=0" & vbCrLf
    sConsulta = sConsulta & "        LEFT JOIN MON WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON CIAS.MON=MON.ID" & vbCrLf
    sConsulta = sConsulta & "        LEFT JOIN MON MON2 WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.MON=MON2.ID" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN IDI WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "               ON USU.IDI=IDI.ID" & vbCrLf
    sConsulta = sConsulta & " WHERE USU.COD=@COD" & vbCrLf
    sConsulta = sConsulta & "   AND CIAS.COD=@CIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ACCIONES_ROL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ACCIONES_ROL]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ACCIONES_ROL] @CIA INT,@BLOQUE INT,@ROL INT, @IDI VARCHAR(50),@COPIA TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ACCIONES_ROL @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @ROL INT, @IDI VARCHAR(50), @COPIA TINYINT ',@BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ACT_REL_CIAS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ACT_REL_CIAS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ACT_REL_CIAS](@COMP VARCHAR(50),@PROVE VARCHAR(50),@ACTIVAS INTEGER,@NUEVAS INTEGER) AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ID_COMP AS INT" & vbCrLf
    sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WITH (NOLOCK) WHERE COD=@COMP)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "UPDATE REL_CIAS SET SOLIC_ACT=@ACTIVAS,SOLIC_NUE=@NUEVAS " & vbCrLf
    sConsulta = sConsulta & " WHERE CIA_COMP=@ID_COMP " & vbCrLf
    sConsulta = sConsulta & "   AND COD_PROVE_CIA=@PROVE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ACTUALIZAR_EN_PROCESO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ACTUALIZAR_EN_PROCESO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ACTUALIZAR_EN_PROCESO] @CIA INT, @ID INT, @BLOQUE INT = 0, @EN_PROCESO INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ACTUALIZAR_EN_PROCESO @ID=@ID, @BLOQUE=@BLOQUE,@EN_PROCESO=@EN_PROCESO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @BLOQUE INT, @EN_PROCESO INT', @ID=@ID,@BLOQUE=@BLOQUE,@EN_PROCESO=@EN_PROCESO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ARBOLPRESUPUESTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ARBOLPRESUPUESTOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ARBOLPRESUPUESTOS] @CIA INT, @TIPO INTEGER, @IDI VARCHAR(50), @ANYO INTEGER, @UON1 AS VARCHAR(50) = NULL, @UON2 AS VARCHAR(50) = NULL, @UON3 AS VARCHAR(50) = NULL, @CODIGO AS VARCHAR(50) = NULL, @DENOMINACION AS VARCHAR(50) = NULL, @UONVacia AS TINYINT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ARBOLPRESUPUESTOS @TIPO=@TIPO, @IDI =@IDI , @ANYO =@ANYO , @UON1 =@UON1 , @UON2 =@UON2 , @UON3 =@UON3, @CODIGO=@CODIGO, @DENOMINACION=@DENOMINACION, @UONVacia= @UONVacia '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INTEGER, @IDI VARCHAR(50), @ANYO INTEGER, @UON1 AS VARCHAR(50) = NULL, @UON2 AS VARCHAR(50) = NULL, @UON3 AS VARCHAR(50) = NULL, @CODIGO AS VARCHAR(50) = NULL, @DENOMINACION AS VARCHAR(50) = NULL, @UONVacia TINYINT', @TIPO=@TIPO, @IDI =@IDI , @ANYO =@ANYO , @UON1 =@UON1 , @UON2 =@UON2 , @UON3 =@UON3, @CODIGO=@CODIGO, @DENOMINACION=@DENOMINACION, @UONVacia=@UONVacia" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ARTICULOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ARTICULOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ARTICULOS] @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA', @INSTANCIAMONEDA VARCHAR(3)='EUR',@CARGAR_ULT_ADJ TINYINT=0 ,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL, @CARGAR_ORG TINYINT=0  AS " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT,@CARGAR_ORG=@CARGAR_ORG'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL, @CARGAR_ORG TINYINT',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT,@CARGAR_ORG=@CARGAR_ORG" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ARTICULOS_GMN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ARTICULOS_GMN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ARTICULOS_GMN] @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA', @INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT AS    " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS_GMN @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS] @CIA INT, @ADJUNTOSACT NVARCHAR(50), @ADJUNTOSNEW NVARCHAR(50),@LAST_ADJUNTO INT OUTPUT, @DEFECTO INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_CREAR_ADJUNTOS_COPIADOS @ADJUNTOSACT =@ADJUNTOSACT ,@ADJUNTOSNEW =@ADJUNTOSNEW,@LAST_ADJUNTO=@LAST_ADJUNTO OUTPUT,@DEFECTO=@DEFECTO '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ADJUNTOSACT NVARCHAR(50), @ADJUNTOSNEW NVARCHAR(50),@LAST_ADJUNTO INT OUTPUT, @DEFECTO INT', @ADJUNTOSACT =@ADJUNTOSACT ,@ADJUNTOSNEW =@ADJUNTOSNEW,@LAST_ADJUNTO=@LAST_ADJUNTO OUTPUT,@DEFECTO=@DEFECTO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEFTABLAEXTERNA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEFTABLAEXTERNA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEFTABLAEXTERNA] @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEFTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DESTINOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DESTINOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DESTINOS] @CIA INT, @PER VARCHAR(50) =NULL, @COD VARCHAR(50) =NULL, @IDI VARCHAR(20) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DESTINOS @PER =@PER ,@COD =@COD, @IDI=@IDI '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PER VARCHAR(50) =NULL, @COD VARCHAR(50) =NULL,@IDI VARCHAR(20)', @PER =@PER ,@COD =@COD,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLUCION_INSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLUCION_INSTANCIA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLUCION_INSTANCIA] @CIA INT,  @CIAPROVE INT, @INSTANCIA INT,@PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE '" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT ', @INSTANCIA=@INSTANCIA, @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
    sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
    sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
    sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
    sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
    sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
    sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
    sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
    sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
    sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
    sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
    sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
    sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
    sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
    sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
    sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_COMENT_ESTADO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_COMENT_ESTADO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_COMENT_ESTADO] @CIA INT,@ID INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DEVOLVER_COMENT_ESTADO @ID=@ID'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_CONTACTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_CONTACTOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_CONTACTOS] @CIA INT,@PROVE VARCHAR(50), @SOLOPORTAL TINYINT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_DEVOLVER_CONTACTOS_QA @PROVE=@PROVE,@SOLOPORTAL=@SOLOPORTAL'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE  VARCHAR(50), @SOLOPORTAL TINYINT ', @PROVE=@PROVE,@SOLOPORTAL=@SOLOPORTAL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_DIAGBLOQUES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_DIAGBLOQUES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_DIAGBLOQUES] @CIA INT,@INSTANCIA INT, @IDIOMA VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_DIAGBLOQUES @INSTANCIA=@INSTANCIA,@IDIOMA=@IDIOMA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDIOMA VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_DIAGENLACES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_DIAGENLACES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_DIAGENLACES] @CIA INT,@BLOQUE INT, @IDIOMA VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_DIAGENLACES @BLOQUE=@BLOQUE,@IDIOMA=@IDIOMA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @IDIOMA VARCHAR(50) ', @BLOQUE=@BLOQUE,@IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ENLACESACCION]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ENLACESACCION]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ENLACESACCION] @CIA INT, @ACCION INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = 'SELECT ENLACE FROM ' +@FSGS +'INSTANCIA_CAMINO WITH (NOLOCK) WHERE ACCION = @ACCION'" & vbCrLf
    sConsulta = sConsulta & "EXEC sp_executesql @SQL, N'@ACCION INT', @ACCION =@ACCION" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL] @CIA INT,@INSTANCIA INT , @IDI VARCHAR(50) , @PROVE VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE  @PROVEGS VARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPA_ACTUAL @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50),@PROVE VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PROVE=@PROVEGS" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO] @CIA INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPA_ACTUAL_TRASLADO @INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA  INT, @IDI VARCHAR(50)', @INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPAS_BLOQUEO] @CIA INT,@BLOQUE INT , @IDI VARCHAR(50)  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_ETAPAS_BLOQUEO @BLOQUE=@BLOQUE,@IDI=@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @IDI VARCHAR(50) ', @BLOQUE=@BLOQUE,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_HISTORICO_INSTANCIA] @CIA INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_HISTORICO_INSTANCIA @INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50) ', @INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_NUM_VINCULACIONES] @CIA INT, @INSTANCIA INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEVOLVER_NUM_VINCULACIONES @INSTANCIA =@INSTANCIA '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @INSTANCIA INT', @INSTANCIA =@INSTANCIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_UO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_UO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_UO] @CIA INT,@IDI VARCHAR(50) ,@USU VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DEVOLVER_UO @IDI=@IDI, @USU=@USU'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50) ,@USU VARCHAR(50)', @IDI=@IDI, @USU=@USU" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ESTADOS_NOCONF]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ESTADOS_NOCONF]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ESTADOS_NOCONF] @CIA INT,@INSTANCIA INT , @DEN VARCHAR(50)=null,  @IDI AS VARCHAR(4)  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ESTADOS_NOCONF @INSTANCIA = @INSTANCIA, @DEN=@DEN , @IDI=@IDI '" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT , @DEN VARCHAR(50)=null ,@IDI VARCHAR(4)',  @INSTANCIA = @INSTANCIA , @DEN=@DEN,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_EXECREMOTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_EXECREMOTO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_EXECREMOTO] @CIA INT, @STMT NTEXT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'SP_EXECUTESQL @STMT = @STMT '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@STMT NTEXT', @STMT =@STMT" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_FIELDSCALC]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_FIELDSCALC]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_FIELDSCALC] @CIA INT, @ID INT, @CASO INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos los campos calculados del grupo" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_FIELDSCALC @ID=@ID,@CASO =@CASO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@ID AS INT, @CASO AS INT', @ID=@ID,@CASO=@CASO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_FORMASPAGO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_FORMASPAGO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_FORMASPAGO] @CIA INT,  @COD VARCHAR(50) =NULL, @IDIOMA VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_FORMASPAGO @COD =@COD,@IDIOMA=@IDIOMA '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =NULL,@IDIOMA VARCHAR(50)',@COD =@COD,@IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_CONTRATO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_CONTRATO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GET_CONTRATO] @CIA INT, @ID INT, @IDIOMA VARCHAR(3) = 'SPA'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_CONTRATO @ID=@ID,@IDIOMA=@IDIOMA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,  @IDIOMA VARCHAR(50)', @ID=@ID,@IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_PRECONDICIONES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_PRECONDICIONES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GET_PRECONDICIONES] @CIA INT, @BSOLICITUD BIT, @IDACC INTEGER, @IDI VARCHAR(3) = 'SPA', @INSTANCIA INT = 0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_PRECONDICIONES @BSOLICITUD=@BSOLICITUD,@IDACC=@IDACC,@IDI=@IDI,@INSTANCIA=@INSTANCIA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BSOLICITUD BIT, @IDACC INTEGER, @IDI VARCHAR(3), @INSTANCIA INT ', @BSOLICITUD=@BSOLICITUD,@IDACC=@IDACC,@IDI=@IDI,@INSTANCIA=@INSTANCIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE] @CIA INT,@PROVE VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "-- Descripci�n: Obtiene las solicitudes pendientes de un proveedor en el Portal" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de entrada:@PRV: C�digo del proveedor del Portal" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de salida: El n� de solicitudes pendientes de un proveedor en el Portal" & vbCrLf
    sConsulta = sConsulta & "-- Tiempo m�ximo: 0 sg                         " & vbCrLf
    sConsulta = sConsulta & "-- Llamada desde: cSolicitudes.DevolverPendientes" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_SOLICITUDES_PDTES_PROVE @PRV=@PRV'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50)', @PRV=@PROVEGS" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETADJUN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETADJUN] @CIA INT, @TIPO INT, @ID INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUN @TIPO =@TIPO, @ID=@ID '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @TIPO INT, @ID INT', @TIPO =@TIPO, @ID=@ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETADJUNTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETADJUNTOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETADJUNTOS] @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETALMACENES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETALMACENES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETALMACENES]  @CIA AS INT,@COD VARCHAR(4)=NULL,@COD_CENTRO VARCHAR(4)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos las Almacenes" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETAlmacenes @COD,@COD_CENTRO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL,@COD_CENTRO VARCHAR(4)=NULL', @COD=@COD,@COD_CENTRO=@COD_CENTRO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETCENTROS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETCENTROS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETCENTROS] @CIA INT,  @COD VARCHAR(4)=NULL,@COD_ORGCOMPRAS VARCHAR(4)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos los CENTROS" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETCENTROS  @COD,@COD_ORGCOMPRAS'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL,@COD_ORGCOMPRAS VARCHAR(4)=NULL', @COD = @COD,@COD_ORGCOMPRAS=@COD_ORGCOMPRAS" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDEPARTAMENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDEPARTAMENTOS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETDEPARTAMENTOS]  @CIA INT,@NIVEL INT=NULL,@UON1 VARCHAR(50)=NULL,@UON2 VARCHAR(50)=NULL,@UON3 VARCHAR(50)=NULL,@USU VARCHAR(50)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDEPARTAMENTOS @NIVEL=@NIVEL,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@USU=@USU'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NIVEL INT=null , @UON1 VARCHAR(20)=null , @UON2 VARCHAR(20)=null, @UON3 VARCHAR(20)=null, @USU VARCHAR(20)=null ', @NIVEL=@NIVEL,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@USU=@USU" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(22/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETDESGLOSE] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL  AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETDESGLOSE @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDICTIONARYDATA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDICTIONARYDATA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETDICTIONARYDATA] @MODULEID int,@LANGUAGE varchar(3)" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'SELECT ID,TEXT_' + @LANGUAGE + ' FROM WEBFSPMTEXT WITH (NOLOCK) WHERE MODULO=@MODULEID'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@MODULEID INT', @MODULEID=@MODULEID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETFIELD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETFIELD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETFIELD] @CIA INT,  @ID INT, @IDI VARCHAR(20) = NULL, @ATTACH AS TINYINT=0, @SOLICITUD AS INT = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELD  @ID=@ID, @IDI = @IDI , @ATTACH =@ATTACH, @SOLICITUD=@SOLICITUD '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @ATTACH TINYINT=0, @SOLICITUD  INT = NULL', @ID=@ID, @IDI = @IDI , @ATTACH =@ATTACH,@SOLICITUD=@SOLICITUD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETFORM]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETFORM]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETFORM] @CIA INT, @ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD INT = NULL, @SOLICPROVE INT =1  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFORM @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD, @SOLICPROVE=@SOLICPROVE '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @SOLICITUD  INT = NULL, @SOLICPROVE INT =1', @ID=@ID, @IDI = @IDI, @SOLICITUD=@SOLICITUD,@SOLICPROVE=@SOLICPROVE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTADJUN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTADJUN] @CIA INT, @TIPO INT, @ID INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "/*" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "@TIPO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "1: El adjunto est� guardado en la BD del gs en COPIA_CAMPO_ADJUN" & vbCrLf
    sConsulta = sConsulta & "3: El adjunto est� guardado en la BD del gs en COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "*/" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUN @TIPO =@TIPO, @ID=@ID '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @TIPO INT, @ID INT', @TIPO =@TIPO, @ID=@ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTADJUNDATA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTADJUNDATA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTADJUNDATA] @CIA INT=NULL, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "-- Descripci�n: Obtiene los datos de un fichero adjunto" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de entrada: @CIA: Cod de la compa�ia" & vbCrLf
    sConsulta = sConsulta & "--                        @ID: Id del adjunto" & vbCrLf
    sConsulta = sConsulta & "--                        @INIT: Init" & vbCrLf
    sConsulta = sConsulta & "--                        @OFFSEt: Offset" & vbCrLf
    sConsulta = sConsulta & "--                        @TIPO: Tipo" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de salida: Los datos de un fichero adjunto" & vbCrLf
    sConsulta = sConsulta & "-- Tiempo m�ximo: 0 sg                         " & vbCrLf
    sConsulta = sConsulta & "-- Llamada desde: PMPortalDatabaseServer --> Root.vb --> Adjunto_InstReadData" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTANCIA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTANCIA] @CIA INT, @ID INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTANCIA @ID=@ID, @IDI =@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI AS VARCHAR(20) =NULL', @ID=@ID, @IDI =@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTANCIAS_PADRE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTANCIAS_PADRE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTANCIAS_PADRE] @CIA INT, @ID INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETINSTANCIAS_PADRE @ID=@ID, @IDI =@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI AS VARCHAR(20) =NULL', @ID=@ID, @IDI =@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_FORMATO_CONTACTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_FORMATO_CONTACTO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_FORMATO_CONTACTO] @CON_PORTAL INT , @COMP VARCHAR(50) ,@PROVE VARCHAR(50),@DATEFMT VARCHAR(50) OUTPUT, @THOUSANFMT CHAR(1) =NULL OUTPUT, @DECIMALFMT CHAR(1)=NULL OUTPUT, @PRECISIONFMT TINYINT =NULL OUTPUT AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ID_COMP INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ID_CIA INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@COMP)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @ID_CIA=CIA_PROVE FROM REL_CIAS WITH(NOLOCK) WHERE COD_PROVE_CIA=@PROVE AND CIA_COMP=@ID_COMP" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @DATEFMT=DATEFMT, @THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT FROM USU WITH(NOLOCK) WHERE CIA=@ID_CIA AND ID=@CON_PORTAL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO] @CIA INT, @SOLICITUD INT = 0, @ID INT= 0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_ADJUNTODEFECTO @SOLICITUD=@SOLICITUD, @ID=@ID'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SOLICITUD INT=0, @ID INT', @SOLICITUD=@SOLICITUD, @ID=@ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTFIELD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTFIELD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETINSTFIELD] @CIA INT, @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT, @CIAPROV INT = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "-- Descripci�n: Obtiene los datos de un campo" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de entrada: @CIA: Cod de la compa�ia" & vbCrLf
    sConsulta = sConsulta & "--                        @ID: Id del campo" & vbCrLf
    sConsulta = sConsulta & "--                        @IDI: Idioma" & vbCrLf
    sConsulta = sConsulta & "--                        @INSTANCIA: Id de la instancia" & vbCrLf
    sConsulta = sConsulta & "--                        @CIAPROV: Proveedor" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de salida: Los datos de un fichero adjunto" & vbCrLf
    sConsulta = sConsulta & "-- Tiempo m�ximo: 0 sg                         " & vbCrLf
    sConsulta = sConsulta & "-- Llamada desde: PMPortalDatabaseServer --> Root.vb --> Adjunto_InstReadData" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTFIELD @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT', @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETORGANIZACIONESCOMPRAS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETORGANIZACIONESCOMPRAS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETORGANIZACIONESCOMPRAS] @CIA int, @COD VARCHAR(4)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos las ORGANIZACIONES de COMPRA" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETORGANIZACIONESCOMPRAS @COD'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(4)=NULL', @COD=@COD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETREQUEST]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETREQUEST]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETREQUEST] @CIA INT, @ID INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUEST @ID=@ID, @IDI =@IDI '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI AS VARCHAR(20) =NULL', @ID=@ID, @IDI =@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETREQUESTADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETREQUESTADJUN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETREQUESTADJUN] @CIA INT, @ID INT AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUESTADJUN @ID=@ID '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT ',@ID=@ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETREQUESTADJUNDATA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETREQUESTADJUNDATA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETREQUESTADJUNDATA] @CIA INT, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0  AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETREQUESTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0 ',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETTABLAEXTERNA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETTABLAEXTERNA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETTABLAEXTERNA] @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETTIPOSOLICITUD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETTIPOSOLICITUD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETTIPOSOLICITUD] @CIA INT,@ID INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos El tipo de solicitud" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETTIPOSOLICITUD @ID'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID AS INT', @ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETUONS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETUONS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETUONS]  @CIA AS INT, @IDI VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos las Unidades Organizativas" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETUONS @IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI AS VARCHAR(50)', @IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE] @CIA INT,@PROVE VARCHAR(50), @IDI VARCHAR(50), @TIPO INT =0 , @FECHA  VARCHAR(10)=NULL, @INSTANCIA INT=0, @FILAESTADO INT =0, @FORMATOFECHA VARCHAR(20)  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETWORKFLOW_PROVE @PRV=@PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@FILAESTADO=@FILAESTADO,@FORMATOFECHA=@FORMATOFECHA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @IDI VARCHAR(50) , @TI INT,@FEC  VARCHAR(10),@INS INT, @FILAESTADO INT,@FORMATOFECHA  VARCHAR(20)', @PRV=@PROVEGS,@IDI=@IDI, @TI=@TIPO,@FEC=@FECHA,@INS=@INSTANCIA,@FILAESTADO=@FILAESTADO,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMN1]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMN1]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMN1] @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN1 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50) ,  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMN2]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMN2]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMN2] @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN2 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50),  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMN3]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMN3]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMN3] @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL, @GMN2 VARCHAR(50) = NULL,  @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN3 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 , @GMN2 = @GMN2 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50), @GMN2 VARCHAR(50) ,  @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @GMN2 = @GMN2 ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMN4]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMN4]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMN4] @CIA INT, @NUMMAX INT = NULL , @GMN1 VARCHAR(50) = NULL, @GMN2 VARCHAR(50) = NULL,  @GMN3 VARCHAR(50) = NULL, @COD VARCHAR(50) = NULL, @DEN VARCHAR(200) = NULL, @IDI VARCHAR(50) = 'SPA'  , @COINCID TINYINT = 1 , @ORDENPORDEN TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMN4 @NUMMAX =@NUMMAX , @GMN1 =@GMN1 , @GMN2 = @GMN2 , @GMN3 =@GMN3 ,  @COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NUMMAX INT , @GMN1 VARCHAR(50), @GMN2 VARCHAR(50) ,  @GMN3 VARCHAR(50) = NULL, @COD VARCHAR(50) , @DEN VARCHAR(200) , @IDI VARCHAR(50)  , @COINCID TINYINT  , @ORDENPORDEN TINYINT=0 ', @NUMMAX =@NUMMAX , @GMN1 =@GMN1 ,  @GMN2 = @GMN2 ,@GMN3 =@GMN3  ,@COD =@COD , @DEN =@DEN , @IDI =@IDI  , @COINCID =@COINCID  , @ORDENPORDEN =@ORDENPORDEN" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMNS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMNS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMNS] @CIA INT,@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) = 'SPA' AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMNS @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) ', @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GUARDARCOMENTARIOADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GUARDARCOMENTARIOADJUNTO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GUARDARCOMENTARIOADJUNTO] @CIA INT, @ID INT,  @TIPO INT =0, @INSTANCIA INT =0, @COMENT VARCHAR(4000) = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GUARDARCOMENTARIOADJUNTO @ID=@ID,@TIPO=@TIPO,@INSTANCIA=@INSTANCIA ,@COMENT=@COMENT'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@TIPO INT =0, @INSTANCIA INT =0, @COMENT VARCHAR(4000) = NULL',@ID =@ID,@TIPO=@TIPO,@INSTANCIA=@INSTANCIA,@COMENT=@COMENT" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_IDACCIONGUARDAR_ROL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_IDACCIONGUARDAR_ROL]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_IDACCIONGUARDAR_ROL] @BLOQUE INT,@ROL INT,@CIA INT  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_IDACCIONGUARDAR_ROL @BLOQUE=@BLOQUE,@ROL=@ROL'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,  N'@BLOQUE INT,@ROL INT', @BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ADJUNTO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LEER_ADJUNTO] @CIA INT, @ID INT = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LEER_ADJUNTO @ID=@ID'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT = NULL', @ID=@ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LISTADOS_ROL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LISTADOS_ROL]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LISTADOS_ROL] @CIA INT,@BLOQUE INT,@ROL INT , @IDI VARCHAR(3),@COPIA TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LISTADOS_ROL @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT,@ROL INT ,@IDI VARCHAR(3),@COPIA TINYINT', @BLOQUE=@BLOQUE,@ROL=@ROL,@IDI=@IDI,@COPIA=@COPIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOAD_ACCION]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOAD_ACCION]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOAD_ACCION] @CIA INT,@ACCION INT , @IDI VARCHAR(50)  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOAD_ACCION @ACCION=@ACCION,@IDI=@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ACCION INT, @IDI VARCHAR(50) ', @ACCION=@ACCION,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOAD_ETAPAS_REALIZADAS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOAD_ETAPAS_REALIZADAS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOAD_ETAPAS_REALIZADAS] @CIA INT,@ACCION INT, @IDI VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOAD_ETAPAS_REALIZADAS @ACCION=@ACCION,@IDI=@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ACCION INT, @IDI VARCHAR(50) ', @ACCION=@ACCION,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADESTADOSINSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADESTADOSINSTANCIA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADESTADOSINSTANCIA] @CIA INT,@INSTANCIA INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADESTADOSINSTANCIA @INSTANCIA=@INSTANCIA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT ', @INSTANCIA=@INSTANCIA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADFIELDSDESGLOSECALC]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADFIELDSDESGLOSECALC]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADFIELDSDESGLOSECALC] @CIA INT,@CAMPO INT, @SOLICITUD INT,  @PER VARCHAR(50)=NULL  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADFIELDSDESGLOSECALC @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT, @SOLICITUD  INT,@PER VARCHAR(50) ', @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADINSTFIELDSCALC]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADINSTFIELDSCALC]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADINSTFIELDSCALC] @CIA INT, @INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50)='SPA', @PROVE varchar(50)=null, @NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTFIELDSCALC @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSCALC @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT, @IDI VARCHAR(50),@PROVE varchar(50)', @INSTANCIA = @INSTANCIA,@VERSION=@VERSION, @IDI=@IDI,@PROVE=@PROVE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC] @CIA INT, @CAMPO INT , @INSTANCIA INT = NULL ,  @PROVE VARCHAR(50) = NULL,@NUEVO_WORKFL TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
    sConsulta = sConsulta & "  SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTFIELDSDESGLOSECALC @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE'" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "  SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADINSTFIELDSDESGLOSECALC @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT,@INSTANCIA INT = NULL ,  @PROVE VARCHAR(50) = NULL', @CAMPO=@CAMPO, @INSTANCIA=@INSTANCIA, @PROVE=@PROVE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADNOCONFORMIDADES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADNOCONFORMIDADES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADNOCONFORMIDADES] @CIA INT,@PROVE VARCHAR(50)=NULL, @USU VARCHAR(50), @TIPO INT=NULL, @FECHA AS VARCHAR(10)=NULL, @FILAESTADO AS INT=0, @ID AS INT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el c�digo del proveedor en el GS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA" & vbCrLf
    sConsulta = sConsulta & "FROM REL_CIAS RC WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CIAS C WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       ON RC.CIA_PROVE = C.ID" & vbCrLf
    sConsulta = sConsulta & "WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos las NoConformidades" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_NO_CONFORMIDADES @USU= @USU, @TIPO=@TIPO, @FECHA=@FECHA, @FILAESTADO=@FILAESTADO, @PROVE='+@PROVEGS+', @ID=@ID'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @USU AS VARCHAR(50), @TIPO AS INT, @FECHA AS VARCHAR(10), @FILAESTADO AS INT, @ID AS INT, @PROVE AS VARCHAR(50)', @USU=@USU, @TIPO =@TIPO, @FECHA=@FECHA, @FILAESTADO=@FILAESTADO, @ID=@ID, @PROVE=@PROVEGS" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADSOLICITUDES_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADSOLICITUDES_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_LOADSOLICITUDES_PROVE] @CIA INT,@PROVE VARCHAR(50), @TIPO INT =0, @IDI VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADSOLICITUDES_PROVE @PROVE=@PROVE,@TIPO=@TIPO,@IDI=@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50), @TIPO INT, @IDI VARCHAR(50) ', @PROVE=@PROVEGS, @TIPO=@TIPO,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_MONEDAS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_MONEDAS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_MONEDAS] @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1, @IDIOMA VARCHAR(20),@ALLDATOS TINYINT = 0 AS" & vbCrLf
    sConsulta = sConsulta & "--jbg 300309" & vbCrLf
    sConsulta = sConsulta & "--Descripci�n: Devolver 2(cod + den ) o todos los campos de la tabla monedas" & vbCrLf
    sConsulta = sConsulta & "--Param. Entrada:" & vbCrLf
    sConsulta = sConsulta & "-- @CIA        codigo de compania" & vbCrLf
    sConsulta = sConsulta & "-- @COD        codigo de moneda" & vbCrLf
    sConsulta = sConsulta & "-- @DEN        denominaci�n de moneda" & vbCrLf
    sConsulta = sConsulta & "-- @COINCID    coincidencia total o por like" & vbCrLf
    sConsulta = sConsulta & "-- @IDIOMA idioma" & vbCrLf
    sConsulta = sConsulta & "--  @ALLDATOS   t2(cod + den ) o todos los campos de la tabla" & vbCrLf
    sConsulta = sConsulta & "--Param. Salida: -" & vbCrLf
    sConsulta = sConsulta & "--Llamada desde: pmportaldatabaseserver/root/Monedas_Get" & vbCrLf
    sConsulta = sConsulta & "--Tiempo:  0,1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_MONEDAS @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA,@ALLDATOS=@ALLDATOS'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50), @DEN VARCHAR(2000) , @COINCID TINYINT,  @IDIOMA VARCHAR (20), @ALLDATOS TINYINT', @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA, @ALLDATOS=@ALLDATOS" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PAISES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PAISES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PAISES] @IDIOMA VARCHAR(20), @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "-- Descripci�n: Devuelve los pa�ses que cumplen las condiciones que se le pasan" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de entrada:@COD: C�digo del pa�s o parte" & vbCrLf
    sConsulta = sConsulta & "--                       @DEN: Denominaci�n o parte de la ella" & vbCrLf
    sConsulta = sConsulta & "--                       @COINCID: S hay que usar un like o no en la consulta" & vbCrLf
    sConsulta = sConsulta & "--                       @IDIOMA: Idioma en el que se quiere obtener las denominaciones" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de salida: los pa�ses que cumplen las condiciones" & vbCrLf
    sConsulta = sConsulta & "-- Tiempo m�ximo: 0 sg                         " & vbCrLf
    sConsulta = sConsulta & "-- Llamada desde: PMPortalDatabaseServer --> Root.vb --> Paises_Get" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PAISES @COD=@COD ,@DEN=@DEN ,@COINCID=@COINCID, @IDIOMA=@IDIOMA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @COINCID TINYINT, @IDIOMA VARCHAR(20)', @COD=@COD , @DEN=@DEN , @COINCID=@COINCID, @IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARGEN_INTERNO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PARGEN_INTERNO] @CIA INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL='SELECT * FROM ' + @FSGS + 'PARGEN_INTERNO WITH (NOLOCK)'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARTICIPANTES_PER]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARTICIPANTES_PER]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PARTICIPANTES_PER] @CIA INT,@ROL INT , @INSTANCIA INT, @FILNOM VARCHAR(500)=NULL, @FILAPE VARCHAR(500)=NULL, @FILUON1 VARCHAR(50)=NULL, @FILUON2 VARCHAR(50)=NULL, @FILUON3 VARCHAR(50)=NULL , @FILDEP VARCHAR(50)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PARTICIPANTES_PER @ROL=@ROL,@INSTANCIA=@INSTANCIA,@FILNOM=@FILNOM,@FILAPE=@FILAPE,@FILUON1=@FILUON1,@FILUON2=@FILUON2,@FILUON3=@FILUON3,@FILDEP=@FILDEP'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ROL INT ,@INSTANCIA INT ,@FILNOM VARCHAR(500), @FILAPE VARCHAR(500), @FILUON1 VARCHAR(50), @FILUON2 VARCHAR(50), @FILUON3 VARCHAR(50) , @FILDEP VARCHAR(50) ', @ROL=@ROL,@INSTANCIA=@INSTANCIA,@FILNOM=@FILNOM,@FILAPE=@FILAPE,@FILUON1=@FILUON1,@FILUON2=@FILUON2,@FILUON3=@FILUON3,@FILDEP=@FILDEP" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PARTICIPANTES_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PARTICIPANTES_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PARTICIPANTES_PROVE] @CIA INT,@ROL INT , @USU VARCHAR(50), @INSTANCIA INT, @COD VARCHAR(50), @DEN VARCHAR(500), @NIF VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PARTICIPANTES_PROVE @ROL=@ROL,@USU=@USU,@INSTANCIA=@INSTANCIA,@COD=@COD,@DEN=@DEN,@NIF=@NIF'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ROL INT , @USU VARCHAR(50), @INSTANCIA INT, @COD VARCHAR(50), @DEN VARCHAR(500), @NIF VARCHAR(50) ', @ROL=@ROL,@USU=@USU,@INSTANCIA=@INSTANCIA,@COD=@COD,@DEN=@DEN,@NIF=@NIF" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PRES1]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PRES1]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRES1] @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES1 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
        
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PRES2]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PRES2]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRES2] @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES2 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PRES3]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PRES3]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRES3] @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES3 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PRES4]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PRES4]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRES4] @CIA INT,@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PRES4 @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4 '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES1 INT, @PRES2 INT, @PRES3 INT, @PRES4 INT ', @PRES1=@PRES1, @PRES2 =@PRES2 , @PRES3 =@PRES3 , @PRES4 =@PRES4" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PRESUP_LIT]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PRESUP_LIT]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PRESUP_LIT] @CIA INT ,@TIPO INT, @IDI VARCHAR(50) ='SPA'  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
    sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtenemos El tipo de solicitud" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_PRESUP_LIT @TIPO=@TIPO,@IDI =@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@TIPO AS INT, @IDI AS VARCHAR(50)', @TIPO=@TIPO,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PROVE] @CIA INT, @PROVE VARCHAR(50), @PORTAL TINYINT = 0, @IDIOMA VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PORTAL = 1 " & vbCrLf
    sConsulta = sConsulta & "  SELECT @PROVEGS = COD_PROVE_CIA   FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "  SET @PROVEGS = @PROVE" & vbCrLf
    sConsulta = sConsulta & "  " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVE @PROVE =@PROVE, @IDIOMA=@IDIOMA '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50), @IDIOMA VARCHAR(50)  ', @PROVE =@PROVEGS, @IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PROVES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PROVES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PROVES] @CIA INT, @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50)='SPA', @ORGCOMPRAS VARCHAR(4)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS AS VARCHAR(1000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @COD " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVES @COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU, @IDI=@IDI,@ORGCOMPRAS =@ORGCOMPRAS '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50) ,@ORGCOMPRAS VARCHAR(4)',@COD=@PROVEGS, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU,@IDI=@IDI,@ORGCOMPRAS =@ORGCOMPRAS" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PROVIS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PROVIS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_PROVIS] @CIA INT,@PAI VARCHAR(50),@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @USALIKE TINYINT =null, @IDIOMA VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "-- Descripci�n: Devuelve las provincias que cumplen las condiciones que se le pasan" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de entrada:@CIA: C�digo de la compa��a" & vbCrLf
    sConsulta = sConsulta & "--                       @PAI: C�digo del pa�s" & vbCrLf
    sConsulta = sConsulta & "--                       @COD: C�digo de la unidad o parte" & vbCrLf
    sConsulta = sConsulta & "--                       @DEN: Denominaci�n o parte de la ella" & vbCrLf
    sConsulta = sConsulta & "--                       @USALIKE: S hay que usar un like o no en la consulta" & vbCrLf
    sConsulta = sConsulta & "--                       @IDIOMA: Idioma en el que se quiere obtener las denominaciones" & vbCrLf
    sConsulta = sConsulta & "-- Par�metros de salida: las provincias que cumplen las condiciones" & vbCrLf
    sConsulta = sConsulta & "-- Tiempo m�ximo: 0 sg                         " & vbCrLf
    sConsulta = sConsulta & "-- Llamada desde: PMDatabaseServer --> Root.vb --> Provincias_Load" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVIS @PAI=@PAI,@COD=@COD,@DEN =@DEN,@USALIKE=@USALIKE,@IDIOMA=@IDIOMA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PAI VARCHAR(50),@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT, @IDIOMA VARCHAR(50)', @PAI=@PAI,@COD=@COD,@DEN =@DEN,@USALIKE=@USALIKE,@IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_REALIZAR_ACCION]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_REALIZAR_ACCION]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_REALIZAR_ACCION] @CIA INT,  @CIAPROVE INT, @INSTANCIA INT, @INSTANCIABLOQUE INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL ,@DEVOLUCION TINYINT=0, @ETAPA INT , @USUNOM VARCHAR(500)=NULL,@IDIOMAPROVE VARCHAR(3)=NULL  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA " & vbCrLf
    sConsulta = sConsulta & "FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @INSTANCIABLOQUE=@INSTANCIABLOQUE, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE, @DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE  '" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @INSTANCIABLOQUE INT,  @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT,@DEVOLUCION TINYINT,@ETAPA INT , @USUNOM VARCHAR(500),@IDIOMAPROVE VARCHAR(3)=NULL  ', @INSTANCIA=@INSTANCIA,  @INSTANCIABLOQUE=@INSTANCIABLOQUE,  @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE,@DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @DEVOLUCION = 1 " & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   --Por programa SET XACT_ABORT ON si falla FSPM_TRASPASAR_INSTANCIA aqui no llega" & vbCrLf
    sConsulta = sConsulta & "   UPDATE REL_CIAS SET SOLIC_ACT= SOLIC_ACT - 1,SOLIC_NUE= SOLIC_NUE - 1 WHERE CIA_COMP = @CIA AND CIA_PROVE = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
    sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
    sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
    sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
    sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
    sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
    sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
    sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
    sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
    sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
    sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
    sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
    sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
    sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
    sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
    sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ROL_PARTICIPANTES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ROL_PARTICIPANTES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ROL_PARTICIPANTES] @CIA INT,@BLOQUE INT,@ROL INT,@INSTANCIA INT, @IDI VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ROL_PARTICIPANTES @BLOQUE=@BLOQUE,@ROL=@ROL,@INSTANCIA=@INSTANCIA,@IDI=@IDI'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@BLOQUE INT, @ROL INT, @INSTANCIA INT, @IDI VARCHAR(50) ',@BLOQUE=@BLOQUE,@ROL=@ROL,@INSTANCIA=@INSTANCIA,@IDI=@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_ADJUN_CONTRATO_WIZARD] @CIA INT, @ID_ADJUN INT OUTPUT,@PER NVARCHAR(50) =NULL,@PROVE NVARCHAR(50),@NOM NVARCHAR(100), @ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_SAVE_ADJUN_CONTRATO_WIZARD @ID_ADJUN=@ID_ADJUN OUTPUT,@PER=@PER,@PROVE=@PROVE, @NOM=@NOM,@ID_CONTRATO=@ID_CONTRATO,@COMENT=@COMENT '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID_ADJUN INT OUTPUT, @PER NVARCHAR(50),@PROVE NVARCHAR(50),@NOM NVARCHAR(100),@ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000) ', @ID_ADJUN=@ID_ADJUN OUTPUT,@PER=@PER,@PROVE=@PROVE, @NOM=@NOM,@ID_CONTRATO=@ID_CONTRATO,@COMENT=@COMENT" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_INSTANCE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_INSTANCE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SAVE_INSTANCE] @CIA INT, @INSTANCIA INT, @PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @ENVIAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_SAVE_INSTANCE @INSTANCIA=@INSTANCIA, @PETICIONARIO =@PETICIONARIO , @COMPRADOR =@COMPRADOR , @MON =@MON , @ENVIAR =@ENVIAR , @IMPORTE =@IMPORTE , @PEDIDO_AUT =@PEDIDO_AUT '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @ENVIAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0 ', @INSTANCIA=@INSTANCIA, @PETICIONARIO =@PETICIONARIO , @COMPRADOR =@COMPRADOR , @MON =@MON , @ENVIAR =@ENVIAR , @IMPORTE =@IMPORTE , @PEDIDO_AUT =@PEDIDO_AUT" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SETREQUESTADJUNDATA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SETREQUESTADJUNDATA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SETREQUESTADJUNDATA] @CIA INT,  @ID INT, @INIT INT, @OFFSET INT, @DATA IMAGE AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_SETREQUESTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @ID INT, @INIT INT, @OFFSET INT, @DATA IMAGE ',@ID = @ID, @INIT=@INIT, @OFFSET=@OFFSET, @DATA=@DATA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SOLICITUD_DATOS_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_EMAIL]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_EMAIL] @CIA INT, @ID INT,@TYPE INT,@CONTACTO INT=NULL,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_SOLICITUD_DATOS_EMAIL @ID=@ID, @TYPE=@TYPE, @CONTACTO=@CONTACTO, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT,@TYPE INT,@CONTACTO INT=NULL,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0 ',@ID=@ID, @TYPE=@TYPE, @CONTACTO=@CONTACTO, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SOLICITUD_DATOS_MENSAJE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_MENSAJE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_MENSAJE] @CIA INT,@CIA_PROVE INT, @ID INT,@TYPE INT,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0,@COD_USU VARCHAR(50), @IDI VARCHAR(20)='SPA',@FORMATOFECHA VARCHAR(20)  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL =  N'EXEC ' + @FSGS + 'FSPM_SOLICITUD_DATOS_MENSAJE @ID=@ID, @TYPE=@TYPE, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE, @IDI=@IDI,@FORMATOFECHA=@FORMATOFECHA  '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT,@TYPE INT,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0, @IDI VARCHAR(20),@FORMATOFECHA VARCHAR(20)',@ID=@ID, @TYPE=@TYPE, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE, @IDI=@IDI,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Obtener los datos del Usuario de la Compa�ia" & vbCrLf
    sConsulta = sConsulta & "SELECT U.COD COD_USU,U.NOM NOM_USU,U.APE APE_USU,U.TFNO TFNO1_USU,U.EMAIL EMAIL_USU,U.FAX FAX_USU, C.COD COD_COM,C.DEN DEN_COM,C.NIF NIF_COM" & vbCrLf
    sConsulta = sConsulta & "FROM USU U WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN CIAS C WITH (NOLOCK) ON U.CIA = C.ID WHERE C.ID =@CIA_PROVE AND U.COD = @COD_USU" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UNIDADES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UNIDADES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UNIDADES] @CIA INT,@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT = 1, @IDIOMA VARCHAR(20) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UNIDADES @COD =@COD , @DEN =@DEN ,@USALIKE  =@USALIKE , @IDIOMA=@IDIOMA '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT, @IDIOMA VARCHAR(20)  ', @COD =@COD , @DEN =@DEN , @USALIKE  =@USALIKE, @IDIOMA=@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UONS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UONS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UONS] @CIA INT,@TIPO INT, @PER VARCHAR(50) = NULL, @IDI VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UONS @TIPO=@TIPO, @PER = @PER , @IDI =@IDI '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT,@PER VARCHAR(50), @IDI VARCHAR(50) ', @TIPO=@TIPO, @PER = @PER , @IDI =@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UONSPRES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UONSPRES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UONSPRES] @CIA INT,@TIPO INT, @ANYO INT, @IDI VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_UONSPRES @TIPO=@TIPO, @ANYO = @ANYO , @IDI =@IDI '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @ANYO INT, @IDI VARCHAR(50) ', @TIPO=@TIPO, @ANYO = @ANYO , @IDI =@IDI" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UPLOAD_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UPLOAD_ADJUN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UPLOAD_ADJUN] @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @CIA INT =NULL, @USU INT =NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "    INSERT INTO COPIA_ADJUN (DATA, NOM, COMENT, FECALTA,CIA,USU) VALUES (NULL, @NOM, @COMENT, GETDATE(),@CIA, @USU)" & vbCrLf
    sConsulta = sConsulta & "    SET @ID = (SELECT MAX(ID) FROM COPIA_ADJUN)" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "    UPDATE COPIA_ADJUN SET NOM = @NOM , COMENT = @COMENT WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SET nocount on " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN WITH (NOLOCK) WHERE ID=@ID " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
    sConsulta = sConsulta & "    UPDATE COPIA_ADJUN SET DATA = @DATA WHERE ID =@ID " & vbCrLf
    sConsulta = sConsulta & "ELSE " & vbCrLf
    sConsulta = sConsulta & "    UPDATETEXT COPIA_ADJUN.DATA @textpointer @init 0 @data" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UPLOAD_ADJUN_GS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UPLOAD_ADJUN_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_UPLOAD_ADJUN_GS] @CIA INT, @CIAPROVE INT,  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(20)=NULL, @USUNOM VARCHAR(500)=NULL AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SOBRA TINYINT" & vbCrLf
    sConsulta = sConsulta & "SET @SOBRA=1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UPLOAD_ADJUN @INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @PROVE=@PROVE, @SOBRA=@SOBRA, @USUNOM=@USUNOM '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL, @PROVE VARCHAR(50)=NULL, @SOBRA TINYINT, @USUNOM VARCHAR(500)=NULL ',@INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @PROVE = @PROVEGS, @SOBRA=@SOBRA, @USUNOM=@USUNOM" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_VER_DETALLE_PERSONA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_VER_DETALLE_PERSONA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_VER_DETALLE_PERSONA] @PER  VARCHAR(50), @INSTANCIA INT, @CIA INT, @PROVE INT = 1 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_VER_DETALLE_PERSONA @PER=@PER, @INSTANCIA = @INSTANCIA , @PROVE =@PROVE '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PER VARCHAR(50), @INSTANCIA INT, @PROVE INT ',@PER = @PER , @INSTANCIA=@INSTANCIA, @PROVE=@PROVE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_DEVOLVER_DETALLE_PARTIDA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_DEVOLVER_DETALLE_PARTIDA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "--Revisado por: dsl(23/12/2011)" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSSM_DEVOLVER_DETALLE_PARTIDA] @CIA INT, @IDI VARCHAR(20),@PRES0 VARCHAR(10),@PRES1 VARCHAR(10), @PRES2 VARCHAR(10)=NULL, @PRES3 VARCHAR(10)=NULL, @PRES4 VARCHAR(10)=NULL, @UON1 VARCHAR(10),@UON2 VARCHAR(10)=NULL,@UON3 VARCHAR(10)=NULL,@UON4 VARCHAR(10)=NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSSM_DEVOLVER_DETALLE_PARTIDA @IDI=@IDI,@PRES0=@PRES0,@PRES1=@PRES1,@PRES2=@PRES2,@PRES3=@PRES3,@PRES4=@PRES4,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(20),@PRES0 VARCHAR(10),@PRES1 VARCHAR(10), @PRES2 VARCHAR(10)=NULL, @PRES3 VARCHAR(10)=NULL, @PRES4 VARCHAR(10)=NULL, @UON1 VARCHAR(10),@UON2 VARCHAR(10)=NULL,@UON3 VARCHAR(10)=NULL,@UON4 VARCHAR(10)=NULL',@IDI=@IDI,@PRES0=@PRES0,@PRES1=@PRES1,@PRES2=@PRES2,@PRES3=@PRES3,@PRES4=@PRES4,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_GETADJUNTO_CONTRATO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO] @CIA INT, @ID INT =NULL, @ID_CONTRATO INT =NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTO_CONTRATO @ID=@ID,@ID_CONTRATO=@ID_CONTRATO'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID  INT, @ID_CONTRATO INT ', @ID=@ID,@ID_CONTRATO=@ID_CONTRATO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_FORMATO_CONTACTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_FORMATO_CONTACTO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
        
    
End Sub


Public Function CodigoDeActualizacion31900_03_15_03A31900_04_15_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_03A31900_04_15_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_03A31900_04_15_04 = False
End Function

Private Sub V_31900_5_Storeds_004()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_SOLPENDIENTES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_SOLPENDIENTES]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSQA_SOLPENDIENTES] @CIACOMP FLOAT, @CIAPROVE FLOAT, @USU  VARCHAR(20), @IDI VARCHAR(20) AS" & vbCrLf
    sConsulta = sConsulta & "SELECT GETDATE() AS DIA, USU.NOM + ' ' + USU.APE AS NOMBRE, CIAS.COD AS PROVEENCIA" & vbCrLf
    sConsulta = sConsulta & ",REL_CIAS.CIA_PROVE,REL_CIAS.CIA_COMP,REL_CIAS.EST,REL_CIAS.COD_PROVE_CIA,REL_CIAS.PROCE_ACT,REL_CIAS.PROCE_NUE" & vbCrLf
    sConsulta = sConsulta & ",REL_CIAS.PROCE_REV,REL_CIAS.PREM_ACTIVO,REL_CIAS.ORDEN_ACT,REL_CIAS.ORDEN_NUE,REL_CIAS.SOLIC_ACT,REL_CIAS.SOLIC_NUE" & vbCrLf
    sConsulta = sConsulta & ",REL_CIAS.CERT_ACT,REL_CIAS.CERT_NUE,REL_CIAS.NOCONF_ACT,REL_CIAS.NOCONF_NUE " & vbCrLf
    sConsulta = sConsulta & "FROM REL_CIAS WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN USU WITH(NOLOCK) ON USU.CIA = REL_CIAS.CIA_PROVE AND COD=@USU" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=@CIAPROVE AND CIAS.ID=CIA_PROVE" & vbCrLf
    sConsulta = sConsulta & "WHERE CIA_COMP = @CIACOMP AND CIA_PROVE = @CIAPROVE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_15_04A31900_04_15_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_005
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_04A31900_04_15_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_04A31900_04_15_05 = False
End Function

Sub V_31900_5_Storeds_005()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTINVISIBLEFIELDS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTINVISIBLEFIELDS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTINVISIBLEFIELDS] @CIA INT,  @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL,@NUEVO_WORKFL TINYINT=0    AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL ',  @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVEGS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_15_05A31900_04_15_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Tablas_006

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_006_001
    V_31900_5_Storeds_006_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_05A31900_04_15_06 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_05A31900_04_15_06 = False
End Function


Sub V_31900_5_Storeds_006_001()
Dim sConsulta As String
    
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MON_COD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[MON_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[MON_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM MON WITH(NOLOCK) WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE MON SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS_MON SET MON_PORTAL=@NEW WHERE MON_PORTAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VOLADJ SET MONPOR=@NEW WHERE MONPOR=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACT_VISIBLE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACT_VISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ACT_VISIBLE] @CIA int,@IDI varchar(20),@PYME varchar(3) = ''" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #mraiz contendr� las actividades pendientes de confirmar por el gerente del portal y en caso" & vbCrLf
sConsulta = sConsulta & "--de que no haya nada pendiente, contendr� una copia de #raiz" & vbCrLf
sConsulta = sConsulta & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4, act5 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
sConsulta = sConsulta & "  from cias_act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3,ACT4" & vbCrLf
sConsulta = sConsulta & "                     from cias_act4 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                     from cias_act3 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 e WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
sConsulta = sConsulta & "  from cias_act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                     from cias_act3 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
sConsulta = sConsulta & "  from cias_act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from cias_act2 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2 " & vbCrLf
sConsulta = sConsulta & "  from cias_act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from cias_act1 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1) " & vbCrLf
sConsulta = sConsulta & "select act1 " & vbCrLf
sConsulta = sConsulta & "  from cias_act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Insertamos los datos correspondientes a #mraiz" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4, act5 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3,ACT4" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act4 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA " & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act3 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 e WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act3 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
sConsulta = sConsulta & "  from mcias_act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act2 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2 ) " & vbCrLf
sConsulta = sConsulta & "select act1, act2 " & vbCrLf
sConsulta = sConsulta & "  from mcias_act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "   and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                     from mcias_act1 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1) " & vbCrLf
sConsulta = sConsulta & "select act1 " & vbCrLf
sConsulta = sConsulta & "  from mcias_act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si no hab�a actividades pendientes de confirmar, se copia #raiz en #mraiz" & vbCrLf
sConsulta = sConsulta & "if (select count(*) from #mraiz WITH(NOLOCK))=0" & vbCrLf
sConsulta = sConsulta & "  insert into #mraiz select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--La tabla #base va a contener las actividades de las cuales se obtendr�n la " & vbCrLf
sConsulta = sConsulta & "--estructura completa (con asignadas, pendientes y hermanas de estas)" & vbCrLf
sConsulta = sConsulta & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,act3,act4,min(act5) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK) where act5 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK) where act5 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3,act4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,act3,min(act4) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK) where act4 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK) where act4 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2,act3)" & vbCrLf
sConsulta = sConsulta & "  select act1,act2,min(act3) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK) where act3 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK) where act3 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1,act2)" & vbCrLf
sConsulta = sConsulta & "  select act1,min(act2) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK) where act2 is not null" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK) where act2 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  insert into #base (act1)" & vbCrLf
sConsulta = sConsulta & "  select min(act1) " & vbCrLf
sConsulta = sConsulta & "    from (select act1, act2, act3, act4, act5 from #raiz WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           Union" & vbCrLf
sConsulta = sConsulta & "          select act1, act2, act3, act4, act5 from #mraiz WITH(NOLOCK)) a" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--La tabla #Estructura contendr� toda la estructura completa de actividades asignadas, pendientes y hermanas" & vbCrLf
sConsulta = sConsulta & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.act4,a.id" & vbCrLf
sConsulta = sConsulta & "  from act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     inner join #base b WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
sConsulta = sConsulta & "            and a.act3 = b.act3 " & vbCrLf
sConsulta = sConsulta & "            and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & " Where b.act5 Is Not Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.id" & vbCrLf
sConsulta = sConsulta & "  from act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     inner join #base b WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
sConsulta = sConsulta & "            and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & " Where b.act5 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act4 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
sConsulta = sConsulta & "                      and a.act3=c.act3 " & vbCrLf
sConsulta = sConsulta & "                      and a.id=c.act4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.id" & vbCrLf
sConsulta = sConsulta & "  from act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     inner join #base b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
sConsulta = sConsulta & "            and a.act2=b.act2" & vbCrLf
sConsulta = sConsulta & " Where b.act4 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act3 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
sConsulta = sConsulta & "                      and a.id=c.act3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.id" & vbCrLf
sConsulta = sConsulta & "  from act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     inner join #base b WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "             on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & " Where b.act3 Is Null " & vbCrLf
sConsulta = sConsulta & "   And b.act2 Is Not Null" & vbCrLf
sConsulta = sConsulta & "   and not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                     FROM #Estructura c WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
sConsulta = sConsulta & "                      and a.id = c.act2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if (@PYME='')" & vbCrLf
sConsulta = sConsulta & "  insert into #Estructura (Act1)" & vbCrLf
sConsulta = sConsulta & "  select a.id" & vbCrLf
sConsulta = sConsulta & "    from act1 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                       FROM #Estructura c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                      where a.id=c.act1)" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  insert into #Estructura (Act1)" & vbCrLf
sConsulta = sConsulta & "  select a.id" & vbCrLf
sConsulta = sConsulta & "    from act1 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where not exists (SELECT Act1,Act2,Act3,Act4 " & vbCrLf
sConsulta = sConsulta & "                       FROM #Estructura c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                      where a.id=c.act1)" & vbCrLf
sConsulta = sConsulta & "  and a.PYME = @PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizamos los campos actAsign y actPend con el valor correspondiente. Estos campos" & vbCrLf
sConsulta = sConsulta & "--contendr�n el nivel al que la actividad est� asignada (actAsign) o en el caso de que est� pendiente (actPend), el nivel" & vbCrLf
sConsulta = sConsulta & "-- al que lo est�" & vbCrLf
sConsulta = sConsulta & "update #Estructura set actAsign=0 ,actPend=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #raiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a WITH(NOLOCK) inner join #mraiz b WITH(NOLOCK) on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "select #Estructura.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
sConsulta = sConsulta & "       #Estructura.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
sConsulta = sConsulta & "       actasign, actpend" & vbCrLf
sConsulta = sConsulta & "from #Estructura WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     left join act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "     left join act2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act2.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "     left join act3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act3.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act3.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "     left join act4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act4.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act4.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act4.act3 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "     left join act5 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #Estructura.act1=act5.act1 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act2 = act5.act2 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act3 = act5.act3 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act4 = act5.act4 " & vbCrLf
sConsulta = sConsulta & "           and #Estructura.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIA_ESP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIA_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIA_ESP] @IDCIA int, @NOM varchar(300), @COM varchar(500) , @IDESP int output AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDESP = 1" & vbCrLf
sConsulta = sConsulta & "select @IDESP = ISNULL(ID,0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA, ID, NOM, COM) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA, @IDESP, @NOM, @COM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIAS_ACT1]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIAS_ACT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIAS_ACT1] (@CIACOD varchar(50), @GMN1 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT1 (CIA,ACT1) VALUES (@ID_CIA ,@ID_ACT1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIAS_ACT2]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIAS_ACT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIAS_ACT2] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT2 (CIA,ACT1,ACT2) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIAS_ACT3]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIAS_ACT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIAS_ACT3] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT3 (CIA,ACT1,ACT2,ACT3) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 , @ID_ACT3 )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIAS_ACT4]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIAS_ACT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ANYA_CIAS_ACT4] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50),@GMN4 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT4 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT4=(SELECT ID FROM ACT4 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND COD=@GMN4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT4 (CIA,ACT1,ACT2,ACT3,ACT4) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 , @ID_ACT3 , @ID_ACT4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_AUTORIZAR_PREMIUM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_AUTORIZAR_PREMIUM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_AUTORIZAR_PREMIUM] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = @LNKSERVER + 'SP_ACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET PREMIUM =2  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PREM_ACTIVO =1 WHERE CIA_PROVE = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_CAMBIO_NIVEL_ACT_CIAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_CAMBIO_NIVEL_ACT_CIAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_CAMBIO_NIVEL_ACT_CIAS] (@NIVEL INTEGER, @RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL_SUP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL_INF AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERROR AS INTEGER" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL = 2 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_INF =  COUNT( DISTINCT (ACT1))  FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   SELECT  ACT2.ACT1,ACT2.ID FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT3 WITH(NOLOCK) ON ACT3.ACT1 = ACT2.ACT1 AND ACT3.ACT2 = ACT2.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT2.ACT1,ACT2.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL=4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   SELECT ACT3.ACT1,ACT3.ACT2,ACT3.ID FROM ACT3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT4 WITH(NOLOCK) ON ACT4.ACT1 = ACT3.ACT1 AND ACT4.ACT2 = ACT3.ACT2 AND ACT4.ACT3=ACT3.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT3.ACT2,ACT3.ACT1,ACT3.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL=5" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   SELECT ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID FROM ACT4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT5 WITH(NOLOCK) ON ACT5.ACT1 = ACT4.ACT1 AND ACT5.ACT2 = ACT4.ACT2 AND ACT5.ACT3=ACT4.ACT3 AND ACT5.ACT4=ACT4.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT4.ACT3,ACT4.ACT2,ACT4.ACT1,ACT4.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL_INF < @NIVEL_SUP " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ERROR = 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "IF @ERROR = 0 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 2 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                DELETE FROM CIAS_ACT3 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT3" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 5" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                DELETE FROM CIAS_ACT3 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT4 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT4 " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET @RES=@ERROR" & vbCrLf
sConsulta = sConsulta & "RETURN " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_COMPROBAR_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_COMPROBAR_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_COMPROBAR_EMAIL](@FSP_CIA VARCHAR(20),@EMAIL VARCHAR(100),@EXISTE INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE   @CIA INTEGER" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CIA = (SELECT ID FROM CIAS WHERE COD=@FSP_CIA)" & vbCrLf
sConsulta = sConsulta & "SET @EXISTE = (SELECT COUNT(*)  FROM USU WITH(NOLOCK) INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA = USU_PORT.CIA AND USU.ID = USU_PORT.USU  WHERE USU.CIA=@CIA AND USU.EMAIL=@EMAIL)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEL_VOLADJ]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEL_VOLADJ]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEL_VOLADJ](@COMP INT,@PROVE VARCHAR(50), @ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ID = (SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@PROVE)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@COMP AND CIA_PROVE=@ID AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DESAUTORIZAR_PREMIUM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DESAUTORIZAR_PREMIUM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DESAUTORIZAR_PREMIUM] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = @LNKSERVER + 'SP_DESACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET PREMIUM = 3  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PREM_ACTIVO = 0 WHERE CIA_PROVE = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_ACTIVIDADES_CIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_ACTIVIDADES_CIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_ACTIVIDADES_CIA] @CIA int,@IDI varchar(20),@TODO bit = 0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int , ActAsign smallint)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @TODO = 0" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    --Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, act5, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, act5, 5" & vbCrLf
sConsulta = sConsulta & "      from cias_act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2,ACT3,ACT4" & vbCrLf
sConsulta = sConsulta & "                         from cias_act4 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                          and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                         from cias_act3 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 e WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, 4  " & vbCrLf
sConsulta = sConsulta & "      from cias_act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2,ACT3" & vbCrLf
sConsulta = sConsulta & "                         from cias_act3 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 d WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, 3" & vbCrLf
sConsulta = sConsulta & "      from cias_act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1,ACT2" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 c WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2 , 2" & vbCrLf
sConsulta = sConsulta & "      from cias_act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select CIA,ACT1" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 b WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, 1 " & vbCrLf
sConsulta = sConsulta & "      from cias_act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    --Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, act5, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, act5, 5" & vbCrLf
sConsulta = sConsulta & "      from cias_act5 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, 4  " & vbCrLf
sConsulta = sConsulta & "      from cias_act4 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, 3" & vbCrLf
sConsulta = sConsulta & "      from cias_act3 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2 , 2" & vbCrLf
sConsulta = sConsulta & "      from cias_act2 a WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, 1 " & vbCrLf
sConsulta = sConsulta & "      from cias_act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "select #raiz.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
sConsulta = sConsulta & "       #raiz.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
sConsulta = sConsulta & "       actasign" & vbCrLf
sConsulta = sConsulta & "from #raiz WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     left join act1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "     left join act2 WITH(NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act2.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "     left join act3 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act3.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act3.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "     left join act4 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act4.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act4.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act4.act3 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "     left join act5 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act5.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act5.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act5.act3 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act4 = act5.act4 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "order by #raiz.act1, #raiz.act2,#raiz.act3,#raiz.act4,#raiz.act5" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIBESP_PROCESO_WEB  @CIA_COMP INTEGER, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_PROCESO @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCESOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROCESOS_PROVE] @CIACOMP INT, @PROVE VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,CIA_COMP,ANYO,GMN1,PROCE,PROVE,CIA_PROVE,USU,FECREC,FECVAL,MON,CAMBIO,OBS,OBSADJUN,NUMOBJ,ERROR,ADJUN,ENVIANDOSE,IMPORTE,IMPORTE_BRUTO " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP = @CIACOMP" & vbCrLf
sConsulta = sConsulta & "   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS] @PROVE int  AS" & vbCrLf
sConsulta = sConsulta & "SELECT A.ID IDCIA, A.COD COD, A.DEN DEN," & vbCrLf
sConsulta = sConsulta & "CASE WHEN B.CIA_COMP IS NULL THEN 0 ELSE 1 END REGISTRADA," & vbCrLf
sConsulta = sConsulta & "CASE WHEN C.CIA_COMP IS NULL THEN 0 ELSE 1 END SOLICITADA" & vbCrLf
sConsulta = sConsulta & "FROM CIAS A WITH(NOLOCK) LEFT JOIN REL_COMP B WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON A.ID = B.CIA_COMP" & vbCrLf
sConsulta = sConsulta & "AND @PROVE = B.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MREL_COMP C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON A.ID = C.CIA_COMP" & vbCrLf
sConsulta = sConsulta & "AND @PROVE = C.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "Where A.FCEST = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL] @CIA INT,@ID_REG INT, @ADJUN INT,@INIT INT,@SIZE INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM REGISTRO_EMAIL_ADJUN WITH(NOLOCK) WHERE CIA=@CIA AND ID_REG=@ID_REG AND ID=@ADJUN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  REGISTRO_EMAIL_ADJUN.DATA @textpointer @INIT  @LSIZE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_CIAS_ACT1]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_CIAS_ACT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_CIAS_ACT1] (@CIACOD varchar(50), @GMN1 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_CIAS_ACT2]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_CIAS_ACT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_CIAS_ACT2] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL " & vbCrLf
sConsulta = sConsulta & " DELETE FROM CIAS_ACT2 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM CIAS_ACT2 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_CIAS_ACT3]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_CIAS_ACT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_CIAS_ACT3] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin    " & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "      IF @GMN3 IS NULL" & vbCrLf
sConsulta = sConsulta & "         begin" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2" & vbCrLf
sConsulta = sConsulta & "         end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         begin" & vbCrLf
sConsulta = sConsulta & "   SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT3 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3" & vbCrLf
sConsulta = sConsulta & "         end" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_CIAS_ACT4]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_CIAS_ACT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIM_CIAS_ACT4] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50),@GMN4 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT4 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NULL" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      IF @GMN3 IS NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "        IF @GMN4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 " & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "            SET @ID_ACT4=(SELECT ID FROM ACT4 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND COD=@GMN4)" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM CIAS_ACT4 WHERE CIA=@ID_CIA AND ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND ACT4=@ID_ACT4" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_PAIS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_PAIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  [dbo].[SP_ELIM_PAIS]  (@PAI_COD NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PAI_DEN WHERE PAI = (SELECT ID FROM PAI WITH(NOLOCK) WHERE COD=@PAI_COD)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PAI WHERE COD=@PAI_COD" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_PROVI]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_PROVI]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  [dbo].[SP_ELIM_PROVI]  (@PAI_COD NVARCHAR(50),@PROVI_COD NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PAIS AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PROVI AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ID_PAIS=(SELECT ID FROM PAI WITH(NOLOCK)  WHERE COD=@PAI_COD)" & vbCrLf
sConsulta = sConsulta & "   SET @ID_PROVI=(SELECT ID FROM PROVI WITH(NOLOCK)  WHERE PAI=@ID_PAIS AND COD=@PROVI_COD)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROVI_DEN WHERE PAI=@ID_PAIS AND PROVI=@ID_PROVI" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROVI WHERE PAI=@ID_PAIS AND COD=@PROVI_COD " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIM_RELCIAS_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIM_RELCIAS_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIM_RELCIAS_GS] (@CIA_COMP INT, @CIA_PROVE VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE  AS INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @IDPROVE= (SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIA_PROVE)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM REL_CIAS WHERE CIA_COMP=@CIA_COMP AND CIA_PROVE=@IDPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "return" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_COMPANIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIMINAR_COMPANIA] @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WITH(NOLOCK) WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin WITH(NOLOCK) porque se leen tablas de GS" & vbCrLf
sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL_ADJUN WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "--- 2.9.5" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "------------" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_PROCE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_ELIMINAR_PROCE] @CIA_COMP SMALLINT, @ANYO SMALLINT, @GMN1 VARCHAR(50), @PROCE SMALLINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curELIMPROCE CURSOR LOCAL FOR SELECT ID FROM  PROCE_OFE WITH(NOLOCK) WHERE CIA_COMP=@CIA_COMP AND ANYO=@ANYO AND  GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curELIMPROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curELIMPROCE INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ITEM_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ITEM_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_GR_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_GR_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM PROCE_OFE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM ITEM_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM GRUPO_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM PROCE_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curELIMPROCE INTO @ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curELIMPROCE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curELIMPROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_REGISTRAR_COMPANIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_REGISTRAR_COMPANIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_REGISTRAR_COMPANIA]  @IDPORTAL int, @COD varchar(50) , @DEN varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @DIR varchar(255), @CP varchar(20), @POB varchar(100), @PAI INT, " & vbCrLf
sConsulta = sConsulta & "                 @PROVI INT, @MON INT, @FCEST tinyint, @FPEST tinyint, @NIF varchar(20)," & vbCrLf
sConsulta = sConsulta & "                 @VOLFACT float, @CLIREF1 varchar(50),@CLIREF2 varchar(50),@CLIREF3 varchar(50),@CLIREF4 varchar(50)," & vbCrLf
sConsulta = sConsulta & "                 @HOM1 varchar(50), @HOM2 varchar(50), @HOM3 varchar(50), @URLCIA varchar(255)," & vbCrLf
sConsulta = sConsulta & "                 @COMENT text, @ORIGINS varchar(50), @PREMIUM int, @TIPOACC tinyint ,@IDI varchar(50), @IDCIA int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIDI int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDCIA=ISNULL(MAX(ID),0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM CIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
sConsulta = sConsulta & "  FROM IDI WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE COD = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS (ID,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,IDI,PORT,FCEST,NIF,PREMIUM,VOLFACT, " & vbCrLf
sConsulta = sConsulta & "                  CLIREF1,CLIREF2,CLIREF3,CLIREF4,HOM1,HOM2,HOM3,URLCIA,COMENT,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA,@COD,@DEN,@DIR,@CP,@POB,@PAI,@PROVI,@MON,@IDIDI,@IDPORTAL,@FCEST,@NIF,@PREMIUM,@VOLFACT ," & vbCrLf
sConsulta = sConsulta & "                  @CLIREF1, @CLIREF2, @CLIREF3, @CLIREF4, @HOM1, @HOM2, @HOM3, @URLCIA, @COMENT, @ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_PORT (CIA,PORT,FPEST,TIPOACC,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA,@IDPORTAL,@FPEST,@TIPOACC, @ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO rel_comp (cia_prove, cia_comp) " & vbCrLf
sConsulta = sConsulta & "SELECT @IDCIA, ID" & vbCrLf
sConsulta = sConsulta & "  FROM CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE FCEST=3" & vbCrLf
sConsulta = sConsulta & "   AND PORT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_REGISTRAR_USUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_REGISTRAR_USUARIO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_REGISTRAR_USUARIO] @CIA int, @IDPORTAL int, @COD varchar(50) , @PWD varchar(128), @FECHA datetime, " & vbCrLf
sConsulta = sConsulta & "                 @APE varchar(100), @NOM varchar(20), @DEP varchar(100), @CAR varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @TFNO varchar(20), @TFNO2 varchar(20), @FAX varchar(20), @EMAIL varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @IDI varchar(20), @TFNO_MOVIL varchar(20), @TIPOEMAIL smallint, @FCEST tinyint, @FPEST tinyint, @ORIGINS varchar(50),  " & vbCrLf
sConsulta = sConsulta & "                 @PRINCIPAL bit AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU int" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIDI int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU=ISNULL(MAX(ID),0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM USU" & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
sConsulta = sConsulta & "  FROM IDI WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE COD = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU (CIA,ID,COD,PWD,FECPWD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
sConsulta = sConsulta & "                 FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL) " & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA, @IDUSU, @COD, @PWD, @FECHA, @APE, @NOM, @DEP, @CAR, @TFNO, @TFNO2," & vbCrLf
sConsulta = sConsulta & "        @FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU_PORT (CIA,USU,PORT,FPEST,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA,@IDUSU,@IDPORTAL,@FPEST,@ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRINCIPAL = 1 " & vbCrLf
sConsulta = sConsulta & "  UPDATE CIAS SET USUPPAL=@IDUSU WHERE ID=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TRANSFER_CIA_ESP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_TRANSFER_CIA_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_TRANSFER_CIA_ESP] @IDCIA INTEGER, @ESP SMALLINT, @INIT INT, @SIZE INT, @DATA IMAGE AS" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @IDCIA AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
sConsulta = sConsulta & "    UPDATE CIAS_ESP SET DATA = @DATA WHERE CIA = @IDCIA AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    UPDATETEXT CIAS_ESP.DATA @textpointer @init 0 @data" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Sub V_31900_5_Storeds_006_002()
Dim sConsulta As String
    
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_ITEM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_ITEM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_ITEM] (@ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @GRUPO VARCHAR(50), @ITEM INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID=ID FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DELETE OFE_ITEM_ADJUN WHERE ID=@ID AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE OFE_ITEM_ATRIB WHERE ID=@ID AND GRUPO=@GRUPO AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "DELETE ITEM_OFE WHERE ID=@ID AND GRUPO=@GRUPO AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_ACT_REL_CIAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_ACT_REL_CIAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSEP_ACT_REL_CIAS](@COMP VARCHAR(50),@PROVE VARCHAR(50),@ACTIVAS INTEGER,@NUEVAS INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP AS INT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WITH (NOLOCK) WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET ORDEN_ACT=@ACTIVAS,ORDEN_NUE=@NUEVAS WHERE CIA_COMP=@ID_COMP AND COD_PROVE_CIA=@PROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_NUEVO_ADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_NUEVO_ADJUNTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_NUEVO_ADJUNTO] @CIA_COMP int, @DATA IMAGE, @MAXID int OUTPUT, @DATASIZE int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ADJUN (CIA_COMP, DATA) VALUES (@CIA_COMP, @DATA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @MAXID = MAX(ID) FROM ADJUN" & vbCrLf
sConsulta = sConsulta & "SELECT @DATASIZE=DATALENGTH(DATA) FROM ADJUN WITH (NOLOCK) WHERE ID = @MAXID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DOWNLOAD_ADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DOWNLOAD_ADJUNTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DOWNLOAD_ADJUNTO] @INIT INT, @OFFSET INT, @ID INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM ADJUN WITH (NOLOCK) WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET >= @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DOWNLOAD_ESPECIFICACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DOWNLOAD_ESPECIFICACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DOWNLOAD_ESPECIFICACION] @INIT INT, @OFFSET INT, @ID INT,@CIA INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM CIAS_ESP WITH (NOLOCK) WHERE CIA=@CIA AND ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET >= @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT CIAS_ESP.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_NUEVA_ESP_CIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_NUEVA_ESP_CIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_NUEVA_ESP_CIA] " & vbCrLf
sConsulta = sConsulta & "   @CIA int, " & vbCrLf
sConsulta = sConsulta & "   @DATA IMAGE," & vbCrLf
sConsulta = sConsulta & "   @NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "   @COM VARCHAR(600), " & vbCrLf
sConsulta = sConsulta & "   @MAXID int OUTPUT, " & vbCrLf
sConsulta = sConsulta & "   @DATASIZE int OUTPUT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MAXID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD: Sin WITH (NOLOCK) porque es para obtener un nuevo ID" & vbCrLf
sConsulta = sConsulta & "select @MAXID = ISNULL(ID,0) + 1 " & vbCrLf
sConsulta = sConsulta & "FROM CIAS_ESP " & vbCrLf
sConsulta = sConsulta & "WHERE CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA,ID,NOM, COM,DATA) VALUES (@CIA,@MAXID,@NOM, @COM,@DATA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WHERE ID = @MAXID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE  PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_GRUPO_WEB] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER, " & vbCrLf
sConsulta = sConsulta & "   @ANYO smallint, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 varchar(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE smallint  , " & vbCrLf
sConsulta = sConsulta & "   @GRUPO varchar(50) " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_GRUPO @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint  , @GRUPO varchar(50)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PUJAS_ITEM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PUJAS_ITEM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PUJAS_ITEM] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER," & vbCrLf
sConsulta = sConsulta & "   @ANYO INTEGER," & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @PROCE INTEGER ," & vbCrLf
sConsulta = sConsulta & "   @ITEM INTEGER," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50)  " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PUJAS_ITEM_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@ITEM=@ITEM,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER,@PROVE VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @ITEM = @ITEM,@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROCE_ATRIBESP_ITEM_WEB] " & vbCrLf
sConsulta = sConsulta & "   @CIA_COMP INTEGER, " & vbCrLf
sConsulta = sConsulta & "   @ANYO smallint, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 varchar(50), " & vbCrLf
sConsulta = sConsulta & "   @PROCE smallint , " & vbCrLf
sConsulta = sConsulta & "   @ITEM smallint " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCE_ATRIBESP_ITEM @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @ITEM = @ITEM'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO smallint, @GMN1 varchar(50), @PROCE smallint  , @ITEM smallint'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ITEMS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ITEMS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  [dbo].[SP_ITEMS]  @ID INT,@GRUPO VARCHAR(50)=null AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            WHERE ID=@ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ITO.ITEM, PRECIO, CANTMAX, PRECIO2, PRECIO3,IA.NUMADJUN, COMENT1, COMENT2, COMENT3, cast(OBSADJUN as varchar(2000)) OBSADJUN, @CAM CAMBIO,ITO.PREC_VALIDO,ITO.IMPORTE" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE ITO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (SELECT IA.ITEM, COUNT(IA.ADJUN) NUMADJUN" & vbCrLf
sConsulta = sConsulta & "                 FROM OFE_ITEM_ADJUN IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                WHERE IA.ID =@ID" & vbCrLf
sConsulta = sConsulta & "             GROUP BY IA.ID, IA.ITEM) IA" & vbCrLf
sConsulta = sConsulta & "      ON ITO.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "WHERE ITO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "  AND ITO.GRUPO=case when @GRUPO is null then ITO.GRUPO else @GRUPO END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_ADJUNTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_ADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_ADJUNTOS] " & vbCrLf
sConsulta = sConsulta & "   @ID int, " & vbCrLf
sConsulta = sConsulta & "   @GRUPO varchar(50) = null, " & vbCrLf
sConsulta = sConsulta & "   @ITEM int = null, " & vbCrLf
sConsulta = sConsulta & "   @AMBITO tinyint, " & vbCrLf
sConsulta = sConsulta & "   @SOLOPORTAL int = 0 OUTPUT " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 0" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT 1 AMBITO, POA.ID,POA.ADJUN, NULL GRUPO, NULL ITEM, POA.NOM, POA.COM, POA.IDGS, POA.DATASIZE, POA.IDPORTAL , PO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE_ADJUN POA WITH (NOLOCK) INNER JOIN PROCE_OFE PO WITH (NOLOCK) ON POA.ID = PO.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE POA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT 2 AMBITO, OGA.ID, OGA.ADJUN, OGA.GRUPO, NULL ITEM, OGA.NOM, OGA.COM, OGA.IDGS, OGA.DATASIZE, OGA.IDPORTAL, GO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_GR_ADJUN OGA WITH (NOLOCK) INNER JOIN GRUPO_OFE GO WITH (NOLOCK) ON OGA.ID = GO.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE OGA.ID = @ID " & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT 3 AMBITO, OIA.ID, OIA.ADJUN, NULL GRUPO, OIA.ITEM, OIA.NOM, OIA.COM, OIA.IDGS, OIA.DATASIZE, OIA.IDPORTAL , IO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA WITH (NOLOCK) INNER JOIN ITEM_OFE IO WITH (NOLOCK) ON OIA.ID = IO.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ID =@ID" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT 2 AMBITO, OGA.ID, OGA.ADJUN, OGA.GRUPO, NULL ITEM, OGA.NOM, OGA.COM, OGA.IDGS, OGA.DATASIZE, OGA.IDPORTAL, GO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_GR_ADJUN OGA WITH (NOLOCK) INNER JOIN GRUPO_OFE GO WITH (NOLOCK) ON OGA.ID = GO.ID AND OGA.GRUPO = GO.GRUPO" & vbCrLf
sConsulta = sConsulta & "     WHERE OGA.ID = @ID " & vbCrLf
sConsulta = sConsulta & "       AND OGA.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT 3 AMBITO, OIA.ID, OIA.ADJUN, NULL GRUPO, OIA.ITEM, OIA.NOM, OIA.COM, OIA.IDGS, OIA.DATASIZE, OIA.IDPORTAL , IO.ADJUN SOLOPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA WITH (NOLOCK) INNER JOIN ITEM_OFE IO WITH (NOLOCK) ON OIA.ID = IO.ID AND IO.ITEM = OIA.ITEM" & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ID =@ID" & vbCrLf
sConsulta = sConsulta & "       AND IO.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM PROCE_OFE WITH (NOLOCK) WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN, NOM,COM, IDGS, DATASIZE, IDPORTAL " & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE_ADJUN WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM GRUPO_OFE WITH (NOLOCK) WHERE ID = @ID AND GRUPO = @GRUPO)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN,GRUPO, NOM, COM, IDGS, DATASIZE, IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_GR_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     WHERE ID = @ID " & vbCrLf
sConsulta = sConsulta & "       AND GRUPO = ISNULL(@GRUPO,GRUPO)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SOLOPORTAL = (SELECT ADJUN FROM ITEM_OFE WITH (NOLOCK) WHERE ID = @ID AND ITEM = @ITEM)" & vbCrLf
sConsulta = sConsulta & "    SELECT ID,ADJUN,ITEM,NOM, COM, IDGS, DATASIZE, IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     WHERE ID =@ID" & vbCrLf
sConsulta = sConsulta & "       AND ITEM = ISNULL(@ITEM,ITEM)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB] @ID int, @AMBITO tinyint, @GRUPO varchar(50)=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "            WHERE ID = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EL CAMBIO QUE DEVUELVE ES EL CAMBIO DE LA MONEDA EN QUE SE GUARD� LA OFERTA RESPECTO A LA MONEDA DEL PROCESO" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL, G.IMPPARCIAL , @CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE ID= @ID" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO ,IP.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_GR_ATRIB G WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.ITEM,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO,IP.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIB G WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE    PROCEDURE [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL] " & vbCrLf
sConsulta = sConsulta & "   @SDEN VARCHAR(8)," & vbCrLf
sConsulta = sConsulta & "   @SBUSCAR VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @SCOMP INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA= 'x.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL = '" & vbCrLf
sConsulta = sConsulta & "SELECT x.ID ACT1, NULL ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA + ' DEN ,COUNT(J.CIA) AS PROVE, CODACT1, NULL CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT1 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.CIA,PP.ACT1 " & vbCrLf
sConsulta = sConsulta & "FROM (SELECT CIA, ACT1 FROM CIAS_ACT1 WITH (NOLOCK) UNION SELECT CIA, ACT1 FROM CIAS_ACT2 WITH (NOLOCK) UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT CIA, ACT1 FROM CIAS_ACT3 WITH (NOLOCK) UNION SELECT CIA, ACT1 FROM CIAS_ACT4 WITH (NOLOCK) UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT CIA, ACT1 FROM CIAS_ACT5 WITH (NOLOCK)) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT1=x.ID " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%'' " & vbCrLf
sConsulta = sConsulta & "GROUP BY   x.ID, CODACT1, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1, x.ID ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT2 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.CIA,PP.ACT1,PP.ACT2 " & vbCrLf
sConsulta = sConsulta & "FROM (SELECT CIA, ACT1, ACT2 FROM CIAS_ACT2 WITH (NOLOCK) UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT3 WITH (NOLOCK) UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT CIA, ACT1, ACT2 FROM CIAS_ACT4 WITH (NOLOCK) UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT5 WITH (NOLOCK)) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=PP.CIA) J ON J.ACT2=x.ID  AND J.ACT1=x.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 WITH (NOLOCK) ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY  x.ACT1, x.ID, CODACT1, CODACT2,' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ID ACT3, NULL ACT4, NULL ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT3 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.CIA,PP.ACT1,PP.ACT2,PP.ACT3 " & vbCrLf
sConsulta = sConsulta & "FROM (SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT3 WITH (NOLOCK) UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT4 WITH (NOLOCK) UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT5 WITH (NOLOCK)) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT3=x.ID AND J.ACT2=x.ACT2 AND J.ACT1=x.ACT1 " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ID, CODACT1, CODACT2, CODACT3, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ACT3, x.ID ACT4, NULL ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3,CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT4 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.CIA,PP.ACT1,PP.ACT2,PP.ACT3,PP.ACT4 " & vbCrLf
sConsulta = sConsulta & "FROM (SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT4 WITH (NOLOCK) UNION SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT5 WITH (NOLOCK)) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA)  J ON J.ACT4=x.ID AND J.ACT3=x.ACT3 AND J.ACT2=x.ACT2 AND J.ACT1=x.ACT1  " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ACT3,x.ID, CODACT1, CODACT2, CODACT3, CODACT4, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ACT3, x.ACT4, x.ID ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3, CODACT4 , CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT5 x" & vbCrLf
sConsulta = sConsulta & "LEFT  JOIN CIAS_ACT5 J WITH (NOLOCK) ON J.ACT5=x.ID AND J.ACT4=x.ACT4 AND J.ACT3=x.ACT3 AND J.ACT2=x.ACT2  AND J.ACT1=x.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP WITH (NOLOCK) ON CP.CIA=J.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC WITH (NOLOCK) ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=J.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA + ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ACT3,x.ACT4,x.ID, CODACT1, CODACT2, CODACT3, CODACT4, CODACT5, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "ORDER by 1, 2, 3, 4, 5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TRANSFER_ADJUN_OFE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_TRANSFER_ADJUN_OFE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_TRANSFER_ADJUN_OFE] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
sConsulta = sConsulta & "   @IDADJUN INT," & vbCrLf
sConsulta = sConsulta & "   @INIT INT," & vbCrLf
sConsulta = sConsulta & "   @SIZE INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM ADJUN WITH (NOLOCK) WHERE CIA_COMP=@CIA AND ID=@IDADJUN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  ADJUN.DATA @textpointer @INIT   @LSIZE HOLDLOCK" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROVE_ESP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROVE_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROVE_ESP] " & vbCrLf
sConsulta = sConsulta & "   @IDPROVE int output, " & vbCrLf
sConsulta = sConsulta & "   @IDESP int = NULL, " & vbCrLf
sConsulta = sConsulta & "   @CODPROVE varchar(50) = null " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @IDPROVE is null" & vbCrLf
sConsulta = sConsulta & "   select @IDPROVE = ID FROM CIAS WITH (NOLOCK) WHERE COD = @CODPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDESP = NULL" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact , datalength(data) TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact, datalength(data)  TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "   AND ID = @IDESP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TRANSFER_PROVE_ESP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_TRANSFER_PROVE_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[SP_TRANSFER_PROVE_ESP] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
sConsulta = sConsulta & "   @ESP INT," & vbCrLf
sConsulta = sConsulta & "   @INIT INT," & vbCrLf
sConsulta = sConsulta & "   @SIZE INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM CIAS_ESP WITH (NOLOCK) WHERE CIA=@CIA AND ID=@ESP " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  CIAS_ESP.DATA @textpointer @INIT  @LSIZE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'----- Borrado de storeds
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_PAIS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_PAIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_PROVI]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_PROVI]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_MON]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_MODIF_PAIS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_MODIF_PAIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_MODIF_PROVI]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_MODIF_PROVI]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_MODIF_MON]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_MODIF_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Sub V_31900_5_Tablas_006()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT1]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "create view VIEW_ACT1 as " & vbCrLf
sConsulta = sConsulta & "SELECT     ID, COD, DEN_SPA, DEN_ENG, FECACT, DEN_GER, PYME, COD AS CODACT1" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT1 AS x WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT2]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "create view VIEW_ACT2 as " & vbCrLf
sConsulta = sConsulta & "SELECT     x.ACT1, x.ID, x.COD, x.DEN_SPA, x.DEN_ENG, x.FECACT, x.DEN_GER, dbo.ACT1.COD AS CODACT1, x.COD AS CODACT2" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT2 AS x WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT1 WITH (NOLOCK) ON x.ACT1 = dbo.ACT1.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT3]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "create view VIEW_ACT3 as " & vbCrLf
sConsulta = sConsulta & "SELECT     x.ACT1, x.ACT2, x.ID, x.COD, x.DEN_SPA, x.DEN_ENG, x.FECACT, x.DEN_GER, dbo.ACT1.COD AS CODACT1, dbo.ACT2.COD AS CODACT2, " & vbCrLf
sConsulta = sConsulta & "                      x.COD AS CODACT3" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT3 AS x WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT1 WITH (NOLOCK) ON x.ACT1 = dbo.ACT1.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT2 WITH (NOLOCK) ON x.ACT1 = dbo.ACT2.ACT1 AND x.ACT2 = dbo.ACT2.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT4]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "create view VIEW_ACT4 as " & vbCrLf
sConsulta = sConsulta & "SELECT     x.ACT1, x.ACT2, x.ACT3, x.ID, x.COD, x.DEN_SPA, x.DEN_ENG, x.FECACT, x.DEN_GER, dbo.ACT1.COD AS CODACT1, dbo.ACT2.COD AS CODACT2, " & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT3.COD AS CODACT3, x.COD AS CODACT4" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT4 AS x WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT1 WITH (NOLOCK) ON x.ACT1 = dbo.ACT1.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT2 WITH (NOLOCK) ON x.ACT1 = dbo.ACT2.ACT1 AND x.ACT2 = dbo.ACT2.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT3 WITH (NOLOCK) ON x.ACT1 = dbo.ACT3.ACT1 AND x.ACT2 = dbo.ACT3.ACT2 AND x.ACT3 = dbo.ACT3.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT5]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT5]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "create view VIEW_ACT5 as " & vbCrLf
sConsulta = sConsulta & "SELECT     x.ACT1, x.ACT2, x.ACT3, x.ACT4, x.ID, x.COD, x.DEN_SPA, x.DEN_ENG, x.FECACT, x.DEN_GER, dbo.ACT1.COD AS CODACT1, " & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT2.COD AS CODACT2, dbo.ACT3.COD AS CODACT3, dbo.ACT4.COD AS CODACT4, x.COD AS CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM         dbo.ACT5 AS x WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT1 WITH (NOLOCK) ON x.ACT1 = dbo.ACT1.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT2 WITH (NOLOCK) ON x.ACT1 = dbo.ACT2.ACT1 AND x.ACT2 = dbo.ACT2.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT3 WITH (NOLOCK) ON x.ACT1 = dbo.ACT3.ACT1 AND x.ACT2 = dbo.ACT3.ACT2 AND x.ACT3 = dbo.ACT3.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                      dbo.ACT4 WITH (NOLOCK) ON x.ACT1 = dbo.ACT4.ACT1 AND x.ACT2 = dbo.ACT4.ACT2 AND x.ACT3 = dbo.ACT4.ACT3 AND x.ACT3 = dbo.ACT4.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_15_06A31900_04_15_07() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Tablas_007

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_007
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_06A31900_04_15_07 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_06A31900_04_15_07 = False
End Function

Private Sub V_31900_5_Tablas_007()
Dim sConsulta As String

sConsulta = "IF NOT EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TablaPartidas' AND ss.name = N'dbo')" & vbCrLf
sConsulta = sConsulta & "   CREATE TYPE TablaPartidas AS TABLE (PRES0 NVARCHAR(200),  PRES1 NVARCHAR(200),  PRES2 NVARCHAR(200),  PRES3 NVARCHAR(200),  PRES4 NVARCHAR(200))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FSP_CONF_VISOR_RECEPCIONES]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[FSP_CONF_VISOR_RECEPCIONES](" & vbCrLf
sConsulta = sConsulta & "[CIA] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "[USU] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "[RETRASADO_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[RETRASADO_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[ALBARAN_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[ALBARAN_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[FRECEP_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[FRECEP_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[EMP_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[EMP_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[ERP_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[ERP_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[PEDIDO_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[PEDIDO_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[NUMPEDPROVE_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[NUMPEDPROVE_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[FEMISION_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[FEMISION_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[FENTREGA_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[FENTREGA_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[FENTREGAPROVE_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[FENTREGAPROVE_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[ART_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[ART_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[CANTPED_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[CANTPED_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[CANTREC_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[CANTREC_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[CANTPEND_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[CANTPEND_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[UNI_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[UNI_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[PREC_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[PREC_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[IMPPED_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[IMPPED_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[IMPREC_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[IMPREC_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[IMPPEND_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[IMPPEND_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[MON_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[MON_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[CC1_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[CC1_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[CC2_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[CC2_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[CC3_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[CC3_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[CC4_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[CC4_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[PARTIDA1_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[PARTIDA1_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[PARTIDA2_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[PARTIDA2_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[PARTIDA3_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[PARTIDA3_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "[PARTIDA4_VISIBLE] [tinyint] NOT NULL DEFAULT 1," & vbCrLf
sConsulta = sConsulta & "[PARTIDA4_WIDTH] [float] NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_FSP_CONF_VISOR_RECEPCIONES_CIAUSU] FOREIGN KEY (CIA, USU) REFERENCES USU(CIA, ID)" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Sub V_31900_5_Storeds_007()
Dim sConsulta As String
    
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_MOD_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL, @RETRASADO_VISIBLE int = NULL, @RETRASADO_WIDTH FLOAT = NULL, @ALBARAN_VISIBLE int = NULL, @ALBARAN_WIDTH FLOAT = NULL, @FRECEP_VISIBLE int = NULL, @FRECEP_WIDTH FLOAT = NULL, @EMP_VISIBLE int = NULL, @EMP_WIDTH FLOAT = NULL, @ERP_VISIBLE int = NULL, @ERP_WIDTH FLOAT = NULL, @PEDIDO_VISIBLE int = NULL, @PEDIDO_WIDTH FLOAT = NULL, @NUMPEDPROVE_VISIBLE int = NULL, @NUMPEDPROVE_WIDTH FLOAT = NULL, @FEMISION_VISIBLE int = NULL, @FEMISION_WIDTH FLOAT = NULL, @FENTREGA_VISIBLE int = NULL, @FENTREGA_WIDTH FLOAT = NULL, @FENTREGAPROVE_VISIBLE int = NULL, @FENTREGAPROVE_WIDTH FLOAT = NULL, @ART_VISIBLE int = NULL, @ART_WIDTH FLOAT = NULL, @CANTPED_VISIBLE int = NULL, @CANTPED_WIDTH FLOAT = NULL, @CANTREC_VISIBLE int = NULL, @CANTREC_WIDTH FLOAT = NULL, @CANTPEND_VISIBLE int = NULL, @CANTPEND_WIDTH FLOAT = NULL, @UNI_VISIBLE int = NULL, @UNI_WIDTH FLOAT = NULL, @PREC_VISIBLE int = NULL, "
sConsulta = sConsulta & "@PREC_WIDTH FLOAT = NULL, @IMPPED_VISIBLE int = NULL, @IMPPED_WIDTH FLOAT = NULL, @IMPREC_VISIBLE int = NULL, @IMPREC_WIDTH FLOAT = NULL, @IMPPEND_VISIBLE int = NULL, @IMPPEND_WIDTH FLOAT = NULL, @MON_VISIBLE int = NULL, @MON_WIDTH FLOAT = NULL, @CC1_VISIBLE int = NULL, @CC1_WIDTH FLOAT = NULL,@CC2_VISIBLE int = NULL, @CC2_WIDTH FLOAT = NULL,@CC3_VISIBLE int = NULL, @CC3_WIDTH FLOAT = NULL,@CC4_VISIBLE int = NULL, @CC4_WIDTH FLOAT = NULL, @PARTIDA1_VISIBLE int = NULL, @PARTIDA1_WIDTH FLOAT = NULL, @PARTIDA2_VISIBLE int = NULL, @PARTIDA2_WIDTH FLOAT = NULL, @PARTIDA3_VISIBLE int = NULL, @PARTIDA3_WIDTH FLOAT = NULL, @PARTIDA4_VISIBLE int = NULL, @PARTIDA4_WIDTH FLOAT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @USU IS NULL AND NOT @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "       SELECT @IDUSU = ID FROM USU WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @HAYREGISTRO BIT = 0" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT USU FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK) WHERE USU = @IDUSU AND CIA = @CIA)" & vbCrLf
sConsulta = sConsulta & "           SET @HAYREGISTRO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --INSERTAR" & vbCrLf
sConsulta = sConsulta & "       IF @HAYREGISTRO = 0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @ANCHODEFECTO INT = 30 --Es un caso muy poco probable que no llegue la informaci�n del ancho. En este caso ponemos un valor arbitrario como ancho. 30 es un tama�o suficiente para ser visible pero no molestar" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'INSERT INTO FSP_CONF_VISOR_RECEPCIONES VALUES (@CIA, @IDUSU'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @RETRASADO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ALBARAN_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FRECEP_WIDTH IS NULL                " & vbCrLf
sConsulta = sConsulta & "                   SET @FRECEP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @EMP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PEDIDO_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @NUMPEDPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FEMISION_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGA_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FENTREGAPROVE_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @ART_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CANTPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UNI_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPED_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPREC_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPPEND_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @MON_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @CC4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA1_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA2_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA3_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_VISIBLE = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "               IF @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @PARTIDA4_WIDTH = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ', @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = @SQLSTRING + ')'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       --ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE @PONCOMA BIT = 0" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET '" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_VISIBLE = @RETRASADO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @RETRASADO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'RETRASADO_WIDTH = @RETRASADO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_VISIBLE = @ALBARAN_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ALBARAN_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ALBARAN_WIDTH = @ALBARAN_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_VISIBLE = @FRECEP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FRECEP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FRECEP_WIDTH = @FRECEP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_VISIBLE = @EMP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @EMP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'EMP_WIDTH = @EMP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_VISIBLE = @ERP_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ERP_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ERP_WIDTH = @ERP_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_VISIBLE = @PEDIDO_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PEDIDO_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PEDIDO_WIDTH = @PEDIDO_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @NUMPEDPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_VISIBLE = @FEMISION_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FEMISION_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FEMISION_WIDTH = @FEMISION_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_VISIBLE = @FENTREGA_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGA_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGA_WIDTH = @FENTREGA_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @FENTREGAPROVE_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_VISIBLE = @ART_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @ART_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'ART_WIDTH = @ART_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_VISIBLE = @CANTPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPED_WIDTH = @CANTPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_VISIBLE = @CANTREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTREC_WIDTH = @CANTREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_VISIBLE = @CANTPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CANTPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CANTPEND_WIDTH = @CANTPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_VISIBLE = @UNI_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @UNI_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'UNI_WIDTH = @UNI_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_VISIBLE = @PREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PREC_WIDTH = @PREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_VISIBLE = @IMPPED_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPED_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPED_WIDTH = @IMPPED_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_VISIBLE = @IMPREC_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPREC_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPREC_WIDTH = @IMPREC_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_VISIBLE = @IMPPEND_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @IMPPEND_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'IMPPEND_WIDTH = @IMPPEND_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_VISIBLE = @MON_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @MON_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'MON_WIDTH = @MON_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_VISIBLE = @CC1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC1_WIDTH = @CC1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_VISIBLE = @CC2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC2_WIDTH = @CC2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_VISIBLE = @CC3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC3_WIDTH = @CC3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_VISIBLE = @CC4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @CC4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'CC4_WIDTH = @CC4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA1_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA1_WIDTH = @PARTIDA1_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA2_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA2_WIDTH = @PARTIDA2_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA3_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA3_WIDTH = @PARTIDA3_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_VISIBLE IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               IF NOT @PARTIDA4_WIDTH IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN       " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = @SQLSTRING + CASE WHEN @PONCOMA = 1 THEN ',' ELSE '' END + 'PARTIDA4_WIDTH = @PARTIDA4_WIDTH'" & vbCrLf
sConsulta = sConsulta & "                   SET @PONCOMA = 1" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = @SQLSTRING + ' WHERE USU = @IDUSU AND CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       --Puede darse el caso de que, al contruir la cadena no se actualice nada, por lo que lo controlamos antes de ejecutarla" & vbCrLf
sConsulta = sConsulta & "       IF @SQLSTRING <> 'UPDATE FSP_CONF_VISOR_RECEPCIONES SET'" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDUSU INT, @CIA INT, @RETRASADO_VISIBLE INT, @RETRASADO_WIDTH FLOAT, @ALBARAN_VISIBLE INT, @ALBARAN_WIDTH FLOAT, @FRECEP_VISIBLE INT, @FRECEP_WIDTH FLOAT, @EMP_VISIBLE INT, @EMP_WIDTH FLOAT, @ERP_VISIBLE INT, @ERP_WIDTH FLOAT, @PEDIDO_VISIBLE INT, @PEDIDO_WIDTH FLOAT, @NUMPEDPROVE_VISIBLE INT, @NUMPEDPROVE_WIDTH FLOAT, @FEMISION_VISIBLE INT, @FEMISION_WIDTH FLOAT, @FENTREGA_VISIBLE INT, @FENTREGA_WIDTH FLOAT, @FENTREGAPROVE_VISIBLE INT, @FENTREGAPROVE_WIDTH FLOAT, @ART_VISIBLE INT, @ART_WIDTH FLOAT, @CANTPED_VISIBLE INT, @CANTPED_WIDTH FLOAT, @CANTREC_VISIBLE INT, @CANTREC_WIDTH FLOAT, @CANTPEND_VISIBLE INT, @CANTPEND_WIDTH FLOAT, @UNI_VISIBLE INT, @UNI_WIDTH FLOAT, @PREC_VISIBLE INT, @PREC_WIDTH FLOAT, @IMPPED_VISIBLE INT, @IMPPED_WIDTH FLOAT, @IMPREC_VISIBLE INT, @IMPREC_WIDTH FLOAT, @IMPPEND_VISIBLE INT, @IMPPEND_WIDTH FLOAT, @MON_VISIBLE INT, @MON_WIDTH FLOAT, @CC1_VISIBLE INT, @CC1_WIDTH FLOAT,@CC2_VISIBLE INT,"
sConsulta = sConsulta & "@CC2_WIDTH FLOAT,@CC3_VISIBLE INT, @CC3_WIDTH FLOAT,@CC4_VISIBLE INT, @CC4_WIDTH FLOAT, @PARTIDA1_VISIBLE INT, @PARTIDA1_WIDTH FLOAT, @PARTIDA2_VISIBLE INT, @PARTIDA2_WIDTH FLOAT, @PARTIDA3_VISIBLE INT, @PARTIDA3_WIDTH FLOAT, @PARTIDA4_VISIBLE INT, @PARTIDA4_WIDTH FLOAT, @ANCHODEFECTO INT'," & vbCrLf
sConsulta = sConsulta & "               @IDUSU = @IDUSU," & vbCrLf
sConsulta = sConsulta & "               @CIA = @CIA," & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_VISIBLE = @RETRASADO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @RETRASADO_WIDTH = @RETRASADO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_VISIBLE = @ALBARAN_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ALBARAN_WIDTH = @ALBARAN_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_VISIBLE = @FRECEP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FRECEP_WIDTH = @FRECEP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @EMP_VISIBLE = @EMP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @EMP_WIDTH = @EMP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ERP_VISIBLE = @ERP_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ERP_WIDTH = @ERP_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_VISIBLE = @PEDIDO_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PEDIDO_WIDTH = @PEDIDO_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_VISIBLE = @NUMPEDPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @NUMPEDPROVE_WIDTH = @NUMPEDPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_VISIBLE = @FEMISION_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FEMISION_WIDTH = @FEMISION_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_VISIBLE = @FENTREGA_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGA_WIDTH = @FENTREGA_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_VISIBLE = @FENTREGAPROVE_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @FENTREGAPROVE_WIDTH = @FENTREGAPROVE_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @ART_VISIBLE = @ART_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @ART_WIDTH = @ART_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_VISIBLE = @CANTPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPED_WIDTH = @CANTPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_VISIBLE = @CANTREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTREC_WIDTH = @CANTREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_VISIBLE = @CANTPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CANTPEND_WIDTH = @CANTPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @UNI_VISIBLE = @UNI_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @UNI_WIDTH = @UNI_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PREC_VISIBLE = @PREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PREC_WIDTH = @PREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_VISIBLE = @IMPPED_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPED_WIDTH = @IMPPED_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_VISIBLE = @IMPREC_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPREC_WIDTH = @IMPREC_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_VISIBLE = @IMPPEND_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @IMPPEND_WIDTH = @IMPPEND_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @MON_VISIBLE = @MON_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @MON_WIDTH = @MON_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC1_VISIBLE = @CC1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC1_WIDTH = @CC1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC2_VISIBLE = @CC2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC2_WIDTH = @CC2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC3_VISIBLE = @CC3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC3_WIDTH = @CC3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @CC4_VISIBLE = @CC4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @CC4_WIDTH = @CC4_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_VISIBLE = @PARTIDA1_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA1_WIDTH = @PARTIDA1_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_VISIBLE = @PARTIDA2_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA2_WIDTH = @PARTIDA2_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_VISIBLE = @PARTIDA3_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA3_WIDTH = @PARTIDA3_WIDTH, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_VISIBLE = @PARTIDA4_VISIBLE, " & vbCrLf
sConsulta = sConsulta & "               @PARTIDA4_WIDTH = @PARTIDA4_WIDTH," & vbCrLf
sConsulta = sConsulta & "               @ANCHODEFECTO = @ANCHODEFECTO" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  @IDIOMA NVARCHAR(20)='SPA', @CIA NVARCHAR(100) = NULL, @PROVE NVARCHAR(100) = NULL, @ANYO INT = NULL, @NUMPEDIDO INT = NULL, @NUMORDEN INT = NULL, @NUMPEDIDOERP NVARCHAR(50) = NULL, @FECRECEPDESDE DATETIME = NULL, @FECRECEPHASTA DATETIME = NULL, @VERPEDIDOSSINRECEP BIT = 1, @NUMALBARAN VARCHAR(100) = NULL, @NUMPEDPROVE VARCHAR(100) = NULL, @ARTICULO VARCHAR(200) = NULL, @FECENTREGASOLICITDESDE DATETIME = NULL, @FECENTREGASOLICITHASTA DATETIME = NULL, @CODEMPRESA INT = NULL, @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL, @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL, @FECEMISIONDESDE DATETIME = NULL, @FECEMISIONHASTA DATETIME = NULL, @CENTRO VARCHAR(50) = NULL, @PARTIDASPRES TablaPartidas READONLY" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECRECEPHASTA_MAS1 DATETIME " & vbCrLf
sConsulta = sConsulta & "SET @FECRECEPHASTA_MAS1 = DATEADD(dd,1,@FECRECEPHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECENTREGASOLICITHASTA = DATEADD(dd,1,@FECENTREGASOLICITHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECENTREGAINDICADAPROVEHASTA = DATEADD(dd,1,@FECENTREGAINDICADAPROVEHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECEMISIONHASTA = DATEADD(dd,1,@FECEMISIONHASTA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) -- BASE DE DATOS DEL CLIENTE" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLQUERY AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_SELECT01 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_SELECT02 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_FIN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_PARTIDA_COD AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_NIVEL_DEN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_CCCOD AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_CCDEN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLWHERE AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_SELECT01 = 'SELECT " & vbCrLf
sConsulta = sConsulta & "       LP.ID LINEAPEDID, LR.ID LINEARECEPID'   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_SELECT02 = '" & vbCrLf
sConsulta = sConsulta & "       ,PR.ALBARAN, " & vbCrLf
sConsulta = sConsulta & "       PR.FECHA FECHARECEP, " & vbCrLf
sConsulta = sConsulta & "       OE.EMPRESA IDEMPRESA, E.DEN DENEMPRESA, " & vbCrLf
sConsulta = sConsulta & "       OE.NUM_PED_ERP, " & vbCrLf
sConsulta = sConsulta & "       OE.ANYO, PE.NUM PEDIDONUM, OE.NUM ORDENNUM, " & vbCrLf
sConsulta = sConsulta & "       CAST(OE.ANYO as VARCHAR(20)) + CAST(''/'' as VARCHAR(3)) + CAST(PE.NUM as VARCHAR(100)) + CAST(''/'' as VARCHAR(3)) + CAST(OE.NUM as VARCHAR(100)) ANYOPEDIDOORDEN, " & vbCrLf
sConsulta = sConsulta & "       OE.ID ORDENID, OE.NUMEXT NUMPEDPROVE, " & vbCrLf
sConsulta = sConsulta & "       PE.FECHA FECHAEMISION," & vbCrLf
sConsulta = sConsulta & "       LP.FECENTREGAPROVE FECENTREGAINDICADAPROVE," & vbCrLf
sConsulta = sConsulta & "       LP.FECENTREGA FECENTREGASOLICITADA," & vbCrLf
sConsulta = sConsulta & "       I.ART ARTCOD, I.DESCR ARTDEN," & vbCrLf
sConsulta = sConsulta & "       LP.CANT_PED CANTLINEAPEDIDATOTAL," & vbCrLf
sConsulta = sConsulta & "       LR.CANT CANTLINEARECIBIDAPARCIAL," & vbCrLf
sConsulta = sConsulta & "       (LP.CANT_PED - SUMAPORORDEN.CANTRECIBIDATOTAL) CANTLINEAPENDIENTETOTAL," & vbCrLf
sConsulta = sConsulta & "       LP.UP UNICOD, UD.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "       LP.PREC_UP," & vbCrLf
sConsulta = sConsulta & "       LP.FC * LP.PREC_UP * LP.CANT_PED IMPORTELINEATOTAL," & vbCrLf
sConsulta = sConsulta & "       LP.FC * LP.PREC_UP * LR.CANT IMPORTELINEARECIBIDOPARCIAL," & vbCrLf
sConsulta = sConsulta & "       (LP.FC * LP.PREC_UP * LP.CANT_PED - (LP.FC * LP.PREC_UP * SUMAPORORDEN.CANTRECIBIDATOTAL)) IMPORTELINEAPENDIENTETOTAL," & vbCrLf
sConsulta = sConsulta & "       OE.MON MONCOD, MD.DEN MONDEN," & vbCrLf
sConsulta = sConsulta & "       OE.EST'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_PARTIDA_COD = '" & vbCrLf
sConsulta = sConsulta & "       ,''PARTIDA_COD_'' + LPI.PRES0 PRES0_PARTIDA_COD" & vbCrLf
sConsulta = sConsulta & "       ,COALESCE(LPI.PRES0,'''') + ''#'' + COALESCE(LPI.PRES1,'''') + ''#'' + COALESCE(LPI.PRES2,'''') + ''#'' + COALESCE(LPI.PRES3,'''') + ''#'' + COALESCE(LPI.PRES4,'''') PARTIDA_COD'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_NIVEL_DEN = '" & vbCrLf
sConsulta = sConsulta & "       ,''NIVEL_DEN_'' + LPI.PRES0 PRES0_NIVEL_DEN" & vbCrLf
sConsulta = sConsulta & "       ,CASE COALESCE (LPI.PRES4, LPI.PRES3, LPI.PRES2, LPI.PRES1)" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES2 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I2.DEN" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES3 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I3.DEN" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES4 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I4.DEN" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           P5I1.DEN" & vbCrLf
sConsulta = sConsulta & "       END NIVEL_DEN' " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_CCCOD = '" & vbCrLf
sConsulta = sConsulta & "       ,''CCCOD_'' + LPI.PRES0 PRES0_CCCOD" & vbCrLf
sConsulta = sConsulta & "       ,LPI_CSM.CENTRO_SM AS CCCOD' " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_CCDEN = '" & vbCrLf
sConsulta = sConsulta & "       ,''CCDEN_'' + LPI.PRES0 PRES0_CCDEN" & vbCrLf
sConsulta = sConsulta & "       ,COALESCE (LPI.UON4, LPI.UON3, LPI.UON2, LPI.UON1) + '' - '' + LPI_CSM.DEN AS CCDEN'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = ' " & vbCrLf
sConsulta = sConsulta & "   FROM ' + @FSGS + 'ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'PEDIDO PE WITH(NOLOCK) ON OE.PEDIDO = PE.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LP.ID=LPI.LINEA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUMPARTIDAS AS INT = 0" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMPARTIDAS = COUNT(*) FROM @PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPARTIDAS > 0" & vbCrLf
sConsulta = sConsulta & "       SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN @PARTIDASPRES PRES" & vbCrLf
sConsulta = sConsulta & "       ON PRES.PRES0 = COALESCE(LPI.PRES0,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES1 = COALESCE(LPI.PRES1,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES2 = COALESCE(LPI.PRES2,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES3 = COALESCE(LPI.PRES3,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES4 = COALESCE(LPI.PRES4,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN  + '" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'ITEM I WITH(NOLOCK) ON LP.ANYO = I.ANYO AND LP.GMN1 = I.GMN1_PROCE AND LP.PROCE = I.PROCE AND LP.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'LINEAS_RECEP LR WITH(NOLOCK) ON LR.LINEA = LP.ID AND LR.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PEDIDO_RECEP PR WITH(NOLOCK) ON PR.ID = LR.PEDIDO_RECEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'MON_DEN MD WITH (NOLOCK) ON MD.MON = OE.MON AND MD.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'UNI_DEN UD WITH (NOLOCK) ON UD.UNI = LP.UP AND UD.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'EMP E WITH(NOLOCK) ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT ORDEN, SUM(CANT) CANTRECIBIDATOTAL " & vbCrLf
sConsulta = sConsulta & "               FROM ' + @FSGS + 'LINEAS_RECEP LR1 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ' + @FSGS + 'PEDIDO PE1 WITH(NOLOCK) ON PE1.ID = LR1.PEDIDO_RECEP" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ' + @FSGS + 'ORDEN_ENTREGA OE1 WITH(NOLOCK) ON OE1.ID = LR1.ORDEN " & vbCrLf
sConsulta = sConsulta & "              GROUP BY ORDEN, PROVE" & vbCrLf
sConsulta = sConsulta & "              HAVING OE1.PROVE = @PROVECOD" & vbCrLf
sConsulta = sConsulta & "             ) SUMAPORORDEN ON SUMAPORORDEN.ORDEN = LR.ORDEN" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "       SELECT CSM.CENTRO_SM, CSM.DEN, LPI.LINEA, LPI.UON1, LPI.UON2, LPI.UON3, LPI.UON4" & vbCrLf
sConsulta = sConsulta & "       FROM ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN ' + @FSGS + 'CENTRO_SM_UON CSM_UON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON ISNULL(LPI.UON1,'''') = ISNULL(CSM_UON.UON1,'''') AND ISNULL(LPI.UON2,'''') = ISNULL(CSM_UON.UON2,'''') AND ISNULL(LPI.UON3,'''') = ISNULL(CSM_UON.UON3,'''') AND ISNULL(LPI.UON4,'''') = ISNULL(CSM_UON.UON4,'''')" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'CENTRO_SM_DEN CSM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON CSM.CENTRO_SM = CSM_UON.CENTRO_SM AND CSM.IDI = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   ) LPI_CSM ON LP.ID = LPI_CSM.LINEA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I0 WITH (NOLOCK) ON P5I0.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.PRES1 IS NULL AND P5I0.PRES2 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.PRES3 IS NULL AND P5I0.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I1 WITH (NOLOCK) ON P5I1.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.PRES1 = LPI.PRES1" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.PRES2 IS NULL AND P5I1.PRES3 IS NULL AND P5I1.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I2 WITH (NOLOCK) ON P5I2.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I2.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.PRES3 IS NULL AND P5I2.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I3 WITH (NOLOCK) ON P5I3.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I3.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I3.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND LPI.PRES3 = P5I3.PRES3 AND P5I3.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I3.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I4 WITH (NOLOCK) ON P5I4.PRES0 = LPI.PRES0 " & vbCrLf
sConsulta = sConsulta & "       AND P5I4.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I4.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND LPI.PRES3 = P5I4.PRES3 AND LPI.PRES4 = P5I4.PRES4" & vbCrLf
sConsulta = sConsulta & "       AND P5I4.IDIOMA = @IDIOMA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = ' " & vbCrLf
sConsulta = sConsulta & "   WHERE" & vbCrLf
sConsulta = sConsulta & "       OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "   IF @ANYO<>0 AND @ANYO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.ANYO = @ANYO'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDIDO<>0 AND @NUMPEDIDO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.NUM = @NUMPEDIDO'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMORDEN<>0 AND @NUMORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUM = @NUMORDEN'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDIDOERP<>'' AND @NUMPEDIDOERP IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUM_PED_ERP = @NUMPEDIDOERP'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --  Restricciones de PEDIDO_RECEP   -----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   IF @VERPEDIDOSSINRECEP = 0 --SE MUESTRAN S�LO LOS PEDIDOS CON RECEPCIONES" & vbCrLf
sConsulta = sConsulta & "       SET @SQlWHERE = @SQLWHERE + ' AND PR.ID IS NOT NULL' " & vbCrLf
sConsulta = sConsulta & "   ELSE --SE MUESTRAN LOS PEDIDOS SIN RECEPCIONES CUYA FECHA DE ENTREGA SOLICITADA EST� COMPRENDIDA ENTRE las fechas FECRECEPDESDE y FECRECEPHASTA" & vbCrLf
sConsulta = sConsulta & "        --y SE MUESTRAN LOS PEDIDOS CON RECEPCIONES QUE CUMPLAN LAS RESTRICCIONES DE FECHA Y ALBAR�N INDICADAS" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQlWHERE = @SQLWHERE + ' " & vbCrLf
sConsulta = sConsulta & "       AND (" & vbCrLf
sConsulta = sConsulta & "            (PR.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "           --LP.FECENTREGA ES UN CAMPO OPCIONAL POR LO QUE PUEDE SER NULO. VAMOS A MOSTRAR LOS VALORES QUE SEAN NULOS." & vbCrLf
sConsulta = sConsulta & "           IF @FECRECEPDESDE<>'' AND @FECRECEPDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "               SET @SQLWHERE = @SQLWHERE + ' AND COALESCE(LP.FECENTREGA,@FECRECEPDESDE) >= @FECRECEPDESDE'" & vbCrLf
sConsulta = sConsulta & "           IF @FECRECEPHASTA<>'' AND @FECRECEPHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "               SET @SQLWHERE = @SQLWHERE + ' AND COALESCE(LP.FECENTREGA,@FECRECEPHASTA) < @FECRECEPHASTA_MAS1'" & vbCrLf
sConsulta = sConsulta & "           SET @SQLWHERE = @SQLWHERE + ')" & vbCrLf
sConsulta = sConsulta & "            OR" & vbCrLf
sConsulta = sConsulta & "            (NOT PR.ID IS NULL' " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @FECRECEPDESDE<>'' AND @FECRECEPDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.FECHA >= @FECRECEPDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECRECEPHASTA<>'' AND @FECRECEPHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.FECHA < @FECRECEPHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMALBARAN<>'' AND @NUMALBARAN IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.ALBARAN = @NUMALBARAN'" & vbCrLf
sConsulta = sConsulta & "   IF @VERPEDIDOSSINRECEP = 1 --CERRAMOS EL PAR�NTESIS QUE HAB�AMOS ABIERTO" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' )" & vbCrLf
sConsulta = sConsulta & "           )'" & vbCrLf
sConsulta = sConsulta & "   --  FIN Restricciones de PEDIDO_RECEP   -----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDPROVE<>'' AND @NUMPEDPROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUMEXT = @NUMPEDPROVE'" & vbCrLf
sConsulta = sConsulta & "   IF @ARTICULO<>'' AND @ARTICULO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND (I.ART LIKE ''%'' + @ARTICULO + ''%''  OR I.DESCR LIKE ''%'' + @ARTICULO + ''%'')'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGASOLICITDESDE<>'' AND @FECENTREGASOLICITDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGA >= @FECENTREGASOLICITDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGASOLICITHASTA<>'' AND @FECENTREGASOLICITHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGA < @FECENTREGASOLICITHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @CODEMPRESA<>'' AND @CODEMPRESA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.EMPRESA = @CODEMPRESA'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGAINDICADAPROVEDESDE<>'' AND @FECENTREGAINDICADAPROVEDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGAPROVE >= @FECENTREGAINDICADAPROVEDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGAINDICADAPROVEHASTA<>'' AND @FECENTREGAINDICADAPROVEHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGAPROVE < @FECENTREGAINDICADAPROVEHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @FECEMISIONDESDE<>'' AND @FECEMISIONDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.FECHA >= @FECEMISIONDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECEMISIONHASTA<>'' AND @FECEMISIONHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.FECHA < @FECEMISIONHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @CENTRO<>'' AND @CENTRO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LPI_CSM.CENTRO_SM = @CENTRO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Vamos a obtener todas las partidas presupuestarias de nivel 0 para poder usarlas luego" & vbCrLf
sConsulta = sConsulta & "   --como columnas al mostrar datos de partidas y centros de coste para cada recepci�n/entrega" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PRES0 NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #PRES5(" & vbCrLf
sConsulta = sConsulta & "       COD VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PRES0 = 'INSERT INTO #PRES5 SELECT COD FROM ' + @FSGS + 'PRES5_NIV0 WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXEC(@PRES0)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @PRES0  = STUFF(( " & vbCrLf
sConsulta = sConsulta & "                       SELECT DISTINCT '],[' + ltrim(#PRES5.COD)" & vbCrLf
sConsulta = sConsulta & "                           FROM #PRES5" & vbCrLf
sConsulta = sConsulta & "                           ORDER BY '],[' + ltrim(#PRES5.COD)" & vbCrLf
sConsulta = sConsulta & "                           FOR XML PATH('')" & vbCrLf
sConsulta = sConsulta & "                       ), 1, 2, '') + ']'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DROP TABLE #PRES5" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Esta select * no llama a una tabla sino a una consulta, por lo que devuelve " & vbCrLf
sConsulta = sConsulta & "   --todos los registros de la consulta, no de la tabla" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = '" & vbCrLf
sConsulta = sConsulta & "   SELECT * FROM ('" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ----DENOMINACION DE LA PARTIDA PRESUPUESTARIA" & vbCrLf
sConsulta = sConsulta & "   SET  @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_SELECT02 + @SUBSQLQUERY_NIVEL_DEN + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + @SUBSQLQUERY" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC1" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(NIVEL_DEN) FOR PRES0_NIVEL_DEN" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[NIVEL_DEN_') + ')) AS PVT1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --CODIGO UON COMPUESTO DE LA PARTIDA (UON1+UON2+UON3+UON4)" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_PARTIDA_COD + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC3" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(PARTIDA_COD) FOR PRES0_PARTIDA_COD" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[PARTIDA_COD_') + ')) AS PVT3" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT3.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT3.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --COD CENTRO DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_CCCOD + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC5" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(CCCOD) FOR PRES0_CCCOD" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[CCCOD_') + ')) AS PVT5" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT5.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT5.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --UON + DEN CENTRO DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_CCDEN + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC6" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(CCDEN) FOR PRES0_CCDEN" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[CCDEN_') + ')) AS PVT6" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT6.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT6.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --ORDER BY" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ORDER BY FECHARECEP DESC'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20), @PROVECOD NVARCHAR(100), @ANYO INT, @NUMPEDIDO INT, @NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50), @FECRECEPDESDE DATETIME, @FECRECEPHASTA DATETIME, @VERPEDIDOSSINRECEP INT, @NUMALBARAN VARCHAR(100), @NUMPEDPROVE VARCHAR(100), @ARTICULO VARCHAR(200), @FECENTREGASOLICITDESDE DATETIME, @FECENTREGASOLICITHASTA DATETIME, @CODEMPRESA INT, @FECENTREGAINDICADAPROVEDESDE DATETIME, @FECENTREGAINDICADAPROVEHASTA DATETIME, @FECEMISIONDESDE DATETIME, @FECEMISIONHASTA DATETIME, @CENTRO VARCHAR(50), @PARTIDASPRES TablaPartidas READONLY, @FECRECEPHASTA_MAS1 DATETIME', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                       @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                       @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                       @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                       @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                       @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                       @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEDESDE =@FECENTREGAINDICADAPROVEDESDE, " & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEHASTA = @FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                       @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA_MAS1=@FECRECEPHASTA_MAS1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "   --TABLA CON LOS NOMBRES DE LAS PARTIDAS" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = 'SELECT * FROM(SELECT P5.COD PRES0," & vbCrLf
sConsulta = sConsulta & "       CASE PSM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "           WHEN 2 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL2,'''')" & vbCrLf
sConsulta = sConsulta & "           WHEN 3 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL3,'''')" & vbCrLf
sConsulta = sConsulta & "           WHEN 4 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL4,'''')" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL1,'''')" & vbCrLf
sConsulta = sConsulta & "           END PARTIDA_DEN" & vbCrLf
sConsulta = sConsulta & "   FROM  ' + @FSGS + 'PRES5_NIV0 P5 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  ' + @FSGS + 'PARGEN_SM PSM WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON P5.COD = PSM.PRES5" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN  ' + @FSGS + 'PRES5_IDIOMAS P5I0 WITH (NOLOCK) ON P5I0.PRES0 = P5.COD" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.PRES1 IS NULL AND P5I0.PRES2 IS NULL" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.PRES3 IS NULL AND P5I0.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.IDIOMA = @IDIOMA)SRC2" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(PARTIDA_DEN) FOR PRES0" & vbCrLf
sConsulta = sConsulta & "   IN (' + @PRES0 + ')) AS PVT2'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20)', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --TABLA CON LOS CENTROS DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = 'SELECT DISTINCT" & vbCrLf
sConsulta = sConsulta & "       LPI_CSM.CENTRO_SM AS CCCOD, " & vbCrLf
sConsulta = sConsulta & "       COALESCE (LPI.UON4, LPI.UON3, LPI.UON2, LPI.UON1) + '' - '' + LPI_CSM.DEN AS CCDEN'" & vbCrLf
sConsulta = sConsulta & "       + @SUBSQLQUERY_FIN +" & vbCrLf
sConsulta = sConsulta & "       ' AND NOT LPI_CSM.CENTRO_SM IS NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20), @PROVECOD NVARCHAR(100), @ANYO INT, @NUMPEDIDO INT, @NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50), @FECRECEPDESDE DATETIME, @FECRECEPHASTA DATETIME, @VERPEDIDOSSINRECEP INT, @NUMALBARAN VARCHAR(100), @NUMPEDPROVE VARCHAR(100), @ARTICULO VARCHAR(200), @FECENTREGASOLICITDESDE DATETIME, @FECENTREGASOLICITHASTA DATETIME, @CODEMPRESA INT, @FECENTREGAINDICADAPROVEDESDE DATETIME, @FECENTREGAINDICADAPROVEHASTA DATETIME, @FECEMISIONDESDE DATETIME, @FECEMISIONHASTA DATETIME, @CENTRO VARCHAR(50), @PARTIDASPRES TablaPartidas READONLY, @FECRECEPHASTA_MAS1 DATETIME', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                       @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                       @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                       @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                       @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                       @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                       @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEDESDE =@FECENTREGAINDICADAPROVEDESDE, " & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEHASTA = @FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                       @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA_MAS1=@FECRECEPHASTA_MAS1" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_PARAM_LITERAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_PARAM_LITERAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSP_OBT_PARAM_LITERAL]  @IDLIT INT, @IDIOMA NVARCHAR(50) = 'SPA', @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @CIA IS NULL AND @CIA <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @FSGS IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING = 'SELECT DEN FROM ' + @FSGS + 'PARGEN_LIT WITH(NOLOCK) WHERE ID = @IDLIT AND IDI=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING, N'@IDLIT INT, @IDIOMA NVARCHAR(50)', @IDLIT=@IDLIT, @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSP_OBT_CONF_VISOR_RECEPCIONES]  @USU NVARCHAR(50) = NULL, @CIA INT = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU AS INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU = ID FROM USU WHERE CIA = @CIA AND COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT [RETRASADO_VISIBLE],[RETRASADO_WIDTH],[ALBARAN_VISIBLE],[ALBARAN_WIDTH],[FRECEP_VISIBLE],[FRECEP_WIDTH],[EMP_VISIBLE],[EMP_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[ERP_VISIBLE],[ERP_WIDTH],[PEDIDO_VISIBLE],[PEDIDO_WIDTH],[NUMPEDPROVE_VISIBLE],[NUMPEDPROVE_WIDTH],[FEMISION_VISIBLE],[FEMISION_WIDTH],[FENTREGA_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[FENTREGA_WIDTH],[FENTREGAPROVE_VISIBLE],[FENTREGAPROVE_WIDTH],[ART_VISIBLE],[ART_WIDTH],[CANTPED_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[CANTPED_WIDTH],[CANTREC_VISIBLE],[CANTREC_WIDTH],[CANTPEND_VISIBLE],[CANTPEND_WIDTH],[UNI_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[UNI_WIDTH],[PREC_VISIBLE],[PREC_WIDTH],[IMPPED_VISIBLE],[IMPPED_WIDTH],[IMPREC_VISIBLE],[IMPREC_WIDTH],[IMPPEND_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[IMPPEND_WIDTH],[MON_VISIBLE],[MON_WIDTH],[CC1_VISIBLE],[CC1_WIDTH],[CC2_VISIBLE],[CC2_WIDTH],[CC3_VISIBLE],[CC3_WIDTH]" & vbCrLf
sConsulta = sConsulta & ",[CC4_VISIBLE],[CC4_WIDTH],[PARTIDA1_VISIBLE],[PARTIDA1_WIDTH],[PARTIDA2_VISIBLE],[PARTIDA2_WIDTH],[PARTIDA3_VISIBLE]" & vbCrLf
sConsulta = sConsulta & ",[PARTIDA3_WIDTH],[PARTIDA4_VISIBLE],[PARTIDA4_WIDTH]" & vbCrLf
sConsulta = sConsulta & "FROM FSP_CONF_VISOR_RECEPCIONES WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE USU = @IDUSU AND CIA = @CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMNS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMNS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_GMNS_PROVE] @CIA INT, @PROVE INT,@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) = 'SPA' AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS = FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMNS_PROVE @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI, @PROVECOD=@PROVECOD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50), @PROVECOD NVARCHAR(100) ', @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI, @PROVECOD=@PROVECOD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ARTICULOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ARTICULOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE [dbo].[FSPM_ARTICULOS_PROVE] @CIA INT, @PROVE INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA', @INSTANCIAMONEDA VARCHAR(3)='EUR',@CARGAR_ULT_ADJ TINYINT=0 ,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL, @CARGAR_ORG TINYINT=0  AS   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS = FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS_PROVE @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT,@CARGAR_ORG=@CARGAR_ORG, @PROVECOD=@PROVECOD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL, @CARGAR_ORG TINYINT, @PROVECOD NVARCHAR(100)',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT,@CARGAR_ORG=@CARGAR_ORG, @PROVECOD=@PROVECOD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_03_15_07A31900_04_15_08() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_008
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_07A31900_04_15_08 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_07A31900_04_15_08 = False
End Function

Private Sub V_31900_5_Storeds_008()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_ITEM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_ITEM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_ITEM] (@ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @GRUPO VARCHAR(50), @ITEM INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ADJUN OA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_OFE P WITH(NOLOCK) ON OA.ID=P.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND OA.ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB OA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_OFE P WITH(NOLOCK) ON OA.ID=P.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND OA.ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "   FROM ITEM_OFE I WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_OFE P WITH(NOLOCK) ON I.ID=P.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.PROCE=@PROCE AND I.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPS_CAMBIAR_COD_GRUPO_OFERTAS] " & vbCrLf
sConsulta = sConsulta & "   @ANYO SMALLINT, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @PROCE SMALLINT," & vbCrLf
sConsulta = sConsulta & "   @COD_OLD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @COD_NEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @OFE_GR_ATRIB TABLE (ID INT,GRUPO VARCHAR(50),ATRIB_ID INT,VALOR_NUM FLOAT,VALOR_TEXT VARCHAR(800)," & vbCrLf
sConsulta = sConsulta & "                                VALOR_FEC DATETIME,VALOR_BOOL TINYINT,IMPPARCIAL FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   --Antes del borrado se hace una inserci�n en la var. tabla @OFE_GR_ATRIB porque GRUPO es una foreign key a GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO @OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ATRIB     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- GRUPO_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE GRUPO_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=GRUPO_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND GRUPO_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- ITEM_OFE ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE ITEM_OFE SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=ITEM_OFE.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND ITEM_OFE.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ADJUN ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,@COD_NEW,OGA.ADJUN,OGA.NOM,OGA.COM,OGA.IDPORTAL,OGA.IDGS,OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ADJUN     " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN OGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGA.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGA.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_ATRIB ---------- " & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.ID,OGA.GRUPO,OGA.ATRIB_ID,OGA.VALOR_NUM,OGA.VALOR_TEXT,OGA.VALOR_FEC,OGA.VALOR_BOOL,OGA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "   FROM @OFE_GR_ATRIB OGA      " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_GR_COSTESDESC ----------" & vbCrLf
sConsulta = sConsulta & "   --Se hace un borrado de los registros existentes y una inserci�n con el nuevo valor porque el grupo est� en la clave" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "   SELECT OGC.ID,@COD_NEW,OGC.ATRIB_ID,OGC.IMPPARCIAL,OGC.FECACT" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_COSTESDESC    " & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_COSTESDESC OGC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OGC.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OGC.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------- OFE_ITEM_ATRIB ----------" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ITEM_ATRIB SET GRUPO=@COD_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROCE_OFE PO WITH (NOLOCK) ON PO.ID=OFE_ITEM_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OFE_ITEM_ATRIB.GRUPO=@COD_OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31900_03_15_08A31900_04_15_09() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_009
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_08A31900_04_15_09 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_08A31900_04_15_09 = False
End Function

Private Sub V_31900_5_Storeds_009()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_GETADJUNTO_CONTRATO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO] @CIA INT, @ID INT =NULL, @ID_CONTRATO INT =NULL, @ID_CAMPO INT=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETADJUNTO_CONTRATO @ID=@ID,@ID_CONTRATO=@ID_CONTRATO,@ID_CAMPO=@ID_CAMPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID  INT, @ID_CONTRATO INT, @ID_CAMPO INT ', @ID=@ID,@ID_CONTRATO=@ID_CONTRATO,@ID_CAMPO=@ID_CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_PROVE_ESP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_PROVE_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_PROVE_ESP] " & vbCrLf
sConsulta = sConsulta & "   @IDPROVE int output, " & vbCrLf
sConsulta = sConsulta & "   @IDESP int = NULL, " & vbCrLf
sConsulta = sConsulta & "   @CODPROVE varchar(50) = null " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @IDPROVE is null" & vbCrLf
sConsulta = sConsulta & "   select @IDPROVE = ID FROM CIAS WITH (NOLOCK) WHERE COD = @CODPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDESP IS NULL" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact , datalength(data) TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact, datalength(data)  TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "   AND ID = @IDESP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @IDI AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CERTIFICADOS_PROVE @PROVE= @PROVE, @IDI = @IDI'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@IDI VARCHAR(3)',@PROVE= @PROVE, @IDI = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =118 AND ((W.ID>=9 AND W.ID<=13) OR W.ID=21) ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @IDI AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CERTIFICADOS_PROVE @PROVE= @PROVE, @IDI = @IDI'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@IDI VARCHAR(3)',@PROVE= @PROVE, @IDI = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =118 AND ((W.ID>=9 AND W.ID<=13) OR W.ID=21) ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_03_15_09A31900_04_15_10() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_010
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_09A31900_04_15_10 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_09A31900_04_15_10 = False
End Function

Private Sub V_31900_5_Storeds_010()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETORGANIZACIONESCOMPRAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETORGANIZACIONESCOMPRAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETORGANIZACIONESCOMPRAS] @CIA int AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las ORGANIZACIONES de COMPRA" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETORGANIZACIONESCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  @IDIOMA NVARCHAR(20)='SPA', @CIA NVARCHAR(100) = NULL, @PROVE NVARCHAR(100) = NULL, @ANYO INT = NULL, @NUMPEDIDO INT = NULL, @NUMORDEN INT = NULL, @NUMPEDIDOERP NVARCHAR(50) = NULL, @FECRECEPDESDE DATETIME = NULL, @FECRECEPHASTA DATETIME = NULL, @VERPEDIDOSSINRECEP BIT = 1, @NUMALBARAN VARCHAR(100) = NULL, @NUMPEDPROVE VARCHAR(100) = NULL, @ARTICULO VARCHAR(200) = NULL, @FECENTREGASOLICITDESDE DATETIME = NULL, @FECENTREGASOLICITHASTA DATETIME = NULL, @CODEMPRESA INT = NULL, @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL, @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL, @FECEMISIONDESDE DATETIME = NULL, @FECEMISIONHASTA DATETIME = NULL, @CENTRO VARCHAR(50) = NULL, @PARTIDASPRES TablaPartidas READONLY" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECRECEPHASTA_MAS1 DATETIME " & vbCrLf
sConsulta = sConsulta & "SET @FECRECEPHASTA_MAS1 = DATEADD(dd,1,@FECRECEPHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECENTREGASOLICITHASTA = DATEADD(dd,1,@FECENTREGASOLICITHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECENTREGAINDICADAPROVEHASTA = DATEADD(dd,1,@FECENTREGAINDICADAPROVEHASTA)" & vbCrLf
sConsulta = sConsulta & "SET @FECEMISIONHASTA = DATEADD(dd,1,@FECEMISIONHASTA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) -- BASE DE DATOS DEL CLIENTE" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLQUERY AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_SELECT01 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_SELECT02 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_FIN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_PARTIDA_COD AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_NIVEL_DEN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_CCCOD AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBSQLQUERY_CCDEN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLWHERE AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_SELECT01 = 'SELECT " & vbCrLf
sConsulta = sConsulta & "       LP.ID LINEAPEDID, LR.ID LINEARECEPID'   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_SELECT02 = '" & vbCrLf
sConsulta = sConsulta & "       ,PR.ALBARAN, " & vbCrLf
sConsulta = sConsulta & "       PR.FECHA FECHARECEP, " & vbCrLf
sConsulta = sConsulta & "       OE.EMPRESA IDEMPRESA, E.DEN DENEMPRESA, " & vbCrLf
sConsulta = sConsulta & "       OE.NUM_PED_ERP, " & vbCrLf
sConsulta = sConsulta & "       OE.ANYO, PE.NUM PEDIDONUM, OE.NUM ORDENNUM, " & vbCrLf
sConsulta = sConsulta & "       CAST(OE.ANYO as VARCHAR(20)) + CAST(''/'' as VARCHAR(3)) + CAST(PE.NUM as VARCHAR(100)) + CAST(''/'' as VARCHAR(3)) + CAST(OE.NUM as VARCHAR(100)) ANYOPEDIDOORDEN, " & vbCrLf
sConsulta = sConsulta & "       OE.ID ORDENID, OE.NUMEXT NUMPEDPROVE, " & vbCrLf
sConsulta = sConsulta & "       PE.FECHA FECHAEMISION," & vbCrLf
sConsulta = sConsulta & "       LP.FECENTREGAPROVE FECENTREGAINDICADAPROVE," & vbCrLf
sConsulta = sConsulta & "       LP.FECENTREGA FECENTREGASOLICITADA," & vbCrLf
sConsulta = sConsulta & "       I.ART ARTCOD, I.DESCR ARTDEN," & vbCrLf
sConsulta = sConsulta & "       LP.CANT_PED CANTLINEAPEDIDATOTAL," & vbCrLf
sConsulta = sConsulta & "       LR.CANT CANTLINEARECIBIDAPARCIAL," & vbCrLf
sConsulta = sConsulta & "       (LP.CANT_PED - SUMAPORLINEA.CANTRECIBIDATOTAL) CANTLINEAPENDIENTETOTAL," & vbCrLf
sConsulta = sConsulta & "       LP.UP UNICOD, UD.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "       LP.PREC_UP," & vbCrLf
sConsulta = sConsulta & "       LP.FC * LP.PREC_UP * LP.CANT_PED IMPORTELINEATOTAL," & vbCrLf
sConsulta = sConsulta & "       LP.FC * LP.PREC_UP * LR.CANT IMPORTELINEARECIBIDOPARCIAL," & vbCrLf
sConsulta = sConsulta & "       (LP.FC * LP.PREC_UP * LP.CANT_PED - (LP.FC * LP.PREC_UP * SUMAPORLINEA.CANTRECIBIDATOTAL)) IMPORTELINEAPENDIENTETOTAL," & vbCrLf
sConsulta = sConsulta & "       OE.MON MONCOD, MD.DEN MONDEN," & vbCrLf
sConsulta = sConsulta & "       OE.EST'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_PARTIDA_COD = '" & vbCrLf
sConsulta = sConsulta & "       ,''PARTIDA_COD_'' + LPI.PRES0 PRES0_PARTIDA_COD" & vbCrLf
sConsulta = sConsulta & "       ,COALESCE(LPI.PRES0,'''') + ''#'' + COALESCE(LPI.PRES1,'''') + ''#'' + COALESCE(LPI.PRES2,'''') + ''#'' + COALESCE(LPI.PRES3,'''') + ''#'' + COALESCE(LPI.PRES4,'''') PARTIDA_COD'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_NIVEL_DEN = '" & vbCrLf
sConsulta = sConsulta & "       ,''NIVEL_DEN_'' + LPI.PRES0 PRES0_NIVEL_DEN" & vbCrLf
sConsulta = sConsulta & "       ,CASE COALESCE (LPI.PRES4, LPI.PRES3, LPI.PRES2, LPI.PRES1)" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES2 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I2.DEN" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES3 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I3.DEN" & vbCrLf
sConsulta = sConsulta & "       WHEN LPI.PRES4 THEN" & vbCrLf
sConsulta = sConsulta & "           P5I4.DEN" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           P5I1.DEN" & vbCrLf
sConsulta = sConsulta & "       END NIVEL_DEN' " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_CCCOD = '" & vbCrLf
sConsulta = sConsulta & "       ,''CCCOD_'' + LPI.PRES0 PRES0_CCCOD" & vbCrLf
sConsulta = sConsulta & "       ,LPI_CSM.CENTRO_SM AS CCCOD' " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_CCDEN = '" & vbCrLf
sConsulta = sConsulta & "       ,''CCDEN_'' + LPI.PRES0 PRES0_CCDEN" & vbCrLf
sConsulta = sConsulta & "       ,COALESCE (LPI.UON4, LPI.UON3, LPI.UON2, LPI.UON1) + '' - '' + LPI_CSM.DEN AS CCDEN'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = ' " & vbCrLf
sConsulta = sConsulta & "   FROM ' + @FSGS + 'ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'PEDIDO PE WITH(NOLOCK) ON OE.PEDIDO = PE.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ' + @FSGS + 'LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LP.ID=LPI.LINEA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUMPARTIDAS AS INT = 0" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMPARTIDAS = COUNT(*) FROM @PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPARTIDAS > 0" & vbCrLf
sConsulta = sConsulta & "       SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN @PARTIDASPRES PRES" & vbCrLf
sConsulta = sConsulta & "       ON PRES.PRES0 = COALESCE(LPI.PRES0,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES1 = COALESCE(LPI.PRES1,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES2 = COALESCE(LPI.PRES2,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES3 = COALESCE(LPI.PRES3,'''')" & vbCrLf
sConsulta = sConsulta & "       AND PRES.PRES4 = COALESCE(LPI.PRES4,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN  + '" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'ITEM I WITH(NOLOCK) ON LP.ANYO = I.ANYO AND LP.GMN1 = I.GMN1_PROCE AND LP.PROCE = I.PROCE AND LP.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'LINEAS_RECEP LR WITH(NOLOCK) ON LR.LINEA = LP.ID AND LR.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PEDIDO_RECEP PR WITH(NOLOCK) ON PR.ID = LR.PEDIDO_RECEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'MON_DEN MD WITH (NOLOCK) ON MD.MON = OE.MON AND MD.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'UNI_DEN UD WITH (NOLOCK) ON UD.UNI = LP.UP AND UD.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'EMP E WITH(NOLOCK) ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT ORDEN, LINEA, SUM(CANT) CANTRECIBIDATOTAL " & vbCrLf
sConsulta = sConsulta & "               FROM ' + @FSGS + 'LINEAS_RECEP LR1 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ' + @FSGS + 'ORDEN_ENTREGA OE1 WITH(NOLOCK) ON OE1.ID = LR1.ORDEN " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ' + @FSGS + 'PEDIDO PE1 WITH(NOLOCK) ON PE1.ID = OE1.PEDIDO" & vbCrLf
sConsulta = sConsulta & "              GROUP BY ORDEN, LINEA, PROVE" & vbCrLf
sConsulta = sConsulta & "              HAVING OE1.PROVE = @PROVECOD" & vbCrLf
sConsulta = sConsulta & "             ) SUMAPORLINEA ON SUMAPORLINEA.ORDEN = LR.ORDEN AND SUMAPORLINEA.LINEA = LR.LINEA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "       SELECT CSM.CENTRO_SM, CSM.DEN, LPI.LINEA, LPI.UON1, LPI.UON2, LPI.UON3, LPI.UON4" & vbCrLf
sConsulta = sConsulta & "       FROM ' + @FSGS + 'LINEAS_PED_IMPUTACION LPI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN ' + @FSGS + 'CENTRO_SM_UON CSM_UON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON ISNULL(LPI.UON1,'''') = ISNULL(CSM_UON.UON1,'''') AND ISNULL(LPI.UON2,'''') = ISNULL(CSM_UON.UON2,'''') AND ISNULL(LPI.UON3,'''') = ISNULL(CSM_UON.UON3,'''') AND ISNULL(LPI.UON4,'''') = ISNULL(CSM_UON.UON4,'''')" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'CENTRO_SM_DEN CSM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           ON CSM.CENTRO_SM = CSM_UON.CENTRO_SM AND CSM.IDI = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   ) LPI_CSM ON LP.ID = LPI_CSM.LINEA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I0 WITH (NOLOCK) ON P5I0.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.PRES1 IS NULL AND P5I0.PRES2 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.PRES3 IS NULL AND P5I0.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I0.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I1 WITH (NOLOCK) ON P5I1.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.PRES1 = LPI.PRES1" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.PRES2 IS NULL AND P5I1.PRES3 IS NULL AND P5I1.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I1.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I2 WITH (NOLOCK) ON P5I2.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I2.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.PRES3 IS NULL AND P5I2.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I2.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I3 WITH (NOLOCK) ON P5I3.PRES0 = LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       AND P5I3.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I3.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND LPI.PRES3 = P5I3.PRES3 AND P5I3.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND P5I3.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ' + @FSGS + 'PRES5_IDIOMAS P5I4 WITH (NOLOCK) ON P5I4.PRES0 = LPI.PRES0 " & vbCrLf
sConsulta = sConsulta & "       AND P5I4.PRES1 = LPI.PRES1 AND LPI.PRES2 = P5I4.PRES2" & vbCrLf
sConsulta = sConsulta & "       AND LPI.PRES3 = P5I4.PRES3 AND LPI.PRES4 = P5I4.PRES4" & vbCrLf
sConsulta = sConsulta & "       AND P5I4.IDIOMA = @IDIOMA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = ' " & vbCrLf
sConsulta = sConsulta & "   WHERE" & vbCrLf
sConsulta = sConsulta & "       OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "   IF @ANYO<>0 AND @ANYO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.ANYO = @ANYO'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDIDO<>0 AND @NUMPEDIDO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.NUM = @NUMPEDIDO'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMORDEN<>0 AND @NUMORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUM = @NUMORDEN'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDIDOERP<>'' AND @NUMPEDIDOERP IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUM_PED_ERP = @NUMPEDIDOERP'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --  Restricciones de PEDIDO_RECEP   -----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   IF @VERPEDIDOSSINRECEP = 0 --SE MUESTRAN S�LO LOS PEDIDOS CON RECEPCIONES" & vbCrLf
sConsulta = sConsulta & "       SET @SQlWHERE = @SQLWHERE + ' AND PR.ID IS NOT NULL' " & vbCrLf
sConsulta = sConsulta & "   ELSE --SE MUESTRAN LOS PEDIDOS SIN RECEPCIONES CUYA FECHA DE ENTREGA SOLICITADA EST� COMPRENDIDA ENTRE las fechas FECRECEPDESDE y FECRECEPHASTA" & vbCrLf
sConsulta = sConsulta & "        --y SE MUESTRAN LOS PEDIDOS CON RECEPCIONES QUE CUMPLAN LAS RESTRICCIONES DE FECHA Y ALBAR�N INDICADAS" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQlWHERE = @SQLWHERE + ' " & vbCrLf
sConsulta = sConsulta & "       AND (" & vbCrLf
sConsulta = sConsulta & "            (PR.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "           --LP.FECENTREGA ES UN CAMPO OPCIONAL POR LO QUE PUEDE SER NULO. VAMOS A MOSTRAR LOS VALORES QUE SEAN NULOS." & vbCrLf
sConsulta = sConsulta & "           IF @FECRECEPDESDE<>'' AND @FECRECEPDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "               SET @SQLWHERE = @SQLWHERE + ' AND COALESCE(LP.FECENTREGA,@FECRECEPDESDE) >= @FECRECEPDESDE'" & vbCrLf
sConsulta = sConsulta & "           IF @FECRECEPHASTA<>'' AND @FECRECEPHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "               SET @SQLWHERE = @SQLWHERE + ' AND COALESCE(LP.FECENTREGA,@FECRECEPHASTA) < @FECRECEPHASTA_MAS1'" & vbCrLf
sConsulta = sConsulta & "           SET @SQLWHERE = @SQLWHERE + ')" & vbCrLf
sConsulta = sConsulta & "            OR" & vbCrLf
sConsulta = sConsulta & "            (NOT PR.ID IS NULL' " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF @FECRECEPDESDE<>'' AND @FECRECEPDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.FECHA >= @FECRECEPDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECRECEPHASTA<>'' AND @FECRECEPHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.FECHA < @FECRECEPHASTA_MAS1'" & vbCrLf
sConsulta = sConsulta & "   IF @NUMALBARAN<>'' AND @NUMALBARAN IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PR.ALBARAN = @NUMALBARAN'" & vbCrLf
sConsulta = sConsulta & "   IF @VERPEDIDOSSINRECEP = 1 --CERRAMOS EL PAR�NTESIS QUE HAB�AMOS ABIERTO" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' )" & vbCrLf
sConsulta = sConsulta & "           )'" & vbCrLf
sConsulta = sConsulta & "   --  FIN Restricciones de PEDIDO_RECEP   -----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @NUMPEDPROVE<>'' AND @NUMPEDPROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.NUMEXT = @NUMPEDPROVE'" & vbCrLf
sConsulta = sConsulta & "   IF @ARTICULO<>'' AND @ARTICULO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND (I.ART LIKE ''%'' + @ARTICULO + ''%''  OR I.DESCR LIKE ''%'' + @ARTICULO + ''%'')'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGASOLICITDESDE<>'' AND @FECENTREGASOLICITDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGA >= @FECENTREGASOLICITDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGASOLICITHASTA<>'' AND @FECENTREGASOLICITHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGA < @FECENTREGASOLICITHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @CODEMPRESA<>'' AND @CODEMPRESA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND OE.EMPRESA = @CODEMPRESA'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGAINDICADAPROVEDESDE<>'' AND @FECENTREGAINDICADAPROVEDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGAPROVE >= @FECENTREGAINDICADAPROVEDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECENTREGAINDICADAPROVEHASTA<>'' AND @FECENTREGAINDICADAPROVEHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LP.FECENTREGAPROVE < @FECENTREGAINDICADAPROVEHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @FECEMISIONDESDE<>'' AND @FECEMISIONDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.FECHA >= @FECEMISIONDESDE'" & vbCrLf
sConsulta = sConsulta & "   IF @FECEMISIONHASTA<>'' AND @FECEMISIONHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND PE.FECHA < @FECEMISIONHASTA'" & vbCrLf
sConsulta = sConsulta & "   IF @CENTRO<>'' AND @CENTRO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND LPI_CSM.CENTRO_SM = @CENTRO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY_FIN = @SUBSQLQUERY_FIN + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Vamos a obtener todas las partidas presupuestarias de nivel 0 para poder usarlas luego" & vbCrLf
sConsulta = sConsulta & "   --como columnas al mostrar datos de partidas y centros de coste para cada recepci�n/entrega" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PRES0 NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #PRES5(" & vbCrLf
sConsulta = sConsulta & "       COD VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PRES0 = 'INSERT INTO #PRES5 SELECT COD FROM ' + @FSGS + 'PRES5_NIV0 WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXEC(@PRES0)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @PRES0  = STUFF(( " & vbCrLf
sConsulta = sConsulta & "                       SELECT DISTINCT '],[' + ltrim(#PRES5.COD)" & vbCrLf
sConsulta = sConsulta & "                           FROM #PRES5" & vbCrLf
sConsulta = sConsulta & "                           ORDER BY '],[' + ltrim(#PRES5.COD)" & vbCrLf
sConsulta = sConsulta & "                           FOR XML PATH('')" & vbCrLf
sConsulta = sConsulta & "                       ), 1, 2, '') + ']'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DROP TABLE #PRES5" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Esta select * no llama a una tabla sino a una consulta, por lo que devuelve " & vbCrLf
sConsulta = sConsulta & "   --todos los registros de la consulta, no de la tabla" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = '" & vbCrLf
sConsulta = sConsulta & "   SELECT * FROM ('" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ----DENOMINACION DE LA PARTIDA PRESUPUESTARIA" & vbCrLf
sConsulta = sConsulta & "   SET  @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_SELECT02 + @SUBSQLQUERY_NIVEL_DEN + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + @SUBSQLQUERY" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC1" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(NIVEL_DEN) FOR PRES0_NIVEL_DEN" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[NIVEL_DEN_') + ')) AS PVT1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --CODIGO UON COMPUESTO DE LA PARTIDA (UON1+UON2+UON3+UON4)" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_PARTIDA_COD + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC3" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(PARTIDA_COD) FOR PRES0_PARTIDA_COD" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[PARTIDA_COD_') + ')) AS PVT3" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT3.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT3.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --COD CENTRO DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_CCCOD + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC5" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(CCCOD) FOR PRES0_CCCOD" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[CCCOD_') + ')) AS PVT5" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT5.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT5.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --UON + DEN CENTRO DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SUBSQLQUERY = @SUBSQLQUERY_SELECT01 + @SUBSQLQUERY_CCDEN + @SUBSQLQUERY_FIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  (' + @SUBSQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ) SRC6" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(CCDEN) FOR PRES0_CCDEN" & vbCrLf
sConsulta = sConsulta & "   IN (' + REPLACE(@PRES0,'[','[CCDEN_') + ')) AS PVT6" & vbCrLf
sConsulta = sConsulta & "       ON PVT1.LINEAPEDID = PVT6.LINEAPEDID" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(PVT1.LINEARECEPID,'''') = ISNULL(PVT6.LINEARECEPID,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --ORDER BY" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = @SQLQUERY + '" & vbCrLf
sConsulta = sConsulta & "   ORDER BY FECHARECEP DESC'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20), @PROVECOD NVARCHAR(100), @ANYO INT, @NUMPEDIDO INT, @NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50), @FECRECEPDESDE DATETIME, @FECRECEPHASTA DATETIME, @VERPEDIDOSSINRECEP INT, @NUMALBARAN VARCHAR(100), @NUMPEDPROVE VARCHAR(100), @ARTICULO VARCHAR(200), @FECENTREGASOLICITDESDE DATETIME, @FECENTREGASOLICITHASTA DATETIME, @CODEMPRESA INT, @FECENTREGAINDICADAPROVEDESDE DATETIME, @FECENTREGAINDICADAPROVEHASTA DATETIME, @FECEMISIONDESDE DATETIME, @FECEMISIONHASTA DATETIME, @CENTRO VARCHAR(50), @PARTIDASPRES TablaPartidas READONLY, @FECRECEPHASTA_MAS1 DATETIME', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                       @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                       @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                       @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                       @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                       @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                       @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEDESDE =@FECENTREGAINDICADAPROVEDESDE, " & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEHASTA = @FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                       @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA_MAS1=@FECRECEPHASTA_MAS1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "   --TABLA CON LOS NOMBRES DE LAS PARTIDAS" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = 'SELECT * FROM(SELECT P5.COD PRES0," & vbCrLf
sConsulta = sConsulta & "       CASE PSM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "           WHEN 2 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL2,'''')" & vbCrLf
sConsulta = sConsulta & "           WHEN 3 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL3,'''')" & vbCrLf
sConsulta = sConsulta & "           WHEN 4 THEN" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL4,'''')" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               ISNULL(P5I0.NOM_NIVEL1,'''')" & vbCrLf
sConsulta = sConsulta & "           END PARTIDA_DEN" & vbCrLf
sConsulta = sConsulta & "   FROM  ' + @FSGS + 'PRES5_NIV0 P5 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN  ' + @FSGS + 'PARGEN_SM PSM WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON P5.COD = PSM.PRES5" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN  ' + @FSGS + 'PRES5_IDIOMAS P5I0 WITH (NOLOCK) ON P5I0.PRES0 = P5.COD" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.PRES1 IS NULL AND P5I0.PRES2 IS NULL" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.PRES3 IS NULL AND P5I0.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   AND P5I0.IDIOMA = @IDIOMA)SRC2" & vbCrLf
sConsulta = sConsulta & "   PIVOT (MAX(PARTIDA_DEN) FOR PRES0" & vbCrLf
sConsulta = sConsulta & "   IN (' + @PRES0 + ')) AS PVT2'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20)', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --TABLA CON LOS CENTROS DE COSTE" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = 'SELECT DISTINCT" & vbCrLf
sConsulta = sConsulta & "       LPI_CSM.CENTRO_SM AS CCCOD, " & vbCrLf
sConsulta = sConsulta & "       COALESCE (LPI.UON4, LPI.UON3, LPI.UON2, LPI.UON1) + '' - '' + LPI_CSM.DEN AS CCDEN'" & vbCrLf
sConsulta = sConsulta & "       + @SUBSQLQUERY_FIN +" & vbCrLf
sConsulta = sConsulta & "       ' AND NOT LPI_CSM.CENTRO_SM IS NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY, " & vbCrLf
sConsulta = sConsulta & "                       N'@IDIOMA NVARCHAR(20), @PROVECOD NVARCHAR(100), @ANYO INT, @NUMPEDIDO INT, @NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50), @FECRECEPDESDE DATETIME, @FECRECEPHASTA DATETIME, @VERPEDIDOSSINRECEP INT, @NUMALBARAN VARCHAR(100), @NUMPEDPROVE VARCHAR(100), @ARTICULO VARCHAR(200), @FECENTREGASOLICITDESDE DATETIME, @FECENTREGASOLICITHASTA DATETIME, @CODEMPRESA INT, @FECENTREGAINDICADAPROVEDESDE DATETIME, @FECENTREGAINDICADAPROVEHASTA DATETIME, @FECEMISIONDESDE DATETIME, @FECEMISIONHASTA DATETIME, @CENTRO VARCHAR(50), @PARTIDASPRES TablaPartidas READONLY, @FECRECEPHASTA_MAS1 DATETIME', " & vbCrLf
sConsulta = sConsulta & "                       @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                       @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                       @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                       @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                       @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                       @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                       @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                       @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEDESDE =@FECENTREGAINDICADAPROVEDESDE, " & vbCrLf
sConsulta = sConsulta & "                       @FECENTREGAINDICADAPROVEHASTA = @FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                       @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                       @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                       @PARTIDASPRES=@PARTIDASPRES," & vbCrLf
sConsulta = sConsulta & "                       @FECRECEPHASTA_MAS1=@FECRECEPHASTA_MAS1" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_03_15_10A31900_04_15_11() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_011
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Tablas_011
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_10A31900_04_15_11 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_10A31900_04_15_11 = False
End Function

Sub V_31900_5_Tablas_011()
Dim sConsulta As String

sConsulta = sConsulta & "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DevuelvePartidas]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))" & vbCrLf
sConsulta = sConsulta & "DROP FUNCTION [dbo].[DevuelvePartidas]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'TablaPartidas' AND ss.name = N'dbo')" & vbCrLf
sConsulta = sConsulta & "DROP TYPE TablaPartidas" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_5_Storeds_011()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_OBT_TODAS_ENTREGASPEDIDOS]  " & vbCrLf
sConsulta = sConsulta & "   @IDIOMA NVARCHAR(20)='SPA'," & vbCrLf
sConsulta = sConsulta & "   @CIA NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDO INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMORDEN INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDIDOERP NVARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECRECEPHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @VERPEDIDOSSINRECEP BIT = 1," & vbCrLf
sConsulta = sConsulta & "   @NUMALBARAN VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @NUMPEDPROVE VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "   @ARTICULO VARCHAR(200) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGASOLICITHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CODEMPRESA INT = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECENTREGAINDICADAPROVEHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONDESDE DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @FECEMISIONHASTA DATETIME = NULL," & vbCrLf
sConsulta = sConsulta & "   @CENTRO VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES NVARCHAR(MAX) = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PROVE IS NULL AND @PROVE <> 0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @FSGS NVARCHAR(100) " & vbCrLf
sConsulta = sConsulta & "   SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "   SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'EP_OBT_TODAS_ENTREGASPEDIDOS @IDIOMA=@IDIOMA,@PROVECOD=@PROVECOD,@ANYO=@ANYO, @NUMPEDIDO=@NUMPEDIDO,@NUMORDEN=@NUMORDEN,@NUMPEDIDOERP=@NUMPEDIDOERP,@FECRECEPDESDE=@FECRECEPDESDE,@FECRECEPHASTA=@FECRECEPHASTA,@VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP,@NUMALBARAN=@NUMALBARAN,@NUMPEDPROVE=@NUMPEDPROVE,@ARTICULO=@ARTICULO,@FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE,@FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA,@CODEMPRESA=@CODEMPRESA,@FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE,@FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA,@FECEMISIONDESDE=@FECEMISIONDESDE,@FECEMISIONHASTA=@FECEMISIONHASTA,@CENTRO=@CENTRO,@PARTIDASPRES=@PARTIDASPRES'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDIOMA VARCHAR(50),@PROVECOD NVARCHAR(100),@ANYO INT, @NUMPEDIDO INT,@NUMORDEN INT, @NUMPEDIDOERP NVARCHAR(50),@FECRECEPDESDE DATETIME,@FECRECEPHASTA DATETIME,@VERPEDIDOSSINRECEP BIT,@NUMALBARAN VARCHAR(100),@NUMPEDPROVE VARCHAR(100),@ARTICULO VARCHAR(200),@FECENTREGASOLICITDESDE DATETIME,@FECENTREGASOLICITHASTA DATETIME,@CODEMPRESA INT,@FECENTREGAINDICADAPROVEDESDE DATETIME,@FECENTREGAINDICADAPROVEHASTA DATETIME,@FECEMISIONDESDE DATETIME,@FECEMISIONHASTA DATETIME,@CENTRO VARCHAR(50), @PARTIDASPRES NVARCHAR(MAX)', " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   @IDIOMA=@IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   @PROVECOD=@PROVECOD," & vbCrLf
sConsulta = sConsulta & "                   @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDO=@NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "                   @NUMORDEN=@NUMORDEN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDIDOERP=@NUMPEDIDOERP," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPDESDE=@FECRECEPDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECRECEPHASTA=@FECRECEPHASTA," & vbCrLf
sConsulta = sConsulta & "                   @VERPEDIDOSSINRECEP=@VERPEDIDOSSINRECEP," & vbCrLf
sConsulta = sConsulta & "                   @NUMALBARAN=@NUMALBARAN," & vbCrLf
sConsulta = sConsulta & "                   @NUMPEDPROVE=@NUMPEDPROVE," & vbCrLf
sConsulta = sConsulta & "                   @ARTICULO=@ARTICULO," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITDESDE=@FECENTREGASOLICITDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGASOLICITHASTA=@FECENTREGASOLICITHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CODEMPRESA=@CODEMPRESA," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEDESDE=@FECENTREGAINDICADAPROVEDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECENTREGAINDICADAPROVEHASTA=@FECENTREGAINDICADAPROVEHASTA," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONDESDE=@FECEMISIONDESDE," & vbCrLf
sConsulta = sConsulta & "                   @FECEMISIONHASTA=@FECEMISIONHASTA," & vbCrLf
sConsulta = sConsulta & "                   @CENTRO=@CENTRO," & vbCrLf
sConsulta = sConsulta & "                   @PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_03_15_11A31900_04_15_12() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_5_Storeds_012
    '-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.15.12'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_03_15_11A31900_04_15_12 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_03_15_11A31900_04_15_12 = False
End Function

Private Sub V_31900_5_Storeds_012()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS] " & vbCrLf
sConsulta = sConsulta & "   @CIA INT, " & vbCrLf
sConsulta = sConsulta & "   @ID INT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINVISIBLEFIELDS @ID=@ID,@SOLIC_PROVE=2 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub
