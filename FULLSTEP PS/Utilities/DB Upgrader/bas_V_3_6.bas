Attribute VB_Name = "bas_V_3_6"
Public Function CodigoDeActualizacion32200_01_30_00_A32200_01_30_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_6_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.30.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32200_01_30_00_A32200_01_30_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32200_01_30_00_A32200_01_30_01 = False
End Function

Private Sub V_3_6_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PROV_REINTENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_PROV_REINTENTOS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR,@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(1000),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(100),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(100),@CIAS_PROVI VARCHAR(100),@CIAS_CP VARCHAR(100),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(50),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100),@PORT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_NIF VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@ESPEC INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CICLOS= (SELECT PROV_CICLOS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "SET @REINTENTOS = (SELECT PROV_REINTENTOS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = (SELECT PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_P INT,@IDI VARCHAR(50),@TIPOEMAIL SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1), @DECIMALFMT CHAR(1), @PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CICLOS <> 0 AND @PASADAS%@CICLOS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA," & vbCrLf
sConsulta = sConsulta & "MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "FROM MVTOS_PROV_EST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "WHERE MVTOS_PROV_EST.EST = 1 AND MVTOS_PROV_EST.INTENTOS < @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- OBTENEMOS LOS DATOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@USU'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= '@COD_PROVE_CIA VARCHAR(200),@ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='I'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT FPEST FROM USU_PORT WITH(NOLOCK) WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT, @USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "FROM USU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI AS IDI WITH(NOLOCK) ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "WHERE USU.CIA = @CIA_PROVE AND USU.ID= @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                              @USU_NIF=@USU_NIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ID_P INT," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT TINYINT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                          @USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='U'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT FPEST FROM USU_PORT WITH(NOLOCK) WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "FROM USU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI AS IDI WITH(NOLOCK) ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "WHERE  USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ID_P INT," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT TINYINT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                             @USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                                     @USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT," & vbCrLf
sConsulta = sConsulta & "@DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "@COM =@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @INTENTO = @INTENTO +1" & vbCrLf
sConsulta = sConsulta & "IF @INTENTO = @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 2" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_GEST SET PROV_PASADAS = @PASADAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3001_A3_3_3602() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_6_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.36.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion3_3_3001_A3_3_3602 = True
    Exit Function
    
Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3001_A3_3_3602 = False
End Function

Private Sub V_3_6_Storeds_002()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PROV_REINTENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_PROV_REINTENTOS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR,@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(1000),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(100),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(100),@CIAS_PROVI VARCHAR(100),@CIAS_CP VARCHAR(100),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(50),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100),@PORT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_NIF VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@ESPEC INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CICLOS= (SELECT PROV_CICLOS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "SET @REINTENTOS = (SELECT PROV_REINTENTOS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = (SELECT PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_P INT,@IDI VARCHAR(50),@TIPOEMAIL SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1), @DECIMALFMT CHAR(1), @PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CICLOS <> 0 AND @PASADAS%@CICLOS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   --La tabla MVTOS_PROV_EST va sin WITH (NOLOCK) porque al final se hace un UPDATE sobre ella y con WITH (NOLOCK) genera el error:" & vbCrLf
sConsulta = sConsulta & "   --Cursor updates are not allowed on tables opened with then NOLOCK option" & vbCrLf
sConsulta = sConsulta & "SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA," & vbCrLf
sConsulta = sConsulta & "MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "FROM MVTOS_PROV_EST" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "WHERE MVTOS_PROV_EST.EST = 1 AND MVTOS_PROV_EST.INTENTOS < @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- OBTENEMOS LOS DATOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@USU'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= '@COD_PROVE_CIA VARCHAR(200),@ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='I'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT FPEST FROM USU_PORT WITH(NOLOCK) WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT, @USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "FROM USU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI AS IDI WITH(NOLOCK) ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "WHERE USU.CIA = @CIA_PROVE AND USU.ID= @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                              @USU_NIF=@USU_NIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ID_P INT," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT TINYINT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                          @USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='U'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT FPEST FROM USU_PORT WITH(NOLOCK) WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=THOUSANFMT, @DECIMALFMT=DECIMALFMT, @PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "FROM USU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI AS IDI WITH(NOLOCK) ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "WHERE  USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ID_P INT," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL SMALLINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT CHAR(1)," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT TINYINT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                             @USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "@TIPOEMAIL=@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "                                     @USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT," & vbCrLf
sConsulta = sConsulta & "@DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "@COM =@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @INTENTO = @INTENTO +1" & vbCrLf
sConsulta = sConsulta & "IF @INTENTO = @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 2" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_GEST SET PROV_PASADAS = @PASADAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3602_A3_3_3603() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_6_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.36.03'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion3_3_3602_A3_3_3603 = True
    Exit Function
    
Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3602_A3_3_3603 = False
End Function

Private Sub V_3_6_Storeds_003()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMNS_PROVE_PORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMNS_PROVE_PORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GMNS_PROVE_PORTAL]" & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN2 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN3 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN4 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50) = 'SPA'" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT COD, COD + '' - '' + DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR COD=@GMN1)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD, ACT2.COD + '' - '' + ACT2.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT2.ACT1" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD GMN2, ACT3.COD, ACT3.COD + '' - '' + ACT3.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT2 WITH(NOLOCK) ON ACT2.ID=ACT3.ACT2 AND ACT2.ACT1=ACT3.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT3.ACT1" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2) AND (@GMN3 IS NULL OR ACT3.COD=@GMN3)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD GMN2, ACT3.COD GMN3, ACT4.COD, ACT4.COD + '' - '' + ACT4.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT3 WITH(NOLOCK) ON ACT3.ID=ACT4.ACT3 AND ACT3.ACT2=ACT4.ACT2 AND ACT3.ACT1=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT2 WITH(NOLOCK) ON ACT2.ID=ACT3.ACT2 AND ACT2.ACT1=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2) AND (@GMN3 IS NULL OR ACT3.COD=@GMN3) AND (@GMN4 IS NULL OR ACT4.COD=@GMN4)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3603_A3_3_3604() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_6_Datos_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.36.04'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion3_3_3603_A3_3_3604 = True
    Exit Function
    
Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3603_A3_3_3604 = False
End Function

Private Sub V_3_6_Datos_004()
Dim sConsulta As String

sConsulta = "IF NOT EXISTS(SELECT ID FROM WEBTEXT WHERE MODULO=124 AND ID=10)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO WEBTEXT SELECT 124,10, 'PORTAL','PORTAL','PORTAL','PORTAL'" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion3_3_3604_A3_3_3610() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_6_Tablas_010
    
    frmProgreso.lblDetalle = "Cambios en triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_6_Triggers_010
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_6_Storeds_010
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.36.10'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion3_3_3604_A3_3_3610 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion3_3_3604_A3_3_3610 = False
End Function

Private Sub V_3_6_Tablas_010()
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[CONDICIONES_ACEPTACION_PROVE](" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CONDICIONES_ACEPTACION_PROVE] PRIMARY KEY CLUSTERED" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [ID] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO](" & vbCrLf
sConsulta = sConsulta & "   [CONDICION] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [IDI] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [TEXTO] [ntext] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CONDICIONES_ACEPTACION_PROVE_TEXTO] PRIMARY KEY CLUSTERED" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [CONDICION] ASC," & vbCrLf
sConsulta = sConsulta & "   [IDI] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO]  WITH CHECK ADD  CONSTRAINT [FK_CONDICIONES_ACEPTACION_PROVE_TEXTO_CONDICIONES_ACEPTACION_PROVE] FOREIGN KEY([CONDICION])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CONDICIONES_ACEPTACION_PROVE] ([ID])" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO] CHECK CONSTRAINT [FK_CONDICIONES_ACEPTACION_PROVE_TEXTO_CONDICIONES_ACEPTACION_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO]  WITH CHECK ADD  CONSTRAINT [FK_CONDICIONES_ACEPTACION_PROVE_TEXTO_IDI] FOREIGN KEY([IDI])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[IDI] ([ID])" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO] CHECK CONSTRAINT [FK_CONDICIONES_ACEPTACION_PROVE_TEXTO_IDI]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'CONDICIONES_ACEPTACION_PROVE' AND Object_ID = Object_ID(N'USU'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU ADD CONDICIONES_ACEPTACION_PROVE INT NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'FEC_ACEP_CONDICIONES' AND Object_ID = Object_ID(N'USU'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU ADD FEC_ACEP_CONDICIONES DATETIME NULL"
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[USU]  WITH CHECK ADD  CONSTRAINT [FK_USU_CONDICIONES_ACEPTACION_PROVE] " & vbCrLf
sConsulta = sConsulta & "FOREIGN KEY([CONDICIONES_ACEPTACION_PROVE]) REFERENCES [dbo].[CONDICIONES_ACEPTACION_PROVE]([ID])"
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_3_6_Triggers_010()
Dim sConsulta As String
sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[CONDICIONES_ACEPTACION_PROVE_TG_INSUPD]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[CONDICIONES_ACEPTACION_PROVE_TG_INSUPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[CONDICIONES_ACEPTACION_PROVE_TG_INSUPD] ON [dbo].[CONDICIONES_ACEPTACION_PROVE]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE CONDICIONES_ACEPTACION_PROVE SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "FROM CONDICIONES_ACEPTACION_PROVE CAP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSERTED I ON CAP.ID=I.ID" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[CONDICIONES_ACEPTACION_PROVE] ENABLE TRIGGER [CONDICIONES_ACEPTACION_PROVE_TG_INSUPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sys.triggers where object_id = OBJECT_ID(N'[dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO_TG_INSUPD]'))" & vbCrLf
sConsulta = sConsulta & "drop TRIGGER [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO_TG_INSUPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO_TG_INSUPD] ON [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE CONDICIONES_ACEPTACION_PROVE_TEXTO SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "FROM CONDICIONES_ACEPTACION_PROVE_TEXTO CAPT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSERTED I ON CAPT.CONDICION=I.CONDICION AND CAPT.IDI=I.IDI" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[CONDICIONES_ACEPTACION_PROVE_TEXTO] ENABLE TRIGGER [CONDICIONES_ACEPTACION_PROVE_TEXTO_TG_INSUPD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_3_6_Storeds_010()
Dim sConsulta As String
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_ULT_VERSION_CONDICIONES_ACEPTACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_ULT_VERSION_CONDICIONES_ACEPTACION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_ULT_VERSION_CONDICIONES_ACEPTACION]" & vbCrLf
sConsulta = sConsulta & "   @CIA_COD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @USU_COD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @IDIOMA VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON  " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDI INT" & vbCrLf
sConsulta = sConsulta & "   SELECT @IDI=U.IDI" & vbCrLf
sConsulta = sConsulta & "   FROM USU U WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CIAS C WITH (NOLOCK) ON C.ID=U.CIA" & vbCrLf
sConsulta = sConsulta & "   WHERE U.COD=@USU_COD AND C.COD=@CIA_COD " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAP INT" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1 @CAP=CAP.ID" & vbCrLf
sConsulta = sConsulta & "   FROM CONDICIONES_ACEPTACION_PROVE CAP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ORDER BY CAP.ID DESC    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF EXISTS(SELECT 1 FROM CONDICIONES_ACEPTACION_PROVE_TEXTO CAPT WITH (NOLOCK) WHERE CAPT.CONDICION=@CAP AND CAPT.IDI=@IDI)  " & vbCrLf
sConsulta = sConsulta & "       SELECT CAPT.CONDICION ID,CAPT.TEXTO" & vbCrLf
sConsulta = sConsulta & "       FROM CONDICIONES_ACEPTACION_PROVE_TEXTO CAPT WITH (NOLOCK)                      " & vbCrLf
sConsulta = sConsulta & "       WHERE CAPT.CONDICION=@CAP AND CAPT.IDI=@IDI                 " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SELECT CAPT.CONDICION ID,CAPT.TEXTO" & vbCrLf
sConsulta = sConsulta & "       FROM CONDICIONES_ACEPTACION_PROVE_TEXTO CAPT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IDI I WITH (NOLOCK) ON I.ID=CAPT.IDI" & vbCrLf
sConsulta = sConsulta & "       WHERE CAPT.CONDICION=@CAP AND I.COD=@IDIOMA                 " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_ACTUALIZAR_CONDICIONES_ACEPTACION_USU]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_ACTUALIZAR_CONDICIONES_ACEPTACION_USU]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_ACTUALIZAR_CONDICIONES_ACEPTACION_USU]" & vbCrLf
sConsulta = sConsulta & "   @CIA_COD VARCHAR(50) ," & vbCrLf
sConsulta = sConsulta & "   @USU_COD VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @COND INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   UPDATE USU SET CONDICIONES_ACEPTACION_PROVE=@COND,FEC_ACEP_CONDICIONES=GETDATE()" & vbCrLf
sConsulta = sConsulta & "   FROM USU U WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CIAS C WITH (NOLOCK) ON C.ID=U.CIA" & vbCrLf
sConsulta = sConsulta & "   WHERE U.COD=@USU_COD AND C.COD=@CIA_COD " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3610_A3_3_3611() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
  
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_3_6_Storeds_011
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.36.11'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion3_3_3610_A3_3_3611 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion3_3_3610_A3_3_3611 = False
End Function

Private Sub V_3_6_Storeds_011()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMNS_PROVE_PORTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMNS_PROVE_PORTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GMNS_PROVE_PORTAL]" & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN2 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN3 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @GMN4 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50) = 'SPA'," & vbCrLf
sConsulta = sConsulta & "   @NIVEL_SELECCION TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT COD, COD + '' - '' + DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "   FROM ACT1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 AND (@GMN1 IS NULL OR COD=@GMN1)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL_SELECCION >=2 OR @NIVEL_SELECCION = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD, ACT2.COD + '' - '' + ACT2.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "       FROM ACT2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT2.ACT1" & vbCrLf
sConsulta = sConsulta & "       WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2)'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL_SELECCION >=3 OR @NIVEL_SELECCION = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD GMN2, ACT3.COD, ACT3.COD + '' - '' + ACT3.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "       FROM ACT3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ACT2 WITH(NOLOCK) ON ACT2.ID=ACT3.ACT2 AND ACT2.ACT1=ACT3.ACT1" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT3.ACT1" & vbCrLf
sConsulta = sConsulta & "       WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2) AND (@GMN3 IS NULL OR ACT3.COD=@GMN3)'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL_SELECCION >=4 OR @NIVEL_SELECCION = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT ACT1.COD GMN1, ACT2.COD GMN2, ACT3.COD GMN3, ACT4.COD, ACT4.COD + '' - '' + ACT4.DEN_' + @IDI + ' DEN_' + @IDI + '" & vbCrLf
sConsulta = sConsulta & "       FROM ACT4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ACT3 WITH(NOLOCK) ON ACT3.ID=ACT4.ACT3 AND ACT3.ACT2=ACT4.ACT2 AND ACT3.ACT1=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ACT2 WITH(NOLOCK) ON ACT2.ID=ACT3.ACT2 AND ACT2.ACT1=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ACT1 WITH(NOLOCK) ON ACT1.ID=ACT4.ACT1" & vbCrLf
sConsulta = sConsulta & "       WHERE 1=1 AND (@GMN1 IS NULL OR ACT1.COD=@GMN1) AND (@GMN2 IS NULL OR ACT2.COD=@GMN2) AND (@GMN3 IS NULL OR ACT3.COD=@GMN3) AND (@GMN4 IS NULL OR ACT4.COD=@GMN4)'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50), @GMN2 VARCHAR(50), @GMN3 VARCHAR(50), @GMN4 VARCHAR(50)', @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GMNS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GMNS_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GMNS_PROVE] @CIA INT, @PROVE INT,@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50) = 'SPA',@NIVEL_SELECCION TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVECOD NVARCHAR(100) -- CODIGO DEL PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVECOD = COD FROM CIAS WITH (NOLOCK) WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS = FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GMNS_PROVE @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI, @PROVECOD=@PROVECOD,@NIVEL_SELECCION=@NIVEL_SELECCION '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@GMN1 VARCHAR(50)=NULL, @GMN2 VARCHAR(50)=NULL, @GMN3 VARCHAR(50)=NULL, @GMN4 VARCHAR(50)=NULL, @IDI VARCHAR(50), @PROVECOD NVARCHAR(100),@NIVEL_SELECCION TINYINT ', @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @IDI =@IDI, @PROVECOD=@PROVECOD,@NIVEL_SELECCION=@NIVEL_SELECCION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
End Sub
