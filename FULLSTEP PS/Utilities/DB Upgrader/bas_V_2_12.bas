Attribute VB_Name = "bas_V_2_12"
Option Explicit


Public Function CodigoDeActualizacion2_11_50_05A2_12_00_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_11_50_05A2_12_00_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_05A2_12_00_00 = False
End Function

Public Function CodigoDeActualizacion2_12_00_00A2_12_00_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    frmProgreso.lblDetalle = "Se van a a�adir campos de base de datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
    V_2_12_Tablas_01
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_00_00A2_12_00_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_00_00A2_12_00_01 = False
End Function

Public Function CodigoDeActualizacion2_12_00_01A2_12_00_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Se van a a�adir campos de base de datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
    V_2_12_Tablas_02
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_00_01A2_12_00_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_00_01A2_12_00_02 = False
End Function


Public Function CodigoDeActualizacion2_12_00_02A2_12_00_03()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Se van a a�adir campos de base de datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    V_2_12_Tablas_03
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.00.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_00_02A2_12_00_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_00_02A2_12_00_03 = False
End Function


Public Function CodigoDeActualizacion2_12_00_03A2_12_00_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Se van a a�adir campos de base de datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    V_2_12_Tablas_04
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_00_03A2_12_00_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_00_03A2_12_00_04 = False
End Function

Public Function CodigoDeActualizacion2_12_00_04A2_12_00_05()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
        
    
    frmProgreso.lblDetalle = "Se van a modificar triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    V_2_12_Triggers_05
    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.00.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_00_04A2_12_00_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_00_04A2_12_00_05 = False
End Function


Private Sub V_2_12_Tablas_01()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.WEBTEXT ADD" & vbCrLf
sConsulta = sConsulta & "   GER text NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.ACT1 ADD" & vbCrLf
sConsulta = sConsulta & "   DEN_GER varchar(1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.ACT2 ADD" & vbCrLf
sConsulta = sConsulta & "   DEN_GER varchar(1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.ACT3 ADD" & vbCrLf
sConsulta = sConsulta & "   DEN_GER varchar(1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.ACT4 ADD" & vbCrLf
sConsulta = sConsulta & "   DEN_GER varchar(1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.ACT5 ADD" & vbCrLf
sConsulta = sConsulta & "   DEN_GER varchar(1000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO IDI (ID,COD,DEN) VALUES (2,'GER','Deutsch')"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE ACT1 SET DEN_GER=DEN_ENG"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE ACT2 SET DEN_GER=DEN_ENG"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE ACT3 SET DEN_GER=DEN_ENG"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE ACT4 SET DEN_GER=DEN_ENG"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE ACT5 SET DEN_GER=DEN_ENG"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE ACT1 ALTER COLUMN DEN_GER VARCHAR(1000) NOT NULL"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE ACT2 ALTER COLUMN DEN_GER VARCHAR(1000) NOT NULL"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE ACT3 ALTER COLUMN DEN_GER VARCHAR(1000) NOT NULL"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE ACT4 ALTER COLUMN DEN_GER VARCHAR(1000) NOT NULL"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE ACT5 ALTER COLUMN DEN_GER VARCHAR(1000) NOT NULL"
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_12_Tablas_02()

Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PARGEN_MAIL]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PARGEN_MAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PARGEN_MAIL] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [TIPOCLTEMAIL] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SERVIDOR] [varchar] (255) ," & vbCrLf
sConsulta = sConsulta & "   [PUERTO] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [SSL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AUTENTICACION] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [BASICA_GS] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (100) ," & vbCrLf
sConsulta = sConsulta & "   [PWD] [varchar] (100) ," & vbCrLf
sConsulta = sConsulta & "   [USAR_CUENTA] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CUENTAMAIL] [varchar] (100)," & vbCrLf
sConsulta = sConsulta & "   [MOSTRARMAIL] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ACUSERECIBO] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECPWD] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_MAIL] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_MAIL_TIPOCLTEMAIL] DEFAULT (0) FOR [TIPOCLTEMAIL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_MAIL_MOSTRARMAIL] DEFAULT (0) FOR [MOSTRARMAIL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_MAIL_ACUSERECIBO] DEFAULT (0) FOR [ACUSERECIBO]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PARGEN_MAIL] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO PARGEN_MAIL (ID) VALUES (1)"
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_Tablas_03()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "alter table cias alter column volfact float null" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "alter table pargen_interno add COMPRUEBANIF int not null CONSTRAINT [DF_PARGEN_INTERNO_COMPRUEBANIF] DEFAULT (0)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_Tablas_04()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   MAILPATH varchar(500) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_Triggers_05()
Dim sConsulta As String

sConsulta = "UPDATE IDI SET DEN = 'Espa�ol' WHERE DEN = 'Spanish'"

ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER [CIAS_TG_INS] ON dbo.CIAS " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_Ins CURSOR FOR SELECT ID,FCEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Ins INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT UNA_COMPRADORA FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @EST=3 " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT count(*) FROM CIAS WHERE CIAS.FCEST=3) > 1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           RAISERROR (50005,16,1)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS SET FECINS=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_Ins INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub
