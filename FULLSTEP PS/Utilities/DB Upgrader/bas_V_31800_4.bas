Attribute VB_Name = "bas_V_31800_4"
Option Explicit

Public Function CodigoDeActualizacion31800_02_05_03A31800_04_06_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_001
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_02_05_03A31800_04_06_00 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_02_05_03A31800_04_06_00 = False
End Function


Private Sub V_31800_4_Storeds_001()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ARTICULOS @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA', @INSTANCIAMONEDA VARCHAR(3)='EUR',@CARGAR_ULT_ADJ TINYINT=0 ,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL, @CARGAR_ORG TINYINT=0  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT,@CARGAR_ORG=@CARGAR_ORG'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL, @CARGAR_ORG TINYINT',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT,@CARGAR_ORG=@CARGAR_ORG" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PARGEN_INTERNO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PARGEN_INTERNO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PARGEN_INTERNO @CIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT * FROM ' + @FSGS + 'PARGEN_INTERNO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELDS_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETFIELDS_INSTANCIA @CIA INT,@CIAPROVE INT,  @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL ,@PROVE VARCHAR(50)=NULL ,@RECENV TINYINT = 1, @NUEVO_WORKFLOW TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA_31600 @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECENV = 1 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT * FROM COPIA_CAMPO WHERE INSTANCIA = -1000 AND CIA = -1000" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT C.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C ON A.CIA=C.CIA AND A.CAMPO = C.ID " & vbCrLf
sConsulta = sConsulta & "    WHERE C.INSTANCIA=-1000 AND  C.CIA = -1000 AND C.ES_SUBCAMPO=0 ORDER BY A.CAMPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT * FROM COPIA_CAMPO WHERE INSTANCIA = @ID AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT C.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C ON A.CIA=C.CIA AND A.CAMPO = C.ID " & vbCrLf
sConsulta = sConsulta & "    WHERE C.INSTANCIA=@ID AND  C.CIA = @CIAPROVE AND C.ES_SUBCAMPO=0 ORDER BY A.CAMPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31800_04_06_00A31800_04_06_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_002
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_00A31800_04_06_01 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_00A31800_04_06_01 = False
End Function

Private Sub V_31800_4_Storeds_002()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTDESGLOSE @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL , @VERSION INT, @NUEVO_WORKFLOW INT=0 , @DEFECTO TINYINT= 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAPROVE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA,@CIAPROVE=RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_31600 @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION, @DEFECTO=@DEFECTO '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION, @DEFECTO=@DEFECTO '  " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50), @VERSION INT, @DEFECTO TINYINT ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS, @VERSION = @VERSION , @DEFECTO=@DEFECTO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT  * FROM COPIA_CAMPO WHERE ID = @COPIA_CAMPO AND CIA=@CIAPROVE)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT LD.* FROM COPIA_LINEA_DESGLOSE LD  WHERE LD.CAMPO_PADRE=@COPIA_CAMPO AND CIA=@CIAPROVE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "   SELECT LDA.ID, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO, LDA.LINEA,  ' ' NOMBRE, LDA.NOM , LDA.IDIOMA, LDA.DATASIZE, LDA.PER, LDA.COMENT, LDA.FECALTA " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN LDA " & vbCrLf
sConsulta = sConsulta & "   WHERE CAMPO_PADRE = @COPIA_CAMPO AND CIA=@CIAPROVE ORDER BY CAMPO_HIJO, LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --para sacar el valor del campo estado" & vbCrLf
sConsulta = sConsulta & "   SELECT * FROM NOCONF_ACC WITH (NOLOCK)  WHERE CAMPO_PADRE = @COPIA_CAMPO AND INSTANCIA=@INSTANCIA  AND CIA=@CIAPROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31800_04_06_01A31800_04_06_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_003
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_01A31800_04_06_02 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_01A31800_04_06_02 = False
End Function


Private Sub V_31800_4_Storeds_003()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_REALIZAR_ACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_REALIZAR_ACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_REALIZAR_ACCION @CIA INT,  @CIAPROVE INT, @INSTANCIA INT, @INSTANCIABLOQUE INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL ,@DEVOLUCION TINYINT=0, @ETAPA INT , @USUNOM VARCHAR(500)=NULL,@IDIOMAPROVE VARCHAR(3)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @INSTANCIABLOQUE=@INSTANCIABLOQUE, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE, @DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @INSTANCIABLOQUE INT,  @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT,@DEVOLUCION TINYINT,@ETAPA INT , @USUNOM VARCHAR(500),@IDIOMAPROVE VARCHAR(3)=NULL  ', @INSTANCIA=@INSTANCIA,  @INSTANCIABLOQUE=@INSTANCIABLOQUE,  @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE,@DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31800_04_06_02A31800_04_06_03()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_004
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_02A31800_04_06_03 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_02A31800_04_06_03 = False
End Function


Private Sub V_31800_4_Storeds_004()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE dbo.FSPM_GETINSTDESGLOSE_ADJUNTODEFECTO @CIA INT, @SOLICITUD INT = 0, @ID INT= 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_ADJUNTODEFECTO @SOLICITUD=@SOLICITUD, @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SOLICITUD INT=0, @ID INT', @SOLICITUD=@SOLICITUD, @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNDATA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNDATA @CIA INT=NULL, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0, @PORTAL TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @TIPO=1 OR @TIPO = 3 OR @TIPO = 5" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN  WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "          SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "          SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "        READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SELECT @IDGS = ID_GS FROM COPIA_CAMPO_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "          IF @IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA INNER JOIN COPIA_CAMPO_ADJUN  CCA ON CA.ID = CCA.ADJUN_PORTAL WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "              if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "              ELSE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "              READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "              EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@IDGS, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        IF @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @IDGS = ID_GS FROM COPIA_LINEA_DESGLOSE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "            IF @IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN  CCA ON CA.ID = CCA.ADJUN_PORTAL WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "                if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "                  SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "                ELSE " & vbCrLf
sConsulta = sConsulta & "                  SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "                READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "                EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@IDGS, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "            if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "              SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "            READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31800_04_06_03A31800_04_06_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_005
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_03A31800_04_06_04 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_03A31800_04_06_04 = False
End Function


Private Sub V_31800_4_Storeds_005()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PUJAS_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PUJAS_ITEM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PUJAS_ITEM @CIA_COMP INTEGER,@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER,@PROVE VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PUJAS_ITEM_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@ITEM=@ITEM,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER,@PROVE VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @ITEM = @ITEM,@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_04_06_04A31800_04_06_05()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_006
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_04A31800_04_06_05 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_04A31800_04_06_05 = False
End Function



Private Sub V_31800_4_Storeds_006()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_REALIZAR_ACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_REALIZAR_ACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_REALIZAR_ACCION @CIA INT,  @CIAPROVE INT, @INSTANCIA INT, @INSTANCIABLOQUE INT, @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL , @IMPORTE FLOAT = NULL ,@DEVOLUCION TINYINT=0, @ETAPA INT , @USUNOM VARCHAR(500)=NULL,@IDIOMAPROVE VARCHAR(3)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_TRASPASAR_INSTANCIA @INSTANCIA=@INSTANCIA, @INSTANCIABLOQUE=@INSTANCIABLOQUE, @PROVE=@PROVE, @COMENT = @COMENT, @IMPORTE = @IMPORTE, @DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @INSTANCIABLOQUE INT,  @PROVE VARCHAR(50), @COMENT VARCHAR(500)=NULL ,@IMPORTE FLOAT,@DEVOLUCION TINYINT,@ETAPA INT , @USUNOM VARCHAR(500),@IDIOMAPROVE VARCHAR(3)=NULL  ', @INSTANCIA=@INSTANCIA,  @INSTANCIABLOQUE=@INSTANCIABLOQUE,  @PROVE=@PROVEGS, @COMENT = @COMENT, @IMPORTE = @IMPORTE,@DEVOLUCION=@DEVOLUCION,@ETAPA=@ETAPA,@USUNOM=@USUNOM,@IDIOMAPROVE=@IDIOMAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEVOLUCION = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Por programa SET XACT_ABORT ON si falla FSPM_TRASPASAR_INSTANCIA aqui no llega" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET SOLIC_ACT= SOLIC_ACT - 1,SOLIC_NUE= SOLIC_NUE - 1 WHERE CIA_COMP = @CIA AND CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & " FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "           ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "          AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "            ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "           AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "  AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "               AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & " WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND C.CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   AND CIA = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_FIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_FIELDSCALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_FIELDSCALC @CIA INT, @ID INT, @CASO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el SERVIDOR y la BBDD" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.'" & vbCrLf
sConsulta = sConsulta & "FROM CIAS" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos los campos calculados del grupo" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_FIELDSCALC @ID=@ID,@CASO =@CASO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@ID AS INT, @CASO AS INT', @ID=@ID,@CASO=@CASO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub




Public Function CodigoDeActualizacion31800_04_06_05A31800_04_06_06()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_007
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_05A31800_04_06_06 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_05A31800_04_06_06 = False
End Function



Private Sub V_31800_4_Storeds_007()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_JOB_CREARJOBACTPROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_JOB_CREARJOBACTPROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_JOB_CREARJOBACTPROVE (@NOMBRE_JOB VARCHAR(50),@NOMBRE_SCH VARCHAR(50),@TIPO INT,@INTERVALO INT,@TIPOPERIODO INT,@FRECUENCIA INT, @FECINI INT,@TIMEINI INT,@TIMEFIN INT,@NOMBRE_STEP VARCHAR(50),@NOMBRE_PROC VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SERVNAME AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @RET_STAT as INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION as varchar(26)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ContadorTareas int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @SERVNAME=@@SERVERNAME" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE_JOB,13,LEN(@NOMBRE_JOB)-12)" & vbCrLf
sConsulta = sConsulta & "   --Comprobacion de si existe la tarea." & vbCrLf
sConsulta = sConsulta & "   SELECT @ContadorTareas = COUNT(Name) FROM msdb.dbo.SysJobs with (nolock) WHERE Name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "IF @ContadorTareas<>0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si existe la tarea..." & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPOPERIODO,@freq_subday_interval=@FRECUENCIA, @active_start_date=@FECINI,@active_start_time =@TIMEINI,@active_end_time =@TIMEFIN" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   --Si NO existe la tarea...Lo crea." & vbCrLf
sConsulta = sConsulta & "    EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE_JOB" & vbCrLf
sConsulta = sConsulta & "       IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                   SET @version = replace(left(@@version,26),' ','')" & vbCrLf
sConsulta = sConsulta & "                   IF @VERSION = 'MicrosoftSQLServer2000'" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SET @COMANDO='osql -E -S ' + @SERVNAME + ' -Q " & Chr(34) & "Exec ' + @BD + '.dbo.' + @NOMBRE_PROC + '" & Chr(34) & "'" & vbCrLf
sConsulta = sConsulta & "               EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'CMDEXEC', @command =@COMANDO" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @COMANDO=@NOMBRE_PROC" & vbCrLf
sConsulta = sConsulta & "                                                     EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE_JOB,@step_id =1,@step_name=@NOMBRE_STEP,@subsystem = 'TSQL', @command =@COMANDO,@database_name=@BD" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                               EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE_JOB,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "                               IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                                       EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE_JOB,@name=@NOMBRE_SCH,@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@TIPOPERIODO,@freq_subday_interval=@FRECUENCIA, @active_start_date=@FECINI,@active_start_time =@TIMEINI,@active_end_time =@TIMEFIN" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_04_06_06A31800_04_06_07()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Triggers"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Triggers_008
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_06A31800_04_06_07 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_06A31800_04_06_07 = False
End Function


Private Sub V_31800_4_Triggers_008()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_TG_INS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [CIAS_TG_INS] ON dbo.CIAS " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_Ins CURSOR FOR SELECT ID,FCEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Ins INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT UNA_COMPRADORA FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @EST=3 " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT count(*) FROM CIAS WHERE CIAS.FCEST=3) > 1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           RAISERROR (50005,16,1)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS SET MAX_ADJUN=(SELECT MAX_ADJUN_CIAS FROM PARGEN_GEST)  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS SET FECINS=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_Ins INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_04_06_07A31800_04_06_08()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Triggers"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_009
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_07A31800_04_06_08 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_07A31800_04_06_08 = False
End Function


Private Sub V_31800_4_Storeds_009()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_CONTADOR_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_CONTADOR_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ACT_CONTADOR_NOCONFORMIDAD  @PROVE NVARCHAR(50), @NOCONFORMIDAD INT ,@ACCION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--@Accion = 1 --> Cerrar la NoConformidad /Eliminar" & vbCrLf
sConsulta = sConsulta & "--@Accion = 2 --> A�adir una nueva Conformidad /(Reabrir)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_PROVE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @CIA_COMP = CIA_COMP,@CIA_PROVE=CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "FROM CIAS C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP RC WITH (NOLOCK) ON RC.CIA_PROVE = C.ID" & vbCrLf
sConsulta = sConsulta & " where C.cod = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACCION = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET NOCONF_NUE = CASE WHEN (NOCONF_NUE -1)<0 THEN 0 ELSE (NOCONF_NUE -1) END" & vbCrLf
sConsulta = sConsulta & "   , NOCONF_ACT = (NOCONF_ACT + 1)" & vbCrLf
sConsulta = sConsulta & "   FROM REL_CIAS RC " & vbCrLf
sConsulta = sConsulta & "   WHERE CIA_COMP = @CIA_COMP AND CIA_PROVE = @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE IF @ACCION = 2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET NOCONF_NUE =NOCONF_NUE +1" & vbCrLf
sConsulta = sConsulta & "   , NOCONF_ACT = (NOCONF_ACT + 1)" & vbCrLf
sConsulta = sConsulta & "   FROM REL_CIAS RC " & vbCrLf
sConsulta = sConsulta & "   WHERE CIA_COMP = @CIA_COMP AND CIA_PROVE = @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31800_04_06_08A31800_04_06_09()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_010
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_08A31800_04_06_09 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_08A31800_04_06_09 = False
End Function


Private Sub V_31800_4_Storeds_010()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_CONTADOR_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_CONTADOR_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_ACT_CONTADOR_NOCONFORMIDAD  @PROVE NVARCHAR(50), @NOCONFORMIDAD INT ,@ACCION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--@Accion = 1 --> Cerrar la NoConformidad " & vbCrLf
sConsulta = sConsulta & "--@Accion = 2 --> A�adir una nueva Conformidad /(Reabrir)" & vbCrLf
sConsulta = sConsulta & "--@Accion = 3 --> /Eliminar" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_PROVE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @CIA_COMP = CIA_COMP,@CIA_PROVE=CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "FROM CIAS C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP RC WITH (NOLOCK) ON RC.CIA_PROVE = C.ID" & vbCrLf
sConsulta = sConsulta & " where C.cod = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACCION = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET NOCONF_NUE = CASE WHEN (NOCONF_NUE -1)<0 THEN 0 ELSE (NOCONF_NUE -1) END" & vbCrLf
sConsulta = sConsulta & "   , NOCONF_ACT = (NOCONF_ACT + 1)" & vbCrLf
sConsulta = sConsulta & "   FROM REL_CIAS RC " & vbCrLf
sConsulta = sConsulta & "   WHERE CIA_COMP = @CIA_COMP AND CIA_PROVE = @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE IF @ACCION = 2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET NOCONF_NUE =NOCONF_NUE +1" & vbCrLf
sConsulta = sConsulta & "   , NOCONF_ACT = (NOCONF_ACT + 1)" & vbCrLf
sConsulta = sConsulta & "   FROM REL_CIAS RC " & vbCrLf
sConsulta = sConsulta & "   WHERE CIA_COMP = @CIA_COMP AND CIA_PROVE = @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE IF @ACCION = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET NOCONF_NUE = CASE WHEN (NOCONF_NUE -1)<0 THEN 0 ELSE (NOCONF_NUE -1) END" & vbCrLf
sConsulta = sConsulta & "   , NOCONF_ACT = CASE WHEN (NOCONF_ACT -1)<0 THEN 0 ELSE (NOCONF_ACT -1) END" & vbCrLf
sConsulta = sConsulta & "   FROM REL_CIAS RC " & vbCrLf
sConsulta = sConsulta & "   WHERE CIA_COMP = @CIA_COMP AND CIA_PROVE = @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub



Public Function CodigoDeActualizacion31800_04_06_09A31800_04_06_10()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Tablas_011

   frmProgreso.lblDetalle = "Cambios en triggers"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Triggers_011


   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_011
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_09A31800_04_06_10 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_09A31800_04_06_10 = False
End Function


Private Sub V_31800_4_Storeds_011()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_INSERT_REGISTRO_MAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_INSERT_REGISTRO_MAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_INSERT_REGISTRO_MAIL] @CIA INT, @SUBJECT NVARCHAR (100),@PARA NVARCHAR (100),@CC NVARCHAR (100),@CCO NVARCHAR (100)," & vbCrLf
sConsulta = sConsulta & "@DIR_RESPUESTA NVARCHAR(100),@CUERPO NTEXT,@ACUSE_RECIBO TINYINT,@TIPO TINYINT,@PRODUCTO INT ,@USU INT,@KO TINYINT," & vbCrLf
sConsulta = sConsulta & "@TEXTO_ERROR NVARCHAR(200) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID=MAX(ISNULL(ID,0))+1 FROM REGISTRO_EMAIL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO REGISTRO_EMAIL (CIA, ID, SUBJECT, PARA ,CC ,CCO ,DIR_RESPUESTA ,CUERPO ,ACUSE_RECIBO ,TIPO ,PRODUCTO ,USU ,KO ,TEXTO_ERROR)" & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA, @ID, @SUBJECT , @PARA ,@CC ,@CCO ,@DIR_RESPUESTA ,@CUERPO ,@ACUSE_RECIBO ,@TIPO ,@PRODUCTO ,@USU ,@KO ,@TEXTO_ERROR)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_31800_4_Tablas_011()

Dim sConsulta As String

    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[REGISTRO_EMAIL]') and name=N'KO')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD [KO]  [tinyint] NOT NULL CONSTRAINT [DF_REGISTRO_EMAIL_KO] DEFAULT (0)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[REGISTRO_EMAIL]') and name=N'TEXTO_ERROR')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD [TEXTO_ERROR] [nvarchar] (200) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[PRODUCTO]') and name=N'TEXTO_ERROR')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD [PRODUCTO] [tinyint]  NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[USU]') and name=N'TEXTO_ERROR')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD [USU] [int]  NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "UPDATE REGISTRO_EMAIL SET PRODUCTO=1,KO=0" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
End Sub


Private Sub V_31800_4_Triggers_011()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REGISTRO_EMAIL_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[REGISTRO_EMAIL_TG_INS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [REGISTRO_EMAIL_TG_INS] ON dbo.REGISTRO_EMAIL " & vbCrLf
sConsulta = sConsulta & "FOR INSERT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REGISTRO_EMAIL SET FECHA=GETDATE()" & vbCrLf
sConsulta = sConsulta & "FROM REGISTRO_EMAIL R INNER JOIN INSERTED I ON R.CIA=I.CIA AND R.ID=I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    
End Sub

Public Function CodigoDeActualizacion31800_04_06_10A31800_04_06_11()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True

   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_4_Storeds_012
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.06.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_10A31800_04_06_11 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_10A31800_04_06_11 = False
End Function

Private Sub V_31800_4_Storeds_012()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEFTABLAEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEFTABLAEXTERNA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEFTABLAEXTERNA @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEFTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETTABLAEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETTABLAEXTERNA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETTABLAEXTERNA @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

