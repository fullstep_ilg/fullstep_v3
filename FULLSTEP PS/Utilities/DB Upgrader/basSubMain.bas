Attribute VB_Name = "basSubMain"
Sub Main()
    Dim sArgs() As String
    Dim summitconn As String
    Dim adoCon As ADODB.Connection
    
    On Error GoTo error
    
    frmSplash.Show 1
    
    If Len(Command$) > 0 Then
        sArgs = Split(Command$, ";")
    
        If Trim(sArgs(0) = "") Then
            MsgBox "El nombre del servidor no es correcto", vbInformation + vbOKOnly
        End If
        gServer = sArgs(0)
        
        If Trim(sArgs(1)) = "" Then
            MsgBox "El nombre de la base de datos no es correcto", vbInformation + vbOKOnly
        End If
        gBD = sArgs(1)
        
        If Trim(sArgs(2)) = "" Then
            MsgBox "El nombre del usuario no es correcto", vbInformation + vbOKOnly
        End If
        gUID = sArgs(2)
        
        If Trim(sArgs(3)) = "" Then
            MsgBox "La clave no es correcta", vbInformation + vbOKOnly
        End If
        gPWD = sArgs(3)
        
        summitconn = "DRIVER={SQL Server};SERVER=" & sArgs(0) & ";UID=" & sArgs(2) & ";PWD=" & sArgs(3) & ";DATABASE=" & sArgs(1) & ";"
        
        rdoEnvironments(0).CursorDriver = rdUseOdbc
        
        Set gRDOCon = rdoEnvironments(0).OpenConnection("", rdDriverNoPrompt, , summitconn)
        Set gRDOErs = rdoEngine.rdoErrors
        
        frmConfirmar.Show 1
    Else
        frmPrincipal.Show 1
    End If
        
    Exit Sub
error:
    MsgBox "Imposible acceder a la BaseDeDatos" & vbLf & "Revise los datos introducidos y vuelva a intentarlo", vbCritical + vbOKOnly
End Sub
