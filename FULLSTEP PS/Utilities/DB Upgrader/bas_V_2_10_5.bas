Attribute VB_Name = "bas_V_2_10_5"
Dim oDMOBD As SQLDMO.Database
Option Explicit


Public Function CodigoDeActualizacion2_10_00_05A2_10_50_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Actualizamos la estructura de tablas a la versi�n 2.10
    frmProgreso.lblDetalle = "Se actualizar� la estructura de tablas a la versi�n 2.10.5"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_10_5_Actualizar_Tablas
        
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.10.50.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_10_00_05A2_10_50_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_00_05A2_10_50_00 = False
End Function



Private Function V_2_10_5_Actualizar_Tablas()
Dim sConsulta As String


sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD [UNA_COMPRADORA] [bit] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_UNA_COMPRADORA] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TRIGGER [PARGEN_INTERNO_TG_INS] ON dbo.PARGEN_INTERNO" & vbCrLf
sConsulta = sConsulta & "FOR INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(UNA_COMPRADORA)" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF (SELECT UNA_COMPRADORA FROM INSERTED) = 1   " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT count(*) FROM CIAS WHERE CIAS.FCEST=3) > 1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           exec sp_addmessage 50006 , 16, 'There are more than one companyes with CIAS.FCEST=03!'" & vbCrLf
sConsulta = sConsulta & "           RAISERROR (50006,16,1)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [CIAS_TG_INS] ON dbo.CIAS " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_Ins CURSOR FOR SELECT ID,FCEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Ins INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT UNA_COMPRADORA FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @EST=3 " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT count(*) FROM CIAS WHERE CIAS.FCEST=3) > 1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           exec sp_addmessage 50005 , 16, ' Only one buyer is allowed with actual settings ! Set PARGEN_INTERNO.UNA_COMPRADORA to 0 to allow more buyers.'" & vbCrLf
sConsulta = sConsulta & "           RAISERROR (50005,16,1)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS SET FECINS=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_Ins INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [CIAS_TG_UPD] ON dbo.CIAS " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_Upd CURSOR LOCAL FOR SELECT ID,FCEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "  IF UPDATE(FCEST) AND (SELECT UNA_COMPRADORA FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @EST=3 " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT count(*) FROM CIAS WHERE CIAS.FCEST=3) > 1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               exec sp_addmessage 50005 , 16, 'Only one buyer is allowed with actual configuration!'" & vbCrLf
sConsulta = sConsulta & "               RAISERROR (50005,16,1)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "  UPDATE CIAS SET FECLASTUPD=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID)" & vbCrLf
sConsulta = sConsulta & "  IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "    UPDATE CIAS SET FECACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTOLD=3 AND @EST<>3 " & vbCrLf
sConsulta = sConsulta & "      UPDATE CIAS SET FECDESACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  -- Inserta un moviemto tipo Udate 'U' en la tabla MVTOS_PROV" & vbCrLf
sConsulta = sConsulta & "  IF UPDATE(DEN) OR UPDATE(DIR) OR UPDATE(CP) OR UPDATE(POB) OR UPDATE(PROVI) OR UPDATE(NIF) OR" & vbCrLf
sConsulta = sConsulta & "    UPDATE(PAI) OR UPDATE(MON) OR UPDATE(URLCIA)" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                                INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                        ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
sConsulta = sConsulta & "                                       AND CIAS.FCEST=3 " & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@ID " & vbCrLf
sConsulta = sConsulta & "                         AND EST=3)" & vbCrLf
sConsulta = sConsulta & "      IF @ENLAZADA >0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV) " & vbCrLf
sConsulta = sConsulta & "          IF @INDICE IS NULL       " & vbCrLf
sConsulta = sConsulta & "            SET @INDICE=1" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @INDICE=@INDICE+1" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@ID,NULL,'U',0,GETDATE())" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Upd" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Function
