Attribute VB_Name = "bas_V_32100_2"
Public Function CodigoDeActualizacion32100_01_207_A32100_02_21_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_2_Storeds_000
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.21.00'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_01_207_A32100_02_21_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_01_207_A32100_02_21_00 = False
End Function


Private Sub V_32100_2_Storeds_000()

Dim sConsulta As String


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR] @CIA INT, @PROVEEDOR NVARCHAR(MAX), @IDCERTIFICADO INT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR @PROVEEDOR=@PROVEEDOR,@IDCERTIFICADO=@IDCERTIFICADO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVEEDOR NVARCHAR(MAX), @IDCERTIFICADO INT ',@PROVEEDOR=@PROVEEDOR,@IDCERTIFICADO=@IDCERTIFICADO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
sConsulta = sConsulta & "@CIA INT," & vbCrLf
sConsulta = sConsulta & "@PROVE VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@IDI AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_CERTIFICADOS_PROVE @PROVE= @PROVE, @IDI = @IDI'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVE VARCHAR(50),@IDI VARCHAR(3)',@PROVE= @PROVE, @IDI = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =N'SELECT W.TEXT_' + @IDI + ' AS DEN FROM WEBFSPMTEXT W WITH(NOLOCK) WHERE W.MODULO =118 AND ((W.ID>=9 AND W.ID<=13) OR W.ID=21 OR W.ID=27 OR W.ID=28 OR W.ID=29) ORDER BY W.ID ASC'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENER_SERVICIO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENER_SERVICIO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_OBTENER_SERVICIO]" & vbCrLf
sConsulta = sConsulta & "@SERVICIO NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@CAMPOID NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=1 AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_OBTENER_SERVICIO @SERVICIO=@SERVICIO,@CAMPOID=@CAMPOID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SERVICIO NVARCHAR(50), @CAMPOID NVARCHAR(50) ',@SERVICIO=@SERVICIO,@CAMPOID=@CAMPOID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_02_21_00_A32100_02_21_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_2_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.21.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_02_21_00_A32100_02_21_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_02_21_00_A32100_02_21_01 = False
End Function

Private Sub V_32100_2_Storeds_001()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR] @CIA INT, @PROVEEDOR NVARCHAR(MAX), @IDCERTIFICADO INT=0, @TIPO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_CERTIFICADO_PDTE_CUMPLIMENTAR @PROVEEDOR=@PROVEEDOR,@IDCERTIFICADO=@IDCERTIFICADO,@TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PROVEEDOR NVARCHAR(MAX), @IDCERTIFICADO INT, @TIPO INT ',@PROVEEDOR=@PROVEEDOR,@IDCERTIFICADO=@IDCERTIFICADO,@TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion32100_02_21_01_A32100_02_21_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_2_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.21.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_02_21_01_A32100_02_21_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_02_21_01_A32100_02_21_02 = False
End Function

Private Sub V_32100_2_Storeds_002()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENER_SERVICIO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENER_SERVICIO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_OBTENER_SERVICIO]" & vbCrLf
sConsulta = sConsulta & "@SERVICIO NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@CAMPOID NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=1 AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_OBTENER_SERVICIO @SERVICIO=@SERVICIO,@CAMPOID=@CAMPOID,@INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@SERVICIO NVARCHAR(50), @CAMPOID NVARCHAR(50),@INSTANCIA INT',@SERVICIO=@SERVICIO,@CAMPOID=@CAMPOID,@INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_02_21_02_A32100_02_21_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_2_Storeds_003
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.21.03'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_02_21_02_A32100_02_21_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_02_21_02_A32100_02_21_03 = False
End Function

Private Sub V_32100_2_Storeds_003()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(MAX),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100),@USU_NIF VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       --De haber repetidos. Sera pq estan en estado 1. De ellos se encarga stored SP_PROV_REINTENTOS." & vbCrLf
sConsulta = sConsulta & "       IF (SELECT COUNT (1) FROM MVTOS_PROV_EST WITH(NOLOCK) WHERE MVTO=@ID AND CIA=@CIA)=0" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)     " & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA," & vbCrLf
sConsulta = sConsulta & "MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "FROM MVTOS_PROV_EST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- OBTENEMOS LOS DATOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='I'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT, @USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "WHERE" & vbCrLf
sConsulta = sConsulta & "USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT, @USU_NIF=@USU_NIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='U'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50),@USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT," & vbCrLf
sConsulta = sConsulta & "@DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "@COM =@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INTENTO = @INTENTO +1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_02_21_03_A32100_02_21_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_2_Storeds_004
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.21.04'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_02_21_03_A32100_02_21_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_02_21_03_A32100_02_21_04 = False
End Function

Private Sub V_32100_2_Storeds_004()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_FSAL_REGISTRAR_ACCESO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_FSAL_REGISTRAR_ACCESO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion32100_02_21_04_A32100_02_21_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
       
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_2_Tablas_005
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_2_Storeds_005
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.21.05'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_02_21_04_A32100_02_21_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_02_21_04_A32100_02_21_05 = False
End Function

Private Sub V_32100_2_Storeds_005()

Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_ACTUALIZAR_PROV_GS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(MAX),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100),@USU_NIF VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DATASIZE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV WITH(NOLOCK)  WHERE FECACT > @FECULT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WITH(NOLOCK) WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       --De haber repetidos. Sera pq estan en estado 1. De ellos se encarga stored SP_PROV_REINTENTOS." & vbCrLf
sConsulta = sConsulta & "       IF (SELECT COUNT (1) FROM MVTOS_PROV_EST WITH(NOLOCK) WHERE MVTO=@ID AND CIA=@CIA)=0" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)     " & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  --CALIDAD: sin with nolock pq se usa en update" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD SIN WITH(NOLOCK) modificado desde el 09/10/2012 versi?n 31900.6 tarea 2519" & vbCrLf
sConsulta = sConsulta & "--De poner  WITH(NOLOCK) se obtendr?a este error" & vbCrLf
sConsulta = sConsulta & "--      Msg 16941, Level 16, State 1, Procedure SP_ACTUALIZAR_PROV_GS, Line 416" & vbCrLf
sConsulta = sConsulta & "--      Cursor updates are not allowed on tables opened with the NOLOCK option." & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA," & vbCrLf
sConsulta = sConsulta & "MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "FROM MVTOS_PROV_EST" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS WITH(NOLOCK) ON CIAS.ID=MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MVTOS_PROV WITH(NOLOCK) ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "FROM REL_CIAS  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WITH(NOLOCK) WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- OBTENEMOS LOS DATOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "FROM CIAS WITH(NOLOCK) LEFT JOIN PAI WITH(NOLOCK) ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVI WITH(NOLOCK) ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MON WITH(NOLOCK) ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "@CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "@CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "@CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "@CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "@CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "@CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "@CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "@CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA?IA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='I'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT, @USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "WHERE" & vbCrLf
sConsulta = sConsulta & "USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT,@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT, @USU_NIF=@USU_NIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO='U'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "SET @THOUSANFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @DECIMALFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @PRECISIONFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @DATEFMT = NULL" & vbCrLf
sConsulta = sConsulta & "SET @USU_NIF = NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = I.COD, @TIPOEMAIL=TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=THOUSANFMT,@DECIMALFMT=DECIMALFMT,@PRECISIONFMT=PRECISIONFMT, @DATEFMT=DATEFMT,@USU_NIF=NIF" & vbCrLf
sConsulta = sConsulta & "FROM USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI I WITH(NOLOCK) ON USU.IDI = I.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_PORT WITH(NOLOCK) ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "WHERE USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT," & vbCrLf
sConsulta = sConsulta & "@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "@DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50),@USU_NIF VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "@USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "@USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "@USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "@USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "@USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "@USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "@THOUSANFMT=@THOUSANFMT,@DECIMALFMT=@DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "@PRECISIONFMT=@PRECISIONFMT, @DATEFMT=@DATEFMT," & vbCrLf
sConsulta = sConsulta & "@USU_NIF=@USU_NIF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT,@DATASIZE=DATALENGTH(DATA) FROM CIAS_ESP WITH(NOLOCK) WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT," & vbCrLf
sConsulta = sConsulta & "@DATASIZE INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC," & vbCrLf
sConsulta = sConsulta & "@DATASIZE=@DATASIZE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "@COM =@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "@ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "@COM=@COM," & vbCrLf
sConsulta = sConsulta & "@FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "@COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "@ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INTENTO = @INTENTO +1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PROVE_ACTIVITIES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PROVE_ACTIVITIES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PROVE_ACTIVITIES]" & vbCrLf
sConsulta = sConsulta & "   @CIA INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   ;WITH PROVE_ACTIVITIES AS(" & vbCrLf
sConsulta = sConsulta & "       SELECT ACT1,0 ACT2,0 ACT3,0 ACT4,0 ACT5 FROM CIAS_ACT1 WITH(NOLOCK) WHERE CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT ACT1,ACT2,0 ACT3,0 ACT4,0 ACT5 FROM CIAS_ACT2 WITH(NOLOCK) WHERE CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT ACT1,ACT2,ACT3,0 ACT4,0 ACT5 FROM CIAS_ACT3 WITH(NOLOCK) WHERE CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT ACT1,ACT2,ACT3,ACT4,0 ACT5 FROM CIAS_ACT4 WITH(NOLOCK) WHERE CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT ACT1,ACT2,ACT3,ACT4,ACT5 FROM CIAS_ACT5 WITH(NOLOCK) WHERE CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ACT1,NULL ACT2,NULL ACT3,NULL ACT4,NULL ACT5,DEN_SPA,DEN_ENG,DEN_GER,DEN_FRA" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE_ACTIVITIES PA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 A WITH(NOLOCK) ON A.ID=PA.ACT1 AND PA.ACT2=0 AND PA.ACT3=0 AND PA.ACT4=0 AND PA.ACT5=0" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ACT1,PA.ACT2,NULL ACT3,NULL ACT4,NULL ACT5," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_SPA +' | '+ A.DEN_SPA DEN_SPA," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_ENG +' | '+ A.DEN_ENG DEN_ENG," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_GER +' | '+ A.DEN_GER DEN_GER," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_FRA +' | '+ A.DEN_FRA DEN_FRA" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE_ACTIVITIES PA    " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT2 A WITH(NOLOCK) ON A.ACT1=PA.ACT1 AND A.ID=PA.ACT2 AND PA.ACT3=0 AND PA.ACT4=0 AND PA.ACT5=0 " & vbCrLf
sConsulta = sConsulta & "       AND NOT EXISTS(SELECT 1" & vbCrLf
sConsulta = sConsulta & "                       FROM PROVE_ACTIVITIES PA2" & vbCrLf
sConsulta = sConsulta & "                       WHERE A.ACT1=PA2.ACT1 AND PA2.ACT2=0 AND PA2.ACT3=0 AND PA2.ACT4=0 AND PA2.ACT5=0)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 A1 WITH(NOLOCK) ON A1.ID=A.ACT1" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ACT1,PA.ACT2,PA.ACT3,NULL ACT4,NULL ACT5," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_SPA +' | '+ A2.DEN_SPA +' | '+ A.DEN_SPA DEN_SPA," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_ENG +' | '+ A2.DEN_ENG +' | '+ A.DEN_ENG DEN_ENG," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_GER +' | '+ A2.DEN_GER +' | '+ A.DEN_GER DEN_GER," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_FRA +' | '+ A2.DEN_FRA +' | '+ A.DEN_FRA DEN_FRA" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE_ACTIVITIES PA    " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT3 A WITH(NOLOCK) ON A.ACT1=PA.ACT1 AND A.ACT2=PA.ACT2 AND A.ID=PA.ACT3 AND PA.ACT4=0 AND PA.ACT5=0" & vbCrLf
sConsulta = sConsulta & "       AND NOT EXISTS(SELECT 1" & vbCrLf
sConsulta = sConsulta & "                       FROM PROVE_ACTIVITIES PA2" & vbCrLf
sConsulta = sConsulta & "                       WHERE A.ACT1=PA2.ACT1 AND (A.ACT2=PA2.ACT2 OR PA2.ACT2=0) AND PA2.ACT3=0 AND PA2.ACT4=0 AND PA2.ACT5=0)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 A1 WITH(NOLOCK) ON A1.ID=A.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT2 A2 WITH(NOLOCK) ON A2.ACT1=A.ACT1 AND A2.ID=A.ACT2" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ACT1,PA.ACT2,PA.ACT3,PA.ACT4,NULL ACT5," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_SPA +' | '+ A2.DEN_SPA +' | '+ A3.DEN_SPA +' | '+ A.DEN_SPA DEN_SPA," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_ENG +' | '+ A2.DEN_ENG +' | '+ A3.DEN_ENG +' | '+ A.DEN_ENG DEN_ENG," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_GER +' | '+ A2.DEN_GER +' | '+ A3.DEN_GER +' | '+ A.DEN_GER DEN_GER," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_FRA +' | '+ A2.DEN_FRA +' | '+ A3.DEN_FRA +' | '+ A.DEN_FRA DEN_FRA" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE_ACTIVITIES PA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT4 A WITH(NOLOCK) ON A.ACT1=PA.ACT1 AND A.ACT2=PA.ACT2 AND A.ACT3=PA.ACT3 AND A.ID=PA.ACT4 AND PA.ACT5=0" & vbCrLf
sConsulta = sConsulta & "   AND NOT EXISTS(SELECT 1" & vbCrLf
sConsulta = sConsulta & "                   FROM PROVE_ACTIVITIES PA2" & vbCrLf
sConsulta = sConsulta & "                   WHERE A.ACT1=PA2.ACT1 AND (A.ACT2=PA2.ACT2 OR PA2.ACT2=0) AND (A.ACT3=PA2.ACT3 OR PA2.ACT3=0) AND PA2.ACT4=0 AND PA2.ACT5=0)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 A1 WITH(NOLOCK) ON A1.ID=A.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT2 A2 WITH(NOLOCK) ON A2.ACT1=A.ACT1 AND A2.ID=A.ACT2" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT3 A3 WITH(NOLOCK) ON A3.ACT1=A.ACT1 AND A3.ACT2=A.ACT2 AND A3.ID=A.ACT3" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ACT1,PA.ACT2,PA.ACT3,PA.ACT4,PA.ACT5," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_SPA +' | '+ A2.DEN_SPA +' | '+ A3.DEN_SPA +' | '+ A4.DEN_SPA +' | '+ A.DEN_SPA DEN_SPA," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_ENG +' | '+ A2.DEN_ENG +' | '+ A3.DEN_ENG +' | '+ A4.DEN_ENG +' | '+ A.DEN_ENG DEN_ENG," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_GER +' | '+ A2.DEN_GER +' | '+ A3.DEN_GER +' | '+ A4.DEN_GER +' | '+ A.DEN_GER DEN_GER," & vbCrLf
sConsulta = sConsulta & "       A1.DEN_FRA +' | '+ A2.DEN_FRA +' | '+ A3.DEN_FRA +' | '+ A4.DEN_FRA +' | '+ A.DEN_FRA DEN_FRA" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE_ACTIVITIES PA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT5 A WITH(NOLOCK) ON A.ACT1=PA.ACT1 AND A.ACT2=PA.ACT2 AND A.ACT3=PA.ACT3 AND A.ACT4=PA.ACT4 AND A.ID=PA.ACT5" & vbCrLf
sConsulta = sConsulta & "   AND NOT EXISTS(SELECT 1" & vbCrLf
sConsulta = sConsulta & "                   FROM PROVE_ACTIVITIES PA2" & vbCrLf
sConsulta = sConsulta & "                   WHERE A.ACT1=PA2.ACT1 AND (A.ACT2=PA2.ACT2 OR PA2.ACT2=0) AND (A.ACT3=PA2.ACT3 OR PA2.ACT3=0) AND (A.ACT4=PA2.ACT4 OR PA2.ACT4=0) AND PA2.ACT5=0)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT1 A1 WITH(NOLOCK) ON A1.ID=A.ACT1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT2 A2 WITH(NOLOCK) ON A2.ACT1=A.ACT1 AND A2.ID=A.ACT2" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT3 A3 WITH(NOLOCK) ON A3.ACT1=A.ACT1 AND A3.ACT2=A.ACT2 AND A3.ID=A.ACT3" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT4 A4 WITH(NOLOCK) ON A4.ACT1=A.ACT1 AND A4.ACT2=A.ACT2 AND A4.ACT3=A.ACT3 AND A4.ID=A.ACT4" & vbCrLf
sConsulta = sConsulta & "   ORDER BY ACT5,ACT4,ACT3,ACT2,ACT1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PERSONAFISICA_INFO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PERSONAFISICA_INFO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GET_PERSONAFISICA_INFO]" & vbCrLf
sConsulta = sConsulta & "   @CIA INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT PERSONAFISICA_NOMBRE,PERSONAFISICA_APELLIDO1,PERSONAFISICA_APELLIDO2,ACEPTACION_CONDICIONES" & vbCrLf
sConsulta = sConsulta & "   FROM CIAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE CIAS.ID=@CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_SAVE_PERSONAFISICA_INFO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_SAVE_PERSONAFISICA_INFO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_SAVE_PERSONAFISICA_INFO]" & vbCrLf
sConsulta = sConsulta & "   @CIA INT," & vbCrLf
sConsulta = sConsulta & "   @USUCOD NVARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "   @NOMBRE NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @PRIMERAPELLIDO NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "   @SEGUNDOAPELLIDO NVARCHAR(100)=NULL,    " & vbCrLf
sConsulta = sConsulta & "   @USUNIF NVARCHAR(100)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS" & vbCrLf
sConsulta = sConsulta & "   SET PERSONAFISICA_NOMBRE=@NOMBRE," & vbCrLf
sConsulta = sConsulta & "       PERSONAFISICA_APELLIDO1=@PRIMERAPELLIDO," & vbCrLf
sConsulta = sConsulta & "       PERSONAFISICA_APELLIDO2=@SEGUNDOAPELLIDO," & vbCrLf
sConsulta = sConsulta & "       ACEPTACION_CONDICIONES=1" & vbCrLf
sConsulta = sConsulta & "   WHERE ID=@CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   UPDATE USU" & vbCrLf
sConsulta = sConsulta & "       SET NIF=@USUNIF" & vbCrLf
sConsulta = sConsulta & "   WHERE CIA=@CIA AND COD=@USUCOD" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_LOGIN] @CIA VARCHAR(50), @COD VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Descripci?n: Obtiene los campos de la tabla USU y los datos de la compa??a de la tabla CIAS" & vbCrLf
sConsulta = sConsulta & "--               que ha hecho el login en la aplicaci?n." & vbCrLf
sConsulta = sConsulta & "-- Par?metros de entrada:@CIA: C?digo de la compa??a." & vbCrLf
sConsulta = sConsulta & "--         @COD: C?digo de la persona que se ha validado en la aplicaci?n." & vbCrLf
sConsulta = sConsulta & "-- Par?metros de salida: Un registro con propiedades del usuario y la compa??a" & vbCrLf
sConsulta = sConsulta & "-- Tiempo m?ximo: 0 sg                         " & vbCrLf
sConsulta = sConsulta & "-- Llamada desde: PMPortalDatabaseServer --> Root.vb --> User_LoadUserData" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
sConsulta = sConsulta & "   CIAS.USUPPAL AS USUPPAL,CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
sConsulta = sConsulta & "   USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
sConsulta = sConsulta & "   USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "   USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "   USU.DATEFMT DATEFMT,USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "   IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
sConsulta = sConsulta & "   USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA,MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU," & vbCrLf
sConsulta = sConsulta & "   CIAS.DEN AS CIADEN,CIAS.NIF,IDI.LANGUAGETAG,USU.NIF USUNIF" & vbCrLf
sConsulta = sConsulta & "FROM USU WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN USU_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND USU.CIA=USU_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON USU.CIA=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CIAS_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON USU.CIA=CIAS_PORT.CIA AND CIAS_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON CIAS.MON=MON.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MON MON2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON USU.MON=MON2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "WHERE USU.COD=@COD AND CIAS.COD=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_32100_2_Tablas_005()

Dim sConsulta As String

sConsulta = "IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'PERSONAFISICA_NOMBRE' AND Object_ID = Object_ID(N'CIAS'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.CIAS ADD PERSONAFISICA_NOMBRE nvarchar(100) NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'PERSONAFISICA_APELLIDO1' AND Object_ID = Object_ID(N'CIAS'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.CIAS ADD PERSONAFISICA_APELLIDO1 nvarchar(100) NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'PERSONAFISICA_APELLIDO2' AND Object_ID = Object_ID(N'CIAS'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.CIAS ADD PERSONAFISICA_APELLIDO2 nvarchar(100) NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE NAME=N'ACEPTACION_CONDICIONES' AND Object_ID = Object_ID(N'CIAS'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.CIAS ADD ACEPTACION_CONDICIONES bit NOT NULL CONSTRAINT DF_CIAS_ACEPTACION_CONDICIONES DEFAULT 0" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub
