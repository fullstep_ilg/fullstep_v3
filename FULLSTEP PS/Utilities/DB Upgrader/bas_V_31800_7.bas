Attribute VB_Name = "bas_V_31800_7"
Option Explicit

Public Function CodigoDeActualizacion31800_05_07_14A31800_07_08_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
           
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_7_Storeds_000
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.08.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_14A31800_07_08_00 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_14A31800_07_08_00 = False
End Function


Private Sub V_31800_7_Storeds_000()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_MONEDAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_MONEDAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_MONEDAS @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1, @IDIOMA VARCHAR(20), @SOLOACT TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_MONEDAS @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA,@ALLDATOS=@ALLDATOS, @SOLOACT=@SOLOACT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50), @DEN VARCHAR(2000) , @COINCID TINYINT,  @IDIOMA VARCHAR (20), @ALLDATOS TINYINT, @SOLOACT TINYINT', @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA,@ALLDATOS=@SOLOACT,@SOLOACT=@SOLOACT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub




Public Function CodigoDeActualizacion31800_07_08_00A31800_07_08_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
           
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_7_Storeds_001
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.08.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_07_08_00A31800_07_08_01 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_07_08_00A31800_07_08_01 = False
End Function



Private Sub V_31800_7_Storeds_001()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VER_PUNTUACION_CALIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VER_PUNTUACION_CALIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_VER_PUNTUACION_CALIDAD @CODPROVE NVARCHAR(100),@CODCIA INT,@IDI VARCHAR(3) ,@PYME NVARCHAR(3)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODPROVEGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CODCIA  AND PORT =0  --las tablas de calidad est�n en el GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_VER_PUNTUACION_CALIDAD  @PROVE=@CODPROVE,@IDI=@IDI,@PYME=@PYME '" & vbCrLf
sConsulta = sConsulta & "SELECT @CODPROVEGS=COD_PROVE_CIA FROM REL_CIAS R WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON C.ID=R.CIA_PROVE WHERE C.COD=@CODPROVE" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CODPROVE NVARCHAR(100),@IDI VARCHAR(3),@PYME NVARCHAR(3) ', @CODPROVE=@CODPROVEGS, @IDI=@IDI, @PYME=@PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_WARNINGS OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL , @PORTAL tinyint = 0, @QA TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @QA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF  @PORTAL = 0 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =null , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "       IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                  SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                    FROM COPIA_ADJUN A WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      LEFT JOIN USU U WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                            AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                   WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                       ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "       IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "         SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA, 0 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU P  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "          WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "          ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                      SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA,  1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                        FROM COPIA_CAMPO_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU P  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                       WHERE  A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                      SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                        FROM COPIA_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN USU U  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                                AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                       WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                           ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "     ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "         SELECT  3 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA, 1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_LINEA_DESGLOSE_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU U  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "          WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "          ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                    SELECT  3 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA,  1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                      FROM COPIA_LINEA_DESGLOSE_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU U  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                     WHERE A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                    SELECT  4 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0)  DATASIZE , A.COMENT, @IDI IDIOMA, A.ID ADJUN, NULL CAMPO,  NULL RUTA,1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                      FROM COPIA_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN USU U  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                                AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                     WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                         ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31800_07_08_01A31800_07_08_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
           
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_7_Storeds_002
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.08.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_07_08_01A31800_07_08_02 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_07_08_01A31800_07_08_02 = False
End Function

Private Sub V_31800_7_Storeds_002()
Dim sConsulta As String

'Portal/ FSQA_ACT_CONTADOR_NOCONFORMIDAD quitar
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_CONTADOR_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_CONTADOR_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Portal/ FSQA_ACT_REL_CIAS_NEW_CERTIF quitar
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_REL_CIAS_NEW_CERTIF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_REL_CIAS_NEW_CERTIF]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Portal/ FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD quitar
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub
