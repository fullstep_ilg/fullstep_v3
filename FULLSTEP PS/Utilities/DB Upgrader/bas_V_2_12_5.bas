Attribute VB_Name = "bas_V_2_12_5"
Option Explicit
Private sFSGS_SRV As String
Private sFSGS_BD As String
Private oADOConnection As ADODB.Connection
Private iCodMon As Integer
Private iCodPai As Integer
Private iCodProvi As Integer

Public Function CodigoDeActualizacion2_12_00_05A2_12_05_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    V_2_12_5_Storeds_0
        
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_00_05A2_12_05_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_00_05A2_12_05_00 = False
End Function

Private Sub V_2_12_5_Storeds_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ADD_VOLADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ADD_VOLADJ]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_ADD_VOLADJ(@COMP VARCHAR(80),@PROVE VARCHAR(50),@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT,@VOL FLOAT,@MONGS VARCHAR(50),@MONPOR VARCHAR(50),@FECADJ DATETIME,@DENPROCE VARCHAR(100) ) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Inserta el registro correspondiente en VOLADJ" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIACOMP AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONPORTAL AS VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VOLPORTAL AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVGS AS FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Obtiene el ID de la compaNYia compradora" & vbCrLf
sConsulta = sConsulta & "SET @CIACOMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "-- Obtiene el ID de la compaNYia proveedora" & vbCrLf
sConsulta = sConsulta & "SET @ID = (SELECT ID FROM CIAS WHERE COD=@PROVE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Comprueba si existe la moneda en el portal" & vbCrLf
sConsulta = sConsulta & "SET @MONPORTAL =(SELECT COD FROM MON WHERE COD=@MONPOR)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si existe el registro primero lo borra" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND CIA_PROVE=@ID AND CIA_COMP=@CIACOMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @MONPORTAL IS NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & " --Si existe la moneda en el portal se calcula el total y lo inserta," & vbCrLf
sConsulta = sConsulta & " --Calcula la equivalencia de la moneda en el portal" & vbCrLf
sConsulta = sConsulta & " SET @EQUIV = (SELECT EQUIV FROM MON WHERE COD=@MONPOR)" & vbCrLf
sConsulta = sConsulta & " IF NOT @EQUIV IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "     --Calcula el total del volumen en la moneda del portal" & vbCrLf
sConsulta = sConsulta & "     SET @VOLPORTAL=(@VOL) / @EQUIV  " & vbCrLf
sConsulta = sConsulta & "              INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @CIACOMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,@MONPORTAL,@VOLPORTAL,@FECADJ,@DENPROCE)" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @CIACOMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,@MONPORTAL,NULL,@FECADJ,@DENPROCE)    " & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " --si no existe la moneda en el portal lo inserta a null" & vbCrLf
sConsulta = sConsulta & " INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @CIACOMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,NULL,NULL,@FECADJ,@DENPROCE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_12_05_00A2_12_05_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sComando As String
Dim sfecha As String
Dim oSQLServer As SQLDMO.SQLServer
Dim oJob As SQLDMO.Job
Dim oJobStep As SQLDMO.JobStep
Dim idStep As Integer
Dim intCont As Integer
Dim bExiste As Boolean
Dim iJob As Integer

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        

    Set oSQLServer = New SQLDMO.SQLServer
    oSQLServer.Connect gServer, gUID, gPWD
    
    If oSQLServer Is Nothing Then
        sConsulta = "ROLLBACK TRANSACTION"
        ExecuteSQL gRDOCon, sConsulta
        CodigoDeActualizacion2_12_05_00A2_12_05_01 = False
        Exit Function
    End If
    
    For intCont = 1 To oSQLServer.JobServer.Jobs.Count
        If UCase(oSQLServer.JobServer.Jobs.Item(intCont).Name) = UCase("JOBACTPROVE_" & gBD) Then
            bExiste = True
            iJob = intCont
            Exit For
        End If
    Next intCont
    
    'Si el job existe no hace nada
    If bExiste = True Then
        'Miro a ver si el job es TSQL y lo cambio
        Set oJob = oSQLServer.JobServer.Jobs.Item(iJob)
        Set oJobStep = oJob.JobSteps.Item(1)
        
        If oJobStep.SubSystem = "TSQL" Then
            oJobStep.BeginAlter
                idStep = 1
                
                oJobStep.SubSystem = "CmdExec"
            
                sComando = "osql -E -S " & gServer & " -Q " & """Exec " & gBD & ".dbo.SP_JOB_ACTUALIZAR_PROV_GS"""
                oJobStep.Command = sComando  'El paso ejecutar� el store procedure SP_AR_CALCULAR
            oJobStep.DoAlter
        End If
        Set oJobStep = Nothing
        Set oJob = Nothing
    
    End If
    oSQLServer.Disconnect
    Set oSQLServer = Nothing

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_05_00A2_12_05_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_00A2_12_05_01 = False
End Function

Public Function CodigoDeActualizacion2_12_05_01A2_12_05_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sComando As String
Dim sfecha As String
Dim intCont As Integer
Dim bExiste As Boolean
Dim iJob As Integer

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Se modificar� la stored de cambio de moneda"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If

    
    V_2_12_5_Storeds_2

    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_05_01A2_12_05_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_01A2_12_05_02 = False
    
End Function



Private Sub V_2_12_5_Storeds_2()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MON_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE MON_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM MON WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE MON SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS_MON SET MON_PORTAL=@NEW WHERE MON_PORTAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VOLADJ SET MONPOR=@NEW WHERE MONPOR=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE MON CHECK CONSTRAINT ALL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE CIAS_MON CHECK CONSTRAINT ALL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE VOLADJ CHECK CONSTRAINT ALL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_12_05_03A2_12_05_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sComando As String
Dim sfecha As String
Dim intCont As Integer
Dim bExiste As Boolean
Dim iJob As Integer

    On Error GoTo error
    
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Modificar tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_12_5_Tablas_4
        
    frmProgreso.lblDetalle = "Modificar stored procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_12_5_Storeds_4

    frmProgreso.lblDetalle = "Modificar triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_12_5_Triggers_4
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_05_03A2_12_05_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_03A2_12_05_04 = False
    
End Function


Private Sub V_2_12_5_Tablas_4()
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.PARGEN_MAIL ADD" & vbCrLf
sConsulta = sConsulta & "   CC VARCHAR(500) NULL," & vbCrLf
sConsulta = sConsulta & "   CCO VARCHAR(500) NULL," & vbCrLf
sConsulta = sConsulta & "   RESPUESTAMAIL VARCHAR(500) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   RPTPATH VARCHAR(500) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_12_5_Storeds_4()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIMINAR_PROCE @CIA_COMP SMALLINT, @ANYO SMALLINT, @GMN1 VARCHAR(50), @PROCE SMALLINT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curELIMPROCE CURSOR LOCAL FOR SELECT ID FROM  PROCE_OFE WHERE CIA_COMP=@CIA_COMP AND ANYO=@ANYO AND  GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curELIMPROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curELIMPROCE INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ITEM_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ITEM_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_GR_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_GR_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM OFE_ATRIB WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM PROCE_OFE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM ITEM_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM GRUPO_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "  DELETE FROM PROCE_OFE WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curELIMPROCE INTO @ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curELIMPROCE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curELIMPROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_MON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_ELIM_MON  (@MON_COD VARCHAR(50),@CODMONCENTRAL VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXISTE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMON INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMONCEN INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDMON=(SELECT ID FROM MON WHERE COD=@MON_COD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDMON IS NOT NULL " & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "    SET @IDMONCEN=(SELECT ID FROM MON WHERE COD=@CODMONCENTRAL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM CIAS WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM PAI WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE PAI SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM USU WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE USU SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM MON WHERE ID=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_PAIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_PAIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_ELIM_PAIS  (@PAI_COD VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PAI WHERE COD=@PAI_COD" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_PROVI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_PROVI]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_ELIM_PROVI  (@PAI_COD VARCHAR(50),@PROVI_COD VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PAIS AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ID_PAIS=(SELECT ID FROM PAI WHERE COD=@PAI_COD)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROVI WHERE PAI=@ID_PAIS AND COD=@PROVI_COD " & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_MON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_MON (@COD varchar(50), @DEN varchar(100), @EQUIV FLOAT,@EQ_ACT TINYINT) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMON INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDMON=MAX(ID) FROM MON" & vbCrLf
sConsulta = sConsulta & "IF @IDMON IS NULL " & vbCrLf
sConsulta = sConsulta & "   SET @IDMON=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @IDMON=@IDMON+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MON (ID,COD, DEN, EQUIV,EQ_ACT) VALUES (@IDMON,@COD, @DEN, @EQUIV,@EQ_ACT)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PAIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PAIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PAIS (@COD varchar(50), @DEN varchar(100), @MON varchar(50)=NULL) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPAIS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMON INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDPAIS=MAX(ID) FROM PAI" & vbCrLf
sConsulta = sConsulta & "IF @IDPAIS IS NULL " & vbCrLf
sConsulta = sConsulta & "   SET @IDPAIS=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @IDPAIS=@IDPAIS+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDMON=(SELECT ID FROM MON WHERE COD=@MON)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PAI (ID,COD, DEN, MON) VALUES (@IDPAIS,@COD, @DEN, @IDMON)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVI]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVI (@PAI varchar(50),@COD varchar(50), @DEN varchar(100)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVI INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPAIS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDPAIS=(SELECT ID FROM PAI WHERE COD=@PAI)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDPROVI=MAX(ID) FROM PROVI WHERE PAI=@IDPAIS" & vbCrLf
sConsulta = sConsulta & "IF @IDPROVI IS NULL " & vbCrLf
sConsulta = sConsulta & "   SET @IDPROVI=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @IDPROVI=@IDPROVI+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVI (PAI,ID,COD,DEN) VALUES (@IDPAIS,@IDPROVI,@COD, @DEN)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_MON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_MODIF_MON  (@COD VARCHAR(50),@DEN VARCHAR(100),@EQUIV FLOAT,@EQ_ACT TINYINT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE MON SET DEN=@DEN,EQUIV=@EQUIV,EQ_ACT=@EQ_ACT WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_PAIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_PAIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_MODIF_PAIS  (@COD VARCHAR(50),@DEN VARCHAR(100),@MON VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_MON INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ID_MON=(SELECT ID FROM MON WHERE COD=@MON)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE PAI SET DEN=@DEN,MON=@ID_MON WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_PROVI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_PROVI]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_MODIF_PROVI  (@PAIS VARCHAR(50),@COD VARCHAR(50),@DEN VARCHAR(100))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PAIS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ID_PAIS=(SELECT ID FROM PAI WHERE COD=@PAIS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROVI SET DEN=@DEN WHERE PAI=@ID_PAIS AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MON_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE MON_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM MON WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE MON SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS_MON SET MON_PORTAL=@NEW WHERE MON_PORTAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VOLADJ SET MONPOR=@NEW WHERE MONPOR=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PAIS_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PAIS_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PAIS_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PAI SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVI_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVI_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROVI_COD (@PAIS VARCHAR(50),@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PAIS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_PAIS=(SELECT ID FROM PAI WHERE COD=@PAIS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVI SET COD=@NEW WHERE PAI=@ID_PAIS AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Private Sub V_2_12_5_Triggers_4()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[MON_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [MON_TG_INS] ON dbo.MON" & vbCrLf
sConsulta = sConsulta & "FOR INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_MON_Ins CURSOR FOR SELECT ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_MON_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_MON_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          UPDATE MON SET FECACT=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          FETCH NEXT FROM curTG_MON_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "Close curTG_MON_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_MON_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion2_12_05_02A2_12_05_03()
Dim sConsulta As String
Dim adores As ADODB.Recordset
Dim cm As ADODB.Command
Dim par As ADODB.Parameter
Dim sPORT_CON As String
Dim iResult As Long
Dim sMensaje As String
Dim iRespuesta As Integer
Dim bTransaccionEnCurso As Boolean
Dim bMon As Boolean
Dim bPai As Boolean
Dim bProvi As Boolean

'Si es una compradora integrar monedas, paises y provincias con el GS
'hecho con ADO

    On Error GoTo error
        
    frmProgreso.lblDetalle = "Recuperando datos de c�a compradora"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120

    Set adores = New ADODB.Recordset
    adores.Open "SELECT UNA_COMPRADORA FROM PARGEN_INTERNO", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If adores.EOF Then
        adores.Close
        Set adores = Nothing
        oADOConnection.Close
        Set oADOConnection = Nothing
        MsgBox "No existen par�metros internos", vbCritical
        CodigoDeActualizacion2_12_05_02A2_12_05_03 = False
        Exit Function
    End If
    If Not adores(0).Value Then
        adores.Close
        Set adores = Nothing
        sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        oADOConnection.Execute sConsulta
        oADOConnection.Close
        Set oADOConnection = Nothing
        CodigoDeActualizacion2_12_05_02A2_12_05_03 = True
        Exit Function
    End If
    adores.Close
    
    oADOConnection.Execute "BEGIN DISTRIBUTED TRAN"
    oADOConnection.Execute "SET XACT_ABORT ON"
    bTransaccionEnCurso = True
    
    V_2_12_5_Storeds_3
    
    adores.Open "SELECT FSGS_SRV,FSGS_BD FROM CIAS WHERE FSGS_SRV IS NOT NULL AND FSGS_BD IS NOT NULL AND FCEST=3 ", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If adores.EOF Then
        adores.Close
        Set adores = Nothing
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        oADOConnection.Close
        Set oADOConnection = Nothing
        MsgBox "No existe la compa��a compradora en la tabla CIAS", vbCritical
        CodigoDeActualizacion2_12_05_02A2_12_05_03 = False
        Exit Function
    ElseIf adores.RecordCount > 1 Then
        adores.Close
        Set adores = Nothing
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        oADOConnection.Close
        Set oADOConnection = Nothing
        MsgBox "Existe m�s de una compradora en la tabla CIAS", vbCritical
        CodigoDeActualizacion2_12_05_02A2_12_05_03 = False
        Exit Function
    End If
        
    sFSGS_SRV = adores(0).Value
    sFSGS_BD = adores(1).Value
    adores.Close
    sPORT_CON = sFSGS_SRV & "." & sFSGS_BD & "." & "dbo."
        
    
    frmProgreso.lblDetalle = "Comprobando que los datos est�n enlazados"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    'Comprueba que los datos est�n enlazados
    Set cm = New ADODB.Command
    Set cm.ActiveConnection = oADOConnection

    Set par = cm.CreateParameter("FSGS", adVarChar, adParamInput, 200, sPORT_CON)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("RES", adInteger, adParamOutput)
    cm.Parameters.Append par

    cm.CommandType = adCmdStoredProc
    cm.CommandText = "COMPROBAR_DATOS"

    cm.Execute
    
    If oADOConnection.Errors.Count > 0 Then GoTo error
    iResult = NullToDbl0(cm.Parameters(1).Value)
    Set cm = Nothing
    
    If iResult <> 0 Then
        Set adores = Nothing
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        oADOConnection.Close
        Set oADOConnection = Nothing
        sMensaje = "La moneda central del GS no est� coordinada, debe coordinarla antes de continuar."
        MsgBox sMensaje, vbCritical
        CodigoDeActualizacion2_12_05_02A2_12_05_03 = False
        Exit Function
    Else 'Comprobar las tablas _TEMP para ver si tienen datos
        adores.Open "SELECT COUNT(*) FROM MON_TEMP ", oADOConnection, adOpenForwardOnly, adLockReadOnly
        If adores(0).Value > 0 Then
            bMon = True
        End If
        adores.Close
        adores.Open "SELECT COUNT(*) FROM PAI_TEMP ", oADOConnection, adOpenForwardOnly, adLockReadOnly
        If adores(0).Value > 0 Then
            bPai = True
        End If
        adores.Close
        adores.Open "SELECT COUNT(*) FROM PROVI_TEMP ", oADOConnection, adOpenForwardOnly, adLockReadOnly
        If adores(0).Value > 0 Then
            bProvi = True
        End If
        adores.Close
        If bMon Or bPai Or bProvi Then
            sMensaje = "Existen datos de monedas, paises y/o provincias que no est�n coordinados con el GS."
            sMensaje = sMensaje & vbCrLf & "Debe coordinar los datos antes de continuar."
            Set adores = Nothing
            MsgBox sMensaje, vbCritical
            MostrarDatosSinCoordinar bMon, bPai, bProvi
            oADOConnection.Close
            Set oADOConnection = Nothing
            CodigoDeActualizacion2_12_05_02A2_12_05_03 = False
            Exit Function
        Else
            oADOConnection.Execute "DROP TABLE MON_TEMP"
            oADOConnection.Execute "DROP TABLE PAI_TEMP"
            oADOConnection.Execute "DROP TABLE PROVI_TEMP"
        End If
    End If
    frmProgreso.lblDetalle = "Coordinando monedas, paises y provincias"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    'Renombremos las tablas
    Set cm = New ADODB.Command
    Set cm.ActiveConnection = oADOConnection
    
    Set par = cm.CreateParameter("FSGS", adVarChar, adParamInput, 200, sPORT_CON)
    cm.Parameters.Append par

    cm.CommandType = adCmdStoredProc
    cm.CommandText = "COORDINAR_DATOS"

    cm.Execute
    
    If oADOConnection.Errors.Count > 0 Then GoTo error
    Set cm = Nothing

    frmProgreso.lblDetalle = "Ajustando tama�os de campos en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    Else
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 100
    End If
    
    
    Set cm = New ADODB.Command
    Set cm.ActiveConnection = oADOConnection
    
    Set par = cm.CreateParameter("SERVER", adVarChar, adParamInput, 100, sFSGS_SRV)
    cm.Parameters.Append par
    Set par = cm.CreateParameter("BD", adVarChar, adParamInput, 100, sFSGS_BD)
    cm.Parameters.Append par

    cm.CommandType = adCmdStoredProc
    cm.CommandText = "CAMBIAR_TAM_CAMPOS"

    cm.Execute
    
    If oADOConnection.Errors.Count > 0 Then GoTo error
    Set cm = Nothing
    
    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COMPROBAR_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[COMPROBAR_DATOS]" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COORDINAR_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[COORDINAR_DATOS]" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CAMBIAR_TAM_CAMPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[CAMBIAR_TAM_CAMPOS]" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "EXEC " & sPORT_CON & "SP_EXECUTESQL N'if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].[PAI_TG_INSUPD]'') and OBJECTPROPERTY(id, N''IsTrigger'') = 1) drop trigger [dbo].[PAI_TG_INSUPD]'"
    oADOConnection.Execute sConsulta
    sConsulta = "EXEC " & sPORT_CON & "SP_EXECUTESQL N'CREATE TRIGGER PAI_TG_INSUPD ON dbo.PAI" & vbCrLf
    sConsulta = sConsulta & " FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & " AS" & vbCrLf
    sConsulta = sConsulta & " DECLARE @COD VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & " DECLARE curTG_Pai_Ins CURSOR FOR SELECT COD FROM INSERTED" & vbCrLf
    sConsulta = sConsulta & " OPEN curTG_Pai_Ins" & vbCrLf
    sConsulta = sConsulta & " FETCH NEXT FROM curTG_Pai_Ins INTO @COD" & vbCrLf
    sConsulta = sConsulta & " WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & " UPDATE PAI SET FECACT=GETDATE() WHERE COD=@COD" & vbCrLf
    sConsulta = sConsulta & " FETCH NEXT FROM curTG_Pai_Ins INTO @COD" & vbCrLf
    sConsulta = sConsulta & " End" & vbCrLf
    sConsulta = sConsulta & " Close curTG_Pai_Ins" & vbCrLf
    sConsulta = sConsulta & " DEALLOCATE curTG_Pai_Ins'" & vbCrLf
    oADOConnection.Execute sConsulta
            
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    oADOConnection.Execute sConsulta
    
    If bTransaccionEnCurso Then
        oADOConnection.Execute "COMMIT TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
    End If
    oADOConnection.Close
    Set oADOConnection = Nothing
''
''    Set oADOConnection = New ADODB.Connection
''    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & sFSGS_SRV & ";Database=" & sFSGS_BD & ";", "summitapp", "3x9ab004lp5mxs4rryqw"
''    oADOConnection.CursorLocation = adUseClient
''    oADOConnection.CommandTimeout = 120
''    oADOConnection.Execute "BEGIN TRANSACTION"
''    oADOConnection.Execute "SET XACT_ABORT ON"
''    oADOConnection.Execute "ALTER TABLE dbo.MON ALTER COLUMN [FSP_COD] [varchar] (" & iCodMon & ") NULL"
''    oADOConnection.Execute "UPDATE MON SET FSP_COD=COD"
''    oADOConnection.Execute "ALTER TABLE dbo.PAI ALTER COLUMN [FSP_COD] [varchar] (" & iCodPai & ") NULL"
''    oADOConnection.Execute "UPDATE PAI SET FSP_COD=COD"
''    oADOConnection.Execute "ALTER TABLE dbo.PROVI ALTER COLUMN [FSP_PAI] [varchar] (" & iCodPai & ") NULL"
''    oADOConnection.Execute "ALTER TABLE dbo.PROVI ALTER COLUMN [FSP_COD] [varchar] (" & iCodProvi & ") NULL"
''    oADOConnection.Execute "UPDATE PROVI SET FSP_PAI=PAI.COD ,FSP_COD=PROVI.COD FROM PROVI INNER JOIN PAI  ON PROVI.PAI=PAI.COD"
''    oADOConnection.Execute "COMMIT TRANSACTION"
''    oADOConnection.Execute "SET XACT_ABORT OFF"
        
    CodigoDeActualizacion2_12_05_02A2_12_05_03 = True
    Exit Function
    
error:

    MsgBox oADOConnection.Errors(0).Description, vbCritical + vbOKOnly
   'Resume 0
    If bTransaccionEnCurso Then
        oADOConnection.Execute "SET XACT_ABORT OFF"
    End If
    oADOConnection.Close
    Set oADOConnection = Nothing
    CodigoDeActualizacion2_12_05_02A2_12_05_03 = False

End Function


'''Public Sub V_2_12_5_AjustarCampos_3(ByVal sFSGS As String)
'''Dim oSQLServer As SQLDMO.SQLServer
'''Dim oDMOBD As SQLDMO.Database
'''Dim oTable As SQLDMO.Table
'''Dim iDenMon As Integer
'''Dim iDenPai As Integer
'''Dim iDenProvi As Integer
'''Dim par As ADODB.Parameter
'''Dim cm As ADODB.Command
'''Dim sServerGS
'''
'''sServerGS = sFSGS_SRV
'''If Left(sServerGS, 1) = "[" Then
'''    sServerGS = Right(sServerGS, Len(sServerGS) - 1)
'''End If
'''If Right(sServerGS, 1) = "]" Then
'''    sServerGS = Left(sServerGS, Len(sServerGS) - 1)
'''End If
'''Set oSQLServer = New SQLDMO.SQLServer
'''oSQLServer.Connect sServerGS, "summitapp", "3x9ab004lp5mxs4rryqw"  'Se conecta
'''
'''Set oDMOBD = oSQLServer.Databases(sFSGS_BD)
'''
'''Set oTable = oDMOBD.Tables("MON")
'''If Err <> 0 Then
'''    Err.Clear
'''End If
'''iCodMon = oTable.Columns("COD").Length
'''iDenMon = oTable.Columns("DEN").Length
'''
'''Set oTable = oDMOBD.Tables("PAI")
'''If Err <> 0 Then
'''    Err.Clear
'''End If
'''iCodPai = oTable.Columns("COD").Length
'''iDenPai = oTable.Columns("DEN").Length
'''
'''Set oTable = oDMOBD.Tables("PROVI")
'''If Err <> 0 Then
'''    Err.Clear
'''End If
'''iCodProvi = oTable.Columns("COD").Length
'''iDenProvi = oTable.Columns("DEN").Length
'''
'''oSQLServer.Close
'''Set oSQLServer = Nothing
'''Set oDMOBD = Nothing
'''Set oTable = Nothing
'''
'''oADOConnection.Execute "DROP INDEX MON.IX_MON_01"
'''oADOConnection.Execute "DROP INDEX MON.IX_MON_02"
'''oADOConnection.Execute "ALTER TABLE MON ALTER COLUMN [COD] [varchar] (" & iCodMon & ") NOT NULL"
'''oADOConnection.Execute "ALTER TABLE MON ALTER COLUMN [DEN] [varchar] (" & iDenMon & ") NOT NULL"
'''oADOConnection.Execute "ALTER TABLE VOLADJ ALTER COLUMN [MONPOR] [varchar] (" & iCodMon & ") NOT NULL"
'''oADOConnection.Execute "ALTER TABLE CIAS_MON ALTER COLUMN [MON_PORTAL] [varchar] (" & iCodMon & ") NOT NULL"
'''oADOConnection.Execute "CREATE  UNIQUE  INDEX [IX_MON_01] ON [dbo].[MON]([COD]) ON [PRIMARY]"
'''oADOConnection.Execute "CREATE  INDEX [IX_MON_02] ON [dbo].[MON]([DEN]) ON [PRIMARY]"
'''oADOConnection.Execute "ALTER TABLE CIAS_MON ADD CONSTRAINT [FK_CIAS_MON_MON] FOREIGN KEY ([MON_PORTAL]) REFERENCES [dbo].[MON] ([COD])"
'''oADOConnection.Execute "ALTER TABLE VOLADJ ADD CONSTRAINT [FK_VOLADJ_MON] FOREIGN KEY  ([MONPOR]) REFERENCES [dbo].[MON] ([COD])"
'''oADOConnection.Execute "DROP INDEX PAI.IX_PAI_01"
'''oADOConnection.Execute "DROP INDEX PAI.IX_PAI_02"
'''oADOConnection.Execute "ALTER TABLE PAI ALTER COLUMN [COD] [varchar] (" & iCodPai & ") NOT NULL"
'''oADOConnection.Execute "ALTER TABLE PAI ALTER COLUMN [DEN] [varchar] (" & iDenPai & ") NOT NULL"
'''oADOConnection.Execute "CREATE  UNIQUE  INDEX [IX_PAI_01] ON [dbo].[PAI]([COD]) ON [PRIMARY]"
'''oADOConnection.Execute "CREATE  INDEX [IX_PAI_02] ON [dbo].[PAI]([DEN]) ON [PRIMARY]"
'''oADOConnection.Execute "DROP INDEX PROVI.IX_PROVI_01"
'''oADOConnection.Execute "DROP INDEX PROVI.IX_PROVI_02"
'''oADOConnection.Execute "ALTER TABLE PROVI ALTER COLUMN [COD] [varchar] (" & iCodProvi & ") NOT NULL"
'''oADOConnection.Execute "ALTER TABLE PROVI ALTER COLUMN [DEN] [varchar] (" & iDenProvi & ") NOT NULL"
'''oADOConnection.Execute "CREATE  UNIQUE  INDEX [IX_PROVI_01] ON [dbo].[PROVI]([PAI], [COD]) ON [PRIMARY]"
'''oADOConnection.Execute "CREATE  INDEX [IX_PROVI_02] ON [dbo].[PROVI]([PAI], [DEN]) ON [PRIMARY]"
'''
'''End Sub


Private Sub V_2_12_5_Storeds_3()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COMPROBAR_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[COMPROBAR_DATOS]" & vbCrLf
oADOConnection.Execute sConsulta

sConsulta = "CREATE PROCEDURE COMPROBAR_DATOS @FSGS VARCHAR(200),@RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL nvarchar(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @RESCHAR VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Crea las tablas con los datos de GS" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE MONGS (COD VARCHAR(50) NOT NULL,DEN VARCHAR(200) NOT NULL,EQUIV FLOAT NOT NULL,EQ_ACT TINYINT NOT NULL, FSP_COD varchar (50) NULL,CENTRAL tinyint NULL ) " & vbCrLf
sConsulta = sConsulta & "CREATE TABLE PAIGS (COD VARCHAR(50) NOT NULL,DEN VARCHAR(200) NOT NULL,MON VARCHAR(50) NULL, FSP_COD varchar (50) NULL ) " & vbCrLf
sConsulta = sConsulta & "CREATE TABLE PROVIGS (PAI VARCHAR(50) NOT NULL,COD VARCHAR(50) NOT NULL,DEN VARCHAR(200) NOT NULL,FSP_PAI VARCHAR(50) NULL, FSP_COD varchar (50) NULL ) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL='INSERT INTO MONGS (COD,DEN,EQUIV,EQ_ACT,FSP_COD) SELECT COD,DEN,EQUIV,EQ_ACT,FSP_COD FROM ' + @FSGS + 'MON '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "set @SQL='INSERT INTO PAIGS (COD,DEN,MON,FSP_COD) SELECT COD,DEN,MON,FSP_COD FROM ' + @FSGS + 'PAI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "set @SQL='INSERT INTO PROVIGS (PAI,COD,DEN,FSP_PAI,FSP_COD) SELECT PAI,COD,DEN,FSP_PAI,FSP_COD FROM ' + @FSGS + 'PROVI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Comprueba la moneda central" & vbCrLf
sConsulta = sConsulta & "set @SQL='UPDATE MONGS SET CENTRAL=1 WHERE MONGS.COD IN (SELECT MONCEN FROM ' + @FSGS + 'PARGEN_DEF WHERE ID=1) '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "UPDATE MONGS SET CENTRAL=0 WHERE CENTRAL IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @RESCHAR=MON.COD FROM MONGS LEFT JOIN MON ON MONGS.FSP_COD=MON.COD WHERE MONGS.CENTRAL=1" & vbCrLf
sConsulta = sConsulta & "IF @RESCHAR IS NULL" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @RES=100" & vbCrLf
sConsulta = sConsulta & "     RETURN" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "--Comprueba las monedas" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE MON_TEMP ( COD VARCHAR(50) NOT NULL, DEN VARCHAR(200) )" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MON_TEMP (COD,DEN) SELECT DISTINCT MON.COD,MON.DEN FROM CIAS INNER JOIN MON ON CIAS.MON=MON.ID WHERE MON.COD NOT IN (SELECT FSP_COD  FROM MONGS WHERE FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MON_TEMP (COD,DEN) SELECT DISTINCT MON.COD,MON.DEN FROM PAI INNER JOIN MON ON PAI.MON=MON.ID WHERE MON.COD NOT IN (SELECT FSP_COD  FROM MONGS WHERE FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MON_TEMP (COD,DEN) SELECT DISTINCT MON.COD,MON.DEN FROM USU INNER JOIN MON ON USU.MON=MON.ID WHERE MON.COD NOT IN (SELECT FSP_COD  FROM MONGS WHERE FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MON_TEMP (COD,DEN) SELECT DISTINCT MON.COD,MON.DEN FROM VOLADJ INNER JOIN MON ON VOLADJ.MONPOR=MON.COD WHERE MON.COD NOT IN (SELECT FSP_COD  FROM MONGS WHERE FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "--Comprueba los paises" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE PAI_TEMP ( COD VARCHAR(50) NOT NULL, DEN VARCHAR(200) )" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PAI_TEMP (COD,DEN) SELECT DISTINCT PAI.COD,PAI.DEN FROM CIAS INNER JOIN PAI ON CIAS.PAI=PAI.ID WHERE PAI.COD NOT IN (SELECT FSP_COD  FROM PAIGS WHERE FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PAI_TEMP (COD,DEN) SELECT DISTINCT PAI.COD,PAI.DEN FROM PARGEN_INTERNO INNER JOIN PAI ON PARGEN_INTERNO.PAI=PAI.ID WHERE PAI.COD NOT IN (SELECT FSP_COD  FROM PAIGS WHERE FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PAI_TEMP (COD,DEN) SELECT DISTINCT PAI.COD,PAI.DEN FROM PROVI INNER JOIN PAI ON PROVI.PAI=PAI.ID WHERE PAI.COD NOT IN (SELECT FSP_COD  FROM PAIGS WHERE FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "--Comprueba las provincias" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE PROVI_TEMP (PAI VARCHAR(50), COD VARCHAR(50) NOT NULL, DEN VARCHAR(200) )" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVI_TEMP (PAI,COD,DEN) SELECT DISTINCT PAI.COD,PROVI.COD,PROVI.DEN FROM CIAS INNER JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID INNER JOIN PAI ON PROVI.PAI =PAI.ID WHERE PROVI.COD NOT IN (SELECT FSP_COD  FROM PROVIGS WHERE PROVIGS.FSP_PAI=PAI.COD  AND FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @RES=0" & vbCrLf
oADOConnection.Execute sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COORDINAR_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[COORDINAR_DATOS]" & vbCrLf
oADOConnection.Execute sConsulta

sConsulta = "CREATE PROCEDURE  COORDINAR_DATOS @FSGS VARCHAR(200) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE  @CIA  INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEN VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQ_ACT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @CIA=ID FROM CIAS WHERE FCEST=3 AND FSGS_SRV IS NOT NULL AND FSGS_BD IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE CIAS_MON DROP CONSTRAINT [FK_CIAS_MON_MON] " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE VOLADJ DROP CONSTRAINT     [FK_VOLADJ_MON]" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[MON] ADD [GSCOD]  [varchar] (50)  NULL " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[MON] ALTER COLUMN [COD]  [varchar] (50) NOT NULL " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[MON] ALTER COLUMN [DEN]  [varchar] (200) NOT NULL " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[CIAS_MON] ALTER COLUMN [MON_PORTAL]  [varchar] (50)  NULL " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[VOLADJ] ALTER COLUMN [MONPOR]  [varchar] (50)  NULL " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE MON SET GSCOD=MONGS.COD,DEN=MONGS.DEN,EQUIV=MONGS.EQUIV,EQ_ACT=MONGS.EQ_ACT FROM MON INNER JOIN MONGS ON MON.COD=MONGS.FSP_COD'" & vbCrLf
sConsulta = sConsulta & "    UPDATE PARGEN_DEF SET MONCEN= (SELECT MON.ID FROM MON INNER JOIN MONGS ON MON.COD=MONGS.FSP_COD AND MONGS.CENTRAL=1)" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM CIAS_MON " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO CIAS_MON (CIA,MON_PORTAL,MON_GS,DEN_GS) SELECT @CIA,COD,COD,DEN FROM MONGS WHERE CENTRAL=1" & vbCrLf
sConsulta = sConsulta & "    UPDATE VOLADJ SET MONPOR=NULL WHERE MONPOR NOT IN (SELECT FSP_COD FROM MONGS WHERE FSP_COD IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE VOLADJ SET MONPOR=MON.GSCOD FROM VOLADJ INNER JOIN MON ON VOLADJ.MONPOR=MON.COD AND VOLADJ.MONPOR IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'DELETE FROM MON WHERE GSCOD IS NULL'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE MON SET COD=GSCOD'" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM MONGS WHERE COD IN (SELECT COD FROM MON)" & vbCrLf
sConsulta = sConsulta & "    SET @ID=NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID =MAX(ID) FROM MON " & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL OR @ID=0 " & vbCrLf
sConsulta = sConsulta & "          SET @ID=1" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @ID=@ID+1" & vbCrLf
sConsulta = sConsulta & "    DECLARE C_MON CURSOR LOCAL FOR SELECT COD,DEN,EQUIV,EQ_ACT FROM MONGS" & vbCrLf
sConsulta = sConsulta & "    OPEN C_MON" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C_MON INTO @COD,@DEN,@EQUIV,@EQ_ACT" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MON (ID,COD,DEN, EQUIV,EQ_ACT) VALUES (@ID,@COD,@DEN,@EQUIV,@EQ_ACT)" & vbCrLf
sConsulta = sConsulta & "                 SET @ID=@ID+1" & vbCrLf
sConsulta = sConsulta & "                 FETCH NEXT FROM C_MON INTO @COD,@DEN,@EQUIV,@EQ_ACT" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    CLOSE C_MON" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C_MON" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'ALTER TABLE MON DROP COLUMN GSCOD'" & vbCrLf
sConsulta = sConsulta & "    DROP TABLE MONGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[PAI] ADD [GSCOD]  [varchar] (50)  NULL " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[PAI] ADD [GSMON]  [varchar] (50)  NULL " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[PAI] ALTER COLUMN [COD]  [varchar] (50) NOT NULL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[PAI] ALTER COLUMN [DEN]  [varchar] (200) NOT NULL " & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE PAI SET GSCOD=PAIGS.COD,DEN=PAIGS.DEN,GSMON=PAIGS.MON FROM PAI INNER JOIN PAIGS ON PAI.COD=PAIGS.FSP_COD'" & vbCrLf
sConsulta = sConsulta & "    UPDATE PAI SET MON=NULL" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE PAI SET MON=MON.ID FROM PAI INNER JOIN MON ON PAI.GSMON=MON.COD'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'DELETE FROM PAI WHERE GSCOD IS NULL'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE PROVIGS SET FSP_PAI=PAI.GSCOD FROM PROVIGS INNER JOIN PAI ON PROVIGS.FSP_PAI=PAI.COD AND FSP_PAI IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE PAI SET COD=GSCOD'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'ALTER TABLE PAI DROP COLUMN GSCOD'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'ALTER TABLE PAI DROP COLUMN GSMON'" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM PAIGS WHERE COD IN (SELECT COD FROM PAI)" & vbCrLf
sConsulta = sConsulta & "    SET @ID=NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID =MAX(ID) FROM PAI" & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL OR @ID=0 " & vbCrLf
sConsulta = sConsulta & "          SET @ID=1" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @ID=@ID+1" & vbCrLf
sConsulta = sConsulta & "    DECLARE C_PAI CURSOR LOCAL FOR SELECT PAIGS.COD,PAIGS.DEN,MON.ID FROM PAIGS LEFT JOIN MON ON PAIGS.MON=MON.COD" & vbCrLf
sConsulta = sConsulta & "    OPEN C_PAI" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C_PAI INTO @COD,@DEN,@INT" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PAI (ID,COD,DEN, MON) VALUES (@ID,@COD,@DEN,@INT)" & vbCrLf
sConsulta = sConsulta & "                 SET @ID=@ID+1" & vbCrLf
sConsulta = sConsulta & "                 FETCH NEXT FROM C_PAI INTO @COD,@DEN,@INT" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    CLOSE C_PAI" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C_PAI" & vbCrLf
sConsulta = sConsulta & "    DROP TABLE PAIGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[PROVI] ADD [GSCOD]  [varchar] (50)  NULL " & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[PROVI] ALTER COLUMN [COD]  [varchar] (50) NOT NULL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE [dbo].[PROVI] ALTER COLUMN [DEN]  [varchar] (200) NOT NULL " & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE PROVI SET GSCOD=PROVIGS.COD,DEN=PROVIGS.DEN FROM PROVI INNER JOIN PAI ON PROVI.PAI=PAI.ID INNER JOIN PROVIGS ON PROVIGS.FSP_PAI=PAI.COD AND PROVIGS.FSP_COD=PROVI.COD'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'DELETE FROM PROVI WHERE GSCOD IS NULL'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'UPDATE PROVI SET COD=GSCOD'" & vbCrLf
sConsulta = sConsulta & "    exec sp_executesql N'ALTER TABLE PROVI DROP COLUMN GSCOD'" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM PROVIGS WHERE EXISTS (SELECT * FROM PROVI INNER JOIN PAI ON PROVI.PAI=PAI.ID WHERE PAI.COD=PROVIGS.FSP_PAI AND PROVI.COD=PROVIGS.COD)" & vbCrLf
sConsulta = sConsulta & "    DECLARE C_PAPROVI CURSOR LOCAL FOR SELECT DISTINCT PAI.ID FROM PROVIGS INNER JOIN PAI ON PROVIGS.PAI=PAI.COD" & vbCrLf
sConsulta = sConsulta & "    OPEN C_PAPROVI" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C_PAPROVI INTO @INT" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @ID=NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID =MAX(ID) FROM PROVI WHERE PAI=@INT" & vbCrLf
sConsulta = sConsulta & "        IF @ID IS NULL OR @ID=0 " & vbCrLf
sConsulta = sConsulta & "          SET @ID=1" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @ID=@ID+1" & vbCrLf
sConsulta = sConsulta & "       DECLARE C_PROVI CURSOR LOCAL FOR SELECT PROVIGS.COD,PROVIGS.DEN FROM PROVIGS INNER JOIN PAI ON PROVIGS.PAI=PAI.COD AND PAI.ID=@INT" & vbCrLf
sConsulta = sConsulta & "       OPEN C_PROVI" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM C_PROVI INTO @COD,@DEN" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "                 INSERT INTO PROVI (PAI,ID,COD,DEN) VALUES (@INT,@ID,@COD,@DEN)" & vbCrLf
sConsulta = sConsulta & "                 SET @ID=@ID+1" & vbCrLf
sConsulta = sConsulta & "                 FETCH NEXT FROM C_PROVI INTO @COD,@DEN" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "       CLOSE C_PROVI" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE C_PROVI" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM C_PAPROVI INTO @INT" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "    CLOSE C_PAPROVI" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C_PAPROVI" & vbCrLf
sConsulta = sConsulta & "    DROP TABLE PROVIGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
oADOConnection.Execute sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CAMBIAR_TAM_CAMPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[CAMBIAR_TAM_CAMPOS]" & vbCrLf
oADOConnection.Execute sConsulta

sConsulta = "CREATE PROCEDURE CAMBIAR_TAM_CAMPOS @SERVER AS VARCHAR (100), @BD AS VARCHAR(100)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONCOD AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONDEN AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAIDEN AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVICOD AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVIDEN AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORT_CON VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT @MONCOD=CHARACTER_MAXIMUM_LENGTH FROM ' + @SERVER + '.' + @BD +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=''MON'' AND COLUMN_NAME=''COD'''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@MONCOD AS INT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @MONCOD=@MONCOD OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT @MONDEN=CHARACTER_MAXIMUM_LENGTH FROM ' + @SERVER + '.' + @BD +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=''MON'' AND COLUMN_NAME=''DEN'''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@MONDEN AS INT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @MONDEN=@MONDEN OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT @PAICOD=CHARACTER_MAXIMUM_LENGTH FROM ' + @SERVER + '.' + @BD +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=''PAI'' AND COLUMN_NAME=''COD'''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@PAICOD AS INT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @PAICOD=@PAICOD OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT @PAIDEN=CHARACTER_MAXIMUM_LENGTH FROM ' + @SERVER + '.' + @BD +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=''PAI'' AND COLUMN_NAME=''DEN'''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@PAIDEN AS INT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @PAIDEN=@PAIDEN OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT @PROVICOD=CHARACTER_MAXIMUM_LENGTH FROM ' + @SERVER + '.' + @BD +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=''PROVI'' AND COLUMN_NAME=''COD'''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@PROVICOD AS INT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @PROVICOD=@PROVICOD OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT @PROVIDEN=CHARACTER_MAXIMUM_LENGTH FROM ' + @SERVER + '.' + @BD +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=''PROVI'' AND COLUMN_NAME=''DEN'''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM='@PROVIDEN AS INT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @PROVIDEN=@PROVIDEN OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP INDEX MON.IX_MON_01" & vbCrLf
sConsulta = sConsulta & "DROP INDEX MON.IX_MON_02" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE MON ALTER COLUMN [COD] [varchar] (' + cast(@MONCOD as varchar(50)) + ') NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE MON ALTER COLUMN [DEN] [varchar] (' + cast(@MONDEN as varchar(50)) + ') NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE VOLADJ ALTER COLUMN [MONPOR] [varchar] (' + cast(@MONCOD as varchar(50)) + ') NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE CIAS_MON ALTER COLUMN [MON_PORTAL] [varchar] (' + cast(@MONCOD as varchar(50)) + ') NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "CREATE  UNIQUE  INDEX [IX_MON_01] ON [dbo].[MON]([COD]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_MON_02] ON [dbo].[MON]([DEN]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON ADD CONSTRAINT [FK_CIAS_MON_MON] FOREIGN KEY ([MON_PORTAL]) REFERENCES [dbo].[MON] ([COD])" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ ADD CONSTRAINT [FK_VOLADJ_MON] FOREIGN KEY  ([MONPOR]) REFERENCES [dbo].[MON] ([COD])" & vbCrLf
sConsulta = sConsulta & "DROP INDEX PAI.IX_PAI_01" & vbCrLf
sConsulta = sConsulta & "DROP INDEX PAI.IX_PAI_02" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE PAI ALTER COLUMN [COD] [varchar] (' + cast(@PAICOD as varchar(50)) + ') NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE PAI ALTER COLUMN [DEN] [varchar] (' + cast(@PAIDEN as varchar(50)) + ') NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "CREATE  UNIQUE  INDEX [IX_PAI_01] ON [dbo].[PAI]([COD]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PAI_02] ON [dbo].[PAI]([DEN]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "DROP INDEX PROVI.IX_PROVI_01" & vbCrLf
sConsulta = sConsulta & "DROP INDEX PROVI.IX_PROVI_02" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE PROVI ALTER COLUMN [COD] [varchar] (' +cast( @PROVICOD as varchar(50)) + ') NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE PROVI ALTER COLUMN [DEN] [varchar] (' + cast(@PROVIDEN as varchar(50)) + ') NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "CREATE  UNIQUE  INDEX [IX_PROVI_01] ON [dbo].[PROVI]([PAI], [COD]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PROVI_02] ON [dbo].[PROVI]([PAI], [DEN]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PORT_CON= @SERVER + '.' + @BD + '.dbo.SP_EXECUTESQL'" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE dbo.MON ALTER COLUMN [FSP_COD] [varchar] (' + cast(@MONCOD as varchar(50)) + ') NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC @PORT_CON @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC @PORT_CON N'UPDATE MON SET FSP_COD=COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE dbo.PAI ALTER COLUMN [FSP_COD] [varchar] (' + cast(@PAICOD as varchar(50)) + ') NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC @PORT_CON @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC @PORT_CON N'UPDATE PAI SET FSP_COD=COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE dbo.PROVI ALTER COLUMN [FSP_PAI] [varchar] (' + cast(@PAICOD as varchar(50)) + ') NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC @PORT_CON @SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQL='ALTER TABLE dbo.PROVI ALTER COLUMN [FSP_COD] [varchar] (' + cast(@PROVICOD as varchar(50)) + ') NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC @PORT_CON @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC @PORT_CON N'UPDATE PROVI SET FSP_PAI=P.COD ,FSP_COD=R.COD FROM PROVI R INNER JOIN PAI P  ON R.PAI=P.COD'" & vbCrLf
oADOConnection.Execute sConsulta

End Sub

Private Sub MostrarDatosSinCoordinar(ByVal bMon As Boolean, ByVal bPai As Boolean, ByVal bProvi As Boolean)
Dim adores As ADODB.Recordset

Set adores = New ADODB.Recordset
If bMon Then
    adores.Open "SELECT DISTINCT COD,DEN FROM MON_TEMP", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        frmCoordinarDatos.txtDatos.Text = "Las siguientes monedas deben coordinarse con las de GS:" & vbCrLf
        While Not adores.EOF
            frmCoordinarDatos.txtDatos.Text = frmCoordinarDatos.txtDatos.Text & vbTab & adores("COD").Value & vbTab & adores("DEN").Value & vbCrLf
            adores.MoveNext
        Wend
    End If
    adores.Close
End If
If bPai Then
    adores.Open "SELECT DISTINCT COD,DEN FROM PAI_TEMP", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If Not bMon Then
            frmCoordinarDatos.txtDatos.Text = "Los siguientes paises deben coordinarse con los de GS:" & vbCrLf
        Else
            frmCoordinarDatos.txtDatos.Text = frmCoordinarDatos.txtDatos.Text & vbCrLf & "Los siguientes paises deben coordinarse con los de GS:" & vbCrLf
        End If
        While Not adores.EOF
            frmCoordinarDatos.txtDatos.Text = frmCoordinarDatos.txtDatos.Text & vbTab & adores("COD").Value & vbTab & adores("DEN").Value & vbCrLf
            adores.MoveNext
        Wend
    End If
    adores.Close
End If
If bProvi Then
    adores.Open "SELECT DISTINCT PAI,COD,DEN FROM PROVI_TEMP", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If Not bMon And Not bPai Then
            frmCoordinarDatos.txtDatos.Text = "Las siguientes provincias deben coordinarse con las de GS:" & vbCrLf
        Else
            frmCoordinarDatos.txtDatos.Text = frmCoordinarDatos.txtDatos.Text & vbCrLf & "Las siguientes provincias deben coordinarse con las de GS:" & vbCrLf
        End If
        While Not adores.EOF
            frmCoordinarDatos.txtDatos.Text = frmCoordinarDatos.txtDatos.Text & vbTab & adores("PAI").Value & vbTab & adores("COD").Value & vbTab & adores("DEN").Value & vbCrLf
            adores.MoveNext
        Wend
    End If
    adores.Close
End If
Set adores = Nothing
oADOConnection.Execute "ROLLBACK TRAN"
oADOConnection.Execute "SET XACT_ABORT OFF"
frmCoordinarDatos.Show 1
End Sub

Public Function CodigoDeActualizacion2_12_05_04A2_12_05_05()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sComando As String
Dim sfecha As String
Dim oSQLServer As SQLDMO.SQLServer
Dim oJob As SQLDMO.Job
Dim oJobStep As SQLDMO.JobStep
Dim idStep As Integer
Dim intCont As Integer
Dim bExiste As Boolean
Dim iJob As Integer

    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        

    Set oSQLServer = New SQLDMO.SQLServer
    oSQLServer.Connect gServer, gUID, gPWD
    
    If oSQLServer Is Nothing Then
        sConsulta = "ROLLBACK TRANSACTION"
        ExecuteSQL gRDOCon, sConsulta
        CodigoDeActualizacion2_12_05_04A2_12_05_05 = False
        Exit Function
    End If
    
    For intCont = 1 To oSQLServer.JobServer.Jobs.Count
        If UCase(oSQLServer.JobServer.Jobs.Item(intCont).Name) = UCase("JOBACTPROVE_" & gBD) Then
            bExiste = True
            iJob = intCont
            Exit For
        End If
    Next intCont
    
    'Si el job existe no hace nada
    If bExiste = True Then
        'Miro a ver si el job es TSQL y lo cambio
        Set oJob = oSQLServer.JobServer.Jobs.Item(iJob)
        Set oJobStep = oJob.JobSteps.Item(1)
        
        'If oJobStep.SubSystem = "TSQL" Then
            oJobStep.BeginAlter
                idStep = 1
                
                oJobStep.SubSystem = "CmdExec"
            
                sComando = "osql -E -S " & gServer & " -Q " & """Exec " & gBD & ".dbo.SP_JOB_ACTUALIZAR_PROV_GS"""
                oJobStep.Command = sComando
            oJobStep.DoAlter
        'End If
        Set oJobStep = Nothing
        Set oJob = Nothing
    
    End If
    oSQLServer.Disconnect
    Set oSQLServer = Nothing

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_05_04A2_12_05_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_04A2_12_05_05 = False
End Function

Public Function CodigoDeActualizacion2_12_05_05A2_12_05_06()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sComando As String
Dim sfecha As String
Dim intCont As Integer
Dim bExiste As Boolean
Dim iJob As Integer

    On Error GoTo error
    
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Modificar tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_12_5_Tablas_6
        
    frmProgreso.lblDetalle = "Modificar stored procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_12_5_Storeds_6

    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_05_05A2_12_05_06 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_05A2_12_05_06 = False
    
End Function

Private Sub V_2_12_5_Tablas_6()
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[REGISTRO_EMAIL] (" & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SUBJECT] [varchar] (100)  NULL ," & vbCrLf
sConsulta = sConsulta & "   [PARA] [varchar] (100)  NULL ," & vbCrLf
sConsulta = sConsulta & "   [CC] [varchar] (100)  NULL ," & vbCrLf
sConsulta = sConsulta & "   [CCO] [varchar] (100)  NULL ," & vbCrLf
sConsulta = sConsulta & "   [DIR_RESPUESTA] [varchar] (100)  NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECHA] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [TIPO] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CUERPO] [text]  NULL ," & vbCrLf
sConsulta = sConsulta & "   [ACUSE_RECIBO] [tinyint] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_REGISTRO_EMAIL] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_REGISTRO_EMAIL_TIPO] DEFAULT (0) FOR [TIPO]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_REGISTRO_EMAIL_ACUSE_RECIBO] DEFAULT (0) FOR [ACUSE_RECIBO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_REGISTRO_EMAIL_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[REGISTRO_EMAIL_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID_REG] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CIA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATA] [image] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL_ADJUN] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_REGISTRO_EMAIL_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID_REG]," & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL_ADJUN] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_REGISTRO_EMAIL_ADJUN_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_REGISTRO_EMAIL_ADJUN_REGISTRO_EMAIL] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [ID_REG]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[REGISTRO_EMAIL] (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
  sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER REGISTRO_EMAIL_ADJUN_TG_INS_UPD ON dbo.REGISTRO_EMAIL_ADJUN " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REGISTRO_EMAIL_ADJUN " & vbCrLf
sConsulta = sConsulta & "   SET FECACT =GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM REGISTRO_EMAIL_ADJUN A" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "            ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[USU] ADD CONSTRAINT [DF_USU_TIPOEMAIL] DEFAULT (0) FOR [TIPOEMAIL]"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE USU SET TIPOEMAIL= 0 WHERE TIPOEMAIL IS NULL"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE [dbo].[USU] ALTER COLUMN TIPOEMAIL TINYINT NOT NULL"
ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_2_12_5_Storeds_6()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DOWNLOAD_ADJUNTO_EMAIL @CIA INT,@ID_REG INT, @ADJUN INT,@INIT INT,@SIZE INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM REGISTRO_EMAIL_ADJUN WHERE CIA=@CIA AND ID_REG=@ID_REG AND ID=@ADJUN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  REGISTRO_EMAIL_ADJUN.DATA @textpointer @INIT  @LSIZE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion2_12_05_06A2_12_05_07()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sComando As String
Dim sfecha As String
Dim intCont As Integer
Dim bExiste As Boolean
Dim iJob As Integer

    On Error GoTo error
    
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Modificar triggers"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_12_5_Triggers_7
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_05_06A2_12_05_07 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_06A2_12_05_07 = False
    
End Function

Private Sub V_2_12_5_Triggers_7()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_PORT_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_PORT_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [CIAS_PORT_TG_INS] ON [CIAS_PORT] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@PORT INT,@FPEST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_PORT_Ins CURSOR FOR SELECT CIA,PORT,FPEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_PORT_Ins INTO @CIA,@PORT,@FPEST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS_PORT SET FECINS=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       IF @FPEST=2 UPDATE CIAS_PORT SET FECDESACTFP=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       IF @FPEST=3 UPDATE CIAS_PORT SET FECACTFP=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_PORT_Ins INTO @CIA,@PORT,@FPEST" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_PORT_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_PORT_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [CIAS_PORT_TG_UPD] ON [CIAS_PORT] " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@PORT INT,@ESTOLD INT, @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_PORT_Upd CURSOR FOR SELECT CIA,PORT,FPEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_PORT_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_PORT_Upd INTO @CIA,@PORT,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS_PORT SET FECLASTUPD=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       SET @ESTOLD=(SELECT FPEST FROM DELETED WHERE CIA=@CIA AND PORT=@PORT)" & vbCrLf
sConsulta = sConsulta & "       IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "       BEGIN   " & vbCrLf
sConsulta = sConsulta & "           UPDATE CIAS_PORT SET FECACTFP=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           IF @ESTOLD<>2 AND @EST=2 " & vbCrLf
sConsulta = sConsulta & "           BEGIN   " & vbCrLf
sConsulta = sConsulta & "               UPDATE CIAS_PORT SET FECDESACTFP=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_PORT_Upd INTO @CIA,@PORT,@EST" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_PORT_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_PORT_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion2_12_05_07A2_12_05_08()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sComando As String
Dim sfecha As String
Dim intCont As Integer
Dim bExiste As Boolean
Dim iJob As Integer

    On Error GoTo error
    
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Modificar storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_12_5_Storeds_8
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_05_07A2_12_05_08 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_07A2_12_05_08 = False
    
End Function


Public Function CodigoDeActualizacion2_12_05_08A2_12_05_09()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sComando As String
Dim sfecha As String
Dim intCont As Integer
Dim bExiste As Boolean
Dim iJob As Integer

    On Error GoTo error
    
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    
    bTransaccionEnCurso = True
        
    frmProgreso.lblDetalle = "Modificar tabla de paises"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_12_5_Tablas_9
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.12.05.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_12_05_08A2_12_05_09 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_08A2_12_05_09 = False
    
End Function


Private Sub V_2_12_5_Storeds_8()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIMINAR_COMPANIA @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL_ADJUN WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REGISTRO_EMAIL WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "------------" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_12_5_Tablas_9()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PAI ADD" & vbCrLf
sConsulta = sConsulta & "   VALIDNIF tinyint NOT NULL CONSTRAINT DF_PAI_VALIDNIF DEFAULT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

