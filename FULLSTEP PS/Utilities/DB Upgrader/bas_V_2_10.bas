Attribute VB_Name = "bas_V_2_10"
Dim oDMOBD As SQLDMO.Database
Option Explicit


Public Function CodigoDeActualizacion2_9_50_07A2_10_00_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String

Dim oSQLServer As SQLDMO.SQLServer


sConexion = gRDOCon.Connect
sServer = gServer

sBD = gBD
sUsu = gUID
sPwd = gPWD
    


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    
        
Set oSQLServer = New SQLDMO.SQLServer
oSQLServer.Connect sServer, sUsu, sPwd  'Se conecta


Set oDMOBD = oSQLServer.Databases(sBD)
 
    'Eliminamos las storeds, triggers, foreign keys de la versi�n 2.9.5
    frmProgreso.lblDetalle = "Se eliminar�n las storeds, triggers y foreign keys de la 2.9.5"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_10_Eliminar_Defaults
    V_2_10_Eliminar_2_9_5
    
    'Actualizamos la estructura de tablas a la versi�n 2.10
    frmProgreso.lblDetalle = "Se actualizar� la estructura de tablas a la versi�n 2.10"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_10_Actualizar_Tablas
    
    
    'Creamos las Primary Keys e Indices
    frmProgreso.lblDetalle = "Se generar�n de nuevo las PKs, Indexes y UC de la versi�n 2.10"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    V_2_10_Constraints
    
    'Creamos las nuevas FKs
    frmProgreso.lblDetalle = "Se generar�n las Foreign Keys de la versi�n 2.10"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    V_2_10_Foreign_Keys
    
    
    
    'Creamos los nuevos storeds
    frmProgreso.lblDetalle = "Se generar�n las stored procedure de la versi�n 2.10"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    V_2_10_Storeds_1
    V_2_10_Storeds_2
    
    'Creamos los nuevos triggers
    frmProgreso.lblDetalle = "Se generar�n los triggers de la versi�n 2.10"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    V_2_10_Triggers
    
    

    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.10.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_9_50_07A2_10_00_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_07A2_10_00_00 = False
End Function



Public Function CodigoDeActualizacion2_10_00_00A2_10_00_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String

Dim oSQLServer As SQLDMO.SQLServer


sConexion = gRDOCon.Connect
sServer = gServer

sBD = gBD
sUsu = gUID
sPwd = gPWD
    


    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    
        
    'Eliminamos las storeds, triggers, foreign keys de la versi�n 2.9.5
    frmProgreso.lblDetalle = "Se modifica el stored de traspaso de proveedores a los gs"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
    
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL" & vbCrLf
sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL" & vbCrLf
sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.10.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_10_00_00A2_10_00_01 = True
    Exit Function
    
error:
    On Error Resume Next
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_00_00A2_10_00_01 = False
End Function


Public Function CodigoDeActualizacion2_10_00_01A2_10_00_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String

    
    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Nuevos storeds procedures para los atributos
    frmProgreso.lblDetalle = "Nuevos storeds procedures para los atributos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_ATRIB_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_ATRIB_OFE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_ATRIB_OFE_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_ATRIB_OFE_ITEM]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_ATRIB_OFE_GR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_ATRIB_OFE_GR]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE SP_MODIF_ATRIB_OFE @CIA_COMP int,  @ANYO int, @GMN1 varchar(50), @PROCE int, @ATRIB int, @PROVE varchar(50), @OFE int, @VALOR_NUM FLOAT = NULL, @VALOR_TEXT VARCHAR(2000) = NULL, @VALOR_FEC DATETIME = NULL, @VALOR_BOOL TINYINT = NULL, @OP varchar(1)='I' AS" & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
    sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_MODIF_ATRIB_OFE @ANYO= @ANYO, @GMN1 =@GMN1, @PROCE =@PROCE,@ATRIB=@ATRIB, @PROVE=@PROVE,@OFE=@OFE, @VALOR_NUM=@VALOR_NUM, @VALOR_TEXT=@VALOR_TEXT, @VALOR_FEC=@VALOR_FEC, @VALOR_BOOL=@VALOR_BOOL, @OP=@OP' " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT, @ATRIB int, @PROVE varchar(50), @OFE int, @VALOR_NUM FLOAT, @VALOR_TEXT VARCHAR(2000), @VALOR_FEC DATETIME , @VALOR_BOOL TINYINT , @OP varchar(1)' " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, " & vbCrLf
    sConsulta = sConsulta & "        @ANYO= @ANYO, " & vbCrLf
    sConsulta = sConsulta & "        @GMN1 =@GMN1, " & vbCrLf
    sConsulta = sConsulta & "        @PROCE =@PROCE," & vbCrLf
    sConsulta = sConsulta & "        @ATRIB=@ATRIB, " & vbCrLf
    sConsulta = sConsulta & "        @PROVE=@PROVE," & vbCrLf
    sConsulta = sConsulta & "        @OFE=@OFE, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_NUM=@VALOR_NUM, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_TEXT=@VALOR_TEXT, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_FEC=@VALOR_FEC, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_BOOL=@VALOR_BOOL," & vbCrLf
    sConsulta = sConsulta & "        @OP=@OP" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE SP_MODIF_ATRIB_OFE_ITEM @CIA_COMP int,  @ANYO int, @GMN1 varchar(50), @PROCE int,  @ITEM int, @ATRIB int, @PROVE varchar(50), @OFE int, @VALOR_NUM FLOAT = NULL, @VALOR_TEXT VARCHAR(2000) = NULL, @VALOR_FEC DATETIME = NULL, @VALOR_BOOL TINYINT = NULL, @OP varchar(1)='I' AS" & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
    sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_MODIF_ATRIB_OFE_ITEM @ANYO= @ANYO, @GMN1=@GMN1, @PROCE =@PROCE,@ITEM=@ITEM, @ATRIB=@ATRIB, @PROVE=@PROVE,@OFE=@OFE, @VALOR_NUM=@VALOR_NUM, @VALOR_TEXT=@VALOR_TEXT, @VALOR_FEC=@VALOR_FEC, @VALOR_BOOL=@VALOR_BOOL, @OP=@OP' " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT, @ITEM int, @ATRIB int, @PROVE varchar(50), @OFE int, @VALOR_NUM FLOAT, @VALOR_TEXT VARCHAR(2000), @VALOR_FEC DATETIME , @VALOR_BOOL TINYINT , @OP varchar(1)' " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, " & vbCrLf
    sConsulta = sConsulta & "        @ANYO= @ANYO, " & vbCrLf
    sConsulta = sConsulta & "        @GMN1 =@GMN1, " & vbCrLf
    sConsulta = sConsulta & "        @PROCE =@PROCE," & vbCrLf
    sConsulta = sConsulta & "        @ITEM = @ITEM," & vbCrLf
    sConsulta = sConsulta & "        @ATRIB=@ATRIB, " & vbCrLf
    sConsulta = sConsulta & "        @PROVE=@PROVE," & vbCrLf
    sConsulta = sConsulta & "        @OFE=@OFE, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_NUM=@VALOR_NUM, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_TEXT=@VALOR_TEXT, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_FEC=@VALOR_FEC, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_BOOL=@VALOR_BOOL," & vbCrLf
    sConsulta = sConsulta & "        @OP=@OP" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE PROCEDURE SP_MODIF_ATRIB_OFE_GR @CIA_COMP int,  @ANYO int, @GMN1 varchar(50), @PROCE int,  @GRUPO varchar(50), @ATRIB int, @PROVE varchar(50), @OFE int, @VALOR_NUM FLOAT = NULL, @VALOR_TEXT VARCHAR(2000) = NULL, @VALOR_FEC DATETIME = NULL, @VALOR_BOOL TINYINT = NULL, @OP varchar(1)='I' AS" & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
    sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_MODIF_ATRIB_OFE_GR @ANYO= @ANYO, @GMN1 =@GMN1, @PROCE =@PROCE,@GRUPO=@GRUPO, @ATRIB=@ATRIB, @PROVE=@PROVE,@OFE=@OFE, @VALOR_NUM=@VALOR_NUM, @VALOR_TEXT=@VALOR_TEXT, @VALOR_FEC=@VALOR_FEC, @VALOR_BOOL=@VALOR_BOOL, @OP=@OP' " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT, @GRUPO varchar(50), @ATRIB int, @PROVE varchar(50), @OFE int, @VALOR_NUM FLOAT, @VALOR_TEXT VARCHAR(2000), @VALOR_FEC DATETIME , @VALOR_BOOL TINYINT , @OP varchar(1)' " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, " & vbCrLf
    sConsulta = sConsulta & "        @ANYO= @ANYO, " & vbCrLf
    sConsulta = sConsulta & "        @GMN1 =@GMN1, " & vbCrLf
    sConsulta = sConsulta & "        @PROCE =@PROCE," & vbCrLf
    sConsulta = sConsulta & "        @GRUPO = @GRUPO," & vbCrLf
    sConsulta = sConsulta & "        @ATRIB=@ATRIB, " & vbCrLf
    sConsulta = sConsulta & "        @PROVE=@PROVE," & vbCrLf
    sConsulta = sConsulta & "        @OFE=@OFE, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_NUM=@VALOR_NUM, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_TEXT=@VALOR_TEXT, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_FEC=@VALOR_FEC, " & vbCrLf
    sConsulta = sConsulta & "        @VALOR_BOOL=@VALOR_BOOL," & vbCrLf
    sConsulta = sConsulta & "        @OP=@OP" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.10.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_10_00_01A2_10_00_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_00_01A2_10_00_02 = False
End Function
Public Function CodigoDeActualizacion2_10_00_02A2_10_00_03()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String

    
    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Nuevos storeds procedures para los atributos
    frmProgreso.lblDetalle = "Nuevos storeds procedures para las actividades"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_VISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_VISIBLE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = ""
    sConsulta = sConsulta & "CREATE PROCEDURE SP_ACT_VISIBLE @CIA int,@IDI varchar(20)" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
    sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
    sConsulta = sConsulta & "--la tabla temporal #mraiz contendr� las actividades pendientes de confirmar por el gerente del portal y en caso" & vbCrLf
    sConsulta = sConsulta & "--de que no haya nada pendiente, contendr� una copia de #raiz" & vbCrLf
    sConsulta = sConsulta & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Insertamos los datos correspondientes a #raiz" & vbCrLf
    sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4, act5 ) " & vbCrLf
    sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
    sConsulta = sConsulta & "  from cias_act5 a" & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act4 b" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
    sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
    sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act3 c" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
    sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act2 d" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act1 e" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4 ) " & vbCrLf
    sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
    sConsulta = sConsulta & "  from cias_act4 a" & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act3 b" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
    sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act2 c" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act1 d" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #raiz (act1, act2, act3 ) " & vbCrLf
    sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
    sConsulta = sConsulta & "  from cias_act3 a" & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act2 b" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act1 c" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #raiz (act1, act2 ) " & vbCrLf
    sConsulta = sConsulta & "select act1, act2 " & vbCrLf
    sConsulta = sConsulta & "  from cias_act2 a" & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from cias_act1 b" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #raiz (act1) " & vbCrLf
    sConsulta = sConsulta & "select act1 " & vbCrLf
    sConsulta = sConsulta & "  from cias_act1 " & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Insertamos los datos correspondientes a #mraiz" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4, act5 ) " & vbCrLf
    sConsulta = sConsulta & "select act1, act2, act3, act4, act5  " & vbCrLf
    sConsulta = sConsulta & "  from mcias_act5 a" & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act4 b" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA " & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
    sConsulta = sConsulta & "                      and a.act3 = b.act3" & vbCrLf
    sConsulta = sConsulta & "                      and a.act4 = b.act4 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act3 c" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = c.act2" & vbCrLf
    sConsulta = sConsulta & "                      and a.act3 = c.act3 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act2 d" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = d.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = d.act2)" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act1 e" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = e.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4 ) " & vbCrLf
    sConsulta = sConsulta & "select act1, act2, act3, act4  " & vbCrLf
    sConsulta = sConsulta & "  from mcias_act4 a" & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act3 b" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = b.act2" & vbCrLf
    sConsulta = sConsulta & "                      and a.act3 = b.act3 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act2 c" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = c.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = c.act2 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act1 d" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = d.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3 ) " & vbCrLf
    sConsulta = sConsulta & "select act1, act2, act3  " & vbCrLf
    sConsulta = sConsulta & "  from mcias_act3 a" & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act2 b" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = b.act1" & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = b.act2 )" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act1 c" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = c.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #mraiz (act1, act2 ) " & vbCrLf
    sConsulta = sConsulta & "select act1, act2 " & vbCrLf
    sConsulta = sConsulta & "  from mcias_act2 a" & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "   and not exists (select *" & vbCrLf
    sConsulta = sConsulta & "                     from mcias_act1 b" & vbCrLf
    sConsulta = sConsulta & "                    where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "                      and a.act1 = b.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #mraiz (act1) " & vbCrLf
    sConsulta = sConsulta & "select act1 " & vbCrLf
    sConsulta = sConsulta & "  from mcias_act1 " & vbCrLf
    sConsulta = sConsulta & " where cia=@CIA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Si no hab�a actividades pendientes de confirmar, se copia #raiz en #mraiz" & vbCrLf
    sConsulta = sConsulta & "if (select count(*) from #mraiz)=0" & vbCrLf
    sConsulta = sConsulta & "  insert into #mraiz select * from #raiz" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--La tabla #base va a contener las actividades de las cuales se obtendr�n la " & vbCrLf
    sConsulta = sConsulta & "--estructura completa (con asignadas, pendientes y hermanas de estas)" & vbCrLf
    sConsulta = sConsulta & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4,act5)" & vbCrLf
    sConsulta = sConsulta & "  select act1,act2,act3,act4,min(act5) " & vbCrLf
    sConsulta = sConsulta & "    from (select * from #raiz where act5 is not null" & vbCrLf
    sConsulta = sConsulta & "           Union" & vbCrLf
    sConsulta = sConsulta & "          select * from #mraiz where act5 is not null) a" & vbCrLf
    sConsulta = sConsulta & "group by act1,act2,act3,act4" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  insert into #base (act1,act2,act3,act4)" & vbCrLf
    sConsulta = sConsulta & "  select act1,act2,act3,min(act4) " & vbCrLf
    sConsulta = sConsulta & "    from (select * from #raiz where act4 is not null" & vbCrLf
    sConsulta = sConsulta & "           Union" & vbCrLf
    sConsulta = sConsulta & "          select * from #mraiz where act4 is not null) a" & vbCrLf
    sConsulta = sConsulta & "group by act1,act2,act3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  insert into #base (act1,act2,act3)" & vbCrLf
    sConsulta = sConsulta & "  select act1,act2,min(act3) " & vbCrLf
    sConsulta = sConsulta & "    from (select * from #raiz where act3 is not null" & vbCrLf
    sConsulta = sConsulta & "           Union" & vbCrLf
    sConsulta = sConsulta & "          select * from #mraiz where act3 is not null) a" & vbCrLf
    sConsulta = sConsulta & "group by act1,act2" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  insert into #base (act1,act2)" & vbCrLf
    sConsulta = sConsulta & "  select act1,min(act2) " & vbCrLf
    sConsulta = sConsulta & "    from (select * from #raiz where act2 is not null" & vbCrLf
    sConsulta = sConsulta & "           Union" & vbCrLf
    sConsulta = sConsulta & "          select * from #mraiz where act2 is not null) a" & vbCrLf
    sConsulta = sConsulta & "group by act1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  insert into #base (act1)" & vbCrLf
    sConsulta = sConsulta & "  select min(act1) " & vbCrLf
    sConsulta = sConsulta & "    from (select * from #raiz" & vbCrLf
    sConsulta = sConsulta & "           Union" & vbCrLf
    sConsulta = sConsulta & "          select * from #mraiz) a" & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--La tabla #Estructura contendr� toda la estructura completa de actividades asignadas, pendientes y hermanas" & vbCrLf
    sConsulta = sConsulta & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4,act5)" & vbCrLf
    sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.act4,a.id" & vbCrLf
    sConsulta = sConsulta & "  from act5 a " & vbCrLf
    sConsulta = sConsulta & "     inner join #base b " & vbCrLf
    sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
    sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
    sConsulta = sConsulta & "            and a.act3 = b.act3 " & vbCrLf
    sConsulta = sConsulta & "            and a.act4 = b.act4" & vbCrLf
    sConsulta = sConsulta & " Where b.act5 Is Not Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4)" & vbCrLf
    sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.id" & vbCrLf
    sConsulta = sConsulta & "  from act4 a " & vbCrLf
    sConsulta = sConsulta & "     inner join #base b " & vbCrLf
    sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
    sConsulta = sConsulta & "            and a.act2=b.act2 " & vbCrLf
    sConsulta = sConsulta & "            and a.act3 = b.act3" & vbCrLf
    sConsulta = sConsulta & " Where b.act5 Is Null " & vbCrLf
    sConsulta = sConsulta & "   And b.act4 Is Not Null" & vbCrLf
    sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
    sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
    sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
    sConsulta = sConsulta & "                      and a.act3=c.act3 " & vbCrLf
    sConsulta = sConsulta & "                      and a.id=c.act4)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3)" & vbCrLf
    sConsulta = sConsulta & "select a.act1,a.act2,a.id" & vbCrLf
    sConsulta = sConsulta & "  from act3 a " & vbCrLf
    sConsulta = sConsulta & "     inner join #base b " & vbCrLf
    sConsulta = sConsulta & "             on a.act1=b.act1 " & vbCrLf
    sConsulta = sConsulta & "            and a.act2=b.act2" & vbCrLf
    sConsulta = sConsulta & " Where b.act4 Is Null " & vbCrLf
    sConsulta = sConsulta & "   And b.act3 Is Not Null" & vbCrLf
    sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
    sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
    sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
    sConsulta = sConsulta & "                      and a.act2 = c.act2 " & vbCrLf
    sConsulta = sConsulta & "                      and a.id=c.act3)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #Estructura (Act1,Act2)" & vbCrLf
    sConsulta = sConsulta & "select a.act1,a.id" & vbCrLf
    sConsulta = sConsulta & "  from act2 a " & vbCrLf
    sConsulta = sConsulta & "     inner join #base b " & vbCrLf
    sConsulta = sConsulta & "             on a.act1=b.act1" & vbCrLf
    sConsulta = sConsulta & " Where b.act3 Is Null " & vbCrLf
    sConsulta = sConsulta & "   And b.act2 Is Not Null" & vbCrLf
    sConsulta = sConsulta & "   and not exists (SELECT * " & vbCrLf
    sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
    sConsulta = sConsulta & "                    where a.act1=c.act1 " & vbCrLf
    sConsulta = sConsulta & "                      and a.id = c.act2)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "insert into #Estructura (Act1)" & vbCrLf
    sConsulta = sConsulta & "select a.id" & vbCrLf
    sConsulta = sConsulta & "  from act1 a" & vbCrLf
    sConsulta = sConsulta & " where not exists (SELECT * " & vbCrLf
    sConsulta = sConsulta & "                     FROM #Estructura c " & vbCrLf
    sConsulta = sConsulta & "                    where a.id=c.act1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Actualizamos los campos actAsign y actPend con el valor correspondiente. Estos campos" & vbCrLf
    sConsulta = sConsulta & "--contendr�n el nivel al que la actividad est� asignada (actAsign) o en el caso de que est� pendiente (actPend), el nivel" & vbCrLf
    sConsulta = sConsulta & "-- al que lo est�" & vbCrLf
    sConsulta = sConsulta & "update #Estructura set actAsign=0 ,actPend=0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actAsign = 1" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1" & vbCrLf
    sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actAsign = 2" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
    sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actAsign = 3" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
    sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actAsign = 4" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
    sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actAsign = 5" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actPend = 1" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1" & vbCrLf
    sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actPend = 2" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
    sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actPend = 3" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
    sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actPend = 4" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
    sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "update #Estructura" & vbCrLf
    sConsulta = sConsulta & "Set actPend = 5" & vbCrLf
    sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
    sConsulta = sConsulta & "select #Estructura.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
    sConsulta = sConsulta & "       #Estructura.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
    sConsulta = sConsulta & "       #Estructura.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
    sConsulta = sConsulta & "       #Estructura.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
    sConsulta = sConsulta & "       #Estructura.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
    sConsulta = sConsulta & "       actasign, actpend" & vbCrLf
    sConsulta = sConsulta & "from #Estructura  " & vbCrLf
    sConsulta = sConsulta & "     left join act1 " & vbCrLf
    sConsulta = sConsulta & "            on #Estructura.act1 = act1.id" & vbCrLf
    sConsulta = sConsulta & "     left join act2 " & vbCrLf
    sConsulta = sConsulta & "            on #Estructura.act1=act2.act1 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act2 = act2.id" & vbCrLf
    sConsulta = sConsulta & "     left join act3 " & vbCrLf
    sConsulta = sConsulta & "            on #Estructura.act1=act3.act1 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act2 = act3.act2 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act3 = act3.id     left join act4 " & vbCrLf
    sConsulta = sConsulta & "            on #Estructura.act1=act4.act1 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act2 = act4.act2 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act3 = act4.act3 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act4 = act4.id" & vbCrLf
    sConsulta = sConsulta & "     left join act5 " & vbCrLf
    sConsulta = sConsulta & "            on #Estructura.act1=act5.act1 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act2 = act5.act2 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act3 = act5.act3 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act4 = act5.act4 " & vbCrLf
    sConsulta = sConsulta & "           and #Estructura.act5= act5.id" & vbCrLf
    sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
    sConsulta = sConsulta & "'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.10.00.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_10_00_02A2_10_00_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_00_02A2_10_00_03 = False
End Function

Public Function CodigoDeActualizacion2_10_00_03A2_10_00_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String

    
    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Nuevos storeds procedures para los atributos
    frmProgreso.lblDetalle = "Nuevos storeds procedures para la b�squeda de proveedores por actividad"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = ""
    sConsulta = sConsulta & "CREATE PROCEDURE SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL @SDEN VARCHAR(8),@SBUSCAR VARCHAR(50),@SCOMP INT AS" & vbCrLf
    sConsulta = sConsulta & "declare @IDIOMA1 AS VARCHAR(15)" & vbCrLf
    sConsulta = sConsulta & "declare @IDIOMA2 AS VARCHAR(15)" & vbCrLf
    sConsulta = sConsulta & "declare @IDIOMA3 AS VARCHAR(15)" & vbCrLf
    sConsulta = sConsulta & "declare @IDIOMA4 AS VARCHAR(15)" & vbCrLf
    sConsulta = sConsulta & "declare @IDIOMA5 AS VARCHAR(15)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @IDIOMA1= 'A1.' + @SDEN" & vbCrLf
    sConsulta = sConsulta & "SET @IDIOMA2= 'A2.' + @SDEN" & vbCrLf
    sConsulta = sConsulta & "SET @IDIOMA3= 'A3.' + @SDEN" & vbCrLf
    sConsulta = sConsulta & "SET @IDIOMA4= 'A4.' + @SDEN" & vbCrLf
    sConsulta = sConsulta & "SET @IDIOMA5= 'A5.' + @SDEN" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "declare @SQL NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "set @SQL = '" & vbCrLf
    sConsulta = sConsulta & "SELECT A1.ID ACT1, NULL ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA1 + ' DEN ,COUNT(J.CIA) AS PROVE, A1.COD CODACT1, NULL CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
    sConsulta = sConsulta & "FROM ACT1 A1" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1 FROM CIAS_ACT1 UNION SELECT CIA, ACT1 FROM CIAS_ACT2 UNION SELECT CIA, ACT1 FROM CIAS_ACT3 UNION SELECT CIA, ACT1 FROM CIAS_ACT4 UNION SELECT CIA, ACT1 FROM CIAS_ACT5) PP" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT1=A1.ID " & vbCrLf
    sConsulta = sConsulta & "WHERE ' +  @IDIOMA1+ ' LIKE ''%' + @SBUSCAR + '%'' " & vbCrLf
    sConsulta = sConsulta & "GROUP BY   A1.ID, A1.COD, ' + @IDIOMA1 + '" & vbCrLf
    sConsulta = sConsulta & "union" & vbCrLf
    sConsulta = sConsulta & "SELECT A2.ACT1, A2.ID ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA2 + ' DEN, COUNT(J.CIA) AS PROVE, A1.COD CODACT1, A2.COD CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
    sConsulta = sConsulta & "FROM ACT2 A2" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2 FROM CIAS_ACT2 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT5) PP" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=PP.CIA) J ON J.ACT2=A2.ID  AND J.ACT1=A2.ACT1" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT1 A1 ON A2.ACT1 = A1.ID" & vbCrLf
    sConsulta = sConsulta & "WHERE ' +  @IDIOMA2+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
    sConsulta = sConsulta & "GROUP BY  A2.ACT1, A2.ID, A1.COD,A2.COD,' + @IDIOMA2 + ' " & vbCrLf
    sConsulta = sConsulta & "union" & vbCrLf
    sConsulta = sConsulta & "SELECT A3.ACT1,A3.ACT2, A3.ID ACT3, NULL ACT4, NULL ACT5, ' + @IDIOMA3 + ' DEN, COUNT(J.CIA) AS PROVE, A1.COD CODACT1, A2.COD CODACT2, A3.COD CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
    sConsulta = sConsulta & "FROM ACT3 A3" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT5) PP" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT3=A3.ID AND J.ACT2=A3.ACT2 AND J.ACT1=A3.ACT1 " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT1 A1 ON A3.ACT1 = A1.ID" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT2 A2 ON A3.ACT1 = A2.ACT1 AND A3.ACT2 = A2.ID" & vbCrLf
    sConsulta = sConsulta & "WHERE ' +  @IDIOMA3+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
    sConsulta = sConsulta & "GROUP BY A3.ACT1,A3.ACT2,A3.ID, A1.COD, A2.COD, A3.COD, ' + @IDIOMA3 + '" & vbCrLf
    sConsulta = sConsulta & "union" & vbCrLf
    sConsulta = sConsulta & "SELECT A4.ACT1,A4.ACT2, A4.ACT3, A4.ID ACT4, NULL ACT5, ' + @IDIOMA4 + ' DEN, COUNT(J.CIA) AS PROVE, A1.COD CODACT1, A2.COD CODACT2, A3.COD CODACT3, A4.COD CODACT4, NULL CODACT5" & vbCrLf
    sConsulta = sConsulta & "FROM ACT4 A4" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT5) PP" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA)  J ON J.ACT4=A4.ID AND J.ACT3=A4.ACT3 AND J.ACT2=A4.ACT2 AND J.ACT1=A4.ACT1  " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT1 A1 ON A4.ACT1 = A1.ID" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT2 A2 ON A4.ACT1 = A2.ACT1 AND A4.ACT2 = A2.ID" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT3 A3 ON A4.ACT1 = A3.ACT1 AND A4.ACT2 = A3.ACT2 AND A4.ACT3 = A3.ID" & vbCrLf
    sConsulta = sConsulta & "WHERE ' +  @IDIOMA4+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
    sConsulta = sConsulta & "GROUP BY A4.ACT1,A4.ACT2,A4.ACT3,A4.ID, A1.COD, A2.COD, A3.COD, A4.COD, ' + @IDIOMA4 + '" & vbCrLf
    sConsulta = sConsulta & "union" & vbCrLf
    sConsulta = sConsulta & "SELECT A5.ACT1,A5.ACT2, A5.ACT3, A5.ACT4, A5.ID ACT5, ' + @IDIOMA5 + ' DEN, COUNT(J.CIA) AS PROVE, A1.COD CODACT1, A2.COD CODACT2, A3.COD CODACT3, A4.COD CODACT4, A5.COD CODACT5" & vbCrLf
    sConsulta = sConsulta & "FROM ACT5 A5" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN CIAS_ACT5 J ON J.ACT5=A5.ID AND J.ACT4=A5.ACT4 AND J.ACT3=A5.ACT3 AND J.ACT2=A5.ACT2  AND J.ACT1=A5.ACT1" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=J.CIA" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=J.CIA" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT1 A1 ON A5.ACT1 = A1.ID" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT2 A2 ON A5.ACT1 = A2.ACT1 AND A5.ACT2 = A2.ID" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT3 A3 ON A5.ACT1 = A3.ACT1 AND A5.ACT2 = A3.ACT2 AND A5.ACT3 = A3.ID" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ACT4 A4 ON A5.ACT1 = A4.ACT1 AND A5.ACT2 = A4.ACT2 AND A5.ACT3 = A4.ACT3 AND A5.ACT3 = A4.ID" & vbCrLf
    sConsulta = sConsulta & "WHERE ' +  @IDIOMA5+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
    sConsulta = sConsulta & "GROUP BY A5.ACT1,A5.ACT2,A5.ACT3,A5.ACT4,A5.ID, A1.COD, A2.COD, A3.COD, A4.COD, A5.COD, ' + @IDIOMA5 + '" & vbCrLf
    sConsulta = sConsulta & "ORDER BY 1,2,3,4,5'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "print @SQL" & vbCrLf
    sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.10.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_10_00_03A2_10_00_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_00_03A2_10_00_04 = False
End Function



Public Function CodigoDeActualizacion2_10_00_04A2_10_00_05()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String

    
    On Error GoTo error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "SET XACT_ABORT OFF"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
        
    'Nuevos storeds procedures para los atributos
    frmProgreso.lblDetalle = "Nuevos storeds procedures para la b�squeda de proveedores por actividad"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then
        frmProgreso.ProgressBar1.Value = 1
    End If
        
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOEMAIL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "           "
sConsulta = sConsulta & "                SET @CIAS_DEN=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_NIF=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PAI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_PROVI=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_CP=NULL" & vbCrLf
sConsulta = sConsulta & "                SET  @CIAS_POB=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_DIR=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_MON=NULL" & vbCrLf
sConsulta = sConsulta & "                SET @CIAS_URLCIA=NULL" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "                   FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                    IF @CIAS_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "                 END       " & vbCrLf
sConsulta = sConsulta & "              END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      ELSE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "                 SET @PARAM='@COD_PROVE_CIA VARCHAR(200), @ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                      @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = IDI, @TIPOEMAIL=TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   IF @USU_APE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE,  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P,@IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                    SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200),  @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI=@IDI, @TIPOEMAIL =@TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                 END                " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_APE=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_DEP=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_CAR=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_FAX=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_EMAIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO2=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @USU_TFNO_MOVIL=NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @IDI = NULL" & vbCrLf
sConsulta = sConsulta & "                 SET @TIPOEMAIL = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2,@USU_FAX=FAX," & vbCrLf
sConsulta = sConsulta & "                       @USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL, @IDI = IDI, @TIPOEMAIL=TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 WHERE " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                 IF @USU_APE IS NOT NULL                   " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                  @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                  @IDI =@IDI,@TIPOEMAIL =@TIPOEMAIL'    " & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                              @ID_P INT, @IDI VARCHAR(100), @TIPOEMAIL TINYINT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                      @ID_P=@USU, @IDI = @IDI, @TIPOEMAIL = @TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                   END                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END        " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @NOM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @COM=NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @FECACT=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   IF @NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   END     " & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='2.10.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
   
    CodigoDeActualizacion2_10_00_04A2_10_00_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_00_04A2_10_00_05 = False
End Function






Private Function V_2_10_Eliminar_2_9_5()
Dim sConsulta As String


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT1_FK_ACT]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT1] DROP CONSTRAINT CIAS_ACT1_FK_ACT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT3_FK_ACT2]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT3] DROP CONSTRAINT ACT3_FK_ACT2" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT2_FK_ACT2]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT2] DROP CONSTRAINT CIAS_ACT2_FK_ACT2" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SUBACT3_FK_SUBACT2]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT4] DROP CONSTRAINT SUBACT3_FK_SUBACT2" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT3_FK_ACT3]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT3] DROP CONSTRAINT CIAS_ACT3_FK_ACT3" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SUBACT4_FK_SUBACT3]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT5] DROP CONSTRAINT SUBACT4_FK_SUBACT3" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT4_FK_ACT4]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT4] DROP CONSTRAINT CIAS_ACT4_FK_ACT4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT5_FK_ACT5]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT5] DROP CONSTRAINT CIAS_ACT5_FK_ACT5" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT1_FK_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT1] DROP CONSTRAINT CIAS_ACT1_FK_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT2_FK_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT2] DROP CONSTRAINT CIAS_ACT2_FK_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT3_FK_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT3] DROP CONSTRAINT CIAS_ACT3_FK_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT4_FK_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT4] DROP CONSTRAINT CIAS_ACT4_FK_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT5_FK_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT5] DROP CONSTRAINT CIAS_ACT5_FK_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_COM_FK_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_COM] DROP CONSTRAINT CIAS_COM_FK_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_CIAS_ESP_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ESP] DROP CONSTRAINT FK_CIAS_ESP_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_PORT_FK_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_PORT] DROP CONSTRAINT CIAS_PORT_FK_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REL_CIAS_FK_CIAS_01]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[REL_CIAS] DROP CONSTRAINT REL_CIAS_FK_CIAS_01" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REL_CIAS_FK_CIAS_02]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[REL_CIAS] DROP CONSTRAINT REL_CIAS_FK_CIAS_02" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_FK_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU] DROP CONSTRAINT USU_FK_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_VOLADJ_CIAS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[VOLADJ] DROP CONSTRAINT FK_VOLADJ_CIAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_VOLADJ_CIAS1]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[VOLADJ] DROP CONSTRAINT FK_VOLADJ_CIAS1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_CIAS_COM_COM]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_COM] DROP CONSTRAINT FK_CIAS_COM_COM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_FK_IDI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] DROP CONSTRAINT CIAS_FK_IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PAI_FK_IDI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PAI] DROP CONSTRAINT PAI_FK_IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PORT_FK_IDI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PORT] DROP CONSTRAINT PORT_FK_IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_FK_IDI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU] DROP CONSTRAINT USU_FK_IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_FK_MON]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] DROP CONSTRAINT CIAS_FK_MON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_CIAS_MON_MON]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_MON] DROP CONSTRAINT FK_CIAS_MON_MON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PAI_FK_MON]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PAI] DROP CONSTRAINT PAI_FK_MON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_PARGEN_DEF_MON]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_DEF] DROP CONSTRAINT FK_PARGEN_DEF_MON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_USU_MON]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU] DROP CONSTRAINT FK_USU_MON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_VOLADJ_MON]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[VOLADJ] DROP CONSTRAINT FK_VOLADJ_MON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_FK_PAI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] DROP CONSTRAINT CIAS_FK_PAI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVI_FK_PAI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROVI] DROP CONSTRAINT PROVI_FK_PAI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_FK_PORT]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] DROP CONSTRAINT CIAS_FK_PORT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_PORT_FK_PORT]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_PORT] DROP CONSTRAINT CIAS_PORT_FK_PORT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_PORT_FK_PORT]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU_PORT] DROP CONSTRAINT USU_PORT_FK_PORT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_FK_PROVI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] DROP CONSTRAINT CIAS_FK_PROVI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_CIAS_USU]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] DROP CONSTRAINT FK_CIAS_USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_PORT_FK_USU]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU_PORT] DROP CONSTRAINT USU_PORT_FK_USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT1_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT1_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT2_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT2_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT2_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT3_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT3_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT3_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT4_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT4_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT4_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT4_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT5_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT5_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ACT5_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ACT5_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT1_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ACT1_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ACT2_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ACT3_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ACT4_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ACT4_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_COM_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_COM_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ESP_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ESP_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_ESP_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_ESP_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_PORT_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_PORT_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_PORT_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CIAS_PORT_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COM_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[COM_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MCIAS_ACT1_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[MCIAS_ACT1_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MCIAS_ACT2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[MCIAS_ACT2_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MCIAS_ACT3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[MCIAS_ACT3_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MCIAS_ACT4_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[MCIAS_ACT4_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REL_CIAS_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[REL_CIAS_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REL_COMP_TG_INS_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[REL_COMP_TG_INS_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_PORT_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_PORT_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_PORT_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_PORT_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_PORT_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_PORT_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VOLADJ_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[VOLADJ_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_ACT_REL_CIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_ACT_REL_CIAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MON_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PEND_NOTIF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PEND_NOTIF]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_PENDIENTE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_PENDIENTE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_PROCE_PUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_PROCE_PUB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACT_VISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACT_VISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ADD_VOLADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ADD_VOLADJ]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_ADJUNTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_CIA_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_CIA_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_OFERTA_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_OFERTA_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AUTORIZAR_PREMIUM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AUTORIZAR_PREMIUM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CAMBIO_NIVEL_ACT_CIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CAMBIO_NIVEL_ACT_CIAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_COMPROBAR_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_COMPROBAR_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEL_VOLADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEL_VOLADJ]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DESAUTORIZAR_PREMIUM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DESAUTORIZAR_PREMIUM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DETALLE_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DETALLE_PROVE_PORTAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ACTIVIDADES_CIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ACTIVIDADES_CIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROVE_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PUJAS_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PUJAS_ITEM]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_REGISTRO_COMPRADORAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_COMPANIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_MVTOS_PROV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_MVTOS_PROV]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_REL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_REL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_RELCIAS_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_RELCIAS_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_JOB_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_JOB_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIFICAR_CODIGO_CIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIFICAR_CODIGO_CIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_ITEM_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_ITEM_OFE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_OBJ_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_OBJ_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_PROVECOD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_PROVECOD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_REL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_REL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_PROV_REINTENTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_PROV_REINTENTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_REGISTRAR_COMPANIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_REGISTRAR_COMPANIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SELECT_ID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_SELECT_ID]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRANSFER_CIA_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRANSFER_CIA_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRANSFER_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRANSFER_PROVE_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_UPLOAD_ADJUN_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_UPLOAD_ADJUN_WEB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_LOGIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_LOGIN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Function


Private Function V_2_10_Actualizar_Tablas()
Dim sConsulta As String


sConsulta = ""
sConsulta = sConsulta & "CREATE TABLE [dbo].[ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CIA_COMP] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID_ADJUN_CIA] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATA] [image] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PARGEN_GEST_TEMP] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOTIF_AUT] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOTIF_DESAUT] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOTIF_CAMB_ACT] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOTIF_REGCIAS] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONFIRM_AUT] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONFIRM_DESAUT] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONFIRM_CAMB_ACT] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONFIRM_REGCIAS] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM_PORTAL] [varchar] (255) NULL ," & vbCrLf
sConsulta = sConsulta & "   [URL] [varchar] (255) NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADM] [varchar] (255) NULL ," & vbCrLf
sConsulta = sConsulta & "   [EMAIL] [varchar] (255) NULL ," & vbCrLf
sConsulta = sConsulta & "   [TIPOEMAIL] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOTIF_SOLICREG] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOTIF_SOLICACTIV] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOTIF_SOLICCOMP] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_REINTENTOS] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_CICLOS] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_PERIODICIDAD] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_HORA_INICIO] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_HORA_FIN] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_CADAMIN] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_PASADAS] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_FECULT] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NIVELMINACT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_UNA_HORA] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PARGEN_GEST_TEMP (ID,NOTIF_AUT,NOTIF_DESAUT,NOTIF_CAMB_ACT,NOTIF_REGCIAS,CONFIRM_AUT,CONFIRM_DESAUT,CONFIRM_CAMB_ACT,CONFIRM_REGCIAS,NOM_PORTAL,URL,ADM,EMAIL,TIPOEMAIL,NOTIF_SOLICREG,NOTIF_SOLICACTIV,NOTIF_SOLICCOMP,PROV_REINTENTOS,PROV_CICLOS,PROV_PERIODICIDAD,PROV_HORA_INICIO ,PROV_HORA_FIN ,PROV_CADAMIN ,PROV_PASADAS,PROV_FECULT,NIVELMINACT,PROV_UNA_HORA)" & vbCrLf
sConsulta = sConsulta & "                       SELECT ID,NOTIF_AUT,NOTIF_DESAUT,NOTIF_CAMB_ACT,NOTIF_REGCIAS,CONFIRM_AUT,CONFIRM_DESAUT,CONFIRM_CAMB_ACT,CONFIRM_REGCIAS,NOM_PORTAL,URL,ADM,EMAIL,TIPOEMAIL,NOTIF_SOLICREG,NOTIF_SOLICACTIV,NOTIF_SOLICCOMP,PROV_REINTENTOS,PROV_CICLOS,PROV_PERIODICIDAD,PROV_HORA_INICIO ,PROV_HORA_FIN ,PROV_CADAMIN ,PROV_PASADAS,PROV_FECULT,NIVELMINACT,PROV_UNA_HORA" & vbCrLf
sConsulta = sConsulta & "  FROM PARGEN_GEST" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE PARGEN_GEST" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_RENAME 'PARGEN_GEST_TEMP', 'PARGEN_GEST'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF ADD [USU] [int] NULL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Private Function V_2_10_Constraints()
Dim sConsulta As String


sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CIAS_PREMIUM] DEFAULT (0) FOR [PREMIUM]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CIAS_PENDCONF] DEFAULT (0) FOR [PENDCONF]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CIAS_PENDCONFREG] DEFAULT (0) FOR [PENDCONFREG]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_GEST] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_AUT] DEFAULT (0) FOR [NOTIF_AUT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_DESAUT] DEFAULT (0) FOR [NOTIF_DESAUT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_CAMB_ACT] DEFAULT (0) FOR [NOTIF_CAMB_ACT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_REGCIAS] DEFAULT (0) FOR [NOTIF_REGCIAS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONFIRM_AUT] DEFAULT (0) FOR [CONFIRM_AUT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONFIRM_DESAUT] DEFAULT (0) FOR [CONFIRM_DESAUT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONFIRM_CAMB_ACT] DEFAULT (0) FOR [CONFIRM_CAMB_ACT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONFIRM_REGCIAS] DEFAULT (0) FOR [CONFIRM_REGCIAS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_TIPOEMAIL] DEFAULT (1) FOR [TIPOEMAIL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_SOLICREG] DEFAULT (0) FOR [NOTIF_SOLICREG]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_SOLICACTIV] DEFAULT (0) FOR [NOTIF_SOLICACTIV]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NOTIF_SOLICCOMP] DEFAULT (0) FOR [NOTIF_SOLICCOMP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_REINTENTOS] DEFAULT (0) FOR [PROV_REINTENTOS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_CICLOS] DEFAULT (0) FOR [PROV_CICLOS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_PERIODICIDAD] DEFAULT (0) FOR [PROV_PERIODICIDAD]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_PASADAS] DEFAULT (0) FOR [PROV_PASADAS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROV_FECULT] DEFAULT (1 / 1 / 2000) FOR [PROV_FECULT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NIVELMINACT] DEFAULT (0) FOR [NIVELMINACT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROv_UNAHORA_1] DEFAULT ('15:15') FOR [PROV_UNA_HORA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_INTERNO] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_INTERNO_MAX_ADJUN] DEFAULT (0) FOR [MAX_ADJUN],"
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_INTERNO_PREMIUM] DEFAULT (1) FOR [PREMIUM],"
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_INTERNO_COLAB] DEFAULT (0) FOR [COLAB]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_INTERNO_PAIS] DEFAULT (198) FOR [PAI]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[REL_CIAS] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_REL_CIAS_PREM_ACTIVO] DEFAULT (0) FOR [PREM_ACTIVO]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_REL_CIAS_ORDEN_ACT] DEFAULT (0) FOR [ORDEN_ACT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_REL_CIAS_ORDEN__NUE] DEFAULT (0) FOR [ORDEN_NUE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function


Private Function V_2_10_Foreign_Keys()
Dim sConsulta As String


sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT3] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [ACT3_FK_ACT2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT2] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT4] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [SUBACT3_FK_SUBACT2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT3] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT5] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [SUBACT4_FK_SUBACT3] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]," & vbCrLf
sConsulta = sConsulta & "       [ACT4]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT4] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_FK_IDI] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [IDI]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[IDI] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_FK_MON] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MON]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[MON] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_FK_PAI] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PAI]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PAI] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_FK_PORT] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PORT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PORT] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_FK_PROVI] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PAI]," & vbCrLf
sConsulta = sConsulta & "       [PROVI]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROVI] (" & vbCrLf
sConsulta = sConsulta & "       [PAI]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_CIAS_USU] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [USUPPAL]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[USU] (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT1] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT1_FK_ACT] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT1] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT1_FK_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT2] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT2_FK_ACT2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT2] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT2_FK_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT3] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT3_FK_ACT3] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT3] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT3_FK_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT4] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT4_FK_ACT4] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]," & vbCrLf
sConsulta = sConsulta & "       [ACT4]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT4] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT4_FK_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ACT5] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT5_FK_ACT5] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]," & vbCrLf
sConsulta = sConsulta & "       [ACT4]," & vbCrLf
sConsulta = sConsulta & "       [ACT5]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT5] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]," & vbCrLf
sConsulta = sConsulta & "       [ACT4]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_ACT5_FK_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_COM] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_COM_FK_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_CIAS_COM_COM] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [TIPOCOM]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[COM] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_ESP] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_CIAS_ESP_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_MON] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_CIAS_MON_MON] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MON_PORTAL]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[MON] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_PORT] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_PORT_FK_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CIAS_PORT_FK_PORT] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PORT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PORT] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PAI] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PAI_FK_IDI] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [IDI]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[IDI] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PAI_FK_MON] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MON]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[MON] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_DEF] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PARGEN_DEF_MON] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MONCEN]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[MON] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PORT] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PORT_FK_IDI] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [IDIDEF]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[IDI] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROVI] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PROVI_FK_PAI] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PAI]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PAI] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[REL_CIAS] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [REL_CIAS_FK_CIAS_01] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA_PROVE]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [REL_CIAS_FK_CIAS_02] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA_COMP]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_USU_MON] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MON]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[MON] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [USU_FK_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [USU_FK_IDI] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [IDI]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[IDI] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU_PORT] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [USU_PORT_FK_PORT] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PORT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PORT] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [USU_PORT_FK_USU] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [USU]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[USU] (" & vbCrLf
sConsulta = sConsulta & "       [CIA]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[VOLADJ] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_VOLADJ_CIAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA_PROVE]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_VOLADJ_CIAS1] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CIA_COMP]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CIAS] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_VOLADJ_MON] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MONPOR]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[MON] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function


Private Function V_2_10_Storeds_1()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_ACT_REL_CIAS(@COMP VARCHAR(50),@PROVE VARCHAR(50),@ACTIVAS INTEGER,@NUEVAS INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_COMP AS INT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ID_COMP = (SELECT ID FROM CIAS WHERE COD=@COMP)" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET ORDEN_ACT=@ACTIVAS,ORDEN_NUE=@NUEVAS WHERE CIA_COMP=@ID_COMP AND COD_PROVE_CIA=@PROVE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE MON_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM MON WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE MON SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS_MON SET MON_PORTAL=@NEW WHERE MON_PORTAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VOLADJ SET MONPOR=@NEW WHERE MONPOR=@OLD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_PEND_NOTIF (@CIACOD VARCHAR(50),@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER," & vbCrLf
sConsulta = sConsulta & "@ITEM INTEGER,@PROVE VARCHAR(50),@PROCEDEN VARCHAR(500),@ITEMDEN VARCHAR(500),@EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "@PROVEDEN VARCHAR(500),@CONTACTO VARCHAR(500),@USU INT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMAIL2 AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONTACTO2 AS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIACOMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAPROVE INT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "exec @CIACOMP = SP_SELECT_ID @CIACOD" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @CIAPROVE=CIA_PROVE FROM REL_CIAS WHERE CIA_COMP=@CIACOMP AND COD_PROVE_CIA=@PROVE" & vbCrLf
sConsulta = sConsulta & "/*Si el email o el contacto vienen como nulos se obtienen de la tabla USU del portal*/" & vbCrLf
sConsulta = sConsulta & "IF @EMAIL IS NULL OR @CONTACTO IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @EMAIL2=EMAIL,@CONTACTO2= NOM + ' ' + APE,@USU2=ID  FROM USU WHERE CIA=@CIAPROVE AND ID=@USU" & vbCrLf
sConsulta = sConsulta & "       IF @EMAIL2 IS NULL OR @CONTACTO2 IS NULL " & vbCrLf
sConsulta = sConsulta & "            SELECT @EMAIL2=EMAIL, @CONTACTO2 = NOM + ' ' + APE,@USU2=USU.ID  FROM USU INNER JOIN CIAS ON USU.CIA=CIAS.ID AND USU.ID=CIAS.USUPPAL" & vbCrLf
sConsulta = sConsulta & "            WHERE CIA=@CIACOMP" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SELECT @EMAIL2=EMAIL, @CONTACTO2 = NOM + ' ' + APE,@USU2=USU.ID  FROM USU INNER JOIN CIAS ON USU.CIA=CIAS.ID AND USU.ID=CIAS.USUPPAL" & vbCrLf
sConsulta = sConsulta & "            WHERE CIA=@CIACOMP" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PROVENOTIF (CIACOMP,ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO,USU) VALUES" & vbCrLf
sConsulta = sConsulta & "            (@CIACOMP,@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL2,@CONTACTO2,@USU2)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @USU IS NULL" & vbCrLf
sConsulta = sConsulta & "       SELECT TOP 1 @USU=ID  FROM USU WHERE CIA=@CIAPROVE AND EMAIL=@EMAIL" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PROVENOTIF (CIACOMP,ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO,USU) VALUES" & vbCrLf
sConsulta = sConsulta & "      (@CIACOMP,@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL,@CONTACTO,@USU)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Replica los cambios registrados en MVTOS_PROV a MVTOS_PROV_EST       ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                   ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                   ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX_FECACT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR(100),@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT,@ID INT,@FECULT DATETIME,@PROV_PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(100),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(20),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),@CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(20),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(20),@USU_TFNO2 VARCHAR(20),@USU_FAX VARCHAR(20),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(20),@PORT INT,@NUM INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESPEC INT,@NOM VARCHAR(300),@COM VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_P INT,@IDI VARCHAR(20),@TIPOEMAIL SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECULT=PROV_FECULT,@PASADAS=PROV_PASADAS FROM PARGEN_GEST" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM = (SELECT COUNT(*) FROM MVTOS_PROV  WHERE FECACT > @FECULT) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUM <> 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_AUX CURSOR FOR SELECT ID,CIA,FECACT FROM MVTOS_PROV WHERE FECACT > @FECULT" & vbCrLf
sConsulta = sConsulta & "   OPEN Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO MVTOS_PROV_EST (MVTO,CIA,EST,INTENTOS,FECACT) VALUES (@ID,@CIA,0,0,@FECACT)" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_AUX INTO @ID,@CIA,@FECACT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @MAX_FECACT =   (SELECT MAX(FECACT) FROM MVTOS_PROV)  -- RECUPERAMOS EL MAYOR VALOR DE FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 0 ORDER BY MVTO,MVTOS_PROV_EST.CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "             SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "             FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXECUTE ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "           SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           EXECUTE SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA            " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "             ELSE  " & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "                            BEGIN" & vbCrLf
sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING3='EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTO @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@ID_P'    " & vbCrLf
sConsulta = sConsulta & "           SET @PARAM='@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                   ,@ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@ID_P" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "              END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                    DECLARE Cur_CONTAC CURSOR FOR   " & vbCrLf
sConsulta = sConsulta & "                             SELECT APE,NOM,DEP,CAR,TFNO,TFNO2,FAX,EMAIL,TFNO_MOVIL,USU.ID, IDI.COD,TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN IDI AS IDI ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              -- SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               OPEN Cur_CONTAC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL,@ID_P,@IDI,@TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "           @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "           @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "           @TIPOEMAIL=@TIPOEMAIL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                   @ID_P INT," & vbCrLf
sConsulta = sConsulta & "          @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "          @TIPOEMAIL SMALLINT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "                       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "                     BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                        @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                            @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                         @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                        @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                   @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                   @TIPOEMAIL=@TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               FETCH NEXT FROM Cur_CONTAC INTO @USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2,@USU_FAX,@USU_EMAIL,@USU_TFNO_MOVIL,@ID_P,@IDI,@TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "               CLOSE Cur_CONTAC" & vbCrLf
sConsulta = sConsulta & "               DEALLOCATE Cur_CONTAC" & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "             SELECT APE,NOM,DEP,CAR,TFNO,TFNO2,FAX,EMAIL,TFNO_MOVIL,USU.ID,IDI.COD,TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                       FROM USU INNER JOIN" & vbCrLf
sConsulta = sConsulta & "                          USU_PORT ON USU.CIA=USU_PORT.CIA AND USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND " & vbCrLf
sConsulta = sConsulta & "                          USU_PORT.FPEST=3 AND USU.CIA = @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN IDI AS IDI ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                       SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "               @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "               @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "               @TIPOEMAIL=@TIPOEMAIL'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                      SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "           @ID_P INT," & vbCrLf
sConsulta = sConsulta & "           @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "           @TIPOEMAIL SMALLINT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                      EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                   @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                   @TIPOEMAIL=@TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                                          " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "             ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "           BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "             IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS=@PASADAS,PROV_FECULT=@MAX_FECACT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACT_PENDIENTE  @CIA int,@IDI varchar(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4, act5 ) select act1, act2, act3, act4, act5  from cias_act5 a" & vbCrLf
sConsulta = sConsulta & "                   where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act4 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act3 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act2 d" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act1 e" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4 ) select act1, act2, act3, act4  from cias_act4 a" & vbCrLf
sConsulta = sConsulta & "                   where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act3 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act2 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act1 d" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3 ) select act1, act2, act3  from cias_act3 a" & vbCrLf
sConsulta = sConsulta & "                   where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act2 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act1 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2 ) select act1, act2 from cias_act2 a" & vbCrLf
sConsulta = sConsulta & "                   where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act1 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1) select act1 from cias_act1 where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "if (select count(*) from mcias_act5 where cia=@CIA)>0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4, act5 ) select act1, act2, act3, act4, act5  from mcias_act5 a" & vbCrLf
sConsulta = sConsulta & "                   where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act4 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act3 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act2 d" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act1 e" & vbCrLf
sConsulta = sConsulta & "                                      where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "        and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4 ) select act1, act2, act3, act4  from mcias_act4 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act3 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act2 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act1 d" & vbCrLf
sConsulta = sConsulta & "      where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                                        and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3 ) select act1, act2, act3  from mcias_act3 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act2 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act1 c" & vbCrLf
sConsulta = sConsulta & "      where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                                        and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2 ) select act1, act2 from mcias_act2 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "       from mcias_act1 b" & vbCrLf
sConsulta = sConsulta & "                                      where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1) select act1 from mcias_act1 where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz select * from #raiz" & vbCrLf
sConsulta = sConsulta & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1,act2,act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "select act1,act2,act3,act4,min(act5) from (select * from #raiz where act5 is not null" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "           select * from #mraiz where act5 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3,act4" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1,act2,act3,act4)" & vbCrLf
sConsulta = sConsulta & "select act1,act2,act3,min(act4) from (select * from #raiz where act4 is not null" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "           select * from #mraiz where act4 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1,act2,act3)" & vbCrLf
sConsulta = sConsulta & "select act1,act2,min(act3) from (select * from #raiz where act3 is not null" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "           select * from #mraiz where act3 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1,act2)" & vbCrLf
sConsulta = sConsulta & "select act1,min(act2) from (select * from #raiz where act2 is not null" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "           select * from #mraiz where act2 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1)" & vbCrLf
sConsulta = sConsulta & "select min(act1) from (select * from #raiz" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "select * from #mraiz) a" & vbCrLf
sConsulta = sConsulta & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.act4,a.id" & vbCrLf
sConsulta = sConsulta & "from act5 a inner join #base b on a.act1=b.act1 and a.act2=b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Not Null" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.id" & vbCrLf
sConsulta = sConsulta & "from act4 a inner join #base b on a.act1=b.act1 and a.act2=b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null And b.act4 Is Not Null" & vbCrLf
sConsulta = sConsulta & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.act2 = c.act2 and a.act3=c.act3 and a.id=c.act4)" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.id" & vbCrLf
sConsulta = sConsulta & "from act3 a inner join #base b on a.act1=b.act1 and a.act2=b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null And b.act3 Is Not Null" & vbCrLf
sConsulta = sConsulta & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.act2 = c.act2 and a.id=c.act3)" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.id" & vbCrLf
sConsulta = sConsulta & "from act2 a inner join #base b on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null And b.act2 Is Not Null" & vbCrLf
sConsulta = sConsulta & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.id = c.act2)" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1)" & vbCrLf
sConsulta = sConsulta & "select a.id" & vbCrLf
sConsulta = sConsulta & "from act1 a" & vbCrLf
sConsulta = sConsulta & "where not exists (SELECT * FROM #Estructura c where a.id=c.act1)" & vbCrLf
sConsulta = sConsulta & "update #Estructura set actAsign=0 ,actPend=0" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actasign = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actasign = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actasign = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actasign = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actasign = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actpend = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actpend = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actpend = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actpend = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actpend = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "if @IDI='SPA'" & vbCrLf
sConsulta = sConsulta & "select #Estructura .act1 act1, act1.cod cod1, act1.den_spa denact1 , #Estructura.act2 act2, act2.cod cod2,  act2.den_spa denact2, #Estructura.act3 act3, act3.cod cod3,  act3.den_spa denact3, #Estructura.act4 act4,  act4.cod cod4, act4.den_spa denact4,  #Estructura.act5,  act5.cod cod5, act5.den_spa denact5, actasign, actpend" & vbCrLf
sConsulta = sConsulta & "from #Estructura  left join act1 on #Estructura.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "left join act2 on #Estructura.act1=act2.act1 and  #Estructura.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "left join act3 on #Estructura.act1=act3.act1 and  #Estructura.act2 = act3.act2 and #Estructura.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "left join act4 on #Estructura.act1=act4.act1 and  #Estructura.act2 = act4.act2 and #Estructura.act3 = act4.act3 and #Estructura.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "left join act5 on #Estructura.act1=act5.act1 and  #Estructura.act2 = act5.act2 and #Estructura.act3 = act5.act3 and #Estructura.act4 = act5.act4 and #Estructura.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "Where actasign > 0 And actpend > 0" & vbCrLf
sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "select #Estructura.act1 act1, act1.cod cod1, act1.den_eng denact1 , #Estructura.act2 act2, act2.cod cod2,  act2.den_eng denact2, #Estructura.act3 act3, act3.cod cod3,  act3.den_eng denact3, #Estructura.act4 act4,  act4.cod cod4, act4.den_eng denact4,  #Estructura.act5,  act5.cod cod5, act5.den_eng denact5, actasign, actpend" & vbCrLf
sConsulta = sConsulta & "from #Estructura left join act1 on #Estructura.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "left join act2 on #Estructura.act1=act2.act1 and  #Estructura.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "left join act3 on #Estructura.act1=act3.act1 and  #Estructura.act2 = act3.act2 and #Estructura.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "left join act4 on #Estructura.act1=act4.act1 and  #Estructura.act2 = act4.act2 and #Estructura.act3 = act4.act3 and #Estructura.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "left join act5 on #Estructura.act1=act5.act1 and  #Estructura.act2 = act5.act2 and #Estructura.act3 = act5.act3 and #Estructura.act4 = act5.act4 and #Estructura.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "Where actasign > 0 And actpend > 0" & vbCrLf
sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACT_PROCE_PUB (@CIA_COMP SMALLINT,@CIA_PROVE VARCHAR(50), @NUM_PUB INT, @NUM_PUB_SINOFE INT,@NUM_PUB_AREV INT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE  AS INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @IDPROVE= (SELECT ID FROM CIAS WHERE COD=@CIA_PROVE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET PROCE_ACT =@NUM_PUB , PROCE_NUE=@NUM_PUB_SINOFE , PROCE_REV=@NUM_PUB_AREV  WHERE CIA_COMP=@CIA_COMP AND CIA_PROVE=@IDPROVE" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACT_VISIBLE @CIA int,@IDI varchar(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "create table #mraiz (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4, act5 ) select act1, act2, act3, act4, act5  from cias_act5 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act4 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act3 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act2 d" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act1 e" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3, act4 ) select act1, act2, act3, act4  from cias_act4 a" & vbCrLf
sConsulta = sConsulta & "                   where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act3 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act2 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act1 d" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2, act3 ) select act1, act2, act3  from cias_act3 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act2 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act1 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1, act2 ) select act1, act2 from cias_act2 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from cias_act1 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #raiz (act1) select act1 from cias_act1 where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "if (select count(*) from mcias_act5 where cia=@CIA)>0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4, act5 ) select act1, act2, act3, act4, act5  from mcias_act5 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act4 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act3 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act2 d" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act1 e" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3, act4 ) select act1, act2, act3, act4  from mcias_act4 a" & vbCrLf
sConsulta = sConsulta & "                   where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act3 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act2 c" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act1 d" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                                        and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2, act3 ) select act1, act2, act3  from mcias_act3 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act2 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act1 c" & vbCrLf
sConsulta = sConsulta & "      where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                                        and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1, act2 ) select act1, act2 from mcias_act2 a" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and not exists (select *" & vbCrLf
sConsulta = sConsulta & "from mcias_act1 b" & vbCrLf
sConsulta = sConsulta & "where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz (act1) select act1 from mcias_act1 where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "insert into #mraiz select * from #raiz" & vbCrLf
sConsulta = sConsulta & "create table #base (act1 int, act2 int, act3 int, act4 int, act5 int)" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1,act2,act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "select act1,act2,act3,act4,min(act5) from (select * from #raiz where act5 is not null" & vbCrLf
sConsulta = sConsulta & "            Union" & vbCrLf
sConsulta = sConsulta & "select * from #mraiz where act5 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3,act4" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1,act2,act3,act4)" & vbCrLf
sConsulta = sConsulta & "select act1,act2,act3,min(act4) from (select * from #raiz where act4 is not null" & vbCrLf
sConsulta = sConsulta & "            Union" & vbCrLf
sConsulta = sConsulta & "select * from #mraiz where act4 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2,act3" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1,act2,act3)" & vbCrLf
sConsulta = sConsulta & "select act1,act2,min(act3) from (select * from #raiz where act3 is not null" & vbCrLf
sConsulta = sConsulta & "            Union" & vbCrLf
sConsulta = sConsulta & "select * from #mraiz where act3 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1,act2" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1,act2)" & vbCrLf
sConsulta = sConsulta & "select act1,min(act2) from (select * from #raiz where act2 is not null" & vbCrLf
sConsulta = sConsulta & "            Union" & vbCrLf
sConsulta = sConsulta & "select * from #mraiz where act2 is not null) a" & vbCrLf
sConsulta = sConsulta & "group by act1" & vbCrLf
sConsulta = sConsulta & "insert into #base (act1)" & vbCrLf
sConsulta = sConsulta & "select min(act1) from (select * from #raiz" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "select * from #mraiz) a" & vbCrLf
sConsulta = sConsulta & "create table #Estructura (Act1 int, act2 int, act3 int, act4 int , act5 int, ActAsign smallint, ActPend smallint)" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4,act5)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.act4,a.id" & vbCrLf
sConsulta = sConsulta & "from act5 a inner join #base b on a.act1=b.act1 and a.act2=b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Not Null" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3,act4)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.act3,a.id" & vbCrLf
sConsulta = sConsulta & "from act4 a inner join #base b on a.act1=b.act1 and a.act2=b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null And b.act4 Is Not Null" & vbCrLf
sConsulta = sConsulta & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.act2 = c.act2 and a.act3=c.act3 and a.id=c.act4)" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2,Act3)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.act2,a.id" & vbCrLf
sConsulta = sConsulta & "from act3 a inner join #base b on a.act1=b.act1 and a.act2=b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null And b.act3 Is Not Null" & vbCrLf
sConsulta = sConsulta & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.act2 = c.act2 and a.id=c.act3)" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1,Act2)" & vbCrLf
sConsulta = sConsulta & "select a.act1,a.id" & vbCrLf
sConsulta = sConsulta & "from act2 a inner join #base b on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null And b.act2 Is Not Null" & vbCrLf
sConsulta = sConsulta & "and not exists (SELECT * FROM #Estructura c where a.act1=c.act1 and a.id = c.act2)" & vbCrLf
sConsulta = sConsulta & "insert into #Estructura (Act1)" & vbCrLf
sConsulta = sConsulta & "select a.id" & vbCrLf
sConsulta = sConsulta & "from act1 a" & vbCrLf
sConsulta = sConsulta & "where not exists (SELECT * FROM #Estructura c where a.id=c.act1)" & vbCrLf
sConsulta = sConsulta & "update #Estructura set actAsign=0 ,actPend=0" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actAsign = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #raiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 1" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1" & vbCrLf
sConsulta = sConsulta & "Where b.act2 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 2" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "Where b.act3 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 3" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "Where b.act4 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 4" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4" & vbCrLf
sConsulta = sConsulta & "Where b.act5 Is Null" & vbCrLf
sConsulta = sConsulta & "update #Estructura" & vbCrLf
sConsulta = sConsulta & "Set actPend = 5" & vbCrLf
sConsulta = sConsulta & "from #Estructura a inner join #mraiz b on a.act1=b.act1 and a.act2 = b.act2 and a.act3 = b.act3 and a.act4 = b.act4 and a.act5 = b.act5" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "if @IDI='SPA'" & vbCrLf
sConsulta = sConsulta & "select #Estructura .act1 act1, act1.cod cod1, act1.den_spa denact1 , #Estructura.act2 act2, act2.cod cod2,  act2.den_spa denact2, #Estructura.act3 act3, act3.cod cod3,  act3.den_spa denact3, #Estructura.act4 act4,  act4.cod cod4, act4.den_spa denact4,  #Estructura.act5,  act5.cod cod5, act5.den_spa denact5, actasign, actpend" & vbCrLf
sConsulta = sConsulta & "from #Estructura  left join act1 on #Estructura.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "left join act2 on #Estructura.act1=act2.act1 and  #Estructura.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "left join act3 on #Estructura.act1=act3.act1 and  #Estructura.act2 = act3.act2 and #Estructura.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "left join act4 on #Estructura.act1=act4.act1 and  #Estructura.act2 = act4.act2 and #Estructura.act3 = act4.act3 and #Estructura.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "left join act5 on #Estructura.act1=act5.act1 and  #Estructura.act2 = act5.act2 and #Estructura.act3 = act5.act3 and #Estructura.act4 = act5.act4 and #Estructura.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "select #Estructura.act1 act1, act1.cod cod1, act1.den_eng denact1 , #Estructura.act2 act2, act2.cod cod2,  act2.den_eng denact2, #Estructura.act3 act3, act3.cod cod3,  act3.den_eng denact3, #Estructura.act4 act4,  act4.cod cod4, act4.den_eng denact4,  #Estructura.act5,  act5.cod cod5, act5.den_eng denact5, actasign, actpend" & vbCrLf
sConsulta = sConsulta & "from #Estructura left join act1 on #Estructura.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "left join act2 on #Estructura.act1=act2.act1 and  #Estructura.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "left join act3 on #Estructura.act1=act3.act1 and  #Estructura.act2 = act3.act2 and #Estructura.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "left join act4 on #Estructura.act1=act4.act1 and  #Estructura.act2 = act4.act2 and #Estructura.act3 = act4.act3 and #Estructura.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "left join act5 on #Estructura.act1=act5.act1 and  #Estructura.act2 = act5.act2 and #Estructura.act3 = act5.act3 and #Estructura.act4 = act5.act4 and #Estructura.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "order by #Estructura.act1, #Estructura.act2,#Estructura.act3,#Estructura.act4,#Estructura.act5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ADD_VOLADJ(@COMP INT,@PROVE VARCHAR(50),@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT,@VOL FLOAT,@MONGS VARCHAR(50),@MONPOR VARCHAR(50),@FECADJ DATETIME,@DENPROCE VARCHAR(100) ) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Inserta el registro correspondiente en VOLADJ" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONPORTAL AS VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VOLPORTAL AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVGS AS FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Obtiene el ID de la compaNYia proveedora" & vbCrLf
sConsulta = sConsulta & "SET @ID = (SELECT ID FROM CIAS WHERE COD=@PROVE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Comprueba si existe la moneda en el portal" & vbCrLf
sConsulta = sConsulta & "SET @MONPORTAL =(SELECT COD FROM MON WHERE COD=@MONPOR)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si existe el registro primero lo borra" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND CIA_PROVE=@ID AND CIA_COMP=@COMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @MONPORTAL IS NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & " --Si existe la moneda en el portal se calcula el total y lo inserta," & vbCrLf
sConsulta = sConsulta & " --Calcula la equivalencia de la moneda en el portal" & vbCrLf
sConsulta = sConsulta & " SET @EQUIV = (SELECT EQUIV FROM MON WHERE COD=@MONPOR)" & vbCrLf
sConsulta = sConsulta & " IF NOT @EQUIV IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "     --Calcula el total del volumen en la moneda del portal" & vbCrLf
sConsulta = sConsulta & "     SET @VOLPORTAL=(@VOL) / @EQUIV  " & vbCrLf
sConsulta = sConsulta & "              INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,@MONPORTAL,@VOLPORTAL,@FECADJ,@DENPROCE)" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,@MONPORTAL,NULL,@FECADJ,@DENPROCE)    " & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " --si no existe la moneda en el portal lo inserta a null" & vbCrLf
sConsulta = sConsulta & " INSERT INTO VOLADJ (CIA_PROVE, CIA_COMP, ANYO, GMN1, PROCE, VOLGS, MONGS, MONPOR, VOLPOR, FECADJ,DENPROCE) VALUES (@ID, @COMP, @ANYO,@GMN1,@PROCE,@VOL,@MONGS,NULL,NULL,@FECADJ,@DENPROCE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ANYA_ADJUNTO @CIA_COMP int, @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @PROVE varchar(50), @OFE smallint,@OFEWEB smallint, @NOM varchar(300), @COM varchar(500) , @ID int = null OUTPUT   AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "--SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_ANYA_ADJUNTO @ANYO= ' + CONVERT(VARCHAR,@ANYO) + ', @GMN1 = ''' + @GMN1 + ''', @PROCE = ' + convert(varchar,@PROCE)  + ', @PROVE=''' +  @PROVE + ''',@OFE=' +  CONVERT(VARCHAR,@OFE) +  ',@OFEWEB=' +  CONVERT(VARCHAR,@OFEWEB) +  ',@ID=' +  CONVERT(VARCHAR,@ID) +  ', @NOM=''' +  @NOM + ''', @COM=''' +  @COM + ''',@WEB=1' " & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_ANYA_ADJUNTO @ANYO= @ANYO, @GMN1 =@GMN1, @PROCE =@PROCE, @PROVE=@PROVE,@OFE=@OFE,@OFEWEB=@OFEWEB, @NOM= @NOM, @COM=@COM,@WEB=1,@ID=@ID OUTPUT' " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @OFE SMALLINT, @OFEWEB SMALLINT, @NOM VARCHAR(300), @COM VARCHAR(500), @ID INT OUTPUT' " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "/* Execute the string with the first parameter value. */ " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, " & vbCrLf
sConsulta = sConsulta & "                      @ANYO = @ANYO,  " & vbCrLf
sConsulta = sConsulta & "         @GMN1 =@GMN1,  " & vbCrLf
sConsulta = sConsulta & "                     @PROCE =@PROCE,  " & vbCrLf
sConsulta = sConsulta & "                     @PROVE=@PROVE, " & vbCrLf
sConsulta = sConsulta & "        @OFE=@OFE, " & vbCrLf
sConsulta = sConsulta & "@OFEWEB=@OFEWEB, @NOM= @NOM, @COM=@COM,@ID=@ID OUTPUT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ANYA_CIA_ESP @IDCIA int, @NOM varchar(300), @COM varchar(500) , @IDESP int output AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDESP = 1" & vbCrLf
sConsulta = sConsulta & "select @IDESP = ISNULL(ID,0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM CIAS_ESP " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA, ID, NOM, COM) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA, @IDESP, @NOM, @COM)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ANYA_OFERTA_WEB @CIA_COMP int,  @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @CODMON VARCHAR(50),@FECVAL VARCHAR(10),@OBS text ,@NUMOFE INT OUTPUT , @NUMOFEWEB  INT OUTPUT  " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "DECLARE @TODOS int" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUEVOS int" & vbCrLf
sConsulta = sConsulta & "DECLARE @AREVISAR int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_ANYA_OFERTA_WEB @ANYO=@ANYO, @GMN1 =@GMN1, @PROCE =@PROCE, @PROVE=@PROVE,@CODMON=@CODMON, @FECVAL=@FECVAL,@OBS='''',@NUMOFE=@NUMOFE OUTPUT ,@NUMOFEWEB=@NUMOFEWEB OUTPUT' " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "/* Build the SQL string once.*/ " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @CODMON VARCHAR(50),@FECVAL VARCHAR(10),@NUMOFE INT OUTPUT, @NUMOFEWEB  INT OUTPUT' " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "/* Execute the string with the first parameter value. */ " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, " & vbCrLf
sConsulta = sConsulta & "                      @ANYO = @ANYO,  " & vbCrLf
sConsulta = sConsulta & "         @GMN1 =@GMN1,  " & vbCrLf
sConsulta = sConsulta & "                     @PROCE =@PROCE,  " & vbCrLf
sConsulta = sConsulta & "                     @PROVE=@PROVE, " & vbCrLf
sConsulta = sConsulta & "                     @CODMON=@CODMON, " & vbCrLf
sConsulta = sConsulta & "                     @FECVAL=@FECVAL, " & vbCrLf
sConsulta = sConsulta & "                     @NUMOFE=@NUMOFE OUTPUT, " & vbCrLf
sConsulta = sConsulta & "         @NUMOFEWEB=@NUMOFEWEB OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_AUTORIZAR_PREMIUM @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = @LNKSERVER + 'SP_ACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET PREMIUM =2  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PREM_ACTIVO =1 WHERE CIA_PROVE = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL @SDEN VARCHAR(8),@SBUSCAR VARCHAR(50),@SCOMP INT AS" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA1 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA2 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA3 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA4 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA5 AS VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA1= 'ACT1.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA2= 'ACT2.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA3= 'ACT3.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA4= 'ACT4.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA5= 'ACT5.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "set @SQL = '" & vbCrLf
sConsulta = sConsulta & "SELECT ACT1.ID ACT1, NULL ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA1 + ' DEN ,COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, NULL CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1 FROM CIAS_ACT1 UNION SELECT CIA, ACT1 FROM CIAS_ACT2 UNION SELECT CIA, ACT1 FROM CIAS_ACT3 UNION SELECT CIA, ACT1 FROM CIAS_ACT4 UNION SELECT CIA, ACT1 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT1=ACT1.ID " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA1+ ' LIKE ''%' + @SBUSCAR + '%'' " & vbCrLf
sConsulta = sConsulta & "GROUP BY   ACT1.ID, ACT1.COD, ' + @IDIOMA1 + '" & vbCrLf
sConsulta = sConsulta & "union" & vbCrLf
sConsulta = sConsulta & "SELECT ACT2.ACT1, ACT2.ID ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA2 + ' DEN, COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, ACT2.COD CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2 FROM CIAS_ACT2 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=PP.CIA) J ON J.ACT2=ACT2.ID  AND J.ACT1=ACT2.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON ACT2.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA2+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY  ACT2.ACT1, ACT2.ID, ACT1.COD,ACT2.COD,' + @IDIOMA2 + ' " & vbCrLf
sConsulta = sConsulta & "union" & vbCrLf
sConsulta = sConsulta & "SELECT ACT3.ACT1,ACT3.ACT2, ACT3.ID ACT3, NULL ACT4, NULL ACT5, ' + @IDIOMA3 + ' DEN, COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT3=ACT3.ID AND J.ACT2=ACT3.ACT2 AND J.ACT1=ACT3.ACT1 " & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON ACT3.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON ACT3.ACT1 = ACT2.ACT1 AND ACT3.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA3+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY ACT3.ACT1,ACT3.ACT2,ACT3.ID, ACT1.COD, ACT2.COD, ACT3.COD, ' + @IDIOMA3 + '" & vbCrLf
sConsulta = sConsulta & "union" & vbCrLf
sConsulta = sConsulta & "SELECT ACT4.ACT1,ACT4.ACT2, ACT4.ACT3, ACT4.ID ACT4, NULL ACT5, ' + @IDIOMA4 + ' DEN, COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, ACT4.COD CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT4" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA)  J ON J.ACT4=ACT4.ID AND J.ACT3=ACT4.ACT3 AND J.ACT2=ACT4.ACT2 AND J.ACT1=ACT4.ACT1  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON ACT4.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON ACT4.ACT1 = ACT2.ACT1 AND ACT4.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 ON ACT4.ACT1 = ACT3.ACT1 AND ACT4.ACT2 = ACT3.ACT2 AND ACT4.ACT3 = ACT3.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA4+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID, ACT1.COD, ACT2.COD, ACT3.COD, ACT4.COD, ' + @IDIOMA4 + '" & vbCrLf
sConsulta = sConsulta & "union" & vbCrLf
sConsulta = sConsulta & "SELECT ACT5.ACT1,ACT5.ACT2, ACT5.ACT3, ACT5.ACT4, ACT5.ID ACT5, ' + @IDIOMA5 + ' DEN, COUNT(J.CIA) AS PROVE, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, ACT4.COD CODACT4, ACT5.COD CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM ACT5" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CIAS_ACT5 J ON J.ACT5=ACT5.ID AND J.ACT4=ACT5.ACT4 AND J.ACT3=ACT5.ACT3 AND J.ACT2=ACT5.ACT2  AND J.ACT1=ACT5.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=J.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=J.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON ACT5.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON ACT5.ACT1 = ACT2.ACT1 AND ACT5.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 ON ACT5.ACT1 = ACT3.ACT1 AND ACT5.ACT2 = ACT3.ACT2 AND ACT5.ACT3 = ACT3.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT4 ON ACT5.ACT1 = ACT4.ACT1 AND ACT5.ACT2 = ACT4.ACT2 AND ACT5.ACT3 = ACT4.ACT3 AND ACT5.ACT3 = ACT4.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA5+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY ACT5.ACT1,ACT5.ACT2,ACT5.ACT3,ACT5.ACT4,ACT5.ID, ACT1.COD, ACT2.COD, ACT3.COD, ACT4.COD, ACT5.COD, ' + @IDIOMA5 + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY 1,2,3,4,5'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CAMBIO_NIVEL_ACT_CIAS (@NIVEL INTEGER, @RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL_SUP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL_INF AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERROR AS INTEGER" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL = 2 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT1" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_INF =  COUNT( DISTINCT (ACT1))  FROM ACT2" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT2" & vbCrLf
sConsulta = sConsulta & "   SELECT  ACT2.ACT1,ACT2.ID FROM ACT2" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT3 ON ACT3.ACT1 = ACT2.ACT1 AND ACT3.ACT2 = ACT2.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT2.ACT1,ACT2.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL=4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT3" & vbCrLf
sConsulta = sConsulta & "   SELECT ACT3.ACT1,ACT3.ACT2,ACT3.ID FROM ACT3" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT4 ON ACT4.ACT1 = ACT3.ACT1 AND ACT4.ACT2 = ACT3.ACT2 AND ACT4.ACT3=ACT3.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT3.ACT2,ACT3.ACT1,ACT3.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL=5" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NIVEL_SUP = COUNT (*) FROM ACT4" & vbCrLf
sConsulta = sConsulta & "   SELECT ACT4.ACT1,ACT4.ACT2,ACT4.ACT3,ACT4.ID FROM ACT4" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ACT5 ON ACT5.ACT1 = ACT4.ACT1 AND ACT5.ACT2 = ACT4.ACT2 AND ACT5.ACT3=ACT4.ACT3 AND ACT5.ACT4=ACT4.ID " & vbCrLf
sConsulta = sConsulta & "   GROUP BY ACT4.ACT3,ACT4.ACT2,ACT4.ACT1,ACT4.ID" & vbCrLf
sConsulta = sConsulta & "   SET @NIVEL_INF = @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL_INF < @NIVEL_SUP " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ERROR = 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "IF @ERROR = 0 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 2 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                DELETE FROM CIAS_ACT3 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT3" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 5" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT2 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                DELETE FROM CIAS_ACT3 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CIAS_ACT4 " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM MCIAS_ACT4 " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET @RES=@ERROR" & vbCrLf
sConsulta = sConsulta & "RETURN " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CARGAR_CONFIG_PROCE @CIACOMP int, @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @DEFDEST tinyint OUTPUT, @DEFPAG tinyint  OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM tinyint OUTPUT , @DEFPROCEESP tinyint OUTPUT, @DEFGRUPOESP tinyint OUTPUT, @DEFITEMESP tinyint OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN tinyint OUTPUT, @DEFGRUPOADJUN tinyint OUTPUT, @DEFITEMADJUN tinyint OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA tinyint OUTPUT, @DEFSUVERPROVE tinyint OUTPUT, @DEFSUVERPUJAS tinyint OUTPUT, @DEFSUMINUTOS tinyint OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER tinyint OUTPUT, @DEFCANTMAX tinyint OUTPUT, @HAYATRIBUTOS AS tinyint OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIACOMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_CARGAR_CONFIG_PROCE @ANYO =@ANYO, @GMN1 =@GMN1, @PROCE =@PROCE, @DEFDEST=@DEFDEST OUTPUT, @DEFPAG =@DEFPAG OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM = @DEFFECSUM OUTPUT, @DEFPROCEESP =@DEFPROCEESP OUTPUT, @DEFGRUPOESP =@DEFGRUPOESP OUTPUT, @DEFITEMESP =@DEFITEMESP OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN =@DEFOFEADJUN OUTPUT, @DEFGRUPOADJUN=@DEFGRUPOADJUN OUTPUT, @DEFITEMADJUN=@DEFITEMADJUN OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA =@DEFSUBASTA OUTPUT, @DEFSUVERPROVE=@DEFSUVERPROVE OUTPUT, @DEFSUVERPUJAS =@DEFSUVERPUJAS OUTPUT,@DEFSUMINUTOS = @DEFSUMINUTOS OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER =@DEFPRECALTER OUTPUT, @DEFCANTMAX =@DEFCANTMAX OUTPUT, @HAYATRIBUTOS =@HAYATRIBUTOS OUTPUT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @DEFDEST tinyint OUTPUT, @DEFPAG tinyint  OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM tinyint OUTPUT , @DEFPROCEESP tinyint OUTPUT, @DEFGRUPOESP tinyint OUTPUT, @DEFITEMESP tinyint OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN tinyint OUTPUT, @DEFGRUPOADJUN tinyint OUTPUT, @DEFITEMADJUN tinyint OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA tinyint OUTPUT, @DEFSUVERPROVE tinyint OUTPUT, @DEFSUVERPUJAS tinyint OUTPUT, @DEFSUMINUTOS tinyint OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER tinyint OUTPUT, @DEFCANTMAX tinyint OUTPUT, @HAYATRIBUTOS AS tinyint OUTPUT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, @ANYO =@ANYO, @GMN1 =@GMN1, @PROCE =@PROCE, @DEFDEST=@DEFDEST OUTPUT, @DEFPAG =@DEFPAG OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM = @DEFFECSUM OUTPUT, @DEFPROCEESP =@DEFPROCEESP OUTPUT, @DEFGRUPOESP =@DEFGRUPOESP OUTPUT, @DEFITEMESP =@DEFITEMESP OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN =@DEFOFEADJUN OUTPUT, @DEFGRUPOADJUN=@DEFGRUPOADJUN OUTPUT, @DEFITEMADJUN=@DEFITEMADJUN OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA =@DEFSUBASTA OUTPUT, @DEFSUVERPROVE=@DEFSUVERPROVE OUTPUT, @DEFSUVERPUJAS =@DEFSUVERPUJAS OUTPUT, @DEFSUMINUTOS = @DEFSUMINUTOS OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER =@DEFPRECALTER OUTPUT, @DEFCANTMAX =@DEFCANTMAX OUTPUT, @HAYATRIBUTOS =@HAYATRIBUTOS OUTPUT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_COMPROBAR_EMAIL(@FSP_CIA VARCHAR(20),@EMAIL VARCHAR(100),@EXISTE INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE   @CIA INTEGER" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CIA = (SELECT ID FROM CIAS WHERE COD=@FSP_CIA)" & vbCrLf
sConsulta = sConsulta & "SET @EXISTE = (SELECT COUNT(*)  FROM USU INNER JOIN USU_PORT ON USU.CIA = USU_PORT.CIA AND USU.ID = USU_PORT.USU  WHERE USU.CIA=@CIA AND USU.EMAIL=@EMAIL)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEL_VOLADJ(@COMP INT,@PROVE VARCHAR(50), @ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ID = (SELECT ID FROM CIAS WHERE COD=@PROVE)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@COMP AND CIA_PROVE=@ID AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DESAUTORIZAR_PREMIUM @ID INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = @LNKSERVER + 'SP_DESACTIVAR_PREMIUM @COD=''' + @COD_PROVE_CIA + ''',@PREMIUM = 1'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET PREMIUM = 3  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PREM_ACTIVO = 0 WHERE CIA_PROVE = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_ACTIVIDADES_CIA @CIA int,@IDI varchar(20),@TODO bit = 0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--la tabla temporal #raiz contendr� las actividades asignadas a la compa��a" & vbCrLf
sConsulta = sConsulta & "create table #raiz (act1 int, act2 int, act3 int, act4 int, act5 int , ActAsign smallint)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @TODO = 0" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    --Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, act5, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, act5, 5" & vbCrLf
sConsulta = sConsulta & "      from cias_act5 a" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act4 b" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = b.act3" & vbCrLf
sConsulta = sConsulta & "                          and a.act4 = b.act4 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act3 c" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = c.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = c.act3 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 d" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = d.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = d.act2)" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 e" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = e.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, 4  " & vbCrLf
sConsulta = sConsulta & "      from cias_act4 a" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act3 b" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2" & vbCrLf
sConsulta = sConsulta & "                          and a.act3 = b.act3 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 c" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = c.act2 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 d" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = d.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, 3" & vbCrLf
sConsulta = sConsulta & "      from cias_act3 a" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act2 b" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1" & vbCrLf
sConsulta = sConsulta & "                          and a.act2 = b.act2 )" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 c" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = c.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2 , 2" & vbCrLf
sConsulta = sConsulta & "      from cias_act2 a" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "       and not exists (select *" & vbCrLf
sConsulta = sConsulta & "                         from cias_act1 b" & vbCrLf
sConsulta = sConsulta & "                        where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "                          and a.act1 = b.act1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, 1 " & vbCrLf
sConsulta = sConsulta & "      from cias_act1 " & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    --Insertamos los datos correspondientes a #raiz" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, act5, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, act5, 5" & vbCrLf
sConsulta = sConsulta & "      from cias_act5 a" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, act4, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, act4, 4  " & vbCrLf
sConsulta = sConsulta & "      from cias_act4 a" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, act3, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2, act3, 3" & vbCrLf
sConsulta = sConsulta & "      from cias_act3 a" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, act2, actAsign ) " & vbCrLf
sConsulta = sConsulta & "    select act1, act2 , 2" & vbCrLf
sConsulta = sConsulta & "      from cias_act2 a" & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    insert into #raiz (act1, actAsign) " & vbCrLf
sConsulta = sConsulta & "    select act1, 1 " & vbCrLf
sConsulta = sConsulta & "      from cias_act1 " & vbCrLf
sConsulta = sConsulta & "     where cia=@CIA" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "select #raiz.act1 act1, act1.cod cod1, act1.den_' + @IDI + ' denact1, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act2 act2, act2.cod cod2, act2.den_' + @IDI + ' denact2, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act3 act3, act3.cod cod3, act3.den_' + @IDI + ' denact3, " & vbCrLf
sConsulta = sConsulta & "       #raiz.act4 act4, act4.cod cod4, act4.den_' + @IDI + ' denact4,  " & vbCrLf
sConsulta = sConsulta & "       #raiz.act5 act5, act5.cod cod5, act5.den_' + @IDI + ' denact5, " & vbCrLf
sConsulta = sConsulta & "       actasign" & vbCrLf
sConsulta = sConsulta & "from #raiz  " & vbCrLf
sConsulta = sConsulta & "     left join act1 " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1 = act1.id" & vbCrLf
sConsulta = sConsulta & "     left join act2 " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act2.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act2.id" & vbCrLf
sConsulta = sConsulta & "     left join act3 " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act3.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act3.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act3.id" & vbCrLf
sConsulta = sConsulta & "     left join act4 " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act4.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act4.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act4.act3 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act4 = act4.id" & vbCrLf
sConsulta = sConsulta & "     left join act5 " & vbCrLf
sConsulta = sConsulta & "            on #raiz.act1=act5.act1 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act2 = act5.act2 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act3 = act5.act3 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act4 = act5.act4 " & vbCrLf
sConsulta = sConsulta & "           and #raiz.act5= act5.id" & vbCrLf
sConsulta = sConsulta & "order by #raiz.act1, #raiz.act2,#raiz.act3,#raiz.act4,#raiz.act5" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "exec sp_executesql @stmt=@SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_ADJUN_OFE @IDCIA int, @IDADJUN int  AS" & vbCrLf
sConsulta = sConsulta & "/*Devuelve el tama�o del adjunto a traspasar*/" & vbCrLf
sConsulta = sConsulta & "SELECT id, id_adjun_cia , datalength(data) TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from adjun " & vbCrLf
sConsulta = sConsulta & " WHERE CIA_COMP = @IDCIA and ID=@IDADJUN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_GRUPO @CIA_COMP INTEGER,@ANYO INT,@GMN1 VARCHAR(20),@PROCE INT ,@GRUPO VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_GRUPO_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@GRUPO=@GRUPO'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO INT,@GMN1 VARCHAR(20),@PROCE INT,@GRUPO VARCHAR(20)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @GRUPO= @GRUPO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Private Sub V_2_10_Storeds_2()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE @ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50), @OFE smallint = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFEENV smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFE smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEENVIADAS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAULTOFE datetime" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEENVIADAS  = COUNT(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @LASTOFEENV = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @FECHAULTOFE = FECREC" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFEENV" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFE IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF EXISTS (SELECT * " & vbCrLf
sConsulta = sConsulta & "               FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "              WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                AND OFE = -1)" & vbCrLf
sConsulta = sConsulta & "    SET @LASTOFE = -1" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT @LASTOFE = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "       AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "       AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "       AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @LASTOFE =@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS =(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "                        AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                        AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.OFE, PO.EST, PO.FECREC," & vbCrLf
sConsulta = sConsulta & "       PO.FECVAL, PO.MON, MON.DEN MONDEN, PO.CAMBIO, PO.OBS , @LASTOFEENV LASTOFEENV, " & vbCrLf
sConsulta = sConsulta & "       @FECHAULTOFE FECHAULTOFE, @NUMOFEENVIADAS NUMOFEENVIADAS, ENVIADA ESTULTOFE,PO.FECACT, @TOTALADJUNTOS TOTALADJUNTOS" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "            ON PO.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROCESO (@CIA_COMP INTEGER,@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ACTIVA SMALLINT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCESO_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@ACTIVA=@ACTIVA OUTPUT'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ACTIVA SMALLINT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @ACTIVA = @ACTIVA OUTPUT" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_PROVE (@CIA_COMP INTEGER,@PROVE VARCHAR(50),@NORMAL SMALLINT OUTPUT,@SUBASTA SMALLINT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PROCESOS_PROVE_WEB @PROVE=@PROVE,@NORMAL=@NORMAL OUTPUT,@SUBASTA=@SUBASTA OUTPUT'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@PROVE VARCHAR(50), @NORMAL SMALLINT OUTPUT, @SUBASTA SMALLINT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, @PROVE = @PROVE, @NORMAL = @NORMAL OUTPUT , @SUBASTA = @SUBASTA OUTPUT" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROVE_ESP @IDPROVE int output, @IDESP int = NULL, @CODPROVE varchar(50) = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @IDPROVE is null" & vbCrLf
sConsulta = sConsulta & "   select @IDPROVE = ID FROM CIAS WHERE COD = @CODPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDESP = NULL" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact , datalength(data) TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT id, nom,com, fecact, datalength(data)  TAMANYO" & vbCrLf
sConsulta = sConsulta & "  from cias_esp  " & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @IDPROVE" & vbCrLf
sConsulta = sConsulta & "   AND ID = @IDESP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PUJAS_ITEM (@CIA_COMP INTEGER,@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_DEVOLVER_PUJAS_ITEM_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@ITEM=@ITEM'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_REGISTRO_COMPRADORAS @PROVE int  AS" & vbCrLf
sConsulta = sConsulta & "SELECT A.ID IDCIA, A.COD COD, A.DEN DEN," & vbCrLf
sConsulta = sConsulta & "CASE WHEN B.CIA_COMP IS NULL THEN 0 ELSE 1 END REGISTRADA," & vbCrLf
sConsulta = sConsulta & "CASE WHEN C.CIA_COMP IS NULL THEN 0 ELSE 1 END SOLICITADA" & vbCrLf
sConsulta = sConsulta & "FROM CIAS A LEFT JOIN REL_COMP B" & vbCrLf
sConsulta = sConsulta & "ON A.ID = B.CIA_COMP" & vbCrLf
sConsulta = sConsulta & "AND @PROVE = B.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN MREL_COMP C" & vbCrLf
sConsulta = sConsulta & "ON A.ID = C.CIA_COMP" & vbCrLf
sConsulta = sConsulta & "AND @PROVE = C.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "Where A.FCEST = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DOWNLOAD_ADJUNTO @INIT INT, @OFFSET INT, @ID INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM ADJUN WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_ADJUN_OFE(@IDADJUN INT) AS" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Elimina un archivo adjunto de oferta                                                                     ******/" & vbCrLf
sConsulta = sConsulta & "/******        se usa despues de traspasar el adjunto a GS                                                                   ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ADJUN WHERE  ID=@IDADJUN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_COMPANIA @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & " SELECT @SQLSTRING = @LNKSERVER + 'SP_ELIM_ENLACE_2 @COD=''' + @COD_PROVE_CIA + ''''" & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQLSTRING " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MREL_COMP WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REL_CIAS WHERE CIA_COMP=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VOLADJ WHERE CIA_COMP=@ID OR CIA_PROVE=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_MON WHERE CIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT1 WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_COM WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_PORT WHERE CIA=@ID " & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_PORT WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "--- 2.9.5" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ESP WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "------------" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET USUPPAL = NULL WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU WHERE CIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_MVTOS_PROV AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/********************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Borra los registros de MVTOS_PROV  y MVTOS_PROV_EST que han acabado  correctamente        ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                                                   ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :   --------------------------                                                                     ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                     ******/" & vbCrLf
sConsulta = sConsulta & "/********************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Borramos los registros correspondientes de MVTOS_PROV" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE MVTOS_PROV FROM MVTOS_PROV INNER JOIN MVTOS_PROV_EST ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO AND MVTOS_PROV_EST.EST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Borramos tambien los registos correspondientes de MVTOS_PROV_EST" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MVTOS_PROV_EST WHERE EST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE  SP_ELIM_REL  (@CIA_COMP INT,@CIA_PROVE INT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM REL_CIAS WHERE CIA_COMP=@CIA_COMP AND CIA_PROVE=@CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIM_RELCIAS_GS (@CIA_COMP INT, @CIA_PROVE VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDPROVE  AS INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @IDPROVE= (SELECT ID FROM CIAS WHERE COD=@CIA_PROVE)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM REL_CIAS WHERE CIA_COMP=@CIA_COMP AND CIA_PROVE=@IDPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "return" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_JOB_ACTUALIZAR_PROV_GS AS" & vbCrLf
sConsulta = sConsulta & "BEGIN DISTRIBUTED TRAN" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "EXEC SP_PROV_REINTENTOS" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_ACTUALIZAR_PROV_GS" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR = 0 " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_ELIMINAR_MVTOS_PROV" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR = 0" & vbCrLf
sConsulta = sConsulta & "  COMMIT TRAN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_MODIFICAR_CODIGO_CIA @CIA INTEGER, @COD VARCHAR(20) " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INTEGER,@COD_PROVE_CIA VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMPRADOR BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE curRELCIAS CURSOR FOR SELECT CIA_COMP,COD_PROVE_CIA FROM  REL_CIAS WHERE CIA_PROVE= @CIA" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SELECT @COMPRADOR = CASE WHEN FCEST>1 THEN 1 ELSE 0 END,  @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA" & vbCrLf
sConsulta = sConsulta & "IF @COMPRADOR = 1 AND @LNKSERVER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING =  @LNKSERVER + 'SP_MODIFICAR_CODIGO_PORTAL @COD=''' + @COD +  ''''" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = @LNKSERVER + 'SP_MODIFICAR_FSP_COD @PROVE=''' + @COD_PROVE_CIA + ''',@COD = ''' + @COD_PROVE_CIA + ''''" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curRELCIAS INTO @CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURRELCIAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS SET COD=@COD  WHERE ID=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_MODIF_ITEM_OFE @CIA_COMP int,  @PRECIO  AS FLOAT,@CANTMAX AS FLOAT,@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@ITEM INT,@PROVE VARCHAR(50) ,@OFE INT  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_MODIF_ITEM_OFE  @PRECIO=@PRECIO, @CANTMAX=@CANTMAX, @ANYO=@ANYO , @GMN1 = @GMN1 , @PROCE = @PROCE, @ITEM = @ITEM, @PROVE= @PROVE ,@OFE=@OFE,@WEB=1'" & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@PRECIO FLOAT, @CANTMAX FLOAT, @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT, @ITEM INT,@PROVE VARCHAR(50),@OFE INT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING , @ParmDefinition, " & vbCrLf
sConsulta = sConsulta & "@PRECIO=@PRECIO,  " & vbCrLf
sConsulta = sConsulta & "@CANTMAX=@CANTMAX," & vbCrLf
sConsulta = sConsulta & "@ANYO = @ANYO, " & vbCrLf
sConsulta = sConsulta & "@GMN1 =@GMN1,  " & vbCrLf
sConsulta = sConsulta & "@PROCE =@PROCE, " & vbCrLf
sConsulta = sConsulta & "@ITEM=@ITEM , " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE," & vbCrLf
sConsulta = sConsulta & "@OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_MODIF_OBJ_PROVE (@CIA_COMP INTEGER,@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INTEGER ," & vbCrLf
sConsulta = sConsulta & "@PROVE VARCHAR(50),@NUMOBJ INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM INTEGER, @NUMNUE INTEGER,@NUMAREV INTEGER" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "/*Obtengo el linked server */" & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "/*Ejecuto el store procedure de GS*/" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_MODIF_OBJ_PROVE_WEB @ANYO=@ANYO, @GMN1=@GMN1,@PROCE=@PROCE,@PROVE=@PROVE,@NUMOBJ=@NUMOBJ,@NUM=@NUM OUTPUT,@NUMNUE=@NUMNUE OUTPUT,@NUMAREV=@NUMAREV OUTPUT'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50),@NUMOBJ INTEGER,@NUM INTEGER OUTPUT,@NUMNUE INTEGER OUTPUT,@NUMAREV INTEGER OUTPUT'  " & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition,@ANYO=@ANYO,@GMN1=@GMN1, @PROCE = @PROCE, @PROVE = @PROVE,@NUMOBJ=@NUMOBJ,@NUM=@NUM OUTPUT,@NUMNUE=@NUMNUE OUTPUT,@NUMAREV=@NUMAREV OUTPUT" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "/*Actualiza REL_CIAS con los valores obtenidos del SP_MODIF_OBJ_PROVE_WEB*/" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET PROCE_ACT=@NUM,PROCE_NUE=@NUMNUE,PROCE_REV=@NUMAREV WHERE CIA_COMP=@CIA_COMP AND COD_PROVE_CIA=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_MODIF_PROVECOD  @OLDCOD VARCHAR(20), @NEWCOD VARCHAR(20) , @CIACOMP INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE REL_CIAS SET COD_PROVE_CIA=@NEWCOD  WHERE COD_PROVE_CIA=@OLDCOD AND CIA_COMP=@CIACOMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE  SP_MODIF_REL  (@EST INT,@CIA_COMP INT,@CIA_PROVE INT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE REL_CIAS SET EST=@EST WHERE CIA_COMP=@CIA_COMP AND CIA_PROVE=@CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_NUEVO_ADJUNTO @CIA_COMP int, @DATA IMAGE, @MAXID int OUTPUT, @DATASIZE int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ADJUN (CIA_COMP, DATA) VALUES (@CIA_COMP, @DATA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @MAXID = MAX(ID) FROM ADJUN" & vbCrLf
sConsulta = sConsulta & "SELECT @DATASIZE=DATALENGTH(DATA) FROM ADJUN WHERE ID = @MAXID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_PROV_REINTENTOS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Intenta pasar los movimientos fallidos si toca       ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                         ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :  ------------------                                                           ******/" & vbCrLf
sConsulta = sConsulta & "/******                                        ******/" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @REINTENTOS SMALLINT,@CIA INT,@USU INT,@MAIL VARCHAR,@CICLOS SMALLINT, @PASADAS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA TINYINT, @TIPO VARCHAR,@EST INT,@INTENTO AS INT,@FECACT DATETIME,@EST_FILA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT,@CIA_PROVE INT,@COD_PROVE_CIA VARCHAR(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FCEST INT,@FSGS_SRV VARCHAR(100),@FSGS_BD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAICOD INT,@PROVICOD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFGS VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1000),@SQLSTRING2 NVARCHAR(1000),@SQLSTRING3 NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAS_DEN VARCHAR(100), @CIAS_NIF VARCHAR(100),@CIAS_DIR VARCHAR(255),@CIAS_PAI VARCHAR(100),@CIAS_PROVI VARCHAR(100),@CIAS_CP VARCHAR(100),@CIAS_POB VARCHAR(100),@CIAS_MON VARCHAR(50),@CIAS_URLCIA VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_APE VARCHAR(100),@USU_NOM VARCHAR(100),@USU_DEP VARCHAR(100),@USU_CAR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO VARCHAR(100),@USU_TFNO2 VARCHAR(100),@USU_FAX VARCHAR(100),@USU_EMAIL VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU_TFNO_MOVIL VARCHAR(100),@PORT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM VARCHAR(300),@COM VARCHAR(500),@ESPEC INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CICLOS= (SELECT PROV_CICLOS FROM PARGEN_GEST) " & vbCrLf
sConsulta = sConsulta & "SET @REINTENTOS = (SELECT PROV_REINTENTOS FROM PARGEN_GEST)" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = (SELECT PROV_PASADAS FROM PARGEN_GEST)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_P INT,@IDI VARCHAR(50),@TIPOEMAIL SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CICLOS <> 0 AND @PASADAS%@CICLOS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE Cur_MVTOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT MVTOS_PROV.CIA, MVTOS_PROV.USU, MVTOS_PROV.TIPO, MVTOS_PROV.TABLA, " & vbCrLf
sConsulta = sConsulta & "        MVTOS_PROV_EST.EST, MVTOS_PROV_EST.INTENTOS,MVTOS_PROV_EST.FECACT,MVTOS_PROV.ESPEC" & vbCrLf
sConsulta = sConsulta & "    FROM MVTOS_PROV_EST INNER JOIN CIAS ON CIAS.ID=MVTOS_PROV_EST.CIA " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN MVTOS_PROV ON MVTOS_PROV.ID = MVTOS_PROV_EST.MVTO" & vbCrLf
sConsulta = sConsulta & "   WHERE MVTOS_PROV_EST.EST = 1 AND MVTOS_PROV_EST.INTENTOS < @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "   OPEN  Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        DECLARE curRELCIAS CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "                       SELECT CIA_PROVE,CIA_COMP,COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "                       FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@CIA" & vbCrLf
sConsulta = sConsulta & "       SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SFGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' ,@FCEST= FCEST FROM CIAS WHERE ID=@CIA_COMP AND PORT =0 --BASPARAMETROS.IDPORTAL " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "              IF @TABLA = 0  -- ES UNA ACTUALIZACION DE LOS DATOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                IF @FCEST = 3" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                     -- OBTENEMOS LOS DATOS DE LA COMPA�IA     " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "             SELECT   @CIAS_DEN=CIAS.DEN, @CIAS_NIF=CIAS.NIF,  @CIAS_CP=CIAS.CP,@CIAS_PAI=PAI.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_POB=CIAS.POB,@CIAS_PROVI=PROVI.COD,@CIAS_MON=MON.COD," & vbCrLf
sConsulta = sConsulta & "                    @CIAS_DIR=CIAS.DIR, @CIAS_URLCIA=CIAS.URLCIA" & vbCrLf
sConsulta = sConsulta & "             FROM CIAS LEFT JOIN PAI ON CIAS.PAI=PAI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PROVI ON CIAS.PAI=PROVI.PAI AND CIAS.PROVI=PROVI.ID" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN MON ON MON.ID=CIAS.MON" & vbCrLf
sConsulta = sConsulta & "                         WHERE CIAS.ID=  @CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_PROV_GS @COD_PROVE_CIA= @COD_PROVE_CIA, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DEN =@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_DIR=@CIAS_DIR, " & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                                   @CIAS_URLCIA=@CIAS_URLCIA'" & vbCrLf
sConsulta = sConsulta & "                    " & vbCrLf
sConsulta = sConsulta & "                                                   " & vbCrLf
sConsulta = sConsulta & "           SET @PARAM = N' @COD_PROVE_CIA VARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA VARCHAR(255)'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                         @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DEN=@CIAS_DEN," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_NIF=@CIAS_NIF," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PAI=@CIAS_PAI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_PROVI=@CIAS_PROVI," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_CP=@CIAS_CP," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_POB=@CIAS_POB," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_DIR=@CIAS_DIR," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_MON=@CIAS_MON," & vbCrLf
sConsulta = sConsulta & "                         @CIAS_URLCIA=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "          END                                          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "             ELSE  " & vbCrLf
sConsulta = sConsulta & "        IF @TABLA = 1   -- ES UN MOVIMIENTO DE LOS CONTACTOS DE LA COMPA�IA" & vbCrLf
sConsulta = sConsulta & "                            BEGIN" & vbCrLf
sConsulta = sConsulta & "           -- Eliminamos todos los contactos del GS que sean del portal" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO='D' -- ELIMINAMOS LOS CONTACTOS" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSTRING3 = 'EXECUTE ' + @SFGS + 'SP_ELIMINAR_CONTACTOS @COD_PROVE_CIA=@COD_PROVE_CIA, @ID_P=@USU' " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           SET @PARAM= '@COD_PROVE_CIA VARCHAR(200),@ID_P INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           EXECUTE sp_executesql @SQLSTRING3,@PARAM,@COD_PROVE_CIA=@COD_PROVE_CIA,@ID_P=@USU" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Insertamos todos los contactos del PS que tengan FPEST = 3 en USU_PORT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @TIPO='I' " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                IF (SELECT FPEST FROM USU_PORT WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3 " & vbCrLf
sConsulta = sConsulta & "                                BEGIN" & vbCrLf
sConsulta = sConsulta & "           SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "            FROM USU " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN IDI AS IDI ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "                                         WHERE USU.CIA = @CIA_PROVE AND USU.ID= @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                               @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                               @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                               @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                               @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                               @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                               @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                               @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                               @TIPOEMAIL=@TIPOEMAIL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_DEP VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                           @ID_P INT," & vbCrLf
sConsulta = sConsulta & "                           @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                           @TIPOEMAIL SMALLINT'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                   @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                   @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                   @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                   @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                   @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                   @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                   @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                   @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                   @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                                        @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                                           @TIPOEMAIL=@TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                               END     " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @TIPO='U' " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                IF (SELECT FPEST FROM USU_PORT WHERE USU_PORT.CIA=@CIA_PROVE AND USU_PORT.USU=@USU  AND USU_PORT.PORT=0)=3 " & vbCrLf
sConsulta = sConsulta & "                                BEGIN" & vbCrLf
sConsulta = sConsulta & "                SELECT @USU_APE=APE,@USU_NOM=NOM,@USU_DEP=DEP,@USU_CAR=CAR,@USU_TFNO=TFNO,@USU_TFNO2=TFNO2," & vbCrLf
sConsulta = sConsulta & "                                         @USU_FAX=FAX,@USU_EMAIL=EMAIL,@USU_TFNO_MOVIL=TFNO_MOVIL,@ID_P=USU.ID, @IDI=IDI.COD,@TIPOEMAIL=TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "            FROM USU INNER JOIN IDI AS IDI ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "                                         WHERE  USU.CIA = @CIA_PROVE AND USU.ID=@USU" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                            SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_CONTACTO @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                  @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                  @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                  @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                  @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                  @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                  @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "               @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "               @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "               @TIPOEMAIL=@TIPOEMAIL'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                        SET @PARAM=N'@USU_APE VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_NOM VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_CAR VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_FAX VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_EMAIL VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO2 VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @USU_TFNO_MOVIL VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "           @ID_P INT," & vbCrLf
sConsulta = sConsulta & "           @IDI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "           @TIPOEMAIL SMALLINT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                           EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_APE=@USU_APE," & vbCrLf
sConsulta = sConsulta & "                                      @USU_NOM=@USU_NOM," & vbCrLf
sConsulta = sConsulta & "                                      @USU_DEP=@USU_DEP," & vbCrLf
sConsulta = sConsulta & "                                      @USU_CAR=@USU_CAR," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO=@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                                      @USU_FAX=@USU_FAX," & vbCrLf
sConsulta = sConsulta & "                                      @USU_EMAIL=@USU_EMAIL," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO2=@USU_TFNO2," & vbCrLf
sConsulta = sConsulta & "                                      @USU_TFNO_MOVIL=@USU_TFNO_MOVIL," & vbCrLf
sConsulta = sConsulta & "                                      @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                   @ID_P=@ID_P," & vbCrLf
sConsulta = sConsulta & "                   @IDI=@IDI," & vbCrLf
sConsulta = sConsulta & "                   @TIPOEMAIL=@TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "                           END                                        " & vbCrLf
sConsulta = sConsulta & "            END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "             ELSE  -- ACTUALIZAMOS LOS ARCHIVOS ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "           BEGIN   " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'I'  -- ES UNA INSERCION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_INSERTAR_ADJUNTO @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                                            @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                                            @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                                            @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'D'  -- ES UN DELETE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                                      " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ELIMINAR_ADJUNTO  @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                                            @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @TIPO = 'U' -- ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @NOM=NOM,@COM=COM,@FECACT=FECACT FROM CIAS_ESP" & vbCrLf
sConsulta = sConsulta & "                                        WHERE CIA = @CIA_PROVE AND ID=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @SQLSTRING = 'EXEC ' + @SFGS + 'SP_ACTUALIZAR_ADJUNTO @NOM= @NOM," & vbCrLf
sConsulta = sConsulta & "                                           @COM =@COM," & vbCrLf
sConsulta = sConsulta & "                                           @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                           @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                           @ESPEC=@ESPEC'" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM= N'@NOM VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "                               @COM VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "                               @FECACT DATETIME," & vbCrLf
sConsulta = sConsulta & "                               @COD_PROVE_CIA VARCHAR(200)," & vbCrLf
sConsulta = sConsulta & "                               @ESPEC INT'" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "                                @NOM=@NOM," & vbCrLf
sConsulta = sConsulta & "                                @COM=@COM," & vbCrLf
sConsulta = sConsulta & "                                @FECACT=@FECACT," & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA=@COD_PROVE_CIA," & vbCrLf
sConsulta = sConsulta & "                                @ESPEC=@ESPEC" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "            " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "            IF @@ERROR <> 0"
sConsulta = sConsulta & "            BEGIN " & vbCrLf
sConsulta = sConsulta & "             SET @INTENTO = @INTENTO +1" & vbCrLf
sConsulta = sConsulta & "             IF @INTENTO = @REINTENTOS" & vbCrLf
sConsulta = sConsulta & "               SET @EST_FILA = 2" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @EST_FILA = 1" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "             SET @EST_FILA = 3"
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            UPDATE MVTOS_PROV_EST SET INTENTOS = @INTENTO,EST=@EST_FILA WHERE CURRENT OF Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "                      FETCH NEXT FROM curRELCIAS INTO @CIA_PROVE,@CIA_COMP,@COD_PROVE_CIA " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END  -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_RELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curRELCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_MVTOS INTO @CIA,@USU,@TIPO,@TABLA,@EST,@INTENTO,@FECACT,@ESPEC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END -- CERRAMOS EL WHILE CON EL QUE RECORREMOS EL CURSOR Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "   CLOSE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_MVTOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PASADAS = @PASADAS + 1" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARGEN_GEST SET PROV_PASADAS = @PASADAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_REGISTRAR_COMPANIA  @IDPORTAL int, @COD varchar(50) , @DEN varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @DIR varchar(255), @CP varchar(20), @POB varchar(100), @PAI INT, " & vbCrLf
sConsulta = sConsulta & "                 @PROVI INT, @MON INT, @FCEST tinyint, @FPEST tinyint, @NIF varchar(20)," & vbCrLf
sConsulta = sConsulta & "                 @VOLFACT float, @CLIREF1 varchar(50),@CLIREF2 varchar(50),@CLIREF3 varchar(50),@CLIREF4 varchar(50)," & vbCrLf
sConsulta = sConsulta & "                 @HOM1 varchar(50), @HOM2 varchar(50), @HOM3 varchar(50), @URLCIA varchar(255)," & vbCrLf
sConsulta = sConsulta & "                 @COMENT text, @ORIGINS varchar(50), @PREMIUM int, @TIPOACC tinyint ,@IDI varchar(50), @IDCIA int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIDI int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDCIA=ISNULL(MAX(ID),0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM CIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
sConsulta = sConsulta & "  FROM IDI" & vbCrLf
sConsulta = sConsulta & " WHERE COD = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS (ID,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,IDI,PORT,FCEST,NIF,PREMIUM,VOLFACT, " & vbCrLf
sConsulta = sConsulta & "                  CLIREF1,CLIREF2,CLIREF3,CLIREF4,HOM1,HOM2,HOM3,URLCIA,COMENT,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA,@COD,@DEN,@DIR,@CP,@POB,@PAI,@PROVI,@MON,@IDIDI,@IDPORTAL,@FCEST,@NIF,@PREMIUM,@VOLFACT ," & vbCrLf
sConsulta = sConsulta & "                  @CLIREF1, @CLIREF2, @CLIREF3, @CLIREF4, @HOM1, @HOM2, @HOM3, @URLCIA, @COMENT, @ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_PORT (CIA,PORT,FPEST,TIPOACC,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@IDCIA,@IDPORTAL,@FPEST,@TIPOACC, @ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO rel_comp (cia_prove, cia_comp) " & vbCrLf
sConsulta = sConsulta & "SELECT @IDCIA, ID" & vbCrLf
sConsulta = sConsulta & "  FROM CIAS" & vbCrLf
sConsulta = sConsulta & " WHERE FCEST=3" & vbCrLf
sConsulta = sConsulta & "   AND PORT = 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_REGISTRAR_USUARIO @CIA int, @IDPORTAL int, @COD varchar(50) , @PWD varchar(128), @FECHA datetime, " & vbCrLf
sConsulta = sConsulta & "                 @APE varchar(100), @NOM varchar(20), @DEP varchar(100), @CAR varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @TFNO varchar(20), @TFNO2 varchar(20), @FAX varchar(20), @EMAIL varchar(100), " & vbCrLf
sConsulta = sConsulta & "                 @IDI varchar(20), @TFNO_MOVIL varchar(20), @TIPOEMAIL smallint, @FCEST tinyint, @FPEST tinyint, @ORIGINS varchar(50),  " & vbCrLf
sConsulta = sConsulta & "                 @PRINCIPAL bit AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDUSU int" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIDI int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDUSU=ISNULL(MAX(ID),0) + 1 " & vbCrLf
sConsulta = sConsulta & "  FROM USU" & vbCrLf
sConsulta = sConsulta & " WHERE CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIDI = ID " & vbCrLf
sConsulta = sConsulta & "  FROM IDI" & vbCrLf
sConsulta = sConsulta & " WHERE COD = @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU (CIA,ID,COD,PWD,FECPWD,APE,NOM,DEP,CAR,TFNO,TFNO2," & vbCrLf
sConsulta = sConsulta & "                 FAX,EMAIL,IDI,FCEST,ORIGINS,TFNO_MOVIL,TIPOEMAIL) " & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA, @IDUSU, @COD, @PWD, @FECHA, @APE, @NOM, @DEP, @CAR, @TFNO, @TFNO2," & vbCrLf
sConsulta = sConsulta & "        @FAX,@EMAIL,@IDIDI,@FCEST,@ORIGINS,@TFNO_MOVIL,@TIPOEMAIL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU_PORT (CIA,USU,PORT,FPEST,ORIGINS) " & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA,@IDUSU,@IDPORTAL,@FPEST,@ORIGINS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRINCIPAL = 1 " & vbCrLf
sConsulta = sConsulta & "  UPDATE CIAS SET USUPPAL=@IDUSU WHERE ID=@CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_SELECT_ID(@FSP_CIA VARCHAR(20)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE   @ID SMALLINT " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ID = (SELECT ID FROM CIAS WHERE COD=@FSP_CIA)" & vbCrLf
sConsulta = sConsulta & "Return @ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRANSFER_ADJUN_OFE @CIA INT, @IDADJUN INT,@INIT INT,@SIZE INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM ADJUN WHERE CIA_COMP=@CIA AND ID=@IDADJUN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  ADJUN.DATA @textpointer @INIT   @LSIZE HOLDLOCK" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRANSFER_CIA_ESP @IDCIA INTEGER, @ESP SMALLINT, @INIT INT, @SIZE INT, @DATA IMAGE AS" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM CIAS_ESP WHERE CIA = @IDCIA AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
sConsulta = sConsulta & "    UPDATE CIAS_ESP SET DATA = @DATA WHERE CIA = @IDCIA AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    UPDATETEXT CIAS_ESP.DATA @textpointer @init 0 @data" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRANSFER_PROVE_ESP @CIA INT, @ESP INT,@INIT INT,@SIZE INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16) " & vbCrLf
sConsulta = sConsulta & "DECLARE @Tsize int" & vbCrLf
sConsulta = sConsulta & "DECLARE @LSIZE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @Tsize=datalength(data), @textpointer = TEXTPTR(data)  FROM CIAS_ESP WHERE CIA=@CIA AND ID=@ESP " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INIT + @SIZE >= @TSIZE" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @TSIZE-@INIT" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @LSIZE = @SIZE +1" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "READTEXT  CIAS_ESP.DATA @textpointer @INIT  @LSIZE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_UPLOAD_ADJUN_WEB @CIA_COMP INT, @INIT INT,@ID INT,@DATA IMAGE  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "DECLARE @LNKSERVER VARCHAR(200) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PARMDEFINITION NVARCHAR(500) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @LNKSERVER=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA_COMP " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING = 'EXECUTE ' + @LNKSERVER + 'SP_UPLOAD_ADJUN_WEB @INIT=@INIT,@ID=@ID,@DATA=@DATA ' " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "/* Build the SQL string once.*/ " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET @PARMDEFINITION = N'@INIT INT, @ID INT, @DATA IMAGE' " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "/* Execute the string with the first parameter value. */ " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLString, @ParmDefinition, @INIT = @INIT, @ID = @ID, @DATA = @DATA " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE USU_LOGIN(@USUOLD VARCHAR(50),@USUNEW VARCHAR(50),@PWDNEW VARCHAR(512),@FECPWD varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE ADM SET  USU=@USUNEW,PWD=@PWDNEW,FECPWD=convert(datetime,@FECPWD,103) WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR=0" & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "       print @@error" & vbCrLf
sConsulta = sConsulta & "       COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_10_Triggers()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT1_TG_INSUPD ON dbo.ACT1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT1 " & vbCrLf
sConsulta = sConsulta & "   SET FECACT=GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ACT1 A" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "            ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT2_TG_INS ON dbo.ACT2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT2 (CIA, ACT1, ACT2)" & vbCrLf
sConsulta = sConsulta & "SELECT C.CIA,I.ACT1,I.ID" & vbCrLf
sConsulta = sConsulta & "  FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN CIAS_ACT1 C" & vbCrLf
sConsulta = sConsulta & "                 ON I.ACT1 = C.ACT1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT2_TG_INSUPD ON dbo.ACT2 " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT2" & vbCrLf
sConsulta = sConsulta & "   SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ACT2 A " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "            ON A.ACT1=I.ACT1" & vbCrLf
sConsulta = sConsulta & "           AND A.ID=I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT3_TG_INS ON dbo.ACT3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT3 (CIA, ACT1, ACT2,ACT3)" & vbCrLf
sConsulta = sConsulta & "SELECT C.CIA,I.ACT1,I.ACT2,I.ID" & vbCrLf
sConsulta = sConsulta & "  FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN CIAS_ACT2 C" & vbCrLf
sConsulta = sConsulta & "                 ON I.ACT1 = C.ACT1" & vbCrLf
sConsulta = sConsulta & "                AND I.ACT2 = C.ACT2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT3_TG_INSUPD ON dbo.ACT3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT3" & vbCrLf
sConsulta = sConsulta & "   SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ACT3 A " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "            ON A.ACT1=I.ACT1" & vbCrLf
sConsulta = sConsulta & "           AND A.ACT2=I.ACT2" & vbCrLf
sConsulta = sConsulta & "           AND A.ID=I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT4_TG_INS ON dbo.ACT4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT4 (CIA, ACT1, ACT2, ACT3, ACT4)" & vbCrLf
sConsulta = sConsulta & "SELECT C.CIA,I.ACT1,I.ACT2,I.ACT3, I.ID" & vbCrLf
sConsulta = sConsulta & "  FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN CIAS_ACT3 C" & vbCrLf
sConsulta = sConsulta & "                 ON I.ACT1 = C.ACT1" & vbCrLf
sConsulta = sConsulta & "                AND I.ACT2 = C.ACT2" & vbCrLf
sConsulta = sConsulta & "                AND I.ACT3 = C.ACT3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT4_TG_INSUPD ON dbo.ACT4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT4" & vbCrLf
sConsulta = sConsulta & "   SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ACT4 A " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "            ON A.ACT1=I.ACT1" & vbCrLf
sConsulta = sConsulta & "           AND A.ACT2=I.ACT2" & vbCrLf
sConsulta = sConsulta & "           AND A.ACT3=I.ACT3" & vbCrLf
sConsulta = sConsulta & "           AND A.ID=I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT5_TG_INS ON dbo.ACT5" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT5 (CIA, ACT1, ACT2, ACT3, ACT4, ACT5)" & vbCrLf
sConsulta = sConsulta & "SELECT C.CIA,I.ACT1,I.ACT2,I.ACT3, I.ACT4, I.ID" & vbCrLf
sConsulta = sConsulta & "  FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN CIAS_ACT4 C" & vbCrLf
sConsulta = sConsulta & "                 ON I.ACT1 = C.ACT1" & vbCrLf
sConsulta = sConsulta & "                AND I.ACT2 = C.ACT2" & vbCrLf
sConsulta = sConsulta & "                AND I.ACT3 = C.ACT3" & vbCrLf
sConsulta = sConsulta & "                AND I.ACT4 = C.ACT4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ACT5_TG_INSUPD ON dbo.ACT5" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE ACT5" & vbCrLf
sConsulta = sConsulta & "   SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ACT5 A " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "            ON A.ACT1=I.ACT1" & vbCrLf
sConsulta = sConsulta & "           AND A.ACT2=I.ACT2" & vbCrLf
sConsulta = sConsulta & "           AND A.ACT3=I.ACT3" & vbCrLf
sConsulta = sConsulta & "           AND A.ACT4=I.ACT4" & vbCrLf
sConsulta = sConsulta & "           AND A.ID=I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ADJUN_TG_INS_UPD ON [dbo].[ADJUN] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "IF NOT UPDATE(FECACT)" & vbCrLf
sConsulta = sConsulta & "UPDATE ADJUN " & vbCrLf
sConsulta = sConsulta & "   SET FECACT =GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM ADJUN A" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "            ON A.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [CIAS_TG_INS] ON dbo.CIAS " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_Ins CURSOR FOR SELECT ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS SET FECINS=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [CIAS_TG_UPD] ON dbo.CIAS " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_Upd CURSOR LOCAL FOR SELECT ID,FCEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  UPDATE CIAS SET FECLASTUPD=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID)" & vbCrLf
sConsulta = sConsulta & "  IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "    UPDATE CIAS SET FECACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTOLD=3 AND @EST<>3 " & vbCrLf
sConsulta = sConsulta & "      UPDATE CIAS SET FECDESACTFC=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  -- Inserta un moviemto tipo Udate 'U' en la tabla MVTOS_PROV" & vbCrLf
sConsulta = sConsulta & "  IF UPDATE(DEN) OR UPDATE(DIR) OR UPDATE(CP) OR UPDATE(POB) OR UPDATE(PROVI) OR UPDATE(NIF) OR" & vbCrLf
sConsulta = sConsulta & "    UPDATE(PAI) OR UPDATE(MON) OR UPDATE(URLCIA)" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                                INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                        ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
sConsulta = sConsulta & "                                       AND CIAS.FCEST=3 " & vbCrLf
sConsulta = sConsulta & "                       WHERE CIA_PROVE=@ID " & vbCrLf
sConsulta = sConsulta & "                         AND EST=3)" & vbCrLf
sConsulta = sConsulta & "      IF @ENLAZADA >0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV) " & vbCrLf
sConsulta = sConsulta & "          IF @INDICE IS NULL       " & vbCrLf
sConsulta = sConsulta & "            SET @INDICE=1" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @INDICE=@INDICE+1" & vbCrLf
sConsulta = sConsulta & "          INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@ID,NULL,'U',0,GETDATE())" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_CIAS_Upd INTO @ID,@EST" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_ACT1_TG_INS ON dbo.CIAS_ACT1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Cia_Act1_Ins CURSOR FOR SELECT CIA,ACT1 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Cia_Act1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Cia_Act1_Ins INTO @CIA,@ACT1" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT2 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT2 SELECT @CIA,ACT1,ID FROM ACT2 WHERE ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Cia_Act1_Ins INTO @CIA,@ACT1" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Cia_Act1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Cia_Act1_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_ACT2_TG_INS ON dbo.CIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Cia_Act2_Ins CURSOR FOR SELECT CIA,ACT1,ACT2 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Cia_Act2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Cia_Act2_Ins INTO @CIA,@ACT1,@ACT2" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT3 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT3 SELECT @CIA,ACT1,ACT2,ID FROM ACT3 WHERE ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Cia_Act2_Ins INTO @CIA,@ACT1,@ACT2" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Close curTG_Cia_Act2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Cia_Act2_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_ACT3_TG_INS ON dbo.CIAS_ACT3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER,@ACT3 INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Cia_Act3_Ins CURSOR FOR SELECT CIA,ACT1,ACT2,ACT3 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Cia_Act3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Cia_Act3_Ins INTO @CIA,@ACT1,@ACT2,@ACT3" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3= @ACT3" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT4 SELECT @CIA,ACT1,ACT2,ACT3,ID FROM ACT4 WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Cia_Act3_Ins INTO @CIA,@ACT1,@ACT2,@ACT3" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Close curTG_Cia_Act3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Cia_Act3_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_ACT4_TG_INS ON dbo.CIAS_ACT4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER,@ACT3 INTEGER,@ACT4 INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Cia_Act4_Ins CURSOR FOR SELECT CIA,ACT1,ACT2,ACT3,ACT4 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Cia_Act4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Cia_Act4_Ins INTO @CIA,@ACT1,@ACT2,@ACT3,@ACT4" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "DELETE FROM CIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3= @ACT3 AND ACT4=@ACT4" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ACT5 SELECT @CIA,ACT1,ACT2,ACT3,ACT4,ID FROM ACT5 WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3 AND ACT4=@ACT4" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Cia_Act4_Ins INTO @CIA,@ACT1,@ACT2,@ACT3,@ACT4" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Close curTG_Cia_Act4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Cia_Act4_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_COM_TG_INSUPD ON dbo.CIAS_COM" & vbCrLf
sConsulta = sConsulta & "FOR INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_COM_Ins CURSOR FOR SELECT CIA,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_COM_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_COM_Ins INTO @CIA,@ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS_COM SET FECACT=GETDATE() WHERE CIA = @CIA AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_COM_Ins INTO @CIA,@ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_COM_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_COM_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_ESP_TG_INSUPD ON dbo.CIAS_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT, @CIA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_ESP_Ins CURSOR FOR SELECT ID,CIA FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_ESP_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_ESP_Ins INTO @ID,@CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE CIAS_ESP SET FECACT=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_ESP_Ins INTO @ID,@CIA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_CIAS_ESP_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_ESP_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_ESP_TG_UPD ON dbo.CIAS_ESP " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Inserta un movimiento tipo Update 'U' en la tabla MVTOS_PROV" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare c1 cursor local for select cia, id from inserted" & vbCrLf
sConsulta = sConsulta & "open c1" & vbCrLf
sConsulta = sConsulta & "fetch next from c1 into @cia, @id" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                   ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
sConsulta = sConsulta & "                                  AND CIAS.FCEST=3 " & vbCrLf
sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
sConsulta = sConsulta & "    IF @ENLAZADA > 0 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE= @INDICE + 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) " & vbCrLf
sConsulta = sConsulta & "                        VALUES  (@INDICE,@CIA,NULL,'U',@ID,2,GETDATE())" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "    fetch next from c1 into @cia, @id" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "close c1" & vbCrLf
sConsulta = sConsulta & "deallocate c1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_ESP_TG_INS ON dbo.CIAS_ESP " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Inserta un movimiento tipo Insert 'I' en la tabla MVTOS_PROV" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare c1 cursor local for select cia, id from inserted" & vbCrLf
sConsulta = sConsulta & "open c1" & vbCrLf
sConsulta = sConsulta & "fetch next from c1 into @cia, @id" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                   ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
sConsulta = sConsulta & "                                  AND CIAS.FCEST=3 " & vbCrLf
sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
sConsulta = sConsulta & "    IF @ENLAZADA > 0 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE= @INDICE + 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) " & vbCrLf
sConsulta = sConsulta & "                        VALUES  (@INDICE,@CIA,NULL,'I',@ID,2,GETDATE())" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "    fetch next from c1 into @cia, @id" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "close c1" & vbCrLf
sConsulta = sConsulta & "deallocate c1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER CIAS_ESP_TG_DEL ON dbo.CIAS_ESP " & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Inserta un movimiento tipo Delete 'D' en la tabla MVTOS_PROV" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@ID INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare c1 cursor local for select cia, id from deleted" & vbCrLf
sConsulta = sConsulta & "open c1" & vbCrLf
sConsulta = sConsulta & "fetch next from c1 into @cia, @id" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                   ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
sConsulta = sConsulta & "                                  AND CIAS.FCEST=3 " & vbCrLf
sConsulta = sConsulta & "                     WHERE CIA_PROVE=@CIA AND EST=3)" & vbCrLf
sConsulta = sConsulta & "    IF @ENLAZADA > 0 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE= @INDICE + 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO MVTOS_PROV (ID,CIA ,USU,TIPO,ESPEC,TABLA,FECACT) " & vbCrLf
sConsulta = sConsulta & "                        VALUES  (@INDICE,@CIA,NULL,'D',@ID,2,GETDATE())" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "    fetch next from c1 into @cia, @id" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "close c1" & vbCrLf
sConsulta = sConsulta & "deallocate c1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [CIAS_PORT_TG_INS] ON [CIAS_PORT] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@PORT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_PORT_Ins CURSOR FOR SELECT CIA,PORT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_PORT_Ins INTO @CIA,@PORT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS_PORT SET FECINS=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_PORT_Ins INTO @CIA,@PORT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [CIAS_PORT_TG_UPD] ON [CIAS_PORT] " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@PORT INT,@ESTOLD INT, @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CIAS_PORT_Upd CURSOR FOR SELECT CIA,PORT,FPEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CIAS_PORT_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CIAS_PORT_Upd INTO @CIA,@PORT,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE CIAS_PORT SET FECLASTUPD=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       SET @ESTOLD=(SELECT FPEST FROM DELETED WHERE CIA=@CIA AND PORT=@PORT)" & vbCrLf
sConsulta = sConsulta & "       IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "       BEGIN   " & vbCrLf
sConsulta = sConsulta & "           UPDATE CIAS_PORT SET FECACTFP=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           IF @ESTOLD=3 AND @EST<>3 " & vbCrLf
sConsulta = sConsulta & "           BEGIN   " & vbCrLf
sConsulta = sConsulta & "               UPDATE CIAS_PORT SET FECDESACTFP=GETDATE() WHERE CIA=@CIA AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_CIAS_PORT_Upd INTO @CIA,@PORT,@EST" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_CIAS_PORT_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CIAS_PORT_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER COM_TG_INSUPD ON dbo.COM" & vbCrLf
sConsulta = sConsulta & "FOR INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_COM_Ins CURSOR FOR SELECT ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_COM_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_COM_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET FECACT=GETDATE() WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_COM_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_COM_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_COM_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER MCIAS_ACT1_TG_INS ON dbo.MCIAS_ACT1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_MCia_Act1_Ins CURSOR FOR SELECT CIA,ACT1 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_MCia_Act1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act1_Ins INTO @CIA,@ACT1" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT2 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MCIAS_ACT2 SELECT @CIA,ACT1,ID FROM ACT2 WHERE ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act1_Ins INTO @CIA,@ACT1" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_MCia_Act1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_MCia_Act1_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER MCIAS_ACT2_TG_INS ON dbo.MCIAS_ACT2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_MCia_Act2_Ins CURSOR FOR SELECT CIA,ACT1,ACT2 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_MCia_Act2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act2_Ins INTO @CIA,@ACT1,@ACT2" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT3 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MCIAS_ACT3 SELECT @CIA,ACT1,ACT2,ID FROM ACT3 WHERE ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act2_Ins INTO @CIA,@ACT1,@ACT2" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Close curTG_MCia_Act2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_MCia_Act2_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER MCIAS_ACT3_TG_INS ON dbo.MCIAS_ACT3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER,@ACT3 INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_MCia_Act3_Ins CURSOR FOR SELECT CIA,ACT1,ACT2,ACT3 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_MCia_Act3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act3_Ins INTO @CIA,@ACT1,@ACT2,@ACT3" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT4 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3" & vbCrLf
sConsulta = sConsulta & "DELETE FROM MCIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3= @ACT3" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MCIAS_ACT4 SELECT @CIA,ACT1,ACT2,ACT3,ID FROM ACT4 WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_MCia_Act3_Ins INTO @CIA,@ACT1,@ACT2,@ACT3" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Close curTG_MCia_Act3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_MCia_Act3_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER MCIAS_ACT4_TG_INS ON dbo.MCIAS_ACT4" & vbCrLf
sConsulta = sConsulta & " FOR INSERT" & vbCrLf
sConsulta = sConsulta & " AS" & vbCrLf
sConsulta = sConsulta & " DECLARE @CIA INTEGER,@ACT1 INTEGER,@ACT2 INTEGER,@ACT3 INTEGER,@ACT4 INTEGER" & vbCrLf
sConsulta = sConsulta & " DECLARE curTG_MCia_Act4_Ins CURSOR FOR SELECT CIA,ACT1,ACT2,ACT3,ACT4 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & " OPEN curTG_MCia_Act4_Ins" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_MCia_Act4_Ins INTO @CIA,@ACT1,@ACT2,@ACT3,@ACT4" & vbCrLf
sConsulta = sConsulta & " WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & " DELETE FROM MCIAS_ACT5 WHERE CIA=@CIA AND ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3= @ACT3 AND ACT4=@ACT4" & vbCrLf
sConsulta = sConsulta & " INSERT INTO MCIAS_ACT5 SELECT @CIA,ACT1,ACT2,ACT3,ACT4,ID FROM ACT5 WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3 AND ACT4=@ACT4" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_MCia_Act4_Ins INTO @CIA,@ACT1,@ACT2,@ACT3,@ACT4" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " Close curTG_MCia_Act4_Ins" & vbCrLf
sConsulta = sConsulta & " DEALLOCATE curTG_MCia_Act4_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER REL_CIAS_TG_UPD ON [dbo].[REL_CIAS]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIACOMP int" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAPROVE int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVECOD varchar(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODOLD varchar(40)" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR SELECT cia_comp, cia_prove, cod_prove_cia from inserted" & vbCrLf
sConsulta = sConsulta & "open c1" & vbCrLf
sConsulta = sConsulta & "declare @PROVESYAENLAZADOS varchar(2000)" & vbCrLf
sConsulta = sConsulta & "SET @PROVESYAENLAZADOS =''" & vbCrLf
sConsulta = sConsulta & "fetch next FROM c1 into @CIACOMP, @CIAPROVE, @PROVECOD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @CODOLD = c.cod" & vbCrLf
sConsulta = sConsulta & "         from rel_cias r" & vbCrLf
sConsulta = sConsulta & "               inner join cias c" & vbCrLf
sConsulta = sConsulta & "                       on r.cia_prove = c.id" & vbCrLf
sConsulta = sConsulta & "        where r.cia_comp = @CIACOMP" & vbCrLf
sConsulta = sConsulta & "          and r.cod_prove_cia = @PROVECOD" & vbCrLf
sConsulta = sConsulta & "          and r.cia_prove != @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "       IF @CODOLD is not null" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF @PROVESYAENLAZADOS = ''" & vbCrLf
sConsulta = sConsulta & "                set @PROVESYAENLAZADOS = @PROVESYAENLAZADOS  + @CODOLD" & vbCrLf
sConsulta = sConsulta & "           Else" & vbCrLf
sConsulta = sConsulta & "                set @PROVESYAENLAZADOS = @PROVESYAENLAZADOS +  ', ' + @CODOLD" & vbCrLf
sConsulta = sConsulta & "          End" & vbCrLf
sConsulta = sConsulta & "       fetch next FROM c1 into @CIACOMP, @CIAPROVE, @PROVECOD" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "Close C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "if @PROVESYAENLAZADOS !=''" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    RAISERROR (50009,16,1,@PROVESYAENLAZADOS)" & vbCrLf
sConsulta = sConsulta & "    ROLLBACK TRANSACTION" & vbCrLf
sConsulta = sConsulta & "    End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER REL_COMP_TG_INS_DEL ON [dbo].[REL_COMP]" & vbCrLf
sConsulta = sConsulta & " FOR INSERT, DELETE" & vbCrLf
sConsulta = sConsulta & " AS" & vbCrLf
sConsulta = sConsulta & "/* DECLARE @PROVE varchar(40)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " DECLARE @MREL int" & vbCrLf
sConsulta = sConsulta & " DECLARE @REL int" & vbCrLf
sConsulta = sConsulta & " DECLARE @COINCIDENTES INT" & vbCrLf
sConsulta = sConsulta & " declare c1 cursor for Select cia_prove from inserted union Select cia_prove from deleted" & vbCrLf
sConsulta = sConsulta & " open c1" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM c1 INTO @PROVE" & vbCrLf
sConsulta = sConsulta & " WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "              SELECT @MREL = COUNT(*) FROM MREL_COMP WHERE CIA_PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              SELECT @REL = COUNT(*) FROM REL_COMP WHERE CIA_PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "          SELECT @COINCIDENTES = COUNT(*)" & vbCrLf
sConsulta = sConsulta & "                FROM MREL_COMP M" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN REL_COMP R" & vbCrLf
sConsulta = sConsulta & "                                 ON M.CIA_PROVE = R.CIA_PROVE" & vbCrLf
sConsulta = sConsulta & "                                AND M.CIA_COMP = R.CIA_COMP" & vbCrLf
sConsulta = sConsulta & "               WHERE M.CIA_PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              IF @COINCIDENTES!=@MREL OR @COINCIDENTES != @REL" & vbCrLf
sConsulta = sConsulta & "                    UPDATE CIAS SET PENDCONFREG = 1" & vbCrLf
sConsulta = sConsulta & "                     WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "              Else" & vbCrLf
sConsulta = sConsulta & "                    UPDATE CIAS SET PENDCONFREG = 0" & vbCrLf
sConsulta = sConsulta & "                    WHERE ID=@PROVE" & vbCrLf
sConsulta = sConsulta & "         FETCH NEXT FROM c1 INTO @PROVE" & vbCrLf
sConsulta = sConsulta & "     End" & vbCrLf
sConsulta = sConsulta & " Close c1" & vbCrLf
sConsulta = sConsulta & " DEALLOCATE c1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "delete from mrel_comp " & vbCrLf
sConsulta = sConsulta & "  from mrel_comp m" & vbCrLf
sConsulta = sConsulta & "         inner join deleted d" & vbCrLf
sConsulta = sConsulta & "                 on m.cia_prove = d.cia_prove" & vbCrLf
sConsulta = sConsulta & "                and m.cia_comp = d.cia_comp" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into mrel_comp (cia_prove, cia_comp)" & vbCrLf
sConsulta = sConsulta & "select i.cia_prove, i.cia_comp " & vbCrLf
sConsulta = sConsulta & "  from inserted i " & vbCrLf
sConsulta = sConsulta & " where not exists (select * " & vbCrLf
sConsulta = sConsulta & "                     from mrel_comp mc " & vbCrLf
sConsulta = sConsulta & "                    where mc.cia_prove = i.cia_prove " & vbCrLf
sConsulta = sConsulta & "                      and mc.cia_comp = i.cia_comp)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [USU_TG_INSUPD] ON dbo.USU " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "      FROM INSERTED I " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN CIAS  C" & vbCrLf
sConsulta = sConsulta & "                      ON I.CIA = C.ID " & vbCrLf
sConsulta = sConsulta & "     WHERE EXISTS (SELECT * " & vbCrLf
sConsulta = sConsulta & "                     FROM USU " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                   ON USU.CIA = CIAS.ID " & vbCrLf
sConsulta = sConsulta & "                    WHERE USU.COD =I.COD " & vbCrLf
sConsulta = sConsulta & "                      AND I.CIA!=USU.CIA" & vbCrLf
sConsulta = sConsulta & "                      AND CIAS.FCEST=3)" & vbCrLf
sConsulta = sConsulta & "       AND C.FCEST=3)>0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    RAISERROR ('El c�digo del usuario comprador ya existe en el portal',16,1)" & vbCrLf
sConsulta = sConsulta & "    ROLLBACK TRANSACTION" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [USU_TG_UPD] ON dbo.USU " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT,@EST INT,@ESTOLD INT,@CIA INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_USU_Upd CURSOR FOR SELECT ID,FCEST,CIA FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_USU_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           UPDATE USU SET FECLASTUPD=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "           SET @ESTOLD=(SELECT FCEST FROM DELETED WHERE ID=@ID AND CIA=@CIA)" & vbCrLf
sConsulta = sConsulta & "           IF @ESTOLD<> 3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "               BEGIN   " & vbCrLf
sConsulta = sConsulta & "               UPDATE USU SET FECACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               IF @ESTOLD=3 AND @EST<>3 " & vbCrLf
sConsulta = sConsulta & "               BEGIN   " & vbCrLf
sConsulta = sConsulta & "               UPDATE USU SET FECDESACTFC=GETDATE() WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curTG_USU_Upd INTO @ID,@EST,@CIA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_USU_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_USU_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Inserta un movimiento en la tabla MVTOS_PROV (el tipo de movimiento no nos importa porque siempre hacemos borrar e insertar)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT CIA, ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @CIA,@ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM INSERTED " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN USU_PORT " & vbCrLf
sConsulta = sConsulta & "                                    ON INSERTED.ID=USU_PORT.USU" & vbCrLf
sConsulta = sConsulta & "                              AND INSERTED.CIA=USU_PORT.CIA " & vbCrLf
sConsulta = sConsulta & "                                   AND (USU_PORT.FPEST=3 OR  USU_PORT.FPEST=2)" & vbCrLf
sConsulta = sConsulta & "                                   AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                                    ON INSERTED.CIA=REL_CIAS.CIA_PROVE " & vbCrLf
sConsulta = sConsulta & "                                   AND REL_CIAS.EST=3 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                    ON REL_CIAS.CIA_COMP=CIAS.ID " & vbCrLf
sConsulta = sConsulta & "                                   AND CIAS.FCEST=3 " & vbCrLf
sConsulta = sConsulta & "                     WHERE INSERTED.ID=@ID " & vbCrLf
sConsulta = sConsulta & "                       AND INSERTED.CIA=@CIA)" & vbCrLf
sConsulta = sConsulta & "    IF @ENLAZADA >0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE= @INDICE + 1" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@CIA,@ID,'U',1,GETDATE())" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @CIA,@ID" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [USU_TG_INS] ON dbo.USU " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE USU " & vbCrLf
sConsulta = sConsulta & "   SET FECINS = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM USU U" & vbCrLf
sConsulta = sConsulta & "     inner join inserted I" & vbCrLf
sConsulta = sConsulta & "             on u.cia = i.cia" & vbCrLf
sConsulta = sConsulta & "            and u.id = i.id" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [USU_PORT_TG_DEL] ON dbo.USU_PORT" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENLAZADA INT,@CIA INT,@ID INT,@INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare c1 cursor local for select usu, cia from deleted" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "open c1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "fetch next from c1 into @ID, @CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) FROM USU " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN DELETED " & vbCrLf
sConsulta = sConsulta & "                                  ON USU.ID=DELETED.USU " & vbCrLf
sConsulta = sConsulta & "                                 AND USU.CIA=DELETED.CIA " & vbCrLf
sConsulta = sConsulta & "                                 AND (DELETED.FPEST=3 OR DELETED.FPEST=2) " & vbCrLf
sConsulta = sConsulta & "                                 AND DELETED.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                                  ON USU.CIA=REL_CIAS.CIA_PROVE " & vbCrLf
sConsulta = sConsulta & "                                 AND REL_CIAS.EST=3 " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                  ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "                                 AND CIAS.FCEST=3 " & vbCrLf
sConsulta = sConsulta & "                    WHERE USU.ID=@ID " & vbCrLf
sConsulta = sConsulta & "                      AND USU.CIA=@CIA)" & vbCrLf
sConsulta = sConsulta & "    IF @ENLAZADA > 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @INDICE=(SELECT MAX(ID) FROM MVTOS_PROV) " & vbCrLf
sConsulta = sConsulta & "        IF @INDICE IS NULL " & vbCrLf
sConsulta = sConsulta & "          SET @INDICE=1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE=@INDICE+1" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@CIA,@ID,'D',1,GETDATE())" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    fetch next from c1 into @ID, @CIA" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [USU_PORT_TG_INS] ON [USU_PORT] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@USU INT,@PORT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_USU_PORT_Ins CURSOR FOR SELECT CIA,USU,PORT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_USU_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_USU_PORT_Ins INTO @CIA,@USU,@PORT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU_PORT SET FECINS=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_USU_PORT_Ins INTO @CIA,@USU,@PORT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_USU_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_USU_PORT_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [USU_PORT_TG_UPD] ON [USU_PORT] " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT,@USU INT,@PORT INT,@EST INT,@ESTOLD INT,@ID INT,@USU_CIA INT,@INDICE INT,@ENLAZADA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_USU_PORT_Upd CURSOR local FOR SELECT CIA,USU,PORT,FPEST FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_USU_PORT_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_USU_PORT_Upd INTO @CIA,@USU,@PORT,@EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU_PORT SET FECLASTUPD=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       SET @ESTOLD=(SELECT FPEST FROM DELETED  WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT)" & vbCrLf
sConsulta = sConsulta & "       IF @ESTOLD<>3 AND @EST=3" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE USU_PORT SET FECACTFP=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           IF @ESTOLD=3 AND @EST<> 3 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE USU_PORT SET FECDESACTFP=GETDATE() WHERE CIA=@CIA AND USU=@USU AND PORT=@PORT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_USU_PORT_Upd INTO @CIA,@USU,@PORT,@EST" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_USU_PORT_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_USU_PORT_Upd  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare c1 cursor local for select usu, cia from INSERTED" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "open c1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "fetch next from c1 into @ID, @CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @ENLAZADA =(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM USU " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN INSERTED " & vbCrLf
sConsulta = sConsulta & "                                    ON USU.ID=INSERTED.USU " & vbCrLf
sConsulta = sConsulta & "                                   AND USU.CIA=INSERTED.CIA " & vbCrLf
sConsulta = sConsulta & "                                   AND (INSERTED.FPEST=3 OR INSERTED.FPEST=2)" & vbCrLf
sConsulta = sConsulta & "                                   AND INSERTED.PORT=0 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN REL_CIAS " & vbCrLf
sConsulta = sConsulta & "                                    ON USU.CIA=REL_CIAS.CIA_PROVE " & vbCrLf
sConsulta = sConsulta & "                                   AND REL_CIAS.EST=3 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN CIAS " & vbCrLf
sConsulta = sConsulta & "                                    ON REL_CIAS.CIA_COMP=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "                                   AND CIAS.FCEST=3 " & vbCrLf
sConsulta = sConsulta & "                     WHERE USU.ID=INSERTED.USU " & vbCrLf
sConsulta = sConsulta & "                       AND USU.CIA=INSERTED.CIA)" & vbCrLf
sConsulta = sConsulta & "    IF @ENLAZADA > 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @INDICE = (SELECT MAX(ID) FROM MVTOS_PROV)" & vbCrLf
sConsulta = sConsulta & "        IF @INDICE IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @INDICE = @INDICE + 1    " & vbCrLf
sConsulta = sConsulta & "        INSERT INTO MVTOS_PROV(ID,CIA,USU,TIPO,TABLA,FECACT) VALUES (@INDICE,@CIA,@ID,'U',1,GETDATE())" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    fetch next from c1 into @ID, @CIA" & vbCrLf
sConsulta = sConsulta & "  END  " & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [VOLADJ_TG_INS] ON dbo.VOLADJ" & vbCrLf
sConsulta = sConsulta & "FOR INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_VOLADJ_Ins CURSOR FOR SELECT CIA_PROVE,CIA_COMP,ANYO,GMN1,PROCE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_VOLADJ_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_VOLADJ_Ins INTO @PROVE,@COMP,@ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          UPDATE VOLADJ SET FECACT=GETDATE() WHERE CIA_PROVE=@PROVE AND CIA_COMP=@COMP AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "          FETCH NEXT FROM curTG_VOLADJ_Ins INTO @PROVE,@COMP,@ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "Close curTG_VOLADJ_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_VOLADJ_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub



Private Sub V_2_10_Eliminar_Defaults()

EliminarDefaults gRDOCon, oDMOBD, "CIAS"
EliminarDefaults gRDOCon, oDMOBD, "PARGEN_GEST"
EliminarDefaults gRDOCon, oDMOBD, "PARGEN_INTERNO"
EliminarDefaults gRDOCon, oDMOBD, "REL_CIAS"

End Sub


