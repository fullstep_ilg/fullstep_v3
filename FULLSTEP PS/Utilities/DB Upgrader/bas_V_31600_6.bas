Attribute VB_Name = "bas_V_31600_6"
Option Explicit

Public Function CodigoDeActualizacion3_00_00_28A3_00_01_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True

   frmProgreso.lblDetalle = "Cambios en Tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31600_Tablas_0

   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31600_Storeds_0
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.01.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_00_28A3_00_01_00 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_00_28A3_00_01_00 = False
End Function

Public Function CodigoDeActualizacion3_00_01_00A3_00_01_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True


   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31600_Storeds_01
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.01.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_01_00A3_00_01_01 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_01_00A3_00_01_01 = False
End Function

Private Sub V_31600_Storeds_01()
Dim sConsulta As String

'FSPM_MONEDAS
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_MONEDAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_MONEDAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_MONEDAS @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1, @IDIOMA VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_MONEDAS @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50), @DEN VARCHAR(2000) , @COINCID TINYINT,  @IDIOMA VARCHAR (20)', @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub



Private Sub V_31600_Storeds_0()
Dim sConsulta As String

'FSPM_GETWORKFLOW_PROVE
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETWORKFLOW_PROVE @CIA INT,@PROVE VARCHAR(50), @IDI VARCHAR(50), @TIPO INT =0 , @FECHA  VARCHAR(10)=NULL, @INSTANCIA INT=0, @FILAESTADO INT =0, @FORMATOFECHA VARCHAR(20)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETWORKFLOW_PROVE @PRV=@PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@FILAESTADO=@FILAESTADO,@FORMATOFECHA=@FORMATOFECHA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @IDI VARCHAR(50) , @TI INT,@FEC  VARCHAR(10),@INS INT, @FILAESTADO INT,@FORMATOFECHA  VARCHAR(20)', @PRV=@PROVEGS,@IDI=@IDI, @TI=@TIPO,@FEC=@FECHA,@INS=@INSTANCIA,@FILAESTADO=@FILAESTADO,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


'FSPM_SOLICITUD_DATOS_MENSAJE
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SOLICITUD_DATOS_MENSAJE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SOLICITUD_DATOS_MENSAJE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_SOLICITUD_DATOS_MENSAJE @CIA INT,@CIA_PROVE INT, @ID INT,@TYPE INT,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0,@COD_USU VARCHAR(50), @IDI VARCHAR(20)='SPA',@FORMATOFECHA VARCHAR(20)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =  N'EXEC ' + @FSGS + 'FSPM_SOLICITUD_DATOS_MENSAJE @ID=@ID, @TYPE=@TYPE, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE, @IDI=@IDI,@FORMATOFECHA=@FORMATOFECHA  '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT,@TYPE INT,@BLOQUE INT=0,@ACCION INT=0,@ENLACE INT=0, @IDI VARCHAR(20),@FORMATOFECHA VARCHAR(20)',@ID=@ID, @TYPE=@TYPE, @BLOQUE=@BLOQUE, @ACCION=@ACCION, @ENLACE=@ENLACE, @IDI=@IDI,@FORMATOFECHA=@FORMATOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtener los datos del Usuario de la Compa�ia" & vbCrLf
sConsulta = sConsulta & "SELECT U.COD COD_USU,U.NOM NOM_USU,U.APE APE_USU,U.TFNO TFNO1_USU,U.EMAIL EMAIL_USU,U.FAX FAX_USU, C.COD COD_COM,C.DEN DEN_COM,C.NIF NIF_COM" & vbCrLf
sConsulta = sConsulta & "FROM USU U" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS C ON U.CIA = C.ID WHERE C.ID =@CIA_PROVE AND U.COD = @COD_USU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'SP_ANYA_MON
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_MON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_MON (@COD varchar(50), @DEN_SPA varchar(100), @DEN_ENG varchar(100), @DEN_GER varchar(100), @EQUIV FLOAT,@EQ_ACT TINYINT) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMON INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDMON=MAX(ID) FROM MON" & vbCrLf
sConsulta = sConsulta & "IF @IDMON IS NULL " & vbCrLf
sConsulta = sConsulta & "   SET @IDMON=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @IDMON=@IDMON+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO MON (ID,COD, DEN_SPA,DEN_ENG,DEN_GER, EQUIV,EQ_ACT) VALUES (@IDMON,@COD, @DEN_SPA, @DEN_ENG, @DEN_GER, @EQUIV,@EQ_ACT)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'SP_MODIF_MON
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_MON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_MODIF_MON  (@COD VARCHAR(50),@DEN_SPA VARCHAR(100),@DEN_ENG VARCHAR(100),@DEN_GER VARCHAR(100),@EQUIV FLOAT,@EQ_ACT TINYINT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE MON SET DEN_SPA=@DEN_SPA,DEN_ENG=@DEN_ENG,DEN_GER=@DEN_GER,EQUIV=@EQUIV,EQ_ACT=@EQ_ACT WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'FSPM_MONEDAS
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_MONEDAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_MONEDAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_MONEDAS @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_MONEDAS @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50), @DEN VARCHAR(2000) , @COINCID TINYINT,  @IDIOMA VARCHAR', @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


'FSPM_PROVE
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PROVE @CIA INT, @PROVE VARCHAR(50), @PORTAL TINYINT = 0, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 1 " & vbCrLf
sConsulta = sConsulta & "  SELECT @PROVEGS = COD_PROVE_CIA   FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @PROVEGS = @PROVE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVE @PROVE =@PROVE, @IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @PROVE VARCHAR(50), @IDIOMA VARCHAR(50)  ', @PROVE =@PROVEGS, @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


'FSPM_PROVES
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PROVES @CIA INT, @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50)='SPA' AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS AS VARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @COD " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVES @COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU, @IDI=@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50) ',@COD=@PROVEGS, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'FSPM_UNIDADES
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UNIDADES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UNIDADES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_UNIDADES @CIA INT,@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT = 1, @IDIOMA VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UNIDADES @COD =@COD , @DEN =@DEN ,@USALIKE  =@USALIKE , @IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT, @IDIOMA VARCHAR(20)  ', @COD =@COD , @DEN =@DEN , @USALIKE  =@USALIKE, @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'FSPM_FORMASPAGO
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_FORMASPAGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_FORMASPAGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_FORMASPAGO @CIA INT,  @COD VARCHAR(50) =NULL, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_FORMASPAGO @COD =@COD,@IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =NULL,@IDIOMA VARCHAR(50)',@COD =@COD,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'FSPM_DESTINOS
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DESTINOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DESTINOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DESTINOS @CIA INT, @PER VARCHAR(50) =NULL, @COD VARCHAR(50) =NULL, @IDI VARCHAR(20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_DESTINOS @PER =@PER ,@COD =@COD, @IDI=@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PER VARCHAR(50) =NULL, @COD VARCHAR(50) =NULL,@IDI VARCHAR(20)', @PER =@PER ,@COD =@COD,@IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'FSPM_ARTICULOS
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ARTICULOS @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA', @INSTANCIAMONEDA VARCHAR(3)='EUR',@CARGAR_ULT_ADJ TINYINT=0 AS    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31600_Tablas_0()
Dim sConsulta As String

sConsulta = "ALTER TABLE [dbo].[MON] ADD [DEN_SPA] [varchar] (100) NULL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[MON] ADD [DEN_ENG] [varchar] (100) NULL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[MON] ADD [DEN_GER] [varchar] (100) NULL " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE [dbo].MON SET DEN_SPA=DEN,DEN_ENG=DEN,DEN_GER=DEN " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "   DROP INDEX MON.IX_MON_02" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "ALTER TABLE [dbo].[MON] DROP COLUMN [DEN] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[MON] ALTER COLUMN [DEN_SPA] [varchar] (100) NOT NULL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[MON] ALTER COLUMN [DEN_ENG] [varchar] (100) NOT NULL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[MON] ALTER COLUMN [DEN_GER] [varchar] (100) NOT NULL " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion3_00_01_01A3_00_01_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True

   frmProgreso.lblDetalle = "Cambios en Tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31600_Tablas_02
   
'   frmProgreso.lblDetalle = "Cambios en Stored Procedures"
'   frmProgreso.lblDetalle.Refresh
'   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
'   V_31600_Storeds_01
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.01.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion3_00_01_01A3_00_01_02 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion3_00_01_01A3_00_01_02 = False
End Function

Private Sub V_31600_Tablas_02()
Dim sConsulta As String


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ACT2_ACT1]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT2] DROP CONSTRAINT FK_ACT2_ACT1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT2] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ACT2_ACT1] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT1] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ACT3_ACT2]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT3] DROP CONSTRAINT FK_ACT3_ACT2" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT3] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ACT3_ACT2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT2] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ACT4_ACT3]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT4] DROP CONSTRAINT FK_ACT4_ACT3" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT4] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ACT4_ACT3] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT3] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ACT5_ACT4]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ACT5] DROP CONSTRAINT FK_ACT5_ACT4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ACT5] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ACT5_ACT4] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]," & vbCrLf
sConsulta = sConsulta & "       [ACT4]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ACT4] (" & vbCrLf
sConsulta = sConsulta & "       [ACT1]," & vbCrLf
sConsulta = sConsulta & "       [ACT2]," & vbCrLf
sConsulta = sConsulta & "       [ACT3]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_00_01_02A3_00_01_03()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

'    frmProgreso.lblDetalle = "Cambios en Tablas"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    frmProgreso.ProgressBar1.Refresh
'    V_31600_Tablas_03
   
    frmProgreso.lblDetalle = "Cambios en Stored Procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31600_Storeds_03

'    frmProgreso.lblDetalle = "Actualiza triggers"
'    frmProgreso.lblDetalle.Refresh
'    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
'    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'    frmProgreso.ProgressBar1.Refresh
'    V_31600_Triggers_03
    
    frmProgreso.lblDetalle = "Actualiza vistas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31600_Vistas_03

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.01.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
   
     ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
     bTransaccionEnCurso = False
    
     CodigoDeActualizacion3_00_01_02A3_00_01_03 = True
     Exit Function
   
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion3_00_01_02A3_00_01_03 = False
End Function

Private Sub V_31600_Storeds_03()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE     PROCEDURE SP_BUSCAR_ACTIVIDADES_PROVE_PORTAL @SDEN VARCHAR(8),@SBUSCAR VARCHAR(50),@SCOMP INT AS" & vbCrLf
sConsulta = sConsulta & "declare @IDIOMA AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA= 'x.' + @SDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "edu" & vbCrLf
sConsulta = sConsulta & "cuidado." & vbCrLf
sConsulta = sConsulta & "la sentencia SQL ocupaba mas de 4000 caracteres y provocaba errores impredecibles y diferentes en ejecucion" & vbCrLf
sConsulta = sConsulta & "segun la longitud del texto a buscar, se truncaba hasta una determinada posicion." & vbCrLf
sConsulta = sConsulta & "Se han creado 5 vistas VIEW_ACT1 a VIEW_ACT5 que realizan la relacion del arbol con sus niveles superiores." & vbCrLf
sConsulta = sConsulta & "Con eso se simplifica el query y ahora ocupa la mitad, aunque sigue siendo un tocho de cuidado." & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL = '" & vbCrLf
sConsulta = sConsulta & "SELECT x.ID ACT1, NULL ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA + ' DEN ,COUNT(J.CIA) AS PROVE, CODACT1, NULL CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT1 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1 FROM CIAS_ACT1 UNION SELECT CIA, ACT1 FROM CIAS_ACT2 UNION SELECT CIA, ACT1 FROM CIAS_ACT3 UNION SELECT CIA, ACT1 FROM CIAS_ACT4 UNION SELECT CIA, ACT1 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT1=x.ID " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%'' " & vbCrLf
sConsulta = sConsulta & "GROUP BY   x.ID, CODACT1, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1, x.ID ACT2, NULL ACT3, NULL ACT4, NULL ACT5,  ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, NULL CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT2 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2 FROM CIAS_ACT2 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=PP.CIA) J ON J.ACT2=x.ID  AND J.ACT1=x.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY  x.ACT1, x.ID, CODACT1, CODACT2,' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ID ACT3, NULL ACT4, NULL ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3, NULL CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT3 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT3 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA) J ON J.ACT3=x.ID AND J.ACT2=x.ACT2 AND J.ACT1=x.ACT1 " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ID, CODACT1, CODACT2, CODACT3, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ACT3, x.ID ACT4, NULL ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3,CODACT4, NULL CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT4 x" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT PP.* FROM (SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT4 UNION SELECT CIA, ACT1, ACT2, ACT3, ACT4 FROM CIAS_ACT5) PP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=PP.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP AND RC.CIA_PROVE=PP.CIA)  J ON J.ACT4=x.ID AND J.ACT3=x.ACT3 AND J.ACT2=x.ACT2 AND J.ACT1=x.ACT1  " & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA+ ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ACT3,x.ID, CODACT1, CODACT2, CODACT3, CODACT4, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT x.ACT1,x.ACT2, x.ACT3, x.ACT4, x.ID ACT5, ' + @IDIOMA + ' DEN, COUNT(J.CIA) AS PROVE, CODACT1, CODACT2, CODACT3, CODACT4 , CODACT5" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_ACT5 x" & vbCrLf
sConsulta = sConsulta & "LEFT  JOIN CIAS_ACT5 J ON J.ACT5=x.ID AND J.ACT4=x.ACT4 AND J.ACT3=x.ACT3 AND J.ACT2=x.ACT2  AND J.ACT1=x.ACT1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CIAS_PORT CP ON CP.CIA=J.CIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN REL_COMP AS RC ON '  + CAST(@SCOMP AS VARCHAR) + '=RC.CIA_COMP  AND RC.CIA_PROVE=J.CIA" & vbCrLf
sConsulta = sConsulta & "WHERE ' +  @IDIOMA + ' LIKE ''%' + @SBUSCAR + '%''" & vbCrLf
sConsulta = sConsulta & "GROUP BY x.ACT1,x.ACT2,x.ACT3,x.ACT4,x.ID, CODACT1, CODACT2, CODACT3, CODACT4, CODACT5, ' + @IDIOMA + '" & vbCrLf
sConsulta = sConsulta & "ORDER by 1, 2, 3, 4, 5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ARTICULOS @CIA INT, @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @FILAS INT =NULL, @DESDEFILA INT = NULL, @IDI VARCHAR(50) = 'SPA', @INSTANCIAMONEDA VARCHAR(3)='EUR',@CARGAR_ULT_ADJ TINYINT=0 ,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL  AS    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_ARTICULOS @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50), @FILAS INT, @DESDEFILA INT , @IDI VARCHAR(50),@INSTANCIAMONEDA VARCHAR(3),@CARGAR_ULT_ADJ TINYINT,@CODORGCOMPRAS VARCHAR(4)=NULL,@CODCENTRO VARCHAR(4)=NULL, @CODINI VARCHAR(50) = NULL, @CODULT VARCHAR(50) = NULL',  @COD =@COD ,@DEN =@DEN , @GMN1 =@GMN1 , @GMN2 =@GMN2 , @GMN3 =@GMN3 , @GMN4 =@GMN4 , @COINCID =@COINCID, @PER =@PER , @FILAS =@FILAS ,@DESDEFILA =@DESDEFILA , @IDI =@IDI,@INSTANCIAMONEDA=@INSTANCIAMONEDA,@CARGAR_ULT_ADJ=@CARGAR_ULT_ADJ,@CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO, @CODINI=@CODINI, @CODULT=@CODULT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_GET_VERSION_NOCONF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_GET_VERSION_NOCONF]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_GET_VERSION_NOCONF @CIA INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GET_VERSION_NOCONF @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PROVES @CIA INT, @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50)='SPA', @ORGCOMPRAS VARCHAR(4)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS AS VARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @COD " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVES @COD=@COD, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU, @IDI=@IDI,@ORGCOMPRAS =@ORGCOMPRAS '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @COD VARCHAR(50), @DEN VARCHAR(200)=NULL, @NIF VARCHAR(50)=NULL , @USALIKE TINYINT = 1,@USU VARCHAR(50)=NULL, @IDI VARCHAR(50) ,@ORGCOMPRAS VARCHAR(4)',@COD=@PROVEGS, @DEN =@DEN , @NIF =@NIF  , @USALIKE =@USALIKE ,@USU =@USU,@IDI=@IDI,@ORGCOMPRAS =@ORGCOMPRAS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31600_Vistas_03()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT1]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT1 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, x.COD CODACT1" & vbCrLf
sConsulta = sConsulta & "FROM ACT1 as x" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT2]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT2 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, ACT1.COD CODACT1, x.COD CODACT2" & vbCrLf
sConsulta = sConsulta & "FROM ACT2 as x" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT3]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT3 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, ACT1.COD CODACT1, ACT2.COD CODACT2, x.COD CODACT3" & vbCrLf
sConsulta = sConsulta & "FROM ACT3 as x" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON x.ACT1 = ACT2.ACT1 AND x.ACT2 = ACT2.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT4]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT4 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, x.COD CODACT4 " & vbCrLf
sConsulta = sConsulta & "FROM ACT4 as x" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON x.ACT1 = ACT2.ACT1 AND x.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 ON x.ACT1 = ACT3.ACT1 AND x.ACT2 = ACT3.ACT2 AND x.ACT3 = ACT3.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_ACT5]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_ACT5]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE view VIEW_ACT5 as " & vbCrLf
sConsulta = sConsulta & "SELECT x.*, ACT1.COD CODACT1, ACT2.COD CODACT2, ACT3.COD CODACT3, ACT4.COD CODACT4, x.COD CODACT5 " & vbCrLf
sConsulta = sConsulta & "FROM ACT5 as x" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT1 ON x.ACT1 = ACT1.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT2 ON x.ACT1 = ACT2.ACT1 AND x.ACT2 = ACT2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT3 ON x.ACT1 = ACT3.ACT1 AND x.ACT2 = ACT3.ACT2 AND x.ACT3 = ACT3.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ACT4 ON x.ACT1 = ACT4.ACT1 AND x.ACT2 = ACT4.ACT2 AND x.ACT3 = ACT4.ACT3 AND x.ACT3 = ACT4.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion3_00_01_03A3_00_01_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True


    frmProgreso.lblDetalle = "Cambios en Stored Procedures"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31600_Storeds_04

    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.01.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
   
     ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
     bTransaccionEnCurso = False
    
     CodigoDeActualizacion3_00_01_03A3_00_01_04 = True
     Exit Function
   
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion3_00_01_03A3_00_01_04 = False
End Function


Private Sub V_31600_Storeds_04()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTFIELD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTFIELD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTFIELD @CIA INT, @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT, @CIAPROV INT = NULL , @PORTAL TINYINT = 0 OUTPUT  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTFIELD @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT', @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT * FROM COPIA_CAMPO WHERE ID = @ID  AND CIA=ISNULL(@CIAPROV,CIA)  AND INSTANCIA=@INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @PORTAL = 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL , @PORTAL tinyint = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @PORTAL = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =null , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "               SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                 FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                   LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                          ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                         AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                    ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "    IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA, 0 PORTAL" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "       ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                        ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT  3 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA, 1 PORTAL" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "       ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  3 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                  WHERE A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  4 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0)  DATASIZE , A.COMENT, @IDI IDIOMA, A.ID ADJUN, NULL CAMPO,  NULL RUTA,1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                  WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                      ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

