VERSION 5.00
Begin VB.Form frmCoordinarDatos 
   Caption         =   "Datos de  monedas, paises y provincias sin coordinar con el GS"
   ClientHeight    =   6645
   ClientLeft      =   3180
   ClientTop       =   2565
   ClientWidth     =   6030
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCoordinarDatos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6645
   ScaleWidth      =   6030
   Begin VB.TextBox txtDatos 
      BackColor       =   &H8000000F&
      Height          =   6735
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   0
      Width           =   6015
   End
End
Attribute VB_Name = "frmCoordinarDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_lWid As Double

Private Sub Form_Load()
m_lWid = Me.Width
End Sub

Private Sub Form_Resize()

On Error Resume Next
If Me.Height < 500 Then Exit Sub
If Me.Width < 500 Then Exit Sub

txtDatos.Height = Me.Height - 405
txtDatos.Width = Me.Width - 135

If m_lWid <> Me.Width Then
    m_lWid = Me.Width
End If
End Sub

