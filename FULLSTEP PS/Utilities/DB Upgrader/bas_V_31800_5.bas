Attribute VB_Name = "bas_V_31800_5"
Option Explicit

Public Function CodigoDeActualizacion31800_04_06_09A31800_05_07_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_04_06_09A31800_05_07_00 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_04_06_09A31800_05_07_00 = False
End Function




Public Function CodigoDeActualizacion31800_05_07_00A31800_05_07_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
    
   frmProgreso.lblDetalle = "Cambios en tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Tablas_001
    
    
   frmProgreso.lblDetalle = "Cambios en triggers"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Triggers_001
    
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_001

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_00A31800_05_07_01 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_00A31800_05_07_01 = False
End Function


Private Sub V_31800_5_Storeds_001()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_INSERT_REGISTRO_MAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_INSERT_REGISTRO_MAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_INSERT_REGISTRO_MAIL] @CIA INT, @SUBJECT NVARCHAR (100),@PARA NVARCHAR (100),@CC NVARCHAR (100),@CCO NVARCHAR (100)," & vbCrLf
sConsulta = sConsulta & "@DIR_RESPUESTA NVARCHAR(100),@CUERPO NTEXT,@ACUSE_RECIBO TINYINT,@TIPO TINYINT,@PRODUCTO INT ,@USU INT,@KO TINYINT," & vbCrLf
sConsulta = sConsulta & "@TEXTO_ERROR NVARCHAR(200) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID=MAX(ISNULL(ID,0))+1 FROM REGISTRO_EMAIL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO REGISTRO_EMAIL (CIA, ID, SUBJECT, PARA ,CC ,CCO ,DIR_RESPUESTA ,CUERPO ,ACUSE_RECIBO ,TIPO ,PRODUCTO ,USU ,KO ,TEXTO_ERROR)" & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA, @ID, @SUBJECT , @PARA ,@CC ,@CCO ,@DIR_RESPUESTA ,@CUERPO ,@ACUSE_RECIBO ,@TIPO ,@PRODUCTO ,@USU ,@KO ,@TEXTO_ERROR)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31800_5_Tablas_001()

Dim sConsulta As String

    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[REGISTRO_EMAIL]') and name=N'KO')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD [KO]  [tinyint] NOT NULL CONSTRAINT [DF_REGISTRO_EMAIL_KO] DEFAULT (0)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[REGISTRO_EMAIL]') and name=N'TEXTO_ERROR')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD [TEXTO_ERROR] [nvarchar] (200) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[REGISTRO_EMAIL]') and name=N'PRODUCTO')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD [PRODUCTO] [tinyint]  NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[REGISTRO_EMAIL]') and name=N'USU')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[REGISTRO_EMAIL] ADD [USU] [int]  NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "UPDATE REGISTRO_EMAIL SET PRODUCTO=1,KO=0" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    


End Sub

Private Sub V_31800_5_Triggers_001()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REGISTRO_EMAIL_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[REGISTRO_EMAIL_TG_INS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [REGISTRO_EMAIL_TG_INS] ON dbo.REGISTRO_EMAIL " & vbCrLf
sConsulta = sConsulta & "FOR INSERT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REGISTRO_EMAIL SET FECHA=GETDATE()" & vbCrLf
sConsulta = sConsulta & "FROM REGISTRO_EMAIL R INNER JOIN INSERTED I ON R.CIA=I.CIA AND R.ID=I.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub



Public Function CodigoDeActualizacion31800_05_07_01A31800_05_07_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
        
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_002

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_01A31800_05_07_02 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_01A31800_05_07_02 = False
End Function

Private Sub V_31800_5_Storeds_002()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEFTABLAEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEFTABLAEXTERNA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEFTABLAEXTERNA @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEFTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETTABLAEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETTABLAEXTERNA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETTABLAEXTERNA @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_02A31800_05_07_03()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
        
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_003

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_02A31800_05_07_03 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_02A31800_05_07_03 = False
End Function


Private Sub V_31800_5_Storeds_003()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ACTUALIZAR_EN_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ACTUALIZAR_EN_PROCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ACTUALIZAR_EN_PROCESO @CIA INT, @ID INT, @BLOQUE INT = 0, @EN_PROCESO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_ACTUALIZAR_EN_PROCESO @ID=@ID, @BLOQUE=@BLOQUE,@EN_PROCESO=@EN_PROCESO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @BLOQUE INT, @EN_PROCESO INT', @ID=@ID,@BLOQUE=@BLOQUE,@EN_PROCESO=@EN_PROCESO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_VALIDARNOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_VALIDARNOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_VALIDARNOCONFORMIDAD @CIA INT, @NOCONFORMIDAD INT , @VERSION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_VALIDARNOCONFORMIDAD @NOCONFORMIDAD =@NOCONFORMIDAD , @VERSION =@VERSION '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @NOCONFORMIDAD INT , @VERSION INT', @NOCONFORMIDAD =@NOCONFORMIDAD , @VERSION =@VERSION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_CREATE_NEW_CERTIFICADO_PREV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_CREATE_NEW_CERTIFICADO_PREV]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_CREATE_NEW_CERTIFICADO_PREV @CIA INT" & vbCrLf
sConsulta = sConsulta & ", @TIPO_CERTIF INT, @PROVE VARCHAR(50),  @FEC_DESPUB DATETIME=NULL, @PETICIONARIO VARCHAR(50),@CONTACTO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & ",  @RENOVACION INT=0, @CERT_PUB VARCHAR(4000) ='' OUTPUT, @CERT_WORKFLOW VARCHAR(4000) ='' OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_CREATE_TEMP_NEW_CERTIFICADO_PREV @TIPO_CERTIF=@TIPO_CERTIF,@PROVE=@PROVE,@FEC_DESPUB=@FEC_DESPUB,@PETICIONARIO=@PETICIONARIO,@CONTACTO=@CONTACTO, @RENOVACION = @RENOVACION, @CERT_PUB=@CERT_PUB, @CERT_WORKFLOW=@CERT_WORKFLOW '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO_CERTIF INT, @PROVE VARCHAR(50), @FEC_DESPUB DATETIME, @PETICIONARIO VARCHAR(50), @CONTACTO VARCHAR(50), @RENOVACION  INT,@CERT_PUB VARCHAR(4000) OUTPUT,@CERT_WORKFLOW VARCHAR(4000) OUTPUT'" & vbCrLf
sConsulta = sConsulta & ",@TIPO_CERTIF=@TIPO_CERTIF,@PROVE=@PROVE,@FEC_DESPUB=@FEC_DESPUB,@PETICIONARIO=@PETICIONARIO,@CONTACTO=@CONTACTO, @RENOVACION = @RENOVACION, @CERT_PUB=@CERT_PUB, @CERT_WORKFLOW=@CERT_WORKFLOW" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_EN_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_EN_PROCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_EN_PROCESO @CIA INT,@ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_EN_PROCESO @ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADCERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADCERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_LOADCERTIFICADO @CIA INT,@PROVE VARCHAR(50),  @CERTIFICADO INT = NULL , @PORTAL TINYINT =NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADCERTIFICADO @CERTIFICADO =@CERTIFICADO, @PORTAL=@PORTAL OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT =NULL,  @PORTAL TINYINT = NULL OUTPUT',@CERTIFICADO =@CERTIFICADO,  @PORTAL=@PORTAL OUTPUT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADNOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADNOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_LOADNOCONFORMIDAD @CIA INT,@PROVE VARCHAR(50),  @NOCONFORMIDAD INT = NULL , @PORTAL TINYINT =NULL OUTPUT , @IDI VARCHAR(3), @VERSION INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADNOCONFORMIDAD @NOCONFORMIDAD =@NOCONFORMIDAD, @PORTAL=@PORTAL OUTPUT, @IDI = @IDI , @VERSION = @VERSION'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NOCONFORMIDAD INT =NULL,  @PORTAL TINYINT = NULL OUTPUT,@IDI VARCHAR(3), @VERSION INT',@NOCONFORMIDAD =@NOCONFORMIDAD,  @PORTAL=@PORTAL OUTPUT, @IDI = @IDI, @VERSION = @VERSION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELDS_INSTANCIA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETFIELDS_INSTANCIA @CIA INT,@CIAPROVE INT,  @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL ,@PROVE VARCHAR(50)=NULL ,@RECENV TINYINT = 1, @NUEVO_WORKFLOW TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA_31600 @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ID INT, @GRUPO VARCHAR(50) , @CANTMAX TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @CANTMAX = 0 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * (CASE WHEN ISNULL(IO.CANTMAX,0) < = ISNULL(IO.CANT,0) THEN ISNULL(IO.CANTMAX,0) ELSE ISNULL(IO.CANT,0) END))" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT OBSADJUN, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEFTABLAEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEFTABLAEXTERNA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEFTABLAEXTERNA @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_DEFTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETTABLAEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETTABLAEXTERNA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETTABLAEXTERNA @CIA int, @ID int, @ART varchar(100)=NULL, @FILTROART bit=1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETTABLAEXTERNA @ID=@ID, @ART=@ART, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @ART VARCHAR(100) = NULL, @FILTROART bit = 1', @ID=@ID, @ART=@ART, @FILTROART=@FILTROART" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_03A31800_05_07_04()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
        
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_004

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_03A31800_05_07_04 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_03A31800_05_07_04 = False
End Function


Private Sub V_31800_5_Storeds_004()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_UPLOAD_ADJUN @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @CIA INT =NULL, @USU INT =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO COPIA_ADJUN (DATA, NOM, COMENT, FECALTA,CIA,USU) VALUES (NULL, @NOM, @COMENT, GETDATE(),@CIA, @USU)" & vbCrLf
sConsulta = sConsulta & "    SET @ID = (SELECT MAX(ID) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    UPDATE COPIA_ADJUN SET NOM = @NOM , COMENT = @COMENT WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
sConsulta = sConsulta & "    UPDATE COPIA_ADJUN SET DATA = @DATA WHERE ID =@ID " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    UPDATETEXT COPIA_ADJUN.DATA @textpointer @init 0 @data" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL , @PORTAL tinyint = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @PORTAL = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "               SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                 FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                   LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                          ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                         AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                    ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "    IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA, 0 PORTAL" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "       ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                     FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                    WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                        ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT  3 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA, 1 PORTAL" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "       ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  3 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                 LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                        ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                       AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                  WHERE A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "        IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  4 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0)  DATASIZE , A.COMENT, @IDI IDIOMA, A.ID ADJUN, NULL CAMPO,  NULL RUTA,1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                       LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                              ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                             AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                  WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                      ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31800_05_07_04A31800_05_07_05()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
        
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_005

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_04A31800_05_07_05 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_04A31800_05_07_05 = False
End Function


Private Sub V_31800_5_Storeds_005()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_UPLOAD_ADJUN_GS @CIA INT, @CIAPROVE INT,  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOBRA TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @SOBRA=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UPLOAD_ADJUN @INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @PROVE=@PROVE, @SOBRA=@SOBRA '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL, @PROVE VARCHAR(50)=NULL, @SOBRA TINYINT ',@INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @PROVE = @PROVEGS, @SOBRA=@SOBRA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_05A31800_05_07_06()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
        
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Datos_006

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_05A31800_05_07_06 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_05A31800_05_07_06 = False
End Function



Private Sub V_31800_5_Datos_006()



Dim sConsulta As String

sConsulta = "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_OLD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHA DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM NVARCHAR(300)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT NVARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CERTIFICADO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOCONFORMIDAD INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FEC_RESPUESTA DATETIME" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curCOPIA_CAMPO CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT FSGS_SRV + '.' + FSGS_BD + '.dbo.'  AS FSGS, REL_CIAS.COD_PROVE_CIA AS PROVEGS, CC.INSTANCIA, CC.CIA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN REL_CIAS WITH (NOLOCK) ON CC.CIA = REL_CIAS.CIA_PROVE " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CIAS WITH (NOLOCK) ON CIAS.ID = REL_CIAS.CIA_COMP AND CIAS.FCEST = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curCOPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curCOPIA_CAMPO INTO @FSGS, @PROVEGS, @INSTANCIA, @CIA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @CERTIFICADO = 0" & vbCrLf
sConsulta = sConsulta & "   SET @NOCONFORMIDAD = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT @CERTIFICADO= MAX (ID) FROM ' + @FSGS + 'CERTIFICADO WHERE INSTANCIA = @INSTANCIA AND PROVE = @PROVE AND INSTANCIA NOT IN (SELECT INSTANCIA FROM ' + @FSGS + 'VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA AND TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "           SELECT @NOCONFORMIDAD= MAX (ID) FROM ' + @FSGS + 'NOCONFORMIDAD WHERE INSTANCIA = @INSTANCIA AND PROVE = @PROVE AND INSTANCIA NOT IN (SELECT INSTANCIA FROM ' + @FSGS + 'VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA AND TIPO = 3)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT = NULL OUTPUT, @NOCONFORMIDAD INT = NULL OUTPUT, @INSTANCIA INT, @PROVE VARCHAR(50)', @CERTIFICADO=@CERTIFICADO OUTPUT, @NOCONFORMIDAD=@NOCONFORMIDAD OUTPUT, @INSTANCIA=@INSTANCIA, @PROVE=@PROVEGS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --MIRAMOS SI ES UN CERTIFICADO O UNA NOCONFORMIDAD" & vbCrLf
sConsulta = sConsulta & "   IF @CERTIFICADO >0 OR @NOCONFORMIDAD > 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       /*" & vbCrLf
sConsulta = sConsulta & "       PRINT 'CERTIFICADO = ' + CONVERT (NVARCHAR,@CERTIFICADO)" & vbCrLf
sConsulta = sConsulta & "       PRINT 'NOCONFORMIDAD = ' + CONVERT (NVARCHAR,@NOCONFORMIDAD)" & vbCrLf
sConsulta = sConsulta & "       PRINT 'CIA: ' + CONVERT(NVARCHAR,@CIA)" & vbCrLf
sConsulta = sConsulta & "       PRINT 'INSTANCIA: ' + CONVERT(NVARCHAR,@INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "       */" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'INSERT INTO ' + @FSGS + 'P_COPIA_CAMPO (PROVE, ID, INSTANCIA, VALOR_TEXT, VALOR_NUM,  VALOR_FEC, VALOR_BOOL )" & vbCrLf
sConsulta = sConsulta & "           SELECT @PROVEGS, ID , INSTANCIA, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "           WHERE INSTANCIA = @INSTANCIA AND CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@PROVEGS VARCHAR(50), @INSTANCIA INT, @CIA INT', @PROVEGS=@PROVEGS, @INSTANCIA=@INSTANCIA,@CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'INSERT INTO ' + @FSGS + 'P_COPIA_LINEA_DESGLOSE (PROVE, LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, LINEA_OLD)" & vbCrLf
sConsulta = sConsulta & "           SELECT @PROVEGS, CLD.LINEA, CLD.CAMPO_PADRE, CLD.CAMPO_HIJO, CLD.VALOR_NUM, CLD.VALOR_TEXT, CLD.VALOR_FEC, CLD.VALOR_BOOL, CLD.LINEA_OLD " & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_LINEA_DESGLOSE CLD " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO C ON CLD.CAMPO_HIJO = C.ID AND CLD.CIA = C.CIA " & vbCrLf
sConsulta = sConsulta & "           WHERE C.INSTANCIA = @INSTANCIA  AND C.CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@PROVEGS VARCHAR(50), @INSTANCIA INT, @CIA INT', @PROVEGS=@PROVEGS, @INSTANCIA=@INSTANCIA,@CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'INSERT INTO ' + @FSGS + 'P_COPIA_CAMPO_ADJUN (PROVE, ID, ID_GS, GRUPO, ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA, ADJUN_PORTAL)" & vbCrLf
sConsulta = sConsulta & "           SELECT @PROVEGS, CA.ID, CA.ID_GS, CA.GRUPO, CA.ADJUN, CA.CAMPO , CA.NOM , CA.IDIOMA , CA.DATASIZE, CA.PER , CA.COMENT , CA.FECALTA , CA.RUTA , CA.ADJUN_PORTAL" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO_ADJUN CA " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO C ON CA.CAMPO = C.ID AND CA.CIA = C.CIA " & vbCrLf
sConsulta = sConsulta & "           WHERE C.INSTANCIA = @INSTANCIA  AND C.CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@PROVEGS VARCHAR(50), @INSTANCIA INT, @CIA INT', @PROVEGS=@PROVEGS, @INSTANCIA=@INSTANCIA,@CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'INSERT INTO ' + @FSGS + 'P_COPIA_LINEA_DESGLOSE_ADJUN (PROVE,ID, ID_GS, LINEA, CAMPO_PADRE, CAMPO_HIJO, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA, ADJUN_PORTAL)" & vbCrLf
sConsulta = sConsulta & "           SELECT @PROVEGS, CLD.ID, CLD.ID_GS, CLD.LINEA, CLD.CAMPO_PADRE, CLD.CAMPO_HIJO, CLD.ADJUN, CLD.NOM, CLD.IDIOMA, CLD.DATASIZE, CLD.PER, CLD.COMENT, CLD.FECALTA, CLD.RUTA, CLD.ADJUN_PORTAL" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_LINEA_DESGLOSE_ADJUN CLD " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO C ON CLD.CAMPO_HIJO = C.ID  AND CLD.CIA = C.CIA " & vbCrLf
sConsulta = sConsulta & "           WHERE C.INSTANCIA = @INSTANCIA  AND C.CIA = @CIA'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@PROVEGS VARCHAR(50), @INSTANCIA INT, @CIA INT', @PROVEGS=@PROVEGS, @INSTANCIA=@INSTANCIA,@CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @NOCONFORMIDAD >0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'INSERT INTO ' + @FSGS + 'P_NOCONF_ACC (PROVE, CAMPO_PADRE, LINEA, NOCONFORMIDAD, INSTANCIA, VERSION, ESTADO, ESTADO_INT, COMENT)" & vbCrLf
sConsulta = sConsulta & "               SELECT @PROVEGS, NA.CAMPO_PADRE, NA.LINEA, NA.NOCONFORMIDAD, NA.INSTANCIA, NA.VERSION, NA.ESTADO, NA.ESTADO_INT, NA.COMENT" & vbCrLf
sConsulta = sConsulta & "               FROM NOCONF_ACC NA " & vbCrLf
sConsulta = sConsulta & "               WHERE NA.CIA = @CIA AND NA.INSTANCIA = @INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVEGS VARCHAR(50), @INSTANCIA INT, @CIA INT', @PROVEGS=@PROVEGS, @INSTANCIA=@INSTANCIA,@CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @CERTIFICADO >0" & vbCrLf
sConsulta = sConsulta & "       BEGIN " & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'SELECT @FEC_RESPUESTA = FEC_RESPUESTA FROM ' + @FSGS + 'CERTIFICADO C WHERE C.ID = @CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT, @FEC_RESPUESTA DATETIME OUTPUT', @CERTIFICADO=@CERTIFICADO, @FEC_RESPUESTA=@FEC_RESPUESTA OUTPUT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'EXEC ' + @FSGS + 'FSQA_ENVIAR_CERTIFICADO @CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION, @PROVE =@PROVE, @USUNOM=@USUNOM '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT, @INSTANCIA INT, @VERSION INT, @PROVE varchar(50), @USUNOM NVARCHAR(500)', @CERTIFICADO=@CERTIFICADO ,@INSTANCIA =@INSTANCIA ,@VERSION =NULL, @PROVE =@PROVEGS, @USUNOM=NULL" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           --COMO HEMOS SIMULADO UN ENV�O, DEBEMOS CORREGIR CIERTAS COSAS QUE ENUMERAMOS AHORA:" & vbCrLf
sConsulta = sConsulta & "           --1. Actualizamos en VERSION_INSTANCIA el tipo a 3 (guardado sin enviar)" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'UPDATE ' + @FSGS + 'VERSION_INSTANCIA SET TIPO = 3 " & vbCrLf
sConsulta = sConsulta & "                   FROM ' + @FSGS + 'VERSION_INSTANCIA VI" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN ' + @FSGS + 'INSTANCIA I ON I.ID = VI.INSTANCIA AND I.ULT_VERSION = VI.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "                   WHERE VI.INSTANCIA = @INSTANCIA AND VI.PROVE = @PROVE AND VI.CERTIFICADO = @CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @CERTIFICADO INT, @INSTANCIA INT', @PROVE =@PROVEGS ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           --2. Actualizamos en CERTIFICADO la versi�n, ya que s�lo tiene que contar como versiones los env�os, no lo guardados sin enviar" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'UPDATE ' + @FSGS + 'CERTIFICADO SET NUM_VERSION = C.NUM_VERSION - 1, FEC_RESPUESTA = @FEC_RESPUESTA" & vbCrLf
sConsulta = sConsulta & "                   FROM ' + @FSGS + 'CERTIFICADO C WHERE C.ID = @CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @CERTIFICADO INT, @INSTANCIA INT, @FEC_RESPUESTA DATETIME', @PROVE =@PROVEGS ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA, @FEC_RESPUESTA = @FEC_RESPUESTA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           --y 3. Eliminamos el registro que nos ha creado en VERSION_CERTIF" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'DELETE FROM ' + @FSGS + 'VERSION_CERTIF " & vbCrLf
sConsulta = sConsulta & "                   FROM ' + @FSGS + 'VERSION_CERTIF VC" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN ' + @FSGS + 'INSTANCIA I ON I.ID = VC.INSTANCIA AND I.ULT_VERSION = VC.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "                   WHERE VC.INSTANCIA = @INSTANCIA AND VC.CERTIFICADO = @CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @CERTIFICADO INT, @INSTANCIA INT', @PROVE =@PROVEGS ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           /*" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'SELECT * FROM ' + @FSGS + 'VERSION_INSTANCIA VI WHERE VI.INSTANCIA = @INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @CERTIFICADO INT, @INSTANCIA INT', @PROVE =@PROVEGS ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'SELECT * FROM ' + @FSGS + 'CERTIFICADO WHERE ID = @CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @CERTIFICADO INT, @INSTANCIA INT', @PROVE =@PROVEGS ,@CERTIFICADO =@CERTIFICADO ,@INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           */      " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @NOCONFORMIDAD >0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'SELECT @FEC_RESPUESTA = FEC_RESPUESTA FROM ' + @FSGS + 'NOCONFORMIDAD NC WHERE NC.ID = @NOCONFORMIDAD '    " & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@NOCONFORMIDAD INT, @FEC_RESPUESTA DATETIME OUTPUT', @NOCONFORMIDAD =@NOCONFORMIDAD, @FEC_RESPUESTA=@FEC_RESPUESTA OUTPUT" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'EXEC ' + @FSGS + 'FSQA_ENVIAR_NOCONFORMIDAD @NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA ,@VERSION =@VERSION, @PROVE =@PROVE, @USUNOM=@USUNOM '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@NOCONFORMIDAD INT, @INSTANCIA INT, @VERSION INT, @PROVE varchar(50), @USUNOM NVARCHAR(500)', @NOCONFORMIDAD=@NOCONFORMIDAD, @INSTANCIA =@INSTANCIA ,@VERSION =NULL, @PROVE =@PROVEGS, @USUNOM=NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'UPDATE ' + @FSGS + 'VERSION_INSTANCIA SET TIPO = 3 " & vbCrLf
sConsulta = sConsulta & "                   FROM ' + @FSGS + 'VERSION_INSTANCIA VI" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN ' + @FSGS + 'INSTANCIA I ON I.ID = VI.INSTANCIA AND I.ULT_VERSION = VI.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "                   WHERE VI.INSTANCIA = @INSTANCIA AND VI.PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @NOCONFORMIDAD INT, @INSTANCIA INT', @PROVE =@PROVEGS ,@NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'UPDATE ' + @FSGS + 'NOCONFORMIDAD SET VERSION = NC.VERSION - 1 , FEC_RESPUESTA = @FEC_RESPUESTA" & vbCrLf
sConsulta = sConsulta & "               FROM ' + @FSGS + 'NOCONFORMIDAD NC WHERE NC.ID = @NOCONFORMIDAD '                   " & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @NOCONFORMIDAD INT, @INSTANCIA INT, @FEC_RESPUESTA DATETIME', @PROVE =@PROVEGS ,@NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA, @FEC_RESPUESTA=@FEC_RESPUESTA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           /*" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'SELECT * FROM ' + @FSGS + 'VERSION_INSTANCIA WHERE INSTANCIA = @INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @NOCONFORMIDAD INT, @INSTANCIA INT', @PROVE =@PROVEGS ,@NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'SELECT * FROM ' + @FSGS + 'NOCONFORMIDAD WHERE ID = @NOCONFORMIDAD '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @NOCONFORMIDAD INT, @INSTANCIA INT', @PROVE =@PROVEGS ,@NOCONFORMIDAD =@NOCONFORMIDAD ,@INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           */" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'UPDATE ' + @FSGS + 'COPIA_ADJUN SET PORTAL = 0, ADJUN_PORTAL = NULL, PER= NULL, PROVE = @PROVE, DATA = CA_PORTAL.DATA" & vbCrLf
sConsulta = sConsulta & "       FROM ' + @FSGS + 'COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'COPIA_CAMPO_ADJUN CCA ON CCA.ADJUN = CA.ID  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'COPIA_CAMPO CC ON CC.ID = CCA.CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'INSTANCIA I ON I.ID = CC.INSTANCIA AND I.ULT_VERSION = CC.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_ADJUN CA_PORTAL ON CA.ADJUN_PORTAL = CA_PORTAL.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE CA.PORTAL = 1 and CA.ADJUN_PORTAL IS NOT NULL AND CC.INSTANCIA = @INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @INSTANCIA INT', @PROVE =@PROVEGS, @INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       /*IF @@ROWCOUNT > 0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'SELECT CA.* " & vbCrLf
sConsulta = sConsulta & "           FROM ' + @FSGS + 'COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'COPIA_CAMPO_ADJUN CCA ON CCA.ADJUN = CA.ID  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'COPIA_CAMPO CC ON CC.ID = CCA.CAMPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'INSTANCIA I ON I.ID = CC.INSTANCIA AND I.ULT_VERSION = CC.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "           WHERE CC.INSTANCIA = @INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @INSTANCIA INT', @PROVE =@PROVEGS, @INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       END*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'UPDATE ' + @FSGS + 'COPIA_ADJUN SET PORTAL = 0, ADJUN_PORTAL = NULL, PER= NULL, PROVE = @PROVE, DATA = CA_PORTAL.DATA" & vbCrLf
sConsulta = sConsulta & "       FROM ' + @FSGS + 'COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'COPIA_LINEA_DESGLOSE_ADJUN CCA ON CCA.ADJUN = CA.ID  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'COPIA_CAMPO CC ON CC.ID = CCA.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ' + @FSGS + 'INSTANCIA I ON I.ID = CC.INSTANCIA AND I.ULT_VERSION = CC.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_ADJUN CA_PORTAL ON CA.ADJUN_PORTAL = CA_PORTAL.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE CA.PORTAL = 1 and CA.ADJUN_PORTAL IS NOT NULL AND CC.INSTANCIA = @INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @INSTANCIA INT', @PROVE =@PROVEGS, @INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       /*IF @@ROWCOUNT > 0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'SELECT CA.* " & vbCrLf
sConsulta = sConsulta & "           FROM ' + @FSGS + 'COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'COPIA_LINEA_DESGLOSE_ADJUN CCA ON CCA.ADJUN = CA.ID  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'COPIA_CAMPO CC ON CC.ID = CCA.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'INSTANCIA I ON I.ID = CC.INSTANCIA AND I.ULT_VERSION = CC.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "           WHERE CC.INSTANCIA = @INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@PROVE varchar(50), @INSTANCIA INT', @PROVE =@PROVEGS, @INSTANCIA =@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       END*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "        FROM COPIA_CAMPO_ADJUN CCA" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                  ON CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "                 AND CCA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "       WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "         AND C.CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "         FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                   ON CLDA.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "                  AND CLDA.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "       WHERE C.INSTANCIA = @INSTANCIA " & vbCrLf
sConsulta = sConsulta & "         AND C.CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM COPIA_LINEA_DESGLOSE " & vbCrLf
sConsulta = sConsulta & "         FROM COPIA_LINEA_DESGLOSE CLD" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "                       ON CLD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "                      AND CLD.CIA = C.CIA" & vbCrLf
sConsulta = sConsulta & "        WHERE C.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "          AND C.CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       DELETE FROM COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "        WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "          AND CIA = @CIA   " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @NOCONFORMIDAD >0" & vbCrLf
sConsulta = sConsulta & "           DELETE FROM NOCONF_ACC" & vbCrLf
sConsulta = sConsulta & "            WHERE INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "              AND CIA = @CIA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curCOPIA_CAMPO INTO @FSGS, @PROVEGS, @INSTANCIA, @CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf

gRDOCon.QueryTimeout = 7200
gRDOCon.Execute sConsulta, rdExecDirect

End Sub


Public Function CodigoDeActualizacion31800_05_07_06A31800_05_07_07()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
        
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_007

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_06A31800_05_07_07 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_06A31800_05_07_07 = False
End Function


Private Sub V_31800_5_Storeds_007()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_UPLOAD_ADJUN_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_UPLOAD_ADJUN_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_UPLOAD_ADJUN_GS @CIA INT, @CIAPROVE INT,  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(20)=NULL, @USUNOM VARCHAR(500)=NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOBRA TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @SOBRA=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND RC.CIA_PROVE = @CIAPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_UPLOAD_ADJUN @INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @PROVE=@PROVE, @SOBRA=@SOBRA, @USUNOM=@USUNOM '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'  @INIT INT,@ID INT = NULL OUTPUT,@DATA IMAGE , @NOM NVARCHAR(300)=NULL, @COMENT NVARCHAR(500)=NULL, @PER VARCHAR(50)=NULL, @PROVE VARCHAR(50)=NULL, @SOBRA TINYINT, @USUNOM VARCHAR(500)=NULL ',@INIT=@INIT, @ID=@ID OUTPUT, @DATA=@DATA, @NOM = @NOM, @COMENT=@COMENT, @PER = @PER, @PROVE = @PROVEGS, @SOBRA=@SOBRA, @USUNOM=@USUNOM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUN @CIA INT, @TIPO INT, @ID INT, @PORTAL TINYINT  = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@TIPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "1: El adjunto est� guardado en la BD del gs en COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "2,4: El adjunto est� guardado en la BD del portal en COPIA_ADJUN (lo ha metido el proveedor y todav�a no ha sido trasferido al gs o todav�a no ha guardado la instancia)" & vbCrLf
sConsulta = sConsulta & "3: El adjunto est� guardado en la BD del gs en COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUN @TIPO =@TIPO, @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N' @TIPO INT, @ID INT', @TIPO =@TIPO, @ID=@ID" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @TIPO=1" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT  A.ID, A.FECALTA, A.PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, A.DATASIZE , A.COMENT, A.IDIOMA " & vbCrLf
sConsulta = sConsulta & "         FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                               LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                      ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                     AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                               ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ID = @ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO=3" & vbCrLf
sConsulta = sConsulta & "        SELECT  A.ID, A.FECALTA, A.PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, A.DATASIZE , A.COMENT, A.IDIOMA " & vbCrLf
sConsulta = sConsulta & "          FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                   LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                   ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                  AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                          ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "         WHERE A.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT  A.ID, A.FECALTA, U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM, DATALENGTH(DATA) DATASIZE , A.COMENT" & vbCrLf
sConsulta = sConsulta & "          FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                   ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                  AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "         WHERE A.ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNDATA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNDATA @CIA INT=NULL, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0, @PORTAL TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          SELECT @IDGS = ID_GS FROM COPIA_CAMPO_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "          IF @IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA INNER JOIN COPIA_CAMPO_ADJUN  CCA ON CA.ID = CCA.ADJUN_PORTAL WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "              if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "              ELSE " & vbCrLf
sConsulta = sConsulta & "                SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "              READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "              EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@IDGS, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        IF @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @IDGS = ID_GS FROM COPIA_LINEA_DESGLOSE_ADJUN WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "            IF @IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @CIA IS NULL" & vbCrLf
sConsulta = sConsulta & "                  SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN  CCA ON CA.ID = CCA.ADJUN_PORTAL WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "                if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "                  SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "                ELSE " & vbCrLf
sConsulta = sConsulta & "                  SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "                READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "                EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@IDGS, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM COPIA_ADJUN CA WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "            if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "              SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "            READTEXT COPIA_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_07A31800_05_07_08()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_008

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_07A31800_05_07_08 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_07A31800_05_07_08 = False
End Function



Private Sub V_31800_5_Storeds_008()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ID INT, @GRUPO VARCHAR(50) , @CANTMAX TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @CANTMAX = 0 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * (CASE WHEN ISNULL(IO.CANTMAX,0) <= ISNULL(IO.CANT,0) THEN ISNULL(IO.CANTMAX,ISNULL(IO.CANT,0)) ELSE ISNULL(IO.CANT,0) END))" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT OBSADJUN, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_08A31800_05_07_09()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_009

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_08A31800_05_07_09 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_08A31800_05_07_09 = False
End Function


Private Sub V_31800_5_Storeds_009()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ID INT, @GRUPO VARCHAR(50) , @CANTMAX TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @CANTMAX = 0 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * (CASE WHEN ISNULL(IO.CANTMAX,0) <= ISNULL(IO.CANT,0) AND IO.CANTMAX > 0  THEN ISNULL(IO.CANTMAX,ISNULL(IO.CANT,0)) ELSE ISNULL(IO.CANT,0) END))" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT OBSADJUN, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_09A31800_05_07_10()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_010

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_09A31800_05_07_10 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_09A31800_05_07_10 = False
End Function


Private Sub V_31800_5_Storeds_010()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_LOADCERTIFICADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_LOADCERTIFICADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_LOADCERTIFICADO @CIA INT,@PROVE VARCHAR(50),  @CERTIFICADO INT = NULL , @PORTAL TINYINT =NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "--Descripci�n: Llama a FSGS.FSQA_LOADCERTIFICADO para obtener la ultima versi�n del certificado y existe una versi�n de tipo 3, es decir una versi�n guardada en portal sin enviar devuelve esa." & vbCrLf
sConsulta = sConsulta & "--Parametros:" & vbCrLf
sConsulta = sConsulta & "-- @CERTIFICADO:= ID del certificado" & vbCrLf
sConsulta = sConsulta & "--  @PROVE:=COD del proveedor en portal" & vbCrLf
sConsulta = sConsulta & "-- @PORTAL:=par�metro de salida en el que se devuelve 1 si la versi�n es un guardado en portal sin enviar" & vbCrLf
sConsulta = sConsulta & "--Llamadas: .PMPortalDataBaseServer/Root.vb Certificado_Load          " & vbCrLf
sConsulta = sConsulta & "--Tiempo ejecucion:= 265 ms." & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADCERTIFICADO @CERTIFICADO =@CERTIFICADO,@PORTAL=@PORTAL OUTPUT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT =NULL,@PORTAL TINYINT = NULL OUTPUT',@CERTIFICADO =@CERTIFICADO, @PORTAL=@PORTAL OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_10A31800_05_07_11()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_011

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_10A31800_05_07_11 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_10A31800_05_07_11 = False
End Function


Private Sub V_31800_5_Storeds_011()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL , @PORTAL tinyint = 0, @QA TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "    'HHF 18/03/2009" & vbCrLf
sConsulta = sConsulta & "    '*** Descripci�n: Funci�n que devuelve los adjuntos requeridos por los par�metros" & vbCrLf
sConsulta = sConsulta & "    '*** Par�metros de entrada: " & vbCrLf
sConsulta = sConsulta & "    '***    @CIA: C�digo de la compa��a" & vbCrLf
sConsulta = sConsulta & "    '***    @TIPO: Tipo de adjuntos (1=Adjuntos de campo normal, 3=Adjuntos de campo de desglose)" & vbCrLf
sConsulta = sConsulta & "    '***    @IDS: String con los Ids de los adjuntos ya guardados en alguna versi�n anterior" & vbCrLf
sConsulta = sConsulta & "    '***    @IDSNEW: String con los Ids de los adjuntos que el usuario haya guardado en esta misma versi�n" & vbCrLf
sConsulta = sConsulta & "    '***    @IDI: Idioma del usuario" & vbCrLf
sConsulta = sConsulta & "    '***    @PORTAL: Booleana que nos indica si debemos recuperar los datos de la BD de Portal (1) o de GS (0)" & vbCrLf
sConsulta = sConsulta & "    '***    @QA: Booleana que nos indica si estamos en QA\Portal (1) o en el PM (0)" & vbCrLf
sConsulta = sConsulta & "    '*** Par�metros de salida: No tiene" & vbCrLf
sConsulta = sConsulta & "    '*** Devuelve: Un dataSet con los adjuntos solicitados" & vbCrLf
sConsulta = sConsulta & "    '*** Llamada desde: los eventos Load de attachedfiles.ascx.vb y desglose.ascx" & vbCrLf
sConsulta = sConsulta & "    '*** Tiempo m�ximo: 0 sec            " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @QA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF  @PORTAL = 0 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =null , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "       IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                  SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                    FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                             ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                            AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                   WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                       ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "       IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "         SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA, 0 PORTAL" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "          WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "          ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                      SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                        FROM COPIA_CAMPO_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU P " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                       WHERE  A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                      SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                        FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                 ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                                AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                       WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                           ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "     ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "         SELECT  3 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA, 1 PORTAL" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "          WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "          ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                    SELECT  3 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA,  1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                      FROM COPIA_LINEA_DESGLOSE_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA" & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                     WHERE A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                    SELECT  4 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0)  DATASIZE , A.COMENT, @IDI IDIOMA, A.ID ADJUN, NULL CAMPO,  NULL RUTA,1 PORTAL" & vbCrLf
sConsulta = sConsulta & "                      FROM COPIA_ADJUN A " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN USU U " & vbCrLf
sConsulta = sConsulta & "                                 ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                                AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                     WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                         ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31800_05_07_11A31800_05_07_12()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_012

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.12'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_11A31800_05_07_12 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_11A31800_05_07_12 = False
End Function


Private Sub V_31800_5_Storeds_012()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_CREATE_NEW_CERTIFICADO_PREV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_CREATE_NEW_CERTIFICADO_PREV]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_CREATE_NEW_CERTIFICADO_PREV @CIA INT, @TIPO_CERTIF INT, @PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "    'HHF 18/03/2009" & vbCrLf
sConsulta = sConsulta & "    '*** Descripci�n: Funci�n que crea la primera versi�n del certificado autom�tico" & vbCrLf
sConsulta = sConsulta & "    '*** Par�metros de entrada: " & vbCrLf
sConsulta = sConsulta & "    '***    @TIPO_CERTIF: Tipo de Certificado (o Solicitud)" & vbCrLf
sConsulta = sConsulta & "    '***    @PROVE: C�digo de proveedor de GS" & vbCrLf
sConsulta = sConsulta & "    '*** Par�metros de salida: No tiene" & vbCrLf
sConsulta = sConsulta & "    '*** Llamada desde: El procedimiento GuardarSinWorkflow de GuardarInstancia.aspx del PMPortalWeb" & vbCrLf
sConsulta = sConsulta & "    '*** Tiempo m�ximo: 0 sec            " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_CREATE_TEMP_NEW_CERTIFICADO_PREV @TIPO_CERTIF=@TIPO_CERTIF,@PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO_CERTIF INT, @PROVE VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & ",@TIPO_CERTIF=@TIPO_CERTIF,@PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_12A31800_05_07_13()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_013

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.13'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_12A31800_05_07_13 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_12A31800_05_07_13 = False
End Function


Private Sub V_31800_5_Storeds_013()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ID INT, @GRUPO VARCHAR(50) , @CANTMAX TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @CANTMAX = 0 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * (CASE WHEN ISNULL(IO.CANTMAX,0) <= ISNULL(IO.CANT,0) AND IO.CANTMAX > 0  THEN ISNULL(IO.CANTMAX,ISNULL(IO.CANT,0)) ELSE ISNULL(IO.CANT,0) END))" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT OBSADJUN, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_13A31800_05_07_14()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en tablas"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Tablas_014
            
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_014

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.14'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_13A31800_05_07_14 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_13A31800_05_07_14 = False
End Function



Private Sub V_31800_5_Storeds_014()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_UTC_SERVIDOR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_UTC_SERVIDOR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_UTC_SERVIDOR] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT  DATEDIFF(MINUTE,GETUTCDATE(),GETDATE()) AS DIFERENCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31800_5_Tablas_014()

Dim sConsulta As String

    sConsulta = "if not exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[PARGEN_INTERNO]') and name=N'MOSTRAR_POPUP_SUBASTA')" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_INTERNO] ADD [MOSTRAR_POPUP_SUBASTA]  [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_MOSTRAR_POPUP_SUBASTA] DEFAULT (0)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub



Public Function CodigoDeActualizacion31800_05_07_14A31800_05_07_15()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_015

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.15'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_14A31800_05_07_15 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_14A31800_05_07_15 = False
End Function



Private Sub V_31800_5_Storeds_015()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL , @PORTAL tinyint = 0, @QA TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @QA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF  @PORTAL = 0 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =null , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "       IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                  SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                    FROM COPIA_ADJUN A WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      LEFT JOIN USU U WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                            AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                   WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                       ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "       IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "         SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA, 0 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU P  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "          WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "          ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                      SELECT  1 TIPO, A.ID, A.FECALTA, P.COD PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO, A.RUTA,  1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                        FROM COPIA_CAMPO_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU P  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = P.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = P.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                       WHERE  A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                      SELECT  2 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM, ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA,  NULL ADJUN, NULL CAMPO, NULL RUTA,  1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                        FROM COPIA_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN USU U  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                                AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                       WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                           ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "     ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "         SELECT  3 TIPO, A.ID, A.FECALTA,U.COD PER, U.NOM + ' ' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA, 1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_LINEA_DESGLOSE_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU U  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "          WHERE A.ID=-1000" & vbCrLf
sConsulta = sConsulta & "          ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                    SELECT  3 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,ROUND(A.DATASIZE /1024,0) DATASIZE, A.COMENT, A.IDIOMA , A.ADJUN, A.CAMPO_HIJO CAMPO,  A.RUTA,  1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                      FROM COPIA_LINEA_DESGLOSE_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             LEFT JOIN (COPIA_ADJUN CA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    LEFT JOIN USU U  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           ON CA.CIA = U.CIA" & vbCrLf
sConsulta = sConsulta & "                                          AND CA.USU = U.ID)" & vbCrLf
sConsulta = sConsulta & "                                    ON A.ADJUN_PORTAL = CA.ID" & vbCrLf
sConsulta = sConsulta & "                     WHERE A.ID IN (' + @IDS + ')'" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N' UNION '" & vbCrLf
sConsulta = sConsulta & "           IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                    SELECT  4 TIPO, A.ID, A.FECALTA, U.COD PER, U.NOM + '' '' + U.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0)  DATASIZE , A.COMENT, @IDI IDIOMA, A.ID ADJUN, NULL CAMPO,  NULL RUTA,1 PORTAL, A.COMENT AS COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                      FROM COPIA_ADJUN A  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          LEFT JOIN USU U  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON U.CIA = A.CIA" & vbCrLf
sConsulta = sConsulta & "                                AND U.ID = A.USU" & vbCrLf
sConsulta = sConsulta & "                     WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                         ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_05_07_15A31800_05_07_16()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
            
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_5_Storeds_016

   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.07.16'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_05_07_15A31800_05_07_16 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_05_07_15A31800_05_07_16 = False
End Function



Private Sub V_31800_5_Storeds_016()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_CONTADOR_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_CONTADOR_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_REL_CIAS_NEW_CERTIF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_REL_CIAS_NEW_CERTIF]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_ACT_REL_CIAS_NEW_NOCONFORMIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub
