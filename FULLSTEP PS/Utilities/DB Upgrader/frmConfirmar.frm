VERSION 5.00
Begin VB.Form frmConfirmar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FSPS Actualizador"
   ClientHeight    =   2325
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4905
   Icon            =   "frmConfirmar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2325
   ScaleWidth      =   4905
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   1755
      Left            =   120
      TabIndex        =   2
      Top             =   60
      Width           =   4635
      Begin VB.Label Label1 
         Caption         =   "Versión actual:"
         Height          =   255
         Left            =   180
         TabIndex        =   8
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Ultima actualización:"
         Height          =   255
         Left            =   180
         TabIndex        =   7
         Top             =   780
         Width           =   1815
      End
      Begin VB.Label lblVersionAct 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2040
         TabIndex        =   6
         Top             =   300
         Width           =   2175
      End
      Begin VB.Label lblFecAct 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2040
         TabIndex        =   5
         Top             =   720
         Width           =   2175
      End
      Begin VB.Label Label3 
         Caption         =   "Actualizar a versión::"
         Height          =   255
         Left            =   180
         TabIndex        =   4
         Top             =   1200
         Width           =   1755
      End
      Begin VB.Label lblVersionFinal 
         BackColor       =   &H00C0E0FF&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   2040
         TabIndex        =   3
         Top             =   1140
         Width           =   2175
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2400
      TabIndex        =   1
      Top             =   1920
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   840
      TabIndex        =   0
      Top             =   1920
      Width           =   1215
   End
End
Attribute VB_Name = "frmConfirmar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdAceptar_Click()
    Dim lRet As Long
        
    Me.Hide
    
    If basPublic.Actualizar(lblVersionAct, lblVersionFinal) Then
        MsgBox "La actualización ha finalizado correctamente. Seguidamente se actualizarán los textos del Portal en los diferentes idiomas.", vbInformation + vbOKOnly
        
        frmProgreso.Show
    
        frmSeleccionarIdiomas.Show 1
        
        If Not basPublic.InsertWEBTEXT Then
            lRet = 2
            MsgBox "Se han producido errores al insertar los textos.", vbInformation + vbOKOnly
        Else
            'If Not basPublic.Insert_SysMessages Then
            '    lRet = 3
            '    MsgBox "Ha ocurrido un error al insertar los mensajes de base de datos personalizados.", vbCritical + vbOKOnly
            'Else
                If basPublic.CrearJobs Then
                    lRet = 0
                    'MsgBox "La actualización de tareas ha finalizado correctamente.", vbInformation + vbOKOnly
                    MsgBox "La actualización ha finalizado correctamente.", vbInformation + vbOKOnly
                Else
                    lRet = 4
                    MsgBox "Se han producido errores en la actualización de tareas. No se han creado los jobs.", vbInformation + vbOKOnly
                End If
            'End If
        End If
    Else
        lRet = 1
        MsgBox "Se han producido errores en la actualización", vbCritical
    End If
    
    ExitProcess lRet
    
    End
End Sub

Private Sub cmdCancelar_Click()
    
    End
    
End Sub

Private Sub Form_Load()
Dim rdores As rdoResultset

On Error GoTo error

    If gRDOCon Is Nothing Then
        MsgBox "No se ha establecido la conexión correctamente" & vbLf & "Vuelva a lanzar el programa", vbCritical + vbOKOnly
        End
    End If

    'Recuperar versión BD y fecha actualización
    
    sConsulta = "SELECT * FROM VERSION"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    
    If rdores.EOF Then
        MsgBox "Imposible encontrar la versión de BD", vbCritical
        rdores.Close
        End
    Else
        basPublic.psVersionBD = rdores("NUM").Value
        If rdores("FECHA").Value <> "" Then
            basPublic.gdatFecha = CDate(rdores("FECHA").Value)
        End If
    End If
    
    rdores.Close
    Set rdores = Nothing
    
    lblFecAct = basPublic.gdatFecha
    lblVersionAct = basPublic.psVersionBD
    lblVersionFinal = basPublic.gcUltimaVersion
    
    Exit Sub
error:
    
    If rdoErrors(rdoErrors.Count - 1).Description = "S0002: [Microsoft][ODBC SQL Server Driver][SQL Server]Invalid object name 'VERSION'." Then
        'Nos encontramos ante la primera actualización.
        'En la que no existía la tabla versión
        basPublic.psVersionBD = "2.0.0"
        lblFecAct = basPublic.gdatFecha
        lblVersionAct = basPublic.psVersionBD
        lblVersionFinal = basPublic.gcUltimaVersion
        Exit Sub
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical
    
End Sub
