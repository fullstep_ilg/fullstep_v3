Attribute VB_Name = "bas_V_31900_2"
Option Explicit

Public Function CodigoDeActualizacion31800_09_11_05A31900_02_12_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_2_Tablas_000
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_2_Storeds_000
    
    frmProgreso.lblDetalle = "Cambios en datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_2_Datos_000
               
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.12.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31800_09_11_05A31900_02_12_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31800_09_11_05A31900_02_12_00 = False
End Function

Private Sub V_31900_2_Tablas_000()
    Dim sConsulta As String
    

    sConsulta = "ALTER TABLE dbo.PROCE_OFE ADD" & vbCrLf
    sConsulta = sConsulta & "   IMPORTE float(53) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_OFE ADD" & vbCrLf
    sConsulta = sConsulta & "   IMPORTE_BRUTO float(53) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_OFE SET (LOCK_ESCALATION = TABLE)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.GRUPO_OFE ADD" & vbCrLf
    sConsulta = sConsulta & "   IMPORTE float(53) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.GRUPO_OFE ADD" & vbCrLf
    sConsulta = sConsulta & "   IMPORTE_BRUTO float(53) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.GRUPO_OFE SET (LOCK_ESCALATION = TABLE)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.ITEM_OFE ADD" & vbCrLf
    sConsulta = sConsulta & "   IMPORTE float(53) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.ITEM_OFE ADD" & vbCrLf
    sConsulta = sConsulta & "   PREC_VALIDO float(53) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.ITEM_OFE SET (LOCK_ESCALATION = TABLE)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE dbo.OFE_ATRIB ADD" & vbCrLf
    sConsulta = sConsulta & "   IMPPARCIAL float(53) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.OFE_ATRIB SET (LOCK_ESCALATION = TABLE)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE dbo.OFE_GR_ATRIB ADD" & vbCrLf
    sConsulta = sConsulta & "   IMPPARCIAL float(53) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "ALTER TABLE dbo.OFE_GR_ATRIB SET (LOCK_ESCALATION = TABLE)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "CREATE TABLE [dbo].[OFE_GR_COSTESDESC](" & vbCrLf
    sConsulta = sConsulta & "   [ID] [int] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "   [GRUPO] [varchar](50) NOT NULL," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB_ID] [int] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "   [IMPPARCIAL] [float] NULL," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
    sConsulta = sConsulta & " CONSTRAINT [PK_OFE_GR_COSTESDESC] PRIMARY KEY CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "(" & vbCrLf
    sConsulta = sConsulta & "   [ID] ASC," & vbCrLf
    sConsulta = sConsulta & "   [GRUPO] ASC," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB_ID] ASC" & vbCrLf
    sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "CREATE TABLE [dbo].[PROCE_COSTESDESC](" & vbCrLf
    sConsulta = sConsulta & "   [ID] [int] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "   [ATRIB_ID] [int] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "   [AMBITO] [smallint] NOT NULL," & vbCrLf
    sConsulta = sConsulta & "   [APLICAR] [smallint] NULL," & vbCrLf
    sConsulta = sConsulta & "   [OPERACION] [varchar](2) NULL," & vbCrLf
    sConsulta = sConsulta & "   [ORDEN] [smallint] NULL," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
    sConsulta = sConsulta & "   [GRUPO] [varchar](50) NULL," & vbCrLf
    sConsulta = sConsulta & " CONSTRAINT [PK_PROCE_COSTESDESC] PRIMARY KEY CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "(" & vbCrLf
    sConsulta = sConsulta & "   [ID] ASC" & vbCrLf
    sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_31900_2_Storeds_000()
    Dim sConsulta As String
    
    'SP_DEVOLVER_GRUPO
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_GRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_GRUPO] @ID INT, @GRUPO VARCHAR(50)  AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT OBSADJUN,  IMPORTE, IMPORTE_BRUTO IMPORTEBRUTO" & vbCrLf
    sConsulta = sConsulta & "  FROM GRUPO_OFE  WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_DEVOLVER_GRUPO_DATOS_OFERTA
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_GRUPO_DATOS_OFERTA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_GRUPO_DATOS_OFERTA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_GRUPO_DATOS_OFERTA] @ID INT, @GRUPO VARCHAR(50) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT IMPORTE, IMPORTE_BRUTO IMPORTEBRUTO" & vbCrLf
    sConsulta = sConsulta & "  FROM GRUPO_OFE  WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "   AND GRUPO = @GRUPO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_IMPORTES_PARCIALES_GRUPO] @ID INT, @COSTEDESC int = null AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT GRUPO, ATRIB_ID COSTEDESC, IMPPARCIAL" & vbCrLf
    sConsulta = sConsulta & " FROM OFE_GR_COSTESDESC WITH (NOLOCK)        " & vbCrLf
    sConsulta = sConsulta & "WHERE ID=@ID" & vbCrLf
    sConsulta = sConsulta & "  AND ATRIB_ID = CASE WHEN @COSTEDESC IS NULL THEN ATRIB_ID ELSE @COSTEDESC END" & vbCrLf
    sConsulta = sConsulta & "ORDER BY GRUPO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_DEVOLVER_OFERTA_PROVE
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_OFERTA_PROVE] @CIA_COMP INT = NULL, @ANYO INT = NULL, @GMN1 VARCHAR(20) = NULL, @PROCE INT = NULL, @PROVE VARCHAR(50) = NULL, @ID INT = NULL AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @ID IS NULL" & vbCrLf
    sConsulta = sConsulta & "SELECT @ID = ID " & vbCrLf
    sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
    sConsulta = sConsulta & " WHERE CIA_COMP = @CIA_COMP" & vbCrLf
    sConsulta = sConsulta & "   AND ANYO = @ANYO" & vbCrLf
    sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
    sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
    sConsulta = sConsulta & "   AND PROVE = @PROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @TOTALADJUNTOS = ISNULL(SUM(DATASIZE),0) " & vbCrLf
    sConsulta = sConsulta & "  FROM" & vbCrLf
    sConsulta = sConsulta & "    (SELECT ISNULL(SUM(DATASIZE), 0) DATASIZE" & vbCrLf
    sConsulta = sConsulta & "       FROM PROCE_OFE_ADJUN" & vbCrLf
    sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "     UNION" & vbCrLf
    sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
    sConsulta = sConsulta & "       FROM OFE_GR_ADJUN" & vbCrLf
    sConsulta = sConsulta & "      WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "     UNION" & vbCrLf
    sConsulta = sConsulta & "     SELECT ISNULL(SUM(DATASIZE), 0)" & vbCrLf
    sConsulta = sConsulta & "       FROM OFE_ITEM_ADJUN" & vbCrLf
    sConsulta = sConsulta & "      WHERE ID = @ID) A" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--DECLARE @IMPORTEOFERTA float" & vbCrLf
    sConsulta = sConsulta & "--SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(IO.CANT,0)) " & vbCrLf
    sConsulta = sConsulta & "--  FROM ITEM_OFE IO" & vbCrLf
    sConsulta = sConsulta & "-- WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT PO.ID, PO.CIA_COMP, PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.CIA_PROVE, PO.USU, PO.FECREC, PO.FECVAL, PO.MON,PO.CAMBIO, PO.OBS, PO.OBSADJUN, PO.NUMOBJ, PO.ADJUN,  @TOTALADJUNTOS TOTALADJUNTOS, PO.IMPORTE IMPORTEOFERTA, U.NOM + ' ' + U.APE NOMUSU, PO.ENVIANDOSE, PO.IMPORTE_BRUTO IMPORTEBRUTO" & vbCrLf
    sConsulta = sConsulta & "  FROM PROCE_OFE PO" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN USU U " & vbCrLf
    sConsulta = sConsulta & "            ON U.CIA = PO.CIA_PROVE" & vbCrLf
    sConsulta = sConsulta & "           AND U.COD = PO.USU" & vbCrLf
    sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_ELIMINAR_OFERTA_PROVE
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_OFERTA_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_OFERTA_PROVE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_OFERTA_PROVE] @ID INT AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM ofe_gr_costesdesc WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ID = @ID" & vbCrLf
    sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ID = @ID" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_ITEMS
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_ITEMS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ITEMS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE  [dbo].[SP_ITEMS]  @ID INT,@GRUPO VARCHAR(50)=null AS " & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
    sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
    sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
    sConsulta = sConsulta & "            WHERE ID=@ID)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT ITO.ITEM, PRECIO, CANTMAX, PRECIO2, PRECIO3,IA.NUMADJUN, COMENT1, COMENT2, COMENT3, cast(OBSADJUN as varchar(2000)) OBSADJUN, @CAM CAMBIO,ITO.PREC_VALIDO,ITO.IMPORTE" & vbCrLf
    sConsulta = sConsulta & "  FROM ITEM_OFE ITO" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN (SELECT IA.ITEM, COUNT(IA.ADJUN) NUMADJUN" & vbCrLf
    sConsulta = sConsulta & "                 FROM OFE_ITEM_ADJUN IA" & vbCrLf
    sConsulta = sConsulta & "                WHERE IA.ID =@ID" & vbCrLf
    sConsulta = sConsulta & "             GROUP BY IA.ID, IA.ITEM) IA" & vbCrLf
    sConsulta = sConsulta & "      ON ITO.ITEM = IA.ITEM " & vbCrLf
    sConsulta = sConsulta & "WHERE ITO.ID = @ID" & vbCrLf
    sConsulta = sConsulta & "  AND ITO.GRUPO=case when @GRUPO is null then ITO.GRUPO else @GRUPO END" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_DEVOLVER_NUM_OFERTAS_PROCE
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_NUM_OFERTAS_PROCE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_NUM_OFERTAS_PROCE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_NUM_OFERTAS_PROCE] (@ANYO int, @GMN1 varchar(50),@PROCE int) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_ELIMINAR_VALOR_ATRIB_OFE
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_VALOR_ATRIB_OFE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_VALOR_ATRIB_OFE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_VALOR_ATRIB_OFE] (@AMBITOANTIGUO INTEGER, @ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @IDATRIB INTEGER, @GRUPO VARCHAR(50)='') AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @ID=ID FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT @ID IS NULL" & vbCrLf
    sConsulta = sConsulta & "BEGIN " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "      IF @AMBITOANTIGUO=1 -- Ambito proceso" & vbCrLf
    sConsulta = sConsulta & "            DELETE OFE_ATRIB WHERE ID = @ID AND ATRIB_ID = @IDATRIB " & vbCrLf
    sConsulta = sConsulta & "            " & vbCrLf
    sConsulta = sConsulta & "      IF @AMBITOANTIGUO=2 -- Ambito grupo" & vbCrLf
    sConsulta = sConsulta & "            DELETE OFE_GR_ATRIB WHERE ID = @ID AND ATRIB_ID = @IDATRIB " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "      IF @AMBITOANTIGUO=3 -- Ambito item" & vbCrLf
    sConsulta = sConsulta & "            IF @GRUPO='' -- Para los items de todos los grupos del proceso." & vbCrLf
    sConsulta = sConsulta & "                  DELETE OFE_ITEM_ATRIB WHERE ID = @ID AND ATRIB_ID = @IDATRIB " & vbCrLf
    sConsulta = sConsulta & "            ELSE -- Para los items de un grupo determinado del proceso." & vbCrLf
    sConsulta = sConsulta & "               DELETE OFE_ITEM_ATRIB WHERE ID = @ID AND ATRIB_ID = @IDATRIB AND GRUPO=@GRUPO" & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "-- Tambi�n se elimina de OFE_GR_COSTESDESC" & vbCrLf
    sConsulta = sConsulta & "      IF @GRUPO='' -- Se elimina ese atributo para todos los grupos." & vbCrLf
    sConsulta = sConsulta & "            DELETE OFE_GR_COSTESDESC WHERE ID = @ID AND ATRIB_ID = @IDATRIB " & vbCrLf
    sConsulta = sConsulta & "      ELSE -- Se elimina ese atributo para grupo concreto." & vbCrLf
    sConsulta = sConsulta & "            DELETE OFE_GR_COSTESDESC WHERE ID = @ID AND ATRIB_ID = @IDATRIB AND GRUPO=@GRUPO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_CAMBIAR_AMBITO_ATRIB_OFE
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_CAMBIAR_AMBITO_ATRIB_OFE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_CAMBIAR_AMBITO_ATRIB_OFE]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_CAMBIAR_AMBITO_ATRIB_OFE] (@AMBITOANTIGUO INTEGER, @AMBITONUEVO INTEGER, @ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @IDATRIB INTEGER, @GRUPO VARCHAR(50)='', @ITEM INTEGER=0 ) AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ATRIB_ID AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @VALOR_NUM AS FLOAT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @VALOR_TEXT AS VARCHAR(800)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @VALOR_FEC AS DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @VALOR_BOOL AS TINYINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @GR AS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IT AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-- Cogemos el registro del atributo a eliminar del �mbito antiguo." & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @ID=ID FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @AMBITOANTIGUO=1 -- Ambito proceso    " & vbCrLf
    sConsulta = sConsulta & "      SELECT @VALOR_NUM=VALOR_NUM, @VALOR_TEXT=VALOR_TEXT, @VALOR_FEC=VALOR_FEC, @VALOR_BOOL=VALOR_BOOL FROM OFE_ATRIB WITH (NOLOCK) WHERE ID=@ID AND ATRIB_ID=@IDATRIB" & vbCrLf
    sConsulta = sConsulta & "ELSE IF @AMBITOANTIGUO=2 -- Ambito grupo" & vbCrLf
    sConsulta = sConsulta & "      IF @GRUPO=''  -- Para los items de todos los grupos del proceso" & vbCrLf
    sConsulta = sConsulta & "            SELECT @VALOR_NUM=VALOR_NUM, @VALOR_TEXT=VALOR_TEXT, @VALOR_FEC=VALOR_FEC, @VALOR_BOOL=VALOR_BOOL FROM OFE_GR_ATRIB WITH (NOLOCK) WHERE ID=@ID AND ATRIB_ID=@IDATRIB" & vbCrLf
    sConsulta = sConsulta & "      ELSE  -- Para los items de un grupo concreto del proceso" & vbCrLf
    sConsulta = sConsulta & "            SELECT @VALOR_NUM=VALOR_NUM, @VALOR_TEXT=VALOR_TEXT, @VALOR_FEC=VALOR_FEC, @VALOR_BOOL=VALOR_BOOL FROM OFE_GR_ATRIB WITH (NOLOCK) WHERE ID=@ID AND GRUPO=@GRUPO AND ATRIB_ID=@IDATRIB                              " & vbCrLf
    sConsulta = sConsulta & "                                   " & vbCrLf
    sConsulta = sConsulta & "-- Lo movemos de �mbito." & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @AMBITOANTIGUO=1 -- Ambito proceso" & vbCrLf
    sConsulta = sConsulta & "begin" & vbCrLf
    sConsulta = sConsulta & "      IF @AMBITONUEVO=2 -- Ambito grupo" & vbCrLf
    sConsulta = sConsulta & "      begin" & vbCrLf
    sConsulta = sConsulta & "            DECLARE C CURSOR LOCAL FOR SELECT GRUPO FROM GRUPO_OFE WITH (NOLOCK) WHERE ID=@ID" & vbCrLf
    sConsulta = sConsulta & "            OPEN C" & vbCrLf
    sConsulta = sConsulta & "            FETCH NEXT FROM C INTO @GR" & vbCrLf
    sConsulta = sConsulta & "            WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "            BEGIN" & vbCrLf
    sConsulta = sConsulta & "                  INSERT INTO OFE_GR_ATRIB (ID,GRUPO,ATRIB_ID,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL,IMPPARCIAL)    VALUES (@ID,@GR,@ATRIB_ID,@VALOR_NUM,@VALOR_TEXT,@VALOR_FEC,@VALOR_BOOL,null)" & vbCrLf
    sConsulta = sConsulta & "                  FETCH NEXT FROM C INTO @GR" & vbCrLf
    sConsulta = sConsulta & "            END" & vbCrLf
    sConsulta = sConsulta & "            CLOSE C " & vbCrLf
    sConsulta = sConsulta & "            DEALLOCATE C" & vbCrLf
    sConsulta = sConsulta & "      end   " & vbCrLf
    sConsulta = sConsulta & "      ELSE IF @AMBITONUEVO=3 -- Ambito item" & vbCrLf
    sConsulta = sConsulta & "      begin" & vbCrLf
    sConsulta = sConsulta & "            DECLARE C CURSOR LOCAL FOR SELECT GRUPO,ITEM FROM ITEM_OFE WITH (NOLOCK) WHERE ID=@ID" & vbCrLf
    sConsulta = sConsulta & "            OPEN C" & vbCrLf
    sConsulta = sConsulta & "            FETCH NEXT FROM C INTO @GR,@IT" & vbCrLf
    sConsulta = sConsulta & "            WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "            BEGIN" & vbCrLf
    sConsulta = sConsulta & "                  INSERT INTO OFE_ITEM_ATRIB (ID,ITEM,ATRIB_ID,GRUPO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL)     VALUES (@ID,@IT,@ATRIB_ID,@GR,@VALOR_NUM,@VALOR_TEXT,@VALOR_FEC,@VALOR_BOOL)" & vbCrLf
    sConsulta = sConsulta & "                  FETCH NEXT FROM C INTO @GR,@IT" & vbCrLf
    sConsulta = sConsulta & "            END" & vbCrLf
    sConsulta = sConsulta & "            CLOSE C " & vbCrLf
    sConsulta = sConsulta & "            DEALLOCATE C" & vbCrLf
    sConsulta = sConsulta & "      end         " & vbCrLf
    sConsulta = sConsulta & "end" & vbCrLf
    sConsulta = sConsulta & "ELSE IF @AMBITOANTIGUO=2 -- Ambito grupo" & vbCrLf
    sConsulta = sConsulta & "begin" & vbCrLf
    sConsulta = sConsulta & "      IF @AMBITONUEVO=3 -- Ambito item" & vbCrLf
    sConsulta = sConsulta & "      begin" & vbCrLf
    sConsulta = sConsulta & "            DECLARE C CURSOR LOCAL FOR SELECT ITEM FROM ITEM_OFE WITH (NOLOCK) WHERE ID=@ID AND GRUPO=@GRUPO" & vbCrLf
    sConsulta = sConsulta & "            OPEN C" & vbCrLf
    sConsulta = sConsulta & "            FETCH NEXT FROM C INTO @IT" & vbCrLf
    sConsulta = sConsulta & "            WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "            BEGIN" & vbCrLf
    sConsulta = sConsulta & "                  INSERT INTO OFE_ITEM_ATRIB (ID,ITEM,ATRIB_ID,GRUPO,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL)     VALUES (@ID,@IT,@ATRIB_ID,@GRUPO,@VALOR_NUM,@VALOR_TEXT,@VALOR_FEC,@VALOR_BOOL)" & vbCrLf
    sConsulta = sConsulta & "                  FETCH NEXT FROM C INTO @IT" & vbCrLf
    sConsulta = sConsulta & "            END" & vbCrLf
    sConsulta = sConsulta & "            CLOSE C " & vbCrLf
    sConsulta = sConsulta & "            DEALLOCATE C" & vbCrLf
    sConsulta = sConsulta & "      end   " & vbCrLf
    sConsulta = sConsulta & "end" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "-- Lo borramos del ambito antiguo.        " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @AMBITOANTIGUO=1 -- Ambito proceso    " & vbCrLf
    sConsulta = sConsulta & "      DELETE OFE_ATRIB WHERE ID=@ID AND ATRIB_ID=@IDATRIB" & vbCrLf
    sConsulta = sConsulta & "ELSE IF @AMBITOANTIGUO=2 -- Ambito grupo" & vbCrLf
    sConsulta = sConsulta & "      IF @GRUPO=''  -- Para los items de todos los grupos del proceso" & vbCrLf
    sConsulta = sConsulta & "            DELETE OFE_GR_ATRIB WHERE ID=@ID AND ATRIB_ID=@IDATRIB" & vbCrLf
    sConsulta = sConsulta & "      ELSE  -- Para los items de un grupo concreto del proceso" & vbCrLf
    sConsulta = sConsulta & "            DELETE OFE_GR_ATRIB WHERE ID=@ID AND GRUPO=@GRUPO AND ATRIB_ID=@IDATRIB" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_DEVOLVER_NUM_OFERTAS_ATRIB
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_NUM_OFERTAS_ATRIB]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_NUM_OFERTAS_ATRIB]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].SP_DEVOLVER_NUM_OFERTAS_ATRIB (@AMBITO int, @ANYO int, @GMN1 varchar(50),@PROCE int,@ATRIBID int ) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @AMBITO=1 --Ambito proceso" & vbCrLf
    sConsulta = sConsulta & "      SELECT COUNT(*) FROM PROCE_OFE PO WITH (NOLOCK) INNER JOIN OFE_ATRIB OA WITH (NOLOCK) ON PO.ID=OA.ID " & vbCrLf
    sConsulta = sConsulta & "      WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OA.ATRIB_ID=@ATRIBID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @AMBITO=2 --Ambito grupo" & vbCrLf
    sConsulta = sConsulta & "      SELECT COUNT(*) FROM PROCE_OFE PO WITH (NOLOCK) INNER JOIN OFE_GR_ATRIB OA WITH (NOLOCK) ON PO.ID=OA.ID " & vbCrLf
    sConsulta = sConsulta & "      WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OA.ATRIB_ID=@ATRIBID" & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "IF @AMBITO=3 --Ambito item" & vbCrLf
    sConsulta = sConsulta & "      SELECT COUNT(*) FROM PROCE_OFE PO WITH (NOLOCK) INNER JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON PO.ID=OA.ID " & vbCrLf
    sConsulta = sConsulta & "      WHERE PO.ANYO=@ANYO AND PO.GMN1=@GMN1 AND PO.PROCE=@PROCE AND OA.ATRIB_ID=@ATRIBID " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'SP_ELIMINAR_ITEM
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_ELIMINAR_ITEM]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ELIMINAR_ITEM]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_ELIMINAR_ITEM] (@ANYO INTEGER, @GMN1 VARCHAR(50), @PROCE INTEGER, @GRUPO VARCHAR(50), @ITEM INTEGER) AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @ID=ID FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT @ID IS NULL" & vbCrLf
    sConsulta = sConsulta & "BEGIN " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "      DELETE OFE_ITEM_ADJUN WHERE ID=@ID AND ITEM=@ITEM " & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "      DELETE OFE_ITEM_ATRIB WHERE ID=@ID AND GRUPO=@GRUPO AND ITEM=@ITEM " & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "DELETE ITEM_OFE WHERE ID=@ID AND GRUPO=@GRUPO AND ITEM=@ITEM" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    'FSPM_GETFIELDS_INSTANCIA
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETFIELDS_INSTANCIA]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETFIELDS_INSTANCIA] @CIA INT,@CIAPROVE INT,  @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL ,@PROVE VARCHAR(50)=NULL ,@RECENV TINYINT = 1, @NUEVO_WORKFLOW TINYINT=0 AS" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH (NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC WITH (NOLOCK) INNER JOIN CIAS C WITH (NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1" & vbCrLf
    sConsulta = sConsulta & " begin" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
    sConsulta = sConsulta & " end" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "begin" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
    sConsulta = sConsulta & "end" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    'FSQA_LOADCERTIFICADO
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FSQA_LOADCERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADCERTIFICADO]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSQA_LOADCERTIFICADO] @CIA INT,@PROVE VARCHAR(50),  @CERTIFICADO INT = NULL , @PORTAL TINYINT =NULL OUTPUT,@IDIOMA NVARCHAR(3) AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADCERTIFICADO @CERTIFICADO =@CERTIFICADO,@PORTAL=@PORTAL OUTPUT,@IDIOMA=@IDIOMA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT =NULL,@PORTAL TINYINT = NULL OUTPUT,@IDIOMA NVARCHAR(3)',@CERTIFICADO =@CERTIFICADO, @PORTAL=@PORTAL OUTPUT,@IDIOMA =@IDIOMA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    'FSQA_LOADNOCONFORMIDAD
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FSQA_LOADNOCONFORMIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDAD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSQA_LOADNOCONFORMIDAD] @CIA INT,@PROVE VARCHAR(50),  @NOCONFORMIDAD INT = NULL , @PORTAL TINYINT =NULL OUTPUT , @IDI VARCHAR(3), @VERSION INT = NULL, @IMPEXP_COMEN_ALTA TINYINT = 0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA , @IDPROVE= RC.CIA_PROVE FROM REL_CIAS RC WITH(NOLOCK) INNER JOIN CIAS C WITH(NOLOCK) ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_LOADNOCONFORMIDAD @NOCONFORMIDAD =@NOCONFORMIDAD, @PORTAL=@PORTAL OUTPUT, @IDI = @IDI , @VERSION = @VERSION, @IMPEXP_COMEN_ALTA=@IMPEXP_COMEN_ALTA'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@NOCONFORMIDAD INT =NULL,  @PORTAL TINYINT = NULL OUTPUT,@IDI VARCHAR(3), @VERSION INT, @IMPEXP_COMEN_ALTA TINYINT = 0',@NOCONFORMIDAD =@NOCONFORMIDAD,  @PORTAL=@PORTAL OUTPUT, @IDI = @IDI, @VERSION = @VERSION,@IMPEXP_COMEN_ALTA=@IMPEXP_COMEN_ALTA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


    
    
End Sub

Private Sub V_31900_2_Datos_000()
    Dim sConsulta As String
    
    'Fichero UpdatePRECIO_IMPORTE.sql: actualiza el precio y el importe netos de los �tems para aquellos registros en los que estos datos no se encuentran rellenados
    sConsulta = "DECLARE @FSGS VARCHAR(200)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD + '.dbo.' FROM CIAS WHERE FCEST=3" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Actualiza los precios netos de item" & vbCrLf
    sConsulta = sConsulta & "UPDATE ITEM_OFE SET  PREC_VALIDO= precio WHERE prec_valido is null " & vbCrLf
    sConsulta = sConsulta & "--Actualiza los importes netos de item" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'UPDATE ITEM_OFE SET  IMPORTE=CASE WHEN PD.SOLCANTMAX =1 AND IO.CANTMAX<I.CANT THEN IO.PREC_VALIDO*IO.CANTMAX ELSE IO.PREC_VALIDO*I.CANT END" & vbCrLf
    sConsulta = sConsulta & "     FROM ITEM_OFE IO INNER JOIN PROCE_OFE PO ON PO.ID=IO.ID" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN ' + @FSGS + 'ITEM I ON PO.ANYO=I.ANYO AND PO.GMN1=I.GMN1_PROCE AND PO.PROCE=I.PROCE AND IO.ITEM=I.ID" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN ' + @FSGS + 'PROCE_DEF PD ON PO.ANYO=PD.ANYO AND PO.GMN1=PD.GMN1 AND PO.PROCE=PD.PROCE " & vbCrLf
    sConsulta = sConsulta & "         WHERE IO.IMPORTE is null AND IO.CANTMAX IS NOT NULL'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = N'UPDATE ITEM_OFE SET  IMPORTE=IO.PREC_VALIDO*I.CANT " & vbCrLf
    sConsulta = sConsulta & "     FROM ITEM_OFE IO INNER JOIN PROCE_OFE PO ON PO.ID=IO.ID" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN ' + @FSGS + 'ITEM I ON PO.ANYO=I.ANYO AND PO.GMN1=I.GMN1_PROCE AND PO.PROCE=I.PROCE AND IO.ITEM=I.ID" & vbCrLf
    sConsulta = sConsulta & "         WHERE IO.IMPORTE is null AND IO.CANTMAX IS NULL'" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Actualiza los importe de grupo" & vbCrLf
    sConsulta = sConsulta & "UPDATE GRUPO_OFE SET IMPORTE_BRUTO=  IMP FROM GRUPO_OFE G INNER JOIN (SELECT GO.ID,GO.GRUPO,SUM(IO.IMPORTE) IMP FROM GRUPO_OFE GO" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN ITEM_OFE IO ON IO.ID=GO.ID AND IO.GRUPO=GO.GRUPO " & vbCrLf
    sConsulta = sConsulta & "     WHERE GO.IMPORTE_BRUTO IS NULL GROUP BY GO.ID,GO.GRUPO) Z ON Z.ID=G.ID AND Z.GRUPO=G.GRUPO " & vbCrLf
    sConsulta = sConsulta & "UPDATE GRUPO_OFE SET IMPORTE=IMPORTE_BRUTO WHERE IMPORTE IS NULL" & vbCrLf
    sConsulta = sConsulta & "--Actualiza los importe de OFERTA" & vbCrLf
    sConsulta = sConsulta & "UPDATE PROCE_OFE SET IMPORTE_BRUTO=  IMP FROM PROCE_OFE G INNER JOIN (SELECT PO.ID ,SUM(GO.IMPORTE) IMP FROM PROCE_OFE PO " & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN GRUPO_OFE GO ON PO.ID=GO.ID " & vbCrLf
    sConsulta = sConsulta & "     WHERE PO.IMPORTE_BRUTO IS NULL GROUP BY PO.ID) Z ON Z.ID=G.ID" & vbCrLf
    sConsulta = sConsulta & "UPDATE PROCE_OFE SET IMPORTE=IMPORTE_BRUTO WHERE IMPORTE IS NULL" & vbCrLf
    sConsulta = sConsulta & "--Inserta en la tabla nueva OFE_GR_COSTESDESC" & vbCrLf
    sConsulta = sConsulta & "SET @SQL='INSERT INTO OFE_GR_COSTESDESC (ID,GRUPO,ATRIB_ID)" & vbCrLf
    sConsulta = sConsulta & "   SELECT   GO.ID,GO.GRUPO,PA.ID FROM GRUPO_OFE GO INNER JOIN PROCE_OFE PO ON PO.ID=GO.ID" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN ' + @FSGS + 'PROCE_GRUPO PG ON PG.ANYO=PO.ANYO AND PG.GMN1=PO.GMN1 AND PG.PROCE=PO.PROCE AND GO.GRUPO=PG.COD " & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN ' + @FSGS + 'PROCE_ATRIB PA ON PA.ANYO=PO.ANYO AND PA.GMN1=PO.GMN1 AND PA.PROCE=PO.PROCE AND ISNULL(PA.GRUPO,0)=CASE WHEN PA.GRUPO IS NULL THEN 0 ELSE PG.ID END " & vbCrLf
    sConsulta = sConsulta & "    WHERE PA.APLIC_PREC IS NOT NULL AND APLIC_PREC <>1 AND PA.USAR_PREC =1 '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL='INSERT INTO OFE_GR_COSTESDESC (ID,GRUPO,ATRIB_ID) " & vbCrLf
    sConsulta = sConsulta & "      SELECT   GO.ID,GO.GRUPO,PA.ID" & vbCrLf
    sConsulta = sConsulta & "    FROM GRUPO_OFE GO INNER JOIN PROCE_OFE PO ON PO.ID=GO.ID " & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN ' + @FSGS + 'PROCE_GRUPO PG ON PG.ANYO=PO.ANYO AND PG.GMN1=PO.GMN1 AND PG.PROCE=PO.PROCE AND GO.GRUPO=PG.COD " & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN ' + @FSGS + 'PROCE_ATRIB PA ON PA.ANYO=PO.ANYO AND PA.GMN1=PO.GMN1 AND PA.PROCE=PO.PROCE AND ISNULL(PA.GRUPO,0)=CASE WHEN PA.GRUPO IS NULL THEN 0 ELSE PG.ID END " & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN ' + @FSGS + 'USAR_GR_ATRIB UG ON UG.ANYO=PO.ANYO AND UG.GMN1=PO.GMN1 AND UG.PROCE=PO.PROCE AND UG.GRUPO=PG.ID AND UG.ATRIB=PA.ID AND UG.USAR_PREC =1 " & vbCrLf
    sConsulta = sConsulta & "    WHERE PA.APLIC_PREC IS NOT NULL AND APLIC_PREC =1 '" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    'Fichero InsertWEBTEXT.sql: inserta (o actualiza) registros en la tabla WEBTEXT con los literales utilizados en los m�dulos
    sConsulta = "DECLARE @MODULO AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @MAXID AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "SET @MODULO = 54" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "UPDATE WEBTEXT SET " & vbCrLf
    sConsulta = sConsulta & "SPA = '�mbito'," & vbCrLf
    sConsulta = sConsulta & "ENG = '�mbito'," & vbCrLf
    sConsulta = sConsulta & "GER = '�mbito'" & vbCrLf
    sConsulta = sConsulta & "WHERE MODULO=@MODULO" & vbCrLf
    sConsulta = sConsulta & "AND ID = 93" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "UPDATE WEBTEXT SET " & vbCrLf
    sConsulta = sConsulta & "SPA = 'Importe oferta'," & vbCrLf
    sConsulta = sConsulta & "ENG = 'Importe oferta'," & vbCrLf
    sConsulta = sConsulta & "GER = 'Importe oferta'" & vbCrLf
    sConsulta = sConsulta & "WHERE MODULO=@MODULO" & vbCrLf
    sConsulta = sConsulta & "AND ID = 94" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @MAXID = MAX(ID) FROM WEBTEXT WHERE MODULO = @MODULO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-- CUMPOFERTA.ASP  MAXID=98" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Recalcular totales de �tem','Recalcular totales de �tem','Recalcular totales de �tem')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'La oferta ser� almacenada. �Desea continuar?','La oferta ser� almacenada. �Desea continuar?','La oferta ser� almacenada. �Desea continuar?')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Importe total bruto del grupo','Importe total bruto del grupo','Importe total bruto del grupo')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Importe total neto del grupo','Importe total neto del grupo','Importe total neto del grupo')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Importe grupo','Importe grupo','Importe grupo')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Precio unitario bruto','Precio unitario bruto','Precio unitario bruto')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Precio unitario neto','Precio unitario neto','Precio unitario neto')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Aceptar','Aceptar','Aceptar')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Cantidad','Cantidad','Cantidad')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Importe total del �tem','Importe total del �tem','Importe total del �tem')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Costes/descuentos a aplicar al precio unitario','Costes/descuentos a aplicar al precio unitario','Costes/descuentos a aplicar al precio unitario')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Costes/descuentos a aplicar al importe total del �tem','Costes/descuentos a aplicar al importe total del �tem','Costes/descuentos a aplicar al importe total del �tem')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--CARGARITEMS.ASP  MAXID=34" & vbCrLf
    sConsulta = sConsulta & "SET @MODULO = 93" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @MAXID = MAX(ID) FROM WEBTEXT WHERE MODULO = @MODULO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Recalcular totales de oferta','Recalcular totales de oferta','Recalcular totales de oferta')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--CARGARSUBASTA.ASP  MAXID=24" & vbCrLf
    sConsulta = sConsulta & "SET @MODULO = 94" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @MAXID = MAX(ID) FROM WEBTEXT WHERE MODULO = @MODULO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Recalcular totales de oferta','Recalcular totales de oferta','Recalcular totales de oferta')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--confirmoferta.asp / imprimiroferta.asp  MAXID=50" & vbCrLf
    sConsulta = sConsulta & "SET @MODULO = 101" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @MAXID = MAX(ID) FROM WEBTEXT WHERE MODULO = @MODULO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Costes/descuentos adicionales de la oferta','Costes/descuentos adicionales de la oferta','Costes/descuentos adicionales de la oferta')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Costes/descuentos adicionales del grupo','Costes/descuentos adicionales del grupo','Costes/descuentos adicionales del grupo')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Costes/descuentos adicionales del �tem: Total del �tem','Costes/descuentos adicionales del �tem: Total del �tem','Costes/descuentos adicionales del �tem: Total del �tem')" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @MAXID = @MAXID + 1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO WEBTEXT(MODULO,ID,SPA,ENG,GER)" & vbCrLf
    sConsulta = sConsulta & "VALUES(@MODULO,@MAXID,'Costes/descuentos adicionales del �tem: Precio del �tem','Costes/descuentos adicionales del �tem: Precio del �tem','Costes/descuentos adicionales del �tem: Precio del �tem')" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub


Public Function CodigoDeActualizacion31900_02_12_00A31900_02_12_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_2_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.12.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31900_02_12_00A31900_02_12_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31900_02_12_00A31900_02_12_01 = False
End Function



Private Sub V_31900_2_Storeds_001()
    Dim sConsulta As String
    
    'SP_DEVOLVER_OFE_ATRIB
    sConsulta = "IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[SP_DEVOLVER_OFE_ATRIB] @ID int, @AMBITO tinyint, @GRUPO varchar(50)=null AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
    sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
    sConsulta = sConsulta & "             FROM PROCE_OFE WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "            WHERE ID = @ID)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--EL CAMBIO QUE DEVUELVE ES EL CAMBIO DE LA MONEDA EN QUE SE GUARD� LA OFERTA RESPECTO A LA MONEDA DEL PROCESO" & vbCrLf
    sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
    sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL, G.IMPPARCIAL , @CAM CAMBIO" & vbCrLf
    sConsulta = sConsulta & "    FROM OFE_ATRIB G WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   WHERE ID= @ID" & vbCrLf
    sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
    sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO ,IP.IMPPARCIAL" & vbCrLf
    sConsulta = sConsulta & "    FROM OFE_GR_ATRIB G WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    left join OFE_GR_COSTESDESC IP WITH(NOLOCK)ON G.ID=IP.ID AND G.GRUPO=IP.GRUPO AND G.ATRIB_ID=IP.ATRIB_ID" & vbCrLf
    sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
    sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
    sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
    sConsulta = sConsulta & "  SELECT G.ATRIB_ID, G.GRUPO,G.ITEM,G.VALOR_TEXT,G.VALOR_NUM,G.VALOR_FEC,G.VALOR_BOOL , @CAM CAMBIO" & vbCrLf
    sConsulta = sConsulta & "    FROM OFE_ITEM_ATRIB G WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   WHERE G.ID = @ID " & vbCrLf
    sConsulta = sConsulta & "     AND G.GRUPO = ISNULL(@GRUPO,G.GRUPO)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
End Sub


