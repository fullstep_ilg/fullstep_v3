Attribute VB_Name = "bas_V_32100_5"
Public Function CodigoDeActualizacion32100_04_24_00_A32100_04_24_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_5_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.24.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_04_24_00_A32100_04_24_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_04_24_00_A32100_04_24_01 = False
End Function

Private Sub V_32100_5_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DOWNLOAD_ADJUNTO_EMAIL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TRANSFER_CIA_ESP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_TRANSFER_CIA_ESP]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_DOWNLOAD_ADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_DOWNLOAD_ADJUNTO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TRANSFER_ADJUN_OFE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_TRANSFER_ADJUN_OFE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PROCE_OFE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PROCE_OFE_ADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIAS_ACT1]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIAS_ACT1]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIAS_ACT1]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIAS_ACT2]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIAS_ACT3]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIAS_ACT3]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ANYA_CIAS_ACT4]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ANYA_CIAS_ACT4]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENERESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENERESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ADJUNTO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETREQUESTADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETREQUESTADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_ADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GUARDARCOMENTARIOADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GUARDARCOMENTARIOADJUNTO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_CREAR_ADJUNTOS_COPIADOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_GETADJUNTO_CONTRATO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_GETADJUNTO_CONTRATO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ARTICULOS_GMN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ARTICULOS_GMN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GRABARESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GRABARESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ESPECIFICACIONCIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ESPECIFICACIONCIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GRABAR_ESPECIFICACION_CIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GRABAR_ESPECIFICACION_CIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_GRABAR_ESPECIFICACION_CIA] @CIA INT, @ID INT, @NOM NVARCHAR(100), @COMENT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CIAS_ESP (CIA,ID,NOM,COM,FECACT,DATA)" & vbCrLf
sConsulta = sConsulta & "VALUES (@CIA,@ID,@NOM,@COMENT,GETDATE(),(0x))" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_LEER_ESPECIFICACION_CIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_LEER_ESPECIFICACION_CIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_LEER_ESPECIFICACION_CIA] @ID INT, @CIA INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT  FROM CIAS_ESP  WITH (NOLOCK)  WHERE ID=@ID AND CIA=@CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_ANYA_CIAS_ACT1]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_ANYA_CIAS_ACT1]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_ANYA_CIAS_ACT1] (@CIACOD varchar(50), @GMN1 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT1 IS NULL INSERT INTO CIAS_ACT1 (CIA,ACT1) VALUES (@ID_CIA ,@ID_ACT1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_ANYA_CIAS_ACT2]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_ANYA_CIAS_ACT2]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_ANYA_CIAS_ACT2] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT1 IS NULL SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT2 IS NULL INSERT INTO CIAS_ACT2 (CIA,ACT1,ACT2) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_ANYA_CIAS_ACT3]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_ANYA_CIAS_ACT3]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_ANYA_CIAS_ACT3] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT1 IS NULL SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT2 IS NULL SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT3 IS NULL INSERT INTO CIAS_ACT3 (CIA,ACT1,ACT2,ACT3) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 , @ID_ACT3 )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_ANYA_CIAS_ACT4]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_ANYA_CIAS_ACT4]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_ANYA_CIAS_ACT4] (@CIACOD varchar(50), @GMN1 varchar(50),@GMN2 varchar(50),@GMN3 varchar(50),@GMN4 varchar(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CIA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ACT4 AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_CIA=(SELECT ID FROM CIAS WITH(NOLOCK) WHERE COD=@CIACOD)" & vbCrLf
sConsulta = sConsulta & "SET @ID_ACT1=(SELECT ID FROM ACT1 WITH(NOLOCK) WHERE COD=@GMN1)" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT1 IS NULL SET @ID_ACT2=(SELECT ID FROM ACT2 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND COD=@GMN2)" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT2 IS NULL SET @ID_ACT3=(SELECT ID FROM ACT3 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND COD=@GMN3)" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT3 IS NULL SET @ID_ACT4=(SELECT ID FROM ACT4 WITH(NOLOCK) WHERE ACT1=@ID_ACT1 AND ACT2=@ID_ACT2 AND ACT3=@ID_ACT3 AND COD=@GMN4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_ACT4 IS NULL INSERT INTO CIAS_ACT4 (CIA,ACT1,ACT2,ACT3,ACT4) VALUES (@ID_CIA , @ID_ACT1 , @ID_ACT2 , @ID_ACT3 , @ID_ACT4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion32100_04_24_01_A32100_04_24_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_5_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.24.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_04_24_01_A32100_04_24_02 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_04_24_01_A32100_04_24_02 = False
End Function

Private Sub V_32100_5_Storeds_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADSOLICITUDES_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADSOLICITUDES_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub
