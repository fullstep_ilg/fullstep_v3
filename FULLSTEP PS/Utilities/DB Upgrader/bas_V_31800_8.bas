Attribute VB_Name = "bas_V_31800_8"
Option Explicit

Public Function CodigoDeActualizacion31800_07_08_02A31800_08_09_00()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
               
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.09.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_07_08_02A31800_08_09_00 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_07_08_02A31800_08_09_00 = False
End Function

Public Function CodigoDeActualizacion31800_08_09_00A31800_08_09_01()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
               
    V_31800_8_Datos_901
               
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.09.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_08_09_00A31800_08_09_01 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_08_09_00A31800_08_09_01 = False
End Function

Public Function CodigoDeActualizacion31800_08_09_01A31800_08_09_02()
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sfecha As String

   
   On Error GoTo error
   ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
   ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
   bTransaccionEnCurso = True
    
   frmProgreso.lblDetalle = "Cambios en Storeds"
   frmProgreso.lblDetalle.Refresh
   If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
   V_31800_8_Storeds_902
                     
   'Introducimos el nuevo valor de versi�n
   sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.09.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
   ExecuteSQL gRDOCon, sConsulta
   
   ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
   bTransaccionEnCurso = False
  
   CodigoDeActualizacion31800_08_09_01A31800_08_09_02 = True
   Exit Function
   
error:
   If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
   End If
   
   MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
   
   CodigoDeActualizacion31800_08_09_01A31800_08_09_02 = False
End Function


Private Sub V_31800_8_Datos_901()

Dim sConsulta As String

sConsulta = "UPDATE WEBFSPMTEXT SET TEXT_ENG = 'Please wait while the selected action is carried out'" & vbCrLf
sConsulta = sConsulta & " WHERE TEXT_SPA = 'Espere mientras se realiza la acci�n seleccionada' AND TEXT_SPA = TEXT_ENG"

gRDOCon.QueryTimeout = 7200
gRDOCon.Execute sConsulta, rdExecDirect
 
sConsulta = "UPDATE WEBFSPMTEXT SET TEXT_GER = 'Bitte warten, w�hrend der gew�hlte Vorgang ausgef�hrt wird'"
sConsulta = sConsulta & " WHERE TEXT_SPA = 'Espere mientras se realiza la acci�n seleccionada' AND TEXT_SPA = TEXT_GER"

gRDOCon.QueryTimeout = 7200
gRDOCon.Execute sConsulta, rdExecDirect

End Sub


Private Sub V_31800_8_Storeds_902()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GET_SOLICITUDES_PDTES_PROVE @CIA INT,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_SOLICITUDES_PDTES_PROVE @PRV=@PRV'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50)', @PRV=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADFIELDSDESGLOSECALC @CIA INT,@CAMPO INT, @SOLICITUD INT,  @PER VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADFIELDSDESGLOSECALC @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT, @SOLICITUD  INT,@PER VARCHAR(50) ', @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADDESGLOSEPADREVISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADDESGLOSEPADREVISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADDESGLOSEPADREVISIBLE] @CIA INT, @FORMULARIO INT, @IDCAMPO INT, @IDI VARCHAR(50)='SPA', @SOLICITUD INT ,@PER VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADDESGLOSEPADREVISIBLE @FORMULARIO=@FORMULARIO, @IDCAMPO = @IDCAMPO, @IDI=@IDI, @SOLICITUD = @SOLICITUD, @PER=@PER '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@FORMULARIO INT, @IDCAMPO  INT, @IDI VARCHAR(50),@SOLICITUD INT, @PER VARCHAR(50) = NULL', @FORMULARIO=@FORMULARIO, @IDCAMPO = @IDCAMPO,  @IDI=@IDI, @SOLICITUD = @SOLICITUD, @PER=@PER" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE] @CIA INT, @INSTANCIA INT,@IDCAMPO INT,@VERSION INT,@IDI VARCHAR(20)='SPA',  @USU VARCHAR(50)=NULL,   @PROVE VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTDESGLOSEPADREVISIBLE @INSTANCIA=@INSTANCIA, @IDCAMPO = @IDCAMPO, @VERSION=@VERSION,@IDI=@IDI, @USU = @USU, @PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDCAMPO  INT, @VERSION INT, @IDI VARCHAR(50),@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL', @INSTANCIA=@INSTANCIA, @IDCAMPO = @IDCAMPO, @VERSION=@VERSION, @IDI=@IDI, @USU = @USU, @PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub
