Attribute VB_Name = "bas_V_31900_1"
Option Explicit

Public Function CodigoDeActualizacion31800_09_10_02A31900_01_11_00() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    
    frmProgreso.lblDetalle = "Cambios en tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_1_Tablas_000
    V_31900_1_Tablas_000_2
    V_31900_1_Tablas_000_3
    
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_1_Storeds_000
               
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.11.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31800_09_10_02A31900_01_11_00 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31800_09_10_02A31900_01_11_00 = False
End Function



Public Function CodigoDeActualizacion31800_09_11_00A31900_01_11_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_1_Storeds_001
               
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.11.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31800_09_11_00A31900_01_11_01 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31800_09_11_00A31900_01_11_01 = False
End Function


Private Sub V_31900_1_Storeds_000()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUN @CIA INT, @TIPO INT, @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUN @TIPO =@TIPO, @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @TIPO INT, @ID INT', @TIPO =@TIPO, @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNDATA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNDATA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNDATA @CIA INT=NULL, @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNDATA @ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @INIT INT, @OFFSET INT ,@TIPO INT = 0',@ID=@ID, @INIT=@INIT, @OFFSET=@OFFSET, @TIPO=@TIPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTADJUNTOS @CIA INT, @TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTADJUNTOS @TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@TIPO INT, @IDS VARCHAR(2000) =NULL,  @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL  ',@TIPO =@TIPO , @IDS = @IDS ,  @IDSNEW =@IDSNEW , @IDI =@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTFIELD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTFIELD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GETINSTFIELD @CIA INT, @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT, @CIAPROV INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTFIELD @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @ID INT, @IDI VARCHAR(20) = NULL, @INSTANCIA INT', @ID=@ID, @IDI=@IDI, @INSTANCIA=@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PAISES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PAISES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PAISES @IDIOMA VARCHAR(20), @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PAISES @COD=@COD ,@DEN=@DEN ,@COINCID=@COINCID, @IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @COINCID TINYINT, @IDIOMA VARCHAR(20)', @COD=@COD , @DEN=@DEN , @COINCID=@COINCID, @IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_MONEDAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_MONEDAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_MONEDAS @CIA INT,@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @COINCID TINYINT = 1, @IDIOMA VARCHAR(20),@ALLDATOS TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_MONEDAS @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA,@ALLDATOS=@ALLDATOS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@COD VARCHAR(50), @DEN VARCHAR(2000) , @COINCID TINYINT,  @IDIOMA VARCHAR (20), @ALLDATOS TINYINT', @COD =@COD , @DEN =@DEN , @COINCID =@COINCID, @IDIOMA=@IDIOMA, @ALLDATOS=@ALLDATOS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_PROVIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_PROVIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_PROVIS @CIA INT,@PAI VARCHAR(50),@COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @USALIKE TINYINT =null, @IDIOMA VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_PROVIS @PAI=@PAI,@COD=@COD,@DEN =@DEN,@USALIKE=@USALIKE,@IDIOMA=@IDIOMA'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PAI VARCHAR(50),@COD VARCHAR(50)=null, @DEN VARCHAR(200)=null, @USALIKE TINYINT, @IDIOMA VARCHAR(50)', @PAI=@PAI,@COD=@COD,@DEN =@DEN,@USALIKE=@USALIKE,@IDIOMA=@IDIOMA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSP_LOGIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSP_LOGIN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSP_LOGIN @CIA VARCHAR(50), @COD VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
sConsulta = sConsulta & "       CIAS.USUPPAL AS USUPPAL, CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
sConsulta = sConsulta & "       USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
sConsulta = sConsulta & "       USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "       USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "       USU.DATEFMT DATEFMT, USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "       IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
sConsulta = sConsulta & "       USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA , MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU," & vbCrLf
sConsulta = sConsulta & "       CIAS.DEN AS CIADEN, CIAS.NIF" & vbCrLf
sConsulta = sConsulta & "  FROM USU" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN USU_PORT" & vbCrLf
sConsulta = sConsulta & "               ON USU.ID=USU_PORT.USU" & vbCrLf
sConsulta = sConsulta & "              AND USU_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "              AND USU.CIA=USU_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CIAS" & vbCrLf
sConsulta = sConsulta & "               ON USU.CIA=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN CIAS_PORT" & vbCrLf
sConsulta = sConsulta & "               ON USU.CIA=CIAS_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "              AND CIAS_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN MON" & vbCrLf
sConsulta = sConsulta & "               ON CIAS.MON=MON.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN MON MON2" & vbCrLf
sConsulta = sConsulta & "               ON USU.MON=MON2.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN IDI" & vbCrLf
sConsulta = sConsulta & "               ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & " WHERE USU.COD=@COD" & vbCrLf
sConsulta = sConsulta & "   AND CIAS.COD=@CIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_MON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_MON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  [dbo].[SP_ELIM_MON]  (@MON_COD NVARCHAR(50),@CODMONCENTRAL NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXISTE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMON INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDMONCEN INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDMON=(SELECT ID FROM MON WHERE COD=@MON_COD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDMON IS NOT NULL " & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "    SET @IDMONCEN=(SELECT ID FROM MON WHERE COD=@CODMONCENTRAL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM CIAS WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE CIAS SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM PAI WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE PAI SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @EXISTE = (SELECT COUNT(*)  FROM USU WHERE MON=@IDMON)" & vbCrLf
sConsulta = sConsulta & "    IF @EXISTE<>0" & vbCrLf
sConsulta = sConsulta & "   UPDATE USU SET MON=@IDMONCEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM MON_DEN WHERE MON=@IDMON" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM MON WHERE ID=@IDMON" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_PAIS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_PAIS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  [dbo].[SP_ELIM_PAIS]  (@PAI_COD NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PAI_DEN WHERE PAI = (SELECT ID FROM PAI WHERE COD=@PAI_COD)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PAI WHERE COD=@PAI_COD" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_PROVI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_PROVI]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  [dbo].[SP_ELIM_PROVI]  (@PAI_COD NVARCHAR(50),@PROVI_COD NVARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PAIS AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PROVI AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ID_PAIS=(SELECT ID FROM PAI  WHERE COD=@PAI_COD)" & vbCrLf
sConsulta = sConsulta & "   SET @ID_PROVI=(SELECT ID FROM PROVI  WHERE PAI=@ID_PAIS AND COD=@PROVI_COD)" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROVI_DEN WHERE PAI=@ID_PAIS AND PROVI=@ID_PROVI" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROVI WHERE PAI=@ID_PAIS AND COD=@PROVI_COD " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_DATOS_GRABAR_CERTIF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_DATOS_GRABAR_CERTIF]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE dbo.FSQA_DATOS_GRABAR_CERTIF @CIA INT, @INSTANCIA INT, @CERTIFICADO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_DATOS_GRABAR_CERTIF @INSTANCIA =@INSTANCIA ,@CERTIFICADO =@CERTIFICADO'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @CERTIFICADO INT', @INSTANCIA =@INSTANCIA ,@CERTIFICADO =@CERTIFICADO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_DATOS_GRABAR_NC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_DATOS_GRABAR_NC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE dbo.FSQA_DATOS_GRABAR_NC @CIA INT, @INSTANCIA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_DATOS_GRABAR_NC @ID =@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID =@INSTANCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_31900_1_Storeds_001()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GET_SOLICITUDES_PDTES_PROVE @CIA INT,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GET_SOLICITUDES_PDTES_PROVE @PRV=@PRV'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50)', @PRV=@PROVEGS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSDESGLOSECALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSDESGLOSECALC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADFIELDSDESGLOSECALC @CIA INT,@CAMPO INT, @SOLICITUD INT,  @PER VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADFIELDSDESGLOSECALC @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@CAMPO INT, @SOLICITUD  INT,@PER VARCHAR(50) ', @CAMPO=@CAMPO, @SOLICITUD = @SOLICITUD, @PER=@PER" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADDESGLOSEPADREVISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADDESGLOSEPADREVISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADDESGLOSEPADREVISIBLE] @CIA INT, @FORMULARIO INT, @IDCAMPO INT, @IDI VARCHAR(50)='SPA', @SOLICITUD INT ,@PER VARCHAR(50)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADDESGLOSEPADREVISIBLE @FORMULARIO=@FORMULARIO, @IDCAMPO = @IDCAMPO, @IDI=@IDI, @SOLICITUD = @SOLICITUD, @PER=@PER '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@FORMULARIO INT, @IDCAMPO  INT, @IDI VARCHAR(50),@SOLICITUD INT, @PER VARCHAR(50) = NULL', @FORMULARIO=@FORMULARIO, @IDCAMPO = @IDCAMPO,  @IDI=@IDI, @SOLICITUD = @SOLICITUD, @PER=@PER" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADINSTDESGLOSEPADREVISIBLE] @CIA INT, @INSTANCIA INT,@IDCAMPO INT,@VERSION INT,@IDI VARCHAR(20)='SPA',  @USU VARCHAR(50)=NULL,   @PROVE VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_LOADINSTDESGLOSEPADREVISIBLE @INSTANCIA=@INSTANCIA, @IDCAMPO = @IDCAMPO, @VERSION=@VERSION,@IDI=@IDI, @USU = @USU, @PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDCAMPO  INT, @VERSION INT, @IDI VARCHAR(50),@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL', @INSTANCIA=@INSTANCIA, @IDCAMPO = @IDCAMPO, @VERSION=@VERSION, @IDI=@IDI, @USU = @USU, @PROVE=@PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_31900_1_Tablas_000()

Dim sConsulta As String
Dim sSQL As String
Dim rdores As rdoResultset
Dim ilongMon As Integer

    gRDOCon.QueryTimeout = 7200


    sSQL = "SELECT LENGTH FROM SYSCOLUMNS C INNER JOIN SYSOBJECTS O ON O.ID=C.ID WHERE O.NAME='MON' AND O.XTYPE='U' AND C.NAME='COD'"
  
    Set rdores = gRDOCon.OpenResultset(sSQL, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    
    If rdores.EOF Then
        ilongMon = 20
    Else
        ilongMon = rdores(0).Value
    End If
    
    rdores.Close
    Set rdores = Nothing
    

sConsulta = "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_CIAS_MON_MON]')  and OBJECTPROPERTY(id, N'IsForeignKey') = 1) " & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE [dbo].[CIAS_MON] DROP CONSTRAINT [FK_CIAS_MON_MON]" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PAI_FK_MON]')  and OBJECTPROPERTY(id, N'IsForeignKey') = 1) " & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE [dbo].[PAI] DROP CONSTRAINT [PAI_FK_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_PAI_03')" & vbCrLf
sConsulta = sConsulta & "   DROP INDEX PAI.IX_PAI_03" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_PARGEN_DEF_MON]')  and OBJECTPROPERTY(id, N'IsForeignKey') = 1) " & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE [dbo].[PARGEN_DEF] DROP CONSTRAINT [FK_PARGEN_DEF_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_USU_MON]')  and OBJECTPROPERTY(id, N'IsForeignKey') = 1) " & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE [dbo].[USU] DROP CONSTRAINT [FK_USU_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAS_FK_MON]')  and OBJECTPROPERTY(id, N'IsForeignKey') = 1) " & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE [dbo].[cias] DROP CONSTRAINT [CIAS_FK_MON]" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_CIAS_08')" & vbCrLf
sConsulta = sConsulta & "   DROP INDEX CIAS.IX_CIAS_08" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_VOLADJ_MON]')  and OBJECTPROPERTY(id, N'IsForeignKey') = 1) " & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE [dbo].[VOLADJ] DROP CONSTRAINT [FK_VOLADJ_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_MON]')  and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1 )" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE [dbo].[MON] DROP CONSTRAINT [PK_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_MON_01')" & vbCrLf
sConsulta = sConsulta & "   DROP INDEX MON.IX_MON_01" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_MON_02')" & vbCrLf
sConsulta = sConsulta & "   DROP INDEX MON.IX_MON_02" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[MON] ALTER COLUMN [COD] [nvarchar] (" & ilongMon & ") NOT NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_MON] ALTER COLUMN [MON_PORTAL] [nvarchar] (" & ilongMon & ") NULL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[VOLADJ] ALTER COLUMN [MONPOR] [nvarchar] (" & ilongMon & ") NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[MON]  ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_MON] PRIMARY KEY NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [ID] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE UNIQUE NONCLUSTERED INDEX [IX_MON_01] ON [MON] " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [COD] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_MON]  WITH NOCHECK ADD  CONSTRAINT [FK_CIAS_MON_MON] FOREIGN KEY([MON_PORTAL])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[MON] ([COD])" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS_MON] CHECK CONSTRAINT [FK_CIAS_MON_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PAI]  WITH NOCHECK ADD  CONSTRAINT [PAI_FK_MON] FOREIGN KEY([MON])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[MON] ([ID])" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PAI] CHECK CONSTRAINT [PAI_FK_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX [IX_PAI_03] ON [dbo].[PAI] " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [MON] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_DEF]  WITH NOCHECK ADD  CONSTRAINT [FK_PARGEN_DEF_MON] FOREIGN KEY([MONCEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[MON] ([ID])" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PARGEN_DEF] CHECK CONSTRAINT [FK_PARGEN_DEF_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU]  WITH NOCHECK ADD  CONSTRAINT [FK_USU_MON] FOREIGN KEY([MON])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[MON] ([ID])" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[USU] CHECK CONSTRAINT [FK_USU_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS]  WITH NOCHECK ADD  CONSTRAINT [CIAS_FK_MON] FOREIGN KEY([MON])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[MON] ([ID])" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CIAS] CHECK CONSTRAINT [CIAS_FK_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX [IX_CIAS_08] ON [CIAS] " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [MON] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[VOLADJ]  WITH NOCHECK ADD  CONSTRAINT [FK_VOLADJ_MON] FOREIGN KEY([MONPOR])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[MON] ([COD])" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[VOLADJ] CHECK CONSTRAINT [FK_VOLADJ_MON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS_MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CIAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VOLADJ CHECK CONSTRAINT ALL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & " drop trigger [dbo].[MON_TG_INS]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE TRIGGER MON_TG_INSUPD ON dbo.MON" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE MON SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM MON M WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I   ON M.ID=I.ID" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


sConsulta = "CREATE TABLE [dbo].[MON_DEN](" & vbCrLf
sConsulta = sConsulta & "   [MON] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [DEN] [nvarchar](100) NULL," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_MON_DEN] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [MON] ASC," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] ASC" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE TRIGGER [dbo].[MON_DEN_TG_INSUPD] ON [dbo].[MON_DEN]" & vbCrLf
sConsulta = sConsulta & "   AFTER INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   -- SET NOCOUNT ON added to prevent extra result sets from" & vbCrLf
sConsulta = sConsulta & "   -- interfering with SELECT statements." & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE MON_DEN SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "   FROM MON_DEN M WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I   ON M.MON=I.MON AND M.IDIOMA = I.IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    -- Insert statements for trigger here" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[MON_DEN]  WITH CHECK ADD  CONSTRAINT [FK_MON_DEN_IDI] FOREIGN KEY([IDIOMA])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[IDI] ([ID])" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[MON_DEN] CHECK CONSTRAINT [FK_MON_DEN_IDI]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[MON_DEN]  WITH CHECK ADD  CONSTRAINT [FK_MON_DEN_MON] FOREIGN KEY([MON])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[MON] ([ID])" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[MON_DEN] CHECK CONSTRAINT [FK_MON_DEN_MON]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "insert into mon_den (MON,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select MON.ID,IDI.ID,MON.DEN_SPA from MON" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='SPA'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "insert into mon_den (MON,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select MON.ID,IDI.ID,MON.DEN_ENG from MON" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='ENG'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "insert into mon_den (MON,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select MON.ID,IDI.ID,MON.DEN_GER from MON" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='GER'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DF_MON_EQ_ACT]') )" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE [dbo].[mon] DROP CONSTRAINT [DF_MON_EQ_ACT]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[MON]') and name=N'DEN_SPA')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.MON DROP COLUMN DEN_SPA" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


sConsulta = "if exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[MON]') and name=N'DEN_GER')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.MON DROP COLUMN DEN_GER" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[MON]') and name=N'DEN_ENG')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.MON DROP COLUMN DEN_ENG" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[MON]') and name=N'EQ_ACT')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.MON DROP COLUMN EQ_ACT" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub


Private Sub V_31900_1_Tablas_000_2()

Dim sConsulta As String

Dim sSQL As String
Dim rdores As rdoResultset
Dim ilongPai As Integer

    gRDOCon.QueryTimeout = 7200


    sSQL = "SELECT LENGTH FROM SYSCOLUMNS C INNER JOIN SYSOBJECTS O ON O.ID=C.ID WHERE O.NAME='PAI' AND O.XTYPE='U' AND C.NAME='COD'"
  
    Set rdores = gRDOCon.OpenResultset(sSQL, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    
    If rdores.EOF Then
        ilongPai = 3
    Else
        ilongPai = rdores(0).Value
    End If
    
    rdores.Close
    Set rdores = Nothing


sConsulta = "ALTER TABLE PAI NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_PAI_01')" & vbCrLf
sConsulta = sConsulta & "   DROP INDEX PAI.IX_PAI_01" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_PAI_02')" & vbCrLf
sConsulta = sConsulta & "   DROP INDEX PAI.IX_PAI_02" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PAI] ALTER COLUMN [COD] [nvarchar] (" & ilongPai & ") NOT NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE UNIQUE NONCLUSTERED INDEX [IX_PAI_01] ON [dbo].[PAI] " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [COD] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI CHECK CONSTRAINT ALL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE TABLE [dbo].[PAI_DEN](" & vbCrLf
sConsulta = sConsulta & "   [PAI] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [DEN] [nvarchar](100) NULL," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_PAI_DEN] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [PAI] ASC," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] ASC" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE TRIGGER [dbo].[PAI_DEN_TG_INSUPD] ON [dbo].[PAI_DEN]" & vbCrLf
sConsulta = sConsulta & "   AFTER INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   -- SET NOCOUNT ON added to prevent extra result sets from" & vbCrLf
sConsulta = sConsulta & "   -- interfering with SELECT statements." & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "   UPDATE PAI_DEN SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PAI_DEN M WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I   ON M.PAI=I.PAI AND M.IDIOMA = I.IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    -- Insert statements for trigger here" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[PAI_DEN]  WITH CHECK ADD  CONSTRAINT [FK_PAI_DEN_IDI] FOREIGN KEY([IDIOMA])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[IDI] ([ID])" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[PAI_DEN] CHECK CONSTRAINT [FK_PAI_DEN_IDI]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[PAI_DEN]  WITH CHECK ADD  CONSTRAINT [FK_PAI_DEN_PAI] FOREIGN KEY([PAI])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[PAI] ([ID])" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[PAI_DEN] CHECK CONSTRAINT [FK_PAI_DEN_PAI]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "insert into pai_den (PAI,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select PAI.ID,IDI.ID,PAI.DEN from PAI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='SPA'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "insert into pai_den (PAI,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select PAI.ID,IDI.ID,PAI.DEN from PAI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='ENG'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "insert into pai_den (PAI,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select PAI.ID,IDI.ID,PAI.DEN from PAI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='GER'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[PAI]') and name=N'DEN')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PAI DROP COLUMN DEN" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub


Private Sub V_31900_1_Tablas_000_3()

Dim sConsulta As String

Dim sSQL As String
Dim rdores As rdoResultset
Dim ilongProvi As Integer

    gRDOCon.QueryTimeout = 7200


    sSQL = "SELECT LENGTH FROM SYSCOLUMNS C INNER JOIN SYSOBJECTS O ON O.ID=C.ID WHERE O.NAME='PROVI' AND O.XTYPE='U' AND C.NAME='COD'"
  
    Set rdores = gRDOCon.OpenResultset(sSQL, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    
    If rdores.EOF Then
        ilongProvi = 3
    Else
        ilongProvi = rdores(0).Value
    End If
    
    rdores.Close
    Set rdores = Nothing



sConsulta = "ALTER TABLE PROVI NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_PROVI_01')" & vbCrLf
sConsulta = sConsulta & "   DROP INDEX PROVI.IX_PROVI_01" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROVI] ALTER COLUMN [COD] [nvarchar] (" & ilongProvi & ") NOT NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE UNIQUE NONCLUSTERED INDEX [IX_PROVI_01] ON [dbo].[PROVI] " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [PAI] ASC," & vbCrLf
sConsulta = sConsulta & "   [COD] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVI CHECK CONSTRAINT ALL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE TABLE [dbo].[PROVI_DEN](" & vbCrLf
sConsulta = sConsulta & "   [PAI] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [PROVI] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [DEN] [nvarchar](100) NULL," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_PROVI_DEN] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [PAI] ASC," & vbCrLf
sConsulta = sConsulta & "   [PROVI] ASC," & vbCrLf
sConsulta = sConsulta & "   [IDIOMA] ASC" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE TRIGGER [dbo].[PROVI_DEN_TG_INSUPD] ON [dbo].[PROVI_DEN]" & vbCrLf
sConsulta = sConsulta & "   AFTER  INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   -- SET NOCOUNT ON added to prevent extra result sets from" & vbCrLf
sConsulta = sConsulta & "   -- interfering with SELECT statements." & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROVI_DEN SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "   FROM PROVI_DEN M WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I   ON M.PAI=I.PAI AND M.PROVI=I.PROVI AND M.IDIOMA = I.IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    -- Insert statements for trigger here" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[PROVI_DEN]  WITH CHECK ADD  CONSTRAINT [FK_PROVI_DEN_IDI] FOREIGN KEY([IDIOMA])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[IDI] ([ID])" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[PROVI_DEN] CHECK CONSTRAINT [FK_PROVI_DEN_IDI]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[PROVI_DEN]  WITH CHECK ADD  CONSTRAINT [FK_PROVI_DEN_PROVI] FOREIGN KEY([PAI], [PROVI])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[PROVI] ([PAI], [ID])" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[PROVI_DEN] CHECK CONSTRAINT [FK_PROVI_DEN_PROVI]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


sConsulta = "insert into provi_den (PAI,PROVI,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select PROVI.PAI,PROVI.ID,IDI.ID,PROVI.DEN from PROVI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='SPA'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "insert into provi_den (PAI,PROVI,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select PROVI.PAI,PROVI.ID,IDI.ID,PROVI.DEN from PROVI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='eng'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "insert into provi_den (PAI,PROVI,IDIOMA,DEN)" & vbCrLf
sConsulta = sConsulta & "Select PROVI.PAI,PROVI.ID,IDI.ID,PROVI.DEN from PROVI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDI ON IDI.COD='GER'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_PROVI_02')" & vbCrLf
sConsulta = sConsulta & "   DROP INDEX PROVI.IX_PROVI_02" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.syscolumns where id=object_id(N'[dbo].[PROVI]') and name=N'DEN')" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PROVI DROP COLUMN DEN" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


End Sub


Public Function CodigoDeActualizacion31800_09_11_01A31900_01_11_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_1_Datos_901_1
               
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.11.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31800_09_11_01A31900_01_11_02 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31800_09_11_01A31900_01_11_02 = False
End Function

Private Sub V_31900_1_Datos_901_1()

Dim sConsulta As String
 
sConsulta = sConsulta & "UPDATE WEBTEXT SET SPA='La subasta ha finalizado, a continuaci�n se proceder� a analizar la informaci�n de las pujas presentadas y proceder a la adjudicaci�n  del proceso de compra. Se le comunicar� la decisi�n adoptada al respecto.'," & vbCrLf
sConsulta = sConsulta & "                  ENG='The auction has finished. The information in the submitted bids will now be analyzed and the purchasing process will be awarded. You will be notified of the corresponding decision.'," & vbCrLf
sConsulta = sConsulta & "                  GER='La subasta ha finalizado, a continuaci�n se proceder� a analizar la informaci�n de las pujas presentadas y proceder a la adjudicaci�n  del proceso de compra. Se le comunicar� la decisi�n adoptada al respecto.'" & vbCrLf
sConsulta = sConsulta & "WHERE MODULO=90 AND ID=10" & vbCrLf

gRDOCon.QueryTimeout = 7200
gRDOCon.Execute sConsulta, rdExecDirect

End Sub

Public Function CodigoDeActualizacion31800_09_11_02A31900_01_11_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
'-------------------------------------------------------------------------------------------
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_1_Storeds_003
               
'-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.11.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31800_09_11_02A31900_01_11_03 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31800_09_11_02A31900_01_11_03 = False
End Function

Private Sub V_31900_1_Storeds_003()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_BUSQREGEXTERNA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_BUSQREGEXTERNA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_BUSQREGEXTERNA] @CIA INT, @IDTABLA INT, @TEXTO VARCHAR(2000), @BUSQEXACTA TINYINT=1, @FILTROART VARCHAR(100)=NULL AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSPM_BUSQREGEXTERNA @IDTABLA=@IDTABLA, @TEXTO=@TEXTO, @BUSQEXACTA=@BUSQEXACTA, @FILTROART=@FILTROART'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDTABLA INT, @TEXTO VARCHAR(2000), @BUSQEXACTA TINYINT=1, @FILTROART VARCHAR(100)=NULL', @IDTABLA=@IDTABLA, @TEXTO=@TEXTO, @BUSQEXACTA=@BUSQEXACTA, @FILTROART=@FILTROART" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31800_09_11_03A31900_01_11_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
'-------------------------------------------------------------------------------------------
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_1_Storeds_004
               
'-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.11.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31800_09_11_03A31900_01_11_04 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31800_09_11_03A31900_01_11_04 = False
End Function

Private Sub V_31900_1_Storeds_004()

Dim sConsulta As String

'FSPM_GETINSTDESGLOSE
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTDESGLOSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTDESGLOSE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTDESGLOSE] @CIA INT,  @ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT ,@PROVE VARCHAR(50)=NULL , @VERSION INT, @NUEVO_WORKFLOW INT=0 , @DEFECTO TINYINT= 0, @VERSION_BD_CERT INT =0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIAPROVE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA,@CIAPROVE=RC.CIA_PROVE FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE_31600 @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION, @DEFECTO=@DEFECTO '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSWS_GETINSTDESGLOSE @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO OUTPUT ,@PROVE = @PROVE, @VERSION =@VERSION, @DEFECTO=@DEFECTO, @VERSION_BD_CERT=@VERSION_BD_CERT '   " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT,@IDI VARCHAR(20) = NULL, @INSTANCIA INT = NULL, @USU VARCHAR(50)=NULL, @COPIA_CAMPO INT OUTPUT,@PROVE VARCHAR(50), @VERSION INT, @DEFECTO TINYINT, @VERSION_BD_CERT INT ', @ID =@ID ,@IDI=@IDI, @INSTANCIA=@INSTANCIA, @USU=@USU, @COPIA_CAMPO =@COPIA_CAMPO  OUTPUT,@PROVE = @PROVEGS, @VERSION = @VERSION , @DEFECTO=@DEFECTO,@VERSION_BD_CERT=@VERSION_BD_CERT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


'FSPM_GETFIELDS_INSTANCIA
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETFIELDS_INSTANCIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETFIELDS_INSTANCIA] @CIA INT,@CIAPROVE INT,  @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL ,@PROVE VARCHAR(50)=NULL ,@RECENV TINYINT = 1, @NUEVO_WORKFLOW TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFLOW=1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETFIELDS_INSTANCIA @ID =@ID, @IDI = @IDI , @VERSION =@VERSION,@USU =@USU,@PROVE = @PROVE'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI VARCHAR(20) , @VERSION INT,@USU VARCHAR(50),@PROVE VARCHAR(50)', @ID=@ID, @IDI=@IDI, @VERSION=@VERSION, @USU=@USU,@PROVE = @PROVEGS" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


'FSPM_GETINSTINVISIBLEFIELDS
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINSTINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINSTINVISIBLEFIELDS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTINVISIBLEFIELDS] @CIA INT,  @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL,@NUEVO_WORKFL TINYINT=0    AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEGS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PROVEGS = COD_PROVE_CIA FROM REL_CIAS RC INNER JOIN CIAS C ON RC.CIA_PROVE = C.ID WHERE RC.CIA_COMP = @CIA AND C.COD = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSPM_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINSTINVISIBLEFIELDS @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL ',  @IDCAMPO =@IDCAMPO, @USU=@USU, @PROVE= @PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



'FSPM_GETINVISIBLEFIELDS
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GETINVISIBLEFIELDS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GETINVISIBLEFIELDS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINVISIBLEFIELDS] @CIA INT, @ID INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSQA_GETINVISIBLEFIELDS @ID=@ID '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31800_09_11_04A31900_01_11_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
    
    
    On Error GoTo error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
'-------------------------------------------------------------------------------------------
    
    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_31900_1_Storeds_005
               
'-------------------------------------------------------------------------------------------
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM='3.00.11.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion31800_09_11_04A31900_01_11_05 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion31800_09_11_04A31900_01_11_05 = False
End Function

Private Sub V_31900_1_Storeds_005()

Dim sConsulta As String

'FSPM_LOADFIELDSCALC
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_LOADFIELDSCALC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_LOADFIELDSCALC]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LOADFIELDSCALC] @CIA INT,@FORMULARIO INT,  @IDI VARCHAR(50)='SPA', @SOLICITUD INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WHERE ID=@CIA  AND PORT =0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' + @FSGS + 'FSWS_LOADFIELDSCALC @IDI = @IDI, @SOLICITUD=@SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(20), @SOLICITUD  INT ', @IDI = @IDI, @SOLICITUD=@SOLICITUD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub
