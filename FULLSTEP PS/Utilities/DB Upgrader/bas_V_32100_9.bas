Attribute VB_Name = "bas_V_32100_9"
Public Function CodigoDeActualizacion32100_07_26_02_A32100_07_26_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_9_Storeds_001
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.26.01'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_07_26_02_A32100_07_26_01 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_07_26_02_A32100_07_26_01 = False
End Function

Private Sub V_32100_9_Storeds_001()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADINSTFIELDSDESGLOSECALC]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_EMPRESAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_EMPRESAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADDESGLOSEPADREVISIBLE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADDESGLOSEPADREVISIBLE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETDESGLOSE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETDESGLOSE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion32100_07_26_01_A32100_07_26_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String
    Dim sfecha As String
        
    On Error GoTo Error
    ExecuteSQL gRDOCon, "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Cambios en Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value > frmProgreso.ProgressBar1.Max - 100 Then frmProgreso.ProgressBar1.Value = 1
    V_32100_9_Storeds_002
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE [dbo].[VERSION] SET NUM ='3.00.26.02'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110)"
    ExecuteSQL gRDOCon, sConsulta
    
    ExecuteSQL gRDOCon, "COMMIT TRANSACTION"
    bTransaccionEnCurso = False
    
    CodigoDeActualizacion32100_07_26_01_A32100_07_26_02 = True
    Exit Function
    
Error:
    If bTransaccionEnCurso Then
       gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion32100_07_26_01_A32100_07_26_02 = False
End Function


Private Sub V_32100_9_Storeds_002()
Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_OBTENER_SERVICIO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_OBTENER_SERVICIO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_LOGIN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_LOGIN]" & vbCrLf
sConsulta = sConsulta & "@CIA VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSGS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SELECT @FSGS=FSGS_SRV + '.' + FSGS_BD +'.dbo.' FROM CIAS WITH(NOLOCK) WHERE FSGS_BD is not null and FCEST=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT CIAS.COD AS CIACOD,CIAS.ID AS CIAID,CIAS.MON AS MON_CIA," & vbCrLf
sConsulta = sConsulta & "              CIAS.USUPPAL AS USUPPAL,CIAS_PORT.FPEST AS CIAFPEST,USU.ID," & vbCrLf
sConsulta = sConsulta & "              USU.COD,USU.PWD,USU.FECPWD,USU.APE,USU.NOM,USU.DEP,USU.CAR,USU.TFNO," & vbCrLf
sConsulta = sConsulta & "              USU.TFNO2,USU.TFNO_MOVIL,USU.FAX,USU.EMAIL,USU.DECIMALFMT DECIMALFMT," & vbCrLf
sConsulta = sConsulta & "              USU.THOUSANFMT THOUSANFMT,USU.PRECISIONFMT PRECISIONFMT," & vbCrLf
sConsulta = sConsulta & "              USU.DATEFMT DATEFMT,USU.MOSTRARFMT MOSTRARFMT,USU.TIPOEMAIL TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "              IDI.COD AS IDICOD,USU_PORT.CIA,USU_PORT.USU,USU_PORT.FPEST," & vbCrLf
sConsulta = sConsulta & "              USU.MON AS MON_USU,MON.EQUIV AS EQUIV_CIA,MON.COD AS MONCOD_CIA,MON2.EQUIV AS EQUIV_USU,MON2.COD AS MONCOD_USU," & vbCrLf
sConsulta = sConsulta & "              CIAS.DEN AS CIADEN,CIAS.NIF,IDI.LANGUAGETAG,USU.NIF USUNIF,CON.ID IDCONTACTO" & vbCrLf
sConsulta = sConsulta & "           FROM USU WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN USU_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.ID=USU_PORT.USU AND USU_PORT.PORT=0 AND USU.CIA=USU_PORT.CIA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN CIAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.CIA=CIAS.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN CIAS_PORT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.CIA=CIAS_PORT.CIA AND CIAS_PORT.PORT=0" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON CIAS.MON=MON.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON MON2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.MON=MON2.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IDI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.IDI=IDI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'PROVE PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON CIAS.COD=PROVE.FSP_COD" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ' + @FSGS + 'CON WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON USU.ID=CON.ID_PORT AND PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "           WHERE USU.COD=''' + @COD + ''' AND CIAS.COD=''' + @CIA + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


End Sub

