VERSION 5.00
Begin VB.Form frmPrincipal 
   ClientHeight    =   3195
   ClientLeft      =   885
   ClientTop       =   1830
   ClientWidth     =   4680
   Icon            =   "frmPrincipal.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private gInstancia As String
Private gServidor As String
Private gBaseDatos As String

''' <summary>
''' Punto de entrada a la aplicaci�n
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub Form_Load()

Dim summitconn As String
Dim oADOConnection As ADODB.Connection
Dim oCrypt2 As cCrypt2
Dim sKeyString As String
Dim sCod As String
Dim sPWD As String
Dim oAdores As ADODB.Recordset
Dim iTipo As Integer
Dim dateFecPwd As Date
Dim sUsuario As String

    Conectar = 0
    CargarInstancia
    
    If gServidor = "" Or gBaseDatos = "" Then
        gServidor = GetStringSetting("FULLSTEP PS", gInstancia, "Conexion", "Servidor")
        gBaseDatos = GetStringSetting("FULLSTEP PS", gInstancia, "Conexion", "BaseDeDatos")
    End If
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Server=" & gServidor & ";Database=" & gBaseDatos & ";", "summitportal", "4y0bc115mq6nyt5sszrx"
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120

    
    'Recojer c�digo de usuario del sistema
    sUsuario = ShowUserName
    
    Set oAdores = New ADODB.Recordset
    oAdores.Open "SELECT USU,PWD,FECPWD FROM ADM", oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    If oAdores.EOF Then
        oAdores.Close
        Set oAdores = Nothing
        oADOConnection.Close
        Set oADOConnection = Nothing
        Err.Raise vbObjectError + 1000, "Load", "No se ha establecido la entrada en la BD correspondiente al administrador"
    End If
    
    sCod = oAdores.Collect(0)
    sPWD = oAdores.Collect(1)
    dateFecPwd = oAdores.Collect(2)
    oAdores.Close
    Set oAdores = Nothing
    oADOConnection.Close
    Set oADOConnection = Nothing
    ''Desencriptar el usuario
    'sCod = DesEncriptarAES(sCod, dateFecPwd)
    If sCod = sUsuario Then
       'Comprobar contrase�a
        sPWD = DesEncriptarAES(sPWD, dateFecPwd)
        ' Lanzar Fullstep con ese usuario
        Shell App.Path & "\FSPSClient.exe" & " " & sUsuario & " " & sPWD
    End If
    
    End
       
    
End Sub



Private Function ShowUserName() As String

    Dim lpbuff As String
    Dim nSize As Long
    Dim res As Long

    nSize = 25&
    lpbuff = Space(20)
    res = GetUserName(lpbuff, nSize)
    ShowUserName = Left(lpbuff, nSize - 1)

End Function
Private Function CargarInstancia()
Dim oFos As FileSystemObject
Dim ostream As TextStream

    Set oFos = New FileSystemObject
    
    Set ostream = oFos.OpenTextFile(App.Path & "\FSPSClient.ini", ForReading, True)
    
    If Not ostream Is Nothing Then
        If Not ostream.AtEndOfStream Then
            gInstancia = ostream.ReadLine
            If InStr(1, gInstancia, "INSTANCIA=") Then
                gInstancia = Right(gInstancia, Len(gInstancia) - 10)
                '10 es el n�mero de letras de 'INSTANCIA=' +1
                If Not ostream.AtEndOfStream Then
                    gServidor = ostream.ReadLine
                    If InStr(1, gServidor, "SERVIDOR=") Then
                        gServidor = Right(gServidor, Len(gServidor) - 9)
                        If Not ostream.AtEndOfStream Then
                            gBaseDatos = ostream.ReadLine
                            If InStr(1, gBaseDatos, "BASEDATOS=") Then
                                gBaseDatos = Right(gBaseDatos, Len(gBaseDatos) - 10)
                            End If
                        End If
                    End If
                End If
                
            Else
                Err.Raise 10000, "FULLSTEP PS", "Instance name missing"
                End
            End If
        Else
            Err.Raise 10000, "FULLSTEP PS", "Instance name missing"
            End
        End If
    End If
    ostream.Close
    Set ostream = Nothing
    
    
    Set oFos = Nothing
End Function

