Attribute VB_Name = "basUtilidades"
Option Explicit

Public Const Clavepar = "agkag�"
Public Const Claveimp = "h+hlL_"

Public Function DblQuote(ByVal StrToDblQuote As String) As String

    Dim Ind As Long
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function
Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function

''' <summary>
''' Funci�n que desencripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a desencriptar.</param>
''' <param name="Fecha">Fecha para la desencriptaci�n</param>
''' <returns>El string desencriptado.</returns>
''' <remarks>Llamada desde:  frmPrincipal.Form_load ; Tiempo m�ximo: 0,1</remarks>
Public Function DesEncriptarAES(ByVal Dato As String, Optional ByVal Fecha As Variant) As String

    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As cCrypt2
    
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
    diacrypt = Day(Fecha)

    fechahoracrypt = Fecha
    
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = Dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = Clavepar
    Else
        oCrypt2.Codigo = Claveimp
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Decrypt
    DesEncriptarAES = oCrypt2.OutBuffer
            
    Set oCrypt2 = Nothing
End Function

''' <summary>
''' Funci�n que encripta un string que se le pasa como par�metro.
''' </summary>
''' <param name="Dato">Dato a encriptar.</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <returns>El string encriptado.</returns>
''' <remarks>Llamada desde: CRaiz.Encriptar ; Tiempo m�ximo: 0,1</remarks>
Public Function EncriptarAES(ByVal Dato As String, Optional ByVal Fecha As Variant) As String
                
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As cCrypt2
        
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
        
    diacrypt = Day(Fecha)
    
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = Dato
    If diacrypt Mod 2 = 0 Then
        oCrypt2.Codigo = Clavepar
    Else
        oCrypt2.Codigo = Claveimp
    End If
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    oCrypt2.Encrypt
    EncriptarAES = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function
